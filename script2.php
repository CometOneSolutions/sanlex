<?php

//for adding additional permission

namespace Sanlex;

$newArray = [
    '[PUR] Purchase Order Accessory',
    '[PUR] Receiving Accessory'
];

$actions = ['Create', 'Read', 'Update', 'Delete'];

foreach ($newArray as $newName) {
    foreach ($actions as $action) {
        $permission = new PermissionModel;
        $permission->setName($action . ' ' . $newName);
        $permission->save();
    }
}

//Temporary seeder for purchase order accessory
// $products = [
//     'Fasteners'          => FastenerModel::first(),
//     'Insulations'        => InsulationModel::first(),
//     'Misc Accessories'   => MiscAccessoryModel::first(),
//     'Paint Adhesives'    => PaintAdhesiveModel::first(),
//     'Pvc Fittings'       => PvcFittingModel::first(),
//     'Skylights'          => SkylightModel::first(),
//     'Stainless Steels'   => StainlessSteelModel::first(),
//     'Structural Steels'  => StructuralSteelModel::first()
// ];


// foreach ($products as $productType => $product) {
//     $purchaseOrderAccessoryDetails = [];
//     $poCounter = 0;

//     $purchaseOrderAccessoryDetail = new App\PurchaseOrderAccessoryDetailModel;
//     $purchaseOrderAccessoryDetail->setUom($product->getProduct()->getUom());
//     $purchaseOrderAccessoryDetail->setProduct($product->getProduct());
//     $purchaseOrderAccessoryDetail->setOrderedQuantity(100);
//     array_push($purchaseOrderAccessoryDetails, $purchaseOrderAccessoryDetail);


//     $poCounter++;
//     app()->make(PurchaseOrderAccessoryRecordServiceImpl::class)->create(
//         $productType . $poCounter,
//         $product->getProduct()->getSupplier(),
//         $productType,
//         new DateTime("now"),
//         'remarks for ' . $productType,
//         $purchaseOrderAccessoryDetails
//     );
// }
