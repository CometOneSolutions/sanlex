README

Questions:
1. Are there purchase order details whose uom is different from the one defined in product?
2.

Notes:
1. Purchase order details do not use received quantity


Products can be
- coil
- skylight
- insulation
- misc accessory
- stainless steel
- structural steel
- fastener
- paint adhesive
- pvc fitting

Purchase orders are for coils
Purchase order details point to products
uom is always MT, where is this used?

Receivings are for coil
Receivings point to product_id, purchase_order_id,

why is product separate from inventories?
what is the purpose of inventories table?

When is inventory created?
- createInventoryFromPurchaseOrder (upon "approval", after uploading)
- createInventoryFromReceivingAccessory (upon "received")
- The difference between coil po and accessory po in terms of having staggered receiving is
  in coil, creation of inventory is when approving PO level
  in accessory, creation of inventory is when approving receiving
  coil PO cannot upload anymore if PO is "received"
  basically, the ui/flow allows for creating multiple receivings in accessories


slit


```mermaid
erDiagram
    products ||--o{ inventories : x
    inventories ||--o{ inventory_details : has
    inventories }o--|| uoms : x
    inventory_details }o--|| uoms : belongs-to
    inventory_details ||--o{ adjustments : can-have
    inventory_details ||--o{ job_order_details : can-have
    inventory_details }o--|| receivings : belongs-to-coil
    inventory_details }o--|| receiving_accessory_details : belongs-to-coil
    products }o--|| uoms : x
```

```mermaid
erDiagram
    purchase_orders ||--|{ purchase_order_details : has
    purchase_order_details }o--|| products : belongs-to
    purchase_order_details }o--|| uoms : belongs-to
    receivings }o--|| purchase_orders : x
    receivings }o--|| products : x
```

```mermaid
erDiagram
    purchase_order_accessories ||--|{ purchase_order_accessory_details : has
    purchase_order_accessory_details }o--|| products : belongs-to
    purchase_order_accessory_details }o--|| uoms : belongs-to
    receiving_accessories ||--|{ receiving_accessory_details : has
    receiving_accessory_details }o--|| products : belongs-to
    receiving_accessories }o--|| purchase_order_accessories : belongs-to
```
    <!--
    receivings ||--o{ inventory_details : has
    suppliers ||--o{ products : supplies
    products ||--|| productables : can-be
    job_orders ||--|{ job_order_details : has -->
