<?php

//Duplicate item movement deletion
$duplicateIds = [
    '13895',
    '13896',

    '19548',
    '19549',

    '19550',
    '19551',

    '19552',
    '19553',
];

foreach ($duplicateIds as $duplicateId) {
    $duplicate = ItemMovementModel::whereId($duplicateId);
    $duplicate->delete();
}
