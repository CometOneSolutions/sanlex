<?php

// Filling Po accs ordered quantity based on receiving qty
$purchaseOrderAccessories = PurchaseOrderAccessoryModel::all();
$purchaseOrderAccessories->each(function ($purchaseOrderAccessory) {
    $purchaseOrderAccessory->getPurchaseOrderAccessoryDetails()->each(function ($purchaseOrderAccessoryDetail) use ($purchaseOrderAccessory) {
        $receivingDetailQuantity = $purchaseOrderAccessory->receivingAccessoryDetails()->product($purchaseOrderAccessoryDetail->getProductId())->sum('quantity');

        if ($purchaseOrderAccessoryDetail->getOrderedQuantity() == 0) {
            $purchaseOrderAccessoryDetail->setOrderedQuantity($receivingDetailQuantity);
            $purchaseOrderAccessoryDetail->save();
        }
    });
});
