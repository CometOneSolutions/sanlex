<?php


$reports = App\SummaryOrder::REPORTS;


collect($reports)->each(function ($report) {
    app()->make(App\SummaryOrderRecordService::class)->create(
        $report,
        []
    );
});
