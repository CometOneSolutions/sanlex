import Index from './components/Index'
import { serviceBus } from './main';

//let format = require('format-number');
//let money = format({decimalsSeparator: '.', round: 2, padRight: 2});
new Vue({
    el: '#index',
    components: {Index, serviceBus},
    data: {
        filterable: filterable,
        baseUrl: baseUrl,
        items: [],
        totals: {},
        isLoading: true,
        sorter: sorter,
        sortAscending: (typeof sortAscending !== "undefined") ? sortAscending : true,
        // conditions: (typeof conditions !== undefined) ? conditions : [],
        // conditions: conditions || [],
    },
    methods: {
        numeric(value) {
            return serviceBus.money(value);
        },
        setSorter(sorter) {
            if (sorter == this.sorter)
                this.sortAscending = !this.sortAscending;
            else
                this.sortAscending = true;
            this.sorter = sorter;
        },

        getSortIcon(column) {
            return {
                'fa-sort-asc': column == this.sorter && this.sortAscending,
                'fa-sort-desc': column == this.sorter && !this.sortAscending,
                'fa-sort': column != this.sorter,
            }
        },
        submit(url) {
            return new Promise((resolve, reject) => {
                axios["DELETE"](url)
                    .then(response => {
                        
                        resolve(response.data);
                    }).catch(error => {
                        
                        reject(error.response.data);
                    });
            });
        }
    }

});