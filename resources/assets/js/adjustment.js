import {Form} from './components/Form';
import { serviceBus } from './main';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'
import VForm from './components/VForm';
import Select2 from './components/Select2';

new Vue({
    el: '#adjustment',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select2, serviceBus
    },
    data: {
        form: new Form({
            to: '',
            from: '',
            remarks: '',
        }),
        dataInitialized: true,
        
    },
    watch: {
        initializationComplete(val) {
            
            this.form.isInitializing = false;
        }
        
    },

    computed: {

        initializationComplete() {
            if (this.dataInitialized)
            {
                this.form.isInitializing = false;
                return true;
            }
            return false;
        }
     
    },
    filters: {
        capitalize: function (value) {
          if (!value) return ''
          value = value.toString()
          return value.charAt(0).toUpperCase() + value.slice(1)
        }
    },
    methods: {
        
        destroy() {  
           this.form.confirm().then((result) => {
                if(result.value) {
                    this.form.delete('/api/adjustments/' + this.form.id)
                    .then(response => {
                        
                        this.$swal({
                            title: 'Success',
                            text: this.form.name.toUpperCase() + ' was removed.',
                            type: 'success'
                        }).then(() => window.location = '/banking/adjustments/');
                    })
                    .catch(error => {
                        serviceBus.errorMessage(error);
                    });
                }
                
            });             
        },
       
        store() {
            this.form.post('/api/adjustments')
            .then(response => {
                this.$swal({
                    title: 'Success',
                    text: this.form.name.toUpperCase() + ' was saved.',
                    type: 'success'
                });
                this.form.reset();
            })
            .catch(error => {
                this.errorMessage(error);
            });

        },

        update() {
            
            this.form.patch('/api/adjustments/' + this.form.id)
            .then(response => {
                this.$swal({
                    title: 'Success',
                    text: this.form.name.toUpperCase() + ' was updated.',
                    type: 'success',
                    confirmButtonColor: '#20a8d8',
                }).then(() => 
                window.location = '/banking/adjustments/' + this.form.id
            );
                
            })
            .catch(error => {
                this.errorMessage(error);
            });
        },
       
        loadData(data) {
            this.form = new Form(data);
        },
    },
    created() {
        
       
     },
    mounted() {
    
        console.log("Init adjustment script...");
        
        this.dataInitialized = false;
       
        if (id != null) {
            this.form.get('/api/adjustments/' + id)
            .then(response => {
                this.loadData(response.data);
            });
        }
        this.dataInitialized = true;

    }
});