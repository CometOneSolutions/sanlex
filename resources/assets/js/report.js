$(document).ready(function () {
    $('tbody').scroll(function (e) {
        $('thead').css("left", -$("tbody").scrollLeft());
        $('thead th.fixed').css("left", $("tbody").scrollLeft());
        $('tbody td.fixed').css("left", $("tbody").scrollLeft());
    });

    $('.print-button').click(function () {
        window.print();
    });
});