import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'
import VForm from './components/VForm';

new Vue({
    el: '#user',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm
    },
    data: {
        form: new Form({
            name: '',
            role: '',
            email: '',
            status: '',
        }),
        dataInitialized: true,
        
    },
    watch: {
        initializationComplete(val) {
            
            this.form.isInitializing = false;
        }
        
    },

    computed: {

        initializationComplete() {
            if (this.dataInitialized)
            {
                this.form.isInitializing = false;
                return true;
            }
            return false;
        }
     
    },
    filters: {
        capitalize: function (value) {
          if (!value) return ''
          value = value.toString()
          return value.charAt(0).toUpperCase() + value.slice(1)
        }
    },
    methods: {
        errorMessage(error) {
            console.log(error);
            this.$swal({
                title: 'Something happened',
                text: 'Error connecting with the server.',
                type: 'error'
            });
        },
        destroy() {
            this.form.confirm().then((result) => {
                if(result.value) {
                    this.form.delete('/api/users/' + this.form.id)
                    .then(response => {
                        
                        this.$swal({
                            title: 'Success',
                            text: this.form.name.toUpperCase() + ' was removed.',
                            type: 'success'
                        }).then(() => window.location = '/master-lists/users/');
                    })
                    .catch(error => {
                        this.errorMessage(error);
                    });
                }
                
            }); 
        },
       
        store() {
           
            this.form.post('/api/users')
            .then(response => {
                this.$swal({
                    title: 'Success',
                    text: this.form.name.toUpperCase() + ' was saved.',
                    type: 'success'
                });
                this.form.reset();
            })
            .catch(error => {
                this.errorMessage(error);
            });
        },

        update() {
            this.form.patch('/api/users/' + this.form.id)
            .then(response => {
                this.$swal({
                    title: 'Success',
                    text: this.form.name.toUpperCase() + ' was updated.',
                    type: 'success',
                    confirmButtonColor: '#20a8d8',
                }).then(() => 
                window.location = '/master-lists/users/' + this.form.id
            );
                
            })
            .catch(error => {
                this.errorMessage(error);
            });
        },
       
        loadData(data) {
            this.form = new Form(data);
        },
    },
   
    mounted() {
    
        console.log("Init location script...");
        
        this.dataInitialized = false;
       
        if (id != null) {
            this.form.get('/api/users/' + id)
            .then(response => {
                this.loadData(response.data);
            });
        }
        this.dataInitialized = true;

    }
});