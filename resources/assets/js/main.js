import {Form} from './components/Form';
import {Money} from 'v-money';


let format = require('format-number');
let money = format({decimalsSeparator: '.', round: 2, padRight: 2});

export const serviceBus = new Vue({
    data: function() {
        return {
            
            peso: {
                decimal: '.',
                thousands: ',',
                prefix: '₱ ',
                precision: 2,
                masked: false
            },
            dollar: {
                decimal: '.',
                thousands: ',',
                prefix: '$ ',
                precision: 2,
                masked: false
            },
            pieces: {
                //decimal: '.',
                thousands: ',',
                //prefix: '₱ ',
                suffix: ' pcs',
                precision: 0,
                masked: false
            }
        }
    },
    
    methods: {
        money(value) {
            return money(value);
        }, 
        
    }
});