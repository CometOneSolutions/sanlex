import { Errors } from './Error';
import swal from 'sweetalert2';


export class Form {

    constructor(data) {



        this.isInitializing = true;     // Loading edit object and list options (customers, etc.)
        this.isSaving = false;          // Saving to DB
        this.isDeleting = false;        // Deleting from DB
        this.isLoading = false;         // Loading table data
        this.isBusy = false;            // Saving or Deleting or Loading
        this.originalData = data;
        // I think slice(0) is not good enough because the items inside may be objects, it's okay if it's just primitive
        // Do a shallow copy for arrays
        for (let field in data) {
            this[field] = Array.isArray(data[field]) ? JSON.parse(JSON.stringify(data[field])) : data[field];
            // this[field] = Array.isArray(data[field]) ? data[field].slice(0) : data[field];
        }
        this.errors = new Errors();
    }

    data() {
        let data = {};
        for (let property in this.originalData) {
            // TODO check this?
            // data[property] = Array.isArray(this[property]) ? JSON.stringify(this[property]) : this[property];
            data[property] = this[property];
        }
        return data;
    }

    reset() {
        console.log('Resetting...');
        for (let field in this.originalData) {
            this[field] = Array.isArray(this.originalData[field]) ? this.originalData[field].slice(0) : this.originalData[field];
        }
        this.errors.clear();
    }

    showAlert(text = '', title = 'Are you sure?', type = 'warning') {
        return swal({
            title: title,
            text: text,
            type: type,
            showCancelButton: true,
        });
    }

    check(url) {
        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(response => {

                    // Process the check results here
                    let message = '';
                    let data = response.data;
                    //
                    //     if (Object.keys(data).length > 0) {
                    //         // Send confirmation
                    //         let message = '';
                    //         for (let field in data) {
                    //             message = field;
                    //         }
                    //     }

                    let result = true;

                    if (result) {
                        resolve(this.showAlert(message));
                    }
                    resolve(true);
                }).catch(error => {
                    reject(error);
                });
        });

    }

    get(url) {
        this.isLoading = true;
        return this.submit('get', url);
    }

    post(url) {
        this.isSaving = true;
        return this.submit('post', url);
    }

    postImage(url, formData) {

        return this.submitImage(url, formData)
    }

    put(url) {
        this.isSaving = true;
        return this.submit('put', url);
    }

    patch(url) {
        this.isSaving = true;
        return this.submit('patch', url);
    }

    delete(url) {
        this.isDeleting = true;
        return this.submit('delete', url);
    }
    confirm(text = "Are you sure?") {
        return swal({
            title: text,
            text: "You will not be able to revert this.",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Yes, delete this.',
            confirmButtonColor: '#f86c6b',
        });
    }
    confirmReceive(text = "Are you sure?") {
        return swal({
            title: text,
            text: "You will not be able to revert this.",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Yes, receive this.',
            confirmButtonColor: '#269320',
        });
    }
    submit(requestType, url) {

        this.isBusy = true;
        // console.log(this.data());
        return new Promise((resolve, reject) => {
            axios[requestType](url, this.data()
            )
                .then(response => {
                    this.onSuccess(response.data);
                    resolve(response.data);
                }).catch(error => {
                    this.onFail(error.response.data);
                    reject(error.response.data);
                });
        });
    }

    submitImage(url, formData, action) {
        return new Promise((resolve, reject) => {
            axios[action](url, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
                .then(response => {
                    this.onSuccess(response.data);
                    resolve(response.data);
                }).catch(error => {
                    this.onFail(error.response.data);
                    reject(error.response.data);
                });
        });
    }

    onSuccess(data) {
        this.isBusy = false;
        this.isDeleting = false;
        this.isSaving = false;
        this.isLoading = false;
    }

    onFail(errors) {
        this.errors.record(errors);
        if (this.errors.hasCode()) {
            swal({
                title: 'Oops!',
                text: this.errors.getMessage(),
                type: 'error'
            }).then(
                () => this.errors.clearAll(),
                (dismiss) => this.errors.clearAll());
        }
        this.isBusy = false;
        this.isDeleting = false;
        this.isSaving = false;
        this.isLoading = false;
        this.isInitializing = false;
    }
}