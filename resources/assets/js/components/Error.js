export class Errors {
    constructor() {
        this.errors = {};
        this.code = '';
        this.message = '';
        this.status_code = '';
    }

    isPageError() {
        return !this.any() && this.status_code != '';
    }

    getMessage() {
        return this.message;
    }

    hasCode() {
        return this.code != '';
    }

    has(field) {
        return this.errors.hasOwnProperty(field);
    }

    any() {
        return Object.keys(this.errors).length > 0;
    }

    get(field) {
        if (this.errors[field]) {
            // TODO what if this is not an array
            return this.errors[field][0];
        }
    }

    list() {
        return this.errors;
    }

    record(apiError) {
        console.log('Recording error...');
        console.log(apiError.message);
        this.code = apiError.hasOwnProperty('code') ? apiError.code : '';
        this.errors = apiError.hasOwnProperty('errors') ? apiError.errors : '';
        this.message = apiError.hasOwnProperty('message') ? apiError.message : '';
        this.status_code = apiError.hasOwnProperty('status_code') ? apiError.status_code : '';
    }

    clearAll() {
        this.errors = {};
        this.code = '';
        this.message = '';
        this.status_code = '';
    }

    clear(field) {
        if (field) {
            delete this.errors[field];
            return;
        }
        this.errors = {};
    }
}