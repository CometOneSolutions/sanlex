@extends('app')
@section('breadcrumbs', Breadcrumbs::render('sales-menu-dashboard.index'))
@section('content')

    <!-- Metrics -->
    <div class="row">
        <div class="col-sm-6 col-md-2">
            <a href="{{route('customer-order_index')}}">
            <div class="card widget">
                <div class="card-body text-center">
                    <div class="h1 text-muted text-center">
                        <i class="far fa-file-alt"></i>
                    </div>
                    <small class="text-muted text-uppercase font-weight-bold">Temporary<br> Order</small>
                </div>
            </div>
            </a>
        </div>
        <div class="col-sm-6 col-md-2">
            <a href="{{route('sales-order_index')}}">
            <div class="card widget">
                <div class="card-body text-center">
                    <div class="h1 text-muted text-center">
                        <i class="fas fa-file"></i>
                    </div>
                    <small class="text-muted text-uppercase font-weight-bold">Commit<br> Order</small>
                </div>
            </div>
            </a>
        </div>
        <div class="col-sm-6 col-md-2">
            <a href="{{route('sales-return_index')}}">
            <div class="card widget">
                <div class="card-body text-center">
                    <div class="h1 text-muted text-center">
                        <i class="fas fa-people-carry"></i>
                    </div>
                    <small class="text-muted text-uppercase font-weight-bold">Return</small><br><br>
                </div>
            </div>
            </a>
        </div>
        <div class="col-sm-6 col-md-2">
            <a href="{{route('inbound-payment_index')}}">
            <div class="card widget">
                <div class="card-body text-center">
                    <div class="h1 text-muted text-center">
                        <i class="fas fa-reply"></i>
                    </div>
                    <small class="text-muted text-uppercase font-weight-bold">Inbound<br> Payment</small>
                </div>
            </div>
            </a>
        </div>
    </div>
    <!-- End Metrics -->
    <!-- Watchlist -->
    <h5>Report</h5>
    <div class="row">
        <div class="col-sm-6 col-md-2">
            <!-- <a href="{{route('sale_report-daily')}}"> -->
            <a href="#">
            <div class="card widget">
                <div class="card-body text-center">
                    <div class="h1 text-muted text-center">
                        <i class="fas fa-print"></i>
                    </div>
                    <small class="text-muted text-uppercase font-weight-bold">Daily Sales</small><br><br>
                </div>
            </div>
            </a>
        </div>
        <div class="col-sm-6 col-md-2">
            <!-- <a href="{{route('sale_report-annual')}}"> -->
            <a href="#">
            <div class="card widget">
                <div class="card-body text-center">
                    <div class="h1 text-muted text-center">
                        <i class="far fa-file-alt"></i>
                    </div>
                    <small class="text-muted text-uppercase font-weight-bold">Annual Sales</small><br><br>
                </div>
            </div>
            </a>
        </div>
        <div class="col-sm-6 col-md-2">
            <!-- <a href="{{route('reports_soa')}}"> -->
            <a href="#">
            <div class="card widget">
                <div class="card-body text-center">
                    <div class="h1 text-muted text-center">
                        <i class="fas fa-print"></i>
                    </div>
                    <small class="text-muted text-uppercase font-weight-bold">Statement of<br> Account</small>
                </div>
            </div>
            </a>
        </div>
        <div class="col-sm-6 col-md-2">
            <!-- <a href="{{route('reports_aging_print')}}"> -->
            <a href="#">
            <div class="card widget">
                <div class="card-body text-center">
                    <div class="h1 text-muted text-center">
                        <i class="far fa-file-alt"></i>
                    </div>
                    <small class="text-muted text-uppercase font-weight-bold">Aging</small><br><br>
                </div>
            </div>
            </a>
        </div>
    </div>

    <!-- End Watchlist-->
@endsection


