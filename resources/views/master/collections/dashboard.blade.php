@extends('app')
@section('breadcrumbs', Breadcrumbs::render('collection.index'))
@section('content')

    <div id="dashboard" class="row">
        @if (auth()->user()->can('Read [COL] Contract'))
            <dashboard-menu link="{{ route('contract_index') }}">
                <template slot="icon">
                    <i class="far fa-file-alt"></i>
                </template>
                <template slot="text">
                    Contract
                </template>
            </dashboard-menu>
        @endif

        @if (auth()->user()->can('Read [COL] Inbound payment'))
            <dashboard-menu link="{{ route('inbound-payment_index') }}">
                <template slot="icon">
                    <i class="fas fa-hand-holding-usd"></i>
                </template>
                <template slot="text">
                    Inbound payment
                </template>
            </dashboard-menu>
        @endif

        @if (auth()->user()->can('Read [MAS] Customer'))
            <dashboard-menu link="{{ route('customer_index') }}">
                <template slot="icon">
                    <i class="fas fa-user-tie"></i>
                </template>
                <template slot="text">
                    Customer
                </template>
            </dashboard-menu>
        @endif
    </div>
@endsection
@push('scripts')
    <script src="{{ mix('js/dashboard.js') }}"></script>
@endpush
