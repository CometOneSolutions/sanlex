@extends('app')
@section('breadcrumbs', Breadcrumbs::render('out-menu-dashboard.index'))
@section('content')

<div id="dashboard" class="row">
    @if(auth()->user()->can('Read [OUT] Delivery Receipt'))
    <dashboard-menu link="{{route('delivery-receipt_index')}}">
        <template slot="icon">
            <i class="far fa-file-alt"></i>
        </template>
        <template slot="text">
            Delivery<br>Receipt
        </template>
    </dashboard-menu>

    @endif
    @if(auth()->user()->can('Read [OUT] Job Order'))
    <dashboard-menu link="{{route('job-order_index')}}">
        <template slot="icon">
            <i class="fab fa-stack-overflow"></i>
        </template>
        <template slot="text">
            Job<br>Order
        </template>
    </dashboard-menu>
    @endif
</div>

@endsection


@push('scripts')
<script src="{{ mix('js/dashboard.js') }}"></script>
@endpush