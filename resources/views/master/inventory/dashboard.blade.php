@extends('app')
@section('breadcrumbs', Breadcrumbs::render('inventory-menu-dashboard.index'))
@section('content')

<!-- Metrics -->
<div id="dashboard" class="row">
    <dashboard-menu link="{{route('inventory_coil_index')}}">
        <template slot="icon">
            <i class="far fa-circle"></i>
        </template>
        <template slot="text">
            Coil<br>Stock
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('inventory_coil_index_archived')}}">
        <template slot="icon">
            <i class="far fa-circle text-secondary"></i>
        </template>
        <template slot="text">
            Coil<br>Stock (<span class="text-danger">0</span>)
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('inventory_accessory_index')}}">
        <template slot="icon">
            <i class="fas fa-tools"></i>
        </template>
        <template slot="text">
            Accessory<br>Stock
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('inventory_accessory_index_archived')}}">
        <template slot="icon">
            <i class="fas fa-tools text-secondary"></i>
        </template>
        <template slot="text">
            Accessory<br>Stock (<span class="text-danger">0</span>)
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('inventory_damaged_index')}}">
        <template slot="icon">
            <i class="fas fa-warehouse"></i>
        </template>
        <template slot="text">
            Damaged<br>Stock
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('adjustment_index')}}">
        <template slot="icon">
            <i class="fas fa-sliders-h"></i>
        </template>
        <template slot="text">
            Adjustments
        </template>
    </dashboard-menu>
</div>

@endsection
@push('scripts')
<script src="{{ mix('js/dashboard.js') }}"></script>
@endpush