@extends('app')
@section('breadcrumbs', Breadcrumbs::render('purchasing-menu-dashboard.index'))
@section('content')

<div id="dashboard" class="row">
    @if(auth()->user()->can('Read [PUR] Purchase Order'))
    <dashboard-menu link="{{route('purchase-order_index')}}">
        <template slot="icon">
            <i class="fas fa-cart-plus"></i>
        </template>
        <template slot="text">
            Purchase Order <br> Coil
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [PUR] Purchase Order Accessory'))
    <dashboard-menu link="{{route('purchase-order-accessory_index')}}">
        <template slot="icon">
            <i class="fas fa-cart-plus"></i>
        </template>
        <template slot="text">
            Purchase Order <br> Accessory
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [PUR] Receiving Accessory'))
    <dashboard-menu link="{{route('receiving-accessory_index')}}">
        <template slot="icon">
            <i class="fas fa-cart-plus"></i>
        </template>
        <template slot="text">
            Receiving <br> Accessory
        </template>
    </dashboard-menu>
    @endif
</div>
@endsection
@push('scripts')
<script src="{{ mix('js/dashboard.js') }}"></script>
@endpush