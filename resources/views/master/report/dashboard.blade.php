@extends('app')
@section('breadcrumbs', Breadcrumbs::render('report-menu-dashboard.index'))
@section('content')
@if (session('status'))
<div class="alert alert-danger">
	{{ session('status') }}
</div>
@endif
<div class="row" id="dashboard">
	@if(auth()->user()->can('Read [REP] Reports'))
	<dashboard-menu link="{{route('reports_coil_total_stock-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			Total Stocks
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_coil_color-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			Coil Summary By Color
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_coil_thickness-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			Coil Summary By Thickness
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_pe-form-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			PE Foam Summary
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_fiberglass-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			Fiberglass Summary
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_bubble-core-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			Bubble Core Summary
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_skylight-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			Skylight Summary
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_misc-accessory-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			Misc Accessory Summary
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_stainless-steel-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			Stainless Steel Summary
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_structural-steel-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			Structural Steel Summary
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_pvc-fitting-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			PVC Fitting Summary
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_paint-adhesive-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			Paint Adhesive Summary
		</template>
	</dashboard-menu>

	<dashboard-menu link="{{route('reports_fastener-index')}}" :open-in-new-tab="true">
		<template slot="icon">
			<i class="far fa-chart-bar"></i>
		</template>
		<template slot="text">
			Fastener Summary
		</template>
	</dashboard-menu>
	@endif
</div>

@endsection

@push('scripts')
<script src="{{ mix('js/dashboard.js') }}"></script>
@endpush