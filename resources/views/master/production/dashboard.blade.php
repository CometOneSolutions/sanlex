@extends('app')
@section('breadcrumbs', Breadcrumbs::render('production-menu-dashboard.index'))
@section('content')
  

    <!-- Metrics -->
    <div class="row">
        <div class="col-sm-6 col-md-2">
            <a href="{{route('job-sheet_index')}}">
            <div class="card widget">
                <div class="card-body text-center">
                    <div class="h1 text-muted text-center">
                        <i class="fas fa-toilet-paper"></i>
                    </div>
                    <small class="text-muted text-uppercase font-weight-bold">Job<br> Sheet</small>
                </div>
            </div>
            </a>
        </div>
        <div class="col-sm-6 col-md-2">
            <a href="{{route('sheeting-report_index')}}">
            <div class="card widget">
                <div class="card-body text-center">
                    <div class="h1 text-muted text-center">
                        <i class="fab fa-stack-overflow"></i>
                    </div>
                    <small class="text-muted text-uppercase font-weight-bold">Sheeting<br> Report</small>
                </div>
            </div>
            </a>
        </div>
    </div>
    <!-- End Metrics -->
    <!-- Watchlist -->
   
    

    <!-- End Watchlist-->
@endsection


