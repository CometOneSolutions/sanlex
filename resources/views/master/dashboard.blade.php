@extends('app')
@section('breadcrumbs', Breadcrumbs::render('master-file.index'))
@section('content')
<h4>GENERAL</h4>
<!-- Metrics -->
<div id="dashboard">
    <div class="row">
        @if(auth()->user()->can('Read [MAS] Customer'))
        <dashboard-menu link="{{route('customer_index')}}">
            <template slot="icon">
                <i class="fas fa-user-tie"></i>
            </template>
            <template slot="text">
                Customer
            </template>
        </dashboard-menu>
        @endif

        @if(auth()->user()->can('Read [MAS] Supplier'))
        <dashboard-menu link="{{route('supplier_index')}}">
            <template slot="icon">
                <i class="fas fa-building"></i>
            </template>
            <template slot="text">
                Supplier
            </template>
        </dashboard-menu>
        @endif

        @if(auth()->user()->can('Read [MAS] Thickness'))
        <dashboard-menu link="{{route('thickness_index')}}">
            <template slot="icon">
                <i class="far fa-hand-lizard"></i>
            </template>
            <template slot="text">
                Thickness
            </template>
        </dashboard-menu>
        @endif

        @if(auth()->user()->can('Read [MAS] Width'))
        <dashboard-menu link="{{route('width_index')}}">
            <template slot="icon">
                <i class="fas fa-ruler-horizontal"></i>
            </template>
            <template slot="text">
                Width
            </template>
        </dashboard-menu>
        @endif

        @if(auth()->user()->can('Read [MAS] Length'))
        <dashboard-menu link="{{route('length_index')}}">
            <template slot="icon">
                <i class="fas fa-ruler-vertical"></i>
            </template>
            <template slot="text">
                Length
            </template>
        </dashboard-menu>
        @endif

        @if(auth()->user()->can('Read [MAS] Dimension X'))
        <dashboard-menu link="{{route('dimension-x_index')}}">
            <template slot="icon">
                <i class="fas fa-ruler-horizontal"></i>
            </template>
            <template slot="text">
                Dimension X
            </template>
        </dashboard-menu>
        @endif
        @if(auth()->user()->can('Read [MAS] Dimension Y'))
        <dashboard-menu link="{{route('dimension-y_index')}}">
            <template slot="icon">
                <i class="fas fa-ruler-vertical"></i>
            </template>
            <template slot="text">
                Dimension Y
            </template>
        </dashboard-menu>
        @endif
        @if(auth()->user()->can('Read [MAS] Size'))
        <dashboard-menu link="{{route('size_index')}}">
            <template slot="icon">
                <i class="fas fa-ruler-combined"></i>
            </template>
            <template slot="text">
                Size Per Unit
            </template>
        </dashboard-menu>
        @endif

        @if(auth()->user()->can('Read [MAS] User'))
        <dashboard-menu link="{{route('user_index')}}">
            <template slot="icon">
                <i class="fas fa-users"></i>
            </template>
            <template slot="text">
                User
            </template>
        </dashboard-menu>
        @endif

        @if(auth()->user()->can('Read [MAS] Role'))
        <dashboard-menu link="{{route('role_index')}}">
            <template slot="icon">
                <i class="fas fa-user-tag"></i>
            </template>
            <template slot="text">
                Roles
            </template>
        </dashboard-menu>
        @endif
        @if(auth()->user()->can('Read [MAS] Summary Order'))
        <dashboard-menu link="{{route('summary-order_index')}}">
            <template slot="icon">
                <i class="fas fa-sort"></i>
            </template>
            <template slot="text">
                Summary Order
            </template>
        </dashboard-menu>
        @endif

    </div>
    <h4>COIL</h4>
    <div class="row">
        @if(auth()->user()->can('Read [MAS] Panel Profile'))
        <dashboard-menu link="{{route('panel-profile_index')}}">
            <template slot="icon">
                <i class="fas fa-store"></i>
            </template>
            <template slot="text">
                Panel Profile
            </template>
        </dashboard-menu>
        @endif
        @if(auth()->user()->can('Read [MAS] Coating'))
        <dashboard-menu link="{{route('coating_index')}}">
            <template slot="icon">
                <i class="fas fa-fill-drip"></i>
            </template>
            <template slot="text">
                Coating
            </template>
        </dashboard-menu>
        @endif
        @if(auth()->user()->can('Read [MAS] Color'))
        <dashboard-menu link="{{route('color_index')}}">
            <template slot="icon">
                <i class="fas fa-palette"></i>
            </template>
            <template slot="text">
                Color
            </template>
        </dashboard-menu>
        @endif

        @if(auth()->user()->can('Read [MAS] Coil'))
        <dashboard-menu link="{{route('coil_index')}}">
            <template slot="icon">
                <i class="far fa-circle"></i>
            </template>
            <template slot="text">
                Coil
            </template>
        </dashboard-menu>
        @endif
    </div>
    <h4>ACCESSORY PARAMETERS</h4>
    <div class="row">
        @if(auth()->user()->can('Read [MAS] Insulation Type'))
        <dashboard-menu link="{{route('insulation-type_index')}}">
            <template slot="icon">
                <i class="fas fa-toilet-paper"></i>
            </template>
            <template slot="text">
                Ins Type
            </template>
        </dashboard-menu>
        @endif
        @if(auth()->user()->can('Read [MAS] Insulation Type'))
        <dashboard-menu link="{{route('insulation-density_index')}}">
            <template slot="icon">
                <i class="fas fa-balance-scale"></i>
            </template>
            <template slot="text">
                Ins Density
            </template>
        </dashboard-menu>
        @endif
        {{-- @if(auth()->user()->can('Read [MAS] Screw Type'))
    <div class="col-sm-6 col-md-2">
        <a href="{{route('screw-type_index')}}">
        <div class="card widget">
            <div class="card-body text-center">
                <div class="h1 text-muted text-center">
                    <i class="fas fa-screwdriver"></i>
                </div>
                <small class="text-muted text-uppercase font-weight-bold">Screw Type</small>
            </div>
        </div>
        </a>
    </div>
    @endif --}}
    @if(auth()->user()->can('Read [MAS] Skylight Class'))
    <dashboard-menu link="{{route('skylight-class_index')}}">
        <template slot="icon">
            <i class="fas fa-sort-alpha-up"></i>
        </template>
        <template slot="text">
            Skylight Class
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Backing Side'))
    <dashboard-menu link="{{route('backing-side_index')}}">
        <template slot="icon">
            <i class="fas fa-kaaba"></i>
        </template>
        <template slot="text">
            Backing Side
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Stainless Steel Type'))
    <dashboard-menu link="{{route('stainless-steel-type_index')}}">
        <template slot="icon">
            <i class="far fa-sticky-note"></i>
        </template>
        <template slot="text">
            SS Type
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Finish'))
    <dashboard-menu link="{{route('finish_index')}}">
        <template slot="icon">
            <i class="fas fa-adjust"></i>
        </template>
        <template slot="text">
            Finish Type
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Misc Accessory Type'))
    <dashboard-menu link="{{route('misc-accessory-type_index')}}">
        <template slot="icon">
            <i class="fab fa-pushed"></i>
        </template>
        <template slot="text">
            Misc Acc Type
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Structural Steel Type'))
    <dashboard-menu link="{{route('structural-steel-type_index')}}">
        <template slot="icon">
            <i class="far fa-sticky-note"></i>
        </template>
        <template slot="text">
            Structural Type
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Paint Adhesive Type'))
    <dashboard-menu link="{{route('paint-adhesive-type_index')}}">
        <template slot="icon">
            <i class="fas fa-brush"></i>
        </template>
        <template slot="text">
            Paint / Adhesive Type
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Pvc Type'))
    <dashboard-menu link="{{route('pvc-type_index')}}">
        <template slot="icon">
            <i class="fab fa-stack-overflow"></i>
        </template>
        <template slot="text">
            PVC Type
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Pvc Diameter'))
    <dashboard-menu link="{{route('pvc-diameter_index')}}">
        <template slot="icon">
            <i class="far fa-circle"></i></i>
        </template>
        <template slot="text">
            PVC Diameter
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Fastener Type'))
    <dashboard-menu link="{{route('fastener-type_index')}}">
        <template slot="icon">
            <i class="fas fa-paperclip"></i>
        </template>
        <template slot="text">
            Fastener Type
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Fastener Dimension'))
    <dashboard-menu link="{{route('fastener-dimension_index')}}">
        <template slot="icon">
            <i class="fas fa-pencil-ruler"></i>
        </template>
        <template slot="text">
            Fastener Dim
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Brand'))
    <dashboard-menu link="{{route('brand_index')}}">
        <template slot="icon">
            <i class="fab fa-bootstrap"></i>
        </template>
        <template slot="text">
            Brand
        </template>
    </dashboard-menu>
    @endif

</div>

<h4>ACCESSORY</h4>
<div class="row">

    @if(auth()->user()->can('Read [MAS] Accessory'))
    <dashboard-menu link="{{route('accessory_index')}}">
        <template slot="icon">
            <i class="fas fa-tools"></i>
        </template>
        <template slot="text">
            General
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Misc Accessory'))
    <dashboard-menu link="{{route('misc-accessory_index')}}">
        <template slot="icon">
            <i class="fas fa-tools"></i>
        </template>
        <template slot="text">
            Misc Accessory
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Skylight'))
    <dashboard-menu link="{{route('skylight_index')}}">
        <template slot="icon">
            <i class="fas fa-tools"></i>
        </template>
        <template slot="text">
            Skylight
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Insulation'))
    <dashboard-menu link="{{route('insulation_index')}}">
        <template slot="icon">
            <i class="fas fa-tools"></i>
        </template>
        <template slot="text">
            Insulation
        </template>
    </dashboard-menu>
    @endif
    {{-- @if(auth()->user()->can('Read [MAS] Screw'))
    <div class="col-sm-6 col-md-2">
            <a href="{{route('screw_index')}}">
    <div class="card widget">
        <div class="card-body text-center">
            <div class="h1 text-muted text-center">
                <i class="fas fa-tools"></i>
            </div>
            <small class="text-muted text-uppercase font-weight-bold">Screw</small>
        </div>
        </a>
    </div>
    @endif --}}

    @if(auth()->user()->can('Read [MAS] Stainless Steel'))
    <dashboard-menu link="{{route('stainless-steel_index')}}">
        <template slot="icon">
            <i class="fas fa-tools"></i>
        </template>
        <template slot="text">
            Stainless Steel
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Structured Steel'))
    <dashboard-menu link="{{route('structural-steel_index')}}">
        <template slot="icon">
            <i class="fas fa-tools"></i>
        </template>
        <template slot="text">
            Structural Steel
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Paint Adhesive'))
    <dashboard-menu link="{{route('paint-adhesive_index')}}">
        <template slot="icon">
            <i class="fas fa-tools"></i>
        </template>
        <template slot="text">
            Paint Adhesive
        </template>
    </dashboard-menu>
    @endif
    @if(auth()->user()->can('Read [MAS] Fastener'))
    <dashboard-menu link="{{route('fastener_index')}}">
        <template slot="icon">
            <i class="fas fa-tools"></i>
        </template>
        <template slot="text">
            Fastener
        </template>
    </dashboard-menu>

    @endif
    @if(auth()->user()->can('Read [MAS] Pvc Fitting'))
    <dashboard-menu link="{{route('pvc-fitting_index')}}">
        <template slot="icon">
            <i class="fas fa-tools"></i>
        </template>
        <template slot="text">
            PVC Fitting
        </template>
    </dashboard-menu>
    @endif
</div>
</div>

@endsection
@push('scripts')
<script src="{{ mix('js/dashboard.js') }}"></script>
@endpush