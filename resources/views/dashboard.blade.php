@extends('app')
@section('breadcrumbs', Breadcrumbs::render('home'))
@section('content')
@if (session('status'))
<div class="alert alert-danger">
    {{ session('status') }}
</div>
@endif

<div id="dashboard" class="row">
    @if(auth()->user()->can('Read [REP] Reports'))
    <dashboard-menu link="{{route('reports_coil_total_stock-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Total Stocks
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_coil_color-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Coil Summary By Color
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_coil_thickness-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Coil Summary By Thickness
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_coil_damaged-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Damaged Coil Summary
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_pe-form-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            PE Foam Summary
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_fiberglass-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Fiberglass Summary
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_bubble-core-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Bubble Core Summary
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_skylight-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Skylight Summary
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_misc-accessory-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Misc Accessory Summary
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_stainless-steel-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Stainless Steel Summary
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_structural-steel-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Structural Steel Summary
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_pvc-fitting-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            PVC Fitting Summary
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_paint-adhesive-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Paint Adhesive Summary
        </template>
    </dashboard-menu>

    <dashboard-menu link="{{route('reports_fastener-index')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Fastener Summary
        </template>
    </dashboard-menu>
    @endif

    @if(auth()->user()->can('Read [OUT] Job Order'))
    <dashboard-menu link="{{route('job-order_pending')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Pending Job Orders (<span class="text-danger">{{$pendingJobOrdersCount}}</span>)
        </template>
    </dashboard-menu>
    @endif

    @if(auth()->user()->can('Read [PUR] Purchase Order'))
    <dashboard-menu link="{{route('purchase-order_pending')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Pending Coil POs (<span class="text-danger">{{$pendingPurchaseOrderCount}}</span>)
        </template>
    </dashboard-menu>
    @endif

    @if(auth()->user()->can('Read [PUR] Purchase Order Accessory'))
    <dashboard-menu link="{{route('purchase-order-accessory_open')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Pending Accessory POs (<span class="text-danger">{{$openPurchaseOrderAccessoryCount}}</span>)
        </template>
    </dashboard-menu>
    @endif

    @if(auth()->user()->can('Read [OUT] Delivery Receipt'))
    <dashboard-menu link="{{route('delivery-receipt_pending')}}" :open-in-new-tab="true">
        <template slot="icon">
            <i class="far fa-chart-bar"></i>
        </template>
        <template slot="text">
            Pending Delivery Receipts (<span class="text-danger">{{$pendingDeliveryReceiptsCount}}</span>)
        </template>
    </dashboard-menu>
    @endif
</div>

<!-- End Watchlist-->
@endsection

@push('scripts')
<script src="{{ mix('js/dashboard.js') }}"></script>
@endpush
