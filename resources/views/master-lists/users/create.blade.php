@extends('app')
@section('breadcrumbs', Breadcrumbs::render('user.master-list.create'))
@section('content')
    <div class="alert alert-primary" role="alert">
        Official Lenox App
    </div>

    <h3>USERS</h3>
    <div>
        <a href="{{route("user_index")}}" role="button" class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
    </div>
    <br/>

    <div class="card table-responsive">
        <div class="card-header">
            Create New User
        </div>
        <div class="card-body">
            <form>
                @include("master-lists.users._form")
            </form>
        </div>
        <div class="card-footer">
            <div class="float-right">
                <a href="{{route("user_index")}}" class="btn btn-success" role="button"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>

@endsection


