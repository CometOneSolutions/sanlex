<div class="form-row">
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Last Name *</label>
        <input type="email" value="{{Route::currentRouteName() == "user_edit" ? "Ng" : ""}}" class="form-control" id="inputEmail4" placeholder="Doe">
    </div>
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">First Name *</label>
        <input type="email" value="{{Route::currentRouteName() == "user_edit" ? "Nexon" : ""}}" class="form-control" id="inputEmail4" placeholder="John">
    </div>
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Username *</label>
        <input type="email" value="{{Route::currentRouteName() == "user_edit" ? "lenoxpaper@yahoo.com" : ""}}" class="form-control" id="inputEmail4" placeholder="hello@lenox.com">
    </div>
    @if(Route::currentRouteName() != "user_edit")
        <div class="form-group col-md-6">
            <label for="inputEmail4" class="col-form-label">Password *</label>
            <input type="email" value="" class="form-control" id="inputEmail4" placeholder="Some Password">
        </div>
    @endif
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Role * <small>(This determines the access level of the user.)</small></label>
        <select class="form-control select2-single">
            <option value="" disabled selected>Please Select</option>
            <option>General Manager</option>
            <option>Secretary</option>
        </select>
    </div>
</div>