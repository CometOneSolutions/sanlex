@extends('app')
@section('breadcrumbs', Breadcrumbs::render('user.master-list.show', $data))
@section('content')
    <div class="alert alert-primary" role="alert">
        Official Lenox App
    </div>

    <h3>USERS</h3>
    <div>
        <a href="{{route("user_index")}}" role="button" class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
    </div>
    <br/>

    <div class="card table-responsive">
        <div class="card-header">
            User Record
            <div class="float-right">
                <a href="{{route("user_edit", [1])}}" class="btn btn-success" role="button" data-toggle="tooltip" data-placement="left" title="Edit this user?"><i class="fa fa-pencil" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="card-body">
            <form>
                @include("master-lists.users._list")
            </form>
        </div>

    </div>

@endsection


