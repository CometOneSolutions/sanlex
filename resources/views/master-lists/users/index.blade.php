
@extends('app')
@section('breadcrumbs', Breadcrumbs::render('user.master-list'))
@section('content')


    <h3>USERS</h3>
    <div>
        <a href="{{route("user_create")}}" role="button" class="btn btn-success" data-toggle="tooltip" data-placement="right" title="Create New User?"><i class="fas fa-plus"></i></a>
    </div>
    <div class="text-right">
        <button type="button" class="btn btn-outline-info"><i class="fa fa-filter" aria-hidden="true"></i></button>
        <button type="button" class="btn btn-outline-danger"><i class="fa fa-times" aria-hidden="true"></i></button>
    </div>
    <br/>
    <form>
        <div class="row">
            <div class="col">
                <select class="form-control select2-single">
                    <option value="" disabled selected>Please Select</option>
                    <option>Name</option>
                    <option>Username</option>
                </select>
            </div>
            <div class="col">
                <input type="text" class="form-control" placeholder="Keyword (e.g. ABC, AB*, *B*)">
            </div>
        </div>
        <br/>
        <div class="text-right">
            <button type="button" class="btn btn-outline-info">Go</button>
        </div>

    </form>
    <br/>

    <div class="card table-responsive">
        <div class="card-body">

            <table class="table table-hover table-bordered">
                <thead class="thead-default">
                <tr>
                    <th>Fullname</th>
                    <th>Username</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row"><a href="{{route("user_show", ["1"])}}">Ng, Nexon</a></th>
                    <td>lenoxpaper@yahoo.com</td>
                    <td>General Manager</td>
                    <td>Active</td>
                    <td class="text-center">
                        <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#cp"><i class="fa fa-key" aria-hidden="true"></i></button>
                        <a href="{{route("user_edit", [1])}}" role="button" class="btn btn-success btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="cp" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="card">
                    <div class="card-header">Change Password</div>
                    <div class="card-body">
                        @include("master-lists.users._changepassword")
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>




    <!-- End Watchlist-->
@endsection


