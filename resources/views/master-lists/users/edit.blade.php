@extends('app')
@section('breadcrumbs', Breadcrumbs::render('user.master-list.edit', $data))
@section('content')
    <div class="alert alert-primary" role="alert">
        Official Lenox App
    </div>

    <h3>USERS</h3>
    <div>
        <a href="{{route("user_show", 1)}}" role="button" class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
    </div>
    <br/>

    <div class="card table-responsive">
        <div class="card-header">
            Edit User Record
        </div>
        <div class="card-body">
            <form>
                @include("master-lists.users._form")
            </form>
        </div>
        <div class="card-footer">
            <small>Last edited by Nexon Ng on November 20, 2017 1:00PM</small>
            <div class="float-right">
                <a href="{{route("user_index")}}" class="btn btn-danger" role="button" data-toggle="tooltip" data-placement="left" title="Delete this user?"><i class="fa fa-trash" aria-hidden="true"></i></a>
                <a href="{{route("user_index")}}" class="btn btn-success" role="button"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>

@endsection


