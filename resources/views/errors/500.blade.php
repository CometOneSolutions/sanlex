<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->

    <title>Please enter credentials.</title>



    <!-- Main styles for this application -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Styles required by this views -->

</head>
<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="clearfix">
                <h1 class="float-left display-3 mr-4">500</h1>
                <h4 class="pt-2">Oops!</h4>
                <p class="text-muted">The page is temporarily unavailable. <br/>Click <a href="/">here</a> to go back.</p>

            </div>
        </div>
    </div>
</div>
</body>
</html>