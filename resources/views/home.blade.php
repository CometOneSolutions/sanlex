@extends('app')
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a>
        <li class="breadcrumb-item active">Purchase Order</li>
    </ol>
@endsection
@section('content')
    <h5>Purchase Orders</h5>
            <div class="card">
                <table class="table table-hover">
                    <thead class="thead-default">
                    <tr>
                        <th>PO No.</th>
                        <th>Container No.</th>
                        <th>Arrival Date</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">JX541</th>
                        <td>PH2345-JJJ</td>
                        <td>October 10, 2017</td>
                        <td>Arrived</td>
                    </tr>
                    <tr>
                        <th scope="row">JX541</th>
                        <td>PH2345-JJJ</td>
                        <td>October 10, 2017</td>
                        <td>Arrived</td>
                    </tr>
                    <tr>
                        <th scope="row">JX541</th>
                        <td>PH2345-JJJ</td>
                        <td>October 10, 2017</td>
                        <td>Arrived</td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-end">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </nav>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <b>New Purchase Order</b>
                </div>
                <div class="card-body">
                    <form>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">PO No.</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="#">

                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Container No.</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="#">
                                    <small id="emailHelp" class="form-text text-muted">Shipment Identifier</small>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Date</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="YYYY-MM-DD">

                                </div>
                            </div>
                        </div>

                        <hr>

                        <h7 class="card-title"><b>Purchase Order Details</b></h7>
                            <p class="card-text text-muted">Add additional items as necessary.</p>
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th class="col-1">#</th>
                                <th class="col-2 text-center">Quantity</th>
                                <th class="col-2 text-center">Unit</th>
                                <th>Item</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td class="text-center">5</td>
                                <td class="text-center">mt</td>
                                <td>C2S #80 Matte</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td class="text-center">10</td>
                                <td class="text-center">mt</td>
                                <td>Bookpaper #50 Yellow</td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td class="text-center">8</td>
                                <td class="text-center">mt</td>
                                <td>Bookpaper #50 Blue</td>
                            </tr>
                            </tbody>
                        </table>
                            <button type="button" class="btn btn-outline-primary">Add Item</button>

                        <br /><br />

                    </form>
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection