<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">

    <title>{{ config('app.name', 'CometOneSolutions') }}</title>

    <!-- Icons -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>

<!-- <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show"> -->

<body class="app header-fixed sidebar-fixed sidebar-lg-show">
    <header class="app-header navbar">
        <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="/">
            <img class="logo navbar-brand-full" src="{{ asset('images/logo.jpeg') }}" width="100" height="50" alt="CoreUI Logo">
            <img class="logo navbar-brand-minimized" src="{{ asset('images/logo.jpeg') }}" width="30" height="30" alt="CoreUI Logo">
        </a>
        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="nav navbar-nav d-md-down-none">
            <li class="nav-item px-3">
                <a class="nav-link" href="{{ route('home') }}">Dashboard</a>
            </li>
            <!-- <li class="nav-item px-3">
                <a class="nav-link" href="#">Users</a>
            </li>
            <li class="nav-item px-3">
                <a class="nav-link" href="#">Settings</a>
            </li> -->
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <!-- <img class="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com"> -->
                    <span class="d-md-down-none">{{ auth()->user()->getName() }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header text-center">
                        <strong>Account</strong>
                    </div>
                    <a class="dropdown-item" href="{{ route('user_show', auth()->user()->getId()) }}">
                        <i class="fa fa-user"></i> Profile</a>
                    <a class="dropdown-item" href="/logout">
                        <i class="fa fa-lock"></i> Logout</a>
                </div>
            </li>
            <li class="nav-item d-md-down-none">
                &nbsp;
            </li>
        </ul>
        <!-- <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
            <span class="navbar-toggler-icon"></span>
        </button> -->
    </header>
    <div class="app-body">
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">

                    @yield('sidebar')
                </ul>
            </nav>
            <button class="sidebar-minimizer brand-minimizer" type="button"></button>
        </div>
        <main class="main">
            <!-- Breadcrumb -->
            @yield('breadcrumbs')

            <div class="container-fluid">
                <div class="animated fadeIn">
                    <!-- <div class="row"> -->
                    @yield('content')

                    <!-- </div> -->
                </div>
            </div>
        </main>
        <!-- <aside class="aside-menu">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#timeline" role="tab">
                        <i class="icon-list"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#messages" role="tab">
                        <i class="icon-speech"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#settings" role="tab">
                        <i class="icon-settings"></i>
                    </a>
                </li>
            </ul>
        </aside> -->
    </div>
    @include('footer')

    <script src="{{ mix('js/app.js') }}"></script>

    @stack('scripts')
   <script type="application/javascript">
/*
   * Hacky fix for a bug in select2 with jQuery 3.6.0's new nested-focus "protection"
   * see: https://github.com/select2/select2/issues/5993
   * see: https://github.com/jquery/jquery/issues/4382
   *
   * TODO: Recheck with the select2 GH issue and remove once this is fixed on their side
   */

   $(document).on('select2:open', () => {
    let allFound = document.querySelectorAll('.select2-container--open .select2-search__field');
    $(this).one('mouseup',()=>{
        setTimeout(()=>{
            allFound[allFound.length - 1].focus();
        },0);
    });
});
    </script>

</body>

</html>
