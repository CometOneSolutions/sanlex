<li class="nav-item">
    <a class="nav-link" href="{{ route('home') }}">
        <i class="nav-icon fas fa-tachometer-alt"></i> Dashboard
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('master-file_index') }}"><i class="nav-icon far fa-folder"></i></i> Master
        File</a>
</li>

@if (auth()->user()->can('Read [PUR] Purchase Order'))
    <li class="nav-item">
        <a class="nav-link" href="{{ route('purchasing-menu-dashboard_index') }}"><i
                class="nav-icon fas fa-hand-holding-usd"></i> Purchasing</a>
    </li>
@endif

@if (auth()->user()->can('Read [INV] Inventory'))
    <li class="nav-item">
        <a class="nav-link" href="{{ route('inventory-menu-dashboard_index') }}"><i
                class="nav-icon fas fa-warehouse"></i> Inventory</a>
    </li>
@endif

@if (auth()->user()->can('Read [REP] Reports'))
    <li class="nav-item">
        <a class="nav-link" href="{{ route('report-menu-dashboard_index') }}"><i
                class="nav-icon far fa-chart-bar"></i> Reports</a>
    </li>
@endif

<li class="nav-item">
    <a class="nav-link" href="{{ route('out-menu-dashboard_index') }}"><i class="nav-icon fas fa-scroll"></i>
        Out</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{ route('collection_index') }}"><i class="nav-icon fas fa-hand-holding-usd"></i>
        Collections</a>
</li>
