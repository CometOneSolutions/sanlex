<?php


DeliveryReceiptModel::all()->each(function ($deliveryReceipt) {
    $deliveryReceipt->timestamps = false;
    $deliveryReceipt->setApprovedDate($deliveryReceipt->updated_at);
    $deliveryReceipt->save();
});
