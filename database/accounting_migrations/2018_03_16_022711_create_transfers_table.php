<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('source_account_id')->unsigned();
            $table->foreign('source_account_id')->references('id')->on('bank_internal_accounts');
            $table->integer('destination_account_id')->unsigned();
            $table->foreign('destination_account_id')->references('id')->on('bank_internal_accounts');
            $table->decimal('amount',15, 2);
            $table->string('remarks')->nullable();
            $table->date('date')->nullable();
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_transfers');
    }
}
