<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('internal_account_id')->unsigned();
            $table->foreign('internal_account_id')->references('id')->on('bank_internal_accounts');
            $table->integer('expense_type_id')->unsigned();
            $table->foreign('expense_type_id')->references('id')->on('bank_expense_types');
            $table->decimal('amount',15, 2);
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_expenses');
    }
}
