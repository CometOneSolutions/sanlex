<?php

use Faker\Generator as Faker;
use App\ProductModel;
use App\PaperTypeModel;

$factory->define(ProductModel::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'sku' => $faker->unique()->uuid,
        'paper_type_id' => PaperTypeModel::inRandomOrder()->first()->getId(),
        'gsm' => random_int(1, 100),
        'variant' => $faker->word,
        'remarks' => $faker->sentence,
    ];
});
