<?php

namespace CometOneSolutions\Auth;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class SanlexPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = [
            '[MAS] Accessory',
            '[MAS] Coil',
            '[MAS] Coating',
            '[MAS] Color',
            // 'Contact',
            '[MAS] Customer',
            '[MAS] Panel Profile',
            '[MAS] Supplier',
            '[MAS] Thickness',
            '[MAS] Width',
            '[MAS] Length',
            '[MAS] Insulation Density',
            '[MAS] Insulation Type',
            '[MAS] Insulation',
            '[MAS] Skylight Class',
            '[MAS] Skylight',
            '[MAS] Screw Type',
            '[MAS] Screw',
            '[MAS] Stainless Steel Type',
            '[MAS] Stainless Steel',
            '[MAS] Backing Side',
            '[MAS] User',
            '[MAS] Role',
            '[PUR] Purchase Order',
            '[OUT] Delivery Receipt',
            '[OUT] Job Order',
        ];

        // Manual 

        $actions = ['Create', 'Read', 'Update', 'Delete'];

        foreach ($actions as $action) {
            foreach ($modules as $module) {
                Permission::create([
                    'name' => $action . ' ' . $module,
                ]);
            }
        }

        $modulesResign = [
            'Approve [OUT] Job Order',
            'Approve [OUT] Delivery Receipt',
            'Approve [OUT] Purchase Order',
            'Read [REP] Reports',
            'Read [INV] Inventory',
            'Adjust [INV] Inventory',
            'Create [INV] Inventory',
            'Toggle-Condition [INV] Inventory',
            'Transfer-SKU [INV] Inventory',
        ];

        foreach ($modulesResign as $data) {
            Permission::create([
                'name' => $data,
            ]);
        }
    }
}
