<?php

use Illuminate\Database\Seeder;

class WidthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        App\WidthModel::create([
            'name' => '915',
            'is_standard' => true,
        ]);
        App\WidthModel::create([
            'name' => '1220',
            'is_standard' => true,
        ]);
    }
}
