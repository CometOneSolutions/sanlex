<?php


use Illuminate\Database\Seeder;

class CoatingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\CoatingModel::create([
            'name' => 'Z35',
        ]);
        App\CoatingModel::create([
            'name' => 'Z50',
        ]);

    }
}
