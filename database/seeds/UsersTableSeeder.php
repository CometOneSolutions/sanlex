<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'username' => 'mrbean',
            'name' => 'mr. bean',
            'email' => 'mr@bean.com',
            'password' => bcrypt('teddy'),
        ]);
    }
}
