<?php


use Illuminate\Database\Seeder;

class ColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\ColorModel::create([
            'name' => 'AQUA BLUE',
        ]);
        App\ColorModel::create([
            'name' => 'BEIGE',
        ]);
        App\ColorModel::create([
            'name' => 'BROWN',
        ]);
        App\ColorModel::create([
            'name' => 'GALVANIZED',
        ]);
        App\ColorModel::create([
            'name' => 'GREEN',
        ]);
        App\ColorModel::create([
            'name' => 'IVORY',
        ]);
        App\ColorModel::create([
            'name' => 'SPANISH RED',
        ]);
        App\ColorModel::create([
            'name' => 'TILE RED',
        ]);
        App\ColorModel::create([
            'name' => 'WHITE',
        ]);
    }
}
