<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use CometOneSolutions\Inventory\LocationRecordService;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $locationRecordService = resolve(LocationRecordService::class);

        $locationRecordService->create('Adjustments', 'In');
        $locationRecordService->create('Purchases', 'Out');
        $locationRecordService->create('Warehouses', 'In');
        $locationRecordService->create('Deliveries', 'In');
        $locationRecordService->create('Job Orders', 'In');
    }
}
