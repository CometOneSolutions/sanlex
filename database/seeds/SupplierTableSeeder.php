<?php


use Illuminate\Database\Seeder;

class SupplierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\SupplierModel::create([
            'name' => 'CMAX',
            'short_code' => 'CMAX'
        ]);
        App\SupplierModel::create([
            'name' => 'ZPXY',
            'short_code' => 'ZPXY'
        ]);
        App\SupplierModel::create([
            'name' => 'XIAOJIN',
            'short_code' => 'XIAOJIN'
        ]);
        App\SupplierModel::create([
            'name' => 'HUAHAI',
            'short_code' => 'HUAHAI'
        ]);
        App\SupplierModel::create([
            'name' => 'DAVID',
            'short_code' => 'DAVID'
        ]);
        App\SupplierModel::create([
            'name' => 'SHANDONG HWAFONE STEEL CO.,LTD.',
            'short_code' => 'HWAFONE'
        ]);
    }
}
