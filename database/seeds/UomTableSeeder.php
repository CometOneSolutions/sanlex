<?php

use Illuminate\Database\Seeder;

class UomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\UomModel::create([
            'name' => 'PIECE',
        ]);

        App\UomModel::create([
            'name' => 'MT',
        ]);
    }
}
