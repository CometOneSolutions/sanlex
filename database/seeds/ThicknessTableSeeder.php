<?php


use Illuminate\Database\Seeder;

class ThicknessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\ThicknessModel::create([
            'name' => '0.27',
        ]);
        App\ThicknessModel::create([
            'name' => '0.35',
        ]);
        App\ThicknessModel::create([
            'name' => '0.47',
        ]);
        App\ThicknessModel::create([
            'name' => '0.57',
        ]);

    }
}
