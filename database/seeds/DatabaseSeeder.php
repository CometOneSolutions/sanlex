<?php

use Illuminate\Database\Seeder;
use CometOneSolutions\Auth\UsersTableSeeder;
use CometOneSolutions\Auth\RolesTableSeeder;
use App\CustomersTableSeeder;
use App\PanelProfilesTableSeeder;

use CometOneSolutions\Auth\SanlexRolesTableSeeder;
use CometOneSolutions\Auth\SanlexPermissionsTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SanlexPermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(SanlexRolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        // $this->call(CustomersTableSeeder::class);
        $this->call(UomTableSeeder::class);
        $this->call(ColorTableSeeder::class);
        $this->call(CoatingTableSeeder::class);
        $this->call(SupplierTableSeeder::class);
        $this->call(ThicknessTableSeeder::class);
        $this->call(WidthTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(PanelProfilesTableSeeder::class);
        // $this->call(ThicknessesTableSeeder::class);
        // $this->call(ColorsTableSeeder::class);
        // $this->call(CoatingsTableSeeder::class);
        // $this->call(SuppliersTableSeeder::class);

        // $this->call(CometOneSolutions\Accounting\AccountingTablesSeeder::class);
        // $this->call(CustomersTableSeeder::class);
        
        // $this->call(PaperTypeTableSeeder::class);
        // $this->call(ProductsTableSeeder::class);
    }
}
