<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsOnCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('customers', function (Blueprint $table) {
            $table->string('telephone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
            $table->dropColumn('main_street_address');
            $table->dropColumn('main_city_address');
            $table->dropColumn('main_zip_address');
            $table->dropColumn('delivery_street_address');
            $table->dropColumn('delivery_city_address');
            $table->dropColumn('delivery_zip_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('telephone');
            $table->dropColumn('mobile');
            $table->dropColumn('email');
            $table->dropColumn('address');
            $table->string('main_street_address')->nullable();
            $table->string('main_city_address')->nullable();
            $table->string('main_zip_address')->nullable();
            $table->string('delivery_street_address')->nullable();
            $table->string('delivery_city_address')->nullable();
            $table->string('delivery_zip_address')->nullable();
        });
    }
}
