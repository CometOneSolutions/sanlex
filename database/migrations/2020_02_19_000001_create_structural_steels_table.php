<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStructuralSteelsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('structural_steels', function (Blueprint $table) {
			$table->increments('id');
			// $table->string('description');
			$table->integer('structural_steel_type_id')->unsigned()->nullable();
			$table->foreign('structural_steel_type_id')->references('id')->on('structural_steel_types');
			$table->integer('thickness_id')->unsigned()->nullable();
			$table->foreign('thickness_id')->references('id')->on('thicknesses');
			$table->integer('dimension_x_id')->unsigned()->nullable();
			$table->foreign('dimension_x_id')->references('id')->on('dimension_x');
			$table->integer('dimension_y_id')->unsigned()->nullable();
			$table->foreign('dimension_y_id')->references('id')->on('dimension_y');
			$table->integer('length_id')->unsigned()->nullable();
			$table->foreign('length_id')->references('id')->on('lengths');
			$table->integer('finish_id')->unsigned()->nullable();
			$table->foreign('finish_id')->references('id')->on('finishes');
			$table->integer('updated_by_user_id')->unsigned()->nullable();
			$table->foreign('updated_by_user_id')->references('id')->on('users');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('structural_steels');
	}
}
