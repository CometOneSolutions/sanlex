<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOrderedQuantityOnDeliveryReceiptDetailsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('delivery_receipt_details', function (Blueprint $table) {
			$table->dropColumn('ordered_quantity');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('delivery_receipt_details', function (Blueprint $table) {

			$table->decimal('ordered_quantity', 15, 2);
		});
	}
}
