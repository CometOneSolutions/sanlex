<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_detail_id')->unsigned();
            $table->foreign('inventory_detail_id')->references('id')->on('inventory_details');
            $table->integer('job_order_id')->unsigned();
            $table->foreign('job_order_id')->references('id')->on('job_orders');
            $table->decimal('length', 15, 2);
            $table->decimal('weight', 15, 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_order_details');
    }
}
