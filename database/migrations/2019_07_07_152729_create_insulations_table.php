<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsulationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insulations', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('description');
            $table->integer('backing_side_id')->unsigned()->nullable();
            $table->foreign('backing_side_id')->references('id')->on('backing_sides');
            $table->integer('insulation_type_id')->unsigned()->nullable();
            $table->foreign('insulation_type_id')->references('id')->on('insulation_types');
            $table->integer('insulation_density_id')->unsigned()->nullable();
            $table->foreign('insulation_density_id')->references('id')->on('insulation_densities');
            $table->integer('thickness_id')->unsigned()->nullable();
            $table->foreign('thickness_id')->references('id')->on('thicknesses');
            $table->integer('width_id')->unsigned()->nullable();
            $table->foreign('width_id')->references('id')->on('widths');
            $table->integer('length_id')->unsigned()->nullable();
            $table->foreign('length_id')->references('id')->on('lengths');
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insulations');
    }
}
