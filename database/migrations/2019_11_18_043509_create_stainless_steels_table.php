<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStainlessSteelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stainless_steels', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('description');
            $table->integer('stainless_steel_type_id')->unsigned()->nullable();
            $table->foreign('stainless_steel_type_id')->references('id')->on('stainless_steel_types');
            $table->integer('thickness_id')->unsigned()->nullable();
            $table->foreign('thickness_id')->references('id')->on('thicknesses');
            $table->integer('width_id')->unsigned()->nullable();
            $table->foreign('width_id')->references('id')->on('widths');
            $table->integer('length_id')->unsigned()->nullable();
            $table->foreign('length_id')->references('id')->on('lengths');
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stainless_steels');
    }
}
