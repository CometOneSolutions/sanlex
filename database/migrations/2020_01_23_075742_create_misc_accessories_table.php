<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiscAccessoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('misc_accessories', function (Blueprint $table) {
			$table->increments('id');
			// $table->string('description');
			$table->integer('backing_side_id')->unsigned();
			$table->foreign('backing_side_id')->references('id')->on('backing_sides');
			$table->integer('length_id')->unsigned();
			$table->foreign('length_id')->references('id')->on('lengths');
			$table->integer('width_id')->unsigned();
			$table->foreign('width_id')->references('id')->on('widths');
			$table->integer('misc_accessory_type_id')->unsigned();
			$table->foreign('misc_accessory_type_id')->references('id')->on('misc_accessory_types');
			$table->integer('updated_by_user_id')->unsigned();
			$table->foreign('updated_by_user_id')->references('id')->on('users');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('misc_accessories');
	}
}
