<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coils', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('color_id')->unsigned()->nullable();
            $table->foreign('color_id')->references('id')->on('colors');
            $table->integer('width_id')->unsigned()->nullable();
            $table->foreign('width_id')->references('id')->on('widths');
            $table->integer('coating_id')->unsigned()->nullable();
            $table->foreign('coating_id')->references('id')->on('coatings');
            $table->integer('thickness_id')->unsigned()->nullable();
            $table->foreign('thickness_id')->references('id')->on('thicknesses');
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coils');
    }
}
