<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryReceiptDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_receipt_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('delivery_receipt_id')->unsigned();
            $table->foreign('delivery_receipt_id')->references('id')->on('delivery_receipts')->onDelete('cascade');

            $table->integer('inventory_detail_id')->unsigned();
            $table->foreign('inventory_detail_id')->references('id')->on('inventory_details');

            $table->integer('ordered_quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_receipt_details');
    }
}
