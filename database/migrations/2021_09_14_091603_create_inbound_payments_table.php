<?php

use CometOneSolutions\Auth\Services\Permissions\PermissionRecordService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInboundPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inbound_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contract_id')->unsigned();
            $table->foreign('contract_id')->references('id')->on('contracts');
            $table->decimal('amount', 15,2);
            $table->date('date');
            $table->string('type');
            $table->date('chk_date')->nullable();
            $table->string('chk_bank')->nullable();
            $table->string('chk_branch')->nullable();
            $table->string('chk_account')->nullable();
            $table->string('chk_number')->nullable();
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });

        $permissionRecordService = resolve(PermissionRecordService::class);
        $permissionRecordService->create('Create [COL] Inbound Payment');
        $permissionRecordService->create('Read [COL] Inbound Payment');
        $permissionRecordService->create('Update [COL] Inbound Payment');
        $permissionRecordService->create('Delete [COL] Inbound Payment');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inbound_payments');
    }
}
