<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFastenersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fasteners', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('description');
            $table->integer('fastener_type_id')->unsigned()->nullable();
            $table->foreign('fastener_type_id')->references('id')->on('fastener_types');
            $table->integer('fastener_dimension_id')->unsigned()->nullable();
            $table->foreign('fastener_dimension_id')->references('id')->on('fastener_dimensions');
            $table->integer('finish_id')->unsigned()->nullable();
            $table->foreign('finish_id')->references('id')->on('finishes');
            $table->integer('size_id')->unsigned()->nullable();
            $table->foreign('size_id')->references('id')->on('sizes');
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fasteners');
    }
}
