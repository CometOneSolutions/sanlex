<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_id')->unsigned();
            $table->foreign('inventory_id')->references('id')->on('inventories');
            $table->integer('receiving_id')->unsigned()->nullable();
            $table->foreign('receiving_id')->references('id')->on('receivings');
            $table->string('slit_identifier')->nullable(); // USED FOR MANUAL INVENTORY ADDING
            $table->integer('uom_id')->unsigned();
            $table->foreign('uom_id')->references('id')->on('uoms');
            // $table->decimal('linear_meter', 15, 2);
            $table->decimal('quantity', 15, 3);
            $table->decimal('received_quantity', 15, 3);
            $table->string('condition');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_details');
    }
}
