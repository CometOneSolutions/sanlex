<?php

use CometOneSolutions\Auth\PermissionRecordService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBrandIdToPvcFittings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissionRecordService = resolve(PermissionRecordService::class);

        Schema::table('pvc_fittings', function (Blueprint $table) {
            $table->integer('brand_id')->unsigned()->default(1);
            $table->foreign('brand_id')->references('id')->on('brands');
        });

        $permissionRecordService->create('Create [MAS] Brand');
        $permissionRecordService->create('Read [MAS] Brand');
        $permissionRecordService->create('Update [MAS] Brand');
        $permissionRecordService->create('Delete [MAS] Brand');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pvc_fittings', function (Blueprint $table) {
            $table->dropForeign(['brand_id']);
            $table->dropColumn('brand_id');
        });
    }
}
