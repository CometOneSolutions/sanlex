<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaintAdhesivesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('paint_adhesives', function (Blueprint $table) {
			$table->increments('id');
			// $table->string('description');
			$table->integer('paint_adhesive_type_id')->unsigned()->nullable();
			$table->foreign('paint_adhesive_type_id')->references('id')->on('paint_adhesive_types');
			$table->integer('color_id')->unsigned()->nullable();
			$table->foreign('color_id')->references('id')->on('colors');
			$table->integer('size_id')->unsigned()->nullable();
			$table->foreign('size_id')->references('id')->on('sizes');
			$table->integer('updated_by_user_id')->unsigned()->nullable();
			$table->foreign('updated_by_user_id')->references('id')->on('users');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('paint_adhesives');
	}
}
