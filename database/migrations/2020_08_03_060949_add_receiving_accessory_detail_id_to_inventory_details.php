<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReceivingAccessoryDetailIdToInventoryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_details', function (Blueprint $table) {
            $table->integer('receiving_accessory_detail_id')->unsigned()->nullable();
            $table->foreign('receiving_accessory_detail_id', 'ra_detail_id')->references('id')->on('receiving_accessory_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_details', function (Blueprint $table) {
            $table->dropForeign(['receiving_accessory_detail_id']);
            $table->dropColumn('receiving_accessory_detail_id');
        });
    }
}
