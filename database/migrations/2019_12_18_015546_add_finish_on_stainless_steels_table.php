<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use CometOneSolutions\Auth\PermissionRecordService;

class AddFinishOnStainlessSteelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissionRecordService = resolve(PermissionRecordService::class);

        Schema::table('stainless_steels', function (Blueprint $table) {
            $table->integer('finish_id')->unsigned()->nullable();
            $table->foreign('finish_id')->references('id')->on('finishes');
        });

        $permissionRecordService->create('Create Finish');
        $permissionRecordService->create('Read Finish');
        $permissionRecordService->create('Update Finish');
        $permissionRecordService->create('Delete Finish');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stainless_steels', function (Blueprint $table) {
            $table->dropForeign(['finish_id']);
            $table->dropColumn('finish_id');
        });
    }
}
