<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderAccessoryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_accessory_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('purchase_order_accessory_id')->unsigned();
            $table->foreign('purchase_order_accessory_id', 'po_accessory_id')->references('id')->on('purchase_order_accessories')->onDelete('cascade');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products');

            $table->integer('uom_id')->unsigned();
            $table->foreign('uom_id')->references('id')->on('uoms');

            $table->decimal('ordered_quantity', 15, 2);
            $table->decimal('received_quantity', 15, 2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_accessory_details');
    }
}
