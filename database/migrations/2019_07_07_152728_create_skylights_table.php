<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkylightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skylights', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('description');
            $table->integer('width_id')->unsigned()->nullable();
            $table->foreign('width_id')->references('id')->on('widths');
            $table->integer('length_id')->unsigned()->nullable();
            $table->foreign('length_id')->references('id')->on('lengths');
            $table->integer('thickness_id')->unsigned()->nullable();
            $table->foreign('thickness_id')->references('id')->on('thicknesses');
            $table->integer('skylight_class_id')->unsigned()->nullable();
            $table->foreign('skylight_class_id')->references('id')->on('skylight_classes');
            $table->integer('panel_profile_id')->unsigned();
            $table->foreign('panel_profile_id')->references('id')->on('panel_profiles');
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skylights');
    }
}
