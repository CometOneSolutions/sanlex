<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSummaryOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary_order_details', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sorter')->unique();
            $table->integer('order')->unsigned();

            $table->integer('summary_order_id')->unsigned();
            $table->foreign('summary_order_id')->references('id')->on('summary_orders')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_order_details');
    }
}
