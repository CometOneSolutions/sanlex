<?php

use CometOneSolutions\Auth\Services\Permissions\PermissionRecordService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->decimal('amount',15, 2);
            $table->decimal('balance', 15,2);
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });

        $permissionRecordService = resolve(PermissionRecordService::class);
        $permissionRecordService->create('Create [COL] Contract');
        $permissionRecordService->create('Read [COL] Contract');
        $permissionRecordService->create('Update [COL] Contract');
        $permissionRecordService->create('Delete [COL] Contract');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
