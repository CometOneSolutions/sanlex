<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('main_street_address')->nullable();
            $table->string('main_city_address')->nullable();
            $table->string('main_zip_address')->nullable();
            // $table->string('telephone')->nullable();
            // $table->string('fax')->nullable();
            // $table->string('email')->nullable();
            $table->string('tin')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('delivery_street_address')->nullable();
            $table->string('delivery_city_address')->nullable();
            $table->string('delivery_zip_address')->nullable();
            $table->decimal('credit_limit', 15, 2)->default(0.00);
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
