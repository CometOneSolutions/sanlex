<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivingAccessoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receiving_accessories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_order_accessory_id')->unsigned();
            $table->foreign('purchase_order_accessory_id')->references('id')->on('purchase_order_accessories');

            $table->string('reference_no')->unique();
            $table->string('remarks')->nullable();
            $table->string('status');
            $table->date('date');

            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receiving_accessories');
    }
}
