<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivingAccessoryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receiving_accessory_details', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('quantity', 15, 2)->nullable();

            $table->integer('receiving_accessory_id')->unsigned();
            $table->foreign('receiving_accessory_id')->references('id')->on('receiving_accessories')->onDelete('cascade');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products');

            $table->integer('old_receiving_id')->unsigned()->nullable();
            $table->foreign('old_receiving_id')->references('id')->on('receivings')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receiving_accessory_details');
    }
}
