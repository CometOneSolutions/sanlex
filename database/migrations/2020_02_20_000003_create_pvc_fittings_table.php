<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePvcFittingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pvc_fittings', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('description');
            $table->integer('pvc_type_id')->unsigned()->nullable();
            $table->foreign('pvc_type_id')->references('id')->on('pvc_types');
            $table->integer('pvc_diameter_id')->unsigned()->nullable();
            $table->foreign('pvc_diameter_id')->references('id')->on('pvc_diameters');

            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pvc_fittings');
    }
}
