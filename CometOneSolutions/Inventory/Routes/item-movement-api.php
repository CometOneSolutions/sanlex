<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'item-movements'], function () use ($api) {
        $api->get('/', 'CometOneSolutions\Inventory\Controllers\ItemMovementApiController@index');
        // $api->get('/list', 'CometOneSolutions\Inventory\Controllers\ItemMovementListApiController@index');
        $api->get('{itemMovementId}', 'CometOneSolutions\Inventory\Controllers\ItemMovementApiController@show');
        $api->post('/', 'CometOneSolutions\Inventory\Controllers\ItemMovementApiController@store');
        $api->patch('{itemMovementId}', 'CometOneSolutions\Inventory\Controllers\ItemMovementApiController@update');
        $api->delete('{itemMovementId}', 'CometOneSolutions\Inventory\Controllers\ItemMovementApiController@destroy');
    });
});
