<?php

Route::group(['prefix' => 'inventories'], function () {
    Route::get('locations', 'LocationController@index')->name('inv_location_index');
    Route::get('locations/create', 'LocationController@create')->name('inv_location_create');
    Route::get('locations/{locationId}', 'LocationController@show')->name('inv_location_show');
    Route::get('locations/{locationId}/edit', 'LocationController@edit')->name('inv_location_edit');
});