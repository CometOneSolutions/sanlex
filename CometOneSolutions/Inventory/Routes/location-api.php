<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'locations'], function () use ($api) {
        $api->get('/', 'CometOneSolutions\Inventory\Controllers\LocationApiController@index');
        // $api->get('/list', 'CometOneSolutions\Inventory\Controllers\LocationListApiController@index');
        $api->get('{locationId}', 'CometOneSolutions\Inventory\Controllers\LocationApiController@show');
        $api->post('/', 'CometOneSolutions\Inventory\Controllers\LocationApiController@store');
        $api->patch('{locationId}', 'CometOneSolutions\Controllers\Inventory\LocationApiController@update');
        $api->delete('{locationId}', 'CometOneSolutions\Controllers\Inventory\LocationApiController@destroy');
    });
});
