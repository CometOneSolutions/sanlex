<?php

Route::group(['prefix' => 'inventories'], function () {
    Route::get('item-movements', 'ItemMovementController@index')->name('item-movement_index');
    Route::get('item-movements/export', 'ItemMovementController@export');
    Route::get('item-movements/create', 'ItemMovementController@create')->name('item-movement_create');
    Route::get('item-movements/{itemMovementId}', 'ItemMovementController@show')->name('item-movement_show');
    Route::get('item-movements/{itemMovementId}/edit', 'ItemMovementController@edit')->name('item-movement_edit');
});
