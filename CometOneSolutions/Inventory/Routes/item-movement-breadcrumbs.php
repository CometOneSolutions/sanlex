<?php

Breadcrumbs::register('inventories.item-movement.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Inventories - Item Movement', route('item-movement_index'));
});
Breadcrumbs::register('inventories.item-movement.create', function ($breadcrumbs) {
    $breadcrumbs->parent('inventories.item-movement.index');
    $breadcrumbs->push('Create', route('item-movement_create'));
});
Breadcrumbs::register('inventories.item-movement.show', function ($breadcrumbs, $itemMovement) {
    $breadcrumbs->parent('inventories.item-movement.index');
    $breadcrumbs->push('Item Movement Details', route('item-movement_show', $itemMovement->getId()));
});
Breadcrumbs::register('inventories.item-movement.edit', function ($breadcrumbs, $itemMovement) {
    $breadcrumbs->parent('inventories.location.show', $itemMovement);
    $breadcrumbs->push('Edit', route('item-movement_edit', $itemMovement->getId()));
});