<?php

Breadcrumbs::register('inventories.location.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Inventories - Location', route('inv_location_index'));
});
Breadcrumbs::register('inventories.location.create', function ($breadcrumbs) {
    $breadcrumbs->parent('inventories.location.index');
    $breadcrumbs->push('Create', route('inv_location_create'));
});
Breadcrumbs::register('inventories.location.show', function ($breadcrumbs, $location) {
    $breadcrumbs->parent('inventories.location.index');
    $breadcrumbs->push('location Details', route('inv_location_show', $location->getId()));
});
Breadcrumbs::register('inventories.location.edit', function ($breadcrumbs, $location) {
    $breadcrumbs->parent('inventories.location.show', $location);
    $breadcrumbs->push('Edit', route('inv_location_edit', $location->getId()));
});
