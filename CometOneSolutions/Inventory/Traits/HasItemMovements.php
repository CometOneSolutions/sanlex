<?php

namespace CometOneSolutions\Inventory\Traits;

use CometOneSolutions\Inventory\Model\ItemMovementModel;

trait HasItemMovements
{
    public function itemMovements()
    {
        return $this->morphMany(ItemMovementModel::class, 'inventoriable');
    }

    public function getItemMovements()
    {
        return $this->itemMovements;
    }

    public function hasItemMovements()
    {
        return $this->itemMovements()->exists();
    }
}
