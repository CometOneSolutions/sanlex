<?php

namespace CometOneSolutions\Inventory\Providers;

use CometOneSolutions\Inventory\Model\ItemMovement;
use CometOneSolutions\Inventory\Model\ItemMovementModel;
use CometOneSolutions\Inventory\Repositories\ItemMovement\EloquentItemMovements;
use CometOneSolutions\Inventory\Repositories\ItemMovement\ItemMovements;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ItemMovementServiceProvider extends ServiceProvider
{
    protected $dbPath = 'CometOneSolutions/Inventory/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(base_path($this->dbPath . 'factories'));
        $this->app->bind(ItemMovementRecordService::class, ItemMovementRecordServiceImpl::class);
        $this->app->bind(ItemMovements::class, EloquentItemMovements::class);
        $this->app->bind(ItemMovement::class, ItemMovementModel::class);
    }
}
