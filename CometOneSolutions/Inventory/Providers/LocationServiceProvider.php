<?php

namespace CometOneSolutions\Inventory\Providers;

use CometOneSolutions\Inventory\Model\Location;
use CometOneSolutions\Inventory\Model\LocationModel;
use CometOneSolutions\Inventory\Repositories\Location\EloquentLocations;
use CometOneSolutions\Inventory\Repositories\Location\Locations;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use CometOneSolutions\Inventory\Services\Location\LocationRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class LocationServiceProvider extends ServiceProvider
{
    protected $dbPath = 'CometOneSolutions/Inventory/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(LocationRecordService::class, LocationRecordServiceImpl::class);
        $this->app->bind(Locations::class, EloquentLocations::class);
        $this->app->bind(Location::class, LocationModel::class);
    }
}
