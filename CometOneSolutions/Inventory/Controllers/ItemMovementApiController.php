<?php

namespace CometOneSolutions\Inventory\Controllers;

use CometOneSolutions\Common\Controllers\ResourceApiController;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
use CometOneSolutions\Inventory\Transformers\ItemMovementTransformer;
use CometOneSolutions\Inventory\Requests\StoreItemMovement;
use CometOneSolutions\Inventory\Requests\UpdateItemMovement;

class ItemMovementApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        ItemMovementRecordService $itemMovementRecordService,
        ItemMovementTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($itemMovementRecordService, $transformer);
        $this->service = $itemMovementRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreItemMovement $request)
    {
        $itemMovement = \DB::transaction(function () use ($request) {
            return $this->service->create(
                $request->getDate(),
                $request->getIn(),
                $request->getOut(),
                $request->getInventoriableType(),
                $request->getInventoriableId(),
                $request->getLocation(),
                $request->getProduct(),
                $request->user()
            );
        });
        return $this->response->item($itemMovement, $this->transformer)->setStatusCode(201);
    }

    public function update($itemMovementId, UpdateItemMovement $request)
    {
        $itemMovement = \DB::transaction(function () use ($itemMovementId, $request) {
            $itemMovement = $this->service->getById($itemMovementId);
            return $this->service->update(
                $itemMovement,
                $request->getDate(),
                $request->getIn(),
                $request->getOut(),
                $request->getInventoriableType(),
                $request->getInventoriableId(),
                $request->getLocation(),
                $request->getProduct(),
                $request->user()
            );
        });
        return $this->response->item($itemMovement, $this->transformer)->setStatusCode(200);
    }

    public function destroy($itemMovementId)
    {
        $itemMovement = $this->service->getById($itemMovementId);
        $this->service->delete($itemMovement);
        return $this->response->item($itemMovement, $this->transformer)->setStatusCode(200);
    }
}
