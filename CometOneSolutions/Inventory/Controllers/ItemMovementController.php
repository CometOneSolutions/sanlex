<?php

namespace CometOneSolutions\Inventory\Controllers;

use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
use JavaScript;
use CometOneSolutions\Common\Controllers\ExportableController;

class ItemMovementController extends ExportableController
{
    private $service;
    protected $exportView = 'item-movements.export';

    public function __construct(ItemMovementRecordService $itemMovementRecordService)
    {
        parent::__construct($itemMovementRecordService);
        $this->service = $itemMovementRecordService;
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/item-movements?include=product,location',
            'exportBaseUrl' => '/inventories/item-movements',
        ]);
        return view('item-movements.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('item-movements.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);

        $itemMovement = $this->service->getById($id);
        return view('item-movements.show', compact('itemMovement'));
    }

    public function edit($id)
    {
        //
        JavaScript::put(['id' => $id]);

        $itemMovement = $this->service->getById($id);
        return view('item-movements.edit', compact('itemMovement'));
    }

    private function getDisplayedFilterable()
    {
        return [
            ['id' => 'name', 'text' => 'Name']
        ];
    }
}
