<?php

namespace CometOneSolutions\Inventory\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use JavaScript;

class LocationController extends Controller
{
    private $service;

    public function __construct(LocationRecordService $locationRecordService)
    {
        $this->service = $locationRecordService;
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/locations'
        ]);
        return view('locations.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('locations.create');
    }

    public function show($id)
    {
        //
        JavaScript::put(['id' => $id]);

        $location = $this->service->getById($id);
        return view('locations.show', compact('location'));
    }

    public function edit($id)
    {
        //
        JavaScript::put(['id' => $id]);

        $location = $this->service->getById($id);
        return view('locations.edit', compact('location'));
    }

    private function getDisplayedFilterable()
    {
        return [
            ['id' => 'name', 'text' => 'Name']
        ];
    }
}
