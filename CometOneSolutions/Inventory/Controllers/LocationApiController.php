<?php

namespace CometOneSolutions\Inventory\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use CometOneSolutions\Inventory\Transformers\LocationTransformer;
use CometOneSolutions\Inventory\Requests\StoreLocation;
use CometOneSolutions\Inventory\Requests\UpdateLocation;

class LocationApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        LocationRecordService $locationRecordService,
        LocationTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($locationRecordService, $transformer);
        $this->service = $locationRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreLocation $request)
    {
        $location = \DB::transaction(function () use ($request) {

            return $this->service->create(
                $request->getName(),
                $request->getType(),
                $request->user()
            );
        });
        return $this->response->item($location, $this->transformer)->setStatusCode(201);
    }

    public function update($locationId, UpdateLocation $request)
    {
        $location = \DB::transaction(function () use ($locationId, $request) {
            $location = $this->service->getById($locationId);
            return $this->service->update(
                $location,
                $request->getName(),
                $request->getType(),
                $request->user()
            );
        });
        return $this->response->item($location, $this->transformer)->setStatusCode(200);
    }

    public function destroy($locationId)
    {
        $location = $this->service->getById($locationId);
        $this->service->delete($location);
        return $this->response->item($location, $this->transformer)->setStatusCode(200);
    }

}
