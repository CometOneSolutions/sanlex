<?php

namespace CometOneSolutions\Inventory\Repositories\ItemMovement;

use CometOneSolutions\Common\Repositories\Repository;
use CometOneSolutions\Inventory\Model\ItemMovement;

interface ItemMovements extends Repository
{
    public function save(ItemMovement $itemMovement);

    public function delete(ItemMovement $itemMovement);
}
