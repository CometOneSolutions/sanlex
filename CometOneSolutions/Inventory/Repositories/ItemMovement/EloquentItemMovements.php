<?php

namespace CometOneSolutions\Inventory\Repositories\ItemMovement;

use CometOneSolutions\Common\Repositories\EloquentRepository;
use CometOneSolutions\Inventory\Model\ItemMovement;

class EloquentItemMovements extends EloquentRepository implements ItemMovements
{
    protected function filterByLocation($value)
    {
        return $this->model->whereHas('location', function ($location) use ($value) {
            return $location->where('name', 'LIKE', $value);
        });
    }

    protected function filterByProduct($value)
    {
        return $this->model->whereHas('product', function ($product) use ($value) {
            return $product->where('name', 'LIKE', $value);
        });
    }

    public function getLastUpdatedCoil($value)
    {
        if ($value == 'COIL') {
            return $this->model->whereHas('inventory', function ($inventoryDetail) {
                return $inventoryDetail->whereHas('inventory', function ($inventory) {
                    return $inventory->whereHas('product', function ($product) {
                        return $product->whereProductableType('\App\Modules\Coil\Models\CoilModel');
                    });
                });
            })->orderBy('updated_at', 'DESC')->limit(1)->get();
        }
        return $this->model->whereHas('inventory', function ($inventoryDetail) {
            return $inventoryDetail->whereHas('inventory', function ($inventory) {
                return $inventory->whereHas('product', function ($product) {
                    return $product->where('productable_type', '!=', '\App\Modules\Coil\Models\CoilModel');
                });
            });
        })->orderBy('updated_at', 'DESC')->limit(1)->get();
    }

    public function __construct(ItemMovement $itemMovement)
    {
        parent::__construct($itemMovement);
    }

    public function save(ItemMovement $itemMovement)
    {
        return $itemMovement->save();
    }

    public function delete(ItemMovement $itemMovement)
    {
        return $itemMovement->delete();
    }
}
