<?php

namespace CometOneSolutions\Inventory\Repositories\Location;

use CometOneSolutions\Common\Repositories\EloquentRepository;
use CometOneSolutions\Inventory\Model\Location;

class EloquentLocations extends EloquentRepository implements Locations
{
    public function __construct(Location $location)
    {
        parent::__construct($location);
    }

    public function save(Location $location)
    {
        return $location->save();
    }

    public function delete(Location $location)
    {
        return $location->delete();
    }
}
