<?php

namespace CometOneSolutions\Inventory\Repositories\Location;

use CometOneSolutions\Common\Repositories\Repository;
use CometOneSolutions\Inventory\Model\Location;

interface Locations extends Repository
{
    public function save(Location $location);

    public function delete(Location $location);
}
