<?php

use App\Modules\Inventory\Models\InventoryModel;

return [
    'model' => InventoryModel::class,
    'table' => 'inventories',
    'id' => 'id',
];
