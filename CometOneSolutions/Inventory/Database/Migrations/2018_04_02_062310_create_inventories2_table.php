<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventories2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Locations
        Schema::create('inv_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->enum('type', ['In', 'Out']);
            $table->timestamps();
        });

        // Item Movements
        Schema::create('inv_item_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->decimal('in', 15, 4)->nullable();
            $table->decimal('out', 15, 4)->nullable();

            $table->decimal('quantity', 15, 4);
            $table->enum('type', ['In', 'Out']);

            $table->string('inventoriable_type');
            $table->integer('inventoriable_id')->unsigned();

            $table->integer('location_id')->unsigned();
            $table->foreign('location_id')->references('id')->on('inv_locations')->onDelete('restrict');

            $table->integer('inventory_id')->unsigned();
            $table
                ->foreign('inventory_id')
                ->references(config('inventory.id') ?? 'id')
                ->on(config('inventory.table') ?? 'inventories')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_item_movements');
        Schema::dropIfExists('inv_locations');
    }
}
