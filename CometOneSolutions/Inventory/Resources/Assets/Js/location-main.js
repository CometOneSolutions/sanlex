import { Form } from "@c1_common_js/components/Form";

export const locationService = new Vue({
    data: function() {
        return {
            form: new Form({}),
            uri: {
                locations: '/api/locations?limit=' + Number.MAX_SAFE_INTEGER,
            }
        };
    },

    methods: {
        getLocations() {
            return this.form.get(this.uri.locations);
        }
    }
});
