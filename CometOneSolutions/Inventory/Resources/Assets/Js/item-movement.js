import {Form} from '@c1_common_js/components/Form';
import { serviceBus } from '@c1_common_js/main';
import { productService } from '@c1_module_js/Product/Resources/Assets/js/product-main';
import { locationService } from '@c1_inventory_js/Resources/Assets/js/location-main';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select2 from '@c1_common_js/components/Select2';

new Vue({
    el: '#item-movement',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select2, serviceBus, productService, locationService
    },
    data: {
        form: new Form({
            date: '',
            in: '',
            out: '',
            inventoriableType: '',
            inventoriableId: '',
            location: {
                data: []
            },
            product: {
                data: []
            },
        }),
        dataInitialized: true,
        locations: [],
        locationsInitialized: false,
        products: [],
        productsInitialized: false,
    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },

    computed: {

        initializationComplete() {
            return this.dataInitialized && this.productsInitialized && this.locationsInitialized;
        }
     
    },

    methods: {
        selectedInternalAccount()
        {
            let selection = this.sourceAccounts.find(sourceAccount => sourceAccount.id == this.form.sourceAccountId);
            if (selection && selection.currency.name != null) 
            {
                return selection.currency.name;
            }
            return undefined;
        },

        destroy() {  
            this.form.deleteWithConfirmation('/api/withdrawals/' + this.form.id).then(response => {
                this.form.successModal('Withdrawal record has been removed').then(() => 
                    window.location = '/banking/withdrawals/'
                );                
            }); 
        },
       
        store() {
            this.form.postWithModal('/api/withdrawals', null, 'Withdrawal record was saved.');
        },

        update() {
            this.form.patch('/api/withdrawals/' + this.form.id).then(response => {
                this.form.successModal('Withdrawal record has been updated.').then(() => 
                    window.location = '/banking/withdrawals/' + this.form.id
                );
            })
        },
       
        loadData(data) {
            this.form = new Form(data);
        },
    },
    created() {
        locationService.getLocations().then(response => {
            this.locations = response.data;
            this.locationsInitialized = true;
        });

        productService.getProducts().then(response => {
            this.products = response.data;
            this.productsInitialized = true;
        });

        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/item-movements/' + id + '?include=location,product')
            .then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
            });
        }
     },
    mounted() {
    
        console.log("Init item-movement script...");
        
        

    }
    
});

