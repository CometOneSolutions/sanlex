<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-cubes" aria-hidden="true"></i> Inventories</a>
    <ul class="nav-dropdown-items">
        @include('item-movements.nav._side')
        @include('locations.nav._side')
    </ul>
</li>