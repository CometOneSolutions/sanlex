@extends('app')
@section('breadcrumbs', Breadcrumbs::render('inventories.item-movement.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Item Movements
            <div class="float-right">
                <a href="#"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
            </div>
        </div>
        <div class="card-body">
            <index :filterable="filterable" :export-base-url="exportBaseUrl" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending" v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table">
                    <thead>
                        <tr class="d-flex">
                            <th class="text-left col-2">
                                <a v-on:click="setSorter('date')">
                                    Date <i class="fa" :class="getSortIcon('date')"></i>
                                </a>
                            </th>
                            <th class="text-right col-1">
                                In
                            </th>
                            <th class="text-right col-1">
                                Out
                            </th>
                            <th class="text-left col-2">
                                Inventory Type
                            </th>
                            <th class="text-left col-3">
                                Location
                            </th>
                            <th class="text-left col-3">
                                Product
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading" class="d-flex">
                            <td class="text-left col-2">@{{ item.date }}</td>
                            <td class="text-right col-1">@{{ item.in || "--"}}</td>
                            <td class="text-right col-1">@{{ item.out || "--"}}</td>
                            <td class="text-left col-2"><a :href="item.showUri">@{{ item.inventoriableType || "--"}}</a></td>
                            <td class="text-left col-3">@{{ item.location.data.name || "--" }}</td>
                            <td class="text-left col-3">@{{ item.product.data.name || "--" }}</td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>






<!-- End Watchlist-->
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush