
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="sourceAccount">Date</label>
            <p><b>@{{form.date}}</b></p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-4">
        <div class="form-group">
            <label>In</label>
            <p><b>@{{form.in}}</b></p>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <label>Out</label>
            <p><b>@{{form.out}}</b></p>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <label>Inventory Type</label>
            <p><b>@{{form.inventoriableType}}</b></p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="remarks">Location</label>
            <p><b>@{{form.location.data.name}}</b></p>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="remarks">Product</label>
            <p><b>@{{form.product.data.name}}</b></p>
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{ mix('js/item-movement.js') }}"></script>
@endpush
            
