<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Date</th>
            <th>In</th>
            <th>Out</th>
            <th>Inventory Type</th>
            <th>Location</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $itemMovement)
        <tr>
            <td>{{ $itemMovement->getId() }}</td>
            <td>{{ $itemMovement->getDate() }}</td>
            <td>{{ $itemMovement->getIn() }}</td>
            <td>{{ $itemMovement->getOut() }}</td>
            <td>{{ $itemMovement->getType() }}</td>
            <td>{{ $itemMovement->getLocation()->getName() }}</td>
        </tr>
    @endforeach
    </tbody>
</table>