<?php

namespace CometOneSolutions\Inventory\Services\ItemMovement;

use CometOneSolutions\Inventory\Model\Inventoriable;
use CometOneSolutions\Inventory\Model\Inventory;
use CometOneSolutions\Inventory\Model\Location;
use \DateTime;
use CometOneSolutions\Common\Services\RecordService;

interface ItemMovementRecordService extends RecordService
{
	public function deleteFromInventoriable(Inventoriable $inventoriable);

	public function create(
		Inventoriable $inventoriable,
		DateTime $date,
		$type,
		$quantity,
		Location $location,
		Inventory $inventory
	);

	public function updateFromInventoriable(
		Inventoriable $inventoriable,
		DateTime $date,
		$quantity,
		Inventory $inventory
	);
}
