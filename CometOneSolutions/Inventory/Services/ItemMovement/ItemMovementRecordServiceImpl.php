<?php

namespace CometOneSolutions\Inventory\Services\ItemMovement;

use CometOneSolutions\Inventory\Model\Inventoriable;
use CometOneSolutions\Inventory\Model\Inventory;
use CometOneSolutions\Inventory\Model\ItemMovement;
use CometOneSolutions\Inventory\Model\ItemMovementValue;
use CometOneSolutions\Inventory\Model\Location;
use CometOneSolutions\Inventory\Repositories\ItemMovement\ItemMovements;
use \DateTime;
use CometOneSolutions\Common\Services\RecordServiceImpl;


class ItemMovementRecordServiceImpl extends RecordServiceImpl implements ItemMovementRecordService
{
	protected $availableFilters = [
		['id' => 'date', 'text' => 'Date'],
		['id' => 'inventoriable_type', 'text' => 'InventoryDetail Type'],
		['id' => 'location', 'text' => 'Location'],
		['id' => 'product', 'text' => 'Product'],
	];

	protected $itemMovements;
	protected $itemMovement;

	public function __construct(
		ItemMovements $itemMovements,
		ItemMovement $itemMovement
	) {
		parent::__construct($itemMovements);
		$this->itemMovements = $itemMovements;
		$this->itemMovement = $itemMovement;
	}

	public function create(
		Inventoriable $inventoriable,
		DateTime $date,
		$type,
		$quantity,
		Location $location,
		Inventory $inventory
	) {
		$itemMovement = new $this->itemMovement;
		$itemMovement->setInventoriable($inventoriable);
		$itemMovement->setDate($date);
		$itemMovement->setValue(new ItemMovementValue($type, $quantity));
		$itemMovement->setLocation($location);
		$itemMovement->setInventory($inventory);

		$this->itemMovements->save($itemMovement);
		return $itemMovement;
	}

	public function updateFromInventoriable(
		Inventoriable $inventoriable,
		DateTime $date,
		$quantity,
		Inventory $inventory
	) {
		foreach ($inventoriable->getItemMovements() as $itemMovement) {
			$this->update($itemMovement, $date, $quantity, $inventory);
		}
	}

	protected function update(
		ItemMovement $itemMovement,
		DateTime $date,
		$quantity,
		Inventory $inventory
	) {
		$tempItemMovement = clone $itemMovement;
		$tempItemMovement->setDate($date);
		$tempItemMovement->setValue(new ItemMovementValue($tempItemMovement->getType(), $quantity));
		$tempItemMovement->setInventory($inventory);
		$this->itemMovements->save($tempItemMovement);
		return $tempItemMovement;
	}

	public function deleteFromInventoriable(Inventoriable $inventoriable)
	{
		foreach ($inventoriable->getItemMovements() as $itemMovement) {
			$this->delete($itemMovement);
		}
	}

	protected function delete(ItemMovement $itemMovement)
	{
		$this->itemMovements->delete($itemMovement);
		return $itemMovement;
	}
	public function getLastItemMovement($value)
	{

		return $this->itemMovements->getLastUpdatedCoil($value);
	}
}
