<?php

namespace CometOneSolutions\Inventory\Services\Location;

use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Inventory\Model\Location;

interface LocationRecordService extends RecordService
{
    public static function getByName($name, $field = 'name');

    public function create(
        $name,
        $type,
        User $user = null
    );

    public function update(
        Location $location,
        $name,
        $type,
        User $user = null
    );

    public function delete(Location $location);
}
