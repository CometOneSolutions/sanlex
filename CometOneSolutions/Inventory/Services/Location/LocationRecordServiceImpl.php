<?php

namespace CometOneSolutions\Inventory\Services\Location;

use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Traits\CanGetByName;
use CometOneSolutions\Inventory\Model\Location;
use CometOneSolutions\Inventory\Repositories\Location\Locations;

class LocationRecordServiceImpl extends RecordServiceImpl implements LocationRecordService
{
    private $locations;
    private $location;

    use CanGetByName;

    public function __construct(Locations $locations, Location $location)
    {
        parent::__construct($locations);
        $this->locations = $locations;
        $this->location = $location;
    }

    public function create(
        $name,
        $type,
        User $user = null
    ) {
        $location = new $this->location;
        $location->setName($name);
        $location->setType($type);
        if ($user) {
            $location->setUpdatedByUser($user);
        }
        $this->locations->save($location);
        return $location;
    }

    public function update(
        Location $location,
        $name,
        $type,
        User $user = null
    ) {
        $tempLocation = clone $location;
        $tempLocation->setName($name);
        $tempLocation->setType($type);
        if ($user) {
            $tempLocation->setUpdatedByUser($user);
        }
        $this->locations->save($tempLocation);
        return $tempLocation;
    }

    public function delete(Location $location)
    {
        // TODO delete checks
        $this->locations->delete($location);
        return $location;
    }
}
