<?php

namespace CometOneSolutions\Inventory\Model;

interface ItemMovementType
{
    const IN = 'In';
    const OUT = 'Out';
}
