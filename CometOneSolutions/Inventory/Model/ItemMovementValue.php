<?php

namespace CometOneSolutions\Inventory\Model;

class ItemMovementValue
{
    protected $type;
    protected $quantity;

    public function __construct($type, $quantity)
    {
        $this->type = $type;
        $this->quantity = $quantity;
    }

    public function isIn()
    {
        return $this->type === ItemMovementType::IN;
    }

    public function isOut()
    {
        return $this->type === ItemMovementType::OUT;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setValue($type, $quantity)
    {
        $this->type = $type;
        $this->quantity = $quantity;
        return $this;
    }
}
