<?php

namespace CometOneSolutions\Inventory\Model;

interface Inventoriable
{
    public function getId();

    public function itemMovements();

    public function getItemMovements();

    public function hasItemMovements();

}