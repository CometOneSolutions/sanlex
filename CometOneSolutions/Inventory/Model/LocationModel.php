<?php

namespace CometOneSolutions\Inventory\Model;

use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;

class LocationModel extends Model implements Location, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'inv_locations';

    public function getId()
    {
        return $this->id;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }
}
