<?php

namespace CometOneSolutions\Inventory\Model;

use App\Modules\DeliveryReceiptDetails\Models\DeliveryReceiptDetailModel;
use App\Modules\Inventory\Models\InventoryModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;


class ItemMovementModel extends Model implements ItemMovement, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'inv_item_movements';

    protected $dates = ['date'];

    public function location()
    {
        return $this->belongsTo(LocationModel::class, 'location_id');
    }

    public function inventory()
    {
        return $this->belongsTo(config('inventory.model') ?? InventoryModel::class);
    }

    public function inventoriable()
    {
        return $this->morphTo();
    }

    //

    public function isIn()
    {
        return $this->getType() === ItemMovementType::IN;
    }

    public function isOut()
    {
        return $this->getType() === ItemMovementType::OUT;
    }

    public function setValue(ItemMovementValue $value)
    {
        $this->type = $value->getType();
        $this->quantity = $value->getQuantity();
        if ($value->isIn()) {
            $this->in = $value->getQuantity();
            $this->out = null;
        } elseif ($value->isOut()) {
            $this->out = $value->getQuantity();
            $this->in = null;
        }
        return $this;
    }

    public function getValue()
    {
        return new ItemMovementValue($this->type, $this->quantity);
    }

    protected function setQuantity($value)
    {
        $this->quantity = $value;
        return $this;
    }

    protected function setType($value)
    {
        $this->type = $value;
        return $this;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setInventoriable(Inventoriable $inventoriable)
    {
        $this->inventoriable()->associate($inventoriable);
        return $this;
    }

    public function getInventoriable()
    {
        return $this->inventoriable;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDate($value)
    {
        $this->date = $value;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    protected function setIn($value)
    {
        $this->in = $value;
        return $this;
    }

    public function getIn()
    {
        return $this->in;
    }

    protected function setOut($value)
    {
        $this->out = $value;
        return $this;
    }

    public function getOut()
    {
        return $this->out;
    }

    public function getInventoriableType()
    {
        return $this->inventoriable_type;
    }

    public function getInventoriableId()
    {
        return $this->inventoriable_id;
    }

    public function setLocation(Location $location)
    {
        $this->location()->associate($location);
        return $this;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setInventory(Inventory $inventory)
    {
        $this->inventory()->associate($inventory);
        return $this;
    }

    public function getInventory()
    {
        return $this->inventory;
    }

    public function isDeliveryReceipt()
    {
        return $this->getInventoriableType() == DeliveryReceiptDetailModel::class;
    }

    public function getApprovedDate()
    {
        return $this->isDeliveryReceipt()  ? $this->getInventoriable()->getApprovedDate() : $this->getDate();
    }
}
