<?php

namespace CometOneSolutions\Inventory\Model;

interface ItemMovement
{
    public function isIn();

    public function isOut();

    public function setValue(ItemMovementValue $value);

    public function getValue();

    public function getQuantity();

    public function getType();

    public function getId();

    public function setInventoriable(Inventoriable $inventoriable);

    public function getInventoriable();

    public function setDate($value);

    public function getDate();

    public function getIn();

    public function getOut();

    public function getInventoriableType();

    public function getInventoriableId();

    public function setLocation(Location $location);

    public function getLocation();

    public function setInventory(Inventory $inventory);

    public function getInventory();
}
