<?php

namespace CometOneSolutions\Inventory\Model;

interface Location
{
    public function getId();

    public function setName($value);

    public function getName();

    public function setType($value);

    public function getType();
}
