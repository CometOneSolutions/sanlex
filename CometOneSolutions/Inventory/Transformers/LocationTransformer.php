<?php

namespace CometOneSolutions\Inventory\Transformers;

use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;
use CometOneSolutions\Inventory\Model\Location;

class LocationTransformer extends UpdatableByUserTransformer
{
    // protected $availableIncludes = ['destinationAccount', 'sourceAccount'];

    public function transform(Location $location)
    {
        return [
            'id' => (int)$location->getId(),
            'name' => $location->getName(),
            'type' => $location->getType()
        ];
    }

    // public function includeDestinationAccount(Transfer $transfer)
    // {
    //     $destinationAccount = $transfer->getDestinationAccount();
    //     return $this->item($destinationAccount, new InternalAccountTransformer);
    // }
}
