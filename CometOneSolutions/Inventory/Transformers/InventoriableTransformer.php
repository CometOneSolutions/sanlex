<?php

namespace CometOneSolutions\Inventory\Transformers;

use CometOneSolutions\Inventory\Model\Inventoriable;
use League\Fractal;

class InventoriableTransformer extends Fractal\TransformerAbstract
{
    public function transform(Inventoriable $inventoriable)
    {
        return [
            'id' => (int) $inventoriable->getId(),
            'refNo' => $inventoriable->getInventoryRefNo(),
            'showUri' => $inventoriable->getInventoryUri(),
        ];
    }
}
