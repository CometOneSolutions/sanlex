<?php

namespace CometOneSolutions\Inventory\Transformers;

use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;
use CometOneSolutions\Inventory\Model\ItemMovement;

class ItemMovementTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = ['inventoriable'];

    public function transform(ItemMovement $itemMovement)
    {
        return [
            'id' => (int) $itemMovement->getId(),
            'isIn' => $itemMovement->isIn(),
            'isOut' => $itemMovement->isOut(),
            'date' => $itemMovement->getDate()->toDateString(),
            'in' => $itemMovement->getIn(),
            'out' => $itemMovement->getOut(),
            'isDeliveryReceipt' => $itemMovement->isDeliveryReceipt(),
            'approvedDate' => $itemMovement->getApprovedDate() ? $itemMovement->getApprovedDate()->toDateString() : '',
        ];
    }

    public function includeInventoriable(ItemMovement $itemMovement)
    {
        $inventoriable = $itemMovement->getInventoriable();
        return $this->item($inventoriable, new InventoriableTransformer);
    }
}
