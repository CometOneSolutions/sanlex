<?php

namespace CometOneSolutions\Inventory\Requests;

use Dingo\Api\Http\FormRequest;
use \DateTime;

class LocationRequest extends FormRequest
{

    public function getName()
    {
        return $this->input('name');
    }

    public function getType()
    {
        return $this->input('type');
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sourceAccountId' => 'required',
            'amount' => 'required|numeric'
        ];
    }
    public function messages()
    {
        return [
            'sourceAccountId.required'      => 'Source account is required',
            'amount.required'               => 'Amount has no value',
            'amount.numeric'                => 'Amount should be in numeric format'
        ];
    }


}
