<?php

namespace CometOneSolutions\Inventory\Requests;

use App\Modules\Product\Services\ProductRecordService;
use Dingo\Api\Http\FormRequest;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;

use \DateTime;

class ItemMovementRequest extends FormRequest
{
    protected $locationService;
    protected $productService;

    public function __construct(
        LocationRecordService $locationRecordService,
        ProductRecordService $productRecordService
    ) {
        $this->locationService = $locationRecordService;
        $this->productService = $productRecordService;
    }

    public function getLocation()
    {
        return $this->locationService->getById($this->input('locationId'));
    }

    public function getProduct()
    {
        return $this->productService->getById($this->input('productId'));
    }

    public function getDate()
    {
        return new DateTime($this->input('date'));
    }

    public function getIn()
    {
        return $this->input('in');
    }

    public function getOut()
    {
        return $this->input('out');
    }

    public function getInventoriableType()
    {
        return $this->input('inventoriableType');
    }

    public function getInventoriableId()
    {
        return $this->input('inventoriableId');
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sourceAccountId' => 'required',
            'amount' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'sourceAccountId.required' => 'Source account is required',
            'amount.required' => 'Amount has no value',
            'amount.numeric' => 'Amount should be in numeric format'
        ];
    }
}
