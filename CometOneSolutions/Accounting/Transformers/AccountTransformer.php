<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\Account\Account;
use League\Fractal\TransformerAbstract;

class AccountTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['book', 'transactions'];

    public function transform(Account $account)
    {
        return [
            'id' => (int)$account->getId(),
            'title' => $account->getTitle(),
            'accountId' => (int)$account->getId(),
            'normalBalance' => $account->getNormalBalance(),
            'bookId' => $account->getBookId(),
            'showUri' => route('accounts_show', (int)$account->getId()),
            'editUri' => route('accounts_edit', (int)$account->getId()),
            'transactionUri' => route('accounts_transactions', (int)$account->getId()),
        ];
    }

    public function includeBook(Account $account)
    {
        $book = $account->getBook();
        return $this->item($book, new BookTransformer);
    }
}
