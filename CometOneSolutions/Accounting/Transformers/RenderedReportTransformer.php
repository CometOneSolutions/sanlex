<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\RenderedReport\RenderedReport;
use League\Fractal\TransformerAbstract;

class RenderedReportTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['report', 'categoryLines', 'balance'];

    public function transform(RenderedReport $renderedReport)
    {
        $balance = $renderedReport->getBalance();
        return [
            'id' => (int)$renderedReport->getId(),
            'notes' => $renderedReport->getNotes(),
            'dateFrom' => $renderedReport->getDateFrom(),
            'dateTo' => $renderedReport->getDateTo(),
            'reportId' => $renderedReport->getReportId(),
            'showUri' => route('rendered-report_show', [$renderedReport->getReportId(), $renderedReport->getId()]),
            'dr' => $renderedReport->getDr(),
            'cr' => $renderedReport->getCr(),
            'createdAt' => $renderedReport->created_at,
            'balance' => [
                // 'value' => [
                    'amount' => $balance->getValue()->getAmount(),
                    'type' => $balance->getValue()->getType(),
                // ],
            ]
        ];
    }

    public function includeBalance(RenderedReport $renderedReport)
    {
        // return $this->item($renderedReport->getBalance()->getValue(), new TransactionValueTransformer);
    }

    public function includeReport(RenderedReport $renderedReport)
    {
        return $this->item($renderedReport->getReport(), new ReportTransformer);
    }

    public function includeCategoryLines(RenderedReport $renderedReport)
    {
        return $this->collection($renderedReport->getCategoryLines(), new CategoryLineTransformer);
    }
}
