<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\CategoryLine\CategoryLine;
use League\Fractal\TransformerAbstract;

class CategoryLineTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['renderedReport', 'parentCategoryLine'];

    protected $defaultIncludes = ['childCategoryLines', 'accountLines', 'category'];

    public function transform(CategoryLine $categoryLine)
    {
        return [
            'id' => (int)$categoryLine->getId(),
            'cr' => $categoryLine->getCr(),
            'dr' => $categoryLine->getDr()
        ];
    }

    public function includeRenderedReport(CategoryLine $categoryLine)
    {
        return $this->item($categoryLine->getRenderedReport(), new RenderedReportTransformer);
    }

    public function includeCategory(CategoryLine $categoryLine)
    {
        return $this->item($categoryLine->getCategory(), new CategoryTransformer);
    }

    public function includeParentCategoryLine(CategoryLine $categoryLine)
    {
        return $this->item($categoryLine->getParentCategoryLine(), new CategoryLineTransformer);
    }

    public function includeChildCategoryLines(CategoryLine $categoryLine)
    {
        return $this->collection($categoryLine->getChildCategoryLines(), new CategoryLineTransformer);
    }

    public function includeAccountLines(CategoryLine $categoryLine)
    {
        return $this->collection($categoryLine->getAccountLines(), new AccountLineTransformer);
    }

    // public function includeChildCategories(Category $category)
    // {
    //     return $this->collection($category->getChildCategories(), new CategoryTransformer);
    // }
}
