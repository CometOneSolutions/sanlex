<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\Book\Book;
use League\Fractal\TransformerAbstract;

class BookTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['accounts', 'organization', 'entries', 'currency'];

    public function transform(Book $book)
    {
        return [
            'id' => (int)$book->getId(),
            'name' => $book->getName(),
            'showUri' => route('books_show', (int)$book->getId() ),
            'editUri' => route('books_edit', (int)$book->getId() ),
            'transactionUri' => route('books_transactions', (int)$book->getId()),
            'organizationId' => $book->getOrganizationId(),
            'currencyId'    => $book->getCurrencyId(),
        ];
    }

    public function includeAccounts(Book $book)
    {
        return $this->collection($book->getAccounts(), new AccountTransformer);
    }

    public function includeOrganization(Book $book)
    {
        return $this->item($book->getOrganization(), new OrganizationTransformer);
    }

    public function includeEntries(Book $book)
    {
        return $this->collection($book->getEntries(), new EntryTransformer);
    }

    public function includeCurrency(Book $book)
    {
        return $this->item($book->getCurrency(), new CurrencyTransformer);
    }
}
