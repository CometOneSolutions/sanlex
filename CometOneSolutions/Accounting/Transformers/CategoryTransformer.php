<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\Category\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['book', 'parentCategory', 'childCategories', 'accounts'];

    public function transform(Category $category)
    {
        return [
            'id' => (int)$category->getId(),
            'categoryId' => (int)$category->getId(),
            'name' => $category->getName(),
            'normalBalance' => $category->getNormalBalance(),
            'bookId' => $category->getBookId(),
            'parentCategoryId' => $category->getParentCategoryId(),
            'showUri' => route('category_show', (int)$category->getId()),
            'editUri' => route('category_edit', (int)$category->getId()),
            'parentCategory' => $category->getParentCategory(),
        ];
    }

    public function includeBook(Category $category)
    {
        return $this->item($category->getBook(), new BookTransformer);
    }

    public function includeParentCategory(Category $category)
    {
        return $this->item($category->getParentCategory(), new CategoryTransformer);
    }

    public function includeChildCategories(Category $category)
    {
        return $this->collection($category->getChildCategories(), new CategoryTransformer);
    }

    public function includeAccounts(Category $category)
    {
        return $this->collection($category->getAccounts(), new AccountTransformer);
    }
}
