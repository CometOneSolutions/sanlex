<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\Organization\Organization;
use League\Fractal\TransformerAbstract;

class OrganizationTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['books'];

    public function transform(Organization $organization)
    {
        return [
            'id' => (int)$organization->getId(),
            'name' => $organization->getName(),
            'showUri' => route('organization_show', (int)$organization->getId()),
            'editUri' => route('organization_edit', (int)$organization->getId()),
        ];
    }

    public function includeBooks(Organization $organization)
    {
        return $this->collection($organization->getBooks(), new BookTransformer);
    }
}
