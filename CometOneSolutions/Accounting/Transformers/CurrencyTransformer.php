<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\Currency\Currency;
use League\Fractal\TransformerAbstract;

class CurrencyTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['books'];

    public function transform(Currency $currency)
    {
        return [
            'id' => (int)$currency->getId(),
            'code' => $currency->getCode(),
            'name' => $currency->getCode(),
            'description' => $currency->getDescription(),
            'showUri' => route('currency_show', (int)$currency->getId()),
            'editUri' => route('currency_edit', (int)$currency->getId()),
        ];
    }
}
