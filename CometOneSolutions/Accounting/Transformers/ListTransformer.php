<?php

namespace CometOneSolutions\Accounting\Transformers;

use League\Fractal\TransformerAbstract;

class ListTransformer extends TransformerAbstract
{
    public function transform(Listable $listable)
    {
        return [
            'id' => (int) $listable->getId(),
            'text' => $listable->getText(),
        ];
    }
}
