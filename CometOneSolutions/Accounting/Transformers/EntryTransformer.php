<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\Entry\Entry;
use League\Fractal\TransformerAbstract;

class EntryTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['book', 'transactions', 'accountable'];

    public function transform(Entry $entry)
    {
        return [
            'id' => (int)$entry->getId(),
            'date' => $entry->getDate()->toDateString(),
            'bookId' => $entry->getBookId(),
            'notes' => $entry->getNotes(),
            'editUri' => route('entries_edit', (int)$entry->getId()),
        ];
    }

    public function includeAccountable(Entry $entry)
    {
        return $this->item($entry->getAccountable(), new AccountableTransformer);
    }

    public function includeBook(Entry $entry)
    {
        return $this->item($entry->getBook(), new BookTransformer);
    }

    public function includeTransactions(Entry $entry)
    {
        return $this->collection($entry->getTransactions(), new TransactionTransformer);
    }
}
