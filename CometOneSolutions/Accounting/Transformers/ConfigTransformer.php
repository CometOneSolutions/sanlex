<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\Config\Config;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class ConfigTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = ['book', 'organization'];

    public function transform(Config $config)
    {
        return [
            'id' => (int)$config->getId(),
            'currentOrganizationId' => $config->getCurrentOrganizationId(),
            'currentBookId' => $config->getCurrentBookId(),
            'updatedAt'     => $config->updated_at,
            // 'interestRate' => $config->getInterestRate(),
        ];
    }

    public function includeOrganization(Config $config){
        return $this->item($config->getCurrentOrganization(), new OrganizationTransformer);
    }

    public function includeBook(Config $config){
        return $this->item($config->getCurrentBook(), new BookTransformer);
    }
}
