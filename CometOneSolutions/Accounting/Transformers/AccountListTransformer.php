<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\Account\Account;
use League\Fractal\TransformerAbstract;

class AccountListTransformer extends TransformerAbstract
{
    public function transform(Account $account)
    {
        return [
            'id' => (int)$account->getId(),
            'text' => $account->getTitle(),
        ];
    }
}
