<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\Balance;
use League\Fractal\TransformerAbstract;

class BalanceTransformer extends TransformerAbstract
{
    public function transform(Balance $balance)
    {
        return [
            'value' => $balance->getValue(),
        ];
    }
}
