<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\Transaction\Transaction;
use League\Fractal\TransformerAbstract;

class TransactionTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['book', 'account', 'entry'];

    public function transform(Transaction $transaction)
    {
        return [
            'id' => $transaction->getId(),
            'accountId' => $transaction->getAccountId(),
            'type' => $transaction->getType(),
            'amount' => $transaction->getAmount(),
            'editUri' => route('entries_edit', $transaction->getEntryId()),
        ];
    }

    public function includeBook(Transaction $transaction)
    {
        return $this->item($transaction->getBook(), new BookTransformer);
    }

    public function includeAccount(Transaction $transaction)
    {
        return $this->item($transaction->getAccount(), new AccountTransformer);
    }

    public function includeEntry(Transaction $transaction)
    {
        return $this->item($transaction->getEntry(), new EntryTransformer);
    }
}
