<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\AccountLine\AccountLine;
use League\Fractal\TransformerAbstract;

class AccountLineTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['categoryLine'];

    protected $defaultIncludes = ['account'];

    public function transform(AccountLine $accountLine)
    {
        return [
            'id' => (int)$accountLine->getId(),
            'cr' => $accountLine->getCr(),
            'dr' => $accountLine->getDr()
        ];
    }

    public function includeAccount(AccountLine $accountLine)
    {
        return $this->item($accountLine->getAccount(), new AccountTransformer);
    }

    public function includeCategoryLine(AccountLine $accountLine)
    {
        return $this->item($accountLine->getCategoryLine(), new CategoryLineTransformer);
    }
}
