<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Models\Report\Report;
use League\Fractal\TransformerAbstract;

class ReportTransformer extends TransformerAbstract
{

    protected $availableIncludes = ['book', 'categories','renderedReports'];

    public function transform(Report $report)
    {
        return [
            'id' => (int)$report->getId(),
            'description' => $report->getDescription(),
            'showUri' => route('report_show', (int)$report->getId() ),
            'editUri' => route('report_edit', (int)$report->getId() ),
            'bookId' => (int)$report->getBookId()

        ];
    }

    public function includeBook(Report $report)
    {
        return $this->item($report->getBook(), new BookTransformer);
    }

    public function includeCategories(Report $report)
    {
        return $this->collection($report->getCategories(), new CategoryTransformer);
    }

    public function includeRenderedReports(Report $report)
    {
        return $this->collection($report->getRenderedReports(), new RenderedReportTransformer);
    }

}
