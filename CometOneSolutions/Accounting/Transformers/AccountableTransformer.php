<?php

namespace CometOneSolutions\Accounting\Transformers;

use CometOneSolutions\Accounting\Accountable;
use League\Fractal\TransformerAbstract;

class AccountableTransformer extends TransformerAbstract
{
    public function transform(Accountable $accountable)
    {
        return [
            'id' => (int)$accountable->getId(),
            'showUri' => $accountable->getAccountingUri(),
            'refNo' => $accountable->getAccountingRefNo(),
        ];
    }
}
