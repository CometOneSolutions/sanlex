<?php

Breadcrumbs::register('accounting', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Accounting');
});

Breadcrumbs::register('accounts.index', function ($breadcrumbs) {
    $breadcrumbs->parent('accounting');
    $breadcrumbs->push('Accounts', route('accounts_index'));
});

Breadcrumbs::register('accounts.create', function ($breadcrumbs) {
    $breadcrumbs->parent('accounts.index');
    $breadcrumbs->push('Create', route('accounts_create'));
});
Breadcrumbs::register('accounts.show', function ($breadcrumbs, $account) {
    $breadcrumbs->parent('accounts.index');
    $breadcrumbs->push($account->getTitle(), route('accounts_show', $account->getId()));
});
Breadcrumbs::register('accounts.edit', function ($breadcrumbs, $account) {
    $breadcrumbs->parent('accounts.index');
    $breadcrumbs->push($account->getTitle(), route('accounts_show', $account->getId()));
    $breadcrumbs->push('Edit');
});

Breadcrumbs::register('accounts.transactions', function ($breadcrumbs, $account) {
    $breadcrumbs->parent('accounts.index');
    $breadcrumbs->push($account->getTitle(), route('accounts_show', $account->getId()));
    $breadcrumbs->push('Transactions');
});

//books
Breadcrumbs::register('books.index', function ($breadcrumbs) {
    $breadcrumbs->parent('accounting');
    $breadcrumbs->push('Books', route('books_index'));
});
Breadcrumbs::register('books.create', function ($breadcrumbs) {
    $breadcrumbs->parent('books.index');
    $breadcrumbs->push('Create', route('books_create'));
});
Breadcrumbs::register('books.show', function ($breadcrumbs, $book) {
    $breadcrumbs->parent('books.index');
    $breadcrumbs->push($book->getName(), route('books_show', $book->getId()));
});

Breadcrumbs::register('entries.edit', function ($breadcrumbs, $entry) {
    $breadcrumbs->parent('books.show', $entry->getBook());
    $breadcrumbs->push('Entries');
    $breadcrumbs->push($entry->getId());
    $breadcrumbs->push('Edit');
});

Breadcrumbs::register('entries.show', function ($breadcrumbs, $entry) {
    $breadcrumbs->parent('books.show', $entry->getBook());
    $breadcrumbs->push('Entries');
    $breadcrumbs->push($entry->getId());
});

Breadcrumbs::register('books.edit', function ($breadcrumbs, $book) {
    $breadcrumbs->parent('books.index');
    $breadcrumbs->push($book->getName(), route('books_show', $book->getId()));
    $breadcrumbs->push('Edit');
});
Breadcrumbs::register('books.transactions', function ($breadcrumbs, $book) {
    $breadcrumbs->parent('books.index');
    $breadcrumbs->push($book->getName(), route('books_show', $book->getId()));
    $breadcrumbs->push('Transactions');
});

//entries
Breadcrumbs::register('entries.create', function ($breadcrumbs) {
    $breadcrumbs->parent('accounting');
    $breadcrumbs->push('Create', route('entries_create'));
});

//category
Breadcrumbs::register('category.index', function ($breadcrumbs) {
    $breadcrumbs->parent('accounting');
    $breadcrumbs->push('Categories', route('category_index'));
});
Breadcrumbs::register('category.create', function ($breadcrumbs) {
    $breadcrumbs->parent('category.index');
    $breadcrumbs->push('Create', route('category_create'));
});
Breadcrumbs::register('category.show', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('category.index');
    $breadcrumbs->push($category->getName(), route('category_show', $category->getId()));
});
Breadcrumbs::register('category.edit', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('category.index');
    $breadcrumbs->push($category->getName(), route('category_show', $category->getId()));
    $breadcrumbs->push('Edit');
});

//reports
Breadcrumbs::register('report.index', function ($breadcrumbs) {
    $breadcrumbs->parent('accounting');
    $breadcrumbs->push('Reports', route('report_index'));
});
Breadcrumbs::register('report.create', function ($breadcrumbs) {
    $breadcrumbs->parent('report.index');
    $breadcrumbs->push('Create', route('report_create'));
});
Breadcrumbs::register('report.show', function ($breadcrumbs, $report) {
    $breadcrumbs->parent('report.index');
    $breadcrumbs->push($report->getDescription(), route('report_show', $report->getId()));
});
Breadcrumbs::register('report.edit', function ($breadcrumbs, $report) {
    $breadcrumbs->parent('report.index');
    $breadcrumbs->push($report->getDescription(), route('report_show', $report->getId()));
    $breadcrumbs->push('Edit');
});

//Organization
Breadcrumbs::register('organization.index', function ($breadcrumbs) {
    $breadcrumbs->parent('accounting');
    $breadcrumbs->push('Organizations', route('organization_index'));
});
Breadcrumbs::register('organization.create', function ($breadcrumbs) {
    $breadcrumbs->parent('organization.index');
    $breadcrumbs->push('Create', route('organization_create'));
});
Breadcrumbs::register('organization.show', function ($breadcrumbs, $organization) {
    $breadcrumbs->parent('organization.index');
    $breadcrumbs->push($organization->getName(), route('organization_show', $organization->getId()));
});
Breadcrumbs::register('organization.edit', function ($breadcrumbs, $organization) {
    $breadcrumbs->parent('organization.index');
    $breadcrumbs->push($organization->getName(), route('organization_show', $organization->getId()));
    $breadcrumbs->push('Edit');
});

//Currency
Breadcrumbs::register('currency.index', function ($breadcrumbs) {
    $breadcrumbs->parent('accounting');
    $breadcrumbs->push('Currencies', route('currency_index'));
});
Breadcrumbs::register('currency.create', function ($breadcrumbs) {
    $breadcrumbs->parent('currency.index');
    $breadcrumbs->push('Create', route('currency_create'));
});
Breadcrumbs::register('currency.show', function ($breadcrumbs, $currency) {
    $breadcrumbs->parent('currency.index');
    $breadcrumbs->push($currency->getDescription(), route('currency_show', $currency->getId()));
});
Breadcrumbs::register('currency.edit', function ($breadcrumbs, $currency) {
    $breadcrumbs->parent('currency.index');
    $breadcrumbs->push($currency->getDescription(), route('currency_show', $currency->getId()));
    $breadcrumbs->push('Edit');
});

//Config

Breadcrumbs::register('accounting-config.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('accounting');
    $breadcrumbs->push('Config', route('config_index'));
});
