<?php

// Route::group(['middleware' => 'auth'], function () {

Route::group(['prefix' => 'accounting'], function () {
    //accounts
    Route::group(['prefix' => 'accounts'], function () {
        Route::get('/', 'AccountController@index')->name('accounts_index');
        Route::get('/export', 'AccountController@export');
        Route::get('create', 'AccountController@create')->name('accounts_create');
        Route::get('{accountId}', 'AccountController@show')->name('accounts_show');
        Route::get('{accountId}/edit', 'AccountController@edit')->name('accounts_edit');
        Route::get('{accountId}/transactions', 'AccountController@transactions')->name('accounts_transactions');
    });

    //entries
    Route::group(['prefix' => 'entries'], function () {
        Route::get('create', 'EntryController@create')->name('entries_create');
        Route::get('{entryId}/edit', 'EntryController@edit')->name('entries_edit');
        Route::get('{entryId}', 'EntryController@show')->name('entries_show');
    });

    //books
    Route::group(['prefix' => 'books'], function () {
        Route::get('/', 'BookController@index')->name('books_index');
        Route::get('/export', 'BookController@export');
        Route::get('create', 'BookController@create')->name('books_create');
        Route::get('{bookId}', 'BookController@show')->name('books_show');
        Route::get('{bookId}/edit', 'BookController@edit')->name('books_edit');
        Route::get('{bookId}/transactions', 'BookController@transactions')->name('books_transactions');
    });

    //categories
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', 'CategoryController@index')->name('category_index');
        Route::get('/export', 'CategoryController@export');
        Route::get('create', 'CategoryController@create')->name('category_create');
        Route::get('{categoryId}', 'CategoryController@show')->name('category_show');
        Route::get('{categoryId}/edit', 'CategoryController@edit')->name('category_edit');
    });

    //reports
    Route::group(['prefix' => 'reports'], function () {
        Route::get('/', 'ReportController@index')->name('report_index');
        Route::get('create', 'ReportController@create')->name('report_create');
        Route::get('{reportId}/rendered/{renderedReportId}', 'ReportController@showRenderedReport')->name('rendered-report_show');
        Route::get('{reportId}/edit', 'ReportController@edit')->name('report_edit');
        Route::get('{reportId}', 'ReportController@show')->name('report_show');
    });

    //organizations
    Route::group(['prefix' => 'organizations'], function () {
        Route::get('/', 'OrganizationController@index')->name('organization_index');
        Route::get('/export', 'OrganizationController@export');
        Route::get('create', 'OrganizationController@create')->name('organization_create');
        Route::get('{organizationId}', 'OrganizationController@show')->name('organization_show');
        Route::get('{organizationId}/edit', 'OrganizationController@edit')->name('organization_edit');
    });

    //currency
    Route::group(['prefix' => 'currencies'], function () {
        Route::get('/', 'CurrencyController@index')->name('currency_index');
        Route::get('/export', 'CurrencyController@export');
        Route::get('create', 'CurrencyController@create')->name('currency_create');
        Route::get('{currencyId}', 'CurrencyController@show')->name('currency_show');
        Route::get('{currencyId}/edit', 'CurrencyController@edit')->name('currency_edit');
    });

    Route::group(['prefix' => 'config'], function () {
        Route::get('/', 'ConfigController@index')->name('config_index');
    });
});
