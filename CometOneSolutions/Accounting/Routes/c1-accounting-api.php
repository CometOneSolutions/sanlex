<?php


$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'accounting'], function () use ($api) {
        $api->group(['prefix' => 'currencies'], function () use ($api) {
            $api->get('/', 'CometOneSolutions\Accounting\CurrencyApiController@index');
            $api->get('{currencyId}', 'CometOneSolutions\Accounting\CurrencyApiController@show');
            $api->post('/', 'CometOneSolutions\Accounting\CurrencyApiController@store');
            $api->patch('{currencyId}', 'CometOneSolutions\Accounting\CurrencyApiController@update');
            $api->delete('{currencyId}', 'CometOneSolutions\Accounting\CurrencyApiController@destroy');
        });
        $api->group(['prefix' => 'books'], function () use ($api) {
            $api->get('/', 'CometOneSolutions\Accounting\BookApiController@index');
            $api->get('{bookId}/transactions', 'CometOneSolutions\Accounting\BookApiController@getTransactions');
            $api->get('{bookId}', 'CometOneSolutions\Accounting\BookApiController@show');
            $api->post('/', 'CometOneSolutions\Accounting\BookApiController@store');
            $api->patch('{bookId}', 'CometOneSolutions\Accounting\BookApiController@update');
            $api->delete('{bookId}', 'CometOneSolutions\Accounting\BookApiController@destroy');
        });
        $api->group(['prefix' => 'organizations'], function () use ($api) {
            $api->get('/', 'CometOneSolutions\Accounting\OrganizationApiController@index');
            $api->get('{organizationId}', 'CometOneSolutions\Accounting\OrganizationApiController@show');
            $api->post('/', 'CometOneSolutions\Accounting\OrganizationApiController@store');
            $api->patch('{organizationId}', 'CometOneSolutions\Accounting\OrganizationApiController@update');
            $api->delete('{organizationId}', 'CometOneSolutions\Accounting\OrganizationApiController@destroy');
        });
        $api->group(['prefix' => 'entries'], function () use ($api) {
            $api->get('/', 'CometOneSolutions\Accounting\EntryApiController@index');
            $api->get('{entryId}', 'CometOneSolutions\Accounting\EntryApiController@show');
            $api->post('/', 'CometOneSolutions\Accounting\EntryApiController@store');
            $api->patch('{entryId}', 'CometOneSolutions\Accounting\EntryApiController@update');
            $api->delete('{entryId}', 'CometOneSolutions\Accounting\EntryApiController@destroy');
        });
        $api->group(['prefix' => 'accounts'], function () use ($api) {
            $api->get('/', 'CometOneSolutions\Accounting\AccountApiController@index');
            $api->get('list', 'CometOneSolutions\Accounting\AccountListApiController@index');
            $api->get('{accountId}/transactions', 'CometOneSolutions\Accounting\AccountApiController@getTransactions');
            $api->get('{accountId}', 'CometOneSolutions\Accounting\AccountApiController@show');
            $api->post('/', 'CometOneSolutions\Accounting\AccountApiController@store');
            $api->patch('{accountId}', 'CometOneSolutions\Accounting\AccountApiController@update');
            $api->delete('{accountId}', 'CometOneSolutions\Accounting\AccountApiController@destroy');
        });

        $api->group(['prefix' => 'categories'], function () use ($api) {
            $api->get('/', 'CometOneSolutions\Accounting\CategoryApiController@index');
            $api->get('{accountId}', 'CometOneSolutions\Accounting\CategoryApiController@show');
            $api->post('/', 'CometOneSolutions\Accounting\CategoryApiController@store');
            $api->patch('{accountId}', 'CometOneSolutions\Accounting\CategoryApiController@update');
            $api->delete('{accountId}', 'CometOneSolutions\Accounting\CategoryApiController@destroy');
        });

        $api->group(['prefix' => 'reports'], function () use ($api) {
            $api->get('/', 'CometOneSolutions\Accounting\ReportApiController@index');
            $api->get('{reportId}', 'CometOneSolutions\Accounting\ReportApiController@show');
            $api->post('/', 'CometOneSolutions\Accounting\ReportApiController@store');
            $api->post('{reportId}/render', 'CometOneSolutions\Accounting\RenderedReportApiController@generate');
            $api->patch('{reportId}', 'CometOneSolutions\Accounting\ReportApiController@update');
            $api->delete('{reportId}', 'CometOneSolutions\Accounting\ReportApiController@destroy');
        });

        $api->group(['prefix' => 'rendered-reports'], function () use ($api) {
            $api->get('{renderedReportId}', 'CometOneSolutions\Accounting\RenderedReportApiController@show');
            $api->delete('{renderedReportId}', 'CometOneSolutions\Accounting\RenderedReportApiController@destroy');
        });

        $api->group(['prefix' => 'configs'], function () use ($api) {
            $api->get('/', 'CometOneSolutions\Accounting\ConfigApiController@show');
            $api->patch('/', 'CometOneSolutions\Accounting\ConfigApiController@update');
        });
    });
});
