<?php

namespace CometOneSolutions\Accounting;

interface Accountable
{
    public function getId();

    public function getAccountingUri();

    public function getAccountingRefNo();
}
