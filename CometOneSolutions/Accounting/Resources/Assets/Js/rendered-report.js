import { Form } from '@c1_common_js/components/Form';
import { accountingService } from './accounting-main';
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import CategoryLineTree from './components/CategoryLineTree'

new Vue({
    el: '#rendered-report',
    components: {
         DeleteButton, Timestamp, VForm , accountingService, Timestamp, CategoryLineTree
    },

    data: {
        form: new Form({
            id: '',
            dateFrom: '',
            dateTo: '',
        }),
        dataInitialized: true,        
    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized;
        },
    },
    methods: {
        
        destroy() {
           this.form.confirm().then((result) => {
                if(result.value) {
                    this.form.delete('/api/accounting/rendered-reports/' + this.form.id)
                    .then(response => {
                        this.$swal({
                            title: 'Success',
                            text: 'Report was removed.',
                            type: 'success'
                        }).then(() => window.location = '/accounting/reports');
                    });
                }    
            });             
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {
        if(id!=null){
            this.dataInitialized = false;
            this.form.get('/api/accounting/rendered-reports/' + id + '?include=report,categoryLines')
            .then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
            });
        }
    },
   
    mounted() {
        console.log("Init account script...");
    }
});