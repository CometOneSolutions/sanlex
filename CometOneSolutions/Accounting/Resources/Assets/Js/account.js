import { Form } from '@c1_common_js/components/Form';
import { accountingService } from './accounting-main';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select2 from '@c1_common_js/components/Select2'

new Vue({
    el: '#account',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm , Timestamp, Select2, accountingService
    },
    data: {
        form: new Form({
            id:null,
            title: null,
            bookId: null,
            normalBalance: null,
        }),
        bookSelections: [],
        booksInitialized: false,
        dataInitialized: true,
    },
    
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized && this.booksInitialized;
        },
    },

    methods: {
        store() {
            this.form.post('/api/accounting/accounts')
                .then(response => {
                    this.$swal({
                        title: 'Account created!',
                        text: this.form.title + ' is Created',
                        type: 'success'
                    })
                    this.form.reset();
                });
        },

        update(){
            this.form.patch('/api/accounting/accounts/'+id)
            .then(response => {
                this.$swal({
                    title: 'Account Updated!',
                    text: 'Title is updated to ' + this.form.title,
                    type: 'success'
                }).then(() => window.location = '/accounting/accounts/' + id
            );});
        },

        destroy(){

           this.form.confirm().then((result) => {
            if(result.value) {
                this.form.delete('/api/accounting/accounts/' + this.form.id)
                .then(response => {
                    this.$swal({
                        title: 'Success',
                        text: 'Account was removed.',
                        type: 'success'
                    }).then(() => window.location = '/accounting/accounts');
                });
            }
            
        });             
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {

        if(id!=null){
            this.dataInitialized = false;
            this.form.get('/api/accounting/accounts/' + id + '?include=book')
            .then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
            });
        }
        accountingService.getBooks().then(response => {
            this.bookSelections = response.data;
            this.booksInitialized = true;
        });
    },
   
    mounted() {     
        console.log("Init account script...");
    }
});