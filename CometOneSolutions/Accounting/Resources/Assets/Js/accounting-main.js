import {Form} from '@c1_common_js/components/Form';

export const accountingService = new Vue({
       
    data: function() {
        return {
            form: new Form({}),
            uri: {
                accounts: '/api/accounting/accounts?limit=' + Number.MAX_SAFE_INTEGER,
                books: '/api/accounting/books?limit=' + Number.MAX_SAFE_INTEGER,
                currency: '/api/accounting/currencies?limit=' + Number.MAX_SAFE_INTEGER,
                organization: '/api/accounting/organizations?limit=' + Number.MAX_SAFE_INTEGER,
                entries: '/api/accounting/entries?limit=' + Number.MAX_SAFE_INTEGER,
                categories: '/api/accounting/categories?limit=' + Number.MAX_SAFE_INTEGER,
                reports: '/api/accounting/reports?limit=' + Number.MAX_SAFE_INTEGER,
                rendered: '/api/accounting/rendered-reports?limit=' + Number.MAX_SAFE_INTEGER,
            }      
        }
    },   

    methods: {

        getAccounts(){
            return this.form.get(this.uri.accounts);
        },

        getAccountsByBookId(id){
            let url = this.uri.accounts + '&book_id=' + id;
            return this.form.get(url);
        },

        getBooks(){
            return this.form.get(this.uri.books);
        },

        getCurrencies(){
            return this.form.get(this.uri.currency);
        },

        getOrganizations(){
            return this.form.get(this.uri.organization).then(response =>
                {return response.data; })
                .catch(error => this.errorMessage(error));
        },

        getCategories(){
            return this.form.get(this.uri.categories);
        },

        getCategoriesByBookId(id){
            let url = this.uri.categories + '&book_id=' +id;
            return this.form.get(url);
        },

    }
});

