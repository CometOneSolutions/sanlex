import { Form } from '@c1_common_js/components/Form';
import { accountingService } from './accounting-main';
import Transaction from './components/Transaction'
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select2 from '@c1_common_js/components/Select2'

new Vue({
    el: '#entry',
    components: {
        SaveButton, DeleteButton, Timestamp, Transaction, accountingService, Select2, VForm
    },
    data: {
        form: new Form({
            id: "null",
            date: '',
            bookId: null,
            notes: null,
            transactions: [],
        }),

        newTransactionAmount: 0.00,
        newTransactionAccountId: null,
       
        bookSelections:[],
        booksInitialized: false,
        accountSelections: [],
        accountInitialized : false,
        dataInitialized: true,    
        isEdit: false,
        isCreate: true,
    },
 
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized && this.booksInitialized;
        },
    },
    methods: {
        
        addDebitTransaction() {
            let title = this.getSelectedTitle(this.newTransactionAccountId);
            this.addTransaction('Debit', title);
        },

        addCreditTransaction()
        {  
            let title = this.getSelectedTitle(this.newTransactionAccountId);
            this.addTransaction('Credit', title);
        },

        addTransaction(type, title) {
            this.form.transactions.push({
                accountId: this.newTransactionAccountId,
                title: title,
                type: type,
                amount: this.newTransactionAmount,
            });
        },

        getSelectedTitle(id){
            let selected = this.accountSelections.find(function(record){
                return record.id === id;
            })
            return selected.title;
        },

        fillAccounts(id){
                this.accountInitialized = false;
                this.accountSelections = [];
                accountingService.getAccountsByBookId(id).then(response => {
                this.accountSelections = response.data;
                this.accountInitialized = true;
               });
        },

        destroy() {
            this.form.confirm().then((result) => {
                if(result.value) {
                    this.form.delete('/api/accounting/entries/' + id)
                    .then(response => {
                        this.$swal({
                            title: 'Success',
                            text: 'Entry was removed.',
                            type: 'success'
                        }).then(() => window.location = '/');
                    });
                }});   
        },
       
        store() {
            this.form.post('/api/accounting/entries')
                .then(response => {
                    this.$swal({
                        title: 'Entry created!',
                        text: 'Entry is Created',
                        type: 'success'
                    })
                    this.form.reset();
                });
        },

        update() {
            this.form.patch('/api/accounting/entries/' + id)
            .then(response => {
                this.$swal({
                    title: 'Entry Updated!',
                    text:  'Entry is Updated!',
                    type: 'success'
                }).then(() => window.location = '/accounting/entries/' + id);
            });
        },
        
        loadData(data) {
            data.transactions = data.transactions.data;
            let accounts = data.transactions;
            accounts.forEach(element => {
                element.title = element.account.data.title;
            });
            this.form = new Form(data);
        },
    },
    
    created() {
        if(id!=null){
            this.dataInitialized = false;
            this.form.get('/api/accounting/entries/' + id + '?include=transactions,book')
            .then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
                this.fillAccounts(this.form.bookId);
            });
            this.isEdit = true;
        }

        accountingService.getBooks().then(response => {
            this.bookSelections = response.data;
            this.booksInitialized = true;
        });
        
    },
    
    mounted() {
        console.log("Init entry script...");
    }
});