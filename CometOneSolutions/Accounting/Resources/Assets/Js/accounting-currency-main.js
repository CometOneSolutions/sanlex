import {Form} from '@c1_common_js/components/Form';

export const accountingCurrencyService = new Vue({
       
    data: function() {
        return {
            form: new Form({}),
            uri: {
                accountingCurrencies: '/api/accounting/currencies?limit=' + Number.MAX_SAFE_INTEGER,
            }      
        }
    },   

    methods: {

        getAccountingCurrencies(){
            
            return this.form.get(this.uri.accountingCurrencies);
        },
    }
});

