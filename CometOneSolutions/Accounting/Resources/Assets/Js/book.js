import { Form } from '@c1_common_js/components/Form';
import { accountingService } from './accounting-main';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select2 from '@c1_common_js/components/Select2'

new Vue({
    el: '#books',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Timestamp, Select2, accountingService
    },
    data: {
        form: new Form({
            organizationId: null,
            currencyId: null,
            name: null,
        }),
        organizationSelections: [],
        organizationInitialized: false,
        currencySelections: [],
        currencyInitialized: false,
        transactions: [],
        transactionInitialized: true,
        dataInitialized: true,
    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized && this.organizationInitialized && this.currencyInitialized;
        },
    },
    methods: {
        store() {
            this.form.post('/api/accounting/books')
                .then(response => {
                    this.$swal({
                        title: 'Book created!',
                        text: this.form.name + ' is Created',
                        type: 'success'
                    })
                    this.form.reset();
                });
        },

        update(){
            this.form.patch('/api/accounting/books/'+id)
            .then(response => {
                this.$swal({
                    title: 'Book Updated!',
                    text: this.form.name + ' is Updated!',
                    type: 'success'
                }).then(() => window.location = '/accounting/books/' + id
            );});
        },

        destroy(){
            this.form.confirm().then((result) => {
             if(result.value) {
                 this.form.delete('/api/accounting/books/' + this.form.id)
                 .then(response => {
                     this.$swal({
                         title: 'Success',
                         text: 'Book was removed.',
                         type: 'success'
                     }).then(() => window.location = '/accounting/books');
                 });
             }});             
         },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {

        if(id!=null){
            this.dataInitialized = false;
            this.transactionInitialized = false;

            //getBooksById
            this.form.get('/api/accounting/books/' + id + '?include=organization,currency,accounts')
            .then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
            });

            //getTransactionsByBookId
            this.form.get('/api/accounting/books/'+ id +  '/transactions')
            .then(response => {
                this.transactions = response.data;
                this.transactionInitialized = true;
            });

        }

        accountingService.getCurrencies().then(response => {
            this.currencySelections = response.data;
            this.currencyInitialized = true;
        });

        accountingService.getOrganizations().then(response =>{
            this.organizationSelections = response.data;
            this.organizationInitialized = true;
        });

    },
   
    mounted() {
        console.log("Init book script...");
    }
});