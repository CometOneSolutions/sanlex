import { Form } from '@c1_common_js/components/Form';
import { accountingService } from './accounting-main';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'

new Vue({
    el: '#currency',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm , accountingService, Timestamp
    },
    data: {
        form: new Form({
            code: null,
            description: null,
        }),
        dataInitialized: true,
    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized;
        },
    },
    methods: {
        store() {
            this.form.post('/api/accounting/currencies')
                .then(response => {
                    this.$swal({
                        title: 'Currency created!',
                        text: this.form.code + ' is Created',
                        type: 'success'
                    })
                    this.form.reset();
                });
        },

        update(){
            this.form.patch('/api/accounting/currencies/'+ id)
            .then(response => {
                this.$swal({
                    title: 'Currency Updated!',
                    text: 'Currency is Updated',
                    type: 'success'
                }).then(() => window.location = '/accounting/currencies/' + id
            );});
        },

        destroy(){
           this.form.confirm().then((result) => {
            if(result.value) {
                this.form.delete('/api/accounting/currencies/' + this.form.id)
                .then(response => {
                    this.$swal({
                        title: 'Success',
                        text: 'Currency was removed.',
                        type: 'success'
                    }).then(() => window.location = '/accounting/currencies');
                });
            }
        });             
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {

        if(id!=null){
            this.dataInitialized = false;
            let url = '/api/accounting/currencies/'+id;
            this.form.get(url).then(response => 
                { this.loadData(response.data); })
                .catch(error => this.errorMessage(error));
            this.dataInitialized = true;
        }
    },
   
    mounted() {
        
        console.log("Init organization script...");
    }
});