import { Form } from '@c1_common_js/components/Form';
import { accountingService } from './accounting-main';
import AttachedAccounts from './components/AttachedAccounts'
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select2 from '@c1_common_js/components/Select2'

new Vue({
    el: '#category',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm , accountingService, Timestamp, Select2, AttachedAccounts
    },

    data: {
        form: new Form({
            name: null,
            normalBalance: null,
            bookId: null,
            parentCategoryId: null,
            accounts : [],
        }),
        selectedAccountId: null,
        selectedAccountTitle: null,
        dataInitialized: true,
        bookSelections: [],
        booksInitialized: false,
        categoriesSelections: [],
        categoriesInitialized: false,
        accountSelections: [],
        accountInitialized: false,

    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized && this.booksInitialized && this.categoriesInitialized;
        },
    },
    methods: {

        fillAccounts(id){
            this.accountInitialized = false;
            this.accountSelections = [];
            accountingService.getAccountsByBookId(id).then(response => {
            this.accountSelections = response.data;
            this.accountInitialized = true;
           });
        },

        syncData(id){
            let selected = this.accountSelections.find(function(record){
                return record.id === id;
            })
            this.selectedAccountTitle = selected.title;
        },

        addAccount(){
            this.form.accounts.push({
                accountId: this.selectedAccountId,
                title: this.selectedAccountTitle,
            });
        },
        store() {
            this.form.post('/api/accounting/categories')
                .then(response => {
                    this.$swal({
                        title: 'Category created!',
                        text: this.form.name + ' is Created',
                        type: 'success'
                    })
                    this.accountInitialized = false;
                    this.form.reset();
                });
        },

        update(){
            this.form.patch('/api/accounting/categories/'+id)
            .then(response => {
                this.$swal({
                    title: 'Category Updated!',
                    text: 'Category is Updated',
                    type: 'success'
                }).then(() => window.location = '/accounting/categories/' + id
            );});
        },

        destroy(){
           this.form.confirm().then((result) => {
            if(result.value) {
                this.form.delete('/api/accounting/categories/' + this.form.id)
                .then(response => {
                    this.$swal({
                        title: 'Success',
                        text: 'Category was removed.',
                        type: 'success'
                    }).then(() => window.location = '/accounting/categories');
                })
            }
        });             
        },

        loadData(data) {
            data.accounts = data.accounts.data;
            data.book = data.book.data;
            data.childCategories = data.childCategories.data;
            this.form = new Form(data);
            this.fillAccounts(this.form.bookId);
        },
    },

    created() {

        if(id!=null){
            this.dataInitialized = false;
            this.form.get('/api/accounting/categories/' + id + '?include=accounts,book,childCategories')
            .then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
            });
        }

        accountingService.getBooks().then(response => {
            this.bookSelections = response.data;
            this.booksInitialized = true;
        });

        accountingService.getCategories(id).then(response => {
            this.categoriesSelections = response.data;
            this.categoriesInitialized = true;
        })
    },
   
    mounted() {
        
        console.log("Init account script...");
    }
});