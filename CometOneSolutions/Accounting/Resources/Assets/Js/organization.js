import { Form } from '@c1_common_js/components/Form';
import { accountingService } from './accounting-main';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'

new Vue({
    el: '#organization',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm , accountingService, Timestamp
    },
    data: {
        form: new Form({
            name: null,
        }),
        dataInitialized: true,
    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized;
        },
    },
    methods: {
        store() {
            this.form.post('/api/accounting/organizations')
                .then(response => {
                    this.$swal({
                        title: 'Organization created!',
                        text: this.form.name + ' is Created',
                        type: 'success'
                    })
                    this.form.reset();
                });
        },

        update(){
            this.form.patch('/api/accounting/organizations/'+ id)
            .then(response => {
                this.$swal({
                    title: 'Organization Updated!',
                    text: 'Title is updated to ' + this.form.name,
                    type: 'success'
                }).then(() => window.location = '/accounting/organizations/' + id
            );});
        },

        destroy(){

           this.form.confirm().then((result) => {
            if(result.value) {
                this.form.delete('/api/accounting/organizations/' + this.form.id)
                .then(response => {
                    this.$swal({
                        title: 'Success',
                        text: 'Organization was removed.',
                        type: 'success'
                    }).then(() => window.location = '/accounting/organizations');
                });
            }
        });             
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {

        if(id!=null){
            this.dataInitialized = false;
            let url = '/api/accounting/organizations/'+id;
            this.form.get(url).then(response => 
                { this.loadData(response.data); })
                .catch(error => this.errorMessage(error));
            this.dataInitialized = true;
        }
    },
   
    mounted() {
        
        console.log("Init organization script...");
    }
});