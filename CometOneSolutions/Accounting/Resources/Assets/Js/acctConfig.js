import {Form} from '@c1_common_js/components/Form';
import { accountingService } from './accounting-main';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select2 from '@c1_common_js/components/Select2'

new Vue({
    el: '#config',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, accountingService, Select2
    },
    data: {
    
        form: new Form({

        }),
        
        dataInitialized: false,
        bookSelections: [],
        bookInitialized: false,
        organizationSelections: [],
        organizationInitialized: false,

    },
    watch: {
        initializationComplete(val) {      
            this.form.isInitializing = !val;
        }
    },
    computed: {

        initializationComplete() {
            return this.dataInitialized && this.bookInitialized && this.organizationInitialized;
        }
    },
    methods: {

        update() {
            this.form.patch('/api/accounting/configs')
            .then(response => {
                this.$swal({
                    title: 'Success',
                    text: 'Config defaults was updated.',
                    type: 'success',
                    confirmButtonColor: '#20a8d8',
                }).then(() => 
                window.location = '/accounting/config'
            );
                
            })
            .catch(error => {
                this.errorMessage(error);
            });
        },

        loadData(data) {
            this.form = new Form(data);
            let id = data.id;
        },
    },
    created() {
        accountingService.getBooks().then(response => {
            this.bookSelections = response.data;
            this.bookInitialized = true;
        });

        accountingService.getOrganizations().then(response => {
            this.organizationSelections = response.data;
            this.organizationInitialized = true;
        });

        this.form.get('/api/accounting/configs')
        .then(response => {
            this.loadData(response.data);
            this.dataInitialized = true;
        });
    },
    mounted() { 
        console.log("Init config script");
    }
});