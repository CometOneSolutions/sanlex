import { Form } from '@c1_common_js/components/Form';
import { accountingService } from './accounting-main';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import AttachedCategories from './components/AttachedCategories'
import Select2 from '@c1_common_js/components/Select2'

new Vue({
    el: '#report',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm , accountingService, Timestamp, Select2, AttachedCategories
    },

    data: {
        form: new Form({
            description: null,
            bookId: null,
            categories: [],
            dateFrom:'',
            dateTo:''
        }),
        bookSelections: [],
        booksInitialized: false,
        selectedCategoryId: null,
        selectedName: null,
        selectedNormalBalance: null,
        categorySelections: [],
        categoryInitialized: false,
        dataInitialized: true,
        isEdit: false,
    
    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized;
        },
    },
    methods: {

        generate() {
            this.form.post(serviceBus.uri.reports + '/' + this.form.id + '/render')
                .then(response => {
                    this.$swal({
                        title: 'Success',
                        text: 'Report was created.',
                        type: 'success'
                    }).then(() =>
                    window.location = '/accounting/reports/' + this.form.id
                    );
                });
        },

        fillCategories(id){
            this.categoryInitialized = false;
            this.selectedCategoryId = null,
            this.selectedName = null,
            this.selectedNormalBalance = null,
            accountingService.getCategoriesByBookId(id).then(response => {
            this.categorySelections = response.data;
            this.categoryInitialized = true;
           });
        },

        syncData(id){
            let selected = this.categorySelections.find(function(record){
                return record.id === id;
            })
            this.selectedName = selected.name;
            this.selectedNormalBalance = selected.normalBalance;
        },

        addCategory(){
            this.form.categories.push({
                categoryId: this.selectedCategoryId,
                name: this.selectedName,
                normalBalance: this.selectedNormalBalance,
            });
        },
        store() {
            this.form.post('/api/accounting/reports')
                .then(response => {
                    this.$swal({
                        title: 'Report created!',
                        text: this.form.description + ' is Created',
                        type: 'success'
                    })
                    this.accountInitialized = false;
                    this.form.reset();
                });
        },

        update(){
            this.form.patch('/api/accounting/reports/'+id)
            .then(response => {
                this.$swal({
                    title: 'Report Updated!',
                    text: 'Report is Updated',
                    type: 'success'
                }).then(() => window.location = '/accounting/reports/' + id
            );});
        },

        destroy(){
           this.form.confirm().then((result) => {
            if(result.value) {
                this.form.delete('/api/accounting/reports/' + this.form.id)
                .then(response => {
                    this.$swal({
                        title: 'Success',
                        text: 'Report was removed.',
                        type: 'success'
                    }).then(() => window.location = '/accounting/reports');
                });
            }
        });             
        },

        loadData(data) {
            data.categories = data.categories.data;
            data.book = data.book.data;
            this.form = new Form(data);
            this.fillCategories(this.form.bookId);
            this.isEdit = true;
        },
    },

    created() {

        if(id!=null){
            this.dataInitialized = false;
            this.form.get('/api/accounting/reports/' + id + '?include=book,categories,renderedReports')
            .then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
            });
        }
        accountingService.getBooks().then(response => {
            this.bookSelections = response.data;
            this.booksInitialized = true;
        });

    },
   
    mounted() {
        
        console.log("Init account script...");
    }
});