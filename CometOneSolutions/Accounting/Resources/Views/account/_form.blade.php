<div class="card-body">
<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">
        <div class="col-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Book</label>
                <select2 class="form-control select2-single selecttwo" v-model.number="form.bookId" :disabled="form.id!=null">
                    <option :value="null">Please Select Account</option>
                    <option 
                        v-for="book in bookSelections" 
                        :value="book.id">
                        @{{ book.name }}
                    </option>
                </select2>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <label for="date">Title</label>
                <input type="text" class="form-control" v-model="form.title">
            </div>
        </div>
</div>
<div class="row">
    <div class="col-4">
        <div class="form-group">
            <label for="normalBalance">Normal Balance:</label><br>
            <label><input type="radio" name="normal" v-model="form.normalBalance" value="Credit" :disabled="form.id!=null"> Credit</label><br>
            <label><input type="radio" name="normal" v-model="form.normalBalance" value="Debit" :disabled="form.id!=null"> Debit</label>
        </div>
    </div>

</div>

</div>
@push('scripts')
<script src="{{ mix('js/account.js') }}"></script>
@endpush