@extends('app')
@section('breadcrumbs', Breadcrumbs::render('accounts.show', $account))
@section('content')
<section id="account">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Details
                        <div class="float-right">
                            <a :href="form.editUri"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> </button></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                <label for="name">Title</label>
                                <p><b>@{{ form.title }}</b></p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                <label for="name">Book</label>
                                <p><b>@{{ form.book.data.name }}</b></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                <label for="name">Normal Balance</label>
                                <p><b>@{{ form.normalBalance }}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script src="{{ mix('js/account.js') }}"></script>
@endpush