@extends('app')
@section('breadcrumbs', Breadcrumbs::render('accounts.transactions', $account))
@section('content')   
    <div id="index" v-cloak>
        <div class="card">
            <div class="card-header">{{ $account->getTitle() }}</div>
            <div class="card-body">
                <div class="row"> 
                    <div class="col-6"> 
                        <div class="form-group"> 
                        <p><b><label for="name">Transactions</label></b></p>
                        </div> 
                    </div> 
                </div>                 
                <index 
                    :filterable="filterable"
                    :base-url="baseUrl"
                    :sorter="sorter"
                    :to-last-page="true"
                    :sort-ascending="sortAscending"
                    v-on:update-totals="(val) => totals = val"
                    v-on:update-items="(val) => items = val"
                    v-on:update-loading="(val) => isLoading = val">

                    <div v-show="false">
                        @{{ runningBalance = totals.startingBalance ? totals.startingBalance.amount : 0 }}
                        @{{ accountType = totals.startingBalance ? totals.startingBalance.type : '' }}
                    </div>
                    
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Notes</th>
                            <th class="text-right">DR</th>
                            <th class="text-right">CR</th>
                            <th class="text-right">Balance</th>                            
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                        <td>@{{ item.entry.data.date }}</td>
                            <td>@{{ item.entry.data.notes }}</a></td>
                            <td class="text-right"><label v-if="item.type=='Debit'">@{{ item.amount }}</label></td>
                            <td class="text-right"><label v-if="item.type=='Credit'">@{{ item.amount }}</label></td>
                            <td class="text-right">@{{ runningBalance += item.amount * (accountType == item.type ? 1 : -1) }}</td>
                        </tr>
                        </tbody>
                    </table> 
                </index>
            </div>
        </div>
    </div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush