<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Normal Balance</th>
            <th>Book</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $account)
        <tr>
            <td>{{ $account->getId() }}</td>
            <td>{{ $account->getTitle() }}</td>
            <td>{{ $account->getNormalBalance() }}</td>
            <td>{{ $account->getBook()->getName() }}</td>
        </tr>
    @endforeach
    </tbody>
</table>