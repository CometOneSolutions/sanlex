@extends('app')
@section('breadcrumbs', Breadcrumbs::render('accounts.index'))
@section('content')   
    <div id="index" v-cloak>
        <div class="card">
            <div class="card-header">
                Account
                <div class="float-right">
                    <a href="{{route('accounts_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
                </div>
            </div>
            <div class="card-body">
                <index 
                :filterable="filterable"
                :base-url="baseUrl"
                
                :sorter="sorter"
                :sort-ascending="sortAscending"
            
                v-on:update-loading="(val) => isLoading = val"
                v-on:update-items="(val) => items = val">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('name')">
                                    ID <i class="fa" :class="getSortIcon('name')"></i>
                                </a>
                            </th>
                            <th>
                                    Title
                            </th>
                            <th>
                                    Book
                            </th>
                            <th>
                                Normal Balance
                            </th>
                            <th>
                                Link
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td>@{{ item.id }}</td>
                            <td>
                            <a :href="item.showUri">@{{ item.title }}</a> 
                            </td>
                            <td>@{{ item.book.data.name}}</td>
                            <td>
                                @{{item.normalBalance}}
                            </td>
                            <td>
                            <a :href="item.transactionUri">Transactions</a> 
                            </td>
                        </tr>
                        </tbody>
                    </table> 
                </index>
            </div>
        </div>
    </div>
    <!-- End Watchlist-->
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush