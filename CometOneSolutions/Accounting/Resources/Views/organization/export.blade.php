<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $organization)
        <tr>
            <td>{{ $organization->getId() }}</td>
            <td>{{ $organization->getName() }}</td>
        </tr>
    @endforeach
    </tbody>
</table>