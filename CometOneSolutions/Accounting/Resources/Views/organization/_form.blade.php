<div class="card-body">
<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">
        <div class="col-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Name *</label>
                <input type="text" class="form-control" v-model="form.name">
            </div>
        </div>
</div>
</div>
@push('scripts')
<script src="{{ mix('js/organization.js') }}"></script>
@endpush