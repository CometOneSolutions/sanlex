
<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
    <div class="row">
        <div class="col-8">
            <div class="form-group">
                <label for="exampleInputEmail1">Book</label>
                <select2 class="form-control select2-single selecttwo" :disabled="isEdit&&isCreate" v-on:change="fillAccounts(form.bookId)" v-model.number="form.bookId">
                    <option :value="null">Please Select Account</option>
                    <option 
                        v-for="book in bookSelections" 
                        :value="book.id">
                        @{{ book.name }}
                    </option>
                </select2>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="date">Date</label>
                <input type="text" class="form-control dateformat" v-model="form.date" id="date" name="date" aria-describedby="dateHelp" pattern="^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$" data-inputmask ="'alias' : 'date'" >
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-8">
            <div class="form-group">
                <label for="exampleInputEmail1">Notes</label>
                <input type="text" class="form-control" v-model="form.notes" @focus="$event.target.select()">    
            </div>
        </div>
    </div>    

    <div class="row">
        <div class="col-4">
            <div class="form-group">
                <label for="exampleInputEmail1">Account</label>
                <select2 :disabled="!accountInitialized" class="form-control select2-single selecttwo" v-model.number="newTransactionAccountId">
                    <option :value="null">Please Select Account</option>
                    <option 
                        v-for="account in accountSelections" 
                        :value="account.id">
                        @{{ account.title }}
                    </option>
                </select2>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="exampleInputEmail1">Amount</label>
                <input type="text" class="form-control" v-model.number="newTransactionAmount" @focus="$event.target.select()">
            </div>
        </div>
        <div class="col-2">
            <div class="form-group">
                <label for="exampleInputEmail1">&nbsp;</label>
                <input type="button" class="btn btn-success form-control" @click="addDebitTransaction" value="DR">            
            </div>
        </div>
        <div class="col-2">
            <div class="form-group">
                <label for="exampleInputEmail1">&nbsp;</label>
                <input type="button" class="btn btn-success form-control" @click="addCreditTransaction" value="CR">            
            </div>
        </div>
        
    </div>
    <br>
    <table class="table">
        <tr class="row">
            <th class="col-5">Account</th>
            <th class="col-3 text-right">DR</th>
            <th class="col-3 text-right">CR</th>
            <th class="text-right col-1">Action</th>
        </tr>
        
        <tr is="transaction"
            v-for="(detail,index) in form.transactions"
            :detail="detail"
            :is-edit="isEdit"
            :is-create="isCreate"
            v-on:remove="form.transactions.splice(index, 1)">
        </tr>
        <tr>
            <td colspan="6">
            <!-- <a href="#" role="button" class="btn btn-success" data-toggle="tooltip" data-placement="left" title="Add New?" @click="add">Add New</a> -->
            </td>
        </tr>
    </table>

@push('scripts')
<script src="{{ mix('js/entry.js') }}"></script>
@endpush