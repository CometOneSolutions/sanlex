@extends('app')
@section('breadcrumbs', Breadcrumbs::render('entries.edit', $entry))
@section('content')
<section id="entry">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
            <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
            </div>
                <div v-else>
                    <div class="card-header">
                        <b>Accounting Entry Edit</b>
                    </div>
                    <div class="card-body">
                            <div class="text-right">
                                <delete-button :is-busy="form.isBusy" :is-deleting="form.isDeleting" @destroy="destroy"></delete-button>
                            </div>
                    <form>
                    @include('Entry._form')                        
                    </form>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <button class="btn btn-success" @click="update"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br/>
@endsection