@extends('app')
@section('breadcrumbs', Breadcrumbs::render('entries.show', $entry))
@section('content')
<section id="entry">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Details
                        <div class="float-right">
                            <a :href="form.editUri"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> </button></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                <p><b><label for="name">Book</label></b></p>
                                @{{ form.book.data.name }}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                <p><b> <label for="name">Date</label></b></p>
                               @{{ form.date }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                <p><b><label for="name">Notes</label></b></p>
                                @{{ form.notes }}
                                </div>
                            </div>
                        </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Account</th>
                            <th>DR</th>
                            <th>CR</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="item in form.transactions">
                            <td>
                            <label v-if="item.type=='Credit'" style="text-indent:100px;">@{{ item.title }}</label>
                            <label v-else>@{{ item.title }}</label>
                            </td>
                            <td><label v-if="item.type=='Debit'">@{{ item.amount }}</label></td>
                            <td><label v-if="item.type=='Credit'">@{{ item.amount }}</label></td>
                        </tr>
                        </tbody>
                    </table> 

                    </div>
                </div>  
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script src="{{ mix('js/entry.js') }}"></script>
@endpush