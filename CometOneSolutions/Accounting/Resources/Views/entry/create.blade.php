@extends('app')
@section('breadcrumbs', Breadcrumbs::render('entries.create'))
@section('content')

<section id="entry">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
            <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
            </div>
                <div v-else>
                    <div class="card-header">
                        <b>Create Accounting Entry</b>
                    </div>
                    <div class="card-body">
                    <form>
                        @include('Entry._form')
                    </form>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <button class="btn btn-success" @click="store"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br/>
@endsection