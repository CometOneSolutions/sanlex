<div class="card-body">
<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
        <label for="date">Name *</label>
                <input type="text" class="form-control" v-model="form.name">
        </div>
    </div>
    <div class="col-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Organization</label>
                <select2 class="form-control select2-single selecttwo" v-model.number="form.organizationId">
                    <option :value="null">Please Select Organization</option>
                    <option 
                        v-for="org in organizationSelections" 
                        :value="org.id">
                        @{{ org.name }}
                    </option>
                </select2>
            </div>
    </div>
</div>
<div class="row">
        <div class="col-6">
            <div class="form-group">
            <label for="exampleInputEmail1">Currency</label>
            <select2 class="form-control select2-single selecttwo" v-model.number="form.currencyId">
                    <option :value="null">Please Select Currency</option>
                    <option 
                        v-for="currency in currencySelections" 
                        :value="currency.id">
                        @{{ currency.code }} - @{{ currency.description}}
                    </option>
                </select2>
            </div>
        </div>
</div>
</div>
@push('scripts')
<script src="{{ mix('js/book.js') }}"></script>
@endpush