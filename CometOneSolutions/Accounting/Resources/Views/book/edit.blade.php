@extends('app')
@section('breadcrumbs', Breadcrumbs::render('books.edit', $book))
@section('content')
<section id="books">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        <b>Create New Book</b>
                    </div>
                    <v-form
                        @validate="update">
                        <div class="card-body">
                            <div class="text-right">
                                <delete-button :is-busy="form.isBusy" :is-deleting="form.isDeleting" @destroy="destroy"></delete-button>
                            </div>
                            @include('Book._form')
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                            </div>
                        </div>
                    </v-form>
                </div>  
            </div>
        </div>
    </div>
</section>
<br/>
@endsection