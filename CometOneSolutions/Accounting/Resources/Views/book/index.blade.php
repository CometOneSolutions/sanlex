@extends('app')
@section('breadcrumbs', Breadcrumbs::render('books.index'))
@section('content')   
    <div id="index" v-cloak>
        <div class="card">
            <div class="card-header">
                Books
                <div class="float-right">
                    <a href="{{route('books_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
                </div>
            </div>
            <div class="card-body">
                <index 
                :filterable="filterable"
                :base-url="baseUrl"
                
                :sorter="sorter"
                :sort-ascending="sortAscending"
            
                v-on:update-loading="(val) => isLoading = val"
                v-on:update-items="(val) => items = val">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><a v-on:click="setSorter('name')">ID <i class="fa" :class="getSortIcon('name')"></i></a></th>
                            <th class="text-left">Name</th>
                            <th class="text-left">Organization</th>
                            <th>Links</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td>@{{ item.id }}</td>
                            <td class="text-left"><a :href="item.showUri">@{{ item.name }}</a></td>
                            <td class="text-left">@{{ item.organization.data.name || "--" }}</td>
                            <td><a :href="item.transactionUri">Transactions</td>
                        </tr>
                        </tbody>
                    </table> 
                </index>
            </div>
        </div>
    </div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush