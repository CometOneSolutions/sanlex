@extends('app')
@section('breadcrumbs', Breadcrumbs::render('books.transactions', $book))
@section('content')   
    <div id="index" v-cloak>
        <div class="card">
            <div class="card-header">{{ $book->getName() }}</div>
            <div class="card-body">
                <div class="row"> 
                    <div class="col-6"> 
                        <div class="form-group"> 
                        <p><b><label for="name">Name</label></b></p> 
                            {{ $book->getName() }}
                        </div> 
                    </div> 
                
                    <div class="col-6"> 
                        <div class="form-group"> 
                        <p><b><label for="name">Organization</label></b></p> 
                            {{ $book->getOrganization()->getName()}}
                        </div> 
                    </div> 
                </div> 

                <div class="row"> 
                    <div class="col-6"> 
                        <div class="form-group"> 
                        <p><b> <label for="name">Currency</label> </b></p> 
                             {{ $book->getCurrency()->getCode()}} - {{ $book->getCurrency()->getDescription() }} 
                        </div> 
                    </div> 
                </div> 
                
                <index :filterable="filterable"
                    :base-url="baseUrl"
                    :sorter="sorter"
                    :to-last-page="true"
                    :sort-ascending="sortAscending"
                    v-on:update-loading="(val) => isLoading = val"
                    v-on:update-items="(val) => items = val">

                    <table class="table">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th><a v-on:click="setSorter('name')">Account<i class="fa" :class="getSortIcon('name')"></i></a></th>
                            <th>DR</th>
                            <th>CR</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                        <td>@{{ item.entry.data.date }}</td>
                            <td>
                            <label v-if="item.type=='Credit'" style="text-indent:100px;">@{{ item.account.data.title }}</label>
                            <label v-else>@{{ item.account.data.title }}</label>
                            </td>
                            <td><label v-if="item.type=='Debit'">@{{ item.amount }}</label></td>
                            <td><label v-if="item.type=='Credit'">@{{ item.amount }}</label></td>
                            <td><a :href="item.editUri">Edit</a></td>
                        </tr>
                        </tbody>
                    </table> 
                </index>
            </div>
        </div>
    </div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush