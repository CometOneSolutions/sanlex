<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Organization</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $book)
        <tr>
            <td>{{ $book->getId() }}</td>
            <td>{{ $book->getName() }}</td>
            <td>{{ $book->getOrganization()->getName() }}</td>
        </tr>
    @endforeach
    </tbody>
</table>