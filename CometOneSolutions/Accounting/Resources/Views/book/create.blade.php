@extends('app')
@section('breadcrumbs', Breadcrumbs::render('books.create'))
@section('content')
<section id="books">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        <b>Create New Book</b>
                    </div>
                    <v-form
                        @validate="store">
                        <div class="card-body">
                            @include('Book._form')
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                            </div>
                        </div>
                    </v-form>
                </div>  
            </div>
        </div>
    </div>
</section>
<br/>
@endsection