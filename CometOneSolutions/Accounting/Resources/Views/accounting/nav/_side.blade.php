<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-calculator" aria-hidden="true"></i> Accounting</a>
    <ul class="nav-dropdown-items">
        @include('Account.nav._side')
        @include('Book.nav._side')
        @include('Category.nav._side')
        @include('Currency.nav._side')
        @include('Organization.nav._side')
        @include('Report.nav._side')
    </ul>
</li>