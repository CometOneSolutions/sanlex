@extends('app')
@section('breadcrumbs', Breadcrumbs::render('accounting-config.edit'))
@section('content')
<section id="config">
    <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
            <div class="col-12 text-center h4">
                <i class="fas fa-cog fa-spin"></i> Initializing
            </div>
    </div>
        <div v-else>        
        <br/>
        <v-form @validate="update">
            <div class="card table-responsive">
                <div class="card-header">
                    <h4>Default Config</h4>

                </div>
                    <div class="card-body" v-cloak>
                                <div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
                                    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
                                            @{{ error[0] }}
                                    </small>
                                </div>
                                <h4>Accounting Defaults</h4>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="inputCustomer" class="col-form-label">Current Organization: <span class="text text-danger">*</span></label>
                                        <select2 class="form-control select2-single selecttwo" v-model.number="form.currentOrganizationId">
                                            <option :value="null">Please Select Organization</option>
                                            <option 
                                                v-for="organization in organizationSelections" 
                                                :value="organization.id">
                                                @{{ organization.name }}
                                            </option>
                                        </select2>
                                    </div>
                                </div> 
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="inputCustomer" class="col-form-label">Current Book: <span class="text text-danger">*</span></label>
                                        <select2 class="form-control select2-single selecttwo" v-model.number="form.currentBookId">
                                            <option :value="null">Please Select Book</option>
                                            <option 
                                                v-for="book in bookSelections" 
                                                :value="book.id">
                                                @{{ book.name }}
                                            </option>
                                        </select2>
                                    </div>
                                </div>                  
                    </div>
                <div class="card-footer">
                    <div class="float-left">
                        <timestamp :name="form.updatedByUser.data.name" :time="form.updatedAt"></timestamp>
                    </div>
                    <div class="float-right">
                        <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                    </div>
                </div>
            </div>
        </v-form>
        </div>
    </section>
@push('scripts')
<script src="/js/acctConfig.js"></script>
@endpush
@endsection