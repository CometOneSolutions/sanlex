<div class="card-body">
<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">
        <div class="col-6">
            <div class="form-group">
                <label for="date">Description</label>
                <input type="text" class="form-control" v-model="form.description">
            </div>
        </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
        <label for="exampleInputEmail1">Book</label>
        <select2 @change="fillCategories(form.bookId)" :disabled="isEdit" class="form-control select2-single selecttwo" v-model.number="form.bookId">
            <option :value="null">Please Select Account</option>
            <option 
            v-for="book in bookSelections" 
            :value="book.id">
            @{{ book.name }}
            </option>
        </select2>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-11">
        <div class="form-group">
        <label for="exampleInputEmail1">Categories</label>
        <select2 :disabled="!categoryInitialized" @change="syncData(selectedCategoryId)" class="form-control select2-single selecttwo" v-model.number="selectedCategoryId">
            <option :value="null">Select Category</option>
            <option 
            v-for="category in categorySelections" 
            :value="category.id">
            @{{ category.name }}
            </option>
        </select2>
        </div>
    </div>
    <div class="col-1">
    <label for="exampleInputEmail1">Add</label><br>
    <button :disabled="!categoryInitialized" type="button" @click="addCategory" class="btn btn-success btn-sm"><i class="fas fa-plus"></i></button>
    </div>
</div>

    <br>
    <table class="table">
        <tr class="row">
            <th class="col-6">Category Name</th>
            <th class="col-5">Normal Balance</th>
            <th class="text-right col-1">Action</th>
        </tr>
        
        <tr is="attached-categories"
            v-for="(detail,index) in form.categories"
            :detail="detail"
            v-on:remove="form.categories.splice(index, 1)">
        </tr>
    </table>

</div>
@push('scripts')
<script src="{{ mix('js/report.js') }}"></script>
@endpush