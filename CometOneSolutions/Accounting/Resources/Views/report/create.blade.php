@extends('app')
@section('breadcrumbs', Breadcrumbs::render('report.create'))
@section('content')
<section id="report">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        <b>Create Report</b>
                    </div>
                    <v-form
                        @validate="store">
                        <div class="card-body">
                            @include('Report._form')
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                            </div>
                        </div>
                    </v-form>
                </div>  
            </div>
        </div>
    </div>
</section>
<br/>
@endsection