@extends('app')
@section('breadcrumbs', Breadcrumbs::render('report.show', $report))
@section('content')
<section id="report">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Details
                        <div class="float-right">
                            <button type="button" class="btn btn-primary btn-sm" @click="generate"><i class="fa fa-newspaper-o" aria-hidden="true"></i> </button>
                            <a :href="form.editUri"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> </button></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                <label for="name"><b>Description</b></label>
                                <p>@{{ form.description }}</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                <label for="name"><b>Book</b></label>
                                <p>@{{ form.book.name}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name"><b>Date From</b></label>
                                    <input type="date" v-model="form.dateFrom" class="form-control">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name"><b>Date To</b></label>
                                    <input type="date" v-model="form.dateTo" class="form-control">    
                                </div>
                            </div>
                        </div>

                            <table class="table">
                                <tr class="row">
                                    <th class="col-6">Category Name</th>
                                    <th class="col-6">Normal balance</th>
                                </tr>
                                <tr class="row" v-for="category in form.categories">
                                    <td class="col-6">@{{ category.name }}</td>
                                    <td class="col-6">@{{ category.normalBalance }}</td>
                                </tr>
                            </table><br>
                            <div class="clearfix"></div>
                            <table class="table">
                                <tr class="row">
                                    <th class="col-1">ID</th>
                                    <th class="col-2 text-center">Generated On</th>
                                    <th class="col-1 text-center">Date From</th>
                                    <th class="col-1 text-center">Date To</th>
                                    <th class="col-7">Notes</th>
                                </tr>
                                <tr class="row" v-for="rendered in form.renderedReports.data">
                                    <td class="col-1"><a :href="rendered.showUri">@{{rendered.id}}</a></td>
                                    <td class="col-2 text-center">@{{ rendered.createdAt.date }}</td>
                                    <td class="col-1 text-center">@{{ rendered.dateFrom }}</td>
                                    <td class="col-1 text-center">@{{ rendered.dateTo }}</td>
                                    <td class="col-7">@{{rendered.notes}}</td>
                                    
                                </tr>
                            </table>
                        
                    </div>
                </div>  
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script src="{{ mix('js/report.js') }}"></script>
@endpush