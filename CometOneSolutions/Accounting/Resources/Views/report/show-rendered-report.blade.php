@extends('app')
@section('breadcrumbs', Breadcrumbs::render('report.show', $report))
@section('content')
<section id="rendered-report">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Details
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <delete-button :is-busy="form.isBusy" :is-deleting="form.isDeleting" @destroy="destroy"></delete-button>
                                </div>
                                <label>Description: </label>
                                <span><strong>{{$report->getDescription()}}</strong></span>
                                <p>@{{form.notes}}</p>
                            </div>
                        </div>

                        <category-line-tree 
                            v-for="categoryLine in form.categoryLines.data" 
                            :category-line="categoryLine"
                            :children="categoryLine.childCategoryLines.data"
                            v-bind:key="categoryLine.id"
                            :depth="0">
                        </category-line-tree>
                    
                    </div>
                </div>  
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script src="{{ mix('js/rendered-report.js') }}"></script>
@endpush
