@extends('app')
@section('breadcrumbs', Breadcrumbs::render('category.show', $category))
@section('content')
<section id="category">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Details
                        <div class="float-right">
                            <a :href="form.editUri"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> </button></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                <label for="name"><b>Name</b></label>
                                <p>@{{ form.name }}</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                <label for="name"><b>Normal Balance</b></label>
                                <p>@{{ form.normalBalance}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                <label for="name"><b>Book</b></label>
                                <p>@{{ form.book.name }}</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                <label for="name"><b>Parent Category</b></label>
                                <p>@{{ form.parentCategory ? form.parentCategory.name : 'None'}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                <label for="name"><b>Child Categories:</b></label>
                                <p v-if="form.childCategories.length <= 0">None</p>
                                <p v-for="category in form.childCategories">- @{{ category.name }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                <label for="name"><b>Accounts:</b></label>
                                <p v-if="form.accounts.length <= 0">None</p>
                                <p v-for="account in form.accounts">- @{{ account.title }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script src="{{ mix('js/category.js') }}"></script>
@endpush