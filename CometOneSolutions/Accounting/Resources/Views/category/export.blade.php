<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Normal Balance</th>
            <th>Book</th>
            <th>Parent Category</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $category)
        <tr>
            <td>{{ $category->getId() }}</td>
            <td>{{ $category->getName() }}</td>
            <td>{{ $category->getNormalBalance() }}</td>
            <td>{{ $category->getBook()->getName() }}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>