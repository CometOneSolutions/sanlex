<div class="card-body">
<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">
        <div class="col-6">
            <div class="form-group">
                <label for="date">Name</label>
                <input type="text" class="form-control" v-model="form.name">
            </div>
        </div>

        <div class="col-4">
        <div class="form-group">
            <label for="normalBalance">Normal Balance:</label><br>
            <label style="text-indent:50px;"><input type="radio" name="normal" v-model="form.normalBalance" value="Credit" :disabled="form.id!=null"> Credit</label><br>
            <label style="text-indent:50px;"><input type="radio" name="normal" v-model="form.normalBalance" value="Debit" :disabled="form.id!=null"> Debit</label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
        <label for="exampleInputEmail1">Book</label>
        <select2 :disabled="form.id!=null" class="form-control select2-single selecttwo" v-on:change="fillAccounts(form.bookId)" v-model.number="form.bookId">
            <option :value="null">Please Select Account</option>
            <option 
            v-for="book in bookSelections" 
            :value="book.id">
            @{{ book.name }}
            </option>
        </select2>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="form-group">
        <label for="exampleInputEmail1">Parent Category</label>
        <select2 class="form-control select2-single selecttwo" v-model.number="form.parentCategoryId">
            <option :value="null">Select Parent Category</option>
            <option 
            v-for="category in categoriesSelections" 
            :value="category.id">
            @{{ category.name }}
            </option>
        </select2>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-11">
        <div class="form-group">
        <label for="exampleInputEmail1">Accounts</label>
        <select2 :disabled="!accountInitialized" @change="syncData(selectedAccountId)" class="form-control select2-single selecttwo" v-model.number="selectedAccountId">
            <option :value="null">Select Account</option>
            <option 
            v-for="account in accountSelections" 
            :value="account.id">
            @{{ account.title }}
            </option>
        </select2>
        </div>
    </div>
    <div class="col-1">
    <label for="exampleInputEmail1">Add</label><br>
    <button type="button" @click="addAccount" class="btn btn-success btn-sm"><i class="fas fa-plus"></i></button>
    </div>
</div>

    <br>
    <table class="table">
        <tr class="row">
            <th class="col-11">Title</th>
            <th class="text-right col-1">Action</th>
        </tr>
        
        <tr is="attached-accounts"
            v-for="(detail,index) in form.accounts"
            :detail="detail"
            v-on:remove="form.accounts.splice(index, 1)">
        </tr>
    </table>

</div>
@push('scripts')
<script src="{{ mix('js/category.js') }}"></script>
@endpush