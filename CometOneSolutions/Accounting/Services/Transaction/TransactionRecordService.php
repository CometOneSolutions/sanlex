<?php

namespace CometOneSolutions\Accounting\Services\Transaction;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\TransactionValue;
use CometOneSolutions\Common\Services\RecordService;

interface TransactionRecordService extends RecordService
{
    public function make(Account $account, TransactionValue $value);

    public function make2(Account $account, $type, $amount);
}
