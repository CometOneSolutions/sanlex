<?php

namespace CometOneSolutions\Accounting\Services\Transaction;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Transaction\Transaction;
use CometOneSolutions\Accounting\Models\TransactionValue;
use CometOneSolutions\Accounting\Repositories\Transaction\Transactions;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class TransactionRecordServiceImpl extends RecordServiceImpl implements TransactionRecordService
{
    protected $transactions;
    protected $transaction;

    public function __construct(Transactions $transactions, Transaction $transaction)
    {
        parent::__construct($transactions);
        $this->transactions = $transactions;
        $this->transaction = $transaction;
    }

    public function make(Account $account, TransactionValue $value)
    {
        $transaction = new $this->transaction;
        $transaction->setAccount($account);
        $transaction->setValue($value);
        return $transaction;
    }

    public function make2(Account $account, $type, $amount)
    {
        $transaction = new $this->transaction;
        $transaction->setAccount($account);
        $value = new TransactionValue($type, $amount);
        $transaction->setValue($value);
        return $transaction;
    }
}
