<?php

namespace CometOneSolutions\Accounting\Services\Book;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Accounting\Repositories\Book\Books;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Utils\Filter;

class BookRecordServiceImpl extends RecordServiceImpl implements BookRecordService
{
    private $books;
    private $book;

    public function __construct(Books $books, Book $book)
    {
        parent::__construct($books);
        $this->books = $books;
        $this->book = $book;
    }

    public function getByOrganizationAndCurrency(Organization $organization, Currency $currency)
    {
        $organizationFilter = new Filter('organization_id', $organization->getId());
        $currencyFilter = new Filter('currency_id', $currency->getId());
        return $this->getFirst([$organizationFilter, $currencyFilter]);
    }

    public function create(Organization $organization, Currency $currency, $name, User $user = null)
    {
        $book = new $this->book;
        $book->setOrganization($organization);
        $book->setCurrency($currency);
        $book->setName($name);
        if ($user) {
            $book->setUpdatedByUser($user);
        }
        $this->books->save($book);
        return $book;
    }

    public function update(Book $book, Organization $organization, Currency $currency, $name, User $user = null)
    {
        $book->setOrganization($organization);
        $book->setCurrency($currency);
        $book->setName($name);
        if ($user) {
            $book->setUpdatedByUser($user);
        }
        $this->books->save($book);
        return $book;
    }

    public function delete(Book $book)
    {
        // TODO delete checks
        $this->books->delete($book);
        return $book;
    }
}
