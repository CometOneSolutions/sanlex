<?php

namespace CometOneSolutions\Accounting\Services\Book;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface BookRecordService extends RecordService
{
    public function getByOrganizationAndCurrency(Organization $organization, Currency $currency);

    public function create(Organization $organization, Currency $currency, $name, User $user = null);

    public function update(Book $book, Organization $organization, Currency $currency, $name, User $user = null);

    public function delete(Book $book);
}
