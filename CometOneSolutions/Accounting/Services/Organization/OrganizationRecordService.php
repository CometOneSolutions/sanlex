<?php

namespace CometOneSolutions\Accounting\Services\Organization;

use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface OrganizationRecordService extends RecordService
{
    public function create($name, User $user = null);

    public function update(Organization $organization, $name, User $user = null);

    public function delete(Organization $organization);
}
