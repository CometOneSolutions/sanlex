<?php

namespace CometOneSolutions\Accounting\Services\Organization;

use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Accounting\Repositories\Organization\Organizations;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class OrganizationRecordServiceImpl extends RecordServiceImpl implements OrganizationRecordService
{
    private $organizations;
    private $organization;

    public function __construct(Organizations $organizations, Organization $organization)
    {
        parent::__construct($organizations);
        $this->organizations = $organizations;
        $this->organization = $organization;
    }

    public function create($name, User $user = null)
    {
        $organization = new $this->organization;
        $organization->setName($name);
        if ($user) {
            $organization->setUpdatedByUser($user);
        }
        $this->organizations->save($organization);
        return $organization;
    }

    public function update(Organization $organization, $name, User $user = null)
    {
        $organization->setName($name);
        if ($user) {
            $organization->setUpdatedByUser($user);
        }
        $this->organizations->save($organization);
        return $organization;
    }

    public function delete(Organization $organization)
    {
        // TODO delete checks
        $this->organizations->delete($organization);
        return $organization;
    }
}
