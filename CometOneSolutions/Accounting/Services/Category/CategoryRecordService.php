<?php

namespace CometOneSolutions\Accounting\Services\Category;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Category\Category;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface CategoryRecordService extends RecordService
{
    public function create(Book $book, string $name, $normalBalance, array $accounts = [], Category $parentCategory = null, User $user = null);

    public function attachCategory(Category $child, Category $parent);

    public function attachAccount(Category $category, Account $account);

    public function detachAccount(Category $category, Account $account);

    public function update(Category $category, Book $book, string $name, $normalBalance, array $accounts = [], Category $parentCategory = null, User $user = null);

    public function delete(Category $category);
}
