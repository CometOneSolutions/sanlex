<?php

namespace CometOneSolutions\Accounting\Services\Category;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Category\Category;
use CometOneSolutions\Accounting\Repositories\Category\Categories;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class CategoryRecordServiceImpl extends RecordServiceImpl implements CategoryRecordService
{
    protected $categories;
    protected $category;

    public function __construct(Categories $categories, Category $category)
    {
        parent::__construct($categories);
        $this->categories = $categories;
        $this->category = $category;
    }

    public function create(Book $book, string $name, $normalBalance, array $accounts = [], Category $parentCategory = null, User $user = null)
    {
        $category = new $this->category;
        $category->setBook($book);
        $category->setName($name);
        $category->setNormalBalance($normalBalance);
        if ($parentCategory) {
            $category->setParentCategory($parentCategory);
        }
        if ($user) {
            $category->setUpdatedByUser($user);
        }
        $category->setAccounts($accounts);
        $this->categories->save($category);
        return $category;
    }

    public function update(Category $category, Book $book, string $name, $normalBalance, array $accounts = [], Category $parentCategory = null, User $user = null)
    {
        $category->setBook($book);
        $category->setName($name);
        $category->setNormalBalance($normalBalance);
        if ($parentCategory) {
            $category->setParentCategory($parentCategory);
        }
        if ($user) {
            $category->setUpdatedByUser($user);
        }
        $category->setAccounts($accounts);
        $this->categories->save($category);
        return $category;
    }

    public function attachCategory(Category $child, Category $parent)
    {
        $child->setParentCategory($parent);
        $this->categories->save($child);
        return $child;
    }

    public function attachAccount(Category $category, Account $account)
    {
        $category->attachAccount($account);
        $this->categories->save($category);
        return $category;
    }

    public function detachAccount(Category $category, Account $account)
    {
        $category->detachAccount($account);
        $this->categories->save($category);
        return $category;
    }

    public function delete(Category $category)
    {
        // TODO delete checks
        $this->categories->delete($category);
        return $category;
    }
}
