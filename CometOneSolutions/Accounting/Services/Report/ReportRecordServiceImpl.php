<?php

namespace CometOneSolutions\Accounting\Services\Report;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Category\Category;
use CometOneSolutions\Accounting\Models\Report\Report;
use CometOneSolutions\Accounting\Repositories\Report\Reports;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class ReportRecordServiceImpl extends RecordServiceImpl implements ReportRecordService
{
    private $reports;
    private $report;

    public function __construct(Reports $reports, Report $report)
    {
        parent::__construct($reports);
        $this->reports = $reports;
        $this->report = $report;
    }

    public function create(Book $book, string $description, array $categories = [], User $user = null)
    {
        $report = new $this->report;
        $report->setBook($book);
        $report->setDescription($description);
        $report->setCategories($categories);
        if ($user) {
            $report->setUpdatedByUser($user);
        }
        $this->reports->save($report);
        return $report;
    }

    public function update(Report $report, Book $book, string $description, array $categories = [], User $user = null)
    {
        $tempReport = clone $report;
        $tempReport->setBook($book);
        $tempReport->setDescription($description);
        $tempReport->setCategories($categories);
        if ($user) {
            $tempReport->setUpdatedByUser($user);
        }
        $this->reports->save($tempReport);
        return $tempReport;
    }

    public function delete(Report $report)
    {
        // TODO delete checks
        $this->reports->delete($report);
        return $report;
    }

    public function attachCategory(Report $report, Category $category)
    {
        $report->attachCategory($category);
        $this->reports->save($report);
        return $report;
    }
}
