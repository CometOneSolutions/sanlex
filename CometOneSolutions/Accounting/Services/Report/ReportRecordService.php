<?php

namespace CometOneSolutions\Accounting\Services\Report;




use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Category\Category;
use CometOneSolutions\Accounting\Models\Report\Report;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface ReportRecordService extends RecordService
{
    public function create(Book $book, string $description, array $categories = [], User $user = null);

    public function update(Report $report, Book $book, string $description, array $categories = [], User $user = null);

    public function attachCategory(Report $report, Category $category);

    public function delete(Report $report);
}
