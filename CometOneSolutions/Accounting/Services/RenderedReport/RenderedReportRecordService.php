<?php

namespace CometOneSolutions\Accounting\Services\RenderedReport;

use CometOneSolutions\Accounting\Models\RenderedReport\RenderedReport;
use CometOneSolutions\Accounting\Models\Report\Report;
use CometOneSolutions\Common\Services\RecordService;

interface RenderedReportRecordService extends RecordService
{
    public function generate(Report $report);

    public function delete(RenderedReport $renderedReport);
}
