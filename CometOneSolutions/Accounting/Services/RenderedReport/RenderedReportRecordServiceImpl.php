<?php

namespace CometOneSolutions\Accounting\Services\RenderedReport;

use CometOneSolutions\Accounting\Models\RenderedReport\RenderedReport;
use CometOneSolutions\Accounting\Models\Report\Report;
use CometOneSolutions\Accounting\Repositories\RenderedReport\RenderedReports;
use CometOneSolutions\Accounting\Services\CategoryLine\CategoryLineRecordService;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class RenderedReportRecordServiceImpl extends RecordServiceImpl implements RenderedReportRecordService
{
    protected $renderedReports;
    protected $renderedReport;
    protected $categoryLineRecordService;

    public function __construct(
        RenderedReports $renderedReports,
        RenderedReport $renderedReport,
        CategoryLineRecordService $categoryLineRecordService
    ) {
        parent::__construct($renderedReports);
        $this->renderedReports = $renderedReports;
        $this->renderedReport = $renderedReport;
        $this->categoryLineRecordService = $categoryLineRecordService;
    }

    public function generate(Report $report)
    {
        $renderedReport = new $this->renderedReport;
        $renderedReport->setReport($report);

        $categoryLines = $report->getCategories()->map(function ($category) {
            return $this->categoryLineRecordService->make($category);
        })->all();

        $renderedReport->setCategoryLines($categoryLines);

        $this->renderedReports->save($renderedReport);
        return $renderedReport;
    }

    public function delete(RenderedReport $renderedReport)
    {
        $this->renderedReports->delete($renderedReport);
        return $renderedReport;
    }
}
