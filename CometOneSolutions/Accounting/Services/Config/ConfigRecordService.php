<?php

namespace CometOneSolutions\Accounting\Services\Config;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Config\Config;
use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface ConfigRecordService extends RecordService
{
    public function getConfig();

    public function update(
        Config $config,
        Organization $organization,
        Book $book,
        User $user = null
    );

    public function setCurrentOrganization(
        Config $config,
        Organization $organization,
        User $user = null
    );

    public function setCurrentBook(
        Config $config,
        Book $book,
        User $user = null
    );

    public function getCurrentBook();

    public function getCurrentOrganization();

}
