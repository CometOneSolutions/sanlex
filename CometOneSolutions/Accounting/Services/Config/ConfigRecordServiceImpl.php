<?php

namespace CometOneSolutions\Accounting\Services\Config;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Config\Config;
use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Accounting\Repositories\Config\Configs;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class ConfigRecordServiceImpl extends RecordServiceImpl implements ConfigRecordService
{
    private $configs;
    private $config;

    public function __construct(
        Configs $configRepository,
        Config $config
    ) {
        parent::__construct($configRepository);
        $this->config = $config;
        $this->configs = $configRepository;
    }

    public function getConfig()
    {
        if ($this->configs->getAll()->count() > 0) {
            return $this->configs->getFirst();
        } else {
            $newConfig = new $this->config;
            $this->configs->save($newConfig);
            return $newConfig;
        }
    }

    public function getCurrentBook()
    {
        $config = $this->getConfig();
        return $config->getCurrentBook();
    }

    public function getCurrentOrganization()
    {
        $config = $this->getConfig();
        return $config->getCurrentOrganization();
    }

    public function update(
        Config $config,
        Organization $organization,
        Book $book,
        User $user = null
    ) {
        $tempConfig = clone $config;
        $tempConfig->setCurrentOrganization($organization);
        $tempConfig->setCurrentBook($book);
        if ($user) {
            $tempConfig->setUpdatedByUser($user);
        }
        $this->configs->save($tempConfig);
        return $tempConfig;
    }

    public function setCurrentOrganization(
        Config $config,
        Organization $organization,
        User $user = null
    ) {
        $tempConfig = clone $config;
        $tempConfig->setCurrentOrganization($organization);
        if ($user) {
            $tempConfig->setUpdatedByUser($user);
        }
        $this->configs->save($tempConfig);
        return $tempConfig;
    }

    public function setCurrentBook(
        Config $config,
        Book $book,
        User $user = null
    ) {
        $tempConfig = clone $config;
        $tempConfig->setCurrentBook($book);
        if ($user) {
            $tempConfig->setUpdatedByUser($user);
        }
        $this->configs->save($tempConfig);
        return $tempConfig;
    }
}
