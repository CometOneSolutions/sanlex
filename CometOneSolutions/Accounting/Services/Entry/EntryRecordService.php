<?php

namespace CometOneSolutions\Accounting\Services\Entry;

use CometOneSolutions\Accounting\Accountable;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Entry\Entry;
use CometOneSolutions\Common\Services\RecordService;
use \DateTime;
use CometOneSolutions\Auth\Models\Users\User;

interface EntryRecordService extends RecordService
{
    public function make(Book $book, DateTime $date, array $transactions, $notes = null, Accountable $accountable = null);

    public function create(Book $book = null, DateTime $date, array $transactions, $notes = null, Accountable $accountable, User $user = null);

    public function updateSparse(Entry $entry, $notes = null);

    public function update(Entry $entry, DateTime $date, array $transactions, $notes = null, User $user = null);

    public function delete(Entry $entry);
}
