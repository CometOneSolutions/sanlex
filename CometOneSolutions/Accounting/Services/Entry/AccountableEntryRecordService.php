<?php

namespace CometOneSolutions\Accounting\Services\Entry;

use CometOneSolutions\Accounting\Accountable;
use CometOneSolutions\Accounting\Models\TransactionType;
use CometOneSolutions\Accounting\Models\TransactionValue;
use CometOneSolutions\Accounting\Services\Transaction\TransactionRecordService;
use \DateTime;
use CometOneSolutions\Common\Services\DecoratorRecordServiceImpl;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

abstract class AccountableEntryRecordService extends DecoratorRecordServiceImpl implements RecordService
{
    protected $entryRecordService;
    protected $transactionRecordService;

    public function __construct(
        RecordService $decoratedRecordService,
        EntryRecordService $entryRecordService,
        TransactionRecordService $transactionRecordService
    ) {
        parent::__construct($decoratedRecordService);
        $this->entryRecordService = $entryRecordService;
        $this->transactionRecordService = $transactionRecordService;
    }

    protected function createAccountableEntry(Accountable $accountable, DateTime $date, $amount, User $user = null)
    {
        $transactions = [
            $this->transactionRecordService->make($this->getDebitAccount($accountable), new TransactionValue(TransactionType::DEBIT, $amount)),
            $this->transactionRecordService->make($this->getCreditAccount($accountable), new TransactionValue(TransactionType::CREDIT, $amount)),
        ];

        $this->entryRecordService->create(
            null,   // book
            $date,
            $transactions,
            null,   // notes
            $accountable,
            $user
        );
        return $this;
    }

    protected function updateAccountableEntries(Accountable $accountable, DateTime $date, $amount, User $user)
    {
        foreach ($accountable->getEntries() as $entry) {
            $transactions = $entry->getTransactions()->map(function ($transaction) use ($accountable, $amount) {
                if ($transaction->getValue()->isDr()) {
                    $transaction->setAccount($this->getDebitAccount($accountable));
                } elseif ($transaction->getValue()->isCr()) {
                    $transaction->setAccount($this->getCreditAccount($accountable));
                }
                $transaction->setValue(new TransactionValue($transaction->getType(), $amount));
                return $transaction;
            })->all();

            $this->entryRecordService->update(
                $entry,
                $date,
                $transactions,
                null,   // notes
                $user
            );
        }

        return $this;
    }

    public function deleteAccountableEntries(Accountable $accountable)
    {
        foreach ($accountable->getEntries() as $entry) {
            $this->entryRecordService->delete($entry);
        }
    }

    abstract protected function getDebitAccount(Accountable $accountable);

    abstract protected function getCreditAccount(Accountable $accountable);
}
