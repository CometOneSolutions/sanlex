<?php

namespace CometOneSolutions\Accounting\Services\Entry;

use CometOneSolutions\Accounting\Accountable;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Entry\Entry;
use CometOneSolutions\Accounting\Models\NormalBalance;
use CometOneSolutions\Accounting\Models\Transaction\Transaction;
use CometOneSolutions\Accounting\Repositories\Entry\Entries;
use CometOneSolutions\Accounting\Services\Config\ConfigRecordService;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use \DateTime;
use \Exception;
use CometOneSolutions\Auth\Models\Users\User;

class EntryRecordServiceImpl extends RecordServiceImpl implements EntryRecordService
{
    protected $entries;
    protected $entry;
    protected $transaction;
    protected $configRecordService;

    public function __construct(
        Entries $entries,
        Entry $entry,
        Transaction $transaction,
        ConfigRecordService $configRecordService
    ) {
        parent::__construct($entries);
        $this->entries = $entries;
        $this->entry = $entry;
        $this->transaction = $transaction;
        $this->configRecordService = $configRecordService;
    }

    protected function transactionsAreBalanced(array $transactions = [])
    {
        $balance[NormalBalance::DEBIT] = 0;
        $balance[NormalBalance::CREDIT] = 0;

        foreach ($transactions as $transaction) {
            $accountNormalBalance = $transaction->getAccount()->getNormalBalance();
            $transactionType = $transaction->getType();
            $multiplier = strcasecmp($accountNormalBalance, $transactionType) == 0 ? 1 : -1;
            $balance[$accountNormalBalance] += $multiplier * $transaction->getAmount();
        }
        return $balance[NormalBalance::DEBIT] == $balance[NormalBalance::CREDIT];
    }

    public function make(Book $book, DateTime $date, array $transactions, $notes = null, Accountable $accountable = null)
    {
        $entry = new $this->entry;

        if (!$this->transactionsAreBalanced($transactions)) {
            throw new Exception('Not Balanced.');
        }

        $entry->setBook($book);
        $entry->setDate($date);
        $entry->setTransactions($transactions);
        $entry->setNotes($notes);
        if ($accountable) {
            $entry->setAccountable($accountable);
        }
        return $entry;
    }

    public function create(Book $book = null, DateTime $date, array $transactions, $notes = null, Accountable $accountable, User $user = null)
    {
        if (!$book) {
            $book = $this->configRecordService->getCurrentBook();
        }
        $entry = $this->make($book, $date, $transactions, $notes, $accountable);
        if ($user) {
            $entry->setUpdatedByUser($user);
        }
        $this->entries->save($entry);
        return $entry;
    }

    public function update(Entry $entry, DateTime $date, array $transactions, $notes = null, User $user = null)
    {
        if (!$this->transactionsAreBalanced($transactions)) {
            throw new Exception('Not Balanced.');
        }

        $entry->setDate($date);
        $entry->setTransactions($transactions);

        $entry->setNotes($notes);
        if ($user) {
            $entry->setUpdatedByUser($user);
        }
        $this->entries->save($entry);

        return $entry;
    }

    public function updateSparse(Entry $entry, $notes = null)
    {
        $entry->setNotes($notes);
        $this->entries->save($entry);
        return $entry;
    }

    public function delete(Entry $entry)
    {
        // TODO delete checks
        $this->entries->delete($entry);
        return $entry;
    }
}
