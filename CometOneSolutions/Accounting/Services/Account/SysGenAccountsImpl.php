<?php

namespace CometOneSolutions\Accounting\Services\Account;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Account\SysGen\AccountsReceivable;
use CometOneSolutions\Accounting\Models\Account\SysGen\CustomerDownpayments;
use CometOneSolutions\Accounting\Models\Account\SysGen\SysGenAccount;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Repositories\Account\Accounts;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class SysGenAccountsImpl extends RecordServiceImpl implements SysGenAccounts
{
    private $accounts;
    private $account;

    public function __construct(Accounts $accounts, Account $account)
    {
        parent::__construct($accounts);
        $this->accounts = $accounts;
        $this->account = $account;
    }

    public function accountsReceivable(Book $book)
    {
        return $this->getOrCreate(new AccountsReceivable($book));
    }

    public function customerDownpayments(Book $book)
    {
        return $this->getOrCreate(new CustomerDownpayments($book));
    }

    protected function getOrCreate(SysGenAccount $sysGenAccount)
    {
        $account = $this->accounts->getByBookAndTitle($sysGenAccount->getBook(), $sysGenAccount->getTitle());
        if ($account == null) {
            $this->accounts->save($sysGenAccount);
            return $sysGenAccount;
        }
        return $account;
    }
}
