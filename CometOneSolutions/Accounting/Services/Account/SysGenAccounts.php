<?php

namespace CometOneSolutions\Accounting\Services\Account;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Common\Services\RecordService;

interface SysGenAccounts extends RecordService
{
    public function accountsReceivable(Book $book);

    public function customerDownpayments(Book $book);
}
