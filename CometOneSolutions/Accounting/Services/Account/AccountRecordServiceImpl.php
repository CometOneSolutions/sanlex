<?php

namespace CometOneSolutions\Accounting\Services\Account;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Accounting\Repositories\Account\Accounts;
use CometOneSolutions\Accounting\Repositories\Transaction\Transactions;
use CometOneSolutions\Accounting\Services\Book\BookRecordService;
use CometOneSolutions\Accounting\Services\Config\ConfigRecordService;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Traits\CanGetByName;

class AccountRecordServiceImpl extends RecordServiceImpl implements AccountRecordService
{
    protected $accounts;
    protected $account;
    protected $transactions;
    protected $bookRecordService;
    protected $configRecordService;

    use CanGetByName;

    public function __construct(
        Accounts $accounts,
        Account $account,
        Transactions $transactions,
        BookRecordService $bookRecordService,
        ConfigRecordService $configRecordService
    ) {
        parent::__construct($accounts);
        $this->accounts = $accounts;
        $this->account = $account;
        $this->transactions = $transactions;
        $this->bookRecordService = $bookRecordService;
        $this->configRecordService = $configRecordService;
    }

    public function getTransactions(Account $account, int $take = null)
    {
        $clone = clone $this->transactions;
        $transactions = $clone->where('account_id', $account->getId())->orderBy('entry_date');
        if ($take !== null) {
            $transactions = $transactions->take($take);
        }
        return $transactions->getAll();
    }

    public function getBalance(Account $account, int $take = null)
    {
        return $account->computeBalance(null, $take)->getValue($account->getNormalBalance());
    }

    public function getPaginatedTransactions(Account $account, $perPage)
    {
        $clone = clone $this->transactions;
        return $clone->where('account_id', $account->getId())
            ->orderBy('entry_date')
            ->getPaginated($perPage);
    }

    public function create2(Currency $currency, string $title, $normalBalance, Organization $organization = null, User $user = null)
    {
        if (!$organization) {
            $organization = $this->configRecordService->getCurrentOrganization();
        }
        $book = $this->bookRecordService->getByOrganizationAndCurrency($organization, $currency);
        return $this->create($book, $title, $normalBalance, $user);
    }

    public function update2(Account $account, string $title, Currency $currency, Organization $organization = null, User $user = null)
    {
        if (!$organization) {
            $organization = $this->configRecordService->getCurrentOrganization();
        }
        $book = $this->bookRecordService->getByOrganizationAndCurrency($organization, $currency);
        return $this->update($account, $title, $book, $user);
    }

    public function create(Book $book, string $title, $normalBalance, User $user = null)
    {
        $account = new $this->account;
        $account->setBook($book);
        $account->setTitle($title);
        $account->setNormalBalance($normalBalance);
        $account->initializeBalance();
        if ($user) {
            $account->setUpdatedByUser($user);
        }
        $this->accounts->save($account);
        return $account;
    }

    public function update(Account $account, string $title, Book $book = null, User $user = null)
    {
        $account->setTitle($title);
        if ($book) {
            $account->setBook($book);
        }
        if ($user) {
            $account->setUpdatedByUser($user);
        }
        $this->accounts->save($account);
        return $account;
    }

    public function delete(Account $account)
    {
        // TODO delete checks
        $this->accounts->delete($account);
        return $account;
    }
}
