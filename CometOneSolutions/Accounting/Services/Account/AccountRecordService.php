<?php

namespace CometOneSolutions\Accounting\Services\Account;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface AccountRecordService extends RecordService
{
    public static function getByName($name, $field = 'name');

    public function getTransactions(Account $account, int $take = null);

    public function getPaginatedTransactions(Account $account, $perPage);

    public function create(Book $book, string $title, $normalBalance, User $user = null);

    public function create2(Currency $currency, string $title, $normalBalance, Organization $organization = null, User $user = null);

    public function update(Account $account, string $title, Book $book = null, User $user = null);

    public function update2(Account $account, string $title, Currency $currency, Organization $organization = null, User $user = null);

    public function delete(Account $account);
}
