<?php

namespace CometOneSolutions\Accounting\Services\Account;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Repositories\Account\Accounts;
use CometOneSolutions\Accounting\Repositories\Transaction\Transactions;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class BankingAccountRecordServiceImpl extends RecordServiceImpl implements AccountRecordService
{
    protected $accounts;
    protected $account;
    protected $transactions;

    protected $accountRecordService;

    public function __construct(Accounts $accounts, Account $account, Transactions $transactions, AccountRecordService $accountRecordService)
    {
        parent::__construct($accounts);
        $this->accounts = $accounts;
        $this->account = $account;
        $this->transactions = $transactions;
        $this->accountRecordService = $accountRecordService;
    }

    public function getTransactions(Account $account, int $take = null)
    {
        return $this->accountRecordService->getTransactions($account, $take);
    }

    public function getBalance(Account $account, int $take = null)
    {
        return $account->computeBalance(null, $take)->getValue($account->getNormalBalance());
    }

    public function getPaginatedTransactions(Account $account, $perPage)
    {
        $clone = clone $this->transactions;
        return $clone->where('account_id', $account->getId())
            ->orderBy('entry_date')
            ->getPaginated($perPage);
    }

    public function create(Book $book, string $title, $normalBalance, User $user = null)
    {
        $account = new $this->account;
        $account->setBook($book);
        $account->setTitle($title);
        $account->setNormalBalance($normalBalance);
        $account->initializeBalance();
        if ($user) {
            $account->setUpdatedByUser($user);
        }
        $this->accounts->save($account);
        return $account;
    }

    public function update(Account $account, string $title, User $user = null)
    {
        $account->setTitle($title);
        if ($user) {
            $account->setUpdatedByUser($user);
        }
        $this->accounts->save($account);
        return $account;
    }

    public function delete(Account $account)
    {
        // TODO delete checks
        $this->accounts->delete($account);
        return $account;
    }
}
