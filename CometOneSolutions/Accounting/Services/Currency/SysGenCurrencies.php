<?php

namespace CometOneSolutions\Accounting\Services\Currency;

use CometOneSolutions\Common\Services\RecordService;

interface SysGenCurrencies extends RecordService
{
    public function php();
}
