<?php

namespace CometOneSolutions\Accounting\Services\Currency;

use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface CurrencyRecordService extends RecordService
{
    public function create($code, $description, User $user = null);

    public function update(Currency $currency, string $code, string $description, User $user = null);

    public function delete(Currency $currency);
}
