<?php

namespace CometOneSolutions\Accounting\Services\Currency;

use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Accounting\Repositories\Currency\Currencies;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class CurrencyRecordServiceImpl extends RecordServiceImpl implements CurrencyRecordService
{
    private $currencies;
    private $currency;

    public function __construct(Currencies $currencies, Currency $currency)
    {
        parent::__construct($currencies);
        $this->currencies = $currencies;
        $this->currency = $currency;
    }

    public function create($code, $description, User $user = null)
    {
        $currency = new $this->currency;
        $currency->setCode($code);
        $currency->setDescription($description);
        if ($user) {
            $currency->setUpdatedByUser($user);
        }
        $this->currencies->save($currency);
        return $currency;
    }

    public function update(Currency $currency, string $code, string $description, User $user = null)
    {
        $currency->setCode($code);
        $currency->setDescription($description);
        if ($user) {
            $currency->setUpdatedByUser($user);
        }
        $this->currencies->save($currency);
        return $currency;
    }

    public function delete(Currency $currency)
    {
        // TODO delete checks
        $this->currencies->delete($currency);
        return $currency;
    }
}
