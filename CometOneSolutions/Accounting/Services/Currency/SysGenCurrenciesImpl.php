<?php

namespace CometOneSolutions\Accounting\Services\Currency;

use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Accounting\Models\Currency\SysGen\PhilippinePeso;
use CometOneSolutions\Accounting\Repositories\Currency\Currencies;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Common\Utils\Filter;

class SysGenCurrenciesImpl extends RecordServiceImpl implements SysGenCurrencies
{
    private $currencies;
    private $currency;

    public function __construct(Currencies $currencies, Currency $currency)
    {
        parent::__construct($currencies);
        $this->currencies = $currencies;
        $this->currency = $currency;
    }

    public function php()
    {
        return $this->getOrCreate(new PhilippinePeso());
    }

    protected function getOrCreate(Currency $sysGenCurrency)
    {
        $codeFilter = new Filter('code', $sysGenCurrency->getCode());
        $descriptionFilter = new Filter('description', $sysGenCurrency->getDescription());

        $currency = $this->getFirst([$codeFilter, $descriptionFilter]);

        if ($currency == null) {
            $this->currencies->save($sysGenCurrency);
            return $sysGenCurrency;
        }

        return $currency;
    }
}
