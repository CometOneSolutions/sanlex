<?php

namespace CometOneSolutions\Accounting\Services\CategoryLine;

use CometOneSolutions\Accounting\Models\Balance;
use CometOneSolutions\Accounting\Models\Category\Category;
use CometOneSolutions\Accounting\Models\CategoryLine\CategoryLine;
use CometOneSolutions\Accounting\Repositories\CategoryLine\CategoryLines;
use CometOneSolutions\Accounting\Services\AccountLine\AccountLineRecordService;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class CategoryLineRecordServiceImpl extends RecordServiceImpl implements CategoryLineRecordService
{
    protected $categoryLines;
    protected $categoryLine;
    protected $accountLineRecordService;

    public function __construct(
        CategoryLines $categoryLines,
        CategoryLine $categoryLine,
        AccountLineRecordService $accountLineRecordService
    ) {
        parent::__construct($categoryLines);
        $this->categoryLines = $categoryLines;
        $this->categoryLine = $categoryLine;
        $this->accountLineRecordService = $accountLineRecordService;
    }

    public function make(Category $category)
    {
        $categoryLine = new $this->categoryLine;

        $categoryLine->setCategory($category);

        $accountLines = $category->getAccounts()->map(function ($account) {
            return $this->accountLineRecordService->make($account);
        })->all();

        $categoryLine->setAccountLines($accountLines);

        $childCategoryLines = $category->getChildCategories()->map(function ($childCategory) {
            return $this->make($childCategory);
        })->all();

        $categoryLine->setChildCategoryLines($childCategoryLines);

        $accountBalance = $categoryLine->getAccountLines()->reduce(function ($balance, $accountLine) use ($category) {
            $drAmount = $balance->getDebitAmount() + $accountLine->getDr();
            $crAmount = $balance->getCreditAmount() + $accountLine->getCr();
            return new Balance($drAmount, $crAmount);
        }, new Balance(0, 0));

        $childCategoriesBalance = $categoryLine->getChildCategoryLines()->reduce(function ($balance, $childCategoryLine) use ($category) {
            $drAmount = $balance->getDebitAmount() + $childCategoryLine->getDr();
            $crAmount = $balance->getCreditAmount() + $childCategoryLine->getCr();
            return new Balance($drAmount, $crAmount);
        }, $accountBalance);

        $categoryLine->setDr($childCategoriesBalance->getDebitAmount());
        $categoryLine->setCr($childCategoriesBalance->getCreditAmount());

        return $categoryLine;
    }
}
