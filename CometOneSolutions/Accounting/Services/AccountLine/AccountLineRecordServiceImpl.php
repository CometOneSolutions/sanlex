<?php

namespace CometOneSolutions\Accounting\Services\AccountLine;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\AccountLine\AccountLine;
use CometOneSolutions\Accounting\Repositories\AccountLine\AccountLines;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class AccountLineRecordServiceImpl extends RecordServiceImpl implements AccountLineRecordService
{
    protected $accountLines;
    protected $accountLine;

    public function __construct(AccountLines $accountLines, AccountLine $accountLine)
    {
        parent::__construct($accountLines);
        $this->accountLines = $accountLines;
        $this->accountLine = $accountLine;
    }

    public function make(Account $account)
    {
        $accountLine = new $this->accountLine;
        $accountLine->setAccount($account);
        $balance = $account->computeBalance();
        $accountLine->setDr($balance->getDebitAmount());
        $accountLine->setCr($balance->getCreditAmount());
        return $accountLine;
    }
}
