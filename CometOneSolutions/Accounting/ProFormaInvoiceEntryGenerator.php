<?php

namespace CometOneSolutions\Accounting;

use CometOneSolutions\Accounting\Services\Account\SysGenAccounts;
use CometOneSolutions\Accounting\Models\Transaction\TransactionModel;
use CometOneSolutions\Accounting\Models\TransactionValue;
use CometOneSolutions\Accounting\Models\TransactionType;

class ProFormaInvoiceEntryGenerator
{
    protected $proFormaInvoice;
    protected $book;
    protected $sysGenAccounts;

    public function __construct(ProFormaInvoice $proFormaInvoice)
    {
        $this->proFormaInvoice = $proFormaInvoice;
        $this->sysGenAccounts = resolve(SysGenAccounts::class);
    }

    protected function initializeBook(ProFormaInvoice $proFormaInvoice)
    {
        $customer = $proFormaInvoice->getCustomer();
        $company = $customer->getCompany();
        $organization = $company->getOrganization(); // ?
        // $currency = $configService->getDefaultCurrency();
        // $this->book = organization->getCurrencyBook($currency);
    }

    // you are here
    protected function generateTransactions()
    {
        // you are here
        $amount = $this->proFormaInvoice->getAmount();
        $drAccount = $this->sysGenAccounts->accountsReceivable($this->book);
        $crAccount = $this->sysGenAccounts->customerDownpayments($this->book);

        $drTransaction = new TransactionModel();
        $drTransaction->setAccount($drAccount);
        $drTransaction->setValue(new TransactionValue(TransactionType::DEBIT, $amount));

        $crTransaction = new TransactionModel();
        $crTransaction->setAccount($crAccount);
        $crTransaction->setValue(new TransactionValue(TransactionType::CREDIT, $amount));

        return [$drTransaction, $crTransaction];
    }

    public function generate()
    {
        $date = $this->proFormaInvoice->getDate();
        $book = $this->book;
        $transactions = $this->generateTransactions();
        return $this->entryRecordService->make($book, $date, $transactions);
    }
}
