<?php

namespace CometOneSolutions\Accounting\Repositories\Config;

use CometOneSolutions\Accounting\Models\Config\Config;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentConfigs extends EloquentRepository implements Configs
{
    public function __construct(Config $config)
    {
        parent::__construct($config);
    }

    public function save(Config $config)
    {
        return $config->save();
    }

    public function delete(Config $config)
    {
        return $config->delete();
    }
}
