<?php

namespace CometOneSolutions\Accounting\Repositories\Config;

use CometOneSolutions\Accounting\Models\Config\Config;
use CometOneSolutions\Common\Repositories\Repository;

interface Configs extends Repository
{
    public function save(Config $config);

    public function delete(Config $config);
}
