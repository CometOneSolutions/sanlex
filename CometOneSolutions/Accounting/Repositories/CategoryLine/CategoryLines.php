<?php

namespace CometOneSolutions\Accounting\Repositories\CategoryLine;

use CometOneSolutions\Accounting\Models\CategoryLine\CategoryLine;
use CometOneSolutions\Common\Repositories\Repository;

interface CategoryLines extends Repository
{
    public function save(CategoryLine $categoryLine);

    public function delete(CategoryLine $categoryLine);
}
