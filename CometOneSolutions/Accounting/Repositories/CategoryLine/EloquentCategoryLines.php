<?php

namespace CometOneSolutions\Accounting\Repositories\CategoryLine;

use CometOneSolutions\Accounting\Models\CategoryLine\CategoryLine;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentCategoryLines extends EloquentRepository implements CategoryLines
{
    public function __construct(CategoryLine $categoryLine)
    {
        parent::__construct($categoryLine);
    }

    public function save(CategoryLine $categoryLine)
    {
        $result = $categoryLine->save();
        return $result;
    }

    public function delete(CategoryLine $categoryLine)
    {
        return $categoryLine->delete();
    }
}
