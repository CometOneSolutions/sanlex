<?php

namespace CometOneSolutions\Accounting\Repositories\Category;

use CometOneSolutions\Accounting\Models\Category\Category;
use CometOneSolutions\Common\Repositories\Repository;

interface Categories extends Repository
{
    public function save(Category $category);

    public function delete(Category $category);
}
