<?php

namespace CometOneSolutions\Accounting\Repositories\Category;

use CometOneSolutions\Accounting\Models\Category\Category;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentCategories extends EloquentRepository implements Categories
{
    public function __construct(Category $category)
    {
        parent::__construct($category);
    }

    public function save(Category $category)
    {
        $result = $category->save();
        $accountIds = $this->getIdsFromAccounts($category->getAccounts()->all());
        $category->accounts()->sync($accountIds);
        return $result;
    }

    public function delete(Category $category)
    {
        return $category->delete();
    }

    private function getIdsFromAccounts(array $accounts)
    {
        return array_map(function ($account) {
            return $account->getId();
        }, $accounts);
    }
}
