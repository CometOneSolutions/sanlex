<?php

namespace CometOneSolutions\Accounting\Repositories\Book;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Common\Repositories\Repository;

interface Books extends Repository
{
    public function save(Book $book);

    public function delete(Book $book);
}
