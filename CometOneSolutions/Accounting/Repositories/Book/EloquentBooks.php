<?php

namespace CometOneSolutions\Accounting\Repositories\Book;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentBooks extends EloquentRepository implements Books
{
    public function __construct(Book $book)
    {
        parent::__construct($book);
    }

    public function save(Book $book)
    {
        return $book->save();
    }

    public function delete(Book $book)
    {
        return $book->delete();
    }
}
