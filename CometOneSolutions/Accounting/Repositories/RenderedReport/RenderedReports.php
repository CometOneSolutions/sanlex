<?php

namespace CometOneSolutions\Accounting\Repositories\RenderedReport;

use CometOneSolutions\Accounting\Models\RenderedReport\RenderedReport;
use CometOneSolutions\Common\Repositories\Repository;

interface RenderedReports extends Repository
{
    public function save(RenderedReport $renderedReport);

    public function delete(RenderedReport $renderedReport);
}
