<?php

namespace CometOneSolutions\Accounting\Repositories\RenderedReport;

use CometOneSolutions\Accounting\Models\RenderedReport\RenderedReport;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentRenderedReports extends EloquentRepository implements RenderedReports
{
    public function __construct(RenderedReport $renderedReport)
    {
        parent::__construct($renderedReport);
    }

    public function save(RenderedReport $renderedReport)
    {
        $renderedReport->save();

        $renderedReport->categoryLines()->saveMany($renderedReport->getCategoryLines());
        $this->foo($renderedReport->getCategoryLines()->all());

        return $renderedReport;
    }

    protected function foo(array $categoryLines)
    {
        foreach ($categoryLines as $categoryLine) {
            $categoryLine->childCategoryLines()->saveMany($categoryLine->getChildCategoryLines());
            $categoryLine->accountLines()->saveMany($categoryLine->getAccountLines());
            $this->foo($categoryLine->getChildCategoryLines()->all());
        }
    }

    protected function bar(CategoryLine $categoryLine)
    {
        foreach ($categoryLine->getChildCategoryLines() as $categoryLine) {
            $categoryLine->childCategoryLines()->saveMany($categoryLine->getChildCategoryLines());
            $categoryLine->accountLines()->saveMany($categoryLine->getAccountLines());
            $this->bar($categoryLine);
        }
    }

    public function delete(RenderedReport $renderedReport)
    {
        return $renderedReport->delete();
    }
}
