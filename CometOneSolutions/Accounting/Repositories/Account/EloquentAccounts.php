<?php

namespace CometOneSolutions\Accounting\Repositories\Account;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentAccounts extends EloquentRepository implements Accounts
{
    public function __construct(Account $account)
    {
        parent::__construct($account);
    }

    public function getByBookAndTitle(Book $book, $title, $field = 'title')
    {
        return $this->where('book_id', $book->getId())->where($field, $title)->getFirst();
    }

    public function save(Account $account)
    {
        $account->save();
        return $account;
    }

    public function delete(Account $account)
    {
        return $account->delete();
    }
}
