<?php

namespace CometOneSolutions\Accounting\Repositories\Account;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Common\Repositories\Repository;

interface Accounts extends Repository
{
    public function getByBookAndTitle(Book $book, $title, $field = 'title');

    public function save(Account $account);

    public function delete(Account $account);
}
