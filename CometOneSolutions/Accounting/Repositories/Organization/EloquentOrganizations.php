<?php

namespace CometOneSolutions\Accounting\Repositories\Organization;

use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentOrganizations extends EloquentRepository implements Organizations
{
    public function __construct(Organization $organization)
    {
        parent::__construct($organization);
    }

    public function save(Organization $organization)
    {
        $organization->save();
        return $organization;
    }

    public function delete(Organization $organization)
    {
        return $organization->delete();
    }
}
