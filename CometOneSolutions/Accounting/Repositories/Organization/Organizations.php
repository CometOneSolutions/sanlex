<?php

namespace CometOneSolutions\Accounting\Repositories\Organization;

use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Common\Repositories\Repository;

interface Organizations extends Repository
{
    public function save(Organization $organization);

    public function delete(Organization $organization);
}
