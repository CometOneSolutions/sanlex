<?php

namespace CometOneSolutions\Accounting\Repositories\Currency;

use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentCurrencies extends EloquentRepository implements Currencies
{
    public function __construct(Currency $currency)
    {
        parent::__construct($currency);
    }

    public function save(Currency $currency)
    {
        $currency->save();
        return $currency;
    }

    public function delete(Currency $currency)
    {
        return $currency->delete();
    }
}
