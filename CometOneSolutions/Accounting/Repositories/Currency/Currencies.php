<?php

namespace CometOneSolutions\Accounting\Repositories\Currency;

use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Common\Repositories\Repository;

interface Currencies extends Repository
{
    public function save(Currency $currency);

    public function delete(Currency $currency);
}
