<?php

namespace CometOneSolutions\Accounting\Repositories\AccountLine;

use CometOneSolutions\Accounting\Models\AccountLine\AccountLine;
use CometOneSolutions\Common\Repositories\Repository;

interface AccountLines extends Repository
{
    public function save(AccountLine $accountLine);

    public function delete(AccountLine $accountLine);
}
