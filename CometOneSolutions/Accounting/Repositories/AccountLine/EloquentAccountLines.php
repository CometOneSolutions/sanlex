<?php

namespace CometOneSolutions\Accounting\Repositories\AccountLine;

use CometOneSolutions\Accounting\Models\AccountLine\AccountLine;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentAccountLines extends EloquentRepository implements AccountLines
{
    public function __construct(AccountLine $accountLine)
    {
        parent::__construct($accountLine);
    }

    public function save(AccountLine $accountLine)
    {
        $result = $accountLine->save();
        return $result;
    }

    public function delete(AccountLine $accountLine)
    {
        return $accountLine->delete();
    }
}
