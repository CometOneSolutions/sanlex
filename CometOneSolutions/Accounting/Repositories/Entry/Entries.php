<?php

namespace CometOneSolutions\Accounting\Repositories\Entry;

use CometOneSolutions\Accounting\Models\Entry\Entry;
use CometOneSolutions\Common\Repositories\Repository;

interface Entries extends Repository
{
    public function save(Entry $entry);

    public function delete(Entry $entry);
}
