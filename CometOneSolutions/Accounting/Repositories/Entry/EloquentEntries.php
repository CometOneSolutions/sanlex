<?php

namespace CometOneSolutions\Accounting\Repositories\Entry;

use CometOneSolutions\Accounting\Models\Entry\Entry;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentEntries extends EloquentRepository implements Entries
{
    public function __construct(Entry $entry)
    {
        parent::__construct($entry);
    }

    public function save(Entry $entry)
    {
        $result = $entry->save();
        // TODO here remove in transaction model 'entry' attribute
        $entry->transactions()->sync($entry->getTransactions());
        return $entry;
    }

    public function delete(Entry $entry)
    {
        return $entry->delete();
    }
}
