<?php

namespace CometOneSolutions\Accounting\Repositories\Transaction;

use CometOneSolutions\Accounting\Models\Transaction\Transaction;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentTransactions extends EloquentRepository implements Transactions
{
    public function __construct(Transaction $transaction)
    {
        parent::__construct($transaction);
    }

    protected function orderByEntryDate($direction)
    {
        return $this->model->join('acct_entries', 'acct_transactions.entry_id', 'acct_entries.id')->orderBy('acct_entries.date', $direction);
    }

    public function save(Transaction $transaction)
    {
        return $transaction->save();
    }

    public function delete(Transaction $transaction)
    {
        return $transaction->delete();
    }

    public function filterByBookId($bookId, $key = 'book_id')
    {
        return $this->model->whereHas('entry', function ($entry) use ($bookId, $key) {
            return $entry->where($key, $bookId);
        });
    }
}
