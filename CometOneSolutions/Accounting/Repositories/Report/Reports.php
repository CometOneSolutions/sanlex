<?php

namespace CometOneSolutions\Accounting\Repositories\Report;

use CometOneSolutions\Accounting\Models\Report\Report;
use CometOneSolutions\Common\Repositories\Repository;

interface Reports extends Repository
{
    public function save(Report $report);

    public function delete(Report $report);
}
