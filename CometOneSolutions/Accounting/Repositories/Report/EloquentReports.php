<?php

namespace CometOneSolutions\Accounting\Repositories\Report;

use CometOneSolutions\Accounting\Models\Report\Report;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentReports extends EloquentRepository implements Reports
{
    public function __construct(Report $report)
    {
        parent::__construct($report);
    }

    public function save(Report $report)
    {
        $result = $report->save();
        $categoryIds = $this->getIdsFromCategories($report->getCategories()->all());
        $report->categories()->sync($categoryIds);
        return $result;
    }

    public function delete(Report $report)
    {
        return $report->delete();
    }

    private function getIdsFromCategories(array $categories)
    {
        return array_map(function ($category) {
            return $category->getId();
        }, $categories);
    }
}
