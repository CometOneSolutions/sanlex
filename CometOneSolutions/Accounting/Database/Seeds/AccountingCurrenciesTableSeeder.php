<?php

namespace CometOneSolutions\Accounting\Database\Seeds;

use Illuminate\Database\Seeder;
use CometOneSolutions\Accounting\Repositories\Currency\Currencies;
use CometOneSolutions\Accounting\Models\Currency\SysGen\PhilippinePeso;

class AccountingCurrenciesTableSeeder extends Seeder
{
    protected $currencies;

    public function __construct(Currencies $currencies)
    {
        $this->currencies = $currencies;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->currencies->save(new PhilippinePeso());
    }
}
