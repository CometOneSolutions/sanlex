<?php

namespace CometOneSolutions\Accounting\Database\Seeds;

use CometOneSolutions\Accounting\Models\Organization\SysGen\DefaultOrganization;
use Illuminate\Database\Seeder;
use CometOneSolutions\Accounting\Repositories\Organization\Organizations;


class OrganizationsTableSeeder extends Seeder
{
    protected $organizations;

    public function __construct(Organizations $organizations)
    {
        $this->organizations = $organizations;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->organizations->save(new DefaultOrganization);
    }
}
