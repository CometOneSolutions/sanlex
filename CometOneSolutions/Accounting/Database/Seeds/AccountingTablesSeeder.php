<?php

namespace CometOneSolutions\Accounting\Database\Seeds;

use CometOneSolutions\Accounting\Models\Account\SysGen\AccountsPayable;
use CometOneSolutions\Accounting\Models\Account\SysGen\AccountsReceivable;
use CometOneSolutions\Accounting\Models\Account\SysGen\Expenses;
use CometOneSolutions\Accounting\Models\Account\SysGen\Inventory;
use CometOneSolutions\Accounting\Models\Account\SysGen\OwnerCapital;
use CometOneSolutions\Accounting\Models\Account\SysGen\OwnerDraws;
use CometOneSolutions\Accounting\Models\Account\SysGen\Salary;
use CometOneSolutions\Accounting\Models\Account\SysGen\Sales;
use CometOneSolutions\Accounting\Models\Account\SysGen\UndepositedChecks;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Currency\SysGen\PhilippinePeso;
use CometOneSolutions\Accounting\Models\Currency\SysGen\UnitedStatesDollar;
use CometOneSolutions\Accounting\Models\NormalBalance;
use CometOneSolutions\Accounting\Models\Organization\SysGen\DefaultOrganization;
use CometOneSolutions\Accounting\Repositories\Account\Accounts;
use CometOneSolutions\Accounting\Repositories\Book\Books;
use CometOneSolutions\Accounting\Repositories\Currency\Currencies;
use CometOneSolutions\Accounting\Repositories\Organization\Organizations;
use CometOneSolutions\Accounting\Services\Category\CategoryRecordService;
use CometOneSolutions\Accounting\Services\Config\ConfigRecordService;
use CometOneSolutions\Accounting\Services\Report\ReportRecordService;
use Illuminate\Database\Seeder;

class AccountingTablesSeeder extends Seeder
{
    protected $organizations;
    protected $currencies;
    protected $books;
    protected $book;
    protected $accounts;
    protected $categoryRecordService;
    protected $reportRecordService;
    protected $configRecordService;

    public function __construct(
        Organizations $organizations,
        Currencies $currencies,
        Books $books,
        Book $book,
        Accounts $accounts,
        CategoryRecordService $categoryRecordService,
        ReportRecordService $reportRecordService,
        ConfigRecordService $configRecordService
    ) {
        $this->organizations = $organizations;
        $this->currencies = $currencies;
        $this->books = $books;
        $this->book = $book;
        $this->accounts = $accounts;
        $this->categoryRecordService = $categoryRecordService;
        $this->reportRecordService = $reportRecordService;
        $this->configRecordService = $configRecordService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $organization = $this->organizations->save(new DefaultOrganization);

        $php = $this->currencies->save(new PhilippinePeso);
        $usd = $this->currencies->save(new UnitedStatesDollar);
        $config = $this->configRecordService->getConfig();

        $this->configRecordService->setCurrentOrganization($config, $organization);

        $usdBook = $this->book::new($organization, $usd);
        $phpBook = $this->book::new($organization, $php);

        $this->books->save($phpBook);
        $this->books->save($usdBook);
        $this->configRecordService->setCurrentBook($config, $phpBook);
        // Seed Accounts
        $sales = $this->accounts->save(new Sales($phpBook));
        $salary = $this->accounts->save(new Salary($phpBook));
        $accountsReceivable = $this->accounts->save(new AccountsReceivable($phpBook));
        $accountsPayable = $this->accounts->save(new AccountsPayable($phpBook));
        $undepositedChecks = $this->accounts->save(new UndepositedChecks($phpBook));
        $expenses = $this->accounts->save(new Expenses($phpBook));
        $inventory = $this->accounts->save(new Inventory($phpBook));
        $ownerDraws = $this->accounts->save(new OwnerDraws($phpBook));
        $ownerCapital = $this->accounts->save(new OwnerCapital($phpBook));

        // Categories
        $incomeCategory = $this->categoryRecordService->create($phpBook, 'Income', NormalBalance::CREDIT);
        $expenseCategory = $this->categoryRecordService->create($phpBook, 'Expense', NormalBalance::DEBIT);
        $assetsCategory = $this->categoryRecordService->create($phpBook, 'Assets', NormalBalance::DEBIT);
        $liabilitiesCategory = $this->categoryRecordService->create($phpBook, 'Liabilities', NormalBalance::CREDIT);
        $equityCategory = $this->categoryRecordService->create($phpBook, 'Equity', NormalBalance::CREDIT);
        $retainedEarningsCategory = $this->categoryRecordService->create($phpBook, 'Retained Earnings', NormalBalance::CREDIT);

        // Attach Accounts to Categories
        $this->categoryRecordService->attachAccount($incomeCategory, $sales);
        $this->categoryRecordService->attachAccount($expenseCategory, $salary);
        $this->categoryRecordService->attachAccount($assetsCategory, $accountsReceivable);

        // Reports
        $incomeStatement = $this->reportRecordService->create($phpBook, 'Income Statement');
        $balanceSheet = $this->reportRecordService->create($phpBook, 'Balance Sheet');

        // Attach categories to income statement
        $this->reportRecordService->attachCategory($incomeStatement, $incomeCategory);
        $this->reportRecordService->attachCategory($incomeStatement, $expenseCategory);

        $this->categoryRecordService->attachCategory($retainedEarningsCategory, $equityCategory);
        $this->categoryRecordService->attachCategory($incomeCategory, $retainedEarningsCategory);
        $this->categoryRecordService->attachCategory($expenseCategory, $retainedEarningsCategory);

        $this->reportRecordService->attachCategory($balanceSheet, $assetsCategory);
        $this->reportRecordService->attachCategory($balanceSheet, $liabilitiesCategory);
        $this->reportRecordService->attachCategory($balanceSheet, $equityCategory);
    }
}
