<?php

use Faker\Generator as Faker;
use CometOneSolutions\Accounting\Models\Account\AccountModel;
use CometOneSolutions\Accounting\Models\Transaction\TransactionModel;

$factory->define(TransactionModel::class, function (Faker $faker) {
    return [
        'account_id' => function () {
            return factory(AccountModel::class)->create()->id;
        },
        'type' => ['Debit', 'Credit'][random_int(0, 1)],
        'amount' => $faker->randomDigitNotNull,
    ];
})->state(TransactionModel::class, 'debit', [
    'type' => 'Debit',
    'dr' => function (array $transaction) {
        return $transaction['amount'];
    },
])->state(TransactionModel::class, 'credit', [
    'type' => 'Credit',
    'cr' => function (array $transaction) {
        return $transaction['amount'];
    },
]);
