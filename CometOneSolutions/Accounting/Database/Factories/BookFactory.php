<?php

use Faker\Generator as Faker;
use CometOneSolutions\Accounting\Models\Book\BookModel;
use CometOneSolutions\Accounting\Models\Organization\OrganizationModel;
use CometOneSolutions\Accounting\Models\Currency\CurrencyModel;

$factory->define(BookModel::class, function (Faker $faker) {
    return [
        'currency_id' => function () {
            return factory(CurrencyModel::class)->create()->id;
        },
        'organization_id' => function () {
            return factory(OrganizationModel::class)->create()->id;
        },
        'name' => $faker->unique()->word,
    ];
});
