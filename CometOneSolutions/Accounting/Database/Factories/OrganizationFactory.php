<?php

use Faker\Generator as Faker;
use CometOneSolutions\Accounting\Models\Organization\OrganizationModel;

$factory->define(OrganizationModel::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->company,
    ];
});
