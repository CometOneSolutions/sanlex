<?php

use Faker\Generator as Faker;
use CometOneSolutions\Accounting\Models\Book\BookModel;
use CometOneSolutions\Accounting\Models\Entry\EntryModel;

$factory->define(EntryModel::class, function (Faker $faker) {
    return [
        'date' => $faker->date,
        'book_id' => function () {
            return factory(BookModel::class)->create()->id;
        },
        'notes' => $faker->sentence,
    ];
});
