<?php

use Faker\Generator as Faker;
use CometOneSolutions\Accounting\Models\Currency\CurrencyModel;

$factory->define(CurrencyModel::class, function (Faker $faker) {
    $currency = $faker->unique()->currencyCode;
    return [
        'code' => $currency,
        'description' => $currency,
    ];
});
