<?php

use Faker\Generator as Faker;
use CometOneSolutions\Accounting\Models\Account\AccountModel;
use CometOneSolutions\Accounting\Models\Book\BookModel;

$factory->define(AccountModel::class, function (Faker $faker) {
    return [
        'book_id' => function () {
            return factory(BookModel::class)->create()->id;
        },
        'title' => $faker->unique()->word,
        'normal_balance' => ['Debit', 'Credit'][random_int(0,1)],
        'dr_balance' => 0,
        'cr_balance' => 0,
    ];
})->state(AccountModel::class, 'debit', [
    'normal_balance' => 'Debit',
])->state(AccountModel::class, 'credit', [
    'normal_balance' => 'Credit',
]);
