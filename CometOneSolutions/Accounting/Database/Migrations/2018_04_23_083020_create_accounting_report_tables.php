<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingReportTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Categories
        Schema::create('acct_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->enum('normal_balance', ['Debit', 'Credit']);
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('acct_books')->onDelete('restrict');
            $table->integer('parent_category_id')->unsigned()->nullable();
            $table->foreign('parent_category_id')->references('id')->on('acct_categories')->onDelete('restrict');
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });

        // Accounts_Categories
        Schema::create('acct_account_acct_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('acct_categories')->onDelete('cascade');
            $table->integer('account_id')->unsigned();
            $table->foreign('account_id')->references('id')->on('acct_accounts')->onDelete('restrict');
            $table->timestamps();
        });

        // Reports
        Schema::create('acct_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('acct_books')->onDelete('restrict');
            $table->string('description');
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });

        // Categories_Reports
        Schema::create('acct_category_acct_report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('acct_categories')->onDelete('restrict');
            $table->integer('report_id')->unsigned();
            $table->foreign('report_id')->references('id')->on('acct_reports')->onDelete('cascade');
            $table->timestamps();
        });

        // Rendered Reports
        Schema::create('acct_rendered_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_id')->unsigned();
            $table->foreign('report_id')->references('id')->on('acct_reports');
            $table->dateTime('date_from')->nullable();
            $table->dateTime('date_to')->nullable();
            $table->decimal('dr', 15, 4)->nullable();
            $table->decimal('cr', 15, 4)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });

        // Category Lines
        Schema::create('acct_category_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('acct_categories');
            $table->integer('rendered_report_id')->unsigned()->nullable();
            $table->foreign('rendered_report_id')->references('id')->on('acct_rendered_reports');
            $table->integer('parent_category_line_id')->unsigned()->nullable();
            $table->foreign('parent_category_line_id')->references('id')->on('acct_category_lines');
            $table->decimal('dr', 15, 4)->nullable();
            $table->decimal('cr', 15, 4)->nullable();
            $table->timestamps();
        });

        // Account Lines
        Schema::create('acct_account_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->unsigned();
            $table->foreign('account_id')->references('id')->on('acct_accounts')->onDelete('restrict');
            $table->integer('category_line_id')->unsigned();
            $table->foreign('category_line_id')->references('id')->on('acct_category_lines')->onDelete('restrict');
            $table->decimal('dr', 15, 4)->nullable();
            $table->decimal('cr', 15, 4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acct_account_lines');
        Schema::dropIfExists('acct_category_lines');
        Schema::dropIfExists('acct_rendered_reports');
        Schema::dropIfExists('acct_category_acct_report');
        Schema::dropIfExists('acct_reports');
        Schema::dropIfExists('acct_account_acct_category');
        Schema::dropIfExists('acct_categories');
    }
}
