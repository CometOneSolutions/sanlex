<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acct_organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });

        Schema::create('acct_currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('description')->unique();
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });

        Schema::create('acct_books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('organization_id')->unsigned();
            $table->foreign('organization_id')->references('id')->on('acct_organizations');
            $table->integer('currency_id')->unsigned();
            $table->foreign('currency_id')->references('id')->on('acct_currencies');
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->unique(['name', 'organization_id', 'currency_id']);
            $table->timestamps();
        });

        // Accounts
        Schema::create('acct_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->enum('normal_balance', ['Debit', 'Credit']);
            $table->decimal('dr_balance', 15, 4);
            $table->decimal('cr_balance', 15, 4);
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('acct_books');
            $table->unique(['title', 'book_id']);
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });

        // Entries
        Schema::create('acct_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->string('notes')->nullable();
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('acct_books')->onDelete('restrict');
            $table->string('accountable_type')->nullable();
            $table->integer('accountable_id')->unsigned()->nullable();
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });

        // Transactions
        Schema::create('acct_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('dr', 15, 4)->nullable();
            $table->decimal('cr', 15, 4)->nullable();
            $table->decimal('amount', 15, 4)->nullable();
            $table->enum('type', ['Debit', 'Credit']);
            $table->integer('entry_id')->unsigned();
            $table->foreign('entry_id')->references('id')->on('acct_entries')->onDelete('cascade');
            $table->integer('account_id')->unsigned();
            $table->foreign('account_id')->references('id')->on('acct_accounts')->onDelete('restrict');
        });

        // Config
        Schema::create('acct_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('current_organization_id')->unsigned();
            $table->foreign('current_organization_id')->references('id')->on('acct_organizations');
            $table->integer('current_book_id')->unsigned();
            $table->foreign('current_book_id')->references('id')->on('acct_books');
            $table->integer('updated_by_user_id')->unsigned()->nullable();
            $table->foreign('updated_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acct_configs');
        Schema::dropIfExists('acct_transactions');
        Schema::dropIfExists('acct_entries');
        Schema::dropIfExists('acct_accounts');
        Schema::dropIfExists('acct_books');
        Schema::dropIfExists('acct_currencies');
        Schema::dropIfExists('acct_organizations');
    }
}
