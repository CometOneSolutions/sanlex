<?php

namespace CometOneSolutions\Accounting\Requests;

use CometOneSolutions\Accounting\Services\Book\BookRecordService;
use CometOneSolutions\Accounting\Services\Category\CategoryRecordService;
use Dingo\Api\Http\FormRequest;
use \DateTime;

class StoreReport extends FormRequest
{
    protected $bookRecordService;
    protected $categoryRecordService;

    public function __construct(
        BookRecordService $bookRecordService,
        CategoryRecordService $categoryRecordService
        )
    {
        $this->bookRecordService = $bookRecordService;
        $this->categoryRecordService = $categoryRecordService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function getDescription()
    {
        return $this->input('description');
    }

    public function getBook()
    {
        return $this->bookRecordService->getById($this->input('bookId'));
    }

    public function getCategories($fieldName = 'categories')
    {
        return array_map(function ($accountAttributes) {
            $category = $this->categoryRecordService->getById($accountAttributes['categoryId']);
            return $category;
        }, $this->input($fieldName));
    }
}
