<?php

namespace CometOneSolutions\Accounting\Requests;

use CometOneSolutions\Accounting\Services\Report\ReportRecordService;
use Dingo\Api\Http\FormRequest;

class RenderReport extends FormRequest
{
    protected $reportRecordService;

    public function __construct(ReportRecordService $reportRecordService)
    {
        $this->reportRecordService = $reportRecordService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function getReport()
    {
        return $this->reportRecordService->getById($this->route('reportId'));
    }
}
