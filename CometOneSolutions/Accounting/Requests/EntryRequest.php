<?php

namespace CometOneSolutions\Accounting\Requests;

use CometOneSolutions\Accounting\Services\Account\AccountRecordService;
use CometOneSolutions\Accounting\Services\Transaction\TransactionRecordService;
use Dingo\Api\Http\FormRequest;
use \DateTime;

abstract class EntryRequest extends FormRequest
{
    protected $transactionRecordService;
    protected $accountRecordService;

    public function __construct(TransactionRecordService $transactionRecordService, AccountRecordService $accountRecordService)
    {
        $this->transactionRecordService = $transactionRecordService;
        $this->accountRecordService = $accountRecordService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function getNotes()
    {
        return $this->input('notes') ?? null;
    }

    public function getDate($index = 'date')
    {
        return new DateTime($this->input($index));
    }

    public function getTransactions($index = 'transactions')
    {
        return array_map(function ($transactionAttributes) {
            $account = $this->accountRecordService->getById($transactionAttributes['accountId']);
            $type = $transactionAttributes['type'];
            $amount = $transactionAttributes['amount'];
            if (array_key_exists('id', $transactionAttributes)) {
                $transaction = $this->transactionRecordService->getById($transactionAttributes['id']);
                $transaction->setValue2($type, $amount);
                $transaction->setAccount($account);
                return $transaction;
            }
            return $this->transactionRecordService->make2($account, $type, $amount);
        }, $this->input($index));
    }
}
