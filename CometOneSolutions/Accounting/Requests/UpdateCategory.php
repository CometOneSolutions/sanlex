<?php

namespace CometOneSolutions\Accounting\Requests;

use CometOneSolutions\Accounting\Services\Account\AccountRecordService;
use CometOneSolutions\Accounting\Services\Book\BookRecordService;
use CometOneSolutions\Accounting\Services\Category\CategoryRecordService;
use Dingo\Api\Http\FormRequest;
use \DateTime;

class UpdateCategory extends FormRequest
{
    protected $accountRecordService;
    protected $bookRecordService;
    protected $categoryRecordService;

    public function __construct(
        AccountRecordService $accountRecordService,
        BookRecordService $bookRecordService,
        CategoryRecordService $categoryRecordService
        )
    {
        $this->accountRecordService = $accountRecordService;
        $this->bookRecordService = $bookRecordService;
        $this->categoryRecordService = $categoryRecordService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function getName()
    {
        return $this->input('name');
    }

    public function getNormalBalance()
    {
        return $this->input('normalBalance');
    }

    public function getBook()
    {
        $bookId = $this->input('bookId');
        if ($bookId !=null){
            return $this->bookRecordService->getById($bookId);
        }
        return null;

    }

    public function getParentCategory()
    {
        $parentId = $this->input('parentCategoryId');
        if ($parentId!=null) {
            return $this->categoryRecordService->getById($parentId);
        }
        return null;
    }

    public function getAccounts($fieldName = 'accounts')
    {

        return array_map(function ($accountAttributes) {
            $account = $this->accountRecordService->getById($accountAttributes['accountId']);
            return $account;
        }, $this->input($fieldName));
    }
}
