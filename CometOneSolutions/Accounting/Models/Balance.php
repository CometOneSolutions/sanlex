<?php

namespace CometOneSolutions\Accounting\Models;

class Balance
{
    private $debitAmount;
    private $creditAmount;

    public function __construct($debitAmount, $creditAmount)
    {
        $this->debitAmount = $debitAmount;
        $this->creditAmount = $creditAmount;
    }

    public function getDebitAmount()
    {
        return $this->debitAmount;
    }

    public function getCreditAmount()
    {
        return $this->creditAmount;
    }

    public function getDebitValue()
    {
        return new TransactionValue(NormalBalance::DEBIT, $this->debitAmount);
    }

    public function getCreditValue()
    {
        return new TransactionValue(NormalBalance::CREDIT, $this->creditAmount);
    }

    public function getValue($normalBalance = null)
    {
        if (!$normalBalance) {
            return $this->getNormalValue();
        }

        $amount = 0;

        if ($normalBalance === NormalBalance::DEBIT) {
            $amount = $this->debitAmount - $this->creditAmount;
        } elseif ($normalBalance === NormalBalance::CREDIT) {
            $amount = $this->creditAmount - $this->debitAmount;
        }

        return new TransactionValue($normalBalance, $amount);
    }

    protected function getNormalValue()
    {
        $amount = 0;
        $normalBalance = NormalBalance::ZERO;

        if ($this->debitAmount > $this->creditAmount) {
            $normalBalance = NormalBalance::DEBIT;
            $amount = $this->debitAmount - $this->creditAmount;
        } elseif ($this->debitAmount < $this->creditAmount) {
            $normalBalance = NormalBalance::CREDIT;
            $amount = $this->creditAmount - $this->debitAmount;
        }

        return new TransactionValue($normalBalance, $amount);
    }
}
