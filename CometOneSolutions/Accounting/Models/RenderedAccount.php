<?php

namespace CometOneSolutions\Accounting\Models;

use CometOneSolutions\Accounting\Models\Account\Account;

class RenderedAccount
{
    private $account;
    private $balance;

    public function __construct(Account $account)
    {
        $this->account = $account;
        $this->balance = $account->computeBalance();
    }

    public function getId()
    {
        return $this->account->getId();
    }

    public function getTitle()
    {
        return $this->account->getTitle();
    }

    public function getNormalBalance()
    {
        return $this->account->getNormalBalance();
    }

    public function getBalance()
    {
        return $this->balance;
    }
}
