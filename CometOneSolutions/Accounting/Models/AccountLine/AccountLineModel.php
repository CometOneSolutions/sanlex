<?php

namespace CometOneSolutions\Accounting\Models\AccountLine;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Account\AccountModel;
use CometOneSolutions\Accounting\Models\CategoryLine\CategoryLineModel;
use CometOneSolutions\Common\Models\C1Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class AccountLineModel extends C1Model implements AccountLine
{
    use HasUpdatedByUser;

    protected $table = 'acct_account_lines';

    public function account()
    {
        return $this->belongsTo(AccountModel::class, 'account_id');
    }

    public function categoryLine()
    {
        return $this->belongsTo(CategoryLineModel::class, 'category_line_id');
    }

    public function getCategoryLine()
    {
        return $this->categoryLine;
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function setAccount(Account $account)
    {
        $this->account()->associate($account);
        $this->account_id = $account->getId();
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDr()
    {
        return $this->dr;
    }

    public function setDr($value)
    {
        $this->dr = $value;
        return $this;
    }

    public function getCr()
    {
        return $this->cr;
    }

    public function setCr($value)
    {
        $this->cr = $value;
        return $this;
    }
}
