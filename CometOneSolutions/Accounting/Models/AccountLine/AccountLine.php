<?php

namespace CometOneSolutions\Accounting\Models\AccountLine;
use CometOneSolutions\Auth\Models\UpdatableByUser;

interface AccountLine extends UpdatableByUser
{
    public function getId();

    public function getDr();

    public function setDr($value);

    public function getCr();

    public function setCr($value);

}
