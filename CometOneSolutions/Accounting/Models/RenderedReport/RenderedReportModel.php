<?php

namespace CometOneSolutions\Accounting\Models\RenderedReport;

use CometOneSolutions\Accounting\Models\Balance;
use CometOneSolutions\Accounting\Models\CategoryLine\CategoryLineModel;
use CometOneSolutions\Accounting\Models\Report\Report;
use CometOneSolutions\Accounting\Models\Report\ReportModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class RenderedReportModel extends Model implements RenderedReport
{
    use HasUpdatedByUser;

    protected $table = 'acct_rendered_reports';

    protected $categoryLinesToSet = null;

    public function report()
    {
        return $this->belongsTo(ReportModel::class, 'report_id');
    }

    public function categoryLines()
    {
        return $this->hasMany(CategoryLineModel::class, 'rendered_report_id');
    }

    //

    public function setCategoryLines(array $categoryLines)
    {
        $this->categoryLinesToSet = $categoryLines;
        $this->setBalance($this->computeBalanceFromCategoryLines($categoryLines));
        return $this;
    }

    protected function setBalance(Balance $balance)
    {
        $this->setDr($balance->getDebitAmount());
        $this->setCr($balance->getCreditAmount());
        return $this;
    }

    public function getBalance()
    {
        return new Balance($this->getDr(), $this->getCr());
    }

    protected function computeBalanceFromCategoryLines(array $categoryLines)
    {
        return array_reduce($categoryLines, function ($balance, $categoryLine) {
            $drAmount = $balance->getDebitAmount() + $categoryLine->getDr();
            $crAmount = $balance->getCreditAmount() + $categoryLine->getCr();
            return new Balance($drAmount, $crAmount);
        }, new Balance(0, 0));
    }

    public function getCategoryLines()
    {
        if ($this->categoryLinesToSet !== null) {
            return collect($this->categoryLinesToSet);
        }
        return $this->categoryLines;
    }

    public function setReport(Report $report)
    {
        $this->report()->associate($report);
        $this->report_id = $report->getId();
        return $this;
    }

    public function getReport()
    {
        return $this->report;
    }

    public function getReportId()
    {
        return $this->getReport()->getId();
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function setNotes($value)
    {
        $this->notes = $value;
        return $this;
    }

    public function getDateFrom()
    {
        return $this->date_from;
    }

    public function setDateFrom(DateTime $value)
    {
        $this->date_from = $value;
        return $this;
    }

    public function getDateTo()
    {
        return $this->date_to;
    }

    public function setDateTo(DateTime $value)
    {
        $this->date_to = $value;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDr()
    {
        return $this->dr;
    }

    protected function setDr($value)
    {
        $this->dr = $value;
        return $this;
    }

    public function getCr()
    {
        return $this->cr;
    }

    protected function setCr($value)
    {
        $this->cr = $value;
        return $this;
    }
}
