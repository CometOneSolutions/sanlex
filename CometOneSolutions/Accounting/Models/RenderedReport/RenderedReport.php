<?php

namespace CometOneSolutions\Accounting\Models\RenderedReport;

use CometOneSolutions\Auth\Models\UpdatableByUser;
use DateTime;

interface RenderedReport extends UpdatableByUser
{
    public function getId();

    public function getNotes();

    public function setNotes($value);

    public function getDateFrom();

    public function setDateFrom(DateTime $value);

    public function getDateTo();

    public function setDateTo(DateTime $value);

    public function getDr();

    public function getCr();

    public function getBalance();
}
