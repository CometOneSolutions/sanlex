<?php

namespace CometOneSolutions\Accounting\Models\Category;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Account\AccountModel;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Book\BookModel;
use CometOneSolutions\Accounting\Models\Report\ReportModel;
use CometOneSolutions\Common\Models\C1Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class CategoryModel extends C1Model implements Category
{
    use HasUpdatedByUser;

    protected $table = 'acct_categories';

    protected $accountsToSet = null;

    public function reports()
    {
        return $this->belongsToMany(ReportModel::class, 'acct_category_acct_report', 'category_id', 'report_id')
            ->withTimestamps();
    }

    public function book()
    {
        return $this->belongsTo(BookModel::class);
    }

    public function parentCategory()
    {
        // TODO confirm
        return $this->belongsTo(CategoryModel::class, 'parent_category_id');
    }

    public function childCategories()
    {
        // TODO confirm
        return $this->hasMany(CategoryModel::class, 'parent_category_id');
    }

    public function accounts()
    {
        return $this->belongsToMany(AccountModel::class, 'acct_account_acct_category', 'category_id', 'account_id')
            ->withTimestamps();
    }

    public function getAccounts()
    {
        if ($this->accountsToSet !== null) {
            return collect($this->accountsToSet);
        }
        return $this->accounts;
    }

    public function attachAccount(Account $account)
    {
        if ($this->accountsToSet === null) {
            $this->accountsToSet = $this->getAccounts()->all();
            $this->accountsToSet[] = $account;
        } else {
            $this->accountsToSet[] = $account;
        }
        return $this;
    }

    public function setAccounts(array $accounts)
    {
        $this->accountsToSet = $accounts;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBook()
    {
        return $this->book;
    }

    public function setBook(Book $book)
    {
        $this->book()->associate($book);
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setNormalBalance($normalBalance)
    {
        $this->normal_balance = $normalBalance;
        return $this;
    }

    public function getNormalBalance()
    {
        return $this->normal_balance;
    }

    public function getParentCategory()
    {
        // TODO temp
        return $this->parentCategory;
    }

    public function getChildCategories()
    {
        return $this->childCategories;
    }

    public function setParentCategory(Category $category)
    {
        $this->parentCategory()->associate($category);
        return $this;
    }

    public function getBookId()
    {
        return $this->book_id;
    }

    public function getParentCategoryId()
    {
        return $this->parent_category_id;
    }
}
