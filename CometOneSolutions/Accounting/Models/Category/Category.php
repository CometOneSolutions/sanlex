<?php

namespace CometOneSolutions\Accounting\Models\Category;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Auth\Models\UpdatableByUser;

interface Category extends UpdatableByUser
{
    public function getId();

    public function getBook();

    public function setBook(Book $book);

    public function attachAccount(Account $account);

    public function setAccounts(array $accounts);

    public function getName();

    public function setName($name);

    public function setNormalBalance($normalBalance);

    public function getNormalBalance();

    public function setParentCategory(Category $category);

    public function getParentCategory();

    public function getChildCategories();

    public function getBookId();

    public function getParentCategoryId();
}
