<?php

namespace CometOneSolutions\Accounting\Models\Report;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Category\Category;
use CometOneSolutions\Auth\Models\UpdatableByUser;

interface Report extends UpdatableByUser
{
    public function getId();

    public function attachCategory(Category $category);

    public function setBook(Book $book);

    public function setDescription($description);

    public function getBook();

    public function getDescription();

    public function getCategories();

    public function getBookId();
}
