<?php

namespace CometOneSolutions\Accounting\Models\Report;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Book\BookModel;
use CometOneSolutions\Accounting\Models\Category\Category;
use CometOneSolutions\Accounting\Models\Category\CategoryModel;
use CometOneSolutions\Accounting\Models\RenderedReport\RenderedReportModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class ReportModel extends Model implements Report
{
    use HasUpdatedByUser;

    protected $table = 'acct_reports';

    protected $dates = [
        'date_from',
        'date_to'
    ];

    protected $categoriesToSet = null;

    public function categories()
    {
        return $this->belongsToMany(CategoryModel::class, 'acct_category_acct_report', 'report_id', 'category_id')
            ->withTimestamps();
    }

    public function renderedReports()
    {
        return $this->hasMany(RenderedReportModel::class, 'report_id');
    }

    public function getRenderedReports()
    {
        return $this->renderedReports;
    }

    public function attachCategory(Category $category)
    {
        if ($this->categoriesToSet === null) {
            $this->categoriesToSet = $this->getCategories()->all();
            $this->categoriesToSet[] = $category;
        } else {
            $this->categoriesToSet[] = $category;
        }
        return $this;
    }

    public function getCategories()
    {
        if ($this->categoriesToSet !== null) {
            return collect($this->categoriesToSet);
        }

        return $this->categories;
    }

    public function setCategories(array $categories)
    {
        $this->categoriesToSet = $categories;
        return $this;
    }

    public function book()
    {
        return $this->belongsTo(BookModel::class);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setBook(Book $book)
    {
        $this->book()->associate($book);
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getBook()
    {
        return $this->book;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getBookId()
    {
        return $this->book_id;
    }
}
