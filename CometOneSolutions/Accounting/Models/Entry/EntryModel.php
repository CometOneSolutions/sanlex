<?php

namespace CometOneSolutions\Accounting\Models\Entry;

use CometOneSolutions\Accounting\Accountable;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Book\BookModel;
use CometOneSolutions\Accounting\Models\NormalBalance;
use CometOneSolutions\Accounting\Models\Transaction\Transaction;
use CometOneSolutions\Accounting\Models\Transaction\TransactionModel;
use \DateTime;
use CometOneSolutions\Common\Models\C1Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class EntryModel extends C1Model implements Entry
{
    use HasUpdatedByUser;

    protected $table = 'acct_entries';

    protected $dates = ['date'];

    private $transactionsToSet = null;

    public function book()
    {
        return $this->belongsTo(BookModel::class);
    }

    public function transactions()
    {
        return $this->hasMany(TransactionModel::class, 'entry_id');
    }

    public function accountable()
    {
        return $this->morphTo();
    }

    public function setAccountable(Accountable $accountable)
    {
        $this->accountable()->associate($accountable);
        return $this;
    }

    public function getAccountable()
    {
        return $this->accountable;
    }

    public function isBalanced()
    {
        $balance[NormalBalance::DEBIT] = 0;
        $balance[NormalBalance::CREDIT] = 0;

        foreach ($this->getTransactions() as $transaction) {
            $accountNormalBalance = $transaction->getAccount()->getNormalBalance();
            $transactionType = $transaction->getType();
            $multiplier = strcasecmp($accountNormalBalance, $transactionType) == 0 ? 1 : -1;
            $balance[$accountNormalBalance] += $multiplier * $transaction->getAmount();
        }

        return $balance[NormalBalance::DEBIT] == $balance[NormalBalance::CREDIT];
    }

    public static function new(Book $book, DateTime $date, array $transactions, $notes)
    {
        $newEntry = new static;
        $newEntry->setBook($book);
        $newEntry->setDate($date);
        $newEntry->setTransactions($transactions);
        $newEntry->setNotes($notes);
        return $newEntry;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDate(DateTime $date)
    {
        $this->date = $date;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getTransactions()
    {
        if ($this->transactionsToSet !== null) {
            return collect($this->transactionsToSet);
        }
        return $this->transactions;
    }

    public function setTransactions(array $transactions)
    {
        // foreach ($transactions as $transaction) {
        //     $transaction->setEntry($this);
        // }
        $this->transactionsToSet = $transactions;
        return $this;
    }

    public function addTransaction(Transaction $transaction)
    {
        $this->transactionsToSet[] = $transaction;
        return $this;
    }

    public function getBook()
    {
        return $this->book;
    }

    public function setBook(Book $book)
    {
        $this->book()->associate($book);
        return $this;
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    public function getBookId()
    {
        return $this->book_id;
    }
}
