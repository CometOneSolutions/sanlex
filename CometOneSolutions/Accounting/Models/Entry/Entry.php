<?php

namespace CometOneSolutions\Accounting\Models\Entry;


use CometOneSolutions\Accounting\Accountable;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Transaction\Transaction;
use \DateTime;

use CometOneSolutions\Auth\Models\UpdatableByUser;

interface Entry extends UpdatableByUser
{
    public static function new(Book $book, DateTime $date, array $transactions, $notes);

    public function getId();

    public function isBalanced();

    public function setDate(DateTime $date);

    public function getDate();

    public function getBook();

    public function setBook(Book $book);

    public function getNotes();

    public function setNotes($notes);

    public function setTransactions(array $transactions);

    public function addTransaction(Transaction $transaction);

    public function setAccountable(Accountable $accountable);

    public function getAccountable();
}
