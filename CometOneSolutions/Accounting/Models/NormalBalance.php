<?php

namespace CometOneSolutions\Accounting\Models;

abstract class NormalBalance
{
    const DEBIT = 'Debit';
    const CREDIT = 'Credit';
    const ZERO = 'Zero';
}
