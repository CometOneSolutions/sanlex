<?php

namespace CometOneSolutions\Accounting\Models;

use CometOneSolutions\Accounting\Models\Category\Category;

class RenderedCategory
{
    private $category;
    private $accounts;
    private $childCategories;
    private $balance;

    public function __construct(Category $category)
    {
        $this->category = $category;

        $this->accounts = $category->getAccounts()->map(function ($account) {
            return new RenderedAccount($account);
        });

        $this->childCategories = $category->getChildCategories()->map(function ($childCategory) {
            return new RenderedCategory($childCategory);
        });

        $accountBalance = $this->accounts->reduce(function ($balance, $account) use ($category) {
            $drAmount = $balance->getDebitAmount() + $account->getBalance()->getDebitAmount();
            $crAmount = $balance->getCreditAmount() + $account->getBalance()->getCreditAmount();
            return new Balance($drAmount, $crAmount);
        }, new Balance(0, 0));

        $childCategoriesBalance = $this->childCategories->reduce(function ($balance, $childCategory) {
            $drAmount = $balance->getDebitAmount() + $childCategory->getBalance()->getDebitAmount();
            $crAmount = $balance->getCreditAmount() + $childCategory->getBalance()->getCreditAmount();
            return new Balance($drAmount, $crAmount);
        }, $accountBalance);

        $this->balance = $childCategoriesBalance;
    }

    public function getId()
    {
        return $this->category->getId();
    }

    public function getName()
    {
        return $this->category->getName();
    }

    public function getNormalBalance()
    {
        return $this->category->getNormalBalance();
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function getAccounts()
    {
        return $this->accounts;
    }
}
