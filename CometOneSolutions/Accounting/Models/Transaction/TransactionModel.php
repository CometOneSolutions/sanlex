<?php

namespace CometOneSolutions\Accounting\Models\Transaction;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Account\AccountModel;
use CometOneSolutions\Accounting\Models\Entry\Entry;
use CometOneSolutions\Accounting\Models\Entry\EntryModel;
use CometOneSolutions\Accounting\Models\TransactionValue;
use Illuminate\Database\Eloquent\Model;

class TransactionModel extends Model implements Transaction
{
    protected $table = 'acct_transactions';

    public $timestamps = false;

    public function account()
    {
        return $this->belongsTo(AccountModel::class, 'account_id');
    }

    public function entry()
    {
        return $this->belongsTo(EntryModel::class, 'entry_id');
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function setAccount(Account $account)
    {
        $this->account()->associate($account);
        return $this;
    }

    public function setValue2($type, $amount)
    {
        $this->type = $type;
        $this->amount = $amount;
        return $this;
    }

    public function setValue(TransactionValue $value)
    {
        if ($value->isDr()) {
            $this->dr = $value->getAmount();
        } elseif ($value->isCr()) {
            $this->cr = $value->getAmount();
        }
        $this->amount = $value->getAmount();
        $this->type = $value->getType();
        return $this;
    }

    public function getValue()
    {
        return new TransactionValue($this->type, $this->amount);
    }

    public function getAmount()
    {
        return (float) $this->amount;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getEntry()
    {
        return $this->entry;
    }

    public function setEntry(Entry $entry)
    {
        $this->entry()->associate($entry);
        $this->entry_id = $entry->getId();
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEntryId()
    {
        return $this->entry_id;
    }

    public function getAccountId()
    {
        return $this->account_id;
    }
}
