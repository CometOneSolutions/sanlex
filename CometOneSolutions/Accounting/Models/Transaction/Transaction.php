<?php

namespace CometOneSolutions\Accounting\Models\Transaction;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Entry\Entry;
use CometOneSolutions\Accounting\Models\TransactionValue;

interface Transaction
{
    public function getAccount();

    public function setAccount(Account $account);

    public function setValue2($type, $amount);

    public function setValue(TransactionValue $value);

    public function getValue();

    public function getAmount();

    public function getType();

    public function getEntry();

    public function setEntry(Entry $entry);

    public function getId();

    public function getEntryId();

    public function getAccountId();
}
