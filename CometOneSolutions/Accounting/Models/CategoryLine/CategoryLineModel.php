<?php

namespace CometOneSolutions\Accounting\Models\CategoryLine;

use CometOneSolutions\Accounting\Models\AccountLine\AccountLineModel;
use CometOneSolutions\Accounting\Models\Category\Category;
use CometOneSolutions\Accounting\Models\Category\CategoryModel;
use CometOneSolutions\Accounting\Models\RenderedReport\RenderedReportModel;
use CometOneSolutions\Common\Models\C1Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class CategoryLineModel extends C1Model implements CategoryLine
{
    use HasUpdatedByUser;

    protected $table = 'acct_category_lines';

    protected $accountLinesToSet = null;
    protected $childCategoryLinesToSet = null;

    public function category()
    {
        return $this->belongsTo(CategoryModel::class, 'category_id');
    }

    public function renderedReport()
    {
        return $this->belongsTo(RenderedReportModel::class, 'rendered_report_id');
    }

    public function parentCategoryLine()
    {
        // return $this->belongsTo(CategoryModel::class, 'parent_category_line_id');
        return $this->belongsTo(CategoryLineModel::class, 'parent_category_line_id');
    }

    public function childCategoryLines()
    {
        return $this->hasMany(CategoryLineModel::class, 'parent_category_line_id');
    }

    public function accountLines()
    {
        return $this->hasMany(AccountLineModel::class, 'category_line_id');
    }

    //

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(Category $category)
    {
        $this->category()->associate($category);
        $this->category_id = $category->getId();
        return $this;
    }

    public function getAccountLines()
    {
        if ($this->accountLinesToSet !== null) {
            return collect($this->accountLinesToSet);
        }
        return $this->accountLines;
    }

    public function setAccountLines(array $accountLines)
    {
        $this->accountLinesToSet = $accountLines;
        return $this;
    }

    public function getChildCategoryLines()
    {
        if ($this->childCategoryLinesToSet !== null) {
            return collect($this->childCategoryLinesToSet);
        }
        return $this->childCategoryLines;
    }

    public function setChildCategoryLines(array $childCategoryLines)
    {
        $this->childCategoryLinesToSet = $childCategoryLines;
        return $this;
    }

    public function getRenderedReport()
    {
        return $this->renderedReport;
    }

    public function getParentCategoryLine()
    {
        return $this->parentCategoryLine;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDr()
    {
        return $this->dr;
    }

    public function setDr($value)
    {
        $this->dr = $value;
        return $this;
    }

    public function getCr()
    {
        return $this->cr;
    }

    public function setCr($value)
    {
        $this->cr = $value;
        return $this;
    }
}
