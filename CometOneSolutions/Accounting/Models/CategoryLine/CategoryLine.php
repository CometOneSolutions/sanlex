<?php

namespace CometOneSolutions\Accounting\Models\CategoryLine;
use CometOneSolutions\Auth\Models\UpdatableByUser;

interface CategoryLine extends UpdatableByUser
{
    public function getId();

    public function getDr();

    public function setDr($value);

    public function getCr();

    public function setCr($value);

}
