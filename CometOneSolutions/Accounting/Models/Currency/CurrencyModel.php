<?php

namespace CometOneSolutions\Accounting\Models\Currency;

use CometOneSolutions\Accounting\Models\Book\BookModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class CurrencyModel extends Model implements Currency
{
    use HasUpdatedByUser;
    protected $table = 'acct_currencies';

    public static function new($code, $description)
    {
        $newCurrency = new static;
        $newCurrency->setCode($code);
        $newCurrency->setDescription($description);
        return $newCurrency;
    }

    public function books()
    {
        return $this->hasMany(BookModel::class, 'currency_id');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBooks()
    {
        return $this->books;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }
}
