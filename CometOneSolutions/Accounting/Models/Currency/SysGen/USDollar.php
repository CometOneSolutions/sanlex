<?php

namespace CometOneSolutions\Accounting\Models\Currency\SysGen;

class USDollar extends SysGenCurrency
{
    const CODE = 'USD';
    const DESCRIPTION = 'US Dollar';
}
