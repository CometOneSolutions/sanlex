<?php

namespace CometOneSolutions\Accounting\Models\Currency\SysGen;

class UnitedStatesDollar extends SysGenCurrency
{
    const CODE = 'USD';
    const DESCRIPTION = 'United States Dollar';
}
