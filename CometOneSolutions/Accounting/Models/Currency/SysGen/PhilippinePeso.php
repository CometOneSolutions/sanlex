<?php

namespace CometOneSolutions\Accounting\Models\Currency\SysGen;

class PhilippinePeso extends SysGenCurrency
{
    const CODE = 'PHP';
    const DESCRIPTION = 'Philippine Peso';
}
