<?php

namespace CometOneSolutions\Accounting\Models\Currency\SysGen;

use CometOneSolutions\Accounting\Models\Currency\CurrencyModel;

abstract class SysGenCurrency extends CurrencyModel
{
    const CODE = '';
    const DESCRIPTION = '';

    public function __construct()
    {
        $this->setCode(static::CODE);
        $this->setDescription(static::DESCRIPTION);
    }
}
