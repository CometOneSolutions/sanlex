<?php

namespace CometOneSolutions\Accounting\Models\Currency;

use CometOneSolutions\Auth\Models\UpdatableByUser;

interface Currency extends UpdatableByUser
{
    public static function new($code, $description);

    public function getId();

    public function getBooks();

    public function getDescription();

    public function setDescription($description);

    public function setCode($code);

    public function getCode();
}
