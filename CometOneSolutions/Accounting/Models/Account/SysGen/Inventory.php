<?php

namespace CometOneSolutions\Accounting\Models\Account\SysGen;

class Inventory extends SysGenAccount
{
    const TITLE = 'Inventory';
    const NORMAL_BALANCE = NormalBalance::DEBIT;
}
