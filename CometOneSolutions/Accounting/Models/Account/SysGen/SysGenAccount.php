<?php

namespace CometOneSolutions\Accounting\Models\Account\SysGen;

use CometOneSolutions\Accounting\Models\Book\Book;

abstract class SysGenAccount extends AccountModel
{
    const TITLE = '';
    const NORMAL_BALANCE = '';

    public function __construct(Book $book)
    {
        $this->setBook($book);
        $this->setTitle(static::TITLE);
        $this->setNormalBalance(static::NORMAL_BALANCE);
        $this->initializeBalance();
    }
}
