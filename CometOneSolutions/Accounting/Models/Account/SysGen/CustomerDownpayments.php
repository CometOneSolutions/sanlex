<?php

namespace CometOneSolutions\Accounting\Models\Account\SysGen;

class CustomerDownpayments extends SysGenAccount
{
    const TITLE = 'Customer Downpayments';
    const NORMAL_BALANCE = NormalBalance::CREDIT;
}
