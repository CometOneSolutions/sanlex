<?php

namespace CometOneSolutions\Accounting\Models\Account\SysGen;

class OwnerCapital extends SysGenAccount
{
    const TITLE = 'Owner Capital';
    const NORMAL_BALANCE = NormalBalance::CREDIT;
}
