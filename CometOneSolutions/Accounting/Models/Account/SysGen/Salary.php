<?php

namespace CometOneSolutions\Accounting\Models\Account\SysGen;

class Salary extends SysGenAccount
{
    const TITLE = 'Salary';
    const NORMAL_BALANCE = NormalBalance::DEBIT;
}
