<?php

namespace CometOneSolutions\Accounting\Models\Account\SysGen;

class AccountsPayable extends SysGenAccount
{
    const TITLE = 'Accounts Payable';
    const NORMAL_BALANCE = NormalBalance::CREDIT;
}
