<?php

namespace CometOneSolutions\Accounting\Models\Account\SysGen;

class OwnerDraws extends SysGenAccount
{
    const TITLE = 'Owner Draws';
    const NORMAL_BALANCE = NormalBalance::CREDIT;
}
