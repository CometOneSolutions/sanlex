<?php

namespace CometOneSolutions\Accounting\Models\Account\SysGen;

class AccountsReceivable extends SysGenAccount
{
    const TITLE = 'Accounts Receivable';
    const NORMAL_BALANCE = NormalBalance::DEBIT;
}
