<?php

namespace CometOneSolutions\Accounting\Models\Account\SysGen;

class Sales extends SysGenAccount
{
    const TITLE = 'Sales';
    const NORMAL_BALANCE = NormalBalance::CREDIT;
}
