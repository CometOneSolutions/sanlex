<?php

namespace CometOneSolutions\Accounting\Models\Account\SysGen;

class UndepositedChecks extends SysGenAccount
{
    const TITLE = 'Undeposited Checks';
    const NORMAL_BALANCE = NormalBalance::DEBIT;
}
