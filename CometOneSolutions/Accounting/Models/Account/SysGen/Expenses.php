<?php

namespace CometOneSolutions\Accounting\Models\Account\SysGen;

class Expenses extends SysGenAccount
{
    const TITLE = 'Expenses';
    const NORMAL_BALANCE = NormalBalance::CREDIT;
}
