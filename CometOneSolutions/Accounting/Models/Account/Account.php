<?php

namespace CometOneSolutions\Accounting\Models\Account;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use \DateTime;

interface Account extends UpdatableByUser
{
    public function getId();

    public function getPaginatedTransactions();

    public function initializeBalance();

    public function getTransactions();

    public function getBook();

    public function setBook(Book $book);

    public function setTitle($title);

    public function getTitle();

    public function setNormalBalance($normalBalance);

    public function getNormalBalance();

    public function computeBalance(DateTime $dateTo = null, int $take = null);

    public function getBookId();
}
