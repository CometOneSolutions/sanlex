<?php

namespace CometOneSolutions\Accounting\Models\Account;

use CometOneSolutions\Accounting\Models\Book\Book;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use \DateTime;


class AccountModel extends Model implements Account
{
    use HasUpdatedByUser;

    protected $table = 'acct_accounts';

    public function transactions()
    {
        return $this->hasMany(TransactionModel::class, 'account_id');
    }

    public function categories()
    {
        return $this->belongsToMany(CategoryModel::class, 'account_category', 'account_id', 'category_id')
            ->withTimestamps();
    }

    public function book()
    {
        return $this->belongsTo(BookModel::class, 'book_id');
    }

//    public function internalAccounts()
//    {
//        return $this->hasMany(InternalAccountModel::class, 'account_id');
//    }

    public function getInternalAccounts()
    {
        return $this->internalAccounts;
    }

    public function getId()
    {
        return $this->id;
    }

    public function initializeBalance()
    {
        $this->dr_balance = 0.00;
        $this->cr_balance = 0.00;
        return $this;
    }

    public function getPaginatedTransactions()
    {
        return $this->transactions()->paginate();
    }

    public function getTransactions()
    {
        return $this->transactions;
    }

    public function getBook()
    {
        return $this->book;
    }

    public function getBookId()
    {
        return $this->book_id;
    }

    public function setBook(Book $book)
    {
        $this->book()->associate($book);
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setNormalBalance($normalBalance)
    {
        $this->normal_balance = $normalBalance;
        return $this;
    }

    public function getNormalBalance()
    {
        return $this->normal_balance;
    }

    public function computeBalance(DateTime $dateTo = null, int $take = null)
    {
        $transactions = $this->transactions();

        if ($dateTo !== null || $take !== null) {
            $transactions = $transactions->join('acct_entries', 'acct_transactions.entry_id', 'acct_entries.id');
        }

        if ($dateTo !== null) {
            $transactions = $transactions->where('acct_entries.date', '<=', $dateTo);
        }

        if ($take !== null) {
            $transactions = $transactions->orderBy('acct_entries.date')->take($take);
        }

        $drSum = $transactions->sum('dr');
        $crSum = $transactions->sum('cr');

        return new Balance($drSum, $crSum);
    }
}
