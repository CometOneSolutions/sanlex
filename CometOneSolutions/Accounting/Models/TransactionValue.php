<?php

namespace CometOneSolutions\Accounting\Models;

class TransactionValue
{
    private $type;
    private $amount;

    public function __construct($type, $amount)
    {
        $this->type = $type;
        $this->amount = $amount;
    }

    public function isZero()
    {
        return $this->type === TransactionType::ZERO;
    }

    public function isDr()
    {
        return $this->type === TransactionType::DEBIT;
    }

    public function isCr()
    {
        return $this->type === TransactionType::CREDIT;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getType()
    {
        return $this->type;
    }
}
