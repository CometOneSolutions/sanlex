<?php

namespace CometOneSolutions\Accounting\Models\Organization;

use CometOneSolutions\Accounting\Models\Book\BookModel;
use CometOneSolutions\Accounting\Models\Currency\Currency;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class OrganizationModel extends Model implements Organization
{
    use HasUpdatedByUser;

    protected $table = 'acct_organizations';

    public function books()
    {
        return $this->hasMany(BookModel::class, 'organization_id');
    }

    public function getCurrencyBook(Currency $currency)
    {
        return $this->books()->where('currency_id', $currency->getId())->first();
    }

    public function getBooks()
    {
        return $this->books;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}
