<?php

namespace CometOneSolutions\Accounting\Models\Organization;
use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Auth\Models\UpdatableByUser;

interface Organization extends UpdatableByUser
{
    public function getId();

    public function getCurrencyBook(Currency $currency);

    public function getName();

    public function setName($name);

    public function getBooks();
}
