<?php

namespace CometOneSolutions\Accounting\Models\Organization\SysGen;

abstract class SysGenOrganization extends OrganizationModel
{
    const NAME = '';

    public function __construct()
    {
        $this->setName(static::NAME);
    }
}
