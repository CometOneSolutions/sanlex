<?php

namespace CometOneSolutions\Accounting\Models\Organization\SysGen;

class DefaultOrganization extends SysGenOrganization
{
    const NAME = 'CometOneSolutions';
}
