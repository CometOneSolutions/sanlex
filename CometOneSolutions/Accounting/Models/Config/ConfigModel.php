<?php

namespace CometOneSolutions\Accounting\Models\Config;

use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Book\BookModel;
use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Accounting\Models\Organization\OrganizationModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class ConfigModel extends Model implements Config
{

    use HasUpdatedByUser;

    protected $table = 'acct_configs';

    public function getId()
    {
        return $this->id;
    }

    public function organization()
    {
        return $this->belongsTo(OrganizationModel::class, 'current_organization_id');
    }

    public function book()
    {
        return $this->belongsTo(BookModel::class, 'current_book_id');
    }

    public function getCurrentOrganizationId()
    {
        return $this->current_organization_id;
    }

    public function getCurrentOrganization()
    {
        return $this->organization;
    }

    public function setCurrentOrganization(Organization $value)
    {
        $this->organization()->associate($value);
        $this->current_organization_id = $value->getId();
        return $this;
    }

    public function getCurrentBookId()
    {
        return $this->current_book_id;
    }

    public function getCurrentBook()
    {
        return $this->book;
    }

    public function setCurrentBook(Book $value)
    {
        $this->book()->associate($value);
        $this->current_book_id = $value->getId();
    }
}
