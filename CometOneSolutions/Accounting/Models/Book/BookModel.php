<?php

namespace CometOneSolutions\Accounting\Models\Book;

use CometOneSolutions\Accounting\Models\Account\AccountModel;
use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Accounting\Models\Currency\CurrencyModel;
use CometOneSolutions\Accounting\Models\Entry\EntryModel;
use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Accounting\Models\Organization\OrganizationModel;
use CometOneSolutions\Accounting\Models\Transaction\TransactionModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class BookModel extends Model implements Book
{
    use HasUpdatedByUser;

    protected $table = 'acct_books';

    public function accounts()
    {
        return $this->hasMany(AccountModel::class, 'book_id');
    }

    public function currency()
    {
        return $this->belongsTo(CurrencyModel::class);
    }

    public function organization()
    {
        return $this->belongsTo(OrganizationModel::class);
    }

    public function entries()
    {
        return $this->hasMany(EntryModel::class, 'book_id');
    }

    public function transactions()
    {
        return $this->hasManyThrough(TransactionModel::class, EntryModel::class, 'book_id', 'entry_id');
    }

    public static function new(Organization $organization, Currency $currency)
    {
        $book = new static;
        $book->setOrganization($organization);
        $book->setCurrency($currency);
        $book->setName(sprintf('%s %s', $organization->getName(), $currency->getCode()));
        return $book;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAccounts()
    {
        return $this->accounts;
    }

    public function getEntries()
    {
        return $this->entries;
    }

    public function getPaginatedTransactions()
    {
        return $this->transactions()->paginate();
    }

    public function getTransactions($paginate = false)
    {
        return $this->transactions;
    }

    public function getOrganization()
    {
        return $this->organization;
    }

    public function setOrganization(Organization $organization)
    {
        $this->organization()->associate($organization);
        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency(Currency $currency)
    {
        $this->currency()->associate($currency);
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getCurrencyId()
    {
        return $this->currency_id;
    }

    public function getOrganizationId()
    {
        return $this->organization_id;
    }
}
