<?php

namespace CometOneSolutions\Accounting\Models\Book;

use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Auth\Models\UpdatableByUser;

interface Book extends UpdatableByUser
{
    public static function new(Organization $organization, Currency $currency);

    public function getPaginatedTransactions();

    public function getId();

    public function getAccounts();

    public function getOrganization();

    public function setOrganization(Organization $organization);

    public function getCurrency();

    public function setCurrency(Currency $currency);

    public function setName($name);

    public function getName();

    public function getCurrencyId();

    public function getOrganizationId();

    public function getEntries();

    public function getTransactions();
}
