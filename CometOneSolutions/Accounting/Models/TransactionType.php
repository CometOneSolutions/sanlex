<?php

namespace CometOneSolutions\Accounting\Models;

abstract class TransactionType
{
    const DEBIT = 'Debit';
    const CREDIT = 'Credit';
    const ZERO = 'Zero';
}
