<?php

namespace CometOneSolutions\Accounting\Providers;

use CometOneSolutions\Accounting\Models\Account\Account;
use CometOneSolutions\Accounting\Models\Account\AccountModel;
use CometOneSolutions\Accounting\Models\AccountLine\AccountLine;
use CometOneSolutions\Accounting\Models\AccountLine\AccountLineModel;
use CometOneSolutions\Accounting\Models\Book\Book;
use CometOneSolutions\Accounting\Models\Book\BookModel;
use CometOneSolutions\Accounting\Models\Category\Category;
use CometOneSolutions\Accounting\Models\Category\CategoryModel;
use CometOneSolutions\Accounting\Models\CategoryLine\CategoryLine;
use CometOneSolutions\Accounting\Models\CategoryLine\CategoryLineModel;
use CometOneSolutions\Accounting\Models\Config\Config;
use CometOneSolutions\Accounting\Models\Config\ConfigModel;
use CometOneSolutions\Accounting\Models\Currency\Currency;
use CometOneSolutions\Accounting\Models\Currency\CurrencyModel;
use CometOneSolutions\Accounting\Models\Entry\Entry;
use CometOneSolutions\Accounting\Models\Entry\EntryModel;
use CometOneSolutions\Accounting\Models\Organization\Organization;
use CometOneSolutions\Accounting\Models\Organization\OrganizationModel;
use CometOneSolutions\Accounting\Models\RenderedReport\RenderedReport;
use CometOneSolutions\Accounting\Models\RenderedReport\RenderedReportModel;
use CometOneSolutions\Accounting\Models\Report\Report;
use CometOneSolutions\Accounting\Models\Report\ReportModel;
use CometOneSolutions\Accounting\Models\Transaction\Transaction;
use CometOneSolutions\Accounting\Models\Transaction\TransactionModel;
use CometOneSolutions\Accounting\Repositories\Account\Accounts;
use CometOneSolutions\Accounting\Repositories\Account\EloquentAccounts;
use CometOneSolutions\Accounting\Repositories\AccountLine\AccountLines;
use CometOneSolutions\Accounting\Repositories\AccountLine\EloquentAccountLines;
use CometOneSolutions\Accounting\Repositories\Book\Books;
use CometOneSolutions\Accounting\Repositories\Book\EloquentBooks;
use CometOneSolutions\Accounting\Repositories\Category\Categories;
use CometOneSolutions\Accounting\Repositories\Category\EloquentCategories;
use CometOneSolutions\Accounting\Repositories\CategoryLine\CategoryLines;
use CometOneSolutions\Accounting\Repositories\CategoryLine\EloquentCategoryLines;
use CometOneSolutions\Accounting\Repositories\Config\Configs;
use CometOneSolutions\Accounting\Repositories\Config\EloquentConfigs;
use CometOneSolutions\Accounting\Repositories\Currency\Currencies;
use CometOneSolutions\Accounting\Repositories\Currency\EloquentCurrencies;
use CometOneSolutions\Accounting\Repositories\Entry\EloquentEntries;
use CometOneSolutions\Accounting\Repositories\Entry\Entries;
use CometOneSolutions\Accounting\Repositories\Organization\EloquentOrganizations;
use CometOneSolutions\Accounting\Repositories\Organization\Organizations;
use CometOneSolutions\Accounting\Repositories\RenderedReport\EloquentRenderedReports;
use CometOneSolutions\Accounting\Repositories\RenderedReport\RenderedReports;
use CometOneSolutions\Accounting\Repositories\Report\EloquentReports;
use CometOneSolutions\Accounting\Repositories\Report\Reports;
use CometOneSolutions\Accounting\Repositories\Transaction\EloquentTransactions;
use CometOneSolutions\Accounting\Repositories\Transaction\Transactions;
use CometOneSolutions\Accounting\Services\Account\AccountRecordService;
use CometOneSolutions\Accounting\Services\Account\AccountRecordServiceImpl;
use CometOneSolutions\Accounting\Services\Account\SysGenAccounts;
use CometOneSolutions\Accounting\Services\Account\SysGenAccountsImpl;
use CometOneSolutions\Accounting\Services\AccountLine\AccountLineRecordService;
use CometOneSolutions\Accounting\Services\AccountLine\AccountLineRecordServiceImpl;
use CometOneSolutions\Accounting\Services\Book\BookRecordService;
use CometOneSolutions\Accounting\Services\Book\BookRecordServiceImpl;
use CometOneSolutions\Accounting\Services\Category\CategoryRecordService;
use CometOneSolutions\Accounting\Services\Category\CategoryRecordServiceImpl;
use CometOneSolutions\Accounting\Services\CategoryLine\CategoryLineRecordService;
use CometOneSolutions\Accounting\Services\CategoryLine\CategoryLineRecordServiceImpl;
use CometOneSolutions\Accounting\Services\Config\ConfigRecordService;
use CometOneSolutions\Accounting\Services\Config\ConfigRecordServiceImpl;
use CometOneSolutions\Accounting\Services\Currency\CurrencyRecordService;
use CometOneSolutions\Accounting\Services\Currency\CurrencyRecordServiceImpl;
use CometOneSolutions\Accounting\Services\Entry\EntryRecordService;
use CometOneSolutions\Accounting\Services\Entry\EntryRecordServiceImpl;
use CometOneSolutions\Accounting\Services\Organization\OrganizationRecordService;
use CometOneSolutions\Accounting\Services\Organization\OrganizationRecordServiceImpl;
use CometOneSolutions\Accounting\Services\RenderedReport\RenderedReportRecordService;
use CometOneSolutions\Accounting\Services\RenderedReport\RenderedReportRecordServiceImpl;
use CometOneSolutions\Accounting\Services\Report\ReportRecordService;
use CometOneSolutions\Accounting\Services\Report\ReportRecordServiceImpl;
use CometOneSolutions\Accounting\Services\Transaction\TransactionRecordService;
use CometOneSolutions\Accounting\Services\Transaction\TransactionRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class AccountingServiceProvider extends ServiceProvider
{
    protected $dbPath = 'CometOneSolutions/Accounting/Database/';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(base_path($this->dbPath . 'factories'));

        $this->app->bind(CategoryRecordService::class, CategoryRecordServiceImpl::class);
        $this->app->bind(Categories::class, EloquentCategories::class);
        $this->app->bind(Category::class, CategoryModel::class);

        $this->app->bind(ReportRecordService::class, ReportRecordServiceImpl::class);
        $this->app->bind(Reports::class, EloquentReports::class);
        $this->app->bind(Report::class, ReportModel::class);

        $this->app->bind(OrganizationRecordService::class, OrganizationRecordServiceImpl::class);
        $this->app->bind(Organizations::class, EloquentOrganizations::class);
        $this->app->bind(Organization::class, OrganizationModel::class);

        $this->app->bind(BookRecordService::class, BookRecordServiceImpl::class);
        $this->app->bind(Books::class, EloquentBooks::class);
        $this->app->bind(Book::class, BookModel::class);

        $this->app->bind(CurrencyRecordService::class, CurrencyRecordServiceImpl::class);
        $this->app->bind(Currencies::class, EloquentCurrencies::class);
        $this->app->bind(Currency::class, CurrencyModel::class);

        $this->app->bind(AccountRecordService::class, AccountRecordServiceImpl::class);
        $this->app->bind(Accounts::class, EloquentAccounts::class);
        $this->app->bind(Account::class, AccountModel::class);

        $this->app->bind(EntryRecordService::class, EntryRecordServiceImpl::class);
        $this->app->bind(Entries::class, EloquentEntries::class);
        $this->app->bind(Entry::class, EntryModel::class);

        $this->app->bind(TransactionRecordService::class, TransactionRecordServiceImpl::class);
        $this->app->bind(Transactions::class, EloquentTransactions::class);
        $this->app->bind(Transaction::class, TransactionModel::class);

        $this->app->bind(RenderedReportRecordService::class, RenderedReportRecordServiceImpl::class);
        $this->app->bind(RenderedReports::class, EloquentRenderedReports::class);
        $this->app->bind(RenderedReport::class, RenderedReportModel::class);

        $this->app->bind(CategoryLineRecordService::class, CategoryLineRecordServiceImpl::class);
        $this->app->bind(CategoryLines::class, EloquentCategoryLines::class);
        $this->app->bind(CategoryLine::class, CategoryLineModel::class);

        $this->app->bind(AccountLineRecordService::class, AccountLineRecordServiceImpl::class);
        $this->app->bind(AccountLines::class, EloquentAccountLines::class);
        $this->app->bind(AccountLine::class, AccountLineModel::class);

        $this->app->bind(ConfigRecordService::class, ConfigRecordServiceImpl::class);
        $this->app->bind(Configs::class, EloquentConfigs::class);
        $this->app->bind(Config::class, ConfigModel::class);

        $this->app->bind(SysGenAccounts::class, SysGenAccountsImpl::class);
    }
}
