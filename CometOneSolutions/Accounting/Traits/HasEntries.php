<?php

namespace CometOneSolutions\Accounting\Traits;

use CometOneSolutions\Accounting\Models\Entry\EntryModel;

trait HasEntries
{
    public function entries()
    {
        return $this->morphMany(EntryModel::class, 'accountable');
    }

    public function getEntries()
    {
        return $this->entries;
    }

    public function hasEntries()
    {
        return $this->entries()->exists();
    }
}
