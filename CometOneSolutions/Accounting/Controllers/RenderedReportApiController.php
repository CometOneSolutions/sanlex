<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Requests\RenderReport;
use CometOneSolutions\Accounting\Services\RenderedReport\RenderedReportRecordService;
use CometOneSolutions\Accounting\Transformers\RenderedReportTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;

class RenderedReportApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        RenderedReportRecordService $renderedReportRecordService,
        RenderedReportTransformer $renderedReportTransformer
    ) {
        parent::__construct($renderedReportRecordService, $renderedReportTransformer);
        $this->middleware('auth:api');
        $this->service = $renderedReportRecordService;
        $this->transformer = $renderedReportTransformer;
    }

    public function destroy($id)
    {
        $renderedReport = $this->service->getById($id);
        $renderedReport = $this->service->delete($renderedReport);
        return $this->response->item($renderedReport, $this->transformer)->setStatusCode(200);
    }

    public function generate(RenderReport $request)
    {
        // $user = $request->user();
        $report = $request->getReport();
        $renderedReport = $this->service->generate($report);

        return $this->response->item($renderedReport, $this->transformer)->setStatusCode(201);
    }
}
