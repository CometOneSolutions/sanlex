<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Services\Account\AccountRecordService;
use CometOneSolutions\Accounting\Services\Book\BookRecordService;
use CometOneSolutions\Accounting\Transformers\AccountTransformer;
use CometOneSolutions\Accounting\Transformers\TransactionTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use Illuminate\Http\Request;


class AccountApiController extends ResourceApiController
{
    protected $accountRecordService;
    protected $bookRecordService;
    protected $transformer;
    protected $transactionTransformer;

    public function __construct(
        AccountRecordService $accountRecordService,
        BookRecordService $bookRecordService,
        AccountTransformer $transformer,
        TransactionTransformer $transactionTransformer
    ) {
        parent::__construct($accountRecordService, $transformer);
        $this->middleware('auth:api');
        $this->accountRecordService = $accountRecordService;
        $this->bookRecordService = $bookRecordService;
        $this->transformer = $transformer;
        $this->transactionTransformer = $transactionTransformer;
    }

    public function destroy($accountId)
    {
        $account = $this->accountRecordService->getById($accountId);
        $this->accountRecordService->delete($account);
        return $this->response->item($account, $this->transformer)->setStatusCode(200);
    }

    public function store(Request $request)
    {
        $user = $request->user();
        $bookId = $request->input('bookId');
        $title = $request->input('title');
        $normalBalance = $request->input('normalBalance');
        $book = $this->bookRecordService->getById($bookId);
        $account = $this->accountRecordService->create($book, $title, $normalBalance, $user);
        return $this->response->item($account, $this->transformer)->setStatusCode(201);
    }

    public function update($accountId, Request $request)
    {
        $account = $this->accountRecordService->getById($accountId);
        $title = $request->input('title');
        $user = $request->user();
        $book = null; // TODO
        $this->accountRecordService->update($account, $title, $book, $user);
        return $this->response->item($account, $this->transformer)->setStatusCode(200);
    }

    public function getTransactions($accountId)
    {
        $account = $this->accountRecordService->getById($accountId);

        $transactions = $this->accountRecordService->getPaginatedTransactions($account, self::INDEX_LIMIT);

        // Transaction count before the current page
        $prevTransactionsCount = self::INDEX_LIMIT * ($transactions->currentPage() - 1);

        $pageStartingBalanceValue = new TransactionValue($account->getNormalBalance(), 0);

        // TODO Go back to this once you figure out how to use Balance.php
        if ($prevTransactionsCount > 0) {
            $pageStartingBalanceValue = $this->accountRecordService->getBalance($account, $prevTransactionsCount);
        }

        return $this->response->paginator($transactions, $this->transactionTransformer)
            ->setMeta([
                'totals' => [
                    'startingBalance' => [
                        'type' => $pageStartingBalanceValue->getType(),
                        'amount' => $pageStartingBalanceValue->getAmount(),
                    ]
                ],
            ])->setStatusCode(200);
    }
}
