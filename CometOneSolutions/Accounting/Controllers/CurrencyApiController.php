<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Services\Currency\CurrencyRecordService;
use CometOneSolutions\Accounting\Transformers\CurrencyTransformer;
use Illuminate\Http\Request;


class CurrencyApiController extends ResourceApiController
{
    protected $currencyRecordService;
    protected $currencyTransformer;

    public function __construct(
        CurrencyRecordService $currencyRecordService,
        CurrencyTransformer $currencyTransformer
    ) {
        $this->middleware('auth:api');
        $this->currencyRecordService = $currencyRecordService;
        $this->currencyTransformer = $currencyTransformer;
        parent::__construct($currencyRecordService, $currencyTransformer);
    }

    public function destroy($currencyId)
    {
        $currency = $this->currencyRecordService->getById($currencyId);
        $this->currencyRecordService->delete($currency);
        return $this->response->item($currency, $this->currencyTransformer)->setStatusCode(200);
    }

    public function store(Request $request)
    {
        $code = $request->input('code');
        $description = $request->input('description');
        $user = $request->user();
        $currency = $this->currencyRecordService->create($code, $description, $user);
        return $this->response->item($currency, $this->currencyTransformer)->setStatusCode(201);
    }

    public function update($currencyId, Request $request)
    {
        $currency = $this->currencyRecordService->getById($currencyId);
        $code = $request->input('code');
        $description = $request->input('description');
        $user = $request->user();
        $this->currencyRecordService->update($currency, $code, $description, $user);
        return $this->response->item($currency, $this->currencyTransformer)->setStatusCode(200);
    }
}
