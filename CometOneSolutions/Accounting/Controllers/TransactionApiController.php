<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Services\Book\BookRecordService;
use CometOneSolutions\Accounting\Services\Transaction\TransactionRecordService;
use CometOneSolutions\Accounting\Transformers\AccountTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use Illuminate\Http\Request;
use CometOneSolutions\Common\Utils\Filter;

class TransactionApiController extends ResourceApiController
{
    protected $transactionRecordService;
    protected $transactionTransformer;
    protected $bookRecordService;

    public function __construct(
        TransactionRecordService $transactionRecordService,
        BookRecordService $bookRecordService,
        AccountTransformer $transactionTransformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($transactionRecordService, $transactionTransformer);
        $this->bookRecordService = $bookRecordService;
        $this->transactionRecordService = $transactionRecordService;
        $this->transformer = $transactionTransformer;
    }

    public function indexByBook($bookId, Request $request)
    {
        $book = $this->bookRecordService->getById($bookId);
        $bookFilter = new Filter('book_id', $bookId);
        return $this->transactionRecordService->getPaginated([$bookFilter]);
    }
}
