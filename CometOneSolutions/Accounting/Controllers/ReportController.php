<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Services\Report\ReportRecordService;
use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\Controller;
use CometOneSolutions\Accounting\Services\RenderedReport\RenderedReportRecordService;
use JavaScript;

class ReportController extends Controller
{

    private $service;
    protected $renderedReportRecordService;

    public function __construct(
        ReportRecordService $reportRecordService,
        RenderedReportRecordService $renderedReportRecordService
    )
    {
        $this->service = $reportRecordService;
        $this->renderedReportRecordService = $renderedReportRecordService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        JavaScript::put([
            'filterable' => $this->getDisplayedFilterable(),
            'sorter' => 'description',
            'baseUrl' => '/api/accounting/reports?include=book,renderedReports'
        ]);
        return view('report.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // to set the routes and controller paths, use a routeserviceprovider
        // to get this edit view.php => paths
        JavaScript::put(['id' => null, ]);
        return view('report.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $report = $this->service->getById($id);

        return view('report.show', compact('report'));
    }

    // public function showRenderedReport($reportId, $renderedReportId)
    // {
    //     JavaScript::put(['id' => $reportId]);
    //     $report = $this->service->getById($reportId);

    //     return view('report.show-rendered-report', compact('report'));
    // }

    public function showRenderedReport($reportId, $renderedReportId)
    {
        JavaScript::put([
            'id' => $reportId,
            'renderedReportId' => $renderedReportId
        ]);
        $report = $this->service->getById($reportId);
        $renderedReport = $this->renderedReportRecordService->getById($renderedReportId);
        return view('report.show-rendered-report', compact('report','renderedReport'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $report = $this->service->getById($id);

        return view('report.edit', compact('report'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getDisplayedFilterable()
    {
        return [
            ['id' => 'description', 'name' => 'description'],
        ];
    }
}
