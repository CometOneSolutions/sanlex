<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Requests\UpdateEntry;
use CometOneSolutions\Accounting\Services\Account\AccountRecordService;
use CometOneSolutions\Accounting\Services\Book\BookRecordService;
use CometOneSolutions\Accounting\Services\Entry\EntryRecordService;
use CometOneSolutions\Accounting\Services\Transaction\TransactionRecordService;
use CometOneSolutions\Accounting\Transformers\EntryTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use Illuminate\Http\Request;

use \DateTime;

class EntryApiController extends ResourceApiController
{
    protected $entryRecordService;
    protected $transactionRecordService;
    protected $accountRecordService;
    protected $bookRecordService;
    protected $entryTransformer;

    public function __construct(
        EntryRecordService $entryRecordService,
        BookRecordService $bookRecordService,
        TransactionRecordService $transactionRecordService,
        AccountRecordService $accountRecordService,
        EntryTransformer $entryTransformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($entryRecordService, $entryTransformer);
        $this->entryRecordService = $entryRecordService;
        $this->bookRecordService = $bookRecordService;
        $this->transactionRecordService = $transactionRecordService;
        $this->accountRecordService = $accountRecordService;
        $this->entryTransformer = $entryTransformer;
    }

    public function store(Request $request)
    {
        $user = $request->user();
        $bookId = (int)$request->input('bookId');
        $book = $this->bookRecordService->getById($bookId);
        $date = new DateTime($request->input('date'));
        $notes = $request->input('notes') ?? null;
        $transactionAttributesArray = $request->input('transactions');
        $transactions = array_map(function ($transactionAttributes) {
            $account = $this->accountRecordService->getById($transactionAttributes['accountId']);
            $type = $transactionAttributes['type'];
            $amount = $transactionAttributes['amount'];
            return $this->transactionRecordService->make2($account, $type, $amount);
        }, $transactionAttributesArray);
        // TODO
        $accountableType = null;
        $accountableId = null;
        $accountable = null;
        $entry = $this->entryRecordService->create($book, $date, $transactions, $notes, $accountable, $user);
        return $this->response->item($entry, $this->entryTransformer)->setStatusCode(201);
    }

    public function update($entryId, UpdateEntry $request)
    {
        $entry = $this->entryRecordService->getById($entryId);
        $entry = $this->entryRecordService->update(
            $entry,
            $request->getDate(),
            $request->getTransactions(),
            $request->getNotes(),
            $request->user()
        );
        return $this->response->item($entry, $this->entryTransformer)->setStatusCode(200);
    }

    public function destroy($entryId)
    {
        $entry = $this->entryRecordService->getById($entryId);
        $this->entryRecordService->delete($entry);
        return $this->response->item($entry, $this->entryTransformer)->setStatusCode(200);
    }
}
