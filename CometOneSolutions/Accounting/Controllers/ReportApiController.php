<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Requests\StoreReport;
use CometOneSolutions\Accounting\Requests\UpdateReport;
use CometOneSolutions\Accounting\Services\Report\ReportRecordService;
use CometOneSolutions\Accounting\Transformers\ReportTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;

class ReportApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        ReportRecordService $reportRecordService,
        ReportTransformer $reportTransformer
    ) {
        parent::__construct($reportRecordService, $reportTransformer);
        $this->middleware('auth:api');
        $this->service = $reportRecordService;
        $this->transformer = $reportTransformer;
    }

    public function destroy($reportId)
    {
        $report = $this->service->getById($reportId);
        $report = $this->service->delete($report);
        return $this->response->item($report, $this->transformer)->setStatusCode(200);
    }

    public function store(StoreReport $request)
    {
        $user = $request->user();
        $book = $request->getBook();
        $description = $request->getDescription();
        $categories = $request->getCategories();
        $report = $this->service->create($book, $description, $categories, $user);

        return $this->response->item($report, $this->transformer)->setStatusCode(201);
    }

    public function update($reportId, UpdateReport $request)
    {
        $user = $request->user();
        $report = $this->service->getById($reportId);
        $book = $request->getBook();
        $description = $request->getDescription();
        $categories = $request->getCategories();
        $report = $this->service->update($report, $book, $description, $categories, $user);

        return $this->response->item($report, $this->transformer)->setStatusCode(201);
    }
}
