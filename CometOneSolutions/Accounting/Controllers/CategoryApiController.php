<?php

namespace CometOneSolutions\Accounting\Controllers;



use CometOneSolutions\Accounting\Requests\StoreCategory;
use CometOneSolutions\Accounting\Requests\UpdateCategory;
use CometOneSolutions\Accounting\Services\Category\CategoryRecordService;
use CometOneSolutions\Accounting\Transformers\CategoryTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;

class CategoryApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        CategoryRecordService $categoryRecordService,
        CategoryTransformer $categoryTransformer
    ) {
        $this->middleware('auth:api');
        $this->service = $categoryRecordService;
        $this->transformer = $categoryTransformer;
        parent::__construct($categoryRecordService, $categoryTransformer);
    }

    public function destroy($categoryId)
    {
        $category = $this->service->getById($categoryId);
        $category = $this->service->delete($category);
        return $this->response->item($category, $this->transformer)->setStatusCode(200);
    }

    public function store(StoreCategory $request)
    {
        $user = $request->user();
        $book = $request->getBook();
        $name = $request->getName();
        $normalBalance = $request->getNormalBalance();
        $accounts = $request->getAccounts();
        $accounts = $request->getAccounts();
        $parentCategory = $request->getParentCategory();
        $account = $this->service->create($book, $name, $normalBalance, $accounts, $parentCategory, $user);
        return $this->response->item($account, $this->transformer)->setStatusCode(201);
    }

    public function update($categoryId, UpdateCategory $request)
    {
        $user = $request->user();
        $category = $this->service->getById($categoryId);
        $book = $request->getBook();
        $name = $request->getName();
        $normalBalance = $request->getNormalBalance();
        $accounts = $request->getAccounts();
        $accounts = $request->getAccounts();
        $parentCategory = $request->getParentCategory();
        $account = $this->service->update($category, $book, $name, $normalBalance, $accounts, $parentCategory, $user);
        return $this->response->item($account, $this->transformer)->setStatusCode(201);
    }
}
