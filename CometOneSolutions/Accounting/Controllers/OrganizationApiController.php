<?php

namespace CometOneSolutions\Accounting\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Accounting\Transformers\OrganizationTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use CometOneSolutions\Accounting\Services\Organization\OrganizationRecordService;

class OrganizationApiController extends ResourceApiController
{
    protected $organizationRecordService;
    protected $organizationTransformer;

    public function __construct(
        OrganizationRecordService $organizationRecordService,
        OrganizationTransformer $organizationTransformer
    ) {
        $this->middleware('auth:api');
        $this->organizationRecordService = $organizationRecordService;
        $this->organizationTransformer = $organizationTransformer;
        parent::__construct($organizationRecordService, $organizationTransformer);
    }

    public function destroy($organizationId)
    {
        $organization = $this->organizationRecordService->getById($organizationId);
        $this->organizationRecordService->delete($organization);
        return $this->response->item($organization, $this->organizationTransformer)->setStatusCode(200);
    }

    public function store(Request $request)
    {
        $name = $request->input('name');
        $user = $request->user();
        $organization = $this->organizationRecordService->create($name, $user);
        return $this->response->item($organization, $this->organizationTransformer)->setStatusCode(201);
    }

    public function update($organizationId, Request $request)
    {
        $organization = $this->organizationRecordService->getById($organizationId);
        $name = $request->input('name');
        $user = $request->user();
        $this->organizationRecordService->update($organization, $name, $user);
        return $this->response->item($organization, $this->organizationTransformer)->setStatusCode(200);
    }
}
