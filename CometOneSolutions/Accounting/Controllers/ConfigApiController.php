<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Services\Book\BookRecordService;
use CometOneSolutions\Accounting\Services\Config\ConfigRecordService;
use CometOneSolutions\Accounting\Services\Organization\OrganizationRecordService;
use CometOneSolutions\Accounting\Transformers\BookTransformer;
use CometOneSolutions\Accounting\Transformers\ConfigTransformer;
use CometOneSolutions\Accounting\Transformers\OrganizationTransformer;
use CometOneSolutions\Common\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;


class ConfigApiController extends Controller
{
    use Helpers;

    protected $service;
    protected $configTransformer;
    private $organizationRecordService;
    private $bookRecordService;

    public function __construct(
        ConfigRecordService $configRecordService,
        OrganizationRecordService $organizationRecordService,
        BookRecordService $bookRecordService,
        BookTransformer $bookTransformer,
        OrganizationTransformer $organizationTransformer,
        ConfigTransformer $configTransformer
    ) {
        $this->middleware('auth:api');
        $this->service = $configRecordService;
        $this->organizationRecordService = $organizationRecordService;
        $this->bookRecordService = $bookRecordService;
        $this->configTransformer = $configTransformer;
    }

    public function show()
    {
        $config = $this->service->getConfig();
        return $this->response->item($config, $this->configTransformer);
    }

    public function update(Request $request)
    {
        $config = $this->service->getConfig();
        $organization = $this->organizationRecordService->getById($request->input('currentOrganizationId'));
        $book = $this->bookRecordService->getById($request->input('currentBookId'));
        $config = $this->service->update(
            $config,
            $organization,
            $book,
            $request->user()
        );
        return $this->response->item($config, $this->configTransformer);
    }
}
