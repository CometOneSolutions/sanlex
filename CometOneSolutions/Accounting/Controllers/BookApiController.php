<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Services\Book\BookRecordService;
use CometOneSolutions\Accounting\Services\Currency\CurrencyRecordService;
use CometOneSolutions\Accounting\Services\Organization\OrganizationRecordService;
use CometOneSolutions\Accounting\Transformers\BookTransformer;
use CometOneSolutions\Accounting\Transformers\TransactionTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use Illuminate\Http\Request;


class BookApiController extends ResourceApiController
{
    protected $bookRecordService;
    protected $currencyRecordService;
    protected $organizationRecordService;
    protected $bookTransformer;
    protected $transactionTransformer;

    public function __construct(
        BookRecordService $bookRecordService,
        CurrencyRecordService $currencyRecordService,
        OrganizationRecordService $organizationRecordService,
        BookTransformer $bookTransformer,
        TransactionTransformer $transactionTransformer
    ) {
        parent::__construct($bookRecordService, $bookTransformer);
        $this->middleware('auth:api');
        $this->bookRecordService = $bookRecordService;
        $this->currencyRecordService = $currencyRecordService;
        $this->organizationRecordService = $organizationRecordService;
        $this->bookTransformer = $bookTransformer;
        $this->transactionTransformer = $transactionTransformer;
    }

    public function getTransactions($bookId)
    {
        // TODO getPaginatedTransactions Should not be in the model
        // TODO actually
        $transactions = $this->bookRecordService->getById($bookId)->getPaginatedTransactions();
        return $this->response->paginator($transactions, $this->transactionTransformer)->setStatusCode(200);
    }

    public function destroy($bookId)
    {
        $book = $this->bookRecordService->getById($bookId);
        $this->bookRecordService->delete($book);
        return $this->response->item($book, $this->bookTransformer)->setStatusCode(200);
    }

    public function store(Request $request)
    {
        $user = $request->user();
        $organizationId = $request->input('organizationId');
        $currencyId = $request->input('currencyId');
        $name = $request->input('name');
        $currency = $this->currencyRecordService->getById($currencyId);
        $organization = $this->organizationRecordService->getById($organizationId);
        $book = $this->bookRecordService->create($organization, $currency, $name, $user);
        return $this->response->item($book, $this->bookTransformer)->setStatusCode(201);
    }

    public function update($bookId, Request $request)
    {
        $user = $request->user();
        $organizationId = $request->input('organizationId');
        $currencyId = $request->input('currencyId');
        $name = $request->input('name');
        $book = $this->bookRecordService->getById($bookId);
        $currency = $this->currencyRecordService->getById($currencyId);
        $organization = $this->organizationRecordService->getById($organizationId);
        $this->bookRecordService->update($book, $organization, $currency, $name, $user);
        return $this->response->item($book, $this->bookTransformer)->setStatusCode(200);
    }
}
