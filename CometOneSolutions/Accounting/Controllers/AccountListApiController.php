<?php

namespace CometOneSolutions\Accounting\Controllers;



use CometOneSolutions\Accounting\Services\Account\AccountRecordService;
use CometOneSolutions\Accounting\Services\Book\BookRecordService;
use CometOneSolutions\Accounting\Transformers\AccountListTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;

class AccountListApiController extends ResourceApiController
{
    public function __construct(
        AccountRecordService $accountRecordService,
        BookRecordService $bookRecordService,
        AccountListTransformer $transformer
    ) {
        parent::__construct($accountRecordService, $transformer);
    }
}
