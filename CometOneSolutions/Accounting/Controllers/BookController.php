<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Services\Book\BookRecordService;
use CometOneSolutions\Common\Controllers\ExportableController;
use Illuminate\Http\Request;
use JavaScript;



class BookController extends ExportableController
{
    private $service;
    protected $exportView = 'Book.export';

    public function __construct(BookRecordService $service)
    {
        parent::__construct($service);
        $this->service = $service;
    }

    public function index()
    {
        // to set the routes and controller paths, use a routeserviceprovider
        // to get this edit view.php => paths

        JavaScript::put([
            'filterable' => $this->getDisplayedFilterable(),
            'sorter' => 'name',
            'baseUrl' => '/api/accounting/books?include=organization,currency',
            'exportBaseUrl' => '/accounting/books',
        ]);
        return view('book.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $book = $this->service->getById($id);

        return view('book.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $book = $this->service->getById($id);

        return view('book.edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function transactions($id)
    {
        JavaScript::put([
            'filterable' => $this->getDisplayedFilterable(),
            'toLastPage' => true,
            'sorter' => 'name',
            'id' => $id,
            'baseUrl' => '/api/accounting/books/' . $id . '/transactions?include=entry,account',
        ]);
        $book = $this->service->getById($id);
        return view('book.transactions', compact('book'));
    }

    private function getDisplayedFilterable()
    {
        return [
            ['id' => 'name', 'name' => 'Name'],
        ];
    }
}
