<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Services\Organization\OrganizationRecordService;
use Illuminate\Http\Request;
use JavaScript;
use CometOneSolutions\Common\Controllers\ExportableController;

class OrganizationController extends ExportableController
{

    private $service;
    protected $exportView = 'organization.export';

    public function __construct(OrganizationRecordService $organizationRecordService)
    {
        parent::__construct($organizationRecordService);
        $this->service = $organizationRecordService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        JavaScript::put([
            'filterable' => $this->getDisplayedFilterable(),
            'sorter' => 'name',
            'baseUrl' => '/api/accounting/organizations',
            'exportBaseUrl' => '/accounting/organizations',
        ]);
        return view('organization.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // to set the routes and controller paths, use a routeserviceprovider
        // to get this edit view.php => paths
        JavaScript::put(['id' => null, ]);
        return view('organization.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $organization = $this->service->getById($id);

        return view('organization.show', compact('organization'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $organization = $this->service->getById($id);

        return view('organization.edit', compact('organization'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getDisplayedFilterable()
    {
        return [
            ['id' => 'name', 'name' => 'Name'],
        ];
    }
}
