<?php

namespace CometOneSolutions\Accounting\Controllers;


use CometOneSolutions\Accounting\Services\Account\AccountRecordService;
use CometOneSolutions\Common\Controllers\ExportableController;
use JavaScript;


class AccountController extends ExportableController
{
    private $service;
    protected $exportView = 'Account.export';

    public function __construct(AccountRecordService $service)
    {
        parent::__construct($service);
        $this->service = $service;
    }

    public function index()
    {
        // to set the routes and controller paths, use a routeserviceprovider
        // to get this edit view.php => paths

        JavaScript::put([
            'filterable' => $this->getDisplayedFilterable(),
            'sorter' => 'title',
            'baseUrl' => '/api/accounting/accounts?include=book',
            'exportBaseUrl' => '/accounting/accounts'
        ]);
        return view('account.index');
    }

    public function transactions($id)
    {
        $account = $this->service->getById($id);
        $normalBalance = $account->getNormalBalance();
        JavaScript::put([
            'filterable' => $this->getDisplayedFilterable(),
            'toLastPage' => true,
            'sorter' => 'name',
            'id' => $id,
            'baseUrl' => '/api/accounting/accounts/' . $id . '/transactions?include=entry,account',
            'normalBalance' => $normalBalance,
        ]);
        return view('account.transactions', compact('account'));
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('account.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $account = $this->service->getById($id);
        return view('account.show', compact('account'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $account = $this->service->getById($id);
        return view('account.edit', compact('account'));
    }

    private function getDisplayedFilterable()
    {
        return [
            ['id' => 'name', 'title' => 'Name'],
        ];
    }
}
