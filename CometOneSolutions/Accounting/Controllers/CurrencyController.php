<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Services\Currency\CurrencyRecordService;
use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;

class CurrencyController extends Controller
{

    private $service;

    public function __construct(CurrencyRecordService $currencyRecordService)
    {
        $this->service = $currencyRecordService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        JavaScript::put([
            'filterable' => $this->getDisplayedFilterable(),
            'sorter' => 'code',
            'baseUrl' => '/api/accounting/currencies'
        ]);
        return view('currency.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // to set the routes and controller paths, use a routeserviceprovider
        // to get this edit view.php => paths
        JavaScript::put(['id' => null, ]);
        return view('currency.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $currency = $this->service->getById($id);

        return view('currency.show', compact('currency'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $currency = $this->service->getById($id);

        return view('currency.edit', compact('currency'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getDisplayedFilterable()
    {
        return [
            ['id' => 'code', 'name' => 'code'],
        ];
    }
}
