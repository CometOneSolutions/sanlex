<?php

namespace CometOneSolutions\Accounting\Controllers;

use CometOneSolutions\Accounting\Services\Category\CategoryRecordService;
use Illuminate\Http\Request;
use JavaScript;


class CategoryController extends ExportableController
{

    private $service;
    protected $exportView = 'category.export';

    public function __construct(CategoryRecordService $categoryRecordService)
    {
        parent::__construct($categoryRecordService);
        $this->service = $categoryRecordService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        JavaScript::put([
            'filterable' => $this->getDisplayedFilterable(),
            'sorter' => 'name',
            'baseUrl' => '/api/accounting/categories?include=book',
            'exportBaseUrl' => '/accounting/categories',
        ]);
        return view('category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // to set the routes and controller paths, use a routeserviceprovider
        // to get this edit view.php => paths
        JavaScript::put(['id' => null, ]);
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $category = $this->service->getById($id);

        return view('category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $category = $this->service->getById($id);

        return view('category.edit', compact('category'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getDisplayedFilterable()
    {
        return [
            ['id' => 'name', 'name' => 'Name'],
        ];
    }
}
