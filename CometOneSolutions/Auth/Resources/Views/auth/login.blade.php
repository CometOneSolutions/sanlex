<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->

    <title>Please enter credentials.</title>

    <!-- Main styles for this application -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Styles required by this views -->
</head>

<body class="app flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
            <div class="card-group">
                    <div class="card p-4">
                        <div class="card-body">
                            <h1>Login</h1>
                            <p class="text-muted">Please sign in to your account.</p>
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('post_login') }}" autocomplete="off">
                                {!! csrf_field() !!}
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
                                </div>
                                <input type="text" id="username" name="username" class="form-control" placeholder="Username">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                         
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-lock" aria-hidden="true"></i></span>
                                </div>
                                <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <!-- <a href="/"> -->
                                    <button type="submit" class="btn btn-primary px-4">Login</button>
                                <!-- </a> -->
                                </div>
                                <div class="col-8 text-right text-muted">
                                    Need help? Contact your supervisor.
                                </div>
                            </div>
                            </form>               
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>

</html>