<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="form-group">
    <label for="name">Name *</label>
    <input type="text" class="form-control" id="name" name="name" v-model="form.name" aria-describedby="nameHelp"
        placeholder="" :class="{'is-invalid': form.errors.has('name')}" required data-msg-required="Enter name">
</div>
<div class="form-group">
    <label for="email">Email *</label>
    <input type="email" class="form-control" id="email" email="email" v-model="form.email" aria-describedby="emailHelp"
        placeholder="" :class="{'is-invalid': form.errors.has('email')}" required data-msg-required="Enter email">
</div>
<div class="form-group">
    <label for="username">Username *</label>
    <input type="text" class="form-control" id="username" name="username" v-model="form.username"
        aria-describedby="usernameHelp" placeholder="" :class="{'is-invalid': form.errors.has('username')}" required
        data-msg-required="Enter username">
</div>
<div class="form-group" v-if="!form.id">
    <label for="password">Password *</label>
    <input type="password" class="form-control" id="password" name="password" v-model="form.password"
        aria-describedby="passwordHelp" placeholder="">
</div>
<div class="form-group" v-if="!form.id">
    <label for="passwordconfirm">Password Confirmation*</label>
    <input type="password" class="form-control" id="passwordconfirm" name="passwordconfirm"
        v-model="form.password_confirmation" aria-describedby="passwordHelp" placeholder="">
</div>
<div class="form-group">
    <label for="role">Role *</label>
    <select2 class="form-control" v-model="form.roleIds" id="role" multiple="multiple" placeholder="">
        <option disabled value=''>Please select Role</option>
        <option v-for="role in roleSelections" :value="role.id">@{{ role.name }}</option>
    </select2>
</div>
<div class="form-group">
    <label for="status">Status *</label>
    <select class="form-control" id="status" name="status" v-model="form.status" aria-describedby="statusHelp"
        :class="{'is-invalid': form.errors.has('status')}" required data-msg-required="Enter status">
        <option :value="null" disabled selected>Please select</option>
        <option>Active</option>
        <option>Inactive</option>
    </select>
</div>
@push('scripts')
<script src="{{ mix('js/user.js') }}"></script>
@endpush