   
<div class="form-row mt-2">
    <div class="form-group col-md-12">
        <label for="inputField" class="col-form-label"><strong>Name</strong></label>
        <!-- <p>@{{form.firstName}}</p> -->
        <p>{{$user->getName()}}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="inputField" class="col-form-label"><strong>Username</strong></label>
        <!-- <p>@{{form.username}}</p> -->
        <p>{{$user->getUsername()}}</p>
    </div>

    <div class="form-group col-md-12">
        <label for="inputField" class="col-form-label"><strong>Role</strong></label>
        <!-- <p>@{{form.roleIdName}}</p> -->
        <p>Administrator</p>
    </div>
</div>

@push('scripts')
    <script src="/js/user.js"></script>
@endpush
 