@extends('app')
@section('breadcrumbs', Breadcrumbs::render('user.change_password', $user))
@section('content')
<section id="user">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-circle-notch fa-spin"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        <b>Change Password</b>
                    </div>
                    <v-form @validate="updatePassword">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="password">Password *</label>
                                <input type="password" class="form-control" id="password" name="password" v-model="form.password" aria-describedby="passwordHelp" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="passwordconfirm">Password Confirmation*</label>
                                <input type="password" class="form-control" id="passwordconfirm" name="passwordconfirm" v-model="form.password_confirmation" aria-describedby="passwordHelp" placeholder="">
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                            </div>
                        </div>
                    </v-form>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection
@push('scripts')
<script src="{{ mix('js/user.js') }}"></script>
@endpush