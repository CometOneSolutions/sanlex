@extends('app')
@section('breadcrumbs', Breadcrumbs::render('user.create'))
@section('content')
    <section id="user">
        <v-form @validate="store">
            <div class="card table-responsive">
                <div class="card-header">
                    Create User

                </div>
                <div class="card-body">
                        @include("user._form")
                </div>
                <div class="card-footer">
                    <div class="float-right">
                        <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                    </div>
                </div>
            </div>
        </v-form>
    </section>
@endsection