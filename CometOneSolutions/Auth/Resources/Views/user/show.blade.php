@extends('app')
@section('breadcrumbs', Breadcrumbs::render('user.show', $user))
@section('content')
<section id="user">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-circle-notch fa-spin"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        <b>{{$user->getUsername()}}</b>
                        @if(auth()->user()->can('Update [MAS] User'))
                            <div class="float-right">
                                <a :href="form.changePasswordUri"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-lock" aria-hidden="true"></i> Change Password</button></a>
                                <a :href="form.editUri"><button type="button" class="btn btn-primary btn-sm"><i class="far fa-edit" aria-hidden="true"></i> </button></a>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name"><b>Name</b></label>
                                    <p>@{{form.name}}</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="street"><b>Email</b></label>
                                    <p>@{{ form.email }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name"><b>Username</b></label>
                                    <p>@{{form.username}}</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="street"><b>Status</b></label>
                                    <p>@{{ form.status }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name"><b>Roles</b></label>
                                    <ul>
                                        <li v-for="role in form.roles.data"> @{{ role.name }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script src="/js/user.js"></script>
@endpush