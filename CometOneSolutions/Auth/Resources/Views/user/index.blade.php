@extends('app')
@section('breadcrumbs', Breadcrumbs::render('user.index'))
@section('content')

<!-- Vue Index Template -->
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Users
            @if(auth()->user()->can('Create [MAS] User'))
                <div class="float-right">
                    <a href="{{route('user_create')}}"><button type="button" class="btn btn-success btn-sm"><i
                                class="fa fa-plus" aria-hidden="true"></i> </button></a>
                </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending"
                v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('username')">
                                    Username <i class="fa" :class="getSortIcon('username')"></i>
                                </a>
                            </th>
                            <th class="text-left">
                                <a v-on:click="setSorter('name')">
                                    Name <i class="fa" :class="getSortIcon('name')"></i>
                                </a>
                            </th>
                            <th class="text-left">
                                <a v-on:click="setSorter('status')">
                                    Status <i class="fa" :class="getSortIcon('status')"></i>
                                </a>
                            </th>
                            <th class="text-right">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td class="text-left align-middle"><a :href="item.showUri">@{{item.username}}</a></td>
                            <td class="text-left align-middle">@{{item.name}}</td>
                            <td class="text-left align-middle">@{{item.status}}</td>
                            <td class="text-right align-middle"><a :href="item.editUri" role="button"
                                    class="btn btn-success btn-sm"><i class="far fa-edit" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>
<!-- /Vue Index Template -->
</div>
</div>






<!-- End Watchlist-->
@endsection


@push('scripts')
<script src="/js/index.js"></script>
@endpush