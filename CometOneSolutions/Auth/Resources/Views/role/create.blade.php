@extends('app')
@section('breadcrumbs', Breadcrumbs::render('role.create'))
@section('content')
    <section id="role">
        <v-form @validate="store">
            <div class="card table-responsive">
                <div class="card-header">
                    Create Role
                </div>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-circle-notch fa-spin"></i> Initializing...
                    </div>
                </div>
                <div v-else class="card-body">
                        @include("role._form")
                </div>
                <div class="card-footer">
                    <div class="float-right">
                        <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                    </div>
                </div>
            </div>
        </v-form>
    </section>
@endsection