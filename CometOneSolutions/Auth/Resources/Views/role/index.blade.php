@extends('app')
@section('breadcrumbs', Breadcrumbs::render('role.index'))
@section('content')

<!-- Vue Index Template -->
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Roles
            @if(auth()->user()->can('Create [MAS] Role'))
                <div class="float-right">
                    <a href="{{route('role_create')}}"><button type="button" class="btn btn-success btn-sm"><i
                                class="fa fa-plus" aria-hidden="true"></i> </button></a>
                </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending"
                v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('name')">
                                    Name <i class="fa" :class="getSortIcon('name')"></i>
                                </a>
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td class="text-left align-middle"><a :href="item.showUri">@{{item.name}}</a></td>
                            <td class="text-right align-middle"><a :href="item.editUri" role="button"
                                    class="btn btn-outline-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>
</div>
</div>
@endsection
@push('scripts')
<script src="/js/index.js"></script>
@endpush