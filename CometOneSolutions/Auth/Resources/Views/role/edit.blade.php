@extends('app')
@section('breadcrumbs', Breadcrumbs::render('role.edit', $role))
@section('content')
<section id="role">
    <v-form @validate="update">
        <div class="row">
            <div class="col-12">
                <div class="card" v-cloak>
                    <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                        <div class="col-12 text-center h4">
                            <i class="fas fa-circle-notch fa-spin"></i> Initializing...
                        </div>
                    </div>
                    <div v-else>
                        <div class="card-header">
                            <b>{{$role->getName()}}</b>
                            @if(auth()->user()->can('Delete [MAS] Role'))
                                <div class="float-right">
                                    <delete-button :is-busy="form.isBusy" :is-deleting="form.isDeleting" @destroy="destroy">
                                    </delete-button>
                                </div>
                            @endif
                        </div>
                        <div class="card-body">
                            @include('role._form')
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </v-form>
</section>
<br />
@endsection