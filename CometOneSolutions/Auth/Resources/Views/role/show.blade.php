@extends('app')
@section('breadcrumbs', Breadcrumbs::render('role.show', $role))
@section('content')
<section id="role">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-circle-notch fa-spin"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        <b>{{$role->getName()}}</b>
                        @if(auth()->user()->can('Update [MAS] Role'))
                            <div class="float-right">
                                <a :href="form.editUri"><button type="button" class="btn btn-primary btn-sm"><i
                                            class="far fa-edit" aria-hidden="true"></i> </button></a>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        @include('role._form')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection