<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="form-group">
    <label for="name">Name *</label>
    <input type="text" :disabled="isNotEditable" class="form-control" id="name" name="name" v-model="form.name"
        aria-describedby="nameHelp" placeholder="" :class="{'is-invalid': form.errors.has('name')}" required
        data-msg-required="Enter name">
</div>

<table class="table">
    <thead>
        <tr>
            <th class="text-left align-middle">Name</th>
            <th v-for="task in tasks">@{{ task }}</th>
        </tr>
    </thead>
    <tbody v-if="permissionDisplay.length > 0">
        <tr v-for="item in permissionDisplay">
            <td class="text-left align-middle">@{{item.name}}</td>
            <td class="align-middle" v-for="task in item.tasks">
                <div class="custom-control custom-switch" v-if="task.permissionId">
                    <input :disabled="isShow" type="checkbox" @change="syncPermissionIds" v-model="task.checked"
                        class="custom-control-input" :id="task.permissionId">
                    <label class="custom-control-label" :for="task.permissionId"></label>
                </div>
            </td>
        </tr>
    </tbody>
</table>

@push('scripts')
<script src="{{ mix('js/role.js') }}"></script>
@endpush