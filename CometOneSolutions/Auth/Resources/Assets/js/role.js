import {
    Form
} from '@c1_common_js/components/Form';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select2 from '@c1_common_js/components/Select2'

new Vue({
    el: '#role',
    components: {
        SaveButton,
        DeleteButton,
        Timestamp,
        VForm,
        Select2
    },
    data: {
        form: new Form({
            name: null,
            permissionIds: [],
            permissions: {
                data: []
            },
        }),
        dataInitialized: true,
        permissionSelections: [],
        permissionDisplay: [],
        tasks: [],
        permissionsInitialized: false,
        displayInitialized: false,
        isEdit: false,
        isShow: false,
    },

    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized && this.permissionsInitialized && this.displayInitialized;
        },

        isNotEditable() {
            if ((this.isEdit) || (this.isShow)) {
                return true;
            } else {
                return false;
            }
        },
    },
    methods: {

        destroy() {
            this.form.deleteWithConfirmation('/api/roles/' + id).then(response => {
                this.form.successModal('Role has been removed').then(() =>
                    window.location = '/master-file/roles/'
                );
            });
        },

        store() {
            this.form.postWithModal('/api/roles', null, 'Role is created!').then(() =>
                window.location = '/master-file/roles/create'
            );
        },

        update() {
            this.form.patch('/api/roles/' + id).then(response => {
                this.form.successModal('Role has been updated.').then(() =>
                    window.location = "/master-file/roles/" + id
                );
            });
        },

        initializePermissionDisplay() {
            this.displayInitialized = false;
            this.permissionDisplay = [];
            let uniquePermissions = this.getModulesForPermissions();
            uniquePermissions.forEach(element => {
                this.permissionDisplay.push({
                    name: element,
                    tasks: this.generateTasksArrayByPermissionName(element),
                });
            })
            this.displayInitialized = true;
        },

        generateTasksArrayByPermissionName(name) {
            let taskForPermission = [];
            let generalTasks = this.getTasksForPermissions();
            generalTasks.forEach(task => {
                let permissionId = this.findPermissionId(task + ' ' + name);
                taskForPermission.push({
                    permissionId: permissionId,
                    checked: this.checkIfExists(this.findPermissionId(task + ' ' + name)),
                    taskName: task
                })
            });
            return taskForPermission;
        },

        getModulesForPermissions() {
            let modules = [];
            this.permissionSelections.forEach(permission => {
                modules.push(permission.name.split(' ').slice(1).join(' '));
            })
            return this.removeDuplicatesFromArray(modules);
        },

        getTasksForPermissions() {
            let tasks = [];
            this.permissionSelections.forEach(permission => {
                tasks.push(permission.name.replace(/ .*/, ''));
            });
            this.tasks = this.removeDuplicatesFromArray(tasks);
            return this.removeDuplicatesFromArray(tasks);
        },

        removeDuplicatesFromArray(array) {
            return array.filter((v, i) => array.indexOf(v) == i);
        },

        findPermissionId(name) {
            let permission = this.permissionSelections.find(permission => permission.name == name);
            if (permission) {
                return permission.id
            }

            return null;
        },

        checkIfExists(id) {
            let exists = this.form.permissions.data.find(permission => permission.id == id);
            if (exists === undefined) {
                return false;
            } else {
                return true;
            }
        },

        syncPermissionIds() {
            this.form.permissionIds = [],
                this.permissionDisplay.forEach(permission => {
                    permission.tasks.forEach(task => {
                        if (task.checked) {
                            this.form.permissionIds.push(task.permissionId);
                        }
                    });
                })
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {
        this.form.get('/api/permissions?limit=' + Number.MAX_SAFE_INTEGER).then(response => {
            this.permissionSelections = response.data;
            this.permissionsInitialized = true;
            this.initializePermissionDisplay();
        });

        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/roles/' + id + '?include=permissions').then(response => {
                this.loadData(response.data);
                this.initializePermissionDisplay();
                this.syncPermissionIds();
                this.dataInitialized = true;
            });
        }
        this.isEdit = isEdit;
        this.isShow = isShow;

    },

    mounted() {
        console.log("Init Roles script...");
    }
});