import {
    Form
} from '@c1_common_js/components/Form';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select2 from '@c1_common_js/components/Select2'
import Select3 from '@c1_common_js/components/Select3'

new Vue({
    el: '#user',
    components: {
        SaveButton,
        DeleteButton,
        Timestamp,
        VForm,
        Select2,
        Select3
    },
    data: {
        form: new Form({
            id: null,
            name: null,
            email: null,
            username: null,
            password: null,
            password_confirmation: null,
            roleIds: [],
            roles: {
                data: [],
            },
            status: null,
        }),
        dataInitialized: true,
        roleSelections: [],
        rolesInitialized: false,
        roleApiUrl: '/api/roles',
        defaultRoles: {},
    },

    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized && this.rolesInitialized;
        },
    },
    methods: {

        destroy() {
            this.form.confirm().then((result) => {
                if (result.value) {
                    this.form.delete('/api/users/' + id)
                        .then(response => {
                            this.$swal({
                                title: 'Success',
                                text: this.form.name.toUpperCase() + ' was removed.',
                                type: 'success'
                            }).then(() => window.location = '/master-file/users');
                        });
                }
            });
        },

        store() {
            this.form.post('/api/users').then(response => {
                this.$swal({
                    title: 'Success',
                    text: this.form.name.toUpperCase() + ' was saved.',
                    type: 'success'
                });
                this.form.reset();
            });
        },

        update() {
            this.form.patch('/api/users/' + this.form.id).then(response => {
                this.$swal({
                    title: 'Success',
                    text: this.form.name.toUpperCase() + ' was updated.',
                    type: 'success',
                    confirmButtonColor: '#20a8d8',
                }).then(() => window.location = '/master-file/users/' + this.form.id);
            })
        },

        updatePassword() {
            this.form.patch('/api/users/' + id + '/update-password').then(response => {
                this.$swal({
                    title: 'Success',
                    text: 'Password was updated.',
                    type: 'success',
                    confirmButtonColor: '#20a8d8',
                }).then(() => window.location = '/master-file/users/' + this.form.id);
            })
        },

        loadData(data) {
            data.roleIds = data.roles.data.map(role => role.id);
            this.form = new Form(data);
        },
    },

    created() {

        //get role selections
        this.form.get('/api/roles').then(response => {
            this.roleSelections = response.data;
            this.rolesInitialized = true;
        });

        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/users/' + id + '?include=roles').then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
            });
        }
    },

    mounted() {
        console.log("Init USER script...");
    }
});