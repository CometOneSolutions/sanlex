<?php

namespace CometOneSolutions\Auth\Models;

use CometOneSolutions\Auth\Models\Users\User;

interface UpdatableByUser
{
    public function setUpdatedByUser(User $user);

    public function getUpdatedByUser();
}
