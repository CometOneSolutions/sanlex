<?php

namespace CometOneSolutions\Auth\Models\Roles;

interface Role
{

    public function getId();

    public function getName();

    public function getGuardName();
    
}
