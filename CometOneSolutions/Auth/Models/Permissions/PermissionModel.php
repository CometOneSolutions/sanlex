<?php

namespace CometOneSolutions\Auth\Models\Permissions;

use Spatie\Permission\Models\Permission as SpatiePermission;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class PermissionModel extends SpatiePermission implements Permission
{
    protected $table = 'permissions';

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(
            config('permission.models.role'),
            config('permission.table_names.role_has_permissions'),
            'permission_id',
            'role_id'
        );
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    public function getGuardName()
    {
        return $this->guard_name;
    }

    public function setGuardname($value)
    {
        $this->guard_name = $value;
        return $this;
    }
}
