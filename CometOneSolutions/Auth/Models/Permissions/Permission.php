<?php

namespace CometOneSolutions\Auth\Models\Permissions;

interface Permission
{
    public function getId();

    public function getName();

    public function getGuardName();
}
