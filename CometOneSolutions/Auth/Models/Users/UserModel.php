<?php

namespace CometOneSolutions\Auth\Models\Users;

use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserModel extends Authenticatable implements User
{
    use Notifiable;

    use HasApiTokens;
    use HasRoles;

    protected $table = 'users';

    protected $rolesToSet = null;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getId()
    {
        return $this->id;
    }

    public function isAdmin()
    {
        return $this->hasRole('ADMIN');
    }

    public function getRoles()
    {
        if ($this->rolesToSet !== null) {
            return collect($this->rolesToSet);
        }
        return $this->roles;
    }

    public function setRoles(array $roles)
    {
        $this->rolesToSet = $roles;
        return $this;
    }

    public function setName($value)
    {
        $this->name = strtoupper($value);
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setUsername($value)
    {
        $this->username = $value;
        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setEmail($value)
    {
        $this->email = $value;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setPassword($value)
    {
        $this->password = bcrypt($value);
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }
}
