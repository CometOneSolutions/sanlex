<?php

namespace CometOneSolutions\Auth\Models\Users;

interface User
{
    public function getId();

    public function isAdmin();

    public function getRoles();

    public function setRoles(array $roles);

    public function setName($value);

    public function getName();

    public function setUsername($value);

    public function getusername();

    public function setEmail($value);

    public function getEmail();

    public function setPassword($value);

    public function getStatus();

    public function setStatus($value);
}
