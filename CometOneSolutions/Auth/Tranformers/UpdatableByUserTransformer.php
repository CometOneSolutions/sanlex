<?php

namespace CometOneSolutions\Auth\Tranformers;

use CometOneSolutions\Auth\Models\UpdatableByUser;
use League\Fractal;

abstract class UpdatableByUserTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = ['updatedByUser'];

    public function includeUpdatedByUser(UpdatableByUser $updatableByUser)
    {
        $user = $updatableByUser->getUpdatedByUser();
        if ($user != null) {
            return $this->item($user, new UserTransformer);
        }
        return $this->null();
    }
}
