<?php
namespace CometOneSolutions\Auth\Tranformers;

use CometOneSolutions\Auth\Models\Roles\Role;
use League\Fractal;

class RoleTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = ['permissions'];

    public function transform(Role $role)
    {
        return [
            'id' => (int)$role->getId(),
            'name' => $role->getName(),
            'text' => $role->getName(),
            'guardName' => $role->getGuardName(),
            'permissionIds' => [],
            'showUri' => route('role_show', $role->getId()),
            'editUri' => route('role_edit', $role->getId()),
        ];
    }

    public function includePermissions(Role $role)
    {
        return $this->collection($role->getPermissions(), new PermissionTransformer);
    }
}
