<?php

namespace CometOneSolutions\Auth\Tranformers;

use CometOneSolutions\Auth\Models\Users\User;
use League\Fractal;

class UserTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'roles'
    ];

    public function transform(User $user)
    {
        return [
            'id' => (int)$user->getId(),
            'name' => $user->getName(),
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
            'status' => $user->getStatus(),
            'password' => null,
            'password_confirmation' => null,
            'showUri' => route('user_show', $user->getId()),
            'editUri' => route('user_edit', $user->getId()),
            'changePasswordUri' => route('user_change_password', $user->getId()),
        ];
    }

    public function includeRoles(User $user)
    {
        return $this->collection($user->getRoles(), new RoleSpattieTransformer);
    }
}
