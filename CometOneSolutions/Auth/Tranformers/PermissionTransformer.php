<?php
namespace CometOneSolutions\Auth\Tranformers;

use CometOneSolutions\Auth\Models\Permissions\Permission;
use League\Fractal;

class PermissionTransformer extends Fractal\TransformerAbstract
{
    public function transform(Permission $permission)
    {
        return [
            'id' => (int)$permission->getId(),
            'name' => $permission->getName(),
            'guardName' => $permission->getGuardName(),
        ];
    }
}
