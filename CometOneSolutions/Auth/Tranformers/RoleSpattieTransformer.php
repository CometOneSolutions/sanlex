<?php
namespace CometOneSolutions\Auth\Tranformers;

use League\Fractal;
use CometOneSolutions\Auth\Models\Roles\Role;

class RoleSpattieTransformer extends Fractal\TransformerAbstract
{
    public function transform(Role $role)
    {
        return [
            'id' => (int)$role->id,
            'name' => $role->name,
            'guardName' => $role->guard_name,
        ];
    }
}
