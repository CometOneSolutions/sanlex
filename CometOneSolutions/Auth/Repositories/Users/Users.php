<?php

namespace CometOneSolutions\Auth\Repositories\Users;

use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Repositories\Repository;

interface Users extends Repository
{
    public function save(User $user);

    public function delete(User $user);
}
