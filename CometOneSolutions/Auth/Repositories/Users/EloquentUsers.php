<?php

namespace CometOneSolutions\Auth\Repositories\Users;

use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentUsers extends EloquentRepository implements Users
{
    protected $user;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->user = $user;
    }

    public function save(User $user)
    {
        $user->save();
        $user->syncRoles($user->getRoles());
        return $user;
    }

    public function delete(User $user)
    {
        return $user->delete();
    }
}
