<?php

namespace CometOneSolutions\Auth\Repositories\Permissions;

use CometOneSolutions\Auth\Models\Permissions\Permission;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentPermissions extends EloquentRepository implements Permissions
{
    protected $permission;

    public function __construct(Permission $permission)
    {
        parent::__construct($permission);
        $this->permission = $permission;
    }
    public function save(Permission $permission)
    {
        $permission->save();
        return $permission;
    }

    public function delete(Permission $permission)
    {
        return $permission->delete();
    }
}
