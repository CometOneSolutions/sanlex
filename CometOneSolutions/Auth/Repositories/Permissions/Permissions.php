<?php

namespace CometOneSolutions\Auth\Repositories\Permissions;

use CometOneSolutions\Auth\Models\Permissions\Permission;
use CometOneSolutions\Common\Repositories\Repository;

interface Permissions extends Repository
{
    public function save(Permission $permission);

    public function delete(Permission $permission);
}
