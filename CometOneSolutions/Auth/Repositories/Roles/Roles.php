<?php

namespace CometOneSolutions\Auth\Repositories\Roles;

use CometOneSolutions\Auth\Models\Roles\Role;
use CometOneSolutions\Common\Repositories\Repository;

interface Roles extends Repository
{
    public function save(Role $role);

    public function delete(Role $role);
}
