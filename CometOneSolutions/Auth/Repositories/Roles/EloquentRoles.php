<?php

namespace CometOneSolutions\Auth\Repositories\Roles;

use CometOneSolutions\Auth\Models\Roles\Role;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentRoles extends EloquentRepository implements Roles
{
    protected $role;

    public function __construct(Role $role)
    {
        parent::__construct($role);
        $this->role = $role;
    }
    public function save(Role $role)
    {
        $role->save();
        $role->syncPermissions($role->getPermissions());
        return $role;
    }

    public function delete(Role $role)
    {
        return $role->delete();
    }
}
