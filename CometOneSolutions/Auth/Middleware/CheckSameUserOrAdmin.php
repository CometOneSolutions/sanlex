<?php

namespace CometOneSolutions\Auth\Middleware;

use Closure;

class CheckSameUserOrAdmin
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = $request->route('userId');

        if ($request->user()->getId() != $userId && !$request->user()->isAdmin()) {
            abort(401);
        }

        return $next($request);
    }
}
