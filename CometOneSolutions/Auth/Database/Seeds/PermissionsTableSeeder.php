<?php

namespace CometOneSolutions\Auth\Database\Seeds;

use Illuminate\Database\Seeder;
use CometOneSolutions\Auth\Models\Permissions\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = [
            'Config',
            'Accessory',
            'Adjustment',
            'Coil',
            'Coating',
            'Color',
            'Sale Order',
            'Contact',
            'Customer',
            'Purchase Order',
            'Delivery Receipt',
            'Inbound Payment',
            'Job Order',
            'Outbound Payment',
            'Panel Profile',
            'Report',
            'Return',
            'Supplier',
            'Thickness',
            'Width',
            'Length',
            'Screw Type',
            'Screw',
            'Skylight Class',
            'Skylight',
            'Insulation',
            'Insulation Type',
            'Insulation Density',
            'Backing Side',
            'User',
            'Role',
            'Coil Summary by Color',
            'Coil Summary by Thickness',
            'Approve'

        ];
        $actions = ['Create', 'Read', 'Update', 'Delete'];

        foreach ($modules as $module) {
            foreach ($actions as $action) {
                Permission::create([
                    'name' => $action . ' ' . $module,
                ]);
            }
        }
    }
}
