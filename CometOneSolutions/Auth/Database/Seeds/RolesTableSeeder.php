<?php

namespace CometOneSolutions\Auth\Database\Seeds;

use Illuminate\Database\Seeder;
use CometOneSolutions\Auth\Models\Roles\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'ADMIN',
        ]);
    }
}
