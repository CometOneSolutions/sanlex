<?php

namespace CometOneSolutions\Auth\Database\Seeds;

use CometOneSolutions\Auth\Services\Users\UserRecordService;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = resolve(UserRecordService::class)->create(
            'Boss',
            'admin@email.com',
            'admin',
            'admin',
            'Active'
        );
        // $admin = UserModel::create([
        //     'username' => 'admin',
        //     'name' => 'Administrator',
        //     'email' => 'admin@email.com',
        //     'password' => bcrypt('admin'),
        //     'status' => 'Active'
        // ]);

        $admin->assignRole('ADMIN');
    }
}
