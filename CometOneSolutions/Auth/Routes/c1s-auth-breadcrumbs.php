<?php

Breadcrumbs::register('user.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Users', route('user_index'));
});

Breadcrumbs::register('user.show', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('user.index');
    $breadcrumbs->push($user->getUsername(), route('user_show', $user->getId()));
});

Breadcrumbs::register('user.create', function ($breadcrumbs) {
    $breadcrumbs->parent('user.index');
    $breadcrumbs->push('Create', route('user_create'));
});

Breadcrumbs::register('user.edit', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('user.show', $user);
    $breadcrumbs->push('Edit', route('user_edit', $user->getId()));
});

Breadcrumbs::register('user.change_password', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('user.show', $user);
    $breadcrumbs->push('Change Password', route('user_change_password', $user->getId()));
});


Breadcrumbs::register('role.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Roles', route('role_index'));
});

Breadcrumbs::register('role.create', function ($breadcrumbs) {
    $breadcrumbs->parent('role.index');
    $breadcrumbs->push('Create', route('role_create'));
});

Breadcrumbs::register('role.show', function ($breadcrumbs, $role) {
    $breadcrumbs->parent('role.index');
    $breadcrumbs->push($role->getName(), route('role_show', $role->getId()));
});

Breadcrumbs::register('role.edit', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('role.show', $user);
    $breadcrumbs->push('Edit');
});
