<?php
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'users'], function () use ($api) {
        $api->get('/', 'CometOneSolutions\Auth\Controllers\UserApiController@index');
        $api->get('{userId}', 'CometOneSolutions\Auth\Controllers\UserApiController@show');
        $api->post('/', 'CometOneSolutions\Auth\Controllers\UserApiController@store');
        $api->patch('{userId}', 'CometOneSolutions\Auth\Controllers\UserApiController@update');
        $api->patch('{userId}/update-password', 'CometOneSolutions\Auth\Controllers\UserApiController@updatePassword');
        $api->delete('{userId}', 'CometOneSolutions\Auth\Controllers\UserApiController@destroy');
    });

    $api->group(['prefix' => 'roles'], function () use ($api) {
        $api->get('/', 'CometOneSolutions\Auth\Controllers\RoleApiController@index');
        $api->get('{roleId}', 'CometOneSolutions\Auth\Controllers\RoleApiController@show');
        $api->post('/', 'CometOneSolutions\Auth\Controllers\RoleApiController@store');
        $api->patch('{roleId}', 'CometOneSolutions\Auth\Controllers\RoleApiController@update');
        $api->delete('{roleId}', 'CometOneSolutions\Auth\Controllers\RoleApiController@destroy');
    });

    $api->group(['prefix' => 'permissions'], function () use ($api) {
        $api->get('/', 'CometOneSolutions\Auth\Controllers\PermissionApiController@index');
    });
});
