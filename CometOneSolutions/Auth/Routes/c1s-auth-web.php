<?php

Route::get('login', 'LoginController@showLoginForm')->name('login');
Route::post('login', 'LoginController@login')->name('post_login');

// TODO this should be a post
Route::get('logout', 'LoginController@logout')->name('logout');

// Users
Route::group(['prefix' => 'master-file'], function () {
    Route::group(['prefix' => 'users', 'middleware' => 'auth'], function () {
        Route::get('/', 'UserController@index')->name('user_index');
        Route::get('create', 'UserController@create')->name('user_create');

        Route::group(['middleware' => 'same_user_or_admin'], function () {
            Route::get('{userId}/edit', 'UserController@edit')->name('user_edit');
            Route::get('{userId}', 'UserController@show')->name('user_show');
            Route::get('{userId}/change-password', 'UserController@changePassword')->name('user_change_password');
        });
    });
});

// Roles
Route::group(['prefix' => 'master-file'], function () {
    Route::group(['prefix' => 'roles', 'middleware' => 'auth'], function () {
        Route::get('/', 'RoleController@index')->name('role_index');
        Route::get('create', 'RoleController@create')->name('role_create');
        Route::get('{roleId}/edit', 'RoleController@edit')->name('role_edit');
        Route::get('{roleId}', 'RoleController@show')->name('role_show');
    });
});
