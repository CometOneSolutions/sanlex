<?php

namespace CometOneSolutions\Auth\Controllers;

use CometOneSolutions\Common\Controllers\Controller;

class AuthenticatedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
}
