<?php

namespace CometOneSolutions\Auth\Controllers;

use CometOneSolutions\Auth\Request\StoreUser;
use CometOneSolutions\Auth\Request\UpdatePassword;
use CometOneSolutions\Auth\Request\UpdateUser;
use CometOneSolutions\Auth\Services\Users\UserRecordService;
use CometOneSolutions\Auth\Tranformers\UserTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use Illuminate\Support\Facades\Gate;

class UserApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        UserRecordService $userRecordService,
        UserTransformer $transformer
    ) {
        parent::__construct($userRecordService, $transformer);
        $this->service = $userRecordService;
        $this->transformer = $transformer;
    }

    public function show($id)
    {
        // TODO
        // you are here problem with gates and api middleware
        // if (Gate::denies('xxx', $id)) {
        //     abort(401);
        // }

        return parent::show($id);
    }

    public function destroy($userId)
    {
        $user = \DB::transaction(function () use ($userId) {
            $user = $this->service->getById($userId);
            $this->service->delete($user);
            return $user;
        });
        return $this->response->item($user, $this->transformer)->setStatusCode(200);
    }

    public function store(StoreUser $request)
    {
        $user = \DB::transaction(function () use ($request) {
            $name = $request->getName();
            $email = $request->getEmail();
            $username = $request->getUsername();
            $password = $request->getPassword();
            $roles = $request->getRoles();
            $status = $request->getStatus();

            return $this->service->create(
                $name,
                $email,
                $username,
                $password,
                $status,
                $roles
            );
        });
        return $this->response->item($user, $this->transformer)->setStatusCode(201);
    }

    public function update($userId, UpdateUser $request)
    {
        $user = \DB::transaction(function () use ($userId, $request) {
            $user = $this->service->getById($userId);
            $name = $request->getName();
            $email = $request->getEmail();
            $username = $request->getUsername();
            $roles = $request->getRoles();
            $status = $request->getStatus();

            return $this->service->update(
                $user,
                $name,
                $email,
                $username,
                $status,
                $roles
            );
        });
        return $this->response->item($user, $this->transformer)->setStatusCode(200);
    }

    public function updatePassword($userId, UpdatePassword $request)
    {
        $user = \DB::transaction(function () use ($userId, $request) {
            $user = $this->service->getById($userId);
            $password = $request->getPassword();

            return $this->service->updatePassword(
                $user,
                $password
            );
        });
        return $this->response->item($user, $this->transformer)->setStatusCode(200);
    }
}
