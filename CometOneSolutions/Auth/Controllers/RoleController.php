<?php

namespace CometOneSolutions\Auth\Controllers;

use CometOneSolutions\Auth\Services\Roles\RoleRecordService;
use JavaScript;
use CometOneSolutions\Common\Controllers\Controller;

class RoleController extends Controller
{
    private $service;

    public function __construct(RoleRecordService $roleRecordService)
    {
        $this->service = $roleRecordService;
        $this->middleware('permission:Read [MAS] Role')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Role')->only('create');
        $this->middleware('permission:Update [MAS] Role')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/roles'
        ]);
        return view('role.index');
    }

    public function create()
    {
        JavaScript::put(
            ['id' => null, 'isEdit' => false, 'isShow' => false]
        );
        return view('role.create');
    }

    public function show($id)
    {
        JavaScript::put(
            ['id' => $id, 'isEdit' => false, 'isShow' => true]
        );
        $role = $this->service->getById($id);
        return view('role.show', compact('role'));
    }

    public function edit($id)
    {
        JavaScript::put(
            ['id' => $id, 'isEdit' => true, 'isShow' => false]
        );
        $role = $this->service->getById($id);
        return view('role.edit', compact('role'));
    }
}
