<?php

namespace CometOneSolutions\Auth\Controllers;

use CometOneSolutions\Auth\Services\Users\UserRecordService;
use JavaScript;
use CometOneSolutions\Common\Controllers\Controller;

class UserController extends Controller
{
    private $service;

    public function __construct(UserRecordService $userRecordService)
    {
        $this->service = $userRecordService;
        $this->middleware('permission:Read [MAS] User')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] User')->only('create');
        $this->middleware('permission:Update [MAS] User')->only('edit', 'changePassword');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'userName',
            'sortAscending' => true,
            'baseUrl' => '/api/users'
        ]);
        return view('user.index');
    }

    public function create()
    {
        JavaScript::put(
            ['id' => null]
        );
        return view('user.create');
    }

    public function show($id)
    {
        JavaScript::put(
            ['id' => $id]
        );
        $user = $this->service->getById($id);
        return view('user.show', compact('user'));
    }

    public function edit($id)
    {
        JavaScript::put(
            ['id' => $id]
        );
        $user = $this->service->getById($id);
        return view('user.edit', compact('user'));
    }

    public function changePassword($id)
    {
        JavaScript::put(
            ['id' => $id]
        );
        $user = $this->service->getById($id);
        return view('user.change_password', compact('user'));
    }
}
