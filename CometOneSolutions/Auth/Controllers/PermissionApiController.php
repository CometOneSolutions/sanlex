<?php

namespace CometOneSolutions\Auth\Controllers;

use CometOneSolutions\Auth\Services\Permissions\PermissionRecordService;
use CometOneSolutions\Auth\Tranformers\PermissionTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;

class PermissionApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        PermissionRecordService $permissionRecordService,
        PermissionTransformer $transformer
    ) {
        parent::__construct($permissionRecordService, $transformer);
        $this->service = $permissionRecordService;
        $this->transformer = $transformer;
    }
}
