<?php

namespace CometOneSolutions\Auth\Controllers;

use CometOneSolutions\Auth\Request\Roles\StoreRole;
use CometOneSolutions\Auth\Request\Roles\UpdateRole;
use CometOneSolutions\Auth\Services\Roles\RoleRecordService;
use CometOneSolutions\Auth\Tranformers\RoleTransformer;
use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;

class RoleApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        RoleRecordService $roleRecordService,
        RoleTransformer $transformer
    ) {
        parent::__construct($roleRecordService, $transformer);
        $this->service = $roleRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreRole $request)
    {
        $role = \DB::transaction(function () use ($request) {
            return $this->service->create(
                $request->getName(),
                $request->getPermissions()
            );
        });
        return $this->response->item($role, $this->transformer)->setStatusCode(201);
    }

    public function update($roleId, UpdateRole $request)
    {
        $role = \DB::transaction(function () use ($request, $roleId) {
            $role = $this->service->getById($roleId);
            return $this->service->update(
                $role,
                $request->getPermissions()
            );
        });
        return $this->response->item($role, $this->transformer)->setStatusCode(200);
    }

    public function destroy($roleId)
    {
        $role = \DB::transaction(function () use ($roleId) {
            $role = $this->service->getById($roleId);
            return $this->service->destroy(
                $role
            );
        });
        return $this->response->item($role, $this->transformer)->setStatusCode(200);
    }
}
