<?php

namespace CometOneSolutions\Auth\Providers;

use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Auth\Models\Users\UserModel;
use CometOneSolutions\Auth\Repositories\Users\EloquentUsers;
use CometOneSolutions\Auth\Repositories\Users\Users;
use CometOneSolutions\Auth\Services\Users\UserRecordService;
use CometOneSolutions\Auth\Services\Users\UserRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Gate;

class CometOneAuthServiceProvider extends ServiceProvider
{
    protected $namespace = 'CometOneSolutions\Auth';
    protected $dbPath = 'CometOneSolutions/auth/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Gate::before(function ($user, $ability) {
            return $user->isAdmin();
        });

        // Gate::define('access-user', function ($user, $userId) {
        //     return $user->getId() == $userId || $user->isAdmin();
        // });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(base_path($this->dbPath . 'Factories'));
        $this->app->bind(UserRecordService::class, UserRecordServiceImpl::class);
        $this->app->bind(Users::class, EloquentUsers::class);
        $this->app->bind(User::class, UserModel::class);
        Passport::routes();
    }
}
