<?php

namespace CometOneSolutions\Auth\Providers;

use CometOneSolutions\Auth\Models\Roles\Role;
use CometOneSolutions\Auth\Models\Roles\RoleModel;
use CometOneSolutions\Auth\Repositories\Roles\EloquentRoles;
use CometOneSolutions\Auth\Repositories\Roles\Roles;
use CometOneSolutions\Auth\Services\Roles\RoleRecordService;
use CometOneSolutions\Auth\Services\Roles\RoleRecordServiceImpl;
use Illuminate\Support\ServiceProvider;

class RoleServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RoleRecordService::class, RoleRecordServiceImpl::class);
        $this->app->bind(Roles::class, EloquentRoles::class);
        $this->app->bind(Role::class, RoleModel::class);
    }
}
