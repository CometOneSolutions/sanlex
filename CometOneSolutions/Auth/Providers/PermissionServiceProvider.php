<?php

namespace CometOneSolutions\Auth\Providers;

use CometOneSolutions\Auth\Models\Permissions\Permission;
use CometOneSolutions\Auth\Models\Permissions\PermissionModel;
use CometOneSolutions\Auth\Repositories\Permissions\EloquentPermissions;
use CometOneSolutions\Auth\Repositories\Permissions\Permissions;
use CometOneSolutions\Auth\Services\Permissions\PermissionRecordService;
use CometOneSolutions\Auth\Services\Permissions\PermissionRecordServiceImpl;
use Illuminate\Support\ServiceProvider;

class PermissionServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PermissionRecordService::class, PermissionRecordServiceImpl::class);
        $this->app->bind(Permissions::class, EloquentPermissions::class);
        $this->app->bind(Permission::class, PermissionModel::class);
    }
}
