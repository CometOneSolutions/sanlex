<?php

namespace CometOneSolutions\Auth\Request\Roles;

use Dingo\Api\Http\FormRequest;

class RoleRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }

    public function getName()
    {
        return $this->input('name');
    }

    public function getPermissions()
    {
        return $this->input('permissionIds');
    }
}
