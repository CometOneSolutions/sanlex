<?php

namespace CometOneSolutions\Auth\Request\Roles;

use Dingo\Api\Http\FormRequest;

class StoreRole extends RoleRequests
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Role');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
