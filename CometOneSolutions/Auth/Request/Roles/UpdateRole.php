<?php

namespace CometOneSolutions\Auth\Request\Roles;

use Dingo\Api\Http\FormRequest;

class UpdateRole extends RoleRequests
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Role');
    }
}
