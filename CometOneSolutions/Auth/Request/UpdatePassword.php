<?php

namespace CometOneSolutions\Auth\Request;

class UpdatePassword extends UserRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] User');
    }
    
    public function rules()
    {
        return [
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'password.confirmed' => 'Password does not match',
        ];
    }
}
