<?php

namespace CometOneSolutions\Auth\Request;

use Dingo\Api\Http\FormRequest;

class UpdateUser extends UserRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] User');
    }
}