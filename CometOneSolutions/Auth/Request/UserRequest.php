<?php

namespace CometOneSolutions\Auth\Request;

use Dingo\Api\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:users,name,' . $this->route('userId') ?? null,
            'username' => 'required|unique:users,username,' . $this->route('userId') ?? null,
            'email' => 'required|email',
            'status' => 'required',
            'roles' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'name.unique' => 'Name already exists',
            'username.required' => 'Username is required',
            'username.unique' => 'Username already exists',
            'email.required' => 'Email is required',
            'email.email' => 'Correct/active email format is required e.g. sample@email.com',
            'status.required' => 'Status is required',
            'roles.required' => 'Role is required',
        ];
    }

    public function getName()
    {
        return $this->input('name');
    }

    public function getUsername()
    {
        return $this->input('username');
    }

    public function getEmail()
    {
        return $this->input('email');
    }

    public function getPassword()
    {
        return $this->input('password');
    }

    public function getStatus()
    {
        return $this->input('status');
    }

    public function getRoles()
    {
        return $this->input('roleIds');
    }
}
