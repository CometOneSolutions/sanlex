<?php

namespace CometOneSolutions\Auth\Request;

use Dingo\Api\Http\FormRequest;

class StoreUser extends UserRequest
{

    public function authorize()
    {
        return $this->user()->can('Create [MAS] User');
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            'password'      => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);
    }

    public function messages()
    {
        return array_merge(parent::messages(), [
            'password.required'     => 'Password is required',
            'password.confirmed'    => 'Password does not match',
        ]);
    }
}