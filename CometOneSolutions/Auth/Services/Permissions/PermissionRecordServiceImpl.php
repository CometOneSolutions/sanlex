<?php

namespace CometOneSolutions\Auth\Services\Permissions;

use CometOneSolutions\Auth\Models\Permissions\Permission;
use CometOneSolutions\Auth\Repositories\Permissions\Permissions;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class PermissionRecordServiceImpl extends RecordServiceImpl implements PermissionRecordService
{
    private $permissions;
    private $permission;


    public function __construct(Permissions $permissions, Permission $permission)
    {
        parent::__construct($permissions);
        $this->permissions = $permissions;
        $this->permission = $permission;
    }

    public function create($name)
    {
        $permission = new $this->permission;
        $permission->setName($name);
        $permission->setGuardName('web');
        $this->permissions->save($permission);
        return $permission;
    }
}
