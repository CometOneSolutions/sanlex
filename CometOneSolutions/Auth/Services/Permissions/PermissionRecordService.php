<?php

namespace CometOneSolutions\Auth\Services\Permissions;

use CometOneSolutions\Common\Services\RecordService;

interface PermissionRecordService extends RecordService
{
}
