<?php

namespace CometOneSolutions\Auth\Services\Roles;

use CometOneSolutions\Common\Services\RecordService;

interface RoleRecordService extends RecordService
{
}
