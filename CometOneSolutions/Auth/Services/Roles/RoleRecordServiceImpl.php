<?php

namespace CometOneSolutions\Auth\Services\Roles;

use CometOneSolutions\Auth\Models\Roles\Role;
use CometOneSolutions\Auth\Repositories\Roles\Roles;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class RoleRecordServiceImpl extends RecordServiceImpl implements RoleRecordService
{
    private $roles;
    private $role;

    public function __construct(Roles $roles, Role $role)
    {
        parent::__construct($roles);
        $this->roles = $roles;
        $this->role = $role;
    }
    protected $availableFilters = [
        ['id' => 'name', 'text' => 'Name']
    ];

    public function create($name, array $permissions)
    {
        $role = new $this->role;
        $role->setName($name);
        $role->setGuardName('web');
        $role->setPermissions($permissions);
        $this->roles->save($role);
        return $role;
    }

    public function update(Role $role, array $permissions)
    {
        $tmpRole = clone $role;
        $tmpRole->setPermissions($permissions);
        $this->roles->save($tmpRole);
        return $this->getById($role->getId());
    }

    public function destroy(Role $role)
    {
        $this->roles->delete($role);
        return $role;
    }
}
