<?php

namespace CometOneSolutions\Auth\Services\Users;

use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Auth\Repositories\Users\Users;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class UserRecordServiceImpl extends RecordServiceImpl implements UserRecordService
{
    private $users;
    private $user;

    protected $availableFilters = [
        ['id' => 'name', 'text' => 'Name'],
        ['id' => 'username', 'text' => 'Username']
    ];

    public function __construct(Users $users, User $user)
    {
        parent::__construct($users);
        $this->users = $users;
        $this->user = $user;
    }

    public function create(
        $name,
        $email,
        $username,
        $password,
        $status,
        array $roles = []
    ) {
        $user = new $this->user;
        $user->setName($name);
        $user->setEmail($email);
        $user->setUsername($username);
        $user->setPassword($password);
        $user->setStatus($status);
        $user->setRoles($roles);
        $this->users->save($user);
        return $user;
    }

    public function update(
        User $user,
        $name,
        $email,
        $username,
        $status,
        array $roles = []
    ) {
        $tempUser = clone $user;
        $tempUser->setName($name);
        $tempUser->setEmail($email);
        $tempUser->setUsername($username);
        $tempUser->setStatus($status);
        $tempUser->setRoles($roles);
        $this->users->save($tempUser);
        return $tempUser;
    }

    public function updatePassword(
        User $user,
        $password
    ){
        $tempUser = clone $user;
        $tempUser->setPassword($password);
        $this->users->save($tempUser);
        return $tempUser;
    }

    public function delete(User $user)
    {
        $this->users->delete($user);
        return $user;
    }
}
