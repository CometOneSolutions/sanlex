<?php

namespace CometOneSolutions\Auth\Services\Users;

use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordService;

interface UserAccountRecordService extends RecordService
{
    public function update(User $user, $name, $email, $password);
}
