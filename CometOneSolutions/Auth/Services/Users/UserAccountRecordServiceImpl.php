<?php

namespace CometOneSolutions\Auth\Services\Users;

use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Auth\Repositories\Users\Users;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class UserAccountRecordServiceImpl extends RecordServiceImpl implements UserAccountRecordService
{
    private $users;
    private $user;

    protected $availableFilters = [
        ['id' => 'name', 'text' => 'Name'],
        ['id' => 'username', 'text' => 'Username']
    ];

    public function __construct(Users $users, User $user)
    {
        parent::__construct($users);
        $this->users = $users;
        $this->user = $user;
    }

    public function update(User $user, $name, $email, $password)
    {
    }
}
