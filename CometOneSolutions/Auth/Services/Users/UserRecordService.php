<?php

namespace CometOneSolutions\Auth\Services\Users;

use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordService;

interface UserRecordService extends RecordService
{
    public function create(
        $name,
        $email,
        $username,
        $password,
        $status,
        array $roles = []);

    public function update(User $user,
        $name,
        $email,
        $username,
        $status,
        array $roles = []);

    public function updatePassword(
        User $user,
        $password
    );

    public function delete(User $user);
}
