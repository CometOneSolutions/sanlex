<?php

namespace CometOneSolutions\Auth\Traits;

use CometOneSolutions\Auth\Models\Users\UserModel;
use CometOneSolutions\Auth\Models\Users\User;

trait HasUpdatedByUser
{
    public function updatedByUser()
    {
        return $this->belongsTo(UserModel::class, 'updated_by_user_id');
    }

    public function setUpdatedByUser(User $user)
    {
        $this->updatedByUser()->associate($user);
        $this->updated_by_user_id = $user->getId();
        return $this;
    }

    public function getUpdatedByUser()
    {
        return $this->updatedByUser;
    }
}
