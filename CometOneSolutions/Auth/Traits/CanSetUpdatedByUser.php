<?php

namespace CometOneSolutions\Auth\Traits;

use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Models\Users\User;

trait CanSetUpdatedByUser
{
    public function setUpdatedByUser(UpdatableByUser $updatableByUser, User $user)
    {
        $updatableByUser->setUpdatedByUser($user);
        return $this;
    }
}
