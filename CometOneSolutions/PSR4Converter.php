<?php

namespace CometOneSolutions\;

class PSR4Converter
{

    // Now that you've changed the namespace of the files
    // You want to change the use of the files that used to reference it
    // So you'd need to find instances of use [old_namespace]\ClassName
    // Or alternatively use
    // - used to be from a different namespace
    // - still is from a different namespace

    // As well as the use of the files that used to not need to reference it (from )
    // - used to be from the same namespace

    // As well as remove the usage of files who are now in the same namespace
    // - used to be in a different namespace, now same

    // So, we need to know the old namespace of that file.

    // The limitation here is that it only sees itself from the root? any way to change that?

    public static function capitalizeDirectory($path, $rootPath = "")
    {
        [$beforeRootPath, $afterRootPath] = preg_split(sprintf("/%s/", $rootPath), $path);
        $afterRootPath = implode("/", array_map("ucwords", preg_split("/\//", $afterRootPath)));
        $capitalizedPath = $beforeRootPath . $rootPath . $afterRootPath;
        if ($path !== $capitalizedPath) {
            shell_exec(sprintf('git mv %s %s-temp; git mv %s-temp %s', $path, $path, $path, $capitalizedPath));
        }
        return $capitalizedPath;
    }

    public static function run($rootPath = "app", $rootNamespace = "App\\")
    {
        // Capitalize directories
        // $foo = collect(self::getDirectoryIterator($rootPath))->map(function ($fileInfo) {
        //     return $fileInfo->getPathname();
        // });
        // The thing with this now is you have js directories with small letters
        $directories = collect([]);
        foreach (self::getDirectoryIterator($rootPath) as $iterator) {
            $directories->prepend($iterator[0]);
        }
        $directories->each(function ($directory) use ($rootPath) {
            self::capitalizeDirectory($directory, $rootPath);
        });
        
        $phpFileIterator = self::getPhpFileIterator($rootPath);

        $changes = [];
        
        // Fix namespace
        foreach ($phpFileIterator as $iterator) {
            $filePath = $iterator[0];
            $change = self::fixNamespace($filePath, $rootPath, $rootNamespace);
            if ($change) {
                $changes[$filePath] = $change;
            }
        }
       
        // Fix use statements
        foreach ($phpFileIterator as $iterator) {
            $filePath = $iterator[0];
            self::findUsage($filePath, $rootPath)->each(function ($usageFilePath) use ($filePath) {
                self::fixUseStatement($filePath, $usageFilePath);
            });
        }
       
        // baka hindi lang dapat i-limit sa changes?
        // foreach ($changes as $filePath => $namespaces) {
        //     $changes[$filePath]['usages'] = self::findUsage($filePath, $usagePath);
        // }

        // // baka hindi lang dapat i-limit
        // foreach ($changes as $filePath => $namespaces) {
        //     // fixUseStatement()
        //     return $changes;
        // }
    }

    // This assumes namespace is already correct
    public static function generateUseStatement($filePath)
    {
        return sprintf('use %s\%s;', self::getNamespace($filePath), self::getClassName($filePath));
    }

    // This assumes that it's already the same as the filename
    public static function getClassName($filePath)
    {
        return pathinfo($filePath)['filename'];
    }

    public static function fixUseStatement($usedFilePath, $referencingFilePath)
    {
        $usedFilePathNamespace = self::getNamespace($usedFilePath);
        $referencingFilePathNamespace = self::getNamespace($referencingFilePath);
        $fileContents = file_get_contents($referencingFilePath);
        preg_match("/use .*\b" . self::getClassName($usedFilePath) . "\b;/", $fileContents, $matches);
        // $test = preg_split("/use .*\b" . self::getClassName($usedFilePath) . "\b;/", $fileContents);
        // Remove usage if exists
        if ($referencingFilePathNamespace == $usedFilePathNamespace && count($matches) > 0) {
            $fileContents = preg_replace(
                "/use .*\b" . self::getClassName($usedFilePath) . "\b;/",
                "",
                $fileContents
            );
        } elseif (count($matches) > 0) {
            // Modify the use statement
            $fileContents = preg_replace(
                "/use .*\b" . self::getClassName($usedFilePath) . "\b;/",
                self::generateUseStatement($usedFilePath),
                $fileContents
            );
        } else {
            // Add the use statement
            $namespace = sprintf("namespace %s;", $referencingFilePathNamespace);
            $fileContents = preg_replace(
                '#\\'.$namespace.'#',
                sprintf("%s\n%s", $namespace, self::generateUseStatement($usedFilePath)),
                $fileContents
            );
        }
        file_put_contents($referencingFilePath, $fileContents);
    }


    // Recursively search for the usage of a file in the given root directory
    // This assumes class name is already same as the file name
    // Except for the file itself
    // Note that this will break when same class name, different namespace
    // str_contains is not enough should be an exact match
    public static function findUsage($filePath, $rootDirectory)
    {
        // TODO should we exclude files na walang namespace?
        $rootDirectory = realpath($rootDirectory);
        $phpFilePaths = self::getPhpFileIterator($rootDirectory);
        return collect($phpFilePaths)
            ->keys()
            ->filter(function ($phpFilePath) use ($filePath) {
                if ($phpFilePath == $filePath) {
                    return false;
                }
                return preg_match("~\b" . self::getClassName($filePath) . "\b~", file_get_contents($phpFilePath));
            });
    }

    protected static function getPhpFileIterator($rootPath)
    {
        $rootPath = realpath($rootPath);
        $directory = new \RecursiveDirectoryIterator($rootPath);
        $iterator = new \RecursiveIteratorIterator($directory);
        return new \RegexIterator($iterator, '/^.+\.php$/i', \RecursiveRegexIterator::GET_MATCH);
    }

    public static function getDirectoryIterator($rootPath)
    {
        $rootPath = realpath($rootPath);
        $directory = new \RecursiveDirectoryIterator($rootPath, \RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new \RecursiveIteratorIterator($directory, \RecursiveIteratorIterator::SELF_FIRST);
        return new \RegexIterator($iterator, '/^[^.]+$/', \RecursiveRegexIterator::GET_MATCH);

        // return new \RecursiveIteratorIterator(
        //     new \RecursiveDirectoryIterator($rootPath, \RecursiveDirectoryIterator::SKIP_DOTS),
        //     \RecursiveIteratorIterator::SELF_FIRST,
        //     \RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
        // );
    }

    // One big thing that you missed is you need to add the usages of the Classes that you use
    // That are part of the old namespace
    // Or alternatively run those files in fix usage
    public static function fixNamespace($filePath, $rootNamespaceDirectory, $rootNamespace)
    {
        $oldNamespace = self::getNamespace($filePath);

        // Don't fix files without a namespace
        if ($oldNamespace == null) {
            return false;
        }

        $newNamespace = self::generateNamespace($filePath, $rootNamespaceDirectory, $rootNamespace);

        if ($oldNamespace == $newNamespace) {
            return false;
        }

        self::replaceNamespace($filePath, $newNamespace);
        return [
            'old' => $oldNamespace,
            'new' => $newNamespace
        ];
    }

    public static function getNamespace($filePath)
    {
        $fileContents = file_get_contents($filePath);
        $matches = [];
        
        preg_match("/namespace[^;]*/", $fileContents, $matches);
        if (count($matches) == 0) {
            return null;
        }
        return explode(" ", $matches[0])[1];
    }

    // public static function getNamespace($filePath)
    // {
    //     $fileContents = file_get_contents($filePath);
    //     if (preg_match('#(namespace)(\\s+)([A-Za-z0-9\\\\]+?)(\\s*);#sm', $fileContents, $matches)) {
    //         return $matches[3];
    //     }
    //     return null;
    // }

    private static function replaceNamespace($filePath, $namespace)
    {
        $fileContents = file_get_contents($filePath);
        $fileContents = preg_replace("/namespace[^;]*/", "namespace " . $namespace, $fileContents, 1);
        file_put_contents($filePath, $fileContents);
    }

    public static function generateNamespace($filePath, $namespaceRootDirectory, $namespaceRoot)
    {
        $pathinfo = pathinfo($filePath);
        // what if may similar folder structure? like app/ faceapp/
        // $temp = str_replace("/", "\\", substr(
        //     $filePath,
        //     strpos($filePath, $namespaceRootDirectory),
        //     strlen($pathinfo['basename']) * -1 - 1
        // ));

        $temp = substr(
            $filePath,
            strpos($filePath, $namespaceRootDirectory),
            strlen($pathinfo['basename']) * -1 - 1
        );
        
        $temp = str_replace($namespaceRootDirectory, $namespaceRoot, $temp);

        return str_replace("/", "\\", $temp);
    }
}
