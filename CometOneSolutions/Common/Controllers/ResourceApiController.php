<?php

namespace CometOneSolutions\Common\Controllers;

use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Common\Utils\Filter;
use CometOneSolutions\Common\Utils\Sorter;
use CometOneSolutions\Common\Utils\UriParser;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use League\Fractal\TransformerAbstract;

abstract class ResourceApiController extends Controller
{
    use Helpers;

    protected $service;
    protected $transformer;
    protected $parser;

    const INDEX_LIMIT = 15;

    public function __construct(RecordService $service, TransformerAbstract $transformer)
    {
        $this->middleware(['auth:api']);
        $this->service = $service;
        $this->transformer = $transformer;
        $this->parser = new UriParser();
    }

    public function show($id)
    {
        $item = $this->service->getById($id);
        if (!$item) {
            throw new \Exception('Resource not found.');
        }
        return $this->response->item($item, $this->transformer);
    }

    public function index(Request $request)
    {
        $this->parser->setup($request);
        $filters = $this->getFiltersFromParser($this->parser);
        $sorters = $this->getSortersFromParser($this->parser);
        $limit = $this->getLimitFromParser($this->parser)['value'] ?? static::INDEX_LIMIT;
        $paginated = $this->service->getPaginated($limit, $filters, $sorters, []);
        return $this->response->paginator($paginated, $this->transformer);
    }

    private function getFiltersFromParser(UriParser $parser)
    {
        $filterArrays = $parser->whereParameters();
        return array_map(function ($filterArray) {
            return new Filter($filterArray['key'], $filterArray['value'], $filterArray['operator']);
        }, $filterArrays);
    }

    private function getSortersFromParser(UriParser $parser)
    {
        if ($this->parser->hasQueryParameter('sort')) {
            $parameter = $this->parser->queryParameter('sort');
            $fields = explode(',', $parameter['value']);

            return array_map(function ($field) {
                $direction = 'ASC';
                if ($field[0] == '-') {
                    $field = substr($field, 1);
                    $direction = 'DESC';
                }
                return new Sorter($field, $direction);
            }, $fields);
            // TODO what is the sorter for the last?
            // $sorters[$field] = $direction;
            // $sorters['id'] = $direction;
        }

        return [];
    }

    private function getLimitFromParser(UriParser $parser)
    {
        if ($this->parser->hasQueryParameter('limit')) {
            return $this->parser->queryParameter('limit');
        }
        return null;
    }
}
