<?php

namespace CometOneSolutions\Common\Controllers;

use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Common\Utils\Export;
use CometOneSolutions\Common\Utils\Filter;
use CometOneSolutions\Common\Utils\Sorter;
use CometOneSolutions\Common\Utils\UriParser;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportableController extends Controller
{
    protected $exportFileName = 'exported.xlsx';
    protected $recordService;
    protected $parser;

    public function __construct(RecordService $recordService)
    {
        $this->recordService = $recordService;
        $this->parser = new UriParser();
    }

    public function export(Request $request)
    {
        $this->parser->setup($request);
        $filters = $this->getFiltersFromParser($this->parser);
        $sorters = $this->getSortersFromParser($this->parser);
        return Excel::download(new Export($this->recordService->getAll($filters, $sorters), $this->exportView), $this->exportFileName);
    }

    private function getFiltersFromParser(UriParser $parser)
    {
        $filterArrays = $parser->whereParameters();
        return array_map(function ($filterArray) {
            return new Filter($filterArray['key'], $filterArray['value'], $filterArray['operator']);
        }, $filterArrays);
    }

    private function getSortersFromParser(UriParser $parser)
    {
        if ($this->parser->hasQueryParameter('sort')) {
            $parameter = $this->parser->queryParameter('sort');
            $fields = explode(',', $parameter['value']);

            return array_map(function ($field) {
                $direction = 'ASC';
                if ($field[0] == '-') {
                    $field = substr($field, 1);
                    $direction = 'DESC';
                }
                return new Sorter($field, $direction);
            }, $fields);
        }

        return [];
    }
}
