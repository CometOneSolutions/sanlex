<?php

namespace CometOneSolutions\Common\Models;

use Illuminate\Database\Eloquent\Model;

// https://stackoverflow.com/questions/27204485/synchronizing-a-one-to-many-relationship-in-laravel
abstract class C1Model extends Model
{
    // /**
    //  * Overrides the default Eloquent hasMany relationship to return a BelongsToManySyncable.
    //  *
    //  * {@inheritDoc}
    //  * @return \CometOneSolutions\Common\BelongsToManySyncable
    //  */
    // public function belongsToMany()
    // {
    // }

    /**
     * Overrides the default Eloquent hasMany relationship to return a HasManySyncable.
     *
     * {@inheritDoc}
     * @return \CometOneSolutions\Common\HasManySyncable
     */
    public function hasMany($related, $foreignKey = null, $localKey = null)
    {
        $instance = $this->newRelatedInstance($related);

        $foreignKey = $foreignKey ?: $this->getForeignKey();

        $localKey = $localKey ?: $this->getKeyName();

        return new HasManySyncable(
            $instance->newQuery(),
            $this,
            $instance->getTable() . '.' . $foreignKey,
            $localKey
        );
    }
}
