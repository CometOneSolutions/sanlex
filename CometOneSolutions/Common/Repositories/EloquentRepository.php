<?php

namespace CometOneSolutions\Common\Repositories;

abstract class EloquentRepository implements Repository
{
    protected $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function __clone()
    {
        $this->model = clone $this->model;
    }

    public function deleteAll()
    {
        return $this->model->delete();
    }

    public function whereIn($needle, $haystack)
    {
        $this->model = $this->model->whereIn($needle, $haystack);
        return $this;
    }

    public function take(int $value)
    {
        $this->model = $this->model->take($value);
        return $this;
    }

    public function getById(int $id)
    {
        return $this->model->find($id);
    }

    public function sum($field)
    {
        return $this->model->sum($field);
    }

    public function orderBy($key, $direction = 'ASC')
    {
        $orderMethod = camel_case('orderBy' . ucfirst($key));

        if (method_exists($this, $orderMethod)) {
            $this->model = $this->$orderMethod($direction);
        } else {
            $this->model = $this->model->orderBy($key, $direction);
        }
        return $this;
    }

    public function where($key, $value, $operator = '=')
    {
        $filterMethod = camel_case('filterBy' . ucfirst($key));

        if (method_exists($this, $filterMethod)) {
            $this->model = $this->$filterMethod($value, $operator);
        } else {
            if (strtoupper($value) == 'NULL') {
                if ($operator == '=') {
                    $this->model = $this->model->whereNull($key);
                } elseif ($operator == '!=') {
                    $this->model = $this->model->whereNotNull($key);
                }
            } else {
                $this->model = $this->model->where($key, $operator, $value);
            }
        }
        return $this;
    }

    public function orWhere($key, $value, $operator = '=')
    {
        $this->model = $this->model->orWhere($key, $operator, $value);
        return $this;
    }

    public function deleteById($id)
    {
        return $this->model->find($id)->delete();
    }

    public function getFirst()
    {
        return $this->model->first();
    }

    public function count()
    {
        return $this->model->count();
    }

    public function getAll()
    {
        return $this->model->get();
    }

    public function getPaginated($limit)
    {
        return $this->model->paginate($limit);
    }
}
