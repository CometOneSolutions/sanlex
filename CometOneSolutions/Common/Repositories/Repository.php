<?php

namespace CometOneSolutions\Common\Repositories;

interface Repository
{
    public function count();

    public function deleteAll();

    public function getById(int $id);

    public function getAll();

    public function getPaginated($limit);

    public function getFirst();

    public function take(int $value);

    public function orderBy($field, $direction);

    public function where($key, $operator, $value);

    public function orWhere($key, $value, $operator = '=');

    public function whereIn($needle, $haystack);

    public function deleteById($id);

    public function sum($field);
}
