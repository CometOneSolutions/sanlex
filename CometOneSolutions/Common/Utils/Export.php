<?php

namespace CometOneSolutions\Common\Utils;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class Export implements FromView, ShouldAutoSize
{
    protected $view;
    protected $data;

    public function __construct(
        $data,
        $view
    ) {
        $this->data = $data;
        $this->view = $view;
    }

    public function view(): View
    {
        $data = $this->data;
        return view($this->view, compact('data'));
    }
}
