<?php

namespace CometOneSolutions\Common\Utils;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Utils\Filter;
use CometOneSolutions\Common\Utils\Sorter;
use CometOneSolutions\Common\Utils\UriParser;

trait UriParserHelper
{
    protected function getFiltersFromRequest(Request $request)
    {
        $parser = new UriParser();
        $parser->setup($request);
        return array_map(function ($filterArray) {
            return new Filter($filterArray['key'], $filterArray['value'], $filterArray['operator']);
        }, $parser->whereParameters());
    }

    protected function getSorterArraysFromRequest(Request $request)
    {
        // u r here
        // return array_map(function ($sorterObject) use ($params) {
        //     return $sorterObject->toArray(...$params);
        // }, $this->getSortersFromRequest($request));
    }

    protected function getFilterArraysFromRequest(Request $request, ?string $keyName = null, ?string $valueName = null, ?string $operatorName = null)
    {
        $params = array_filter([$keyName, $valueName, $operatorName], function ($param) {
            return $param != null;
        });

        return array_map(function ($filterObject) use ($params) {
            return $filterObject->toArray(...$params);
        }, $this->getFiltersFromRequest($request));
    }

    protected function getSortersFromRequest(Request $request)
    {
        $parser = new UriParser();
        $parser->setup($request);

        if ($parser->hasQueryParameter('sort')) {
            $parameter = $parser->queryParameter('sort');
            $fields = explode(',', $parameter['value']);

            return array_map(function ($field) {
                $direction = 'ASC';
                if ($field[0] == '-') {
                    $field = substr($field, 1);
                    $direction = 'DESC';
                }
                return new Sorter($field, $direction);
            }, $fields);
        }

        return [];
    }

    protected function getLimitFromRequest(Request $request)
    {
        $parser = new UriParser();
        $parser->setup($request);
        if ($parser->hasQueryParameter('limit')) {
            return $parser->queryParameter('limit');
        }
        return null;
    }
}
