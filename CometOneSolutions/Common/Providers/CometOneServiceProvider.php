<?php

namespace CometOneSolutions\Common\Providers;

use CometOneSolutions\Common\Exceptions\ApiException;
use Illuminate\Support\ServiceProvider;

class CometOneServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app('Dingo\Api\Exception\Handler')->register(function (ApiException $apiException) {
            return \Response::make([
                'modal' => $apiException->isModal(),
                'title' => $apiException->getTitle(),
                'code' => $apiException->getInternalCode(),
                'errors' => $apiException->getErrorMessages(),
                'message' => $apiException->getMessage(),
                'status_code' => $apiException->getStatusCode(),
            ], $apiException->getStatusCode());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(base_path('CometOneSolutions/Common/Helpers/*.php')) as $filename) {
            require_once $filename;
        }
    }
}
