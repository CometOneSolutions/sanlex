<?php

namespace CometOneSolutions\Common\Services;

interface RecordService
{
    public function getById(int $id);

    public function getManyByIds(array $ids);

    public function getAll($filters = [], $sorters = [], $columns = []);

    public function getPaginated($perPage, $filters = [], $sorters = [], $columns = []);

    public function getFirst($filters = [], $sorters = [], $columns = []);

    public function getSum($field, array $filters = []);

    public function getAvailableFilters();
}
