<?php

namespace CometOneSolutions\Common\Services;

use CometOneSolutions\Common\Repositories\Repository;

abstract class RecordServiceImpl implements RecordService
{
    private $repository;

    protected $availableFilters = [];

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function getAvailableFilters()
    {
        return $this->availableFilters;
    }

    protected static function getInstance()
    {
        return resolve(static::class);
    }

    public function getById(int $id)
    {
        return $this->repository->getById($id);
    }

    public function getManyByIds(array $ids = [])
    {
        $repoClone = clone $this->repository;
        return $repoClone->whereIn('id', $ids)->getAll();
    }

    public function getSum($field, array $filters = [])
    {
        $repoClone = clone $this->repository;
        $this->filter($filters, $repoClone);
        return $repoClone->sum($field);
    }

    public function getAll($filters = [], $sorters = [], $columns = [], $take = null)
    {
        $repoClone = clone $this->repository;
        $this->filter($filters, $repoClone);
        $this->sort($sorters, $repoClone);
        $this->columns($columns, $repoClone);
        if ($take) {
            $repoClone = $repoClone->take($take);
        }
        return $repoClone->getAll();
    }

    public function getPaginated($perPage, $filters = [], $sorters = [], $columns = [])
    {
        $repoClone = clone $this->repository;
        $this->filter($filters, $repoClone);
        $this->sort($sorters, $repoClone);
        $this->columns($columns, $repoClone);
        return $repoClone->getPaginated($perPage);
    }

    public function getFirst($filters = [], $sorters = [], $columns = [])
    {
        $repoClone = clone $this->repository;
        $this->filter($filters, $repoClone);
        $this->sort($sorters, $repoClone);
        $this->columns($columns, $repoClone);
        return $repoClone->getFirst();
    }

    protected function columns(array $columnns = [], Repository $repository)
    {
    }

    protected function sort(array $sorters = [], Repository $repository)
    {
        foreach ($sorters as $sorter) {
            $repository = $repository->orderBy($sorter->getKey(), $sorter->getDirection());
        }
    }

    protected function filter(array $filters = [], Repository $repository)
    {
        foreach ($filters as $filter) {
            $repository = $repository->where($filter->getKey(), $filter->getValue(), $filter->getOperator());
        }
    }
}
