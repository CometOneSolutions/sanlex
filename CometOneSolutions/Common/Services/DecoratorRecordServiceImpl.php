<?php

namespace CometOneSolutions\Common\Services;

abstract class DecoratorRecordServiceImpl implements RecordService
{
    protected $decoratedRecordService;

    public function __construct(RecordService $decoratedRecordService)
    {
        $this->decoratedRecordService = $decoratedRecordService;
    }

    public function getAvailableFilters()
    {
        return $this->decoratedRecordService->getAvailableFilters();
    }

    public function getById(int $id)
    {
        return $this->decoratedRecordService->getById($id);
    }

    public function getManyByIds(array $ids = [])
    {
        return $this->decoratedRecordService->getManyByIds($ids);
    }

    public function getSum($field, array $filters = [])
    {
        return $this->decoratedRecordService->getSum($field, $filters);
    }

    public function getAll($filters = [], $sorters = [], $columns = [], $take = null)
    {
        return $this->decoratedRecordService->getAll($filters, $sorters, $columns, $take);
    }

    public function getPaginated($perPage, $filters = [], $sorters = [], $columns = [])
    {
        return $this->decoratedRecordService->getPaginated($perPage, $filters, $sorters, $columns);
    }

    public function getFirst($filters = [], $sorters = [], $columns = [])
    {
        return $this->decoratedRecordService->getFirst($filters, $sorters, $columns);
    }
}
