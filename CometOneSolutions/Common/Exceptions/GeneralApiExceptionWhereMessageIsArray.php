<?php

namespace CometOneSolutions\Common\Exceptions;

class GeneralApiExceptionWhereMessageIsArray extends ApiException
{
    public function __construct($message = [], $title = '', $statusCode = 400, $isModal = false, array $errors = [], $internalCode = '', \Exception $previous = null)
    {
        $convertedToStringMessage = '';
        $count = count($message);
        for($x = 0; $x <= $count-1; $x++) {
            $convertedToStringMessage .= $message[$x];
        }
        
        $this->message = $convertedToStringMessage;
        $this->title = $title;
        $this->statusCode = $statusCode;
        $this->internalCode = $internalCode;
        $this->isModal = $isModal;
        $this->errors = $errors;
        parent::__construct($previous);
    }
}
