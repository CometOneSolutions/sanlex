<?php

namespace CometOneSolutions\Common\Traits;

use CometOneSolutions\Common\Utils\Filter;

trait CanGetByName
{
    public static function getByName($name, $field = 'name')
    {
        $nameFilter = new Filter($field, $name);
        return static::getInstance()->getFirst([$nameFilter]);
    }
}
