import { Form } from "./components/Form";
import { Money } from "v-money";

let format = require("format-number");
let money = format({ decimalsSeparator: ".", round: 2, padRight: 2 });

export const serviceBus = new Vue({
    data: function() {
        return {
            form: new Form({}),
            peso: {
                decimal: ".",
                thousands: ",",
                prefix: "₱ ",
                precision: 2,
                masked: false
            },
            dollar: {
                decimal: '.',
                thousands: ',',
                prefix: '$ ',
                precision: 2,
                masked: false
            },
            integer: {
                precision: 0,
                thousands: ",",
                masked: false
            },
            // uri: {
            //     purchaseOrder: {
            //         base: "/api/purchase-orders"
            //     }
            // }
        };
    },

    methods: {
        money(value) {
            return money(value);
        },
        

        // cost(value) {
        //     return cost(value);
        // },

        // isArrayInitialized(selections) {
        //     if (selections && selections.length) {
        //         return selections.length > 0;
        //     }
        //     return false;
        // },

        
    }
});
