import Index from "./components/Index";
import { Form } from '@c1_common_js/components/Form';
import VForm from '@c1_common_js/components/VForm';
import Select3 from "@c1_common_js/components/Select3";
import SaveButton from '@c1_common_js/components/SaveButton';

const MT = 'MT';
const PIECE = 'PIECE';
new Vue({
    el: "#index",
    components: { Index, VForm, Select3, SaveButton },
    data: {
        filterable: filterable,
        baseUrl: baseUrl,
        items: [],
        totals: {},
        isLoading: true,
        toLastPage: typeof toLastPage !== "undefined" ? toLastPage : false,
        sorter: sorter,
        sortAscending: typeof sortAscending !== "undefined" ? sortAscending : true,
        filters: typeof filters !== "undefined" ? filters : [],
        // conditions: (typeof conditions !== undefined) ? conditions : [],
        // conditions: conditions || [],
        isSaving: false,
        url: '/api/products?productable_type=App\\Modules\\Coil\\Models\\CoilModel',

        form: new Form({
            productId: null,
            quantity: 0,
            identifier: null,
            details: [],
            adjustedQuantity: null,
            remarks: null
        }),
        moneyConfig: {
            precision: 3
        }
    },
    methods: {

        setSorter(sorter) {
            if (sorter == this.sorter) this.sortAscending = !this.sortAscending;
            else this.sortAscending = true;
            this.sorter = sorter;
        },

        getSortIcon(column) {
            return {
                "fas fa-sort-up": column == this.sorter && this.sortAscending,
                "fas fa-sort-down": column == this.sorter && !this.sortAscending,
                "fas fa-sort": column != this.sorter
            };
        },
        approveAllJo() {
            let url = '/api/job-orders/approve/all';
            this.form.affirm().then((result) => {
                if (result.value) {
                    this.form.post(url).then(
                        response => {
                            this.$swal({
                                title: 'Done',
                                html: response.message,
                                type: 'info'
                            }).then(() => window.location = '/out/job-orders');
                        }
                    )
                }
            });
        },
        approveAllDr() {
            let url = '/api/delivery-receipts/approve/all';
            this.form.affirm().then((result) => {
                if (result.value) {
                    this.form.post(url).then(
                        response => {
                            this.$swal({
                                title: 'Done',
                                html: response.message,
                                type: 'info'
                            }).then(() => window.location = '/out/delivery-receipts');
                        }
                    )
                }
            });
        },
        toggleToDamaged(inventoryDetail) {
            let url = '/inventory/stock/';
            if (inventoryDetail.type == MT) {
                url += 'coil'
            } else {
                url += 'accessory'
            }
            this.form.confirmDamaged().then((result) => {
                if (result.value) {

                    this.form.patch('/api/inventories/' + inventoryDetail.id + '/damaged').then(
                        response => {

                            this.$swal({
                                title: 'Success',
                                text: 'Item was successfully marked as damaged.',
                                type: 'success'
                            }).then(() => window.location = url);

                        })
                }
            });
        },

        toggleToGood(inventoryDetail) {

            this.form.confirmGood().then((result) => {
                if (result.value) {

                    this.form.patch('/api/inventories/' + inventoryDetail.id + '/good').then(
                        response => {

                            this.$swal({
                                title: 'Success',
                                text: 'Item was successfully marked as good.',
                                type: 'success'
                            }).then(() => window.location = '/inventory/stock/damaged');

                        })
                }
            });
        },
        store() {
            let url = '/api/inventories/';


            this.form.post(url)
                .then(response => {

                    this.$swal({
                        title: 'Success',
                        text: 'New item/s were saved.',
                        type: 'success'
                    }).then(() => window.location = '/inventory/stock/coil');

                })



        },
        adjust(inventoryDetail) {
            let url = '/inventory/stock/';
            if (inventoryDetail.type == MT) {
                url += 'coil'
            } else {
                url += 'accessory'
            }
            this.form.adjustedQuantity = inventoryDetail.adjustedQuantity;
            this.form.post('/api/inventories/' + inventoryDetail.id + '/adjust')
                .then(response => {

                    this.$swal({
                        title: 'Success',
                        text: 'Inventory balance was adjusted.',
                        type: 'success'
                    }).then(() => window.location = url);

                })
        },
        addToInventory() {
            this.form.details.push({
                quantity: 0,
                identifier: null,
            });
        },
        removeFromDetails(index) {
            this.form.details.splice(index, 1);
        }
    }



});
