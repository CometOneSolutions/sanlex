<?php
// For Model Has Roles
DB::table('model_has_roles')->where('model_type', "CometOneSolutions\\Auth\\UserModel")->update(['model_type' => \CometOneSolutions\Auth\Models\Users\UserModel::class]);

//For Products
\App\Modules\Product\Models\ProductModel::all()->each(function ($product) {
    $productableType = $product->productable_type;

    switch ($productableType) {
        case "Sanlex\\CoilModel":
            $product->productable_type = \App\Modules\Coil\Models\CoilModel::class;
            break;
        case "Sanlex\\AccessoryModel":
            $product->productable_type = \App\Modules\Accessory\Models\AccessoryModel::class;
            break;
        case "Sanlex\\InsulationModel":
            $product->productable_type = \App\Modules\Insulation\Models\InsulationModel::class;
            break;
        case "Sanlex\\ScrewModel":
            $product->productable_type = \App\Modules\Screw\Models\ScrewModel::class;
            break;
        case "Sanlex\\SkylightModel":
            $product->productable_type = \App\Modules\Skylight\Models\SkylightModel::class;
            break;
        case "Sanlex\\MiscAccessoryModel":
            $product->productable_type = \App\Modules\MiscAccessory\Models\MiscAccessoryModel::class;
            break;
        case "Sanlex\\StainlessSteelModel":
            $product->productable_type = \App\Modules\StainlessSteel\Models\StainlessSteelModel::class;
            break;
        case "Sanlex\\StructuralSteelModel":
            $product->productable_type = \App\Modules\StructuralSteel\Models\StructuralSteelModel::class;
            break;
        case "Sanlex\\PaintAdhesiveModel":
            $product->productable_type = \App\Modules\PaintAdhesive\Models\PaintAdhesiveModel::class;
            break;
        case "Sanlex\\FastenerModel":
            $product->productable_type = \App\Modules\Fastener\Models\FastenerModel::class;
            break;
        case "Sanlex\\PvcFittingModel":
            $product->productable_type = \App\Modules\PvcFitting\Models\PvcFittingModel::class;
            break;
    }

    $product->save();
});


//For Item Movements 
\CometOneSolutions\Inventory\Model\ItemMovementModel::all()->each(function ($itemMovement) {
    $inventoriableType = $itemMovement->inventoriable_type;

    switch ($inventoriableType) {
        case "Sanlex\\ReceivingModel":
            $itemMovement->inventoriable_type = \App\Modules\Receiving\Models\ReceivingModel::class;
            break;
        case "Sanlex\\JobOrderDetailModel":
            $itemMovement->inventoriable_type = \App\Modules\JobOrders\Models\JobOrderDetailModel::class;
            break;
        case "Sanlex\\AdjustmentModel":
            $itemMovement->inventoriable_type = \App\Modules\Adjustment\Models\AdjustmentModel::class;
            break;
        case "Sanlex\\InventoryDetailModel":
            $itemMovement->inventoriable_type = \App\Modules\InventoryDetail\Models\InventoryDetailModel::class;
            break;
        case "Sanlex\\ReceivingAccessoryDetailModel":
            $itemMovement->inventoriable_type = \App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetailModel::class;
            break;
        case "Sanlex\\DeliveryReceiptDetailModel":
            $itemMovement->inventoriable_type = \App\Modules\DeliveryReceiptDetails\Models\DeliveryReceiptDetailModel::class;
            break;
    }

    $itemMovement->save();
});
