<?php

//Move po to po accessory
function mapPurchaseOrderAccessoryDetails($purchaseOrderDetails)
{
    return $purchaseOrderDetails->map(function ($purchaseOrderDetail) {
        $purchaseOrderAccessoryDetail = new PurchaseOrderAccessoryDetailModel;
        $purchaseOrderAccessoryDetail->setUom($purchaseOrderDetail->getUom());
        $purchaseOrderAccessoryDetail->setProduct($purchaseOrderDetail->getProduct());
        $purchaseOrderAccessoryDetail->setOrderedQuantity($purchaseOrderDetail->getOrderedQuantity());

        return $purchaseOrderAccessoryDetail;
    });
}

function mapPurchaseAccessory($purchaseOrder)
{
    $purchaseOrderAccessory = new PurchaseOrderAccessoryModel;
    $purchaseOrderAccessory->setPONumber($purchaseOrder->getPoNumber());
    $purchaseOrderAccessory->setSupplier($purchaseOrder->getSupplier());
    $purchaseOrderAccessory->setProductType($purchaseOrder->getProductType());
    $purchaseOrderAccessory->setDate($purchaseOrder->getDate());
    $purchaseOrderAccessory->setRemarks($purchaseOrder->getRemarks());
    $purchaseOrderAccessory->setStatus('CLOSED');
    $purchaseOrderAccessory->created_at = $purchaseOrder->created_at;
    $purchaseOrderAccessory->updated_at = $purchaseOrder->updated_at;

    return $purchaseOrderAccessory;
}

function mapReceivingAccessory($purchaseOrderAccessory)
{
    $receivingAccessory = new ReceivingAccessoryModel;
    $receivingAccessory->setReferenceNo('Receiving For ' . $purchaseOrderAccessory->getPoNumber());
    $receivingAccessory->setDate($purchaseOrderAccessory->getDate());
    $receivingAccessory->setStatus('Received');
    $receivingAccessory->setPurchaseOrderAccessory($purchaseOrderAccessory);
    $receivingAccessory->created_at = $purchaseOrderAccessory->created_at;
    $receivingAccessory->updated_at = $purchaseOrderAccessory->updated_at;

    return $receivingAccessory;
}

function mapReceivingAccessoryDetails($receivings)
{
    return $receivings->map(function ($receiving) {
        $receivingAccessoryDetail = new ReceivingAccessoryDetailModel;
        $receivingAccessoryDetail->setOldReceiving($receiving);
        $receivingAccessoryDetail->setProduct($receiving->getProduct());
        $receivingAccessoryDetail->setQuantity($receiving->getQuantity());

        return $receivingAccessoryDetail;
    });
}

function createPurchaseOrderAccessory($purchaseOrderToMove)
{
    $purchaseOrderAccessory = mapPurchaseAccessory($purchaseOrderToMove);
    $purchaseOrderAccessory->save();

    $purchaseOrderAccessory->purchaseOrderAccessoryDetails()->sync(
        mapPurchaseOrderAccessoryDetails($purchaseOrderToMove->getPurchaseOrderDetails())
    );

    return $purchaseOrderAccessory;
}

function createReceivingAccessory($purchaseOrderAccessory, $purchaseOrderToMove)
{
    $receivingAccessory = mapReceivingAccessory($purchaseOrderAccessory);
    $receivingAccessory->save();

    $receivingAccessory->receivingAccessoryDetails()->sync(
        mapReceivingAccessoryDetails($purchaseOrderToMove->getReceivings())
    );

    return $receivingAccessory;
}

function setInventoryDetailReceivingAccessory($receivingAccessoryDetails)
{
    $receivingAccessoryDetails->each(function ($receivingAccessoryDetail) {
        $receivingAccessoryDetail->getOldReceiving()->getInventoryDetails()->each(function ($inventoryDetail) use ($receivingAccessoryDetail) {
            $inventoryDetail->unSetReceiving();
            $inventoryDetail->setReceivingAccessoryDetail($receivingAccessoryDetail);
            $inventoryDetail->save();
        });
    });
}

function setItemMovementReceivingToReceivingAccessory($receivingAccessoryDetails)
{
    $receivingAccessoryDetails->each(function ($receivingAccessoryDetail) {
        $receivingAccessoryDetail->getOldReceiving()->getItemMovements()->each(function ($itemMovement) use ($receivingAccessoryDetail) {
            $itemMovement->setInventoriable($receivingAccessoryDetail);
            $itemMovement->save();
        });
    });
}

function deleteReceivings($receivings)
{
    $receivings->each(function ($receiving) {
        $receiving->delete();
    });
}

function deletePurchaseOrder($purchaseOrder)
{
    $purchaseOrder->delete();
}

$purchaseOrdersToMove = PurchaseOrderModel::where('product_type', '<>', 'Coils')->get();

$purchaseOrdersToMove->each(function ($purchaseOrderToMove) {
    $purchaseOrderAccessory     = createPurchaseOrderAccessory($purchaseOrderToMove);
    $receivingAccessory         = createReceivingAccessory($purchaseOrderAccessory, $purchaseOrderToMove);

    setItemMovementReceivingToReceivingAccessory($receivingAccessory->getReceivingAccessoryDetails());
    setInventoryDetailReceivingAccessory($receivingAccessory->getReceivingAccessoryDetails());

    deleteReceivings($purchaseOrderToMove->getReceivings());
    deletePurchaseOrder($purchaseOrderToMove);
});


//Recompute inventory stocks  (for testing)
// $inventories = InventoryModel::all()->each(function ($inventory) {

//     $inventory->getInventoryDetails()->each(function ($inventoryDetail) {
//         $inventoryDetail->receivingAccessoryDetail()->exists() ? $inventoryDetail->updateAccessoryStock() : $inventoryDetail->updateStock();
//         $inventoryDetail->save();
//     });

//     $inventory->updateStock();
//     $inventory->save();
// });

// 1213043.983 old inventory stock (before changes)

// must be the same after applying recompute script
