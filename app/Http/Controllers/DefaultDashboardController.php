<?php

namespace App\Http\Controllers;


use App\Modules\DeliveryReceipt\Services\DeliveryReceiptRecordService;
use App\Modules\JobOrders\Services\JobOrderRecordService;
use App\Modules\PurchaseOrderAccessory\Services\PurchaseOrderAccessoryRecordService;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\PurchaseOrder\Services\PurchaseOrderRecordService;
use CometOneSolutions\Common\Utils\Filter;
use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;


class DefaultDashboardController extends Controller
{
    private $jobOrderRecordService;
    private $purchaseOrderRecordService;
    private $deliveryReceiptRecordService;
    private $purchaseOrderAccessoryRecordService;

    public function __construct(
        JobOrderRecordService $jobOrderRecordService,
        PurchaseOrderRecordService $purchaseOrderRecordService,
        DeliveryReceiptRecordService $deliveryReceiptRecordService,
        PurchaseOrderAccessoryRecordService $purchaseOrderAccessoryRecordService
    )
    {

        $this->jobOrderRecordService = $jobOrderRecordService;

        $this->purchaseOrderRecordService = $purchaseOrderRecordService;
        $this->deliveryReceiptRecordService = $deliveryReceiptRecordService;
        $this->purchaseOrderAccessoryRecordService = $purchaseOrderAccessoryRecordService;
    }

    public function dashboard()
    {
//        dd(resolve(JobOrderRecordService::class));
        $jobOrderFilter = new Filter('status', 'Draft');
        $pendingJobOrdersCount = count($this->jobOrderRecordService->getAll([$jobOrderFilter]));

        $purchaseOrderFilter = new Filter('status', 'In Transit');
        $pendingPurchaseOrderCount = count($this->purchaseOrderRecordService->getAll([$purchaseOrderFilter]));

        $purchaseOrderAccessoryFilter = new Filter('status', PurchaseOrderAccessory::STATUS_OPEN);
        $openPurchaseOrderAccessoryCount = count($this->purchaseOrderAccessoryRecordService->getAll([$purchaseOrderAccessoryFilter]));

        $deliveryReceiptFilter = new Filter('status', 'Draft');
        $pendingDeliveryReceiptsCount = count($this->deliveryReceiptRecordService->getAll([$deliveryReceiptFilter]));

        return view('dashboard', compact('pendingJobOrdersCount', 'pendingPurchaseOrderCount', 'openPurchaseOrderAccessoryCount', 'pendingDeliveryReceiptsCount'));
    }
}
