<?php

namespace App\Http\Controllers;


use App\Modules\Inventory\Services\InventoryRecordService;


class InventoryMenuDashboardController extends Controller
{
    protected $inventoryRecordService;
    public function __construct(InventoryRecordService $inventoryRecordService)
    {
        $this->inventoryRecordService = $inventoryRecordService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('master.inventory.dashboard');
    }
}
