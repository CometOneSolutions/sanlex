<?php

namespace App\Http\Controllers;

class ReportMenuDashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:Read [REP] Reports')->only(['show', 'index']);
    }

    public function dashboard()
    {
        return view('master.report.dashboard');
    }
}
