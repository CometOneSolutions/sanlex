<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SalesMenuDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('master.sales.dashboard');
    }

}
