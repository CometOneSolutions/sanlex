<?php

namespace App\Http\Controllers;

class OutMenuDashboardController extends Controller
{
    public function dashboard()
    {
        return view('master.out.dashboard');
    }
}
