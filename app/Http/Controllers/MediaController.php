<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\MediaLibrary\Media;

class MediaController extends Controller
{
    //

    public function getFile($mediaId)
    {
        $media = Media::find($mediaId);
        return response()->file($media->getPath());
    }

    public function downloadFile($mediaId)
    {
        $media = Media::find($mediaId);
        return response()->download($media->getPath());
    }

    public function delete($mediaId)
    {
        $media = Media::find($mediaId);
        $media->delete();
        return response('success', 200);
    }
}
