<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SanlexServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        app('api.transformer')->getAdapter()->disableEagerLoading();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(CustomerRecordService::class, CustomerRecordServiceImpl::class);
        $this->app->bind(CustomerRepository::class, EloquentCustomerRepository::class);
        $this->app->bind(Customer::class, CustomerModel::class);

        $this->app->bind(SupplierRecordService::class, SupplierRecordServiceImpl::class);
        $this->app->bind(SupplierRepository::class, EloquentSupplierRepository::class);
        $this->app->bind(Supplier::class, SupplierModel::class);

        

        // $this->app->bind(PartnerTypeRecordService::class, PartnerTypeRecordServiceImpl::class);
        // $this->app->bind(PartnerTypeRepository::class, EloquentPartnerTypeRepository::class);
        // $this->app->bind(PartnerType::class, PartnerTypeModel::class);

        // $this->app->bind(SupplierPartnershipRecordService::class, SupplierPartnershipRecordServiceImpl::class);
        // $this->app->bind(SupplierPartnership::class, SupplierPartnershipModel::class);

        // $this->app->bind(PaperTypeRecordService::class, PaperTypeRecordServiceImpl::class);
        // $this->app->bind(PaperTypeRepository::class, EloquentPaperTypeRepository::class);
        // $this->app->bind(PaperType::class, PaperTypeModel::class);

        $this->app->bind(ProductRecordService::class, ProductRecordServiceImpl::class);
        $this->app->bind(ProductRepository::class, EloquentProductRepository::class);
        $this->app->bind(Product::class, ProductModel::class);

        // $this->app->bind(InternalAccountRecordService::class, InternalAccountRecordServiceImpl::class);
        // $this->app->bind(InternalAccountRepository::class, EloquentInternalAccountRepository::class);
        // $this->app->bind(InternalAccount::class, InternalAccountModel::class);

        // $this->app->bind(CurrencyRecordService::class, CurrencyRecordServiceImpl::class);
        // $this->app->bind(CurrencyRepository::class, EloquentCurrencyRepository::class);
        // $this->app->bind(Currency::class, CurrencyModel::class);

        $this->app->bind(UomRecordService::class, UomRecordServiceImpl::class);
        $this->app->bind(UomRepository::class, EloquentUomRepository::class);
        $this->app->bind(Uom::class, UomModel::class);

        // $this->app->bind(CurrencyExchangeRecordService::class, CurrencyExchangeRecordServiceImpl::class);
        // $this->app->bind(CurrencyExchangeRepository::class, EloquentCurrencyExchangeRepository::class);
        // $this->app->bind(CurrencyExchange::class, CurrencyExchangeModel::class);

        // $this->app->bind(WithdrawalRecordService::class, WithdrawalRecordServiceImpl::class);
        // $this->app->bind(WithdrawalRepository::class, EloquentWithdrawalRepository::class);
        // $this->app->bind(Withdrawal::class, WithdrawalModel::class);

        // $this->app->bind(TransferRecordService::class, TransferRecordServiceImpl::class);
        // $this->app->bind(TransferRepository::class, EloquentTransferRepository::class);
        // $this->app->bind(Transfer::class, TransferModel::class);

        $this->app->bind(PurchaseOrderRecordService::class, PurchaseOrderRecordServiceImpl::class);
        $this->app->bind(PurchaseOrderRepository::class, EloquentPurchaseOrderRepository::class);
        $this->app->bind(PurchaseOrder::class, PurchaseOrderModel::class);

        $this->app->bind(PackingListRecordService::class, PackingListRecordServiceImpl::class);
        $this->app->bind(PackingListRepository::class, EloquentPackingListRepository::class);
        $this->app->bind(PackingList::class, PackingListModel::class);

        // $this->app->bind(CommercialInvoiceRecordService::class, CommercialInvoiceRecordServiceImpl::class);
        // $this->app->bind(CommercialInvoiceRepository::class, EloquentCommercialInvoiceRepository::class);
        // $this->app->bind(CommercialInvoice::class, CommercialInvoiceModel::class);

        // $this->app->bind(CustomerOrderRecordService::class, CustomerOrderRecordServiceImpl::class);
        // $this->app->bind(CustomerOrderRepository::class, EloquentCustomerOrderRepository::class);
        // $this->app->bind(CustomerOrder::class, CustomerOrderModel::class);

        // $this->app->bind(CustomerOrderDetailRecordService::class, CustomerOrderDetailRecordServiceImpl::class);
        // $this->app->bind(CustomerOrderDetailRepository::class, EloquentCustomerOrderDetailRepository::class);
        // $this->app->bind(CustomerOrderDetail::class, CustomerOrderDetailModel::class);

        $this->app->bind(PurchaseOrderDetailRecordService::class, PurchaseOrderDetailRecordServiceImpl::class);
        $this->app->bind(PurchaseOrderDetailRepository::class, EloquentPurchaseOrderDetailRepository::class);
        $this->app->bind(PurchaseOrderDetail::class, PurchaseOrderDetailModel::class);

        // $this->app->bind(OutboundPaymentRecordService::class, OutboundPaymentRecordServiceImpl::class);
        // $this->app->bind(OutboundPaymentRepository::class, EloquentOutboundPaymentRepository::class);
        // $this->app->bind(OutboundPayment::class, OutboundPaymentModel::class);

        $this->app->bind(InventoryRecordService::class, InventoryRecordServiceImpl::class);
        $this->app->bind(InventoryRepository::class, EloquentInventoryRepository::class);
        $this->app->bind(Inventory::class, InventoryModel::class);

        $this->app->bind(InventoryDetailRecordService::class, InventoryDetailRecordServiceImpl::class);
        $this->app->bind(InventoryDetailRepository::class, EloquentInventoryDetailRepository::class);
        $this->app->bind(InventoryDetail::class, InventoryDetailModel::class);

        // $this->app->bind(JobSheetRecordService::class, JobSheetRecordServiceImpl::class);
        // $this->app->bind(JobSheetRepository::class, EloquentJobSheetRepository::class);
        // $this->app->bind(JobSheet::class, JobSheetModel::class);

        // $this->app->bind(JobSheetDetailRecordService::class, JobSheetDetailRecordServiceImpl::class);
        // $this->app->bind(JobSheetDetailRepository::class, EloquentJobSheetDetailRepository::class);
        // $this->app->bind(JobSheetDetail::class, JobSheetDetailModel::class);

        // $this->app->bind(SheetingReportRecordService::class, SheetingReportRecordServiceImpl::class);
        // $this->app->bind(SheetingReportRepository::class, EloquentSheetingReportRepository::class);
        // $this->app->bind(SheetingReport::class, SheetingReportModel::class);

        // $this->app->bind(SheetingReportDestDetailRecordService::class, SheetingReportDestDetailRecordServiceImpl::class);
        // $this->app->bind(SheetingReportDestDetailRepository::class, EloquentSheetingReportDestDetailRepository::class);
        // $this->app->bind(SheetingReportDestDetail::class, SheetingReportDestDetailModel::class);

        // $this->app->bind(SheetingReportSourceDetailRecordService::class, SheetingReportSourceDetailRecordServiceImpl::class);
        // $this->app->bind(SheetingReportSourceDetailRepository::class, EloquentSheetingReportSourceDetailRepository::class);
        // $this->app->bind(SheetingReportSourceDetail::class, SheetingReportSourceDetailModel::class);

        // $this->app->bind(ContainerRecordService::class, ContainerRecordServiceImpl::class);
        // $this->app->bind(ContainerRepository::class, EloquentContainerRepository::class);
        // $this->app->bind(Container::class, ContainerModel::class);

        // $this->app->bind(ContainerDetailRecordService::class, ContainerDetailRecordServiceImpl::class);
        // $this->app->bind(ContainerDetailRepository::class, EloquentContainerDetailRepository::class);
        // $this->app->bind(ContainerDetail::class, ContainerDetailModel::class);
    }
}
