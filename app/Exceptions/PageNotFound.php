<?php
namespace  App\Exceptions;

class PageNotFound extends \Exception {
    public function render($request)
    {
        abort(404);
    }
}
