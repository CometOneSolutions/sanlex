<?php

namespace App\Models;

use App\Modules\Product\Models\Product;

class SKU
{
    private $product;

    const FORMAT = '%s-%s-%s-%s';

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getCode()
    {
        $name = $this->product->getName();
        $paperTypeName = $this->product->getPaperType()->getName();
        $gsm = $this->product->getGSM();
        $variant = $this->product->getVariant();
        return sprintf(self::FORMAT, $name, $paperTypeName, $gsm, $variant);
    }

    public static function generate(Product $product)
    {
        $sku = new static($product);
        return $sku->getCode();
    }
}
