<?php

namespace App\Models;

class Address
{   
    private $street;
    private $zip;
    private $city;

    function __construct($street, $city, $zip)
    {
        $this->setStreet($street);
        $this->setCity($city);
        $this->setZip($zip);
    }

    public function setStreet($value)
    {
        $this->street = $value;
        return $this;
    }

    public function setZip($value)
    {
        $this->zip = $value;
        return $this;
    }

    public function setCity($value)
    {
        $this->city = $value;
        return $this;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function getCity()
    {
        return $this->city;
    }

}




