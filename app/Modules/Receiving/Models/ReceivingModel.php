<?php

namespace App\Modules\Receiving\Models;

use App\Modules\Product\Models\Product;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Product\Models\ProductModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Inventory\Model\Inventoriable;
use App\Modules\PurchaseOrder\Models\PurchaseOrderModel;
use CometOneSolutions\Inventory\Traits\HasItemMovements;
use App\Modules\InventoryDetail\Models\InventoryDetailModel;

class ReceivingModel extends Model implements Receiving, UpdatableByUser, Inventoriable
{
    use HasUpdatedByUser;
    use HasItemMovements;

    protected $table = 'receivings';

    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrderModel::class, 'purchase_order_id');
    }

    public function product()
    {
        return $this->belongsTo(ProductModel::class, 'product_id');
    }

    public function inventoryDetails()
    {
        return $this->hasMany(InventoryDetailModel::class, 'receiving_id');
    }

    public function getInventoryDetails()
    {
        return $this->inventoryDetails;
    }

    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    public function getPurchaseOrderId()
    {
        return $this->purchase_order_id;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getProductId()
    {
        return $this->product_id;
    }

    public function setPurchaseOrder(PurchaseOrder $purchaseOrder)
    {
        $this->purchaseOrder()->associate($purchaseOrder);
        $this->purchase_order_id = $purchaseOrder->getId();
        return $this;
    }

    public function setProduct(Product $product)
    {
        $this->product()->associate($product);

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCoilNumber()
    {
        return $this->coil_number;
    }

    public function setCoilNumber($value)
    {
        $this->coil_number = $value;
        return $this;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($value)
    {
        $this->quantity = $value;
        return $this;
    }

    public function getNetWeight()
    {
        return $this->net_weight;
    }

    public function setNetWeight($value)
    {
        $this->net_weight = $value;
        return $this;
    }

    public function getLinearMeter()
    {
        return $this->linear_meter;
    }

    public function setLinearMeter($value)
    {
        $this->linear_meter = $value;
        return $this;
    }

    public function getInventoryRefNo()
    {
        return $this->getPurchaseOrder()->getPONumber();
    }

    public function getInventoryUri()
    {
        return route('purchase-order_show', $this->getPurchaseOrderId());
    }
    public function scopeProduct($query, $productId)
    {
        return $query->whereProductId($productId);
    }
}
