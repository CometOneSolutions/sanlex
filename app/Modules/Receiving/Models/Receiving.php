<?php

namespace App\Modules\Receiving\Models;

use App\Modules\Product\Models\Product;
use CometOneSolutions\Auth\Models\UpdatableByUser;

interface Receiving
{
    const GOOD = 'Good';
    const BAD  = 'Bad';

    public function getId();

    public function getProduct();

    public function setProduct(Product $product);

    public function getCoilNumber();

    public function setCoilNumber($value);

    public function getNetWeight();

    public function setNetWeight($value);

    public function getLinearMeter();

    public function setLinearMeter($value);

    public function getQuantity();

    public function setQuantity($value);
}
