<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'receivings'], function () {
        Route::get('/', 'ReceivingController@index')->name('receiving_index');
        Route::get('/create', 'ReceivingController@create')->name('receiving_create');
        Route::get('{receivingId}/edit', 'ReceivingController@edit')->name('receiving_edit');
        Route::get('{receivingId}/print', 'ReceivingController@print')->name('receiving_print');
        Route::get('{receivingId}', 'ReceivingController@show')->name('receiving_show');
    });

});
