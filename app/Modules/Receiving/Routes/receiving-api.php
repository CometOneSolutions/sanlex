<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'receivings'], function () use ($api) {
        $api->get('/', 'App\Modules\Receiving\Controllers\ReceivingApiController@index');
        $api->get('{receivingId}', 'App\Modules\Receiving\Controllers\ReceivingApiController@show');
        $api->post('/', 'App\Modules\Receiving\Controllers\ReceivingApiController@store');
        $api->patch('{receivingId}', 'App\Modules\Receiving\Controllers\ReceivingApiController@update');
        $api->delete('{receivingId}', 'App\Modules\Receiving\Controllers\ReceivingApiController@destroy');
    });
});
