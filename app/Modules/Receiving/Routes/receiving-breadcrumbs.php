<?php

Breadcrumbs::register('receiving.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Coils', route('receiving_index'));
});
Breadcrumbs::register('receiving.create', function ($breadcrumbs) {
    $breadcrumbs->parent('receiving.index');
    $breadcrumbs->push('Create', route('receiving_create'));
});
Breadcrumbs::register('receiving.show', function ($breadcrumbs, $receiving) {
    $breadcrumbs->parent('receiving.index');
    $breadcrumbs->push('Detail', route('receiving_show', $receiving->getId()));
});
Breadcrumbs::register('receiving.edit', function ($breadcrumbs, $receiving) {
    $breadcrumbs->parent('receiving.show', $receiving);
    $breadcrumbs->push('Edit', route('receiving_edit', $receiving->getId()));
});
