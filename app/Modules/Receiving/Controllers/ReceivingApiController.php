<?php

namespace App\Modules\Receiving\Controllers;

use App\Modules\Receiving\Services\ReceivingRecordService;
use App\Modules\Receiving\Transformer\ReceivingTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;

class ReceivingApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        ReceivingRecordService $receivingRecordService,
        ReceivingTransformer $transformer
    ) {
        $this->middleware('auth:api');
        $this->service = $receivingRecordService;
        $this->transformer = $transformer;
        parent::__construct($receivingRecordService, $transformer);
    }

//    public function store(StoreReceiving $request)
//    {
//        $receiving = \DB::transaction(function () use ($request) {
//            return $this->service->create(
//                $request->getName(),
//                $request->getWidth(),
//                $request->getThickness(),
//                $request->getNw(),
//                $request->getLm(),
//                $request->getColor(),
//                $request->getProduct(),
//                $request->getPurchaseOrder(),
//                $request->user()
//            );
//        });
//        return $this->response->item($receiving, $this->transformer)->setStatusCode(201);
//    }
//
//    public function update(UpdateReceiving $request)
//    {
//        $receiving = \DB::transaction(function () use ($request) {
//            return $this->service->update(
//                $request->getReceiving(),
//                $request->getName(),
//                $request->getWidth(),
//                $request->getThickness(),
//                $request->getNw(),
//                $request->getLm(),
//                $request->getColor(),
//                $request->getProduct(),
//                $request->getPurchaseOrder(),
//                $request->user()
//            );
//        });
//        return $this->response->item($receiving, $this->transformer)->setStatusCode(200);
//    }
//
//    public function destroy($id, DestroyReceiving $request)
//    {
//        $receiving = \DB::transaction(function () use ($id) {
//            $receiving = $this->service->getById($id);
//            $this->service->delete($receiving);
//            return $receiving;
//        });
//        return $this->response->item($receiving, $this->transformer)->setStatusCode(200);
//    }
}
