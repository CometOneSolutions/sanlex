<?php

namespace App\Modules\Receiving\Transformer;

use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\PurchaseOrder\Transformer\PurchaseOrderTransformer;
use App\Modules\Receiving\Models\Receiving;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class ReceivingTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'product', 'purchaseOrder'
    ];

    public function transform(Receiving $receiving)
    {

        return [
            'id' => (int) $receiving->getId(),
            'productName' => $receiving->getProduct()->getName(),
            'netWeight' => $receiving->getNetWeight(),
            'linearMeter' => $receiving->getLinearMeter(),
            'productName' => $receiving->getProduct()->getName(),
            'coilNumber'    => $receiving->getCoilNumber(),
            'quantity' => $receiving->getQuantity(),

        ];
    }

    public function includeProduct(Receiving $receiving)
    {
        $product = $receiving->getProduct();
        return $this->item($product, new ProductTransformer);
    }

    public function includePurchaseOrder(Receiving $receiving)
    {
        $purchaseOrder = $receiving->getColor();
        return $this->item($purchaseOrder, new PurchaseOrderTransformer);
    }
}
