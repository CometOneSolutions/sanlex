<?php

namespace App\Modules\Receiving\Repositories;

use App\Modules\Receiving\Models\Receiving;
use CometOneSolutions\Common\Repositories\Repository;

interface ReceivingRepository extends Repository
{
    public function save(Receiving $receiving);
    public function delete(Receiving $receiving);
}
