<?php

namespace App\Modules\Receiving\Repositories;

use App\Modules\Receiving\Models\Receiving;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentReceivingRepository extends EloquentRepository implements ReceivingRepository
{
    public function __construct(Receiving $receiving)
    {
        parent::__construct($receiving);
    }

    public function save(Receiving $receiving)
    {
        return $receiving->save();
    }

    public function delete(Receiving $receiving)
    {
        return $receiving->delete();
    }

}
