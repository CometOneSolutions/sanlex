<?php

namespace App\Modules\Receiving\Provider;

use App\Modules\Receiving\Models\Receiving;
use App\Modules\Receiving\Models\ReceivingModel;
use App\Modules\Receiving\Repositories\EloquentReceivingRepository;
use App\Modules\Receiving\Repositories\ReceivingRepository;
use App\Modules\Receiving\Services\ReceivingRecordService;
use App\Modules\Receiving\Services\ReceivingRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ReceivingServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Receiving/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(ReceivingRecordService::class, ReceivingRecordServiceImpl::class);
        $this->app->bind(ReceivingRepository::class, EloquentReceivingRepository::class);
        $this->app->bind(Receiving::class, ReceivingModel::class);
    }
}
