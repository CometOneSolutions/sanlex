<?php

namespace App\Modules\Receiving\Provider;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class ReceivingRouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Modules\Receiving\Controllers';
    protected $routesPath = 'Modules/Receiving/Routes/';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        // $this->mapWebRoutes();
        // $this->mapBreadcrumbRoutes();
    }

    // protected function mapBreadcrumbRoutes()
    // {
    //     Route::namespace($this->namespace)->group(app_path($this->routesPath . 'receiving-breadcrumbs.php'));
    // }


    // protected function mapWebRoutes()
    // {
    //     Route::middleware('web')
    //          ->namespace($this->namespace)
    //          ->group(app_path($this->routesPath . 'receiving-web.php'));
    // }

    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(app_path($this->routesPath . 'receiving-api.php'));
    }
}
