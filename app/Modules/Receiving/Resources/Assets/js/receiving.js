import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select2 from '@c1_common_js/components/Select2';


new Vue({
    el: '#receiving',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select2
    },
    data: {
        form: new Form({
            coilNumber: null,
            width: null,
            thickness: null,
            netWeight: null,
            grossWeight: null,
            linearMeter: null,
            color: null,
            productId: null,
            purchaseOrderId: null
        }),
        dataInitialized: true,
    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },
    computed: {

        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        destroy() {
            this.form.deleteWithConfirmation('/api/receivings/' + this.form.id).then(response => {
                this.form.successModal(this.form.name.toUpperCase() + ' was removed.').then(() =>
                    window.location = '/master-file/receivings/'
                );
            });
        },

        store() {
            this.form.postWithModal('/api/receivings', null, this.form.name.toUpperCase() + ' was saved.');
        },

        update() {
            this.form.patch('/api/receivings/' + this.form.id).then(response => {
                this.form.successModal(this.form.name.toUpperCase() + ' was updated.').then(() =>
                    window.location = '/master-file/receivings/' + this.form.id
                );
            })
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {
        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/receivings/' + id)
                .then(response => {
                    this.loadData(response.data);
                    this.dataInitialized = true;
                });
        }
    },
    mounted() {
        console.log("Init receiving script...");
    }
});