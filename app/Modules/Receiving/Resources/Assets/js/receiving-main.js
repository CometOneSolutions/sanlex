import {Form} from '@c1_common_js/components/Form';

export const receivingService = new Vue({

    data: function() {
        return {
            form: new Form({}),
            uri: {
              receivings: '/api/receivings?limit=' + Number.MAX_SAFE_INTEGER
            },
        }
    },   

    methods: {

        getReceivings(){
            return this.form.get(this.uri.receivings);
        },
    }
});