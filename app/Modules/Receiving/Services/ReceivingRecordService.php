<?php

namespace App\Modules\Receiving\Services;

use App\Modules\Receiving\Models\Receiving;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface ReceivingRecordService extends RecordService
{
    // public function create($name, $supplier, $coating, $width, $thickness, $netWeight, $linearMeter, $color, $productId, PurchaseOrder $purchaseOrder, $quantity,  User $user = null);
    // public function createCoilReceiving($coilNumber, $supplier, $coating, $width, $thickness, $netWeight, $linearMeter, $color, $productId, PurchaseOrder $purchaseOrder, User $user = null);
    // public function createAccessoryReceiving($supplier, $productId, PurchaseOrder $purchaseOrder, $quantity,  User $user = null);
    // public function update(Receiving $receiving, $name, $supplier, $coating, $width, $thickness, $netWeight, $linearMeter, $color, Product $product, PurchaseOrder $purchaseOrder, $quantity, User $user = null);
    public function delete(Receiving $receiving);
}
