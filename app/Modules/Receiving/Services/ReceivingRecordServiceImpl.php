<?php

namespace App\Modules\Receiving\Services;

use App\Modules\Product\Services\ProductRecordService;
use App\Modules\PurchaseOrder\Services\PurchaseOrderRecordService;
use App\Modules\Receiving\Models\Receiving;
use App\Modules\Receiving\Repositories\ReceivingRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class ReceivingRecordServiceImpl extends RecordServiceImpl implements ReceivingRecordService
{
    private $receiving;
    private $receivings;
    private $productRecordService;
    private $purchaseOrderRecordService;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"],
    ];

    public function __construct(ReceivingRepository $receivings, Receiving $receiving, ProductRecordService $productRecordService, PurchaseOrderRecordService $purchaseOrderRecordService)
    {
        parent::__construct($receivings);
        $this->receivings = $receivings;
        $this->receiving = $receiving;
        $this->productRecordService = $productRecordService;
        $this->purchaseOrderRecordService = $purchaseOrderRecordService;
    }





    // public function createAccessoryReceiving(
    //     $supplier,
    //     $productId,
    //     PurchaseOrder $purchaseOrder,
    //     $quantity,
    //     User $user = null
    // ) {
    //     $product = $this->productRecordService->getById($productId);
    //     $receiving = new $this->receiving;
    //     $receiving->setSupplier($supplier);
    //     $receiving->setProduct($product);
    //     $receiving->setQuantity($quantity);
    //     $receiving->setPurchaseOrder($purchaseOrder);
    //     if ($user) {
    //         $receiving->setUpdatedByUser($user);
    //     }
    //     $this->receivings->save($receiving);
    //     return $receiving;
    // }



    public function delete(Receiving $receiving)
    {
        $this->receivings->delete($receiving);
        return $receiving;
    }
}
