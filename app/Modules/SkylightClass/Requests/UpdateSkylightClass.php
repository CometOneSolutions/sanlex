<?php

namespace App\Modules\SkylightClass\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateSkylightClass extends SkylightClassRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Skylight Class');
    }
}
