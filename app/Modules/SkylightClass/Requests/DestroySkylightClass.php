<?php

namespace App\Modules\SkylightClass\Requests;

use Dingo\Api\Http\FormRequest;

class DestroySkylightClass extends SkylightClassRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Skylight Class');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
