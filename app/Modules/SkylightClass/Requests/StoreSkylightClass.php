<?php

namespace App\Modules\SkylightClass\Requests;

use Dingo\Api\Http\FormRequest;

class StoreSkylightClass extends SkylightClassRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Skylight Class');
    }
}
