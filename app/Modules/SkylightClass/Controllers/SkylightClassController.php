<?php

namespace App\Modules\SkylightClass\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\SkylightClass\Services\SkylightClassRecordService;


class SkylightClassController extends Controller
{
    private $service;

    public function __construct(SkylightClassRecordService $skylightClassRecordService)
    {
        $this->service = $skylightClassRecordService;
        $this->middleware('permission:Read [MAS] Skylight Class')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Skylight Class')->only('create');
        $this->middleware('permission:Update [MAS] Skylight Class')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/skylight-classes'
        ]);
        return view('skylight-classes.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('skylight-classes.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $skylightClass = $this->service->getById($id);
        return view('skylight-classes.show', compact('skylightClass'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $skylightClass = $this->service->getById($id);
        return view('skylight-classes.edit', compact('skylightClass'));
    }
}
