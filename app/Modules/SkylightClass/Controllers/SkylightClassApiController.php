<?php

namespace App\Modules\SkylightClass\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\SkylightClass\Services\SkylightClassRecordService;
use App\Modules\SkylightClass\Transformer\SkylightClassTransformer;
use App\Modules\SkylightClass\Requests\StoreSkylightClass;
use App\Modules\SkylightClass\Requests\UpdateSkylightClass;
use App\Modules\SkylightClass\Requests\DestroySkylightClass;

class SkylightClassApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        SkylightClassRecordService $skylightClassRecordService,
        SkylightClassTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($skylightClassRecordService, $transformer);
        $this->service = $skylightClassRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreSkylightClass $request)
    {
        $skylightClass = \DB::transaction(function () use ($request) {
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->create(
                $name,
                $user
            );
        });
        return $this->response->item($skylightClass, $this->transformer)->setStatusCode(201);
    }

    public function update($skylightClassId, UpdateSkylightClass $request)
    {
        $skylightClass = \DB::transaction(function () use ($skylightClassId, $request) {
            $skylightClass   = $this->service->getById($skylightClassId);
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->update(
                $skylightClass,
                $name,
                $user
            );
        });
        return $this->response->item($skylightClass, $this->transformer)->setStatusCode(200);
    }

    public function destroy($skylightClassId, DestroySkylightClass $request)
    {
        $skylightClass = $this->service->getById($skylightClassId);
        $this->service->delete($skylightClass);
        return $this->response->item($skylightClass, $this->transformer)->setStatusCode(200);
    }
}
