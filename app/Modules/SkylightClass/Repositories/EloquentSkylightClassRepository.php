<?php

namespace App\Modules\SkylightClass\Repositories;

use App\Modules\SkylightClass\Models\SkylightClass;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentSkylightClassRepository extends EloquentRepository implements SkylightClassRepository
{
    public function __construct(SkylightClass $skylightClass)
    {
        parent::__construct($skylightClass);
    }

    public function save(SkylightClass $skylightClass)
    {
        return $skylightClass->save();
    }

    public function delete(SkylightClass $skylightClass)
    {
        return $skylightClass->delete();
    }
}
