<?php

namespace App\Modules\SkylightClass\Repositories;

use App\Modules\SkylightClass\Models\SkylightClass;
use CometOneSolutions\Common\Repositories\Repository;

interface SkylightClassRepository extends Repository
{
    public function save(SkylightClass $skylightClass);
    public function delete(SkylightClass $skylightClass);
}
