<?php

Breadcrumbs::register('skylight-class.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Skylight Classes', route('skylight-class_index'));
});
Breadcrumbs::register('skylight-class.create', function ($breadcrumbs) {
    $breadcrumbs->parent('skylight-class.index');
    $breadcrumbs->push('Create', route('skylight-class_create'));
});
Breadcrumbs::register('skylight-class.show', function ($breadcrumbs, $length) {
    $breadcrumbs->parent('skylight-class.index');
    $breadcrumbs->push($length->getName(), route('skylight-class_show', $length->getId()));
});
Breadcrumbs::register('skylight-class.edit', function ($breadcrumbs, $length) {
    $breadcrumbs->parent('skylight-class.show', $length);
    $breadcrumbs->push('Edit', route('skylight-class_edit', $length->getId()));
});
