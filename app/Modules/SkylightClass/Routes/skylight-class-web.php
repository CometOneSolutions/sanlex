<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'skylight-classes'], function () {
        Route::get('/', 'SkylightClassController@index')->name('skylight-class_index');
        Route::get('/create', 'SkylightClassController@create')->name('skylight-class_create');
        Route::get('{skylightClassId}/edit', 'SkylightClassController@edit')->name('skylight-class_edit');
        Route::get('{skylightClassId}/print', 'SkylightClassController@print')->name('skylight-class_print');
        Route::get('{skylightClassId}', 'SkylightClassController@show')->name('skylight-class_show');
    });
});
