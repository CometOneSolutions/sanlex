<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'skylight-classes'], function () use ($api) {
        $api->get('/', 'App\Modules\SkylightClass\Controllers\SkylightClassApiController@index');
        $api->get('{skylightClassId}', 'App\Modules\SkylightClass\Controllers\SkylightClassApiController@show');
        $api->post('/', 'App\Modules\SkylightClass\Controllers\SkylightClassApiController@store');
        $api->patch('{skylightClassId}', 'App\Modules\SkylightClass\Controllers\SkylightClassApiController@update');
        $api->delete('{skylightClassId}', 'App\Modules\SkylightClass\Controllers\SkylightClassApiController@destroy');
    });
});
