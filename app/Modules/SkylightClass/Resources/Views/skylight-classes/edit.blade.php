@extends('app')
@section('breadcrumbs', Breadcrumbs::render('skylight-class.edit', $skylightClass))
@section('content')
<section id="skylight-class">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header clearfix">
                        Edit Skylight Class Record
                        @if(auth()->user()->can('Delete [MAS] Skylight Class'))
                        <div class="float-right">
                            <delete-button :is-busy="form.isBusy" :is-deleting="form.isDeleting" @destroy="destroy"></delete-button>
                        </div>
                        @endif
                    </div>

                    <v-form @validate="update">
                        <div class="card-body">

                            @include('skylight-classes._form')
                        </div>

                        <div class="card-footer">
                            <div class="text-right">
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                            </div>
                        </div>
                    </v-form>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection