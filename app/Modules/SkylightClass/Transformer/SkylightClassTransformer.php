<?php

namespace App\Modules\SkylightClass\Transformer;

use App\Modules\SkylightClass\Models\SkylightClass;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class SkylightClassTransformer extends UpdatableByUserTransformer
{
    public function transform(SkylightClass $skylightClass)
    {
        return [
            'id' => (int) $skylightClass->getId(),
            'text' => $skylightClass->getName(),
            'name' => $skylightClass->getName(),
            'updatedAt' => $skylightClass->updated_at,
            'editUri' => route('skylight-class_edit', $skylightClass->getId()),
            'showUri' => route('skylight-class_show', $skylightClass->getId()),
        ];
    }
}
