<?php

namespace App\Modules\SkylightClass\Services;

use App\Modules\SkylightClass\Models\SkylightClass;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface SkylightClassRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        SkylightClass $skylightClass,
        $name,
        User $user = null
    );

    public function delete(SkylightClass $skylightClass);
}
