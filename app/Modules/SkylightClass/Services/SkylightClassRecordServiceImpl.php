<?php

namespace App\Modules\SkylightClass\Services;

use App\Modules\Product\Traits\CanUpdateProductName;
use App\Modules\SkylightClass\Models\SkylightClass;
use App\Modules\SkylightClass\Repositories\SkylightClassRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class SkylightClassRecordServiceImpl extends RecordServiceImpl implements SkylightClassRecordService
{

    use CanUpdateProductName;

    private $skylightClass;
    private $skylightClasses;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(SkylightClassRepository $skylightClasses, SkylightClass $skylightClass)
    {
        $this->lengths = $skylightClasses;
        $this->skylightClass = $skylightClass;
        parent::__construct($skylightClasses);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $skylightClass = new $this->skylightClass;
        $skylightClass->setName($name);

        if ($user) {
            $skylightClass->setUpdatedByUser($user);
        }
        $this->lengths->save($skylightClass);
        return $skylightClass;
    }

    public function update(
        SkylightClass $skylightClass,
        $name,
        User $user = null
    ) {
        $tempSkylightClass = clone $skylightClass;
        $tempSkylightClass->setName($name);
        if ($user) {
            $tempSkylightClass->setUpdatedByUser($user);
        }
        $this->lengths->save($tempSkylightClass);


        $this->updateProductableProductNames([
            $tempSkylightClass->getSkylights()
        ]);

        return $tempSkylightClass;
    }

    public function delete(SkylightClass $skylightClass)
    {
        $this->lengths->delete($skylightClass);
        return $skylightClass;
    }
}
