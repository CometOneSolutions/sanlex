<?php

namespace App\Modules\SkylightClass\Models;



interface SkylightClass
{
    public function getId();

    public function getName();

    public function setName($value);
}
