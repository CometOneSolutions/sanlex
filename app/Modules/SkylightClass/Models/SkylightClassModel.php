<?php

namespace App\Modules\SkylightClass\Models;

use App\Modules\Skylight\Models\SkylightModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;

use CometOneSolutions\Auth\Models\Users\User;

class SkylightClassModel extends Model implements SkylightClass, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'skylight_classes';

    public function skylights()
    {
        return $this->hasMany(SkylightModel::class, 'skylight_class_id');
    }

    public function getSkylights()
    {
        return $this->skylights;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
