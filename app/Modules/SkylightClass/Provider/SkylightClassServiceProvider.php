<?php

namespace App\Modules\SkylightClass\Provider;

use App\Modules\SkylightClass\Models\SkylightClass;
use App\Modules\SkylightClass\Models\SkylightClassModel;
use App\Modules\SkylightClass\Repositories\EloquentSkylightClassRepository;
use App\Modules\SkylightClass\Repositories\SkylightClassRepository;
use App\Modules\SkylightClass\Services\SkylightClassRecordService;
use App\Modules\SkylightClass\Services\SkylightClassRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class SkylightClassServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/SkylightClass/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(SkylightClassRecordService::class, SkylightClassRecordServiceImpl::class);
        $this->app->bind(SkylightClassRepository::class, EloquentSkylightClassRepository::class);
        $this->app->bind(SkylightClass::class, SkylightClassModel::class);
    }
}
