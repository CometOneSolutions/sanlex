<?php

namespace App\Modules\Screw\Provider;

use App\Modules\Screw\Models\Screw;
use App\Modules\Screw\Models\ScrewModel;
use App\Modules\Screw\Repositories\EloquentScrewRepository;
use App\Modules\Screw\Repositories\ScrewRepository;
use App\Modules\Screw\Services\ScrewRecordService;
use App\Modules\Screw\Services\ScrewRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ScrewServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Screw/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(ScrewRecordService::class, ScrewRecordServiceImpl::class);
        $this->app->bind(ScrewRepository::class, EloquentScrewRepository::class);
        $this->app->bind(Screw::class, ScrewModel::class);
    }
}
