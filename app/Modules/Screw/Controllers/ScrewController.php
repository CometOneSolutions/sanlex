<?php

namespace App\Modules\Screw\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Screw\Services\ScrewRecordService;


class ScrewController extends Controller
{
    private $service;

    public function __construct(ScrewRecordService $screwRecordService)
    {
        $this->service = $screwRecordService;
        $this->middleware('permission:Read [MAS] Screw')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Screw')->only('create');
        $this->middleware('permission:Update [MAS] Screw')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/screws?include=product,length,screwType'
        ]);
        return view('screws.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('screws.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $screw = $this->service->getById($id);
        return view('screws.show', compact('screw'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $screw = $this->service->getById($id);
        return view('screws.edit', compact('screw'));
    }
}
