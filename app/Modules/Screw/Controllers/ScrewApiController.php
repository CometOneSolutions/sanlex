<?php

namespace App\Modules\Screw\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Screw\Services\ScrewRecordService;
use App\Modules\Screw\Transformer\ScrewTransformer;
use App\Modules\Screw\Requests\StoreScrew;
use App\Modules\Screw\Requests\UpdateScrew;
use App\Modules\Screw\Requests\DestroyScrew;
use App\Modules\Product\Services\ProductRecordService;


class ScrewApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;
    protected $productRecordService;

    public function __construct(
        ScrewRecordService $screwRecordService,
        ScrewTransformer $transformer,
        ProductRecordService $productRecordService
    ) {
        $this->middleware('auth:api');
        parent::__construct($screwRecordService, $transformer);
        $this->service = $screwRecordService;
        $this->transformer = $transformer;
        $this->productRecordService = $productRecordService;
    }

    public function store(StoreScrew $request)
    {

        $screw = \DB::transaction(function () use ($request) {
            $name = $request->getName();

            return $this->service->create(
                $name,
                $request->getSupplier(),

                $request->getLength(),
                $request->getScrewType(),
                $request->user()
            );
        });
        return $this->response->item($screw, $this->transformer)->setStatusCode(201);
    }

    public function update($screwId, UpdateScrew $request)
    {
        $screw = \DB::transaction(function () use ($request) {
            $name = $request->getName();

            return $this->service->update(
                $request->getScrew(),
                $name,
                $request->getSupplier(),

                $request->getLength(),
                $request->getScrewType(),
                $request->user()
            );
        });
        return $this->response->item($screw, $this->transformer)->setStatusCode(200);
    }

    public function destroy($screwId, DestroyScrew $request)
    {
        $screw = $this->service->getById($screwId);
        $this->service->delete($screw);
        return $this->response->item($screw, $this->transformer)->setStatusCode(200);
    }
}
