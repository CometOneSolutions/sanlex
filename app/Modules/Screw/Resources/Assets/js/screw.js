import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select3 from "@c1_common_js/components/Select3";


new Vue({
    el: '#screw',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select3
    },
    data: {
        form: new Form({
            name: null,
            supplierId: null,
            shortCode: null,
            lengthId: null,
            lengthText: null,
            screwTypeId: null,
            screwTypeText: null
        }),
        dataInitialized: true,
        supplierUrl: '/api/suppliers?sort=name',
        defaultSelectedSupplier: {},
        lengthUrl: '/api/lengths?sort=name',
        defaultSelectedLength: {},
        screwTypeUrl: '/api/screw-types?sort=name',
        defaultSelectedScrewType: {},
        
    },
    watch: {
        initializationComplete(val) {      
            this.form.isInitializing = !val;
        }
    },
    computed: {
        // selectedSupplier() {
        //     if(this.form.supplierId == null)
        //     {
        //         return undefined;
        //     }
        //     return this.supplierSelections.find(supplier => supplier.id == this.form.supplierId);
        // },
        name() {
            if(this.form.supplierId === undefined || this.form.lengthId === null || this.form.screwTypeId === null)
            {
                return undefined;
            }
            return this.form.shortCode + '~' + this.form.screwTypeText.toUpperCase() + '~' + this.form.lengthText.toUpperCase();
        },
        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        setShortCode(selectedValue, selectedObjects) 
        {
            if (selectedObjects[0].hasOwnProperty("shortCode")) {
                this.form.shortCode = selectedObjects[0].shortCode;
              }
        },
        setLength(selectedValue, selectedObjects)
        {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.lengthText = selectedObjects[0].name;
            }
            
        },
        setScrewType(selectedValue, selectedObjects)
        {
            console.log(selectedObjects[0].name);
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.screwTypeTest = selectedObjects[0].name;
            }
            
        },
        destroy() {
            this.form.deleteWithConfirmation('/api/screws/' + this.form.id).then(response => {
                this.form.successModal('Screw was removed.').then(() => 
                    window.location = '/master-file/screws/'
                );                
            }); 
        },
       
        store() {
            this.form.name = this.name;
            this.form.postWithModal('/api/screws', null, 'Screw was saved.');
        },

        update() {
            this.form.name = this.name;
            this.form.patch('/api/screws/' + this.form.id).then(response => {
                this.form.successModal('Screw was updated.').then(() => 
                    window.location = '/master-file/screws/' + this.form.id
                );
            })
        },
       
        loadData(data) {
            this.form = new Form(data);
        },
    },

    created(){
       
        
        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/screws/' + id + '?include=product.supplier,length,screwType')
            .then(response => {
                this.loadData(response.data);
                this.form.shortCode = response.data.product.data.supplier.data.shortCode;
                this.defaultSelectedSupplier = {
                    text: response.data.product.data.supplier.data.name,
                    id: response.data.product.data.supplier.data.id,
                };
                this.defaultSelectedLength = {
                    text: response.data.length.data.name,
                    id: response.data.length.data.id,
                };
                this.form.lengthText = response.data.length.data.name;
                this.defaultSelectedScrewType = {
                    text: response.data.screwType.data.name,
                    id: response.data.screwType.data.id,
                };
                this.form.screwTypeText = response.data.screwType.data.name;
              
                this.dataInitialized = true;
            });
        }
    },
    mounted() {
        console.log("Init screw script...");
    }
});