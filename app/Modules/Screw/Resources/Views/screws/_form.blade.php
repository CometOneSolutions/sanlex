<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Supplier <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter supplier" v-model="form.supplierId" :selected="defaultSelectedSupplier" :url="supplierUrl" search="name" placeholder="Please select" @change="setShortCode" id="supplier" name="supplier">
            </select3>
        </div>
    </div>


    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Length <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter length" v-model="form.lengthId" :selected="defaultSelectedLength" :url="lengthUrl" search="name" placeholder="Please select" @change="setLength" id="length" name="length">
            </select3>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Screw Type <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter screw type" v-model="form.screwTypeId" :selected="defaultSelectedScrewType" :url="screwTypeUrl" search="name" placeholder="Please select" @change="setScrewType" id="screwType" name="screwType">
            </select3>
        </div>
    </div>

</div>
@push('scripts')
<script src="{{ mix('js/screw.js') }}"></script>
@endpush