<?php

use Faker\Generator as Faker;
use App\Modules\Screw\Models\ScrewModel;
use CometOneSolutions\Auth\UserModel;

$factory->define(ScrewModel::class, function (Faker $faker) {
    return [
        'description' => $faker->unique()->company,
        'updated_by_user_id' => $faker->randomElement(UserModel::pluck('id')->toArray()),
    ];
});
