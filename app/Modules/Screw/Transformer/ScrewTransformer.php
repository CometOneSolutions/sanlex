<?php

namespace App\Modules\Screw\Transformer;

use App\Modules\Length\Transformer\LengthTransformer;
use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\Screw\Models\Screw;
use App\Modules\ScrewType\Transformer\ScrewTypeTransformer;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class ScrewTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'product', 'length', 'screwType'
    ];
    public function transform(Screw $screw)
    {
        return [
            'id' => (int) $screw->getId(),
            'name' => $screw->getProduct() ? $screw->getProduct()->getName() : null,
            'supplierId' => $screw->getProduct() ? $screw->getProduct()->getSupplier()->getId() : null,
            'lengthId'    => $screw->getLengthId(),
            'lengthText'    => null,
            'screwTypeId' => $screw->getScrewTypeId(),
            'screwTypeText' => null,

            'updatedAt' => $screw->updated_at,
            'editUri' => route('screw_edit', $screw->getId()),
            'showUri' => route('screw_show', $screw->getId()),
        ];
    }

    public function includeProduct(Screw $screw)
    {
        $product = $screw->getProduct();
        return $this->item($product, new ProductTransformer);
    }

    public function includeLength(Screw $screw)
    {
        $length = $screw->getLength();
        return $this->item($length, new LengthTransformer);
    }
    public function includeScrewType(Screw $screw)
    {
        $screwType = $screw->getScrewType();
        return $this->item($screwType, new ScrewTypeTransformer);
    }
}
