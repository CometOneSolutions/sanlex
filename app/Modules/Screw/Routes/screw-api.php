<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'screws'], function () use ($api) {
        $api->get('/', 'App\Modules\Screw\Controllers\ScrewApiController@index');
        $api->get('{screwId}', 'App\Modules\Screw\Controllers\ScrewApiController@show');
        $api->post('/', 'App\Modules\Screw\Controllers\ScrewApiController@store');
        $api->patch('{screwId}', 'App\Modules\Screw\Controllers\ScrewApiController@update');
        $api->delete('{screwId}', 'App\Modules\Screw\Controllers\ScrewApiController@destroy');
    });
});
