<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'screws'], function () {
        Route::get('/', 'ScrewController@index')->name('screw_index');
        Route::get('/create', 'ScrewController@create')->name('screw_create');
        Route::get('{screwId}/edit', 'ScrewController@edit')->name('screw_edit');
        Route::get('{screwId}/print', 'ScrewController@print')->name('screw_print');
        Route::get('{screwId}', 'ScrewController@show')->name('screw_show');
    });
});
