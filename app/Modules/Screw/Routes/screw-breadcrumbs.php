<?php

Breadcrumbs::register('screw.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Screws', route('screw_index'));
});
Breadcrumbs::register('screw.create', function ($breadcrumbs) {
    $breadcrumbs->parent('screw.index');
    $breadcrumbs->push('Create', route('screw_create'));
});
Breadcrumbs::register('screw.show', function ($breadcrumbs, $screw) {
    $breadcrumbs->parent('screw.index');
    $breadcrumbs->push($screw->getProduct()->getName(), route('screw_show', $screw->getId()));
});
Breadcrumbs::register('screw.edit', function ($breadcrumbs, $screw) {
    $breadcrumbs->parent('screw.show', $screw);
    $breadcrumbs->push('Edit', route('screw_edit', $screw->getId()));
});
