<?php

namespace App\Modules\Screw\Models;



interface Screw
{
    public function getId();

    public function getDescription();

    public function setDescription($value);
}
