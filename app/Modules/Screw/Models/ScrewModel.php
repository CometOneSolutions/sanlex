<?php

namespace App\Modules\Screw\Models;

use App\Modules\Length\Models\Length;
use App\Modules\Length\Models\LengthModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Product\Traits\Productable;
use App\Modules\ScrewType\Models\ScrewType;
use App\Modules\ScrewType\Models\ScrewTypeModel;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Thickness\Models\ThicknessModel;
use App\Modules\Width\Models\Width;
use App\Modules\Width\Models\WidthModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;


class ScrewModel extends Model implements Screw, UpdatableByUser, Productable
{
    use HasUpdatedByUser;

    protected $table = 'screws';

    public function product()
    {
        return $this->morphOne(ProductModel::class, 'productable');
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

//    public function screwDensity()
//    {
//        return $this->belongsTo(ScrewDensityModel::class, 'screw_density_id');
//    }
    public function width()
    {
        return $this->belongsTo(WidthModel::class, 'width_id');
    }

    public function length()
    {
        return $this->belongsTo(LengthModel::class, 'length_id');
    }

    public function thickness()
    {
        return $this->belongsTo(ThicknessModel::class, 'thickness_id');
    }

    public function screwType()
    {
        return $this->belongsTo(ScrewTypeModel::class, 'screw_type_id');
    }

    public function getThicknessId()
    {
        return (int) $this->thickness_id;
    }
    public function getThickness()
    {
        return $this->thickness;
    }

    public function setThickness(Thickness $thickness)
    {
        $this->thickness()->associate($thickness);
        $this->thickness_id = $thickness->getId();
        return $this;
    }
    public function getWidthId()
    {
        return (int) $this->width_id;
    }
    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth(Width $width)
    {
        $this->width()->associate($width);
        $this->width_id = $width->getId();
        return $this;
    }
    public function getLengthId()
    {
        return (int) $this->length_id;
    }
    public function getLength()
    {
        return $this->length;
    }

    public function setLength(Length $length)
    {
        $this->length()->associate($length);
        $this->length_id = $length->getId();
        return $this;
    }

    public function getScrewType()
    {
        return $this->screwType;
    }

    public function getScrewTypeId()
    {
        return (int) $this->screw_type_id;
    }

    public function setScrewType(ScrewType $screwType)
    {
        $this->screwType()->associate($screwType);
        return $this;
    }

//    public function getScrewDensity()
//    {
//        return $this->screwDensity;
//    }

//    public function setScrewDensity(ScrewDensity $screwDensity)
//    {
//        $this->screwDensity()->associate($screwDensity);
//        return $this;
//    }

//    public function getScrewDensityId()
//    {
//        return $this->screw_type_id;
//    }



    public function isCoil()
    {
        return false;
    }
    public function scopeInStock($query)
    {
        return $query->whereHas('product', function ($product) {
            return $product->whereHas('inventory', function ($inventory) {
                return $inventory->where('stock', '>', 0);
            });
        });
    }

    public function getAssembledProductName()
    {
        return "";
    }
}
