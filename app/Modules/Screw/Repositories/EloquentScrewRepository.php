<?php

namespace App\Modules\Screw\Repositories;


use App\Modules\Screw\Models\Screw;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentScrewRepository extends EloquentRepository implements ScrewRepository
{
    public function __construct(Screw $screw)
    {
        parent::__construct($screw);
    }

    public function save(Screw $screw)
    {
        return $screw->save();
    }

    public function delete(Screw $screw)
    {
        $product = $screw->getProduct();
        $product->delete();
        return $screw->delete();
    }
    public function filterByName($productName)
    {
        return $this->model->whereHas('product', function ($product) use ($productName) {
            return $product->where('name', 'like', '%' . $productName . '%');
        });
    }
    public function filterBySupplierName($supplierName)
    {
        return $this->model->whereHas('product', function ($product) use ($supplierName) {
            return $product->whereHas('supplier', function ($supplier) use ($supplierName) {
                return $supplier->where('name', 'like', '%' . $supplierName . '%');
            });
        });
    }
    public function inStock()
    {
        $this->model = $this->model->inStock();
        return $this;
    }
}
