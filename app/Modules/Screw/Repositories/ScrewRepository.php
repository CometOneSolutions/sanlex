<?php

namespace App\Modules\Screw\Repositories;

use App\Modules\Screw\Models\Screw;
use CometOneSolutions\Common\Repositories\Repository;

interface ScrewRepository extends Repository
{
    public function save(Screw $screw);
    public function delete(Screw $screw);
}
