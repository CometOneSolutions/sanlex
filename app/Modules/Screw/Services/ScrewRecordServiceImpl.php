<?php

namespace App\Modules\Screw\Services;

use App\Modules\Length\Models\Length;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Screw\Models\Screw;
use App\Modules\Screw\Repositories\ScrewRepository;
use App\Modules\ScrewType\Models\ScrewType;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class ScrewRecordServiceImpl extends RecordServiceImpl implements ScrewRecordService
{
    private $screw;
    private $screws;
    private $productRecordService;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"],

    ];

    public function __construct(ScrewRepository $screws, Screw $screw, ProductRecordService $productRecordService)
    {
        $this->screws = $screws;
        $this->screw = $screw;
        $this->productRecordService = $productRecordService;
        parent::__construct($screws);
    }

    public function create(
        $name,
        Supplier $supplier,
        Length $length,
        ScrewType $screwType,
        User $user = null
    ) {
        $screw = new $this->screw;
        $screw->setLength($length);
        $screw->setScrewType($screwType);

        if ($user) {
            $screw->setUpdatedByUser($user);
        }
        $this->screws->save($screw);
        $this->productRecordService->create($name, $supplier, $screw, $user);
        return $screw;
    }

    public function update(
        Screw $screw,
        $name,
        Supplier $supplier,
        Length $length,
        ScrewType $screwType,
        User $user = null
    ) {
        $tempScrew = clone $screw;
        $product = $tempScrew->getProduct();
        $tempScrew->setLength($length);
        $tempScrew->setScrewType($screwType);

        if ($user) {
            $tempScrew->setUpdatedByUser($user);
        }
        $this->screws->save($tempScrew);
        $this->productRecordService->update($product, $name, $supplier, $tempScrew, $user);
        return $tempScrew;
    }

    public function delete(Screw $screw)
    {
        $this->screws->delete($screw);
        return $screw;
    }
    public function inStock()
    {
        $this->screws = $this->screws->inStock();
        return $this;
    }
}
