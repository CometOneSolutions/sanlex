<?php

namespace App\Modules\Screw\Services;

use App\Modules\Length\Models\Length;
use App\Modules\Screw\Models\Screw;
use App\Modules\ScrewType\Models\ScrewType;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface ScrewRecordService extends RecordService
{
    public function create(
        $name,
        Supplier $supplier,
        Length $length,
        ScrewType $screwType,
        User $user = null
    );

    public function update(
        Screw $screw,
        $name,
        Supplier $supplier,
        Length $length,
        ScrewType $screwType,
        User $user = null
    );

    public function delete(Screw $screw);
}
