<?php

namespace App\Modules\Screw\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyScrew extends ScrewRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Screw');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}