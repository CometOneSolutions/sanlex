<?php

namespace App\Modules\Screw\Requests;

use App\Modules\Length\Services\LengthRecordService;
use App\Modules\Screw\Services\ScrewRecordService;
use App\Modules\ScrewType\Services\ScrewTypeRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use Dingo\Api\Http\FormRequest;

class ScrewRequest extends FormRequest
{
    private $service;
    private $supplierRecordService;
    private $lengthRecordService;
    private $screwTypeRecordService;


    public function __construct(
        SupplierRecordService $supplierRecordService,
        ScrewRecordService $screwRecordService,
        LengthRecordService $lengthRecordService,
        ScrewTypeRecordService $screwTypeRecordService

    ) {
        $this->supplierRecordService = $supplierRecordService;
        $this->service = $screwRecordService;
        $this->lengthRecordService = $lengthRecordService;
        $this->screwTypeRecordService = $screwTypeRecordService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $screw = $this->route('screwId') ? $this->service->getById($this->route('screwId')) : null;

        if ($screw) {
            return [
                'name'          => 'required|unique:products,name,' . $screw->getProduct()->getId(),

                'lengthId'      => 'required',
                'screwTypeId' => 'required'
            ];
        } else {
            return [
                'name'                  => 'required|unique:products,name',

                'lengthId'              => 'required',
                'screwTypeId'       => 'required'
            ];
        }
    }
    public function messages()
    {
        return [
            'name.required'                  => 'Name is required.',
            'name.unique'                    => 'Name already exists.',

            'lengthId.required'              => 'Length is required.',
            'screwTypeId.required'       => 'Screw type is required.'
        ];
    }



    public function getName($index = 'name')
    {
        return $this->input($index);
    }

    public function getSupplier($index = 'supplierId')
    {
        return $this->supplierRecordService->getById($this->input($index));
    }

    public function getScrew($index = 'id')
    {
        return $this->service->getById($this->input($index));
    }

    public function getLength($index = 'lengthId')
    {
        return $this->lengthRecordService->getById($this->input($index));
    }

    public function getScrewType($index = 'screwTypeId')
    {
        return $this->screwTypeRecordService->getById($this->input($index));
    }
}
