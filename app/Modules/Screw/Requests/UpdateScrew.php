<?php

namespace App\Modules\Screw\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateScrew extends ScrewRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Screw');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }

}