<?php

namespace App\Modules\Screw\Requests;

use Dingo\Api\Http\FormRequest;

class StoreScrew extends ScrewRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Screw');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }

}
