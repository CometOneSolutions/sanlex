<?php

namespace App\Modules\Contract\Requests;

use App\Modules\Contract\Services\ContractRecordService;

class StoreContract extends ContractRequest
{

    public function authorize()
    {
        return $this->user()->can('Create [COL] Contract');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

}
