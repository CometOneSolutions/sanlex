<?php

namespace App\Modules\Contract\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyContract extends FormRequest
{

    public function authorize()
    {
        return $this->user()->can('Delete [COL] Contract');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }

    


}
