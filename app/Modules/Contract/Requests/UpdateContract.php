<?php

namespace App\Modules\Contract\Requests;

use App\Modules\Contract\Services\ContractRecordService;

class UpdateContract extends ContractRequest
{

    public function authorize()
    {
        return $this->user()->can('Update [COL] Contract');
    }

}
