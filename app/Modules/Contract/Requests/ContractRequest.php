<?php

namespace App\Modules\Contract\Requests;

use App\Modules\Customer\Services\CustomerRecordService;
use DateTime;
use Dingo\Api\Http\FormRequest;

class ContractRequest extends FormRequest
{
    private $customerRecordService;

    public function __construct(CustomerRecordService $customerRecordService)
    {
        $this->customerRecordService = $customerRecordService;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerId' => 'required',
            'number' => 'required',
            'amount' => 'required',
            'date' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'customerId.required' => 'Customer is required.',
            'amount.required' => 'Amount is required',
            'number.required' => 'Number is required',
            'date.required' => "Date is required",
        ];
    }

    public function getCustomer()
    {
        return $this->customerRecordService->getById($this->input('customerId'));
    }

    public function getAmount()
    {
        return $this->input('amount');
    }

    public function getDate()
    {
        return new DateTime($this->input('date'));
    }

    public function getDescription()
    {
        return $this->input('description');
    }

    public function getFiles()
    {
        if (!$this->files->get('files')) {
            return [];
        }
        return $this->files->get('files');
    }


    public function getNumber()
    {
        return $this->input('number');
    }

}
