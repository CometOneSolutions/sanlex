<?php

namespace App\Modules\Contract\Models;

use App\Modules\Customer\Models\Customer;
use App\Modules\Customer\Models\CustomerModel;
use App\Modules\InboundPayment\Models\InboundPaymentModel;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class ContractModel extends Model implements Contract, UpdatableByUser, HasMedia
{
    use HasUpdatedByUser;
    use HasMediaTrait;

    protected $table = 'contracts';

    protected $dates = ['date'];

    public function customer()
    {
        return $this->belongsTo(CustomerModel::class, 'customer_id');
    }

    public function inboundPayments()
    {
        return $this->hasMany(InboundPaymentModel::class, 'contract_id');
    }

    public function hasPayments()
    {
        return $this->inboundPayments()->exists();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getText()
    {
        return $this->number;
    }

    public function getIndexText()
    {
        return $this->number;
    }

    public function computeBalance()
    {
        $clearedCheckPaymentsSum = $this->inboundPayments()->where('type', 'Check')->where('cleared', 1)->sum('amount');
        $otherPaymentsSum = $this->inboundPayments()->whereIn('type', ['Cash', 'Transfer'])->sum('amount');
        return $this->amount - ($clearedCheckPaymentsSum + $otherPaymentsSum);
    }

    public function updateBalance()
    {
        $this->balance = $this->computeBalance();
        $this->save();
        $this->customer->updateBalance();
    }
}
