import {Form} from '@c1_common_js/components/Form';
import { contractService } from '@c1_module_js/Contract/Resources/Assets/js/contract-main';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm';
import Datepicker from 'vuejs-datepicker';
import Select3 from "@c1_common_js/components/Select3";

new Vue({
    el: '#contract',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, 
        contractService, Datepicker, Select3
    },
    data: {
        form: new Form({
            id: null,
            customerId: null,
            amount: 0,
            date: moment(),
            number: null,
            description: null,
        }),
        isSaving: false,
        isBusy: false,
        isDeleting: false,
        files: [],
        isBusyDeleting: false,
        dataInitialized: true,
        customerUrl: '/api/customers',
        defaultSelectedCustomer: {},
        
    },
    watch: {
        initializationComplete(val) {    
            this.form.isInitializing = false;
        }
        
    },

    computed: {

        initializationComplete() {
            if (this.dataInitialized)
            {
                this.form.isInitializing = false;
                return true;
            }
            return false;
        }
     
    },
    
    methods: {
        uploadFile (event) {
            this.files = event.target.files
        },
        arrangeFiles(){
            return new Promise((resolve,reject) => {
                let formData = new FormData();
                this.files.forEach(file => {
                    console.log(file);
                    if(file.size > 1024 * 1024 * 10){
                        reject();
                    }
                    formData.append('files[]', file);
                });
                resolve(formData);
            });
        },


        deleteFile(id) {  
            this.form.confirm().then((result) => {
                 if(result.value) {
                     this.isDeleting = true;
                     this.isBusyDeleting = true;
                     this.form.delete('/files/' + id)
                     .then(response => {
                         this.isDeleting = false;
                         this.isDeleting = false;
                        this.reloadData()
                     }).catch(error => {
                         this.isDeleting = false;
                         this.isBusyDeleting = false;
                     });
                    
                 }
                 
             });             
         },
        destroy() {  
           this.form.confirm().then((result) => {
                if(result.value) {
                    this.isDeleting = true;
                    this.isBusyDeleting = true;
                    this.form.delete('/api/contracts/' + this.form.id)
                    .then(response => {
                        this.isDeleting = false;
                        this.isDeleting = false;
                        this.$swal({
                            title: 'Success',
                            text: 'Contract was removed.',
                            type: 'success'
                        }).then(() => window.location = '/collections/contracts/');
                    }).catch(error => {
                        this.isDeleting = false;
                        this.isBusyDeleting = false;
                    });
                   
                }
                
            });             
        },
        
        store() {
            this.isSaving = true;
            this.isBusy = true;
            this.arrangeFiles().then(result => {
                let formData = result;
                formData.append('customerId', this.form.customerId);
                formData.append('amount', this.form.amount);
                formData.append('number', this.form.number);
                formData.append('date', this.form.date);
                formData.append('description', this.form.description);
                this.form.submitImage('/api/contracts', formData, 'post')
                .then(response => {
                    this.isSaving = false;
                    this.isBusy = false;
                    this.$swal({
                        title: 'Success',
                        text: 'Contract was saved.',
                        type: 'success'
                    });
                    this.files = [];
                    this.form.reset();
                })
                .catch(error => {
                    this.isSaving = false;
                    this.isBusy = false;
                });
            }).catch(() => {
                this.isSaving = false;
                this.isBusy = false;
                this.$swal({
                    title: 'File too large',
                    text: 'maximum of 10MB only',
                    type: 'error',
                    confirmButtonColor: '#20a8d8',
                });
            });         
        },

        update() {
            this.isSaving = true;
            this.isBusy = true;
            this.arrangeFiles().then(result => {
                let formData = result;
                formData.append('customerId', this.form.customerId);
                formData.append('amount', this.form.amount);
                formData.append('number', this.form.number);
                formData.append('date', this.form.date);
                formData.append('description', this.form.description);
                this.form.submitImage(`/api/contracts/${this.form.id}`, formData, 'post').then(response => {
                    this.isSaving = false;
                    this.isBusy = false;
                    this.$swal({
                        title: 'Success',
                        text: 'Contract was updated.',
                        type: 'success',
                        confirmButtonColor: '#20a8d8',
                    }).then(data => { 
                        
                        window.location = '/collections/contracts/' + this.form.id
                    });  
                }).catch(error => {
                    this.isSaving = false;
                    this.isBusy = false;
                });
            }).catch(() => {
                this.isSaving = false;
                this.isBusy = false;
                this.$swal({
                    title: 'File too large',
                    text: 'maximum of 10MB only',
                    type: 'error',
                    confirmButtonColor: '#20a8d8',
                });
            });

           
        },
       
        loadData(data) {
            this.form = new Form(data);
        },
        reloadData(){
            this.dataInitialized = false;
            this.form.get('/api/contracts/' + this.form.id + '?include=customer,inboundPayments,files')
            .then(response => {
                this.loadData(response.data);
                this.defaultSelectedCustomer = {
                    text: response.data.customer.data.name,
                    id: response.data.customerId,
                };
                this.dataInitialized = true;
            });
        },
    },
    created() {
        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/contracts/' + id + '?include=customer,inboundPayments,files')
            .then(response => {
                this.loadData(response.data);
                this.defaultSelectedCustomer = {
                    text: response.data.customer.data.name,
                    id: response.data.customerId,
                };
                this.dataInitialized = true;
            });
        }
        
    },
    mounted() {
    
        console.log("Init contract script...");
    }
});