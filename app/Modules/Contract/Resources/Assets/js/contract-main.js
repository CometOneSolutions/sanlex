import {Form} from '@c1_common_js/components/Form';

export const contractService = new Vue({
       
    data: function() {
        return {
            form: new Form({}),
            uri: {
                contracts: '/api/contracts?limit=' + Number.MAX_SAFE_INTEGER,
            }      
        }
    },   

    methods: {

        getCustomers(){
            
            return this.form.get(this.uri.contracts);
        },
    }
});

