@extends('app')
@section('breadcrumbs', Breadcrumbs::render('contract.collections.show', $contract))
@section('content')
    <section id="contract" v-cloak>
        <div>
            <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                <div class="col-12 text-center h4">
                    <i class="fas fa-cog fa-spin"></i> Initializing
                </div>
            </div>
            <div v-else>
                <div class="card">
                    <div class="card-header">
                        Contract Record
                        @if (auth()->user()->can('Update [COL] Contract'))
                            <div class="float-right">
                                <a :href="form.editUri" class="btn btn-sm btn-primary" role="button"><i
                                        class="fas fa-pencil-alt"></i></a>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        @include("contracts._list")
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <timestamp :name="form.updatedByUser.data.name || 'System'" :time="form.updatedAt"></timestamp>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </section>
@endsection
@push('scripts')
    <script src="{{ mix('js/contract.js') }}"></script>
@endpush
