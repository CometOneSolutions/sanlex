<div class="form-row">
    <div class="form-group col-md-6">
        <label for="customerName" class="col-form-label">Customer</label>
        <p>@{{ form . customer . data . name }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="date" class="col-form-label">Date</label>
        <p>@{{ form . date }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="amount" class="col-form-label">Amount</label>
        <p>@{{ (form . amount) | numeric }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="balance" class="col-form-label">Balance</label>
        <p>@{{ (form . balance) | numeric }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="description" class="col-form-label">Desciption</label>
        <p>@{{ form . description || '--' }}</p>
    </div>
</div>
<div class="col-12" v-if="form.files.data.length > 0">
    <table class="table table-simple">
        <thead>
            <tr>
                <th>Name</th>
                <th class="text-right">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="file in form.files.data">
                <td>@{{ file . name }}</td>
                <td class="text-right">
                    <a target="_blank" :href="file.viewUrl">
                        <button type="button" class="btn btn-sm btn-primary">
                            <i class="far fa-eye"></i>
                        </button>
                    </a>
                    <a target="_blank" :href="file.downloadUrl">
                        <button type="button" class="btn btn-sm btn-secondary">
                            <i class="fas fa-download"></i>
                        </button>
                    </a>
                    <button type="button" @click="deleteFile(file.id)" class="btn btn-sm btn-danger">
                        <i class="fas fa-trash"></i>
                    </button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="row" v-if="form.hasPayments">
    <div class="col-12">
        <table class="table table-simple">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Check Date</th>
                    <th>Check Bank</th>
                    <th>Check Branch</th>
                    <th>Check Number</th>
                    <th>Cleared</th>
                    <th class="text-right">Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="payment in form.inboundPayments.data">
                    <td><a :href="payment.showUri">@{{ payment . date }}</a></td>
                    <td>@{{ payment . type }}</td>
                    <td>@{{ payment . chkDate || '--' }}</td>
                    <td>@{{ payment . chkBank || '--' }}</td>
                    <td>@{{ payment . chkBranch || '--' }}</td>
                    <td>@{{ payment . chkNumber || '--' }}</td>
                    <td>@{{ payment . type == 'Check' ? (payment . cleared ? 'Yes' : 'No') : '--' }}</td>
                    <td class="text-right">@{{ (payment . amount) | numeric }}</td>
                </tr>
            </tbody>
        </table>
    </div>

</div>
