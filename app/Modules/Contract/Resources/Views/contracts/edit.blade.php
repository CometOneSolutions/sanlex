@extends('app')
@section('breadcrumbs', Breadcrumbs::render('contract.collections.edit', $contract))
@section('content')
    <section id="contract" v-cloak>
        <div>
            <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                <div class="col-12 text-center h4">
                    <i class="fas fa-cog fa-spin"></i> Initializing
                </div>
            </div>
            <div v-else>
                <div class="card">
                    <div class="card-header">
                        Edit Contract Record
                        @if (auth()->user()->can('Delete [COL] Contract'))
                            <div class="float-right">
                                <button class="btn btn-danger btn-sm" @click="destroy" :disabled="isBusyDeleting">
                                    <div v-if="isDeleting">
                                        <i class="fas fa-spinner fa-pulse"></i> Deleting
                                    </div>
                                    <div v-if="!isDeleting">
                                        <i class="far fa-trash-alt"></i>
                                    </div>
                                </button>
                            </div>
                        @endif

                    </div>
                    <v-form @validate="update">
                        <div class="card-body">
                            @include("contracts._form")
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <button class="btn btn-primary btn-sm" type="submit" :disabled="isBusy">
                                    <div v-if="isSaving">
                                        <i class="fas fa-spinner fa-pulse"></i> Saving
                                    </div>
                                    <div v-if="!isSaving">
                                        Save
                                    </div>
                                </button>
                            </div>

                        </div>
                    </v-form>
                </div>

            </div>
        </div>

    </section>
@endsection
