@extends('app')
@section('breadcrumbs', Breadcrumbs::render('contract.collections.create'))
@section('content')
    <section id="contract" v-cloak>
        <div class="card table-responsive">
            <div class="card-header">
                Create New Contract
            </div>
            <v-form @validate="store">
                <div class="card-body">
                    @include("contracts._form")
                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <button class="btn btn-success btn-sm" type="submit" :disabled="isBusy">
                            <div v-if="isSaving">
                                <i class="fas fa-spinner fa-pulse"></i> Saving
                            </div>
                            <div v-if="!isSaving">
                                Create
                            </div>
                        </button>
                    </div>
                </div>
            </v-form>
        </div>
    </section>
@endsection
