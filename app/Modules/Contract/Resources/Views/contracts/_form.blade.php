<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>

<div class="form-group">
    <label for="inputEmail4" class="col-form-label">Customer <span class="text text-danger">*</span></label>
    <select3 class="custom-select" required data-msg-required="Enter Customer" v-model="form.customerId"
        :selected="defaultSelectedCustomer" :url="customerUrl" search="name">
    </select3>
</div>

<div class="form-group">
    <label for="Number" class="col-form-label">Number</label>
    <input type="text" class="form-control" placeholder="" v-model="form.number"
        :class="{'is-invalid': form.errors.has('number') }" data-msg-required="Enter Number">
</div>

<div class="form-group">
    <label for="date">Date <span class="text text-danger">*</span></label>
    <datepicker :typeable="true" :input-class="{'is-invalid': form.errors.has('date'),'form-control': true}"
        v-model="form.date" :required="true"></datepicker>
</div>

<div class="form-group">
    <label for="amount" class="col-form-label">Amount</label>
    <money class="form-control text-right" v-model.number="form.amount"></money>
</div>

<div class="form-group">
    <label for="Desciption" class="col-form-label">Desciption</label>
    <input type="text" class="form-control" placeholder="" v-model="form.description"
        :class="{'is-invalid': form.errors.has('description') }" data-msg-required="Enter Desciption">
</div>

<div class="form-group">
    <label for="Files" class="col-form-label">Files</label>
    <input type="file" @change="uploadFile" multiple>
</div>

@push('scripts')
    <script src="{{ mix('js/contract.js') }}"></script>
@endpush
