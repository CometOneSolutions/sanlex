@extends('app')
@section('breadcrumbs', Breadcrumbs::render('contract.collections'))
@section('content')
    <div id="index" v-cloak>
        <div class="card">
            <div class="card-header">
                Contract
                @if (auth()->user()->can('Create [COL] Contract'))
                    <div class="float-right">
                        <a href="{{ route('contract_create') }}"><button type="button" class="btn btn-success btn-sm"><i
                                    class="fas fa-plus"></i> </button></a>
                    </div>
                @endif
            </div>
            <div class="card-body">
                <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending"
                    v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                    <table class="table table-responsive-sm">
                        <thead>
                            <tr>
                                <th>
                                    <a v-on:click="setSorter('date')">
                                        Date <i class="fa" :class="getSortIcon('date')"></i>
                                    </a>
                                </th>
                                <th>
                                    <a v-on:click="setSorter('number')">
                                        Number <i class="fa" :class="getSortIcon('number')"></i>
                                    </a>
                                </th>
                                <th>
                                    <a v-on:click="setSorter('customer_name')">
                                        Customer <i class="fa" :class="getSortIcon('customer_name')"></i>
                                    </a>
                                </th>
                                <th class="text-right">
                                    <a v-on:click="setSorter('amount')"> Amount <i class="fa"
                                            :class="getSortIcon('amount')"></i>
                                    </a>
                                </th>
                                <th class="text-right">
                                    <a v-on:click="setSorter('balance')"> Balance <i class="fa"
                                            :class="getSortIcon('balance')"></i>
                                    </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item in items" v-if="!isLoading">
                                <td>@{{ item . date }}</td>
                                <td><a :href="item.showUri">@{{ item . number ? item . number : '--' }}</a></td>
                                <td>@{{ item . customer . data . name }}</td>
                                <td class="text-right">@{{ (item . amount) | numeric }}</td>
                                <td class="text-right">@{{ (item . balance) | numeric }}</td>

                            </tr>
                        </tbody>
                    </table>
                </index>
            </div>
        </div>
    </div>
    <!-- End Watchlist-->
@stop
@push('scripts')
    <script src="{{ mix('js/index.js') }}"></script>
@endpush
