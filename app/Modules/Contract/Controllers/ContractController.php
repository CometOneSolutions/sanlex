<?php

namespace App\Modules\Contract\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use Illuminate\Http\Request;
use JavaScript;
use App\Modules\Contract\Services\ContractRecordService;

class ContractController extends Controller
{
    private $service;

    public function __construct(ContractRecordService $contractRecordService)
    {
        $this->service = $contractRecordService;
        $this->middleware('permission:Read [COL] Contract')->only(['show', 'index']);
        $this->middleware('permission:Create [COL] Contract')->only('create');
        $this->middleware('permission:Update [COL] Contract')->only('edit');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'date',
            'sortAscending' => true,
            'baseUrl' => '/api/contracts?include=customer'
        ]);
        return view("contracts.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        JavaScript::put(['id' => null]);
        return view("contracts.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $contract = $this->service->getById($id);
        return view("contracts.show", compact('contract'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $contract = $this->service->getById($id);
        return view("contracts.edit", compact('contract'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getDisplayedFilterable()
    {
        return [
            ["id" => "name", "text" => "Name"]
        ];
    }
}
