<?php

namespace App\Modules\Contract\Controllers;

use App\Modules\Contract\Services\ContractRecordService;
use Illuminate\Http\Request;
use App\Modules\Contract\Transformer\ContractTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Contract\Requests\StoreContract;
use App\Modules\Contract\Requests\UpdateContract;
use App\Modules\Contract\Requests\DestroyContract;


class ContractApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;
    protected $contactRecordService;

    public function __construct(
        ContractRecordService $contractRecordService,
        ContractTransformer $transformer
    ) {
        $this->service = $contractRecordService;
        $this->transformer = $transformer;
        parent::__construct($contractRecordService, $transformer);
        $this->middleware(['auth:api']);
    }

    public function destroy($contractId, DestroyContract $request)
    {
        $contract = \DB::transaction(function () use ($contractId) {
            $contract = $this->service->getById($contractId);
            $this->service->delete($contract);
            return $contract;
        });
        return $this->response->item($contract, $this->transformer)->setStatusCode(200);
    }

    public function store(StoreContract $request)
    {
        $contract = \DB::transaction(function () use ($request) {
            return $this->service->create(
                $request->getCustomer(),
                $request->getNumber(),
                $request->getDate(),
                $request->getAmount(),
                $request->getDescription(),
                $request->getFiles(),
                $request->user()
            );

        });
        return $this->response->item($contract, $this->transformer)->setStatusCode(201);
    }

    public function update($contractId, UpdateContract $request)
    {
        $contract = \DB::transaction(function () use ($request, $contractId) {
            $contract = $this->service->getById($contractId);
            return $this->service->update(
                $contract,
                $request->getCustomer(),
                $request->getNumber(),
                $request->getDate(),
                $request->getAmount(),
                $request->getDescription(),
                $request->getFiles(),
                $request->user()
            );
        });
        return $this->response->item($contract, $this->transformer)->setStatusCode(200);
    }


}
