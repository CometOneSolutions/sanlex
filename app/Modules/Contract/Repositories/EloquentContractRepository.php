<?php

namespace App\Modules\Contract\Repositories;

use App\Modules\Contract\Models\Contract;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentContractRepository extends EloquentRepository implements ContractRepository
{
    public function __construct(Contract $contract)
    {
        parent::__construct($contract);
    }

    public function save(Contract $contract)
    {
        return $contract->save();
    }

    public function delete(Contract $contract)
    {
        return $contract->delete();
    }
    
    protected function orderbyCustomerName($direction)
    {
        return $this->model->join('customers', 'customers.id', 'contracts.customer_id')
            ->select('customers.name as customer_name', 'contracts.*')
            ->orderBy('customer_name', $direction);
    }

    public function filterByCustomerName($value)
    {
        return $this->model->wherehas('customer', function($customer) use ($value){
            return $customer->where('name', 'like', "%{$value}%");
        });
    }

    public function filterByIncludeContractId($value)
    {
        return $this->model->orWhere('id', $value);
    }

    public function filterByHasBalance()
    {
        return $this->model->where('balance', ">", 0);
    }

    public function filterByHasBalanceAndIncludeSelected($selected)
    {
        return $this->model->where('balance', '>', 0)->orWhere('id', $selected);
    }
}
