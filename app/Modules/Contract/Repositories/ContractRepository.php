<?php

namespace App\Modules\Contract\Repositories;

use App\Modules\Contract\Models\Contract;
use CometOneSolutions\Common\Repositories\Repository;

interface ContractRepository extends Repository
{
    public function save(Contract $contract);
    public function delete(Contract $contract);
}
