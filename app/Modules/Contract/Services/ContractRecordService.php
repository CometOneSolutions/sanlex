<?php

namespace App\Modules\Contract\Services;

use App\Modules\Contract\Models\Contract;
use App\Modules\Customer\Models\Customer;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordService;
use DateTime;

interface ContractRecordService extends RecordService
{
    public function create(
        Customer $customer,
        $number,
        DateTime $date,
        $amount,
        $description,
        $files,
        User $user = null
    );

    public function update(
        Contract $contract,
        Customer $customer,
        $number,
        DateTime $date,
        $amount,
        $description,
        $files,
        User $user = null
    );

    public function delete(Contract $contract);

}
