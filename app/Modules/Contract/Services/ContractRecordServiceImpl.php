<?php

namespace App\Modules\Contract\Services;

use App\Modules\Contract\Models\Contract;
use App\Modules\Contract\Repositories\ContractRepository;
use App\Modules\Customer\Models\Customer;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use DateTime;
use CometOneSolutions\Auth\Models\Users\User;

class ContractRecordServiceImpl extends RecordServiceImpl implements ContractRecordService
{

    private $contracts;
    private $contract;

    protected $availableFilters = [
        ['id' => 'customer_name', 'text' => 'Customer'],
    ];

    public function __construct(ContractRepository $contracts, Contract $contract)
    {
        $this->contracts = $contracts;
        $this->contract = $contract;
        parent::__construct($contracts);
    }

    public function create(
        Customer $customer,
        $number,
        DateTime $date,
        $amount,
        $description,
        $files,
        User $user = null
    ){
        $contract = new $this->contract;
        $contract->customer()->associate($customer);
        $contract->customer_id = $customer->id;
        $contract->date = $date;
        $contract->number = $number;
        $contract->amount = $amount;
        $contract->balance = $contract->computeBalance();
        $contract->description = $description;
        if($user){
            $contract->setUpdatedByUser($user);
        }
        if(count($files)){
            foreach($files as $file){
                $contract->addMedia($file->getRealPath())
                    ->usingFileName($file->getClientOriginalName())->usingName($file->getClientOriginalName())->toMediaCollection();
            }
        }
        $customer->updateBalance();
        $this->contracts->save($contract);
        return $contract;
    }

    public function update(
        Contract $contract,
        Customer $customer,
        $number,
        DateTime $date,
        $amount,
        $description,
        $files,
        User $user = null
    ){
        $oldCustomer = $contract->customer;
        $tmpContract = clone $contract;
        $tmpContract->customer()->associate($customer);
        $tmpContract->customer_id = $customer->id;
        $tmpContract->date = $date;
        $tmpContract->number = $number;
        $tmpContract->amount = $amount;
        $tmpContract->balance = $tmpContract->computeBalance();
        $tmpContract->description = $description;
        if($user){
            $tmpContract->setUpdatedByUser($user);
        }
        if(count($files)){
            foreach($files as $file){
                $contract->addMedia($file->getRealPath())
                    ->usingFileName($file->getClientOriginalName())->usingName($file->getClientOriginalName())->toMediaCollection();
            }
        }
        $this->contracts->save($tmpContract);
        $customer->updateBalance();
        $oldCustomer->updateBalance();
        return $this->getById($contract->id);
    }

    public function delete(Contract $contract)
    {
        // TODO delete checks
        $this->contracts->delete($contract);
        return $contract;
    }
}
