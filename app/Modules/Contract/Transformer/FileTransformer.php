<?php

namespace App\Modules\Contract\Transformer;

use App\Modules\Contract\Models\Contract;
use Spatie\MediaLibrary\Media;
use League\Fractal;

class FileTransformer extends Fractal\TransformerAbstract
{

    public function transform(Media $media)
    {
        return [
            'id' => (int) $media->id,
            'fullUrl' => $media->getFullUrl(),
            'path' => $media->getUrl(),
            'name' => $media->name,
            'downloadUrl' => route('media_download', $media->id),
            'viewUrl' => route('media_show', $media->id)
        ];
    }
}
