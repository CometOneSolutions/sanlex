<?php

namespace App\Modules\Contract\Transformer;

use App\Modules\Contract\Models\Contract;
use App\Modules\Customer\Transformer\CustomerTransformer;
use App\Modules\InboundPayment\Transformer\InboundPaymentTransformer;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;


class ContractTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'customer',
        'inboundPayments',
        'files',
    ];

    public function transform(Contract $contract)
    {
        return [
            'id' => (int) $contract->id,
            'customerId' => $contract->customer_id,
            'text' => $contract->getText(),
            'indexText' => $contract->getIndexText(),
            'number' => $contract->number,
            'amount' => $contract->amount,
            'balance' => $contract->balance,
            'hasPayments' => $contract->hasPayments(),
            'description' => $contract->description,
            'date' => $contract->date->toFormattedDateString(),
            'updatedAt' => $contract->updated_at,
            'editUri' => route('contract_edit', $contract->id),
            'showUri' => route('contract_show', $contract->id),
        ];
    }

    public function includeFiles(Contract $contract)
    {
        return $this->collection($contract->getMedia(), new FileTransformer);
    }

    public function includeCustomer(Contract $contract)
    {
        return $this->item($contract->customer, new CustomerTransformer);
    }

    public function includeInboundPayments(Contract $contract)
    {
        return $this->collection($contract->inboundPayments, new InboundPaymentTransformer);
    }
}
