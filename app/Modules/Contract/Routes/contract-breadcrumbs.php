<?php

Breadcrumbs::register('contract.collections', function ($breadcrumbs) {
    $breadcrumbs->parent('collection.index');
    $breadcrumbs->push('Contracts', route('contract_index'));
});
Breadcrumbs::register('contract.collections.create', function ($breadcrumbs) {
    $breadcrumbs->parent('contract.collections');
    $breadcrumbs->push('Create', route('contract_create'));
});
Breadcrumbs::register('contract.collections.show', function ($breadcrumbs, $contract) {
    $breadcrumbs->parent('contract.collections');
    $breadcrumbs->push($contract->id, route('contract_show', $contract->getId()));
});
Breadcrumbs::register('contract.collections.edit', function ($breadcrumbs, $contract) {
    $breadcrumbs->parent('contract.collections.show', $contract);
    $breadcrumbs->push('Edit', route('contract_edit', $contract->getId()));
});
