<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'contracts'], function () use ($api) {
        $api->get('/', 'App\Modules\Contract\Controllers\ContractApiController@index');
        $api->get('{contractId}', 'App\Modules\Contract\Controllers\ContractApiController@show');
        $api->post('/', 'App\Modules\Contract\Controllers\ContractApiController@store');
        $api->post('{contractId}', 'App\Modules\Contract\Controllers\ContractApiController@update');
        $api->delete('{contractId}', 'App\Modules\Contract\Controllers\ContractApiController@destroy');
    });
});
