<?php

Route::group(['prefix' => 'collections'], function () {
    Route::get('/', '\App\Http\Controllers\CollectionMenuDashboardController@dashboard')->name('collection_index');
    Route::group(['prefix' => 'contracts'], function () {
        Route::get('/', 'ContractController@index')->name('contract_index');
        // Route::get('/', function(){
        //     JavaScript::put([
        //         'filterable' => [],
        //         'sorter' => 'name',
        //         'sortAscending' => true,
        //         'baseUrl' => '/api/contracts'
        //     ]);
        //     return view("contracts.index");
        // });
        Route::get('create', 'ContractController@create')->name('contract_create');
        Route::get('{contractId}', 'ContractController@show')->name('contract_show');
        Route::get('{contractId}/edit', 'ContractController@edit')->name('contract_edit');
    });
});
