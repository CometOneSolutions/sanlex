<?php

namespace App\Modules\Contract\Providers;

use App\Modules\Contract\Models\Contract;
use App\Modules\Contract\Models\ContractModel;
use App\Modules\Contract\Repositories\ContractRepository;
use App\Modules\Contract\Repositories\EloquentContractRepository;
use App\Modules\Contract\Services\ContractRecordService;
use App\Modules\Contract\Services\ContractRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ContractServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Contract/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(ContractRecordService::class, ContractRecordServiceImpl::class);
        $this->app->bind(ContractRepository::class, EloquentContractRepository::class);
        $this->app->bind(Contract::class, ContractModel::class);
    }
}
