<?php

namespace App\Modules\StructuralSteel\Services;

use App\Modules\DimensionX\Models\DimensionX;
use App\Modules\DimensionY\Models\DimensionY;
use App\Modules\Finish\Models\Finish;
use App\Modules\Length\Models\Length;
use App\Modules\StructuralSteel\Models\StructuralSteel;
use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface StructuralSteelRecordService extends RecordService
{
	public function create(
		$name,
		Supplier $supplier,

		DimensionX $dimensionX,
		DimensionY $dimensionY,
		Thickness $thickness,
		Length $length,
		StructuralSteelType $structuralSteelType,
		Finish $finish,
		User $user = null
	);

	public function update(
		StructuralSteel $structuralSteel,
		$name,
		Supplier $supplier,

		DimensionX $dimensionX,
		DimensionY $dimensionY,
		Thickness $thickness,
		Length $length,
		StructuralSteelType $structuralSteelType,
		Finish $finish,
		User $user = null
	);

	public function delete(StructuralSteel $structuralSteel);
}
