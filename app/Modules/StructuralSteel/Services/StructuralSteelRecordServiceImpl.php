<?php

namespace App\Modules\StructuralSteel\Services;

use App\Modules\DimensionX\Models\DimensionX;
use App\Modules\DimensionY\Models\DimensionY;
use App\Modules\Finish\Models\Finish;
use App\Modules\Length\Models\Length;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\StructuralSteel\Models\StructuralSteel;
use App\Modules\StructuralSteel\Repositories\StructuralSteelRepository;
use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class StructuralSteelRecordServiceImpl extends RecordServiceImpl implements StructuralSteelRecordService
{
	private $structuralSteel;
	private $structuralSteels;
	private $productRecordService;

	protected $availableFilters = [
		["id" => "name", "text" => "Name"],

	];

	public function __construct(StructuralSteelRepository $structuralSteels, StructuralSteel $structuralSteel, ProductRecordService $productRecordService)
	{
		$this->structuralSteels = $structuralSteels;
		$this->structuralSteel = $structuralSteel;
		$this->productRecordService = $productRecordService;
		parent::__construct($structuralSteels);
	}

	public function create(
		$name,
		Supplier $supplier,

		DimensionX $dimensionX,
		DimensionY $dimensionY,
		Thickness $thickness,
		Length $length,
		StructuralSteelType $structuralSteelType,
		Finish $finish,
		User $user = null
	) {
		$structuralSteel = new $this->structuralSteel;
		$structuralSteel->setDimensionX($dimensionX);
		$structuralSteel->setDimensionY($dimensionY);
		$structuralSteel->setThickness($thickness);
		$structuralSteel->setLength($length);

		$structuralSteel->setStructuralSteelType($structuralSteelType);
		$structuralSteel->setFinish($finish);
		if ($user) {
			$structuralSteel->setUpdatedByUser($user);
		}
		$this->structuralSteels->save($structuralSteel);
		$this->productRecordService->create($name, $supplier, $structuralSteel, $user);
		return $structuralSteel;
	}

	public function update(
		StructuralSteel $structuralSteel,
		$name,
		Supplier $supplier,

		DimensionX $dimensionX,
		DimensionY $dimensionY,
		Thickness $thickness,
		Length $length,

		StructuralSteelType $structuralSteelType,
		Finish $finish,
		User $user = null
	) {
		$tempStructuralSteel = clone $structuralSteel;
		$product = $tempStructuralSteel->getProduct();
		$tempStructuralSteel->setDimensionX($dimensionX);
		$tempStructuralSteel->setDimensionY($dimensionY);
		$tempStructuralSteel->setThickness($thickness);
		$tempStructuralSteel->setLength($length);

		$tempStructuralSteel->setStructuralSteelType($structuralSteelType);
		$tempStructuralSteel->setFinish($finish);
		if ($user) {
			$tempStructuralSteel->setUpdatedByUser($user);
		}
		$this->structuralSteels->save($tempStructuralSteel);
		$this->productRecordService->update($product, $name, $supplier, $tempStructuralSteel, $user);
		return $tempStructuralSteel;
	}

	public function delete(StructuralSteel $structuralSteel)
	{
		$this->structuralSteels->delete($structuralSteel);
		return $structuralSteel;
	}
	public function inStock()
	{
		$this->structuralSteels = $this->structuralSteels->inStock();
		return $this;
	}
}
