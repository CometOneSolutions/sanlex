<?php

namespace App\Modules\StructuralSteel\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\StructuralSteel\Services\StructuralSteelRecordService;


class StructuralSteelController extends Controller
{
	private $service;

	public function __construct(StructuralSteelRecordService $structuralSteelRecordService)
	{
		$this->service = $structuralSteelRecordService;
		$this->middleware('permission:Read [MAS] Structural Steel')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Structural Steel')->only('create');
		$this->middleware('permission:Update [MAS] Structural Steel')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'id',
			'sortAscending' => true,
			'baseUrl' => '/api/structural-steels?include=product,dimensionX,dimensionY,thickness,length,structuralSteelType,finish'
		]);
		return view('structural-steels.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('structural-steels.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$structuralSteel = $this->service->getById($id);
		return view('structural-steels.show', compact('structuralSteel'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$structuralSteel = $this->service->getById($id);
		return view('structural-steels.edit', compact('structuralSteel'));
	}
}
