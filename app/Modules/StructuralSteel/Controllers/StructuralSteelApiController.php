<?php

namespace App\Modules\StructuralSteel\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\StructuralSteel\Services\StructuralSteelRecordService;
use App\Modules\StructuralSteel\Transformer\StructuralSteelTransformer;
use App\Modules\StructuralSteel\Requests\StoreStructuralSteel;
use App\Modules\StructuralSteel\Requests\UpdateStructuralSteel;
use App\Modules\StructuralSteel\Requests\DestroyStructuralSteel;
use App\Modules\Product\Services\ProductRecordService;


class StructuralSteelApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;
	protected $productRecordService;

	public function __construct(
		StructuralSteelRecordService $structuralSteelRecordService,
		StructuralSteelTransformer $transformer,
		ProductRecordService $productRecordService
	) {
		$this->middleware('auth:api');
		parent::__construct($structuralSteelRecordService, $transformer);
		$this->service = $structuralSteelRecordService;
		$this->transformer = $transformer;
		$this->productRecordService = $productRecordService;
	}

	public function store(StoreStructuralSteel $request)
	{
		$structuralSteel = \DB::transaction(function () use ($request) {
			$name = $request->getName();

			return $this->service->create(
				$name,
				$request->getSupplier(),
				$request->getDimensionX(),
				$request->getDimensionY(),
				$request->getThickness(),
				$request->getLength(),
				$request->getStructuralSteelType(),
				$request->getFinish(),


				$request->user()
			);
		});
		return $this->response->item($structuralSteel, $this->transformer)->setStatusCode(201);
	}

	public function update($structuralSteelId, UpdateStructuralSteel $request)
	{
		$structuralSteel = \DB::transaction(function () use ($request) {
			$name = $request->getName();

			return $this->service->update(
				$request->getStructuralSteel(),
				$name,
				$request->getSupplier(),
				$request->getDimensionX(),
				$request->getDimensionY(),
				$request->getThickness(),
				$request->getLength(),
				$request->getStructuralSteelType(),
				$request->getFinish(),
				$request->user()
			);
		});
		return $this->response->item($structuralSteel, $this->transformer)->setStatusCode(200);
	}

	public function destroy($structuralSteelId, DestroyStructuralSteel $request)
	{
		$structuralSteel = $this->service->getById($structuralSteelId);
		$this->service->delete($structuralSteel);
		return $this->response->item($structuralSteel, $this->transformer)->setStatusCode(200);
	}
}
