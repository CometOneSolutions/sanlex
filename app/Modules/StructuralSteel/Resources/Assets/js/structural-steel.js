import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select3 from "@c1_common_js/components/Select3";


new Vue({
    el: '#structural-steel',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select3
    },
    data: {
        form: new Form({
            name: null,
            supplierId: null,
            shortCode: null,
            dimensionXId: null,
            dimensionYId: null,
            dimensionXText: null,
            dimensionYText: null,
            lengthId: null,
            lengthText: null,
            thicknessId: null,
            thicknessText: null,
            structuralSteelTypeId: null,
            structuralSteelTypeText: null,
            finishId: null,
            finishText: null
        }),
        dataInitialized: true,
        supplierUrl: '/api/suppliers?sort=name',
        defaultSelectedSupplier: {},
        dimensionXUrl: '/api/dimension-x?sort=name',
        defaultSelectedDimensionX: {},
        dimensionYUrl: '/api/dimension-y?sort=name',
        defaultSelectedDimensionY: {},
        lengthUrl: '/api/lengths?sort=name',
        defaultSelectedLength: {},
        thicknessUrl: '/api/thicknesses?sort=name',
        defaultSelectedThickness: {},
        finishUrl: '/api/finishes?sort=name',
        defaultSelectedFinish: {},
        structuralSteelTypeUrl: '/api/structural-steel-types?sort=name',
        defaultSelectedStructuralSteelType: {},

    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },
    computed: {
        // selectedSupplier() {
        //     if(this.form.supplierId == null)
        //     {
        //         return undefined;
        //     }
        //     return this.supplierSelections.find(supplier => supplier.id == this.form.supplierId);
        // },
        name() {
            if (this.form.supplierId === undefined || this.form.dimensionXId === null || this.form.dimensionYId === null || this.form.lengthId === null || this.form.structuralSteelTypeId === null || this.form.thicknessId === null || this.form.finishId === null) {
                return undefined;
            }
            return this.form.shortCode + '~' + this.form.structuralSteelTypeText.toUpperCase() + '~' + this.form.finishText.toUpperCase()
                + '~' + this.form.thicknessText.toUpperCase() + '~' + this.form.dimensionXText.toUpperCase() + '~' + this.form.dimensionYText.toUpperCase() + '~' + this.form.lengthText.toUpperCase();
        },
        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        setShortCode(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("shortCode")) {
                this.form.shortCode = selectedObjects[0].shortCode;
            }
        },
        setLength(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.lengthText = selectedObjects[0].name;
            }

        },
        setThickness(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.thicknessText = selectedObjects[0].name;
            }

        },
        setFinish(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.finishText = selectedObjects[0].name;
            }

        },
        setDimensionX(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.dimensionXText = selectedObjects[0].name;
            }

        },
        setDimensionY(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.dimensionYText = selectedObjects[0].name;
            }

        },
        setStructuralSteelType(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.structuralSteelTypeText = selectedObjects[0].name;
            }

        },
        destroy() {
            this.form.deleteWithConfirmation('/api/structural-steels/' + this.form.id).then(response => {
                this.form.successModal('Structural steel was removed.').then(() =>
                    window.location = '/master-file/structural-steels/'
                );
            });
        },

        store() {
            this.form.name = this.name;
            this.form.postWithModal('/api/structural-steels', null, 'Structural steel was saved.');
        },

        update() {
            this.form.name = this.name;
            this.form.patch('/api/structural-steels/' + this.form.id).then(response => {
                this.form.successModal('Structural steel was updated.').then(() =>
                    window.location = '/master-file/structural-steels/' + this.form.id
                );
            })
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {


        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/structural-steels/' + id + '?include=product.supplier,dimensionX,dimensionY,thickness,length,structuralSteelType,finish')
                .then(response => {
                    this.loadData(response.data);

                    this.form.shortCode = response.data.product.data.supplier.data.shortCode;
                    this.form.lengthText = response.data.length.data.name;
                    this.form.dimensionXText = response.data.dimensionX.data.name;
                    this.form.dimensionYText = response.data.dimensionY.data.name;
                    this.form.thicknessText = response.data.thickness.data.name;
                    this.form.finishText = response.data.finish.data.name;
                    this.form.structuralSteelTypeText = response.data.structuralSteelType.data.name;

                    this.defaultSelectedSupplier = {
                        text: response.data.product.data.supplier.data.name,
                        id: response.data.product.data.supplier.data.id,
                    };
                    this.defaultSelectedDimensionX = {
                        text: response.data.dimensionX.data.name,
                        id: response.data.dimensionX.data.id,
                    };
                    this.defaultSelectedDimensionY = {
                        text: response.data.dimensionY.data.name,
                        id: response.data.dimensionY.data.id,
                    };
                    this.defaultSelectedThickness = {
                        text: response.data.thickness.data.name,
                        id: response.data.thickness.data.id,
                    };
                    this.defaultSelectedLength = {
                        text: response.data.length.data.name,
                        id: response.data.length.data.id,
                    };

                    this.defaultSelectedStructuralSteelType = {
                        text: response.data.structuralSteelType.data.name,
                        id: response.data.structuralSteelType.data.id,
                    };
                    this.defaultSelectedFinish = {
                        text: response.data.finish.data.name,
                        id: response.data.finish.data.id,
                    };
                    this.dataInitialized = true;
                });
        }
    },
    mounted() {
        console.log("Init structural steel script...");
    }
});