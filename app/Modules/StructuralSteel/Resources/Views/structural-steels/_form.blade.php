<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Supplier <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter supplier" v-model="form.supplierId" :selected="defaultSelectedSupplier" :url="supplierUrl" search="name" placeholder="Please select" @change="setShortCode" id="supplier" name="supplier">
            </select3>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Dimension X <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter dimension x" v-model="form.dimensionXId" :selected="defaultSelectedDimensionX" :url="dimensionXUrl" search="name" placeholder="Please select" @change="setDimensionX" id="dimensionX" name="dimensionX">
            </select3>
        </div>
	</div>
	<div class="col-12">
        <div class="form-group">
            <label for="supplier">Dimension Y <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter dimension y" v-model="form.dimensionYId" :selected="defaultSelectedDimensionY" :url="dimensionYUrl" search="name" placeholder="Please select" @change="setDimensionY" id="dimensionY" name="dimensionY">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Length <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter length" v-model="form.lengthId" :selected="defaultSelectedLength" :url="lengthUrl" search="name" placeholder="Please select" id="length" @change="setLength" name="length">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Thickness <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter thickness" v-model="form.thicknessId" :selected="defaultSelectedThickness" :url="thicknessUrl" search="name" placeholder="Please select" @change="setThickness" id="thickness" name="thickness">
            </select3>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Structural Steel Type <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter structural steel class" v-model="form.structuralSteelTypeId" :selected="defaultSelectedStructuralSteelType" :url="structuralSteelTypeUrl" @change="setStructuralSteelType" search="name" placeholder="Please select" id="structuralSteelType" name="structuralSteelType">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Finish Type <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter finish type" v-model="form.finishId" :selected="defaultSelectedFinish" :url="finishUrl" @change="setFinish" search="name" placeholder="Please select" id="finish" name="finish">
            </select3>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{ mix('js/structural-steel.js') }}"></script>
@endpush