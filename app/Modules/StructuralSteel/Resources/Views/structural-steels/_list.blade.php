<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Name</label>
            <p>@{{form.product.data.name }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Supplier</label>
            <p>@{{form.product.data.supplier.data.name }}</p>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Dimension X</label>
            <p>@{{form.dimensionX.data.name || "--" }}</p>
        </div>
	</div>
	<div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Dimension Y</label>
            <p>@{{form.dimensionY.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Thickness</label>
            <p>@{{form.thickness.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Length</label>
            <p>@{{form.length.data.name || "--" }}</p>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Structural Steel Type</label>
            <p>@{{form.structuralSteelType.data.name || "--" }}</p>
        </div>
	</div>
	<div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Finish Type</label>
            <p>@{{form.finish.data.name || "--" }}</p>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{ mix('js/structural-steel.js') }}"></script>
@endpush