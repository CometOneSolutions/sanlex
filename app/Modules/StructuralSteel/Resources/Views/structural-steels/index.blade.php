@extends('app')
@section('breadcrumbs', Breadcrumbs::render('structural-steel.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Structural Steels
            @if(auth()->user()->can('Create [MAS] Stainless Steel'))
            <div class="float-right">
                <a href="{{route('structural-steel_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
            </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sort-ascending="sortAscending" :sorter="sorter" v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('id')">
                                    Product <i class="fa" :class="getSortIcon('id')"></i>
                                </a>
                            </th>
							<th>Dimension X</th>
							<th>Dimension Y</th>
                            <th>Thickness</th>
                            <th>Length</th>
                            <th>Structural Type</th>
                            <th>Finish Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{ item.product.data.name }}</a></td>
							<td>@{{item.dimensionX.data.name}}</td>
							<td>@{{item.dimensionY.data.name}}</td>
                            <td>@{{item.thickness.data.name}}</td>
                            <td>@{{item.length.data.name}}</td>

                            <td>@{{item.structuralSteelType.data.name}}</td>
                            <td>@{{item.finish.data.name}}</td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush