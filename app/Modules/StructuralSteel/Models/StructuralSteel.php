<?php

namespace App\Modules\StructuralSteel\Models;



interface StructuralSteel
{
	public function getId();

	public function getDescription();

	public function setDescription($value);
}
