<?php

namespace App\Modules\StructuralSteel\Models;

use App\Modules\DimensionX\Models\DimensionX;
use App\Modules\DimensionX\Models\DimensionXModel;
use App\Modules\DimensionY\Models\DimensionY;
use App\Modules\DimensionY\Models\DimensionYModel;
use App\Modules\Finish\Models\Finish;
use App\Modules\Finish\Models\FinishModel;
use App\Modules\Length\Models\Length;
use App\Modules\Length\Models\LengthModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Product\Traits\Productable;
use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use App\Modules\StructuralSteelType\Models\StructuralSteelTypeModel;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Thickness\Models\ThicknessModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class StructuralSteelModel extends Model implements StructuralSteel, UpdatableByUser, Productable
{
	use HasUpdatedByUser;

	protected $table = 'structural_steels';

	public function product()
	{
		return $this->morphOne(ProductModel::class, 'productable');
	}

	public function getProduct()
	{
		return $this->product;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setDescription($value)
	{
		$this->description = $value;
		return $this;
	}

	public function structuralSteelType()
	{
		return $this->belongsTo(StructuralSteelTypeModel::class, 'structural_steel_type_id');
	}
	public function finish()
	{
		return $this->belongsTo(FinishModel::class, 'finish_id');
	}
	public function dimensionX()
	{
		return $this->belongsTo(DimensionXModel::class, 'dimension_x_id');
	}
	public function dimensionY()
	{
		return $this->belongsTo(DimensionYModel::class, 'dimension_y_id');
	}

	public function length()
	{
		return $this->belongsTo(LengthModel::class, 'length_id');
	}

	public function thickness()
	{
		return $this->belongsTo(ThicknessModel::class, 'thickness_id');
	}

	public function getThicknessId()
	{
		return (int) $this->thickness_id;
	}
	public function getThickness()
	{
		return $this->thickness;
	}

	public function setThickness(Thickness $thickness)
	{
		$this->thickness()->associate($thickness);
		$this->thickness_id = $thickness->getId();
		return $this;
	}

	public function getFinishId()
	{
		return (int) $this->finish_id;
	}
	public function getFinish()
	{
		return $this->finish;
	}

	public function setFinish(Finish $finish)
	{
		$this->finish()->associate($finish);
		$this->finish_id = $finish->getId();
		return $this;
	}
	public function getDimensionXId()
	{
		return (int) $this->dimension_x_id;
	}
	public function getDimensionX()
	{
		return $this->dimensionX;
	}
	public function setDimensionX(DimensionX $x)
	{
		$this->dimensionX()->associate($x);
		return $this;
	}
	public function getDimensionYId()
	{
		return (int) $this->dimension_y_id;
	}
	public function getDimensionY()
	{
		return $this->dimensionY;
	}
	public function setDimensionY(DimensionY $y)
	{
		$this->dimensionY()->associate($y);
		return $this;
	}

	public function getLengthId()
	{
		return (int) $this->length_id;
	}
	public function getLength()
	{
		return $this->length;
	}

	public function setLength(Length $length)
	{
		$this->length()->associate($length);
		$this->length_id = $length->getId();
		return $this;
	}

	public function getStructuralSteelType()
	{
		return $this->structuralSteelType;
	}

	public function setStructuralSteelType(StructuralSteelType $structuralSteelType)
	{
		$this->structuralSteelType()->associate($structuralSteelType);
		return $this;
	}

	public function getStructuralSteelTypeId()
	{
		return $this->structuralSteel_class_id;
	}

	public function isCoil()
	{
		return false;
	}
	public function scopeInStock($query)
	{
		return $query->whereHas('product', function ($product) {
			return $product->whereHas('inventory', function ($inventory) {
				return $inventory->where('stock', '>', 0);
			});
		});
	}

	public function getAssembledProductName()
	{
		$nameAttributes = [
			$this->getProduct()->getSupplier()->getShortCode(),
			$this->getStructuralSteelType()->getName(),
			$this->getFinish()->getName(),
			$this->getThickness()->getName(),
			$this->getDimensionX()->getName(),
			$this->getDimensionY()->getName(),
			$this->getLength()->getName()
		];

		return  strtoupper(implode("~", $nameAttributes));
	}

	public function scopeStructuralSteelType($query, StructuralSteelType $structuralSteelType)
	{
		return $query->whereStructuralSteelTypeId($structuralSteelType->getId());
	}

	public function scopeFinish($query, Finish $finish)
	{
		return $query->whereFinishId($finish->getId());
	}

	public function scopeDimensionX($query, DimensionX $dimensionX)
	{
		return $query->whereDimensionXId($dimensionX->getId());
	}

	public function scopeDimensionY($query, DimensionY $dimensionY)
	{
		return $query->whereDimensionYId($dimensionY->getId());
	}

	public function scopeLength($query, Length $length)
	{
		return $query->whereLengthId($length->getId());
	}

	public function scopeThickness($query, Thickness $thickness)
	{
		return $query->whereThicknessId($thickness->getId());
	}
}
