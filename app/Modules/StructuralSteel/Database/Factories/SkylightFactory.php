<?php

use Faker\Generator as Faker;
use App\Modules\StainlessSteel\Models\StainlessSteelModel;
use CometOneSolutions\Auth\UserModel;

$factory->define(StainlessSteelModel::class, function (Faker $faker) {
    return [
        'description' => $faker->unique()->company,
        'updated_by_user_id' => $faker->randomElement(UserModel::pluck('id')->toArray()),
    ];
});
