<?php

namespace App\Modules\StructuralSteel\Repositories;

use App\Modules\StructuralSteel\Models\StructuralSteel;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentStructuralSteelRepository extends EloquentRepository implements StructuralSteelRepository
{
	public function __construct(StructuralSteel $structuralSteel)
	{
		parent::__construct($structuralSteel);
	}

	public function save(StructuralSteel $structuralSteel)
	{
		return $structuralSteel->save();
	}

	public function delete(StructuralSteel $structuralSteel)
	{
		$product = $structuralSteel->getProduct();
		$product->delete();
		return $structuralSteel->delete();
	}
	public function filterByName($productName)
	{
		return $this->model->whereHas('product', function ($product) use ($productName) {
			return $product->where('name', 'like', '%' . $productName . '%');
		});
	}
	public function filterBySupplierName($supplierName)
	{
		return $this->model->whereHas('product', function ($product) use ($supplierName) {
			return $product->whereHas('supplier', function ($supplier) use ($supplierName) {
				return $supplier->where('name', 'like', '%' . $supplierName . '%');
			});
		});
	}
	public function inStock()
	{
		$this->model = $this->model->inStock();
		return $this;
	}
}
