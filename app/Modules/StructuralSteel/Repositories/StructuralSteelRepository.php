<?php

namespace App\Modules\StructuralSteel\Repositories;

use App\Modules\StructuralSteel\Models\StructuralSteel;
use CometOneSolutions\Common\Repositories\Repository;

interface StructuralSteelRepository extends Repository
{
	public function save(StructuralSteel $structuralSteel);
	public function delete(StructuralSteel $structuralSteel);
}
