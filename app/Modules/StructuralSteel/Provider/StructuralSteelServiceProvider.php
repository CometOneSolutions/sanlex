<?php

namespace App\Modules\StructuralSteel\Provider;

use App\Modules\StructuralSteel\Models\StructuralSteel;
use App\Modules\StructuralSteel\Models\StructuralSteelModel;
use App\Modules\StructuralSteel\Repositories\EloquentStructuralSteelRepository;
use App\Modules\StructuralSteel\Repositories\StructuralSteelRepository;
use App\Modules\StructuralSteel\Services\StructuralSteelRecordService;
use App\Modules\StructuralSteel\Services\StructuralSteelRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class StructuralSteelServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/StructuralSteel/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(StructuralSteelRecordService::class, StructuralSteelRecordServiceImpl::class);
		$this->app->bind(StructuralSteelRepository::class, EloquentStructuralSteelRepository::class);
		$this->app->bind(StructuralSteel::class, StructuralSteelModel::class);
	}
}
