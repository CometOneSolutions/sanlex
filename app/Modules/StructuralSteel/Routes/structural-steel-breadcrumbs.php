<?php

Breadcrumbs::register('structural-steel.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Structural Steels', route('structural-steel_index'));
});
Breadcrumbs::register('structural-steel.create', function ($breadcrumbs) {
	$breadcrumbs->parent('structural-steel.index');
	$breadcrumbs->push('Create', route('structural-steel_create'));
});
Breadcrumbs::register('structural-steel.show', function ($breadcrumbs, $structuralSteel) {
	$breadcrumbs->parent('structural-steel.index');
	$breadcrumbs->push($structuralSteel->getProduct()->getName(), route('structural-steel_show', $structuralSteel->getId()));
});
Breadcrumbs::register('structural-steel.edit', function ($breadcrumbs, $structuralSteel) {
	$breadcrumbs->parent('structural-steel.show', $structuralSteel);
	$breadcrumbs->push('Edit', route('structural-steel_edit', $structuralSteel->getId()));
});
