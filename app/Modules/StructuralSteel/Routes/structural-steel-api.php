<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'structural-steels'], function () use ($api) {
		$api->get('/', 'App\Modules\StructuralSteel\Controllers\StructuralSteelApiController@index');
		$api->get('{structuralSteelId}', 'App\Modules\StructuralSteel\Controllers\StructuralSteelApiController@show');
		$api->post('/', 'App\Modules\StructuralSteel\Controllers\StructuralSteelApiController@store');
		$api->patch('{structuralSteelId}', 'App\Modules\StructuralSteel\Controllers\StructuralSteelApiController@update');
		$api->delete('{structuralSteelId}', 'App\Modules\StructuralSteel\Controllers\StructuralSteelApiController@destroy');
	});
});
