<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'structural-steels'], function () {
		Route::get('/', 'StructuralSteelController@index')->name('structural-steel_index');
		Route::get('/create', 'StructuralSteelController@create')->name('structural-steel_create');
		Route::get('{structuralSteelId}/edit', 'StructuralSteelController@edit')->name('structural-steel_edit');
		Route::get('{structuralSteelId}/print', 'StructuralSteelController@print')->name('structural-steel_print');
		Route::get('{structuralSteelId}', 'StructuralSteelController@show')->name('structural-steel_show');
	});
});
