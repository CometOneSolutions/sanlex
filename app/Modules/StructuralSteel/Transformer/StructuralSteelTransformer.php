<?php

namespace App\Modules\StructuralSteel\Transformer;

use App\Modules\DimensionX\Transformer\DimensionXTransformer;
use App\Modules\DimensionY\Transformer\DimensionYTransformer;
use App\Modules\Finish\Transformer\FinishTransformer;
use App\Modules\Length\Transformer\LengthTransformer;
use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\StructuralSteel\Models\StructuralSteel;
use App\Modules\StructuralSteelType\Transformer\StructuralSteelTypeTransformer;
use App\Modules\Thickness\Transformer\ThicknessTransformer;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class StructuralSteelTransformer extends UpdatableByUserTransformer
{
	protected $availableIncludes = [
		'product', 'dimensionX', 'dimensionY', 'thickness', 'length', 'structuralSteelType', 'finish'
	];
	public function transform(StructuralSteel $structuralSteel)
	{
		return [
			'id' => (int) $structuralSteel->getId(),
			'name' => $structuralSteel->getProduct() ? $structuralSteel->getProduct()->getName() : null,
			'supplierId' => $structuralSteel->getProduct() ? $structuralSteel->getProduct()->getSupplier()->getId() : null,
			'dimensionXId' => $structuralSteel->getDimensionXId(),
			'dimensionXText' => null,
			'dimensionYId' => $structuralSteel->getDimensionYId(),
			'dimensionYText' => null,
			'thicknessId' => $structuralSteel->getThicknessId(),
			'thicknessText' => null,
			'lengthId'    => $structuralSteel->getLengthId(),
			'lengthText' => null,
			'finishId'    => $structuralSteel->getFinishId(),
			'finishText' => null,
			'structuralSteelTypeId' => $structuralSteel->getStructuralSteelTypeId(),
			'structuralSteelTypeText' => null,

			'updatedAt' => $structuralSteel->updated_at,
			'editUri' => route('structural-steel_edit', $structuralSteel->getId()),
			'showUri' => route('structural-steel_show', $structuralSteel->getId()),
		];
	}

	public function includeProduct(StructuralSteel $structuralSteel)
	{
		$product = $structuralSteel->getProduct();
		return $this->item($product, new ProductTransformer);
	}
	public function includeDimensionX(StructuralSteel $structuralSteel)
	{
		$dimensionX = $structuralSteel->getDimensionX();
		return $this->item($dimensionX, new DimensionXTransformer);
	}
	public function includeDimensionY(StructuralSteel $structuralSteel)
	{
		$dimensionY = $structuralSteel->getDimensionY();
		return $this->item($dimensionY, new DimensionYTransformer);
	}
	public function includeThickness(StructuralSteel $structuralSteel)
	{
		$thickness = $structuralSteel->getThickness();
		return $this->item($thickness, new ThicknessTransformer);
	}
	public function includeLength(StructuralSteel $structuralSteel)
	{
		$length = $structuralSteel->getLength();
		return $this->item($length, new LengthTransformer);
	}

	public function includeStructuralSteelType(StructuralSteel $structuralSteel)
	{
		$structuralSteelType = $structuralSteel->getStructuralSteelType();
		return $this->item($structuralSteelType, new StructuralSteelTypeTransformer);
	}
	public function includeFinish(StructuralSteel $structuralSteel)
	{
		$finish = $structuralSteel->getFinish();
		return $this->item($finish, new FinishTransformer);
	}
}
