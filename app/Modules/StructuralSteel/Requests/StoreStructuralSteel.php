<?php

namespace App\Modules\StructuralSteel\Requests;

use Dingo\Api\Http\FormRequest;

class StoreStructuralSteel extends StructuralSteelRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [MAS] Structural Steel');
	}
}
