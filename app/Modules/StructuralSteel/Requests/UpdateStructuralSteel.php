<?php

namespace App\Modules\StructuralSteel\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateStructuralSteel extends StructuralSteelRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] Structural Steel');
	}
}
