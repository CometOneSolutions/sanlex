<?php

namespace App\Modules\StructuralSteel\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyStructuralSteel extends StructuralSteelRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Structural Steel');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
