<?php

namespace App\Modules\StructuralSteel\Requests;

use App\Modules\DimensionX\Services\DimensionXRecordService;
use App\Modules\DimensionY\Services\DimensionYRecordService;
use App\Modules\Finish\Services\FinishRecordService;
use App\Modules\Length\Services\LengthRecordService;
use App\Modules\StructuralSteel\Services\StructuralSteelRecordService;
use App\Modules\StructuralSteelType\Services\StructuralSteelTypeRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\Thickness\Services\ThicknessRecordService;
use Dingo\Api\Http\FormRequest;

class StructuralSteelRequest extends FormRequest
{
	private $service;
	private $supplierRecordService;
	private $dimensionXRecordService;
	private $dimensionYRecordService;
	private $lengthRecordService;
	private $thicknessRecordService;
	private $structuralSteelTypeRecordService;
	private $finishRecordService;

	public function __construct(
		SupplierRecordService $supplierRecordService,
		StructuralSteelRecordService $structuralSteelRecordService,
		DimensionXRecordService $dimensionXRecordService,
		DimensionYRecordService $dimensionYRecordService,
		LengthRecordService $lengthRecordService,
		ThicknessRecordService $thicknessRecordService,
		StructuralSteelTypeRecordService $structuralSteelTypeRecordService,
		FinishRecordService $finishRecordService
	) {
		$this->supplierRecordService = $supplierRecordService;
		$this->service = $structuralSteelRecordService;
		$this->dimensionXRecordService = $dimensionXRecordService;
		$this->dimensionYRecordService = $dimensionYRecordService;
		$this->lengthRecordService = $lengthRecordService;
		$this->thicknessRecordService = $thicknessRecordService;
		$this->structuralSteelTypeRecordService = $structuralSteelTypeRecordService;
		$this->finishRecordService = $finishRecordService;
	}

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$structuralSteel = $this->route('structuralSteelId') ? $this->service->getById($this->route('structuralSteelId')) : null;

		if ($structuralSteel) {
			return [
				'name'          => 'required|unique:products,name,' . $structuralSteel->getProduct()->getId(),

				'dimensionXId'       => 'required',
				'dimensionYId'       => 'required',
				'lengthId'      => 'required',
				'thicknessId'   => 'required',
				'finishId'      => 'required',
				'structuralSteelTypeId' => 'required'
			];
		} else {
			return [
				'name'                  => 'required|unique:products,name',

				'dimensionXId'       => 'required',
				'dimensionYId'       => 'required',
				'lengthId'      => 'required',
				'thicknessId'   => 'required',
				'finishId'      => 'required',
				'structuralSteelTypeId' => 'required'
			];
		}
	}
	public function messages()
	{
		return [
			'name.required'                  => 'Name is required.',
			'name.unique'                    => 'Name already exists.',
			'dimensionXId.required'          => 'Dimension X is required.',
			'dimensionYId.required'          => 'Dimension Y is required.',
			'lengthId.required'              => 'Length is required.',
			'thicknessId.required'           => 'Thickness is required.',
			'finishId.required'				 => 'Finish type is required.',
			'structuralSteelTypeId.required'       => 'Structural steel type is required.'
		];
	}



	public function getName($index = 'name')
	{
		return $this->input($index);
	}

	public function getSupplier($index = 'supplierId')
	{
		return $this->supplierRecordService->getById($this->input($index));
	}

	public function getStructuralSteel($index = 'id')
	{
		return $this->service->getById($this->input($index));
	}

	public function getDimensionX($index = 'dimensionXId')
	{
		return $this->dimensionXRecordService->getById($this->input($index));
	}
	public function getDimensionY($index = 'dimensionYId')
	{
		return $this->dimensionYRecordService->getById($this->input($index));
	}
	public function getLength($index = 'lengthId')
	{
		return $this->lengthRecordService->getById($this->input($index));
	}
	public function getThickness($index = 'thicknessId')
	{

		return $this->thicknessRecordService->getById($this->input($index));
	}
	public function getStructuralSteelType($index = 'structuralSteelTypeId')
	{
		return $this->structuralSteelTypeRecordService->getById($this->input($index));
	}
	public function getFinish($index = 'finishId')
	{
		return $this->finishRecordService->getById($this->input($index));
	}
}
