<?php

namespace App\Modules\SummaryOrderDetail\Services;

use CometOneSolutions\Common\Services\RecordService;

interface SummaryOrderDetailRecordService extends RecordService
{ }
