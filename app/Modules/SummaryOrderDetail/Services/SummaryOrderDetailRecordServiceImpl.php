<?php

namespace App\Modules\SummaryOrderDetail\Services;

use App\Modules\SummaryOrderDetail\Repositories\SummaryOrderDetails;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class SummaryOrderDetailRecordServiceImpl extends RecordServiceImpl implements SummaryOrderDetailRecordService
{
    private $summaryOrderDetails;

    public function __construct(SummaryOrderDetails $summaryOrderDetails)
    {
        parent::__construct($summaryOrderDetails);
        $this->summaryOrderDetails = $summaryOrderDetails;
    }
}
