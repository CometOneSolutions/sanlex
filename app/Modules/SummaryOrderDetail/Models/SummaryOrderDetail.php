<?php

namespace App\Modules\SummaryOrderDetail\Models;

interface SummaryOrderDetail
{

    public function getName();

    public function setName($value);

    public function getOrder();

    public function setOrder($value);
}
