<?php

namespace App\Modules\SummaryOrderDetail\Models;

use App\Modules\SummaryOrder\Models\SummaryOrderModel;
use Illuminate\Database\Eloquent\Model;

class SummaryOrderDetailModel extends Model implements SummaryOrderDetail
{

    protected $table = 'summary_order_details';
    protected $inventoryDetailsToSet = null;

    public function summaryOrder()
    {
        return $this->belongsTo(SummaryOrderModel::class, 'summary_order_id');
    }

    public function getSummaryOrder()
    {
        return $this->summaryOrder;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($value)
    {
        $this->order = $value;
        return $this;
    }

    public function scopeName($query, $name)
    {
        return $query->whereName($name);
    }

    public function scopeReportName($query, $reportName)
    {
        return $query->whereHas('summaryOrder', function ($summaryOrder) use ($reportName) {
            return $summaryOrder->whereReport($reportName);
        });
    }

    public function scopeLastOrder($query)
    {
        return $query->orderBy('order', 'desc');
    }
}
