<?php

namespace App\Modules\SummaryOrderDetail\Repositories;

use App\Modules\SummaryOrderDetail\Models\SummaryOrderDetail;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentSummaryOrderDetails extends EloquentRepository implements SummaryOrderDetails
{
    protected $model;

    public function __construct(SummaryOrderDetail $summaryOrderDetail)
    {
        parent::__construct($summaryOrderDetail);
    }

    public function save(SummaryOrderDetail $summaryOrderDetail)
    {
        return $summaryOrderDetail->save();
    }

    public function delete(SummaryOrderDetail $summaryOrderDetail)
    {
        return $summaryOrderDetail->delete();
    }

    public function name($name)
    {
        $this->model = $this->model->name($name);

        return $this;
    }

    public function lastOrder()
    {
        $this->model = $this->model->lastOrder();
        return $this;
    }

    public function reportName($reportName)
    {
        $this->model = $this->model->reportName($reportName);
        return $this;
    }
}
