<?php

namespace App\Modules\SummaryOrderDetail\Transformer;

use App\Modules\SummaryOrder\Transformer\SummaryOrderTransformer;
use App\Modules\SummaryOrderDetail\Models\SummaryOrderDetail;
use League\Fractal;

class SummaryOrderDetailTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'summaryOrder'
    ];

    public function transform(SummaryOrderDetail $summaryOrderDetail)
    {

        return [
            'id'          => (int) $summaryOrderDetail->id,
            'name'        => $summaryOrderDetail->getName(),
            'order'       => (int) $summaryOrderDetail->getOrder(),
        ];
    }

    public function includeSummaryOrder(SummaryOrderDetail $summaryOrderDetail)
    {
        $summaryOrder = $summaryOrderDetail->getSummaryOrder();
        return $this->item($summaryOrder, new SummaryOrderTransformer);
    }
}
