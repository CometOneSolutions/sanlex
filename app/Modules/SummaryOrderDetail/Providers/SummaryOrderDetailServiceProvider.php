<?php

namespace App\Modules\SummaryOrderDetail\Providers;

use App\Modules\SummaryOrderDetail\Models\SummaryOrderDetail;
use App\Modules\SummaryOrderDetail\Models\SummaryOrderDetailModel;
use App\Modules\SummaryOrderDetail\Repositories\EloquentSummaryOrderDetails;
use App\Modules\SummaryOrderDetail\Repositories\SummaryOrderDetails;
use App\Modules\SummaryOrderDetail\Services\SummaryOrderDetailRecordService;
use App\Modules\SummaryOrderDetail\Services\SummaryOrderDetailRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class SummaryOrderDetailServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/SummaryOrderDetail/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(SummaryOrderDetailRecordService::class, SummaryOrderDetailRecordServiceImpl::class);
        $this->app->bind(SummaryOrderDetails::class, EloquentSummaryOrderDetails::class);
        $this->app->bind(SummaryOrderDetail::class, SummaryOrderDetailModel::class);
    }
}
