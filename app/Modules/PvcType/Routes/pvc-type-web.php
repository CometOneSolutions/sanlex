<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'pvc-types'], function () {
		Route::get('/', 'PvcTypeController@index')->name('pvc-type_index');
		Route::get('/create', 'PvcTypeController@create')->name('pvc-type_create');
		Route::get('{pvcTypeId}/edit', 'PvcTypeController@edit')->name('pvc-type_edit');
		Route::get('{pvcTypeId}/print', 'PvcTypeController@print')->name('pvc-type_print');
		Route::get('{pvcTypeId}', 'PvcTypeController@show')->name('pvc-type_show');
	});
});
