<?php

Breadcrumbs::register('pvc-type.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Pvc Types', route('pvc-type_index'));
});
Breadcrumbs::register('pvc-type.create', function ($breadcrumbs) {
	$breadcrumbs->parent('pvc-type.index');
	$breadcrumbs->push('Create', route('pvc-type_create'));
});
Breadcrumbs::register('pvc-type.show', function ($breadcrumbs, $pvc) {
	$breadcrumbs->parent('pvc-type.index');
	$breadcrumbs->push($pvc->getName(), route('pvc-type_show', $pvc->getId()));
});
Breadcrumbs::register('pvc-type.edit', function ($breadcrumbs, $pvc) {
	$breadcrumbs->parent('pvc-type.show', $pvc);
	$breadcrumbs->push('Edit', route('pvc-type_edit', $pvc->getId()));
});
