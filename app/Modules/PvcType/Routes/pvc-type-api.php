<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'pvc-types'], function () use ($api) {
		$api->get('/', 'App\Modules\PvcType\Controllers\PvcTypeApiController@index');
		$api->get('{pvcTypeId}', 'App\Modules\PvcType\Controllers\PvcTypeApiController@show');
		$api->post('/', 'App\Modules\PvcType\Controllers\PvcTypeApiController@store');
		$api->patch('{pvcTypeId}', 'App\Modules\PvcType\Controllers\PvcTypeApiController@update');
		$api->delete('{pvcTypeId}', 'App\Modules\PvcType\Controllers\PvcTypeApiController@destroy');
	});
});
