<?php

namespace App\Modules\PvcType\Provider;

use App\Modules\PvcType\Models\PvcType;
use App\Modules\PvcType\Models\PvcTypeModel;
use App\Modules\PvcType\Repositories\EloquentPvcTypeRepository;
use App\Modules\PvcType\Repositories\PvcTypeRepository;
use App\Modules\PvcType\Services\PvcTypeRecordService;
use App\Modules\PvcType\Services\PvcTypeRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class PvcTypeServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/PvcType/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(PvcTypeRecordService::class, PvcTypeRecordServiceImpl::class);
		$this->app->bind(PvcTypeRepository::class, EloquentPvcTypeRepository::class);
		$this->app->bind(PvcType::class, PvcTypeModel::class);
	}
}
