<?php

namespace App\Modules\PvcType\Requests;

use Dingo\Api\Http\FormRequest;

class UpdatePvcType extends PvcTypeRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] Pvc Type');
	}
}
