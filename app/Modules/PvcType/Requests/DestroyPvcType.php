<?php

namespace App\Modules\PvcType\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyPvcType extends PvcTypeRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Pvc Type');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
