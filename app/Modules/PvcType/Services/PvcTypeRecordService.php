<?php

namespace App\Modules\PvcType\Services;

use App\Modules\PvcType\Models\PvcType;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface PvcTypeRecordService extends RecordService
{
	public function create(
		$name,
		User $user = null
	);

	public function update(
		PvcType $pvcType,
		$name,
		User $user = null
	);

	public function delete(PvcType $pvcType);
}
