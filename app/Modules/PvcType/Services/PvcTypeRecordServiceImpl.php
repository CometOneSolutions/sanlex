<?php

namespace App\Modules\PvcType\Services;

use App\Modules\Product\Traits\CanUpdateProductName;
use App\Modules\PvcType\Models\PvcType;
use App\Modules\PvcType\Repositories\PvcTypeRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class PvcTypeRecordServiceImpl extends RecordServiceImpl implements PvcTypeRecordService
{
	use CanUpdateProductName;

	private $pvcType;
	private $pvcTypes;

	protected $availableFilters = [
		["id" => "name", "text" => "Name"]
	];

	public function __construct(PvcTypeRepository $pvcTypes, PvcType $pvcType)
	{
		$this->pvcTypes = $pvcTypes;
		$this->pvcType = $pvcType;
		parent::__construct($pvcTypes);
	}

	public function create(
		$name,
		User $user = null
	) {
		$pvcType = new $this->pvcType;
		$pvcType->setName($name);

		if ($user) {
			$pvcType->setUpdatedByUser($user);
		}
		$this->pvcTypes->save($pvcType);
		return $pvcType;
	}

	public function update(
		PvcType $pvcType,
		$name,
		User $user = null
	) {
		$tempPvcType = clone $pvcType;
		$tempPvcType->setName($name);
		if ($user) {
			$tempPvcType->setUpdatedByUser($user);
		}
		$this->pvcTypes->save($tempPvcType);

		$this->updateProductableProductNames([
			$tempPvcType->getPvcFittings(),
		]);

		return $tempPvcType;
	}

	public function delete(PvcType $pvcType)
	{
		$this->pvcTypes->delete($pvcType);
		return $pvcType;
	}
}
