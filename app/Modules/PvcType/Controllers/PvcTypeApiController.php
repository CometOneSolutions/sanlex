<?php

namespace App\Modules\PvcType\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\PvcType\Services\PvcTypeRecordService;
use App\Modules\PvcType\Transformer\PvcTypeTransformer;
use App\Modules\PvcType\Requests\StorePvcType;
use App\Modules\PvcType\Requests\UpdatePvcType;
use App\Modules\PvcType\Requests\DestroyPvcType;

class PvcTypeApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;

	public function __construct(
		PvcTypeRecordService $pvcTypeRecordService,
		PvcTypeTransformer $transformer
	) {
		$this->middleware('auth:api');
		parent::__construct($pvcTypeRecordService, $transformer);
		$this->service = $pvcTypeRecordService;
		$this->transformer = $transformer;
	}

	public function store(StorePvcType $request)
	{
		$pvcType = \DB::transaction(function () use ($request) {
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->create(
				$name,

				$user
			);
		});
		return $this->response->item($pvcType, $this->transformer)->setStatusCode(201);
	}

	public function update($pvcTypeId, UpdatePvcType $request)
	{
		$pvcType = \DB::transaction(function () use ($pvcTypeId, $request) {
			$pvcType   = $this->service->getById($pvcTypeId);
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->update(
				$pvcType,
				$name,

				$user
			);
		});
		return $this->response->item($pvcType, $this->transformer)->setStatusCode(200);
	}

	public function destroy($pvcTypeId, DestroyPvcType $request)
	{
		$pvcType = $this->service->getById($pvcTypeId);
		$this->service->delete($pvcType);
		return $this->response->item($pvcType, $this->transformer)->setStatusCode(200);
	}
}
