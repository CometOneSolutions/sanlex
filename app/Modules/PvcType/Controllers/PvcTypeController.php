<?php

namespace App\Modules\PvcType\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\PvcType\Services\PvcTypeRecordService;


class PvcTypeController extends Controller
{
	private $service;

	public function __construct(PvcTypeRecordService $pvcTypeRecordService)
	{
		$this->service = $pvcTypeRecordService;
		$this->middleware('permission:Read [MAS] Pvc Type')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Pvc Type')->only('create');
		$this->middleware('permission:Update [MAS] Pvc Type')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'name',
			'sortAscending' => true,
			'baseUrl' => '/api/pvc-types'
		]);
		return view('pvc-types.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('pvc-types.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$pvcType = $this->service->getById($id);
		return view('pvc-types.show', compact('pvcType'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$pvcType = $this->service->getById($id);
		return view('pvc-types.edit', compact('pvcType'));
	}
}
