<?php

namespace App\Modules\PvcType\Repositories;

use App\Modules\PvcType\Models\PvcType;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentPvcTypeRepository extends EloquentRepository implements PvcTypeRepository
{
	public function __construct(PvcType $pvcType)
	{
		parent::__construct($pvcType);
	}

	public function save(PvcType $pvcType)
	{
		return $pvcType->save();
	}

	public function delete(PvcType $pvcType)
	{
		return $pvcType->delete();
	}
}
