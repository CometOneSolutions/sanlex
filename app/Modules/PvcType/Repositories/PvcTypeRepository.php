<?php

namespace App\Modules\PvcType\Repositories;

use App\Modules\PvcType\Models\PvcType;
use CometOneSolutions\Common\Repositories\Repository;

interface PvcTypeRepository extends Repository
{
	public function save(PvcType $pvcType);
	public function delete(PvcType $pvcType);
}
