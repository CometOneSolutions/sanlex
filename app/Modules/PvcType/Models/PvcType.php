<?php

namespace App\Modules\PvcType\Models;



interface PvcType
{
	public function getId();

	public function getName();

	public function setName($value);
}
