<?php

namespace App\Modules\PvcType\Models;

use App\Modules\Brand\Models\BrandModel;
use App\Modules\PvcFitting\Models\PvcFittingModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Models\Users\User;

class PvcTypeModel extends Model implements PvcType, UpdatableByUser
{
	use HasUpdatedByUser;

	protected $table = 'pvc_types';

	public function pvcFittings()
	{
		return $this->hasMany(PvcFittingModel::class, 'pvc_type_id');
	}

	public function getPvcFittings()
	{
		return $this->pvcFittings;
	}

	public function pvcFittingBrands()
	{
		return $this->belongsToMany(BrandModel::class, 'pvc_fittings', 'pvc_type_id', 'brand_id')
			->withTimestamps();
	}

	public function getPvcFittingBrands()
	{
		return $this->pvcFittingBrands;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($value)
	{
		$this->name = $value;
		return $this;
	}
}
