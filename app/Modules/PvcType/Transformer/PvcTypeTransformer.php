<?php

namespace App\Modules\PvcType\Transformer;

use League\Fractal;
use App\Modules\PvcType\Models\PvcType;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class PvcTypeTransformer extends UpdatableByUserTransformer
{
	public function transform(PvcType $pvcType)
	{
		return [
			'id' => (int) $pvcType->getId(),
			'text' => $pvcType->getName(),
			'name' => $pvcType->getName(),
			'updatedAt' => $pvcType->updated_at,
			'editUri' => route('pvc-type_edit', $pvcType->getId()),
			'showUri' => route('pvc-type_show', $pvcType->getId()),
		];
	}
}
