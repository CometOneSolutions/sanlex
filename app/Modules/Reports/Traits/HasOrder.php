<?php

namespace App\Modules\Reports\Traits;

use App\Modules\SummaryOrderDetail\Repositories\SummaryOrderDetails;

trait HasOrder
{
    public static function getOrderByName($name, $reportName)
    {
        $summaryOrderDetails = app()->make(SummaryOrderDetails::class);
        $temp = clone $summaryOrderDetails->reportName($reportName);

        $hasSummaryOrderDetail = $temp->count() > 0;
        $lastOrder = $hasSummaryOrderDetail ? $temp->lastOrder()->getFirst()->getOrder() + 1 : 1;

        $sorter = $temp->name($name)->getFirst();

        if ($sorter) {
            return $sorter->getOrder();
        }

        if (!$sorter) {
            return $lastOrder;
        }
    }
    public static function isOrdered($name, $reportName)
    {
        $summaryOrderDetails = app()->make(SummaryOrderDetails::class);
        $temp = clone $summaryOrderDetails->reportName($reportName);
        $sorter = $temp->name($name)->getFirst();

        return $sorter ? true : false;
    }
}
