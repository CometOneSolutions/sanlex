<?php

namespace App\Modules\Reports\Traits;


use App\Modules\PurchaseOrderAccessory\Services\PurchaseOrderAccessoryRecordService;
use CometOneSolutions\Common\Utils\Filter;
use CometOneSolutions\Common\Utils\Sorter;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;

trait HasLastUpdatedAt
{
    //Only for Purchase Order Accessories. This should not be used with Coils. 
    public static function getByProductTypeName($value)
    {
        $purchaseOrderAccessoryRecordService = resolve(PurchaseOrderAccessoryRecordService::class);
        $itemMovementRecordService = resolve(ItemMovementRecordService::class);

        $filter = new Filter('product_type', $value);
        $updatedAtSorter = new Sorter('updated_at', 'DESC');

        $lastPurchaseOrderAccessory = $purchaseOrderAccessoryRecordService->getFirst([$filter], [$updatedAtSorter]);

        $lastItemMovements = $itemMovementRecordService->getLastItemMovement($value);
        $lastUpdatedPurchaseOrder = null;
        $lastUpdatedItemMovement = null;
        $lastUpdated = null;

        if ($lastPurchaseOrderAccessory) {
            $lastUpdatedPurchaseOrder = $lastPurchaseOrderAccessory->updated_at->setTimezone('Asia/Manila');
        }

        if ($lastItemMovements->isNotEmpty()) {
            $lastUpdatedItemMovement = $lastItemMovements->first()->updated_at->setTimezone('Asia/Manila');
        }

        if ($lastUpdatedPurchaseOrder !== null) {
            if ($lastUpdatedItemMovement !== null) {
                $lastUpdated = ($lastUpdatedPurchaseOrder->greaterThan($lastUpdatedItemMovement)) ? $lastUpdatedPurchaseOrder : $lastUpdatedItemMovement;
            } else {
                $lastUpdated = $lastUpdatedPurchaseOrder;
            }
        } else {
            if ($lastUpdatedItemMovement !== null) {
                $lastUpdated = $lastUpdatedItemMovement;
            }
        }
        return $lastUpdated;
    }
}
