<?php

namespace App\Modules\Reports\Traits;

use App\Modules\InsulationType\Repositories\InsulationTypeRepository;

trait HasInsulationType
{
    public static function getInsulationByInsulationTypeName($value)
    {
        $insulationTypeRepository = resolve(InsulationTypeRepository::class);

        return $insulationTypeRepository->where('name', $value)->getFirst();
    }
}
