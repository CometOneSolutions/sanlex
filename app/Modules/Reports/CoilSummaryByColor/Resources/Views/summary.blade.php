@extends('app')
@section('breadcrumbs', Breadcrumbs::render('report-color.summary'))
@section('content')
<div class="card table-responsive">

    <div class="card-body">
        <div class="card" class="col-md-6">
            <div class="card-header">
                Available Coil Colors (In Stock)
            </div>
            @if(count($data->unique('color')->values()->all()) > 0)
            <ul class="list-group list-group-flush">
                @foreach($data->unique('color')->values()->all() as $item)
                <li class="list-group-item"><a target="_blank" href="{{route('inventory_color', $item->color)}}"><span
                            style="font-size:1.3em; font-weight: bold;">{{strtoupper($item->color)}}</span></a></li>
                @endforeach
            </ul>
            @else
            <div class="text-center">No items to display.</div>
            @endif
        </div>
    </div>
</div>
@stop