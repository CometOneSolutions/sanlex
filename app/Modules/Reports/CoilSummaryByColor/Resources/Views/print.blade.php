
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inventory Summary</title>
    <style>
        html {
            display: table;
            margin: 0 10px;
        }
        body {
            margin: 0 auto;
            font-size: 12px;
            width: 794px;
            font-family: 'Open Sans', sans-serif;
            margin-left: 10px;
            margin-right: 10px;
            
        }
        .green {
            color: darkgreen;
        }
        .pending {
            color: gray
        }
        .title {
            text-align: center;
            text-decoration: underline;
            text-transform: uppercase;
            font-size: 1.2em;
        }
        table, tr, th, td{
            border: 1px solid black;
            border-collapse: collapse;
            
        }
        table tr td:nth-child(2), table tr td:nth-child(3), table tr td:nth-child(4), table tr td:nth-child(5) {
            text-align: center;
        }
        table .bordered tr, table .bordered th, table .bordered td{
            
            border: 1px solid black;
            
            
        }
        p {
            margin: 7px auto 7px auto;
        }
        .page-header, .page-header-space {
        height: 0;
        }

        .page-footer, .page-footer-space {
        height: 50px;

        }
        .border-on-tr > tr {
            border: 1px solid black;
        }

        .border-on-th > th {
            border: 1px solid black;
        }
        .border-on-td > td {
            border: 1px solid black;
        }

        .page-footer {
            position: fixed;
            bottom: 0;
            width: 100%;
            border: 0;
        }

        .page-header {
        position: fixed;
        top: 0mm;
        width: 100%;
        }

        .page {
        page-break-after: always;
        }
        table, tr, th, td {
            border: 0;
            
        }
        table {
            width: 100%;
        }

        @page {
    
        margin-top: 30px;
        margin-bottom: 80px;
    
        }
        @media screen {
            html {
                margin: 0 auto;
            }
            body {
                font-size:16px;
                width: 100%;
            }
        }
        tr:nth-child(even) {background: #CCC}
        tr:nth-child(odd) {background: #FFF}
        @media print {
       
        
        button {display: none;}
        
        body {
            margin: 0;
            }
        }
        tr:nth-child(even) {background: #CCC}
        tr:nth-child(odd) {background: #FFF}
        
    </style>
</head>
<body>
    <table>
            <tr>
                <td>
                    <div class="header-space print-margin-top">
                        <center>
                            <h1>{{$color}} OPTION 1</h1>
                        </center>
                    </div>
                </td>
            </tr>
        <tbody>
            <tr>
                <td>
                    
                    <table class="bordered">
                        <thead>
                            <th colspan="2">&nbsp;</th>
                        
                            <th align="center">
                                <sup>0.27MM</sup>x<sub>914MM</sub>
                            </th>
                            <th align="center">
                                <sup>0.35MM</sup>x<sub>914MM</sub>
                            </th>
                            <th align="center">
                                <sup>0.47MM</sup>x<sub>914MM</sub>
                            </th>
                            <th align="center">
                                <sup>0.57MM</sup>x<sub>914MM</sub>
                            </th>
                            <th align="center">
                                <sup>0.27MM</sup>x<sub>1220MM</sub>
                            </th>
                            <th align="center">
                                <sup>0.35MM</sup>x<sub>1220MM</sub>
                            </th>
                            <th align="center">
                                <sup>0.47MM</sup>x<sub>1220MM</sub>
                            </th>
                            <th align="center">
                                <sup>0.57MM</sup>x<sub>1220MM</sub>
                            </th>
                            <th align="center">
                                <sup>0.27MM</sup>x<sub>1440MM</sub>
                            </th>
                            <th align="center">
                                <sup>0.35MM</sup>x<sub>1440MM</sub>
                            </th>
                            <th align="center">
                                <sup>0.47MM</sup>&frasl;<sub>1440MM</sub>
                            </th>
                            <th align="center">
                                <sup>0.57MM</sup>&frasl;<sub>1440MM</sub>
                            </th>
                        
                        </thead>
                        <tbody>
                            <tr>
                                <td align="center" colspan="2"><b>T O T A L</b></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                <td align="center"><sup class="table-success"><b>12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</b></sub></td>
                                
                            </tr>
                            <tr>
                                <td align="center">CMAX</td>
                                <td align="center">Z60</td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                            </tr>
                            <tr>
                                <td align="center">CMAX</td>
                                <td align="center">Z80</td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                            </tr>
                            <tr>
                                <td align="center">CMAX</td>
                                <td align="center">AZ60</td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                            </tr>
                            <tr>
                                <td align="center">CMAX</td>
                                <td align="center">AZ70</td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                            </tr>
                            <tr>
                                <td align="center">CMAX</td>
                                <td align="center">AZ60</td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                            </tr>
                            <tr>
                                <td align="center">ZPXY</td>
                                <td align="center">Z80</td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                                <td align="center"><sup class="table-success">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="table-secondary">0.000</sub></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <br>
                    <div class="header-space print-margin-top">
                        <center>
                            <h1>{{$color}} OPTION 2</h1>
                        </center>
                    </div>
                    <table class="bordered">
                        <thead>
                            <th>&nbsp;</th>
                            <th align="center">
                                <sup><b>CMAX</b><br> Z60</sub>
                            </th>
                            <th align="center">
                                <sup><b>CMAX</b><br> Z80</sub>
                            </th>
                            <th align="center">
                                <sup><b>CMAX</b><br> AZ80</sub>
                            </th>
                            <th align="center">
                                <sup><b>CMAX</b><br> AZ70</sub>
                            </th>
                            <th align="center">
                                <sup><b>CMAX</b><br> AZ60</sub>
                            </th>
                            <th align="center">
                                <sup><b>ZPXY</b><br> Z80</sub>
                            </th>
                            <th align="center">
                                <sup><b>ZPXY</b><br> AZ80</sub>
                            </th>
                            <th align="center">
                                <sup><b>XIAOJIN</b><br> AZ60</sub>
                            </th>
                            <th align="center">
                                <sup><b>HUAHAI</b><br> Z60</sub>
                            </th>
                            <th align="center">
                                <sup><b>DAVID</b><br> Z40</sub>
                            </th>
                            <th align="center">
                                <sup>TOTAL</sub>
                            </th>
                            
                        </thead>
                        <tbody>
    
                            <tr>
                                <td align="center"><b>0.27 x 914</b></td>
                                <td align="center"><sup class="green">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                            </tr>
                            <tr>
                                <td align="center"><b>0.35 x 914</b></td>
                                <td align="center"><sup class="green">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                            </tr>
                            <tr>
                                <td align="center"><b>0.47 x 914</b></td>
                                <td align="center"><sup class="green">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                            </tr>
                            <tr>
                                <td align="center"><b>0.57 x 914</b></td>
                                <td align="center"><sup class="green">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">-</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                                <td align="center"><sup class="green">12.400</sup>&nbsp;&frasl;&nbsp;<sub class="gray">-</sub></td>
                            </tr>
                        </tbody>
                    </table>
                  
                </td>
            </tr>
        </tbody>
    </table>
    
</body>
</html>



