<?php

namespace App\Modules\Reports\Accessory\PaintAdhesiveReport\Controller;

use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Reports\Traits\HasLastUpdatedAt;
use App\Modules\Reports\Accessory\PaintAdhesiveReport\Model\PaintAdhesiveReport;
use CometOneSolutions\Common\Controllers\Controller;

class PaintAdhesiveReportController extends Controller
{
    protected $productRepository;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
        $this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
    }

    public function index()
    {
        $products = $this->productRepository->paintAdhesive()->inStockOrHasPending()->getAll();

        if ($products->isEmpty()) {
            return redirect()->back()->with('status', 'No paint adhesive products available');
        }

        $lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::PAINT);
        $report = new PaintAdhesiveReport($this->productRepository);

        return view('paint-adhesive-report.index', compact('report',  'lastUpdatedAt'));
    }

    public function responsive()
    {
        $products = $this->productRepository->paintAdhesive()->inStockOrHasPending()->getAll();

        if ($products->isEmpty()) {
            return redirect()->back()->with('status', 'No paint adhesive products available');
        }

        $lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::PAINT);
        $report = new PaintAdhesiveReport($this->productRepository);
        // dd($report);
        return view('paint-adhesive-report.responsive', compact('report',  'lastUpdatedAt'));
    }
}
