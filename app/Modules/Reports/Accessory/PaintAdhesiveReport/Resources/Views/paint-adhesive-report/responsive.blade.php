@extends('reports.template-responsive')
@section('title', 'Paint Adhesive')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
<table>
    <thead>
        <tr>
            <th class="fixed no-shade width-md" rowspan="2">TYPE</th>
            <th class="fixed no-shade width-md" rowspan="2">BRAND</th>
            <th class="fixed no-shade width-md" rowspan="2">COLOR</th>
            <th class="fixed no-shade" colspan="{{$report->getSizes()->count()}}">UNIT</th>
        </tr>
        <tr>
            @foreach($report->getSizes() as $size)
            <th class="width-md">{{$size->getName()}}</th>
            @endforeach
        </tr>
    </thead>

    <tbody>
        @foreach($report->getPaintAdhesiveTypes() as $paintAdhesiveType)
        <tr>
            <td class="fixed no-shade width-md" align="center" rowspan="{{ $paintAdhesiveType->getPaintAdhesiveBrandColorsCount() }}"> {{ $paintAdhesiveType->getName() }}</td>
            @foreach($paintAdhesiveType->getPaintAdhesiveBrands() as $paintAdhesiveBrand)
            <td class="fixed no-shade width-md" align="center" rowspan="{{ $paintAdhesiveBrand->getPaintAdhesiveColorsCount() }}"> {{ $paintAdhesiveBrand->getName() }}</td>
            @foreach($paintAdhesiveBrand->getPaintAdhesiveColors() as $paintAdhesiveColor)
            <td class=" fixed width-md" align="center"> {{ $paintAdhesiveColor->getName() }}</td>
            @foreach($report->getSizes() as $size)
            @php
            $products = $report->getProducts($size, $paintAdhesiveColor, $paintAdhesiveBrand, $paintAdhesiveType)
            @endphp
            <td align="center" class="width-md">
                @if($products->count() > 0)
                {{ $report->getNumberFormattedStockQuantity($products) }}
                @if($report->getNumberFormattedPendingQuantity($products) != '-')
                /
                <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                @endif
                @else
                -
                @endif
            </td>
            @endforeach
        </tr>
        @endforeach
        @endforeach
        @endforeach
    </tbody>
</table>
@endsection
