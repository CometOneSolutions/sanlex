@extends('reports.template')
@section('title', 'Paint Adhesive')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
<table class="bordered">
    <thead>
        <tr>
            <th rowspan="2" style="width:100px" rowspan="2">TYPE</th>
            <th style="width:80px" rowspan="2">BRAND</th>
            <th style="width:80px" rowspan="2">COLOR</th>
            <th style="width:80px" colspan="{{$report->getSizes()->count()}}">UNIT</th>
        </tr>
        <tr>
            @foreach($report->getSizes() as $size)
            <th style="width:80px">{{$size->getName()}}</th>
            @endforeach
        </tr>
    </thead>

    <tbody>
        @foreach($report->getPaintAdhesiveTypes() as $paintAdhesiveType)
        <tr>
            <td class="no-shade" align="center" rowspan="{{ $paintAdhesiveType->getPaintAdhesiveBrandColorsCount() }}"> {{ $paintAdhesiveType->getName() }}</td>
            @foreach($paintAdhesiveType->getPaintAdhesiveBrands() as $paintAdhesiveBrand)
            <td class="no-shade" align="center" rowspan="{{ $paintAdhesiveBrand->getPaintAdhesiveColorsCount() }}"> {{ $paintAdhesiveBrand->getName() }}</td>
            @foreach($paintAdhesiveBrand->getPaintAdhesiveColors() as $paintAdhesiveColor)
            <td class="no-shade" align="center"> {{ $paintAdhesiveColor->getName() }}</td>
            @foreach($report->getSizes() as $size)
            @php
            $products = $report->getProducts($size, $paintAdhesiveColor, $paintAdhesiveBrand, $paintAdhesiveType)
            @endphp
            <td align="center">
                @if($products->count() > 0)
                {{ $report->getNumberFormattedStockQuantity($products) }}
                @if($report->getNumberFormattedPendingQuantity($products) != '-')
                /
                <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                @endif
                @else
                -
                @endif
            </td>
            @endforeach
        </tr>
        @endforeach
        @endforeach
        @endforeach
    </tbody>
</table>
@endsection
