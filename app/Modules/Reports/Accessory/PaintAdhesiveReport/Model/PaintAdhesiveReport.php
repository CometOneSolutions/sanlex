<?php

namespace App\Modules\Reports\Accessory\PaintAdhesiveReport\Model;

use App\Modules\Size\Models\Size;
use Illuminate\Support\Collection;
use App\Modules\Color\Models\Color;
use App\Modules\Reports\Traits\HasOrder;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\Model\AccessoryReport;

class PaintAdhesiveReport extends AccessoryReport
{
    protected $productRepository;
    protected $products;
    protected $paintAdhesiveTypes;
    protected $sizes;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository->paintAdhesive()->inStockOrHasPending();
        $this->products = $this->productRepository->getAll();
        $this->paintAdhesiveTypes = $this->initPaintAdhesiveTypes($this->products);
        $this->sizes = $this->initSizes($this->products);
    }

    protected function initPaintAdhesiveTypes(Collection $products)
    {
        return  $products->map(function ($product) {
            $paintAdhesive = $product->productable;
            return new PaintAdhesiveTypeReport($paintAdhesive->getPaintAdhesiveType(), $this->productRepository);
        })->unique(function ($paintAdhesiveType) {
            return $paintAdhesiveType->getName();
        });
    }

    protected function initSizes(Collection $products)
    {
        return  $products->map(function ($product) {
            $paintAdhesive = $product->productable;
            return $paintAdhesive->getSize();
        })->unique(function ($size) {
            return $size->getName();
        });
    }

    public function getPaintAdhesiveTypes()
    {
        return $this->paintAdhesiveTypes->sortBy(function ($paintAdhesiveType) {
            return HasOrder::getOrderByName($paintAdhesiveType->getName(), 'PAINT ADHESIVE');
        });
    }

    public function getSizes()
    {
        return $this->sizes->sortBy(function ($size) {
            return $size->getName();
        });
    }

    public function getProducts(Size $size, Color $color, PaintAdhesiveBrandReport $brand, PaintAdhesiveTypeReport $paintAdhesiveType)
    {
        $temp = clone $this->productRepository;
        return $temp
            ->paintAdhesiveSize($size)
            ->paintAdhesiveColor($color)
            ->paintAdhesiveBrand($brand->getBrand())
            ->paintAdhesiveType($paintAdhesiveType->getPaintAdhesiveType())
            ->getAll();
    }
}
