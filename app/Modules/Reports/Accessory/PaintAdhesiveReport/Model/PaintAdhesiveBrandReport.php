<?php

namespace App\Modules\Reports\Accessory\PaintAdhesiveReport\Model;

use App\Modules\Brand\Models\Brand;
use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use App\Modules\Product\Repositories\EloquentProductRepository;
use Illuminate\Database\Eloquent\Collection;

class PaintAdhesiveBrandReport
{
    protected $name;
    protected $brand;
    protected $paintAdhesiveColors;
    protected $paintAdhesiveType;
    protected $productRepository;
    protected $products;

    public function __construct(
        Brand $brand,
        PaintAdhesiveType $paintAdhesiveType,
        EloquentProductRepository $productRepository
    ) {
        $this->name = $brand->getName();
        $this->brand = $brand;
        $this->paintAdhesiveType = $paintAdhesiveType;
        $this->productRepository = $productRepository;

        $this->products = $this->initProducts();
        $this->paintAdhesiveColors = $this->initPaintAdhesiveColors($this->products);
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;
        return $temp
            ->paintAdhesiveType($this->getPaintAdhesiveType())
            ->paintAdhesiveBrand($this->getBrand())
            ->getAll();
    }

    protected function initPaintAdhesiveColors(Collection $products)
    {
        return $products->map(function ($product) {
            $paintAdhesive = $product->productable;
            return $paintAdhesive->getColor();
        })->unique(function ($color) {
            return $color->getName();
        });
    }

    public function getPaintAdhesiveColors()
    {
        return $this->paintAdhesiveColors->sortBy(function ($color) {
            return $color->getName();
        });
    }

    public function getPaintAdhesiveColorsCount()
    {
        return $this->paintAdhesiveColors->count();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function getPaintAdhesiveType()
    {
        return $this->paintAdhesiveType;
    }
}
