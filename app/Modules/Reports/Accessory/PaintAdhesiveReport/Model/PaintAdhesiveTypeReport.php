<?php

namespace App\Modules\Reports\Accessory\PaintAdhesiveReport\Model;


use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use App\Modules\Product\Repositories\EloquentProductRepository;

class PaintAdhesiveTypeReport
{
    protected $name;
    protected $paintAdhesiveType;
    protected $paintAdhesiveBrands;
    protected $productRepository;

    public function __construct(PaintAdhesiveType $paintAdhesiveType, EloquentProductRepository $productRepository)
    {
        $this->paintAdhesiveType = $paintAdhesiveType;
        $this->name = $this->paintAdhesiveType->getName();
        $this->productRepository = $productRepository;
        $this->products = $this->initProducts();
        $this->paintAdhesiveBrands = $this->initPaintAdhesiveBrands($this->paintAdhesiveType);
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;
        return $temp
            ->paintAdhesiveType($this->getPaintAdhesiveType())
            ->getAll();
    }

    public function initPaintAdhesiveBrands(PaintAdhesiveType $paintAdhesiveType)
    {
        return $this->products->map(function ($product) use ($paintAdhesiveType) {
            $paintAdhesive = $product->productable;
            return new PaintAdhesiveBrandReport($paintAdhesive->getBrand(), $paintAdhesiveType, $this->productRepository);
        })->unique(function ($paintAdhesiveBrand) {
            return $paintAdhesiveBrand->getName();
        });
    }

    public function getPaintAdhesiveBrands()
    {
        return $this->paintAdhesiveBrands->sortBy(function ($paintAdhesiveBrand) {
            return $paintAdhesiveBrand->getName();
        });
    }

    public function getPaintAdhesiveBrandsCount()
    {
        return $this->paintAdhesiveBrands->count();
    }

    public function getPaintAdhesiveType()
    {
        return $this->paintAdhesiveType;
    }

    public function getPaintAdhesiveBrandColorsCount()
    {
        return $this->paintAdhesiveBrands->reduce(function ($carry, $item) {
            return $carry + $item->getPaintAdhesiveColorsCount();
        }, 0);
    }

    public function getName()
    {
        return  $this->name;
    }
}
