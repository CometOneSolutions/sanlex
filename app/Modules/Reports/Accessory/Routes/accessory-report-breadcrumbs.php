<?php

//Reports
Breadcrumbs::register('report-accessory.summary', function ($breadcrumbs) {
    $breadcrumbs->parent('report-menu-dashboard.index');
    $breadcrumbs->push('Accessory', route('reports_accessory-index'));
});
