<?php

Route::group(['prefix' => 'reports'], function () {
    // Route::get('accessory', 'Controller\AccessoryReportController@index')->name('reports_accessory-index');
    // Route::get('accessory/{keyword}', 'AccessoryReportController@print')->name('reports_accessory-print');
    Route::get('skylight', 'SkylightReport\Controller\SkylightReportController@responsive')->name('reports_skylight-index');
    Route::get('pe-foam', 'PEFoamReport\Controller\PEFoamReportController@responsive')->name('reports_pe-form-index');
    Route::get('fiberglass', 'FiberglassReport\Controller\FiberglassReportController@responsive')->name('reports_fiberglass-index');
    Route::get('bubble-core', 'BubbleCoreReport\Controller\BubbleCoreReportController@responsive')->name('reports_bubble-core-index');
    Route::get('misc-accessory', 'MiscAccessoryReport\Controller\MiscAccessoryReportController@responsive')->name('reports_misc-accessory-index');
    Route::get('stainless-steel', 'StainlessSteelReport\Controller\StainlessSteelReportController@responsive')->name('reports_stainless-steel-index');
    Route::get('structural-steel', 'StructuralSteelReport\Controller\StructuralSteelReportController@responsive')->name('reports_structural-steel-index');
    Route::get('paint-adhesive', 'PaintAdhesiveReport\Controller\PaintAdhesiveReportController@responsive')->name('reports_paint-adhesive-index');
    Route::get('fastener', 'FastenerReport\Controller\FastenerReportController@responsive')->name('reports_fastener-index');
    Route::get('pvc-fitting', 'PvcFittingReport\Controller\PvcFittingReportController@responsive')->name('reports_pvc-fitting-index');

    // Route::get('skylight/responsive', 'SkylightReportController@responsive');
    // Route::get('pe-foam/responsive', 'PEFoamReportController@responsive');
    // Route::get('fiberglass/responsive', 'FiberglassReportController@responsive');
    // Route::get('bubble-core/responsive', 'BubbleCoreReportController@responsive');
    // Route::get('misc-accessory/responsive', 'MiscAccessoryReportController@responsive');
    // Route::get('stainless-steel/responsive', 'StainlessSteelReportController@responsive');
    // Route::get('structural-steel/responsive', 'StructuralSteelReportController@responsive');
    // Route::get('paint-adhesive/responsive', 'PaintAdhesiveReportController@responsive');
    // Route::get('fastener/responsive', 'FastenerReportController@responsive');
    // Route::get('pvc-fitting/responsive', 'PvcFittingReportController@responsive');
});
