<?php

namespace App\Modules\Reports\Accessory\StructuralSteelReport\Model;

use App\Modules\Finish\Models\Finish;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use Illuminate\Support\Collection;

class StructuralSteelTypeAndFinishReport
{
    protected $name;
    protected $structuralSteelType;
    protected $productRepository;
    protected $products;
    protected $specifications;

    protected $finish;

    public function __construct(StructuralSteelType $structuralSteelType, Finish $finish, ProductRepository $productRepository)
    {
        $this->structuralSteelType = $structuralSteelType;
        $this->finish = $finish;
        $this->productRepository = $productRepository;
        $this->products = $this->initProducts();
        $this->specifications = $this->initSpecifications();
    }

    protected function initSpecifications()
    {
        return $this->products->map(function ($product) {
            $structuralSteel = $product->productable;
            return new StructuralSteelSpecificationReport($structuralSteel->getDimensionX(), $structuralSteel->getDimensionY(), $structuralSteel->getLength());
        })->unique(function ($specification) {
            return $specification->getName();
        });
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;
        return $temp
            ->structuralSteelType($this->getStructuralSteelType())
            ->structuralSteelFinish($this->getFinish())
            ->getAll();
    }

    public function getSpecifications()
    {
        return $this->specifications->sortBy(function ($specification) {
            return  $specification->getName();
        });
    }

    public function getSpecificationsCount()
    {
        return $this->specifications->count();
    }

    public function getName()
    {
        return sprintf('%s %s', $this->structuralSteelType->getName(), $this->finish->getName());
    }

    public function getStructuralSteelType()
    {
        return $this->structuralSteelType;
    }

    public function getFinish()
    {
        return $this->finish;
    }
}
