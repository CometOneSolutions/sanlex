<?php

namespace App\Modules\Reports\Accessory\StructuralSteelReport\Model;

use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\Model\AccessoryReport;
use App\Modules\Reports\Traits\HasOrder;
use App\Modules\Thickness\Models\Thickness;
use Illuminate\Support\Collection;

class StructuralSteelReport extends AccessoryReport
{
    protected $productRepository;
    protected $products;
    protected $thicknesses;
    protected $structuralSteelTypeAndFinishes;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository->structuralSteel()->inStockOrHasPending();
        $this->products = $this->productRepository->getAll();
        $this->thicknesses = $this->initThicknesses($this->products);
        $this->structuralSteelTypeAndFinishes = $this->initStructuralSteelTypeAndFinishes($this->products);
    }

    protected function initThicknesses(Collection $products)
    {
        return  $products->map(function ($product) {
            $structuralSteel = $product->productable;
            return $structuralSteel->getThickness();
        })->unique(function ($thickness) {
            return $thickness->getName();
        });
    }

    protected function initStructuralSteelTypeAndFinishes(Collection $products)
    {
        return  $products->map(function ($product) {
            $structuralSteel = $product->productable;
            return new StructuralSteelTypeAndFinishReport($structuralSteel->getStructuralSteelType(), $structuralSteel->getFinish(), $this->productRepository);
        })->unique(function ($structuralSteelTypeAndFinish) {
            return $structuralSteelTypeAndFinish->getName();
        });
    }

    public function getThicknesses()
    {
        return $this->thicknesses->sortBy(function ($thickness) {
            return $thickness->getName();
        });
    }

    public function getStructuralSteelTypeAndFinishes()
    {
        return $this->structuralSteelTypeAndFinishes->sortBy(function ($structuralSteelTypeAndFinish) {
            return HasOrder::getOrderByName($structuralSteelTypeAndFinish->getName(), 'STRUCTURAL STEEL');
        });
    }

    public function getProducts(StructuralSteelTypeAndFinishReport $structuralSteelTypeAndFinish, StructuralSteelSpecificationReport $structuralSteelSpecification, Thickness $thickness)
    {
        $structuralSteelType = $structuralSteelTypeAndFinish->getStructuralSteelType();
        $finish = $structuralSteelTypeAndFinish->getFinish();
        $dimensionX = $structuralSteelSpecification->getDimensionX();
        $dimensionY = $structuralSteelSpecification->getDimensionY();
        $length = $structuralSteelSpecification->getLength();

        $temp = clone $this->productRepository;
        return $temp
            ->structuralSteelType($structuralSteelType)
            ->structuralSteelFinish($finish)
            ->structuralSteelDimensionX($dimensionX)
            ->structuralSteelDimensionY($dimensionY)
            ->structuralSteelLength($length)
            ->structuralSteelThickness($thickness)
            ->getAll();
    }
}
