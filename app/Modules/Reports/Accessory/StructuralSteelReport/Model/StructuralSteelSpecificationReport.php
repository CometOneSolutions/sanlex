<?php

namespace App\Modules\Reports\Accessory\StructuralSteelReport\Model;

use App\Modules\DimensionX\Models\DimensionX;
use App\Modules\DimensionY\Models\DimensionY;
use App\Modules\Length\Models\Length;

class StructuralSteelSpecificationReport
{
    protected $name;

    public function __construct(DimensionX $dimensionX, DimensionY $dimensionY, Length $length)
    {
        $this->dimensionX = $dimensionX;
        $this->dimensionY = $dimensionY;
        $this->length = $length;
        $this->name = $this->initName($dimensionX, $dimensionY, $length);
    }

    protected function initName(DimensionX $dimensionX, DimensionY $dimensionY, Length $length)
    {
        return sprintf('%s x %s x %s', $dimensionX->getName(), $dimensionY->getName(), $length->getName());
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDimensionX()
    {
        return $this->dimensionX;
    }
    public function getDimensionY()
    {
        return $this->dimensionY;
    }
    public function getLength()
    {
        return $this->length;
    }
}
