@extends('reports.template-responsive')
@section('title', 'Structural Steel')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
<table>
    <thead>
        <tr>
            <th class="fixed no-shade width-lg" rowspan="2">TYPE</th>
            <th class="fixed no-shade width-lg" rowspan="2">SPECIFICATIONS</th>
            <th colspan="{{$report->getThicknesses()->count()}}">Thickness</th>
        </tr>
        <tr>
            @foreach($report->getThicknesses() as $thickness)
                <th class="width-sm">{{$thickness->getName()}}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        <tr>
            @foreach($report->getStructuralSteelTypeAndFinishes() as $structuralSteelTypeAndFinish)
            <td rowspan="{{ $structuralSteelTypeAndFinish->getSpecificationsCount()}}"      align="center"  class="fixed no-shade width-lg">{{ $structuralSteelTypeAndFinish->getName() }}</td>
            @foreach($structuralSteelTypeAndFinish->getSpecifications() as $specification)
                    <td class="fixed width-lg" align="center">{{ $specification->getName() }} </td>
                    @foreach($report->getThicknesses() as $thickness)
                    @php
                    $products = $report->getProducts($structuralSteelTypeAndFinish, $specification, $thickness)
                    @endphp
                    <td align="center"  class="width-sm">
                        @if($products->count() > 0)
                        {{ $report->getNumberFormattedStockQuantity($products) }}
                        @if($report->getNumberFormattedPendingQuantity($products) != '-')
                        /
                        <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                        @endif
                        @else
                        -
                        @endif
                    </td>
                    @endforeach
                </tr>
            @endforeach
            @endforeach
    </tbody>
</table>
@endsection