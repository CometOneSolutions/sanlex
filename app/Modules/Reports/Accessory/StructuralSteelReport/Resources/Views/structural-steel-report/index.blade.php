@extends('reports.template')
@section('title', 'Structural Steel')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
<table class="bordered">
    <thead>
        <tr>
            <th rowspan="2" style="width:100px" rowspan="2">TYPE</th>
            <th style="width:80px" rowspan="2">SPECIFICATIONS</th>
            <th style="width:80px" colspan="{{$report->getThicknesses()->count()}}">Thickness</th>
        </tr>
        <tr>
            @foreach($report->getThicknesses() as $thickness)
                <th style="width:80px">{{$thickness->getName()}}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        <tr>
            @foreach($report->getStructuralSteelTypeAndFinishes() as $structuralSteelTypeAndFinish)
            <td rowspan="{{ $structuralSteelTypeAndFinish->getSpecificationsCount()}}"      align="center"  class="no-shade">{{ $structuralSteelTypeAndFinish->getName() }}</td>
            @foreach($structuralSteelTypeAndFinish->getSpecifications() as $specification)
                    <td align="center">{{ $specification->getName() }} </td>
                    @foreach($report->getThicknesses() as $thickness)
                    @php
                    $products = $report->getProducts($structuralSteelTypeAndFinish, $specification, $thickness)
                    @endphp
                    <td align="center"  >
                        @if($products->count() > 0)
                        {{ $report->getNumberFormattedStockQuantity($products) }}
                        @if($report->getNumberFormattedPendingQuantity($products) != '-')
                        /
                        <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                        @endif
                        @else
                        -
                        @endif
                    </td>
                    @endforeach
                </tr>
            @endforeach
            @endforeach
    </tbody>
</table>
@endsection