<?php

namespace App\Modules\Reports\Accessory\StructuralSteelReport\Controller;

use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Reports\Traits\HasLastUpdatedAt;
use App\Modules\Reports\Accessory\StructuralSteelReport\Model\StructuralSteelReport;
use CometOneSolutions\Common\Controllers\Controller;

class StructuralSteelReportController extends Controller
{
    protected $productRepository;


    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
        $this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
    }

    public function index()
    {
        $products = $this->productRepository->structuralSteel()->inStockOrHasPending()->getAll();

        if ($products->isEmpty()) {
            return redirect()->back()->with('status', 'No structural steel products available');
        }

        $report = new StructuralSteelReport($this->productRepository);
        $lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::ST);

        return view('structural-steel-report.index', compact('report', 'lastUpdatedAt'));
    }

    public function responsive()
    {
        $products = $this->productRepository->structuralSteel()->inStockOrHasPending()->getAll();

        if ($products->isEmpty()) {
            return redirect()->back()->with('status', 'No structural steel products available');
        }

        $report = new StructuralSteelReport($this->productRepository);
        $lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::ST);

        return view('structural-steel-report.responsive', compact('report', 'lastUpdatedAt'));
    }
}
