@extends('reports.template')
@section('title', 'Stainless Steel')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
    <table class="bordered">
        <thead>
            <tr>
                <th rowspan="2" style="width:100px">TYPE</th>
                <th style="width:80px">T/W</th>
                @foreach($report->getWidths() as $width)
                <th style="width:80px">{{ $width->getName() }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            <tr>
                @foreach($report->getStainlessSteelTypeAndFinishAndLengths() as $stainlessSteelTypeAndFinishAndLength)
                <td rowspan="{{ $stainlessSteelTypeAndFinishAndLength->getThicknessesCount()}}"      align="center"  class="no-shade">{{ $stainlessSteelTypeAndFinishAndLength->getName() }}</td>
                @foreach($stainlessSteelTypeAndFinishAndLength->getThicknesses() as $thickness)
                        <td align="center" class="no-shade">{{ $thickness->getName() }} </td>
                        @foreach($report->getWidths() as $width)
                        @php
                        $products = $report->getProducts($stainlessSteelTypeAndFinishAndLength, $thickness, $width)
                        @endphp
                        <td align="center"  >
                            @if($products->count() > 0)
                            {{ $report->getNumberFormattedStockQuantity($products) }}
                            @if($report->getNumberFormattedPendingQuantity($products) != '-')
                            /
                            <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                            @endif
                            @else
                            -
                            @endif
                        </td>
                        @endforeach
                    </tr>
                @endforeach
                @endforeach
        </tbody>
    </table>
@endsection