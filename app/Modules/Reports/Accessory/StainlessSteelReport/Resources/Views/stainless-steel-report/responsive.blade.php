@extends('reports.template-responsive')
@section('title', 'Stainless Steel')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
    <table>
        <thead>
            <tr>
                <th class="fixed no-shade width-md" rowspan="2">TYPE</th>
                <th class="fixed no-shade width-md">T/W</th>
                @foreach($report->getWidths() as $width)
                <th class="width-lg">{{ $width->getName() }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            <tr>
                @foreach($report->getStainlessSteelTypeAndFinishAndLengths() as $stainlessSteelTypeAndFinishAndLength)
                <td rowspan="{{ $stainlessSteelTypeAndFinishAndLength->getThicknessesCount()}}"      align="center"  class="fixed no-shade width-md">{{ $stainlessSteelTypeAndFinishAndLength->getName() }}</td>
                @foreach($stainlessSteelTypeAndFinishAndLength->getThicknesses() as $thickness)
                        <td align="center" class="fixed  width-md">{{ $thickness->getName() }} </td>
                        @foreach($report->getWidths() as $width)
                        @php
                        $products = $report->getProducts($stainlessSteelTypeAndFinishAndLength, $thickness, $width)
                        @endphp
                        <td align="center" class="width-lg" >
                            @if($products->count() > 0)
                            {{ $report->getNumberFormattedStockQuantity($products) }}
                            @if($report->getNumberFormattedPendingQuantity($products) != '-')
                            /
                            <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                            @endif
                            @else
                            -
                            @endif
                        </td>
                        @endforeach
                    </tr>
                @endforeach
                @endforeach
        </tbody>
    </table>
@endsection