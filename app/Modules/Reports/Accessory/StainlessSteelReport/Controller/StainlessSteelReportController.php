<?php

namespace App\Modules\Reports\Accessory\StainlessSteelReport\Controller;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Reports\Traits\HasLastUpdatedAt;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\StainlessSteelReport\Model\StainlessSteelReport;
use CometOneSolutions\Common\Controllers\Controller;

class StainlessSteelReportController extends Controller
{
	protected $productRepository;

	public function __construct(
		ProductRepository $productRepository
	) {
		$this->productRepository = $productRepository;
		$this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
	}

	public function index()
	{
		$products = $this->productRepository->stainlessSteel()->inStockOrHasPending()->getAll();

		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No stainless steel products available');
		}

		$report = new StainlessSteelReport($this->productRepository);
		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::SS);

		return view('stainless-steel-report.index', compact('report', 'lastUpdatedAt'));
	}

	public function responsive()
	{
		$products = $this->productRepository->stainlessSteel()->inStockOrHasPending()->getAll();

		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No stainless steel products available');
		}

		$report = new StainlessSteelReport($this->productRepository);
		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::SS);

		return view('stainless-steel-report.responsive', compact('report', 'lastUpdatedAt'));
	}
}
