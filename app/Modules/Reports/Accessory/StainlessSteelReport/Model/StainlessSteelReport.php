<?php

namespace App\Modules\Reports\Accessory\StainlessSteelReport\Model;

use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\Model\AccessoryReport;
use App\Modules\Reports\Traits\HasOrder;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;
use Illuminate\Support\Collection;

class StainlessSteelReport extends AccessoryReport
{
    protected $productRepository;
    protected $products;
    protected $stainlessSteelTypeAndFinishAndLengths;
    protected $widths;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
        $this->products = $this->productRepository->getAll();
        $this->stainlessSteelTypeAndFinishAndLengths = $this->initStainlessSteelTypeAndFinisheAndLengths($this->products);
        $this->widths = $this->initWidths($this->products);
    }

    protected function initStainlessSteelTypeAndFinisheAndLengths(Collection $products)
    {
        $uniqueProducts =  $products->unique(function ($product) {
            $stainlessSteel = $product->productable;
            $name = sprintf('%s / %s %s', $stainlessSteel->getStainlessSteelType()->getName(), $stainlessSteel->getFinish()->getName(), $stainlessSteel->getLength()->getName());
            return $name;
        });

        return $uniqueProducts->map(function ($product) {
            $stainlessSteel = $product->productable;
            return new StainlessSteelTypeAndFinishAndLengthReport(
                $stainlessSteel->getStainlessSteelType(),
                $stainlessSteel->getFinish(),
                $stainlessSteel->getLength(),
                $this->productRepository
            );
        });
    }

    protected function initWidths(Collection $products)
    {
        return $products->map(function ($product) {
            $stainlessSteel = $product->productable;
            return $stainlessSteel->getWidth();
        })->unique(function ($width) {
            return $width->getName();
        });
    }

    public function getStainlessSteelTypeAndFinishAndLengths()
    {
        return $this->stainlessSteelTypeAndFinishAndLengths->sortBy(function ($stainlessSteelTypeAndFinishAndLength) {
            return HasOrder::getOrderByName($stainlessSteelTypeAndFinishAndLength->getName(), 'STAINLESS STEEL');
        });
    }

    public function getWidths()
    {
        return $this->widths->sortBy(function ($width) {
            return  substr($width->getName(), 0, -1);
        });
    }

    public function getProducts(StainlessSteelTypeAndFinishAndLengthReport $stainlessSteelTypeAndFinishAndLength, Thickness $thickness, Width $width)
    {
        $stainlessSteelType = $stainlessSteelTypeAndFinishAndLength->getStainlessSteelType();
        $finish = $stainlessSteelTypeAndFinishAndLength->getFinish();
        $length = $stainlessSteelTypeAndFinishAndLength->getLength();
        $temp = clone $this->productRepository;

        return $temp
            ->stainlessSteelType($stainlessSteelType)
            ->stainlessSteelFinish($finish)
            ->stainlessSteelLength($length)
            ->stainlessSteelThickness($thickness)
            ->stainlessSteelWidth($width)
            ->getAll();
    }
}
