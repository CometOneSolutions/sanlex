<?php

namespace App\Modules\Reports\Accessory\StainlessSteelReport\Model;


use App\Modules\Finish\Models\Finish;
use App\Modules\Length\Models\Length;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\StainlessSteelType\Models\StainlessSteelType;

class StainlessSteelTypeAndFinishAndLengthReport
{
    protected $name;
    protected $productRepository;
    protected $thicknesses;
    protected $finish;
    protected $length;
    protected $stainlessSteelType;

    public function __construct(
        StainlessSteelType $stainlessSteelType,
        Finish $finish,
        Length $length,
        ProductRepository $productRepository
    ) {
        $this->stainlessSteelType = $stainlessSteelType;
        $this->finish = $finish;
        $this->length = $length;
        $this->productRepository = $productRepository;
        $this->products = $this->initProducts();
        $this->thicknesses = $this->initThicknesses();
    }

    protected function initThicknesses()
    {
        return $this->products->map(function ($product) {
            $stainlessSteel = $product->productable;
            return $stainlessSteel->getThickness();
        })->unique(function ($specification) {
            return $specification->getName();
        });
    }
    protected function initProducts()
    {
        $temp = clone $this->productRepository;
        return $temp
            ->stainlessSteelType($this->getStainlessSteelType())
            ->stainlessSteelFinish($this->getFinish())
            ->stainlessSteelLength($this->getLength())
            ->getAll();
    }

    public function getThicknessesCount()
    {
        return $this->thicknesses->count();
    }

    public function getThicknesses()
    {
        return $this->thicknesses->sortBy(function ($thickness) {
            return $thickness->getName();
        });
    }

    public function getName()
    {
        return sprintf('%s / %s %s', $this->stainlessSteelType->getName(), $this->finish->getName(), $this->length->getName());
    }

    public function getStainlessSteelType()
    {
        return $this->stainlessSteelType;
    }

    public function getFinish()
    {
        return $this->finish;
    }

    public function getLength()
    {
        return $this->length;
    }
}
