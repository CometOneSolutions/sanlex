<?php

namespace App\Modules\Reports\Accessory\PEFoamReport\Model;

use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\Length\Models\Length;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\Model\AccessoryReport;
use App\Modules\Reports\Traits\HasOrder;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;
use Illuminate\Support\Collection;

class PEFoamReport extends AccessoryReport
{
    const INSULATION_TYPE = 'PE FOAM';
    protected $productRepository;
    protected $products;
    protected $backingSides;
    protected $insulationDensities;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
        $this->products = $productRepository->getAll();

        $this->backingSides = $this->initBackingSides($this->products);
        $this->insulationDensities = $this->initInsulationDensities($this->products);
    }

    protected function initBackingSides(Collection $products)
    {
        return $products->map(function ($product) {
            $insulation = $product->productable;
            return $insulation->getBackingSide();
        })->unique(function ($backingSide) {
            return $backingSide->getName();
        });
    }

    protected function initInsulationDensities(Collection $products)
    {
        return $products->map(function ($product) {
            $insulation = $product->productable;
            return new PEFoamInsulationDensityReport($insulation->getInsulationDensity(), $this->productRepository);
        })->unique(function ($insulationDensity) {
            return $insulationDensity->getName();
        });
    }

    public function getBackingSides()
    {
        return $this->backingSides->sortBy(function ($backingSide) {
            return $backingSide->getName();
        });
    }

    public function getInsulationDensities()
    {
        return $this->insulationDensities->sortBy(function ($insulationDensity) {
            return HasOrder::getOrderByName($insulationDensity->getName(), 'PE FOAM');
        });
    }

    public function getProducts(PEFoamInsulationDensityReport $insulationDensity, Thickness $thickness, Width $width, Length $length, BackingSide $backingSide)
    {
        $temp = clone $this->productRepository;

        return $temp
            ->insulationDensity($insulationDensity->getInsulationDensity())
            ->insulationThickness($thickness)
            ->insulationWidth($width)
            ->insulationLength($length)
            ->insulationBackingSide($backingSide)
            ->getAll();
    }
}
