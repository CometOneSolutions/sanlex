<?php

namespace App\Modules\Reports\Accessory\PEFoamReport\Model;

use App\Modules\InsulationDensity\Models\InsulationDensity;
use App\Modules\Product\Repositories\ProductRepository;
use Illuminate\Support\Collection;

class PEFoamInsulationDensityReport
{
    protected $insulationDensity;
    protected $insulations;
    protected $productRepository;

    public function __construct(
        InsulationDensity $insulationDensity,
        ProductRepository $productRepository
    ) {
        $this->insulationDensity = $insulationDensity;
        $this->productRepository = $productRepository;
        $this->products = $this->initProducts();

        $this->insulations = $this->initInsulations($this->products);
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;
        return $temp
            ->insulationDensity($this->insulationDensity)
            ->getAll();
    }

    protected function initInsulations(Collection $products)
    {
        return $products->map(function ($product) {
            $insulation = $product->productable;
            return $insulation;
        })->unique(function ($insulation) {
            return sprintf('%s %s %s', $insulation->getThickness()->getName(), $insulation->getWidth()->getName(), $insulation->getLength()->getName());
        });
    }

    public function getInsulations()
    {
        return $this->insulations->sortBy(function ($insulation) {
            return $insulation->getThickness()->getName();
        });
    }

    public function getInsulationsCount()
    {
        return $this->insulations->count();
    }

    public function getName()
    {
        return $this->insulationDensity->getName();
    }

    public function getInsulationDensity()
    {
        return $this->insulationDensity;
    }
}
