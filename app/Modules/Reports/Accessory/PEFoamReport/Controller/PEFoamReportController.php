<?php

namespace App\Modules\Reports\Accessory\PEFoamReport\Controller;

use App\Modules\Reports\Traits\HasLastUpdatedAt;
use App\Modules\Reports\Traits\HasInsulationType;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\PEFoamReport\Model\PEFoamReport;


class PEFoamReportController extends Controller
{
	protected $productRepository;

	public function __construct(
		ProductRepository $productRepository
	) {
		$this->productRepository = $productRepository;
		$this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
	}

	public function index()
	{
		$insulationType = HasInsulationType::getInsulationByInsulationTypeName(PEFoamReport::INSULATION_TYPE);

		$products = $this->productRepository->insulation()->inStockOrHasPending()->getAll();
		$peFoams = $this->productRepository->insulationType($insulationType)->getAll();

		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No insulation products available');
		}

		if ($peFoams->isEmpty()) {
			return redirect()->back()->with('status', 'No pe foam products available');
		}

		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::INSULATION);
		$report = new PEFoamReport($this->productRepository);

		return view('pe-foam-report.index', compact('report', 'lastUpdatedAt'));
	}

	public function responsive()
	{
		$insulationType = HasInsulationType::getInsulationByInsulationTypeName(PEFoamReport::INSULATION_TYPE);

		$products = $this->productRepository->insulation()->inStockOrHasPending()->getAll();
		$peFoams = $this->productRepository->insulationType($insulationType)->getAll();

		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No insulation products available');
		}

		if ($peFoams->isEmpty()) {
			return redirect()->back()->with('status', 'No pe foam products available');
		}

		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::INSULATION);
		$report = new PEFoamReport($this->productRepository);

		return view('pe-foam-report.responsive', compact('report', 'lastUpdatedAt'));
	}
}
