<?php

namespace App\Modules\Reports\Accessory\SkylightReport\Controller;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Reports\Traits\HasLastUpdatedAt;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\SkylightReport\Model\SkylightReport;
use CometOneSolutions\Common\Controllers\Controller;

class SkylightReportController extends Controller
{
	protected $productRepository;

	public function __construct(
		ProductRepository $productRepository
	) {
		$this->productRepository = $productRepository;
		$this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
	}

	public function index()
	{
		$products = $this->productRepository->skylight()->inStockOrHasPending()->getAll();
		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No skylight products available');
		}

		$report = new SkylightReport($this->productRepository);
		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::SKYLIGHT);

		return view('skylight-report.index', compact('report', 'lastUpdatedAt'));
	}

	public function responsive()
	{
		$products = $this->productRepository->skylight()->inStockOrHasPending()->getAll();
		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No skylight products available');
		}

		$report = new SkylightReport($this->productRepository);
		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::SKYLIGHT);

		return view('skylight-report.responsive', compact('report', 'lastUpdatedAt'));
	}
}
