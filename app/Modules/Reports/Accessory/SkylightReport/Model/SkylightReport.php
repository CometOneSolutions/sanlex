<?php

namespace App\Modules\Reports\Accessory\SkylightReport\Model;

use App\Modules\Length\Models\Length;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\Model\AccessoryReport;
use App\Modules\Reports\Traits\HasOrder;
use App\Modules\Thickness\Models\Thickness;
use Illuminate\Support\Collection;

class SkylightReport extends AccessoryReport
{

    protected $productRepository;
    protected $products;
    protected $panelProfileAndSkylightClasses;
    protected $lengths;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository->skylight()->inStockOrHasPending();
        $this->products = $this->productRepository->getAll();
        $this->panelProfileAndSkylightClasses = $this->initPanelProfileAndSkylightClasses($this->products);
        $this->lengths = $this->initLengths($this->products);
    }

    protected function initPanelProfileAndSkylightClasses(Collection $products)
    {
        $uniqueProducts = $products->unique(function ($product) {
            $skylight = $product->productable;
            $name =  sprintf('%s %s %s', $skylight->getPanelProfile()->getName(), $skylight->getSkylightClass()->getName(), $skylight->getWidth()->getName());
            return $name;
        });

        return  $uniqueProducts->map(function ($product) {
            $skylight = $product->productable;
            return new PanelProfileAndSkylightClassReport(
                $skylight->getPanelProfile(),
                $skylight->getSkylightClass(),
                $skylight->getWidth(),
                $this->productRepository
            );
        });
    }

    protected function initLengths(Collection $products)
    {
        return $products->map(function ($product) {
            $skylight = $product->productable;
            return $skylight->getLength();
        })->unique(function ($length) {
            return $length->getName();
        });
    }

    public function getPanelProfileAndSkylightClasses()
    {
        return $this->panelProfileAndSkylightClasses->sortBy(function ($panelProfileAndSkylightClass) {
            return HasOrder::getOrderByName($panelProfileAndSkylightClass->getName(), 'SKYLIGHT');
        });
    }

    public function getLengths()
    {
        return $this->lengths->sortBy(function ($length) {
            return substr($length->getName(), 0, -1);
        });
    }

    public function getLengthsCount()
    {
        return $this->lengths->count();
    }

    public function getProducts(PanelProfileAndSkylightClassReport $panelProfileAndSkylightClass, SkylightWidthReport $width, Thickness $thickness, Length $length)
    {
        $panelProfile = $panelProfileAndSkylightClass->getPanelProfile();
        $skylightClass = $panelProfileAndSkylightClass->getSkylightClass();
        $width = $width->getWidth();

        $temp = clone $this->productRepository;

        return $temp
            ->skylightPanelProfile($panelProfile)
            ->skylightClass($skylightClass)
            ->skylightWidth($width)
            ->skylightThickness($thickness)
            ->skylightLength($length)
            ->getAll();
    }
}
