<?php

namespace App\Modules\Reports\Accessory\SkylightReport\Model;

use App\Modules\PanelProfile\Models\PanelProfile;
use App\Modules\Product\Repositories\EloquentProductRepository;
use App\Modules\SkylightClass\Models\SkylightClass;
use App\Modules\Width\Models\Width;

class PanelProfileAndSkylightClassReport
{
    protected $panelProfile;
    protected $skylightClass;
    protected $productRepository;
    protected $products;

    public function __construct(
        PanelProfile $panelProfile,
        SkylightClass $skylightClass,
        Width $width,
        EloquentProductRepository $productRepository
    ) {

        $this->panelProfile = $panelProfile;
        $this->skylightClass = $skylightClass;
        $this->width = $width;
        $this->productRepository = $productRepository;

        $this->products = $this->initProducts();
        $this->width = $this->initWidth($this->products);
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;

        return $temp
            ->skylightPanelProfile($this->getPanelProfile())
            ->skylightClass($this->getSkylightClass())
            ->skylightWidth($this->getWidth())
            ->getAll();
    }

    protected function initWidth()
    {
        return new SkylightWidthReport($this->getWidth(), $this->getPanelProfile(), $this->getSkylightClass(), $this->productRepository);
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getWidthThicknessesCount()
    {
        return $this->width->getThicknessesCount();
    }

    public function getName()
    {
        return sprintf('%s %s %s', $this->panelProfile->getName(), $this->skylightClass->getName(), $this->width->getName());
    }

    // public function getNameWithWidth()
    // {
    //     return sprintf('%s %s', $this->getName(), $this->width->getName());
    // }

    public function getPanelProfile()
    {
        return $this->panelProfile;
    }

    public function getSkylightClass()
    {
        return $this->skylightClass;
    }
}
