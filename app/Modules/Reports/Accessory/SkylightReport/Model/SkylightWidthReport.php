<?php

namespace App\Modules\Reports\Accessory\SkylightReport\Model;

use App\Modules\PanelProfile\Models\PanelProfile;
use App\Modules\Product\Repositories\EloquentProductRepository;
use App\Modules\SkylightClass\Models\SkylightClass;
use App\Modules\Width\Models\Width;
use Illuminate\Support\Collection;

class SkylightWidthReport
{

    protected $panelProfile;
    protected $skylightClass;
    protected $productRepository;
    protected $products;
    protected $thicknesses;
    protected $width;

    public function __construct(
        Width $width,
        PanelProfile $panelProfile,
        SkylightClass $skylightClass,
        EloquentProductRepository $productRepository
    ) {

        $this->width = $width;
        $this->panelProfile = $panelProfile;
        $this->skylightClass = $skylightClass;
        $this->productRepository = $productRepository;

        $this->products = $this->initProducts();
        $this->thicknesses = $this->initThicknesses($this->products);
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;

        return $temp
            ->skylightPanelProfile($this->getPanelProfile())
            ->skylightClass($this->getSkylightClass())
            ->skylightWidth($this->getWidth())
            ->getAll();
    }

    protected function initThicknesses(Collection $products)
    {
        return $products->map(function ($product) {
            $skylight = $product->productable;
            return $skylight->getThickness();
        })->unique(function ($thickness) {
            return $thickness->getName();
        });
    }

    public function getThicknesses()
    {
        return $this->thicknesses->sortBy(function ($thickness) {
            return $thickness->getName();
        });
    }

    public function getThicknessesCount()
    {
        return $this->thicknesses->count();
    }

    public function getName()
    {
        return $this->width->getName();
    }

    public function getPanelProfile()
    {
        return $this->panelProfile;
    }

    public function getSkylightClass()
    {
        return $this->skylightClass;
    }

    public function getWidth()
    {
        return $this->width;
    }
}
