@extends('reports.template')
@section('title', 'Skylight')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
    <table class="bordered">
        <thead>
            <tr>
                <th rowspan="2" style="width:225px">Profile / Class</th>
                <th rowspan="2" style="width:40px">W</th>
                <th rowspan="2" style="width:40px">T</th>
                <th colspan="{{$report->getLengthsCount()}}">Length</th>
            </tr>
            <tr>
                @foreach($report->getLengths() as $length)
                <th style="width:55px">{{ $length->getName() }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($report->getPanelProfileAndSkylightClasses() as $panelProfileAndSkylightClass)
            <tr>
                <td class="no-shade" align="center" rowspan="{{ $panelProfileAndSkylightClass->getWidthThicknessesCount() }}"> {{ $panelProfileAndSkylightClass->getName() }}</td>  
                @php
                    $width = $panelProfileAndSkylightClass->getWidth();                
                @endphp
                <td class="no-shade" align="center" rowspan="{{ $width->getThicknessesCount() }}"> {{ $width->getName() }}</td>
                    @foreach($width->getThicknesses() as $thickness)
                    <td class="no-shade" align="center"> {{ $thickness->getName() }}</td>
                    @foreach($report->getLengths() as $length)
                        @php
                        $products = $report->getProducts($panelProfileAndSkylightClass, $width, $thickness, $length)
                        @endphp
                        <td align="center"  >
                            @if($products->count() > 0)
                            {{ $report->getNumberFormattedStockQuantity($products) }}
                            @if($report->getNumberFormattedPendingQuantity($products) != '-')
                            /
                            <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                            @endif
                            @else
                            -
                            @endif
                        </td>
                        @endforeach             
            </tr>   
                @endforeach
            @endforeach
        </tbody>
    </table>
@endsection