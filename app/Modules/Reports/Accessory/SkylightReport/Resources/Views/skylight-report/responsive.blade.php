@extends('reports.template-responsive')
@section('title', 'Skylight')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
<table>
    <thead>
        <tr>
            <th rowspan="2" class="fixed no-shade width-md">Profile / Class / Width</th>
            <th rowspan="2" class="fixed no-shade width-sm">T</th>
            <th colspan="{{$report->getLengthsCount()}}">Length</th>
        </tr>
        <tr>
            @foreach($report->getLengths() as $length)
            <th class="width-lg">{{ $length->getName() }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($report->getPanelProfileAndSkylightClasses() as $panelProfileAndSkylightClass)
        <tr>
            <td class="fixed no-shade width-md" align="center" rowspan="{{ $panelProfileAndSkylightClass->getWidthThicknessesCount() }}"> {{ $panelProfileAndSkylightClass->getName() }}</td>
            @php
            $width = $panelProfileAndSkylightClass->getWidth();
            @endphp
            {{-- <td class="fixed no-shade width-sm" align="center" rowspan="{{ $width->getThicknessesCount() }}"> {{ $width->getName() }}</td> --}}
            @foreach($width->getThicknesses() as $thickness)
            <td class="fixed  width-sm" align="center"> {{ $thickness->getName() }}</td>
            @foreach($report->getLengths() as $length)
            @php
            $products = $report->getProducts($panelProfileAndSkylightClass, $width, $thickness, $length)
            @endphp
            <td align="center" class="width-lg">
                @if($products->count() > 0)
                {{ $report->getNumberFormattedStockQuantity($products) }}
                @if($report->getNumberFormattedPendingQuantity($products) != '-')
                /
                <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                @endif
                @else
                -
                @endif
            </td>
            @endforeach
        </tr>
        @endforeach
        @endforeach
    </tbody>
</table>
@endsection
