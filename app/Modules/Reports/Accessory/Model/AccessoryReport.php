<?php

namespace App\Modules\Reports\Accessory\Model;

use Illuminate\Support\Collection;

class AccessoryReport
{
    public function getNumberFormattedPendingQuantity(Collection $products)
    {
        $totalPendingQuantity =  $products->reduce(function ($carry, $item) {
            return $carry + $item->getPendingQuantity();
        }, 0);

        if ($totalPendingQuantity == 0) {
            return '-';
        }

        return ($this->is_decimal($totalPendingQuantity)) ? number_format($totalPendingQuantity, 2) : number_format($totalPendingQuantity, 0);
    }

    public function getNumberFormattedStockQuantity(Collection $products)
    {
        $totalStockQuantity =  $products->reduce(function ($carry, $item) {
            return $carry + $item->getStockQuantity();
        }, 0);

        if ($totalStockQuantity == 0) {
            return '-';
        }

        return ($this->is_decimal($totalStockQuantity)) ? number_format($totalStockQuantity, 2) : number_format($totalStockQuantity, 0);
    }

    private function is_decimal($val)
    {
        return is_numeric($val) && floor($val) != $val;
    }
}
