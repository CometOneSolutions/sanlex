@extends('app')
@section('breadcrumbs', Breadcrumbs::render('report-accessory.summary'))
@section('content')
<div class="card table-responsive">

    <div class="card-body">
        <div class="card" class="col-md-6">
            <div class="card-header">
                Available Keywords
            </div>
            @if(count($keywords) > 0)
            <ul class="list-group list-group-flush">
                @foreach($keywords as $keyword)
                <li class="list-group-item"><a target="_blank" href="{{route('reports_accessory-print', $keyword)}}"><span style="font-size:1.3em; font-weight: bold;">{{$keyword}}</span></a>
                </li>
                @endforeach
            </ul>
            @else
            <div class="text-center">No items to display.</div>
            @endif
        </div>
    </div>
</div>
@stop