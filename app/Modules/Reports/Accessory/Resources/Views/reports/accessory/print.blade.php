<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Accessory Summary</title>
    <style>
        html {
            display: table;
            margin: 0 10px;
        }

        body {
            margin: 0 auto;
            font-size: 12px;
            width: 794px;
            font-family: 'Open Sans', sans-serif;
            margin-left: 10px;
            margin-right: 10px;

        }

        .green {
            color: darkgreen;
        }

        .pending {
            color: gray
        }

        .title {
            text-align: center;
            text-decoration: underline;
            text-transform: uppercase;
            font-size: 1.2em;
        }

        table,
        tr,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 2px;

        }

        table tr td:nth-child(2),
        table tr td:nth-child(3),
        table tr td:nth-child(4),
        table tr td:nth-child(5) {
            text-align: center;
        }

        table .bordered tr,
        table .bordered th,
        table .bordered td {

            border: 1px solid black;


        }

        p {
            margin: 7px auto 7px auto;
        }

        .page-header,
        .page-header-space {
            height: 0;
        }

        .page-footer,
        .page-footer-space {
            height: 50px;

        }

        .border-on-tr>tr {
            border: 1px solid black;
        }

        .border-on-th>th {
            border: 1px solid black;
        }

        .border-on-td>td {
            border: 1px solid black;
        }

        .page-footer {
            position: fixed;
            bottom: 0;
            width: 100%;
            border: 0;
        }

        .page-header {
            position: fixed;
            top: 0mm;
            width: 100%;
        }

        .page {
            page-break-after: always;
        }

        table,
        tr,
        th,
        td {
            border: 0;

        }

        table {
            width: 100%;
        }



        @media screen {
            html {
                margin: 0 auto;
            }

            body {
                font-size: 16px;
                width: 100%;
            }
        }

        tr:nth-child(even) {
            background: #CCC
        }

        tr:nth-child(odd) {
            background: #FFF
        }

        @media print {


            button {
                display: none;
            }

            body {
                margin: 0;
            }
        }

        tr:nth-child(even) {
            background: #CCC
        }

        tr:nth-child(odd) {
            background: #FFF
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td>
                <div class="header-space print-margin-top">
																	<center>
																		<h1 style="float:left;">ACCESSORY</h1>
																						<div style="float:right; margin-top: 30px">Updated as of: {{$lastUpdatedAt->toDayDateTimeString()}}</div>
																						<div style="clear:both;"></div>

																		</center>
                </div>
            </td>
        </tr>
        <tbody>
            <tr>
                <td>
                    <table class="bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">
                                    #
                                </th>
                                <th align="left">
                                    Description
                                </th>
                                <th align="center">
                                    Stock
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                          
                       
																												@php 
																												$index = 0;
                            @endphp

                            @foreach($accessories as $accessory)
                            
                            <tr>
                                <td style="text-align:center">{{$index+1}}</td>
                                <td style="text-align:left">{{$accessory->getDescription()}}</td>
                                <td>{{number_format($accessory->product->getInventory()->getStock(), 0)}}</td>
                            </tr>
                            @php 
																												$index++;
                            @endphp
                            @endforeach
                            

                        </tbody>
                    </table>
                    <br>
                    <br>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>