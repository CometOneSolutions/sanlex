<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inventory Summary</title>
    <style>
        html {
            display: table;
            margin: 0 10px;
        }

        body {
            margin: 0 auto;
            font-size: 12px;
            width: 794px;
            font-family: 'Open Sans', sans-serif;
            margin-left: 10px;
            margin-right: 10px;

        }

        .green {
            color: darkgreen;
        }

        .pending {
            color: gray
        }

        .title {
            text-align: center;
            text-decoration: underline;
            text-transform: uppercase;
            font-size: 1.2em;
        }

        table,
        tr,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;

        }

        table tr td:nth-child(2),
        table tr td:nth-child(3),
        table tr td:nth-child(4),
        table tr td:nth-child(5) {
            text-align: center;
        }

        table .bordered tr,
        table .bordered th,
        table .bordered td {

            border: 1px solid black;


        }

        p {
            margin: 7px auto 7px auto;
        }

        .red {
            color: red;
        }
        .shade {
            background: #DDD;
        }

        .page-header,
        .page-header-space {
            height: 0;
        }

        .page-footer,
        .page-footer-space {
            height: 50px;

        }

        .border-on-tr>tr {
            border: 1px solid black;
        }

        .border-on-th>th {
            border: 1px solid black;
        }

        .border-on-td>td {
            border: 1px solid black;
        }

        .page-footer {
            position: fixed;
            bottom: 0;
            width: 100%;
            border: 0;
        }

        .page-header {
            position: fixed;
            top: 0mm;
            width: 100%;
        }

        .page {
            page-break-after: always;
        }

        table,
        tr,
        th,
        td {
            border: 0;

        }

        table {
            width: 100%;
        }
								.rotate {
										/* text-orientation: sideways; */
										writing-mode: vertical-rl;
										transform: rotate(180deg);
										/* text-align: center; */
										}



        @media screen {
            html {
                margin: 0 auto;
            }

            body {
                font-size: 16px;
                width: 100%;
            }
        }

        /* tr.shade:nth-child(even) {
            background: #DDD
        }

        tr.shade:nth-child(odd) {
            background: #FFF
        } */
							
        @media print {


            button {
                display: none;
            }

            body {
                margin: 0;
            }
        }

        /* tr.shade:nth-child(even) {
            background: #DDD
        }

        tr:nth-child(odd) {
            background: #FFF
        } */
    </style>
</head>

<body>
    <table>
        <tr>
            <td>
                <div class="header-space print-margin-top">
                    <center>
                        <h1 style="float:left;">PVC FITTING</h1>
                        <div style="float:right; margin-top: 30px">Updated as of: {{$lastUpdatedAt->toDayDateTimeString()}}</div>
                        <div style="clear:both;"></div>

                    </center>
                </div>
            </td>
        </tr>
            <tbody>
                <tr>
                    <td>
                        <table class="bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2" style="width:100px" rowspan="2">TYPE</th>
                                    <th rowspan="2" style="width:100px" rowspan="2">BRAND</th>
                                    <th style="width:80px" colspan="{{$diameters->count()}}">DIAMETER</th>
                                   
                                </tr>
                                <tr>
                                    @foreach($diameters as $key => $list)
                                        <th style="width:80px">{{$key}}</th>
                                    @endforeach
                                </tr>
                            </thead>

                            <tbody>
                                @php
                                    $row = 0;
                                @endphp
                                @foreach($types as $key => $brandList)
                                    @php
                                        
                                        $count = 0;
                                    @endphp
                                    @foreach($brandList as $brandKey => $diameterList)
                                        @php
                                            $brandCount = 0;
                                          
                                        @endphp
                                        
                                        
                                        <tr>
                                            @if ($count === 0)
                                                <td  align="center"  rowspan="{{$brandList->count()}}">{{$key}}</td>	
                                            @endif
                                            @if($brandCount === 0)
                                                <td  align="center" @if($row%2 === 0) class="shade" @endif>{{$brandKey}}</td>	
                                            @endif

                                                @foreach($diameters as $dKey => $dList)
                                                    @php
                                                    
                                                    $isFound = false;
                                                    // dd($thicknessList);
                                                    @endphp
                                                    @foreach($diameterList as $ddKey => $products)
                                            
                                                        @if($dKey === $ddKey
                                                        )
                                                        @php
                                                        
                                                        // dd($products->first()->productable->getThickness()->getName() === $tKey);
                                                        $isFound = true;
                                                        @endphp
                                                        <td align="center" @if($row%2 === 0) class="shade" @endif>
                                                            {{ $products->first()->getNumberFormattedStockQuantity() }} 
                                                            @if($products->first()->getNumberFormattedPendingQuantity() != '-')
                                                            /
                                                            <span class="red">{{ $products->first()->getNumberFormattedPendingQuantity() }}</span>
                                                            @endif
                                                        </td>
                                                        @php
                                                        
                                                        @endphp
                                                        @endif
                                                    @endforeach
                                                    @if(!$isFound)
                                                    <td align="center" @if($row%2 === 0) class="shade" @endif>-</td>
                                                    @endif
                                                @endforeach
                                            
                                        </tr>
                                        @php
                                            $row++;
                                            $count++;
                                            $brandCount++;
                                        @endphp
                                        
                                    @endforeach
                                @endforeach
                            </tbody>


             


                            
                        </table>
                        <br>
                        <br>
                    </td>
                </tr>
            </tbody>
    </table>
				
</body>

</html>