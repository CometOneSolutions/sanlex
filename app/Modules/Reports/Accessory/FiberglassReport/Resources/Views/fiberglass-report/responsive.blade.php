@extends('reports.template-responsive')
@section('title', 'fiberglass Insulation')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
    <table class="bordered">
        <thead>
            <tr>
                <th rowspan="2" class="fixed no-shade width-md">DENSITY</th>
                <th class="fixed no-shade width-sm">T</th>
                <th class="fixed no-shade width-sm">W</th>
                <th class="fixed no-shade width-sm">L</th>
                @foreach($report->getBackingSides() as $backingSide)
                <th>{{ $backingSide->getName() }}</th>
                @endforeach
            </tr>
            <tr>
                <th class="fixed no-shade width-sm">mm</th>
                <th class="fixed no-shade width-sm">m</th>
                <th class="fixed no-shade width-sm">m</th>
                @foreach($report->getBackingSides() as $backingSide)
                <th class="width-lg">ROLLS</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($report->getInsulationDensities() as $insulationDensity)
            <tr>
                <td class="fixed no-shade width-md" align="center" rowspan="{{ $insulationDensity->getInsulationsCount() }}"> {{ $insulationDensity->getName() }}</td>                                    
                    @foreach($insulationDensity->getInsulations() as $insulation)
                    @php
                        $thickness = $insulation->getThickness();
                        $width = $insulation->getWidth();
                        $length = $insulation->getLength();
                    @endphp
                    <td class="fixed  width-sm" align="center"> {{ $thickness->getName() }}</td>
                    <td class="fixed  width-sm" align="center"> {{ $width->getName() }}</td>
                    <td class="fixed  width-sm" align="center"> {{ $length->getName() }}</td>
                    @foreach($report->getBackingSides() as $backingSide)
                        @php
                        $products = $report->getProducts($insulationDensity, $thickness, $width, $length, $backingSide)
                        @endphp
                        <td align="center" class="width-lg">
                            @if($products->count() > 0)
                            {{ $report->getNumberFormattedStockQuantity($products, $type = 'Insulation') }}
                            @if($report->getNumberFormattedPendingQuantity($products, $type = 'Insulation') != '-')
                            /
                            <span class="red">{{ $report->getNumberFormattedPendingQuantity($products, $type = 'Insulation') }}</span>
                            @endif
                            @else
                            -
                            @endif
                        </td>
                        @endforeach             
            </tr>   
                @endforeach
            @endforeach
        </tbody>
    </table>
@endsection