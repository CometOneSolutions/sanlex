@extends('reports.template')
@section('title', 'fiberglass Insulation')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
    <table class="bordered">
        <thead>
            <tr>
                <th rowspan="2" style="width:30px">DENSITY</th>
                <th style="width:30px">T</th>
                <th style="width:30px">W</th>
                <th style="width:30px">L</th>
                @foreach($report->getBackingSides() as $backingSide)
                <th style="width:30px">{{ $backingSide->getName() }}</th>
                @endforeach
            </tr>
            <tr>
                <th style="width:30px">mm</th>
                <th style="width:30px">m</th>
                <th style="width:30px">m</th>
                @foreach($report->getBackingSides() as $backingSide)
                <th style="width:30px">ROLLS</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($report->getInsulationDensities() as $insulationDensity)
            <tr>
                <td class="no-shade" align="center" rowspan="{{ $insulationDensity->getInsulationsCount() }}"> {{ $insulationDensity->getName() }}</td>                                    
                    @foreach($insulationDensity->getInsulations() as $insulation)
                    @php
                        $thickness = $insulation->getThickness();
                        $width = $insulation->getWidth();
                        $length = $insulation->getLength();
                    @endphp
                    <td  align="center"> {{ $thickness->getName() }}</td>
                    <td  align="center"> {{ $width->getName() }}</td>
                    <td  align="center"> {{ $length->getName() }}</td>
                    @foreach($report->getBackingSides() as $backingSide)
                        @php
                        $products = $report->getProducts($insulationDensity, $thickness, $width, $length, $backingSide)
                        @endphp
                        <td align="center"  >
                            @if($products->count() > 0)
                            {{ $report->getNumberFormattedStockQuantity($products, $type = 'Insulation') }}
                            @if($report->getNumberFormattedPendingQuantity($products, $type = 'Insulation') != '-')
                            /
                            <span class="red">{{ $report->getNumberFormattedPendingQuantity($products, $type = 'Insulation') }}</span>
                            @endif
                            @else
                            -
                            @endif
                        </td>
                        @endforeach             
            </tr>   
                @endforeach
            @endforeach
        </tbody>
    </table>
@endsection