<?php

namespace App\Modules\Reports\Accessory\FiberglassReport\Controller;


use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Reports\Accessory\FiberglassReport\Model\FiberGlassReport;
use App\Modules\Reports\Traits\HasInsulationType;
use App\Modules\Reports\Traits\HasLastUpdatedAt;
use App\Modules\Product\Repositories\ProductRepository;
use CometOneSolutions\Common\Controllers\Controller;

class FiberglassReportController extends Controller
{
	protected $productRepository;

	public function __construct(
		ProductRepository $productRepository
	) {
		$this->productRepository = $productRepository;
		$this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
	}

	public function index()
	{
		$insulationType = HasInsulationType::getInsulationByInsulationTypeName(FiberglassReport::INSULATION_TYPE);

		$products = $this->productRepository->insulation()->inStockOrHasPending()->getAll();
		$fiberGlasses = $this->productRepository->insulationType($insulationType)->getAll();

		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No insulation products available');
		}

		if ($fiberGlasses->isEmpty()) {
			return redirect()->back()->with('status', 'No fiberglass products available');
		}

		$report = new FiberglassReport($this->productRepository);
		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::INSULATION);

		return view('fiberglass-report.index', compact('report', 'lastUpdatedAt'));
	}

	public function responsive()
	{
		$insulationType = HasInsulationType::getInsulationByInsulationTypeName(FiberglassReport::INSULATION_TYPE);

		$products = $this->productRepository->insulation()->inStockOrHasPending()->getAll();
		$fiberGlasses = $this->productRepository->insulationType($insulationType)->getAll();

		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No insulation products available');
		}

		if ($fiberGlasses->isEmpty()) {
			return redirect()->back()->with('status', 'No fiberglass products available');
		}

		$report = new FiberglassReport($this->productRepository);
		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::INSULATION);

		return view('fiberglass-report.responsive', compact('report', 'lastUpdatedAt'));
	}
}
