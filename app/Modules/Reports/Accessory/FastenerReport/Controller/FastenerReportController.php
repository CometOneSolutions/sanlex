<?php

namespace App\Modules\Reports\Accessory\FastenerReport\Controller;

use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Reports\Accessory\FastenerReport\Model\FastenerReport;
use App\Modules\Reports\Traits\HasLastUpdatedAt;
use CometOneSolutions\Common\Controllers\Controller;

class FastenerReportController extends Controller
{
    protected $productRepository;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
        $this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
    }

    public function index()
    {
        if ($this->productRepository->fastener()->inStockOrHasPending()->count() == 0) {
            return redirect()->back()->with('status', 'No fastener products available');
        }

        $lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::FASTENER);
        $report = new FastenerReport($this->productRepository);

        return view('fastener-report.index', compact('report', 'lastUpdatedAt'));
    }

    public function responsive()
    {
        if ($this->productRepository->fastener()->inStockOrHasPending()->count() == 0) {
            return redirect()->back()->with('status', 'No fastener products available');
        }
        $lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::FASTENER);
        $report = new FastenerReport($this->productRepository);

        return view('fastener-report.responsive', compact('report', 'lastUpdatedAt'));
    }
}
