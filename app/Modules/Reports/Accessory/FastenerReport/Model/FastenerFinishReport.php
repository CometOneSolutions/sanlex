<?php

namespace App\Modules\Reports\Accessory\FastenerReport\Model;

use App\Modules\Finish\Models\Finish;

class FastenerFinishReport
{
    protected $name;
    protected $fastenerSizes;
    protected $finish;

    public function __construct(Finish $finish)
    {
        $this->finish = $finish;
        $this->name = $this->finish->getName();
        $this->fastenerSizes = $this->initFastenerSizes($this->finish);
    }

    protected function initFastenerSizes(Finish $finish)
    {
        return $finish->getFastenerSizes()->map(function ($fastenerSize) {
            return $fastenerSize;
        })->unique(function ($fastenerSize) {
            return $fastenerSize->getName();
        });
    }

    public function getFastenerSizes()
    {
        return $this->fastenerSizes->sortBy(function ($fastenerSize) {
            return $fastenerSize->getName();
        });
    }

    public function getFastenerSizesCount()
    {
        return $this->fastenerSizes->count();
    }

    public function getName()
    {
        return  $this->name;
    }

    public function getFinish()
    {
        return $this->finish;
    }
}
