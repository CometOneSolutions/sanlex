<?php

namespace App\Modules\Reports\Accessory\FastenerReport\Model;


use App\Modules\FastenerType\Models\FastenerType;
use App\Modules\Reports\Traits\HasOrder;

class FastenerTypeReport
{
    protected $name;
    protected $fastenerType;
    protected $fastenerDimensions;
    protected $finishes;

    public function __construct(FastenerType $fastenerType)
    {
        $this->fastenerType = $fastenerType;
        $this->name = $this->fastenerType->getName();
        $this->fastenerDimensions = $this->initFastenerDimensions($this->fastenerType);
    }

    protected function initFastenerDimensions(FastenerType $fastenerType)
    {
        return $fastenerType->getFastenerDimensions()->map(function ($fastenerDimension) {
            return $fastenerDimension;
        })->unique(function ($fastenerDimension) {
            return $fastenerDimension->getName();
        });
    }

    public function getFastenerDimensions()
    {
        return $this->fastenerDimensions->sortBy(function ($fastenerDimension) {
            return (float) (trim(preg_replace('/^[0-9]+(\\.[0-9]+)?$/', '', $fastenerDimension->getName()), '"'));
        });
    }

    public function getFastenerType()
    {
        return $this->fastenerType;
    }

    public function getFastenerDimensionsCount()
    {
        return $this->fastenerDimensions->count();
    }

    public function getName()
    {
        return  $this->name;
    }

    public function isOrdered()
    {
        return HasOrder::isOrdered($this->getName(), 'FASTENER');
    }
}
