<?php

namespace App\Modules\Reports\Accessory\FastenerReport\Model;

use App\Modules\Size\Models\Size;
use Illuminate\Support\Collection;
use App\Modules\Reports\Traits\HasOrder;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\Model\AccessoryReport;
use App\Modules\FastenerDimension\Models\FastenerDimension;

class FastenerReport extends AccessoryReport
{
    protected $productRepository;
    protected $products;
    protected $fastenerTypes;
    protected $finishes;
    protected $summaryOrderDetails;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository->fastener()->inStockOrHasPending();
        $this->products = $this->productRepository->getAll();


        $this->finishes = $this->initFinishes($this->products, $this->productRepository);
        $this->fastenerTypes = $this->initFastenerTypes($this->products);
    }

    protected function initFinishes(Collection $products)
    {
        return $products->map(function ($product) {
            $fastener = $product->productable;
            return new FastenerFinishReport($fastener->getFinish());
        })->unique(function ($finish) {
            return $finish->getName();
        });
    }

    protected function initFastenerTypes(Collection $products)
    {
        return  $products->map(function ($product) {
            $fastener = $product->productable;
            return new FastenerTypeReport($fastener->getFastenerType());
        })->unique(function ($fastenerType) {
            return $fastenerType->getName();
        });
    }

    public function getFinishes()
    {
        return $this->finishes->sortBy(function ($finish) {
            return $finish->getName();
        });
    }

    public function getFastenerTypes()
    {
        return $this->fastenerTypes->sortBy(function ($fastenerType) {
            return HasOrder::getOrderByName($fastenerType->getName(), 'FASTENER');
        });
    }

    public function getProducts(
        Size $size,
        FastenerFinishReport $finishReport,
        FastenerDimension $fastenerDimension,
        FastenerTypeReport $fastenerTypeReport
    ) {
        $temp = clone $this->productRepository;
        return $temp
            ->fastenerSize($size)
            ->fastenerFinish($finishReport->getFinish())
            ->fastenerDimension($fastenerDimension)
            ->fastenerType($fastenerTypeReport->getFastenerType())
            ->getAll();
    }
}
