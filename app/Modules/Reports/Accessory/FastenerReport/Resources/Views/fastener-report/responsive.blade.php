@extends('reports.template-responsive')
@section('title', 'Fastener')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
<table>
    <thead>
        <tr>
            <th rowspan="2" class="no-shade fixed width-lg">TYPE</th>
            <th rowspan="2" class="no-shade fixed width-lg">SPECIFICATIONS</th>
            @foreach($report->getFinishes() as $finish)
            <th class="width-md" colspan="{{ $finish->getFastenerSizesCount() }}">{{$finish->getName()}}</th>
            @endforeach
        </tr>
        <tr>
            @foreach($report->getFinishes() as $finish)
            @foreach($finish->getFastenerSizes() as $fastenerSizes)
            <th class="width-md">{{ $fastenerSizes->getName() }}</th>
            @endforeach
            @endforeach
        </tr>
    </thead>

    <tbody>
        @foreach($report->getFastenerTypes() as $fastenerType)
        <tr>
            <td rowspan="{{ $fastenerType->getFastenerDimensionsCount() }}" align="center" class="fixed no-shade width-lg">
                {{ $fastenerType->getName() }}
                @if($fastenerType->isOrdered())
                <br>
                <div class="ordered"></div>
                @else
                <br>
                <div class="unordered"></div>
                @endif
            </td>
            @foreach($fastenerType->getFastenerDimensions() as $fastenerDimension)
            <td align="center" class="fixed width-lg"> {{ $fastenerDimension->getName() }} </td>

            @foreach($report->getFinishes() as $finish)
            @foreach($finish->getFastenerSizes() as $fastenerSize)
            @php
            $products = $report->getProducts($fastenerSize, $finish, $fastenerDimension, $fastenerType)
            @endphp
            <td align="center" class="width-md">
                @if($products->count() > 0)
                {{ $report->getNumberFormattedStockQuantity($products) }}
                @if($report->getNumberFormattedPendingQuantity($products) != '-')
                /
                <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                @endif
                @else
                -
                @endif
            </td>
            @endforeach
            @endforeach
        </tr>
        @endforeach
        @endforeach
    </tbody>
</table>
@endsection
