@extends('reports.template')
@section('title', 'Fastener')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
<table class="bordered">
    <thead>
        <tr>
            <th  style="width:100px" rowspan="2">TYPE</th>
            <th style="width:80px" rowspan="2">SPECIFICATIONS</th>
            @foreach($report->getFinishes() as $finish)
                <th style="width:80px" colspan="{{ $finish->getFastenerSizesCount() }}">{{$finish->getName()}}</th>
            @endforeach
        </tr>
        <tr>
            @foreach($report->getFinishes() as $finish)
                @foreach($finish->getFastenerSizes() as $fastenerSizes)
                    <th style="width:80px">{{ $fastenerSizes->getName() }}</th>
                @endforeach
            @endforeach
        </tr>
    </thead>   

    <tbody>
    @foreach($report->getFastenerTypes() as $fastenerType)
    <tr>
        <td rowspan="{{ $fastenerType->getFastenerDimensionsCount() }}"  align="center" class="no-shade" >{{ $fastenerType->getName() }}</td>
        @foreach($fastenerType->getFastenerDimensions() as $fastenerDimension)
        <td  align="center">  {{ $fastenerDimension->getName() }} </td>
        
            @foreach($report->getFinishes() as $finish)
                @foreach($finish->getFastenerSizes() as $fastenerSize)
                @php
                $products = $report->getProducts($fastenerSize, $finish, $fastenerDimension, $fastenerType)
                @endphp
                <td align="center"  >
                    @if($products->count() > 0)
                    {{ $report->getNumberFormattedStockQuantity($products) }}
                    @if($report->getNumberFormattedPendingQuantity($products) != '-')
                    /
                    <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                    @endif
                    @else
                    -
                    @endif
                </td>
                @endforeach
        @endforeach
    </tr>
        @endforeach
    @endforeach
    </tbody>
</table>   
@endsection
