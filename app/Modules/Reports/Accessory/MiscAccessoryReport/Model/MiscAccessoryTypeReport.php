<?php

namespace App\Modules\Reports\Accessory\MiscAccessoryReport\Model;

use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use App\Modules\Product\Repositories\EloquentProductRepository;

class MiscAccessoryTypeReport
{
    protected $name;
    protected $miscAccessoryType;
    protected $productRepository;
    protected $widths;
    protected $products;

    public function __construct(
        MiscAccessoryType $miscAccessoryType,
        EloquentProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
        $this->name = $miscAccessoryType->getName();
        $this->miscAccessoryType = $miscAccessoryType;
        $this->products = $this->initProducts();
        $this->widths = $this->initWidths($this->miscAccessoryType);
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;
        return $temp
            ->miscAccessoryType($this->getMiscAccessoryType())
            ->getAll();
    }


    protected function initWidths(MiscAccessoryType $miscAccessoryType)
    {
        return $this->products->map(function ($product) use ($miscAccessoryType) {
            $miscAccessory = $product->productable;
            return new MiscAccessoryWidthReport($miscAccessory->getWidth(), $miscAccessoryType, $this->productRepository);
        })->unique(function ($width) {
            return $width->getName();
        });
    }

    public function getWidths()
    {
        return $this->widths->sortBy(function ($width) {
            return $width->getName();
        });
    }

    public function getWidthsCount()
    {
        return $this->widths->count();
    }

    public function getWidthsLengthsCount()
    {
        return $this->widths->reduce(function ($carry, $item) {
            return $carry + $item->getLengthsCount();
        }, 0);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getMiscAccessoryType()
    {
        return $this->miscAccessoryType;
    }
}
