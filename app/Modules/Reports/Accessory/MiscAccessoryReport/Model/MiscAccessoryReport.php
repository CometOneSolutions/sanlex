<?php

namespace App\Modules\Reports\Accessory\MiscAccessoryReport\Model;

use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\Length\Models\Length;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\Model\AccessoryReport;
use App\Modules\Reports\Traits\HasOrder;
use Illuminate\Support\Collection;

class MiscAccessoryReport extends AccessoryReport
{
    protected $productRepository;
    protected $products;
    protected $miscAccessoryTypes;
    protected $backingSides;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository->miscAccessory()->inStockOrHasPending();
        $this->products = $this->productRepository->getAll();
        $this->miscAccessoryTypes = $this->initMiscAccessoryTypes($this->products);
        $this->backingSides = $this->initBackingSides($this->products);
    }

    protected function initMiscAccessoryTypes(Collection $products)
    {
        return  $products->map(function ($product) {
            $miscAccessory = $product->productable;
            return new MiscAccessoryTypeReport($miscAccessory->getMiscAccessoryType(), $this->productRepository);
        })->unique(function ($miscAccessoryType) {
            return $miscAccessoryType->getName();
        });
    }

    protected function initBackingSides(Collection $products)
    {
        return  $products->map(function ($product) {
            $miscAccessory = $product->productable;
            return $miscAccessory->getBackingSide();
        })->unique(function ($backingSide) {
            return $backingSide->getName();
        });
    }

    public function getMiscAccessoryTypes()
    {
        return $this->miscAccessoryTypes->sortBy(function ($miscAccessoryType) {
            return HasOrder::getOrderByName($miscAccessoryType->getName(), 'MISC ACCESSORY');
        });
    }

    public function getBackingSides()
    {
        return $this->backingSides->sortBy(function ($backingSide) {
            return $backingSide->getName();
        });
    }

    public function getProducts(MiscAccessoryTypeReport $miscAccessoryType, MiscAccessoryWidthReport $width, Length $length, BackingSide $backingSide)
    {
        $temp = clone $this->productRepository;
        return $temp
            ->miscAccessoryType($miscAccessoryType->getMiscAccessoryType())
            ->miscAccessoryLength($length)
            ->miscAccessoryWidth($width->getWidth())
            ->miscAccessoryBackingSide($backingSide)
            ->getAll();
    }
}
