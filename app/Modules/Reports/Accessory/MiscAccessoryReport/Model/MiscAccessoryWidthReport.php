<?php

namespace App\Modules\Reports\Accessory\MiscAccessoryReport\Model;

use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use App\Modules\Product\Repositories\EloquentProductRepository;
use App\Modules\Width\Models\Width;
use Illuminate\Database\Eloquent\Collection;

class MiscAccessoryWidthReport
{
    protected $name;
    protected $width;
    protected $miscAccessoryType;
    protected $productRepository;
    protected $products;
    protected $lengths;

    public function __construct(
        Width $width,
        MiscAccessoryType $miscAccessoryType,
        EloquentProductRepository $productRepository
    ) {
        $this->name = $width->getName();
        $this->width = $width;
        $this->miscAccessoryType = $miscAccessoryType;
        $this->productRepository = $productRepository;
        $this->products = $this->initProducts();
        $this->lengths = $this->initLengths($this->products);
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;
        return $temp
            ->miscAccessoryType($this->getMiscAccessoryType())
            ->miscAccessoryWidth($this->getWidth())
            ->getAll();
    }

    protected function initLengths(Collection $products)
    {
        return $products->map(function ($product) {
            $miscAccessory = $product->productable;
            return $miscAccessory->getLength();
        })->unique(function ($length) {
            return $length->getName();
        });
    }

    public function getLengths()
    {
        return $this->lengths->sortBy(function ($length) {
            return $length->getName();
        });
    }

    public function getLengthsCount()
    {
        return $this->lengths->count();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getMiscAccessoryType()
    {
        return $this->miscAccessoryType;
    }
}
