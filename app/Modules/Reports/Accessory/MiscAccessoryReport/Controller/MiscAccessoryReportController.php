<?php

namespace App\Modules\Reports\Accessory\MiscAccessoryReport\Controller;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Reports\Accessory\MiscAccessoryReport\Model\MiscAccessoryReport;
use App\Modules\Reports\Traits\HasLastUpdatedAt;
use App\Modules\Product\Repositories\ProductRepository;
use CometOneSolutions\Common\Controllers\Controller;


class MiscAccessoryReportController extends Controller
{
	protected $productRepository;

	public function __construct(
		ProductRepository $productRepository
	) {
		$this->productRepository = $productRepository;
		$this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
	}

	public function index()
	{
		$products = $this->productRepository->miscAccessory()->inStockOrHasPending()->getAll();

		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No misc accessory products available');
		}


		$report = new MiscAccessoryReport($this->productRepository);
		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::MISC);

		return view('misc-accessory-report.index', compact('report', 'lastUpdatedAt'));
	}

	public function responsive()
	{
		$products = $this->productRepository->miscAccessory()->inStockOrHasPending()->getAll();

		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No misc accessory products available');
		}


		$report = new MiscAccessoryReport($this->productRepository);
		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::MISC);

		return view('misc-accessory-report.responsive', compact('report', 'lastUpdatedAt'));
	}
}
