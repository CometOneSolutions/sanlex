@extends('reports.template')
@section('title', 'Misc Accessory')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
    <table class="bordered">
        <thead>
            <tr>
                <th rowspan="2" style="width:30px">TYPE</th>
                <th style="width:30px">W</th>
                <th style="width:30px">L</th>
                @foreach($report->getBackingSides() as $backingSide)
                <th style="width:30px">{{ $backingSide->getName() }}</th>
                @endforeach
            </tr>
            <tr>
                <th style="width:30px">m</th>
                <th style="width:30px">m</th>
                @foreach($report->getBackingSides() as $backingSide)
                <th style="width:30px">ROLLS</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
                <tr>
                    @foreach($report->getMiscAccessoryTypes() as $miscAccessoryType)
                    <td rowspan="{{ $miscAccessoryType->getWidthsLengthsCount()}}"      align="center"  class="no-shade">{{ $miscAccessoryType->getName() }}</td>
                            @foreach($miscAccessoryType->getWidths() as $width)
                            <td align="center" rowspan="{{ $width->getLengthsCount() }}" >{{$width->getName()  }}</td>
                            @foreach($width->getLengths() as $length)
                            <td align="center">{{$length->getName()  }}</td>
                            @foreach($report->getBackingSides() as $backingSide)
                            @php
                            $products = $report->getProducts($miscAccessoryType, $width, $length, $backingSide);
                            @endphp
                                <td align="center"  >
                                    @if($products->count() > 0)
                                    {{ $report->getNumberFormattedStockQuantity($products) }}
                                    @if($report->getNumberFormattedPendingQuantity($products) != '-')
                                    /
                                    <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                                    @endif
                                    @else
                                    -
                                    @endif
                                </td>
                            @endforeach             
                </tr>   
                        @endforeach
                    @endforeach
                @endforeach
            </tbody>
    </table>
@endsection