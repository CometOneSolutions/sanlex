@extends('reports.template-responsive')
@section('title', 'Misc Accessory')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
    <table class="bordered">
        <thead>
            <tr>
                <th rowspan="2" class="fixed no-shade width-md">TYPE</th>
                <th class="fixed no-shade width-sm">W</th>
                <th class="fixed no-shade width-sm">L</th>
                @foreach($report->getBackingSides() as $backingSide)
                <th>{{ $backingSide->getName() }}</th>
                @endforeach
            </tr>
            <tr>
                <th class="fixed no-shade width-sm">m</th>
                <th class="fixed no-shade width-sm">m</th>
                @foreach($report->getBackingSides() as $backingSide)
                <th class="width-lg">ROLLS</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
                <tr>
                    @foreach($report->getMiscAccessoryTypes() as $miscAccessoryType)
                    <td rowspan="{{ $miscAccessoryType->getWidthsLengthsCount()}}"      align="center"  class="fixed no-shade width-md">{{ $miscAccessoryType->getName() }}</td>
                            @foreach($miscAccessoryType->getWidths() as $width)
                            <td align="center" class="fixed width-sm" rowspan="{{ $width->getLengthsCount() }}" >{{$width->getName()  }}</td>
                            @foreach($width->getLengths() as $length)
                            <td align="center" class="fixed width-sm">{{$length->getName()  }}</td>
                            @foreach($report->getBackingSides() as $backingSide)
                            @php
                            $products = $report->getProducts($miscAccessoryType, $width, $length, $backingSide);
                            @endphp
                                <td align="center" class="width-lg">
                                    @if($products->count() > 0)
                                    {{ $report->getNumberFormattedStockQuantity($products) }}
                                    @if($report->getNumberFormattedPendingQuantity($products) != '-')
                                    /
                                    <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                                    @endif
                                    @else
                                    -
                                    @endif
                                </td>
                            @endforeach             
                </tr>   
                        @endforeach
                    @endforeach
                @endforeach
            </tbody>
    </table>
@endsection