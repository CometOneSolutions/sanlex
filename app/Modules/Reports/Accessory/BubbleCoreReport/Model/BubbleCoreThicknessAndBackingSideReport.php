<?php

namespace App\Modules\Reports\Accessory\BubbleCoreReport\Model;

use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Thickness\Models\Thickness;
use Illuminate\Support\Collection;

class BubbleCoreThicknessAndBackingSideReport
{
    protected $thickness;
    protected $backingSide;
    protected $productRepository;
    protected $insulations;

    public function __construct(
        Thickness $thickness,
        BackingSide $backingSide,
        ProductRepository $productRepository
    ) {
        $this->thickness = $thickness;
        $this->backingSide = $backingSide;
        $this->productRepository = $productRepository;
        $this->products = $this->initProducts();
        $this->insulations = $this->initInsulations($this->products);
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;
        return $temp
            ->insulationThickness($this->thickness)
            ->insulationBackingSide($this->backingSide)
            ->getAll();
    }

    protected function initInsulations(Collection $products)
    {
        return $products->map(function ($product) {
            $insulation = $product->productable;
            return $insulation;
        })->unique(function ($insulation) {
            return sprintf('%s %s', $insulation->getWidth()->getName(), $insulation->getLength()->getName());
        });
    }

    public function getInsulations()
    {
        return $this->insulations->sortBy(function ($insulation) {
            return $insulation->getWidth()->getName();
        });
    }

    public function getInsulationsCount()
    {
        return $this->insulations->count();
    }

    public function getName()
    {
        return sprintf('%s%s', $this->thickness->getName(), $this->backingSide->getName());
    }

    public function getThickness()
    {
        return $this->thickness;
    }

    public function getBackingSide()
    {
        return $this->backingSide;
    }
}
