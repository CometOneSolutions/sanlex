<?php

namespace App\Modules\Reports\Accessory\BubbleCoreReport\Model;

use App\Modules\InsulationDensity\Models\InsulationDensity;
use App\Modules\Length\Models\Length;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\Model\AccessoryReport;
use App\Modules\Reports\Traits\HasOrder;
use App\Modules\Width\Models\Width;
use Illuminate\Support\Collection;

class BubbleCoreReport extends AccessoryReport
{
    const INSULATION_TYPE = 'BUBBLE CORE';

    protected $productRepository;
    protected $products;
    protected $thicknessAndBackingSides;
    protected $insulationDensities;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
        $this->products = $productRepository->getAll();

        $this->thicknessAndBackingSides = $this->initThicknessAndBackingSides($this->products);
        $this->insulationDensities = $this->initInsulationDensities($this->products);
    }

    protected function initThicknessAndBackingSides(Collection $products)
    {
        return $products->map(function ($product) {
            $insulation = $product->productable;
            return new BubbleCoreThicknessAndBackingSideReport(
                $insulation->getThickness(),
                $insulation->getBackingSide(),
                $this->productRepository
            );
        })->unique(function ($thicknessAndBackingSide) {
            return $thicknessAndBackingSide->getName();
        });
    }
    protected function initInsulationDensities(Collection $products)
    {
        return $products->map(function ($product) {
            $insulation = $product->productable;
            return $insulation->getInsulationDensity();
        })->unique(function ($insulationDensity) {
            return $insulationDensity->getName();
        });
    }

    public function getThicknessAndBackingSides()
    {
        return $this->thicknessAndBackingSides->sortBy(function ($thicknessAndBackingSide) {
            return HasOrder::getOrderByName($thicknessAndBackingSide->getName(), 'BUBBLE CORE');
        });
    }

    public function getInsulationDensities()
    {
        return $this->insulationDensities->sortBy(function ($insulationDensity) {
            return $insulationDensity->getName();
        });
    }

    public function getProducts(BubbleCoreThicknessAndBackingSideReport $thicknessAndBackingSide, Width $width, Length $length, InsulationDensity $insulationDensity)
    {
        $thickness = $thicknessAndBackingSide->getThickness();
        $backingSide = $thicknessAndBackingSide->getBackingSide();
        $temp = clone $this->productRepository;

        return $temp
            ->insulationThickness($thickness)
            ->insulationBackingSide($backingSide)
            ->insulationWidth($width)
            ->insulationLength($length)
            ->insulationDensity($insulationDensity)
            ->getAll();
    }
}
