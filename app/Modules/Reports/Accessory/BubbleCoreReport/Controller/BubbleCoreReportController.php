<?php

namespace App\Modules\Reports\Accessory\BubbleCoreReport\Controller;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Reports\Traits\HasLastUpdatedAt;
use App\Modules\Reports\Traits\HasInsulationType;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Accessory\BubbleCoreReport\Model\BubbleCoreReport;
use CometOneSolutions\Common\Controllers\Controller;


class BubbleCoreReportController extends Controller
{
	protected $productRepository;

	public function __construct(
		ProductRepository $productRepository
	) {
		$this->productRepository = $productRepository;
		$this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
	}

	public function index()
	{
		$insulationType = HasInsulationType::getInsulationByInsulationTypeName(BubbleCoreReport::INSULATION_TYPE);

		$products = $this->productRepository->insulation()->inStockOrHasPending()->getAll();
		$bubbleCores = $this->productRepository->insulationType($insulationType)->getAll();

		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No insulation products available');
		}

		if ($bubbleCores->isEmpty()) {
			return redirect()->back()->with('status', 'No bubble core products available');
		}

		$report = new BubbleCoreReport($this->productRepository);
		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::INSULATION);
		return view('bubble-core-report.index', compact('report', 'lastUpdatedAt'));
	}

	public function responsive()
	{
		$insulationType = HasInsulationType::getInsulationByInsulationTypeName(BubbleCoreReport::INSULATION_TYPE);

		$products = $this->productRepository->insulation()->inStockOrHasPending()->getAll();
		$bubbleCores = $this->productRepository->insulationType($insulationType)->getAll();

		if ($products->isEmpty()) {
			return redirect()->back()->with('status', 'No insulation products available');
		}

		if ($bubbleCores->isEmpty()) {
			return redirect()->back()->with('status', 'No bubble core products available');
		}

		$report = new BubbleCoreReport($this->productRepository);
		$lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::INSULATION);
		return view('bubble-core-report.responsive', compact('report', 'lastUpdatedAt'));
	}
}
