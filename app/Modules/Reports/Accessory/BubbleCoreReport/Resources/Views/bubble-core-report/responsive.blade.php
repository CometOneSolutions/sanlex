@extends('reports.template-responsive')
@section('title', "Bubble Core")
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
    <table>
        <thead>
            <tr>
                <th rowspan="2" class="fixed no-shade width-md">TYPE</th>
                <th class="fixed no-shade width-sm">W</th>
                <th class="fixed no-shade width-sm">L</th>
                @foreach($report->getInsulationDensities() as $insulationDensity)
                <th class=" no-shade">{{$insulationDensity->getName()}}</th>
                @endforeach
            </tr>
            <tr>
                <th class="fixed no-shade width-sm">m</th>
                <th class="fixed no-shade width-sm">m</th>
                @foreach($report->getInsulationDensities() as $insulationDensity)
                <th class=" no-shade width-lg">ROLLS</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($report->getThicknessAndBackingSides() as $thicknessAndBackingSide)
            <tr>
                <td class="fixed no-shade width-md" align="center" rowspan="{{ $thicknessAndBackingSide->getInsulationsCount() }}"> {{ $thicknessAndBackingSide->getName() }}</td>                                    
                    @foreach($thicknessAndBackingSide->getInsulations() as $insulation)
                    @php
                        $width = $insulation->getWidth();
                        $length = $insulation->getLength();
                    @endphp
                    <td  class="fixed width-sm" align="center"> {{ $width->getName() }}</td>
                    <td  class="fixed width-sm" align="center"> {{  $length->getName() }}</td>
                    @foreach($report->getInsulationDensities() as $insulationDensity)
                        @php
                        $products = $report->getProducts($thicknessAndBackingSide, $width, $length, $insulationDensity)
                        @endphp
                             <td align="center" class="width-lg">
                                @if($products->count() > 0)
                                {{ $report->getNumberFormattedStockQuantity($products, $type = 'Insulation') }}
                                @if($report->getNumberFormattedPendingQuantity($products, $type = 'Insulation') != '-')
                                /
                                <span class="red">{{ $report->getNumberFormattedPendingQuantity($products, $type = 'Insulation') }}</span>
                                @endif
                                @else
                                -
                                @endif
                            </td>
                        @endforeach             
            </tr>   
                @endforeach
            @endforeach
        </tbody>
    </table>
@endsection