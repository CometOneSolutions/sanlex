@extends('reports.template')
@section('title', 'Bubble Core Insulation')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
    <table class="bordered">
        <thead>
            <tr>
                <th rowspan="2" style="width:30px">TYPE</th>
                <th style="width:30px">W</th>
                <th style="width:30px">L</th>
                @foreach($report->getInsulationDensities() as $insulationDensity)
                <th style="width:30px">{{$insulationDensity->getName()}}</th>
                @endforeach
            </tr>
            <tr>

                <th style="width:30px">m</th>
                <th style="width:30px">m</th>
                @foreach($report->getInsulationDensities() as $insulationDensity)
                <th style="width:30px">ROLLS</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($report->getThicknessAndBackingSides() as $thicknessAndBackingSide)
            <tr>
                <td class="no-shade" align="center" rowspan="{{ $thicknessAndBackingSide->getInsulationsCount() }}"> {{ $thicknessAndBackingSide->getName() }}</td>                                    
                    @foreach($thicknessAndBackingSide->getInsulations() as $insulation)
                    @php
                        $width = $insulation->getWidth();
                        $length = $insulation->getLength();
                    @endphp
                    <td  align="center"> {{ $width->getName() }}</td>
                    <td  align="center"> {{  $length->getName() }}</td>
                    @foreach($report->getInsulationDensities() as $insulationDensity)
                        @php
                        $products = $report->getProducts($thicknessAndBackingSide, $width, $length, $insulationDensity)
                        @endphp
                             <td align="center"  >
                                @if($products->count() > 0)
                                {{ $report->getNumberFormattedStockQuantity($products, $type = 'Insulation') }}
                                @if($report->getNumberFormattedPendingQuantity($products, $type = 'Insulation') != '-')
                                /
                                <span class="red">{{ $report->getNumberFormattedPendingQuantity($products, $type = 'Insulation') }}</span>
                                @endif
                                @else
                                -
                                @endif
                            </td>
                        @endforeach             
            </tr>   
                @endforeach
            @endforeach
        </tbody>
    </table>
@endsection