<?php

namespace App\Modules\Reports\Accessory\PvcFittingReport\Model;

use App\Modules\Brand\Models\Brand;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\PvcDiameter\Models\PvcDiameter;
use App\Modules\Reports\Accessory\Model\AccessoryReport;
use App\Modules\Reports\Traits\HasOrder;
use Illuminate\Support\Collection;

class PvcFittingReport extends AccessoryReport
{
    protected $productRepository;
    protected $products;
    protected $pvcTypes;
    protected $pvcDiameters;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository->pvcFitting()->inStockOrHasPending();
        $this->products = $this->productRepository->getAll();
        $this->pvcTypes = $this->initPvcTypes($this->products);
        $this->pvcDiameters = $this->initPvcDiameters($this->products);
    }

    protected function initPvcTypes(Collection $products)
    {
        return  $products->map(function ($product) {
            $pvcFitting = $product->productable;
            return new PvcTypeReport($pvcFitting->getPvcType(), $this->productRepository);
        })->unique(function ($pvcType) {
            return $pvcType->getName();
        });
    }

    protected function initPvcDiameters(Collection $products)
    {
        return  $products->map(function ($product) {
            $pvcFitting = $product->productable;
            return $pvcFitting->getPvcDiameter();
        })->unique(function ($pvcDiameter) {
            return $pvcDiameter->getName();
        });
    }

    public function getPvcTypes()
    {
        return $this->pvcTypes->sortBy(function ($pvcType) {
            return HasOrder::getOrderByName($pvcType->getName(), 'PVC FITTING');
        });
    }

    public function getPvcDiameters()
    {
        return $this->pvcDiameters->sortBy(function ($pvcDiameter) {
            return substr($pvcDiameter->getName(), 0, -1);
        });
    }

    public function getProducts(PvcTypeReport $pvcType, Brand $brand, PvcDiameter $pvcDiameter)
    {
        $temp = clone $this->productRepository;
        return $temp
            ->pvcType($pvcType->getPvcType())
            ->pvcFittingBrand($brand)
            ->pvcDiameter($pvcDiameter)
            ->getAll();
    }
}
