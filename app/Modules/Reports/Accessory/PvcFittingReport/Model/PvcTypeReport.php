<?php

namespace App\Modules\Reports\Accessory\PvcFittingReport\Model;


use App\Modules\Product\Repositories\EloquentProductRepository;
use App\Modules\PvcType\Models\PvcType;

class PvcTypeReport
{
    protected $name;
    protected $pvcType;
    protected $pvcFittingBrands;
    protected $productRepository;

    public function __construct(
        PvcType $pvcType,
        EloquentProductRepository $productRepository
    ) {
        $this->pvcType = $pvcType;
        $this->name = $this->pvcType->getName();
        $this->productRepository = $productRepository;
        $this->products = $this->initProducts();
        $this->pvcFittingBrands = $this->initPvcFittingBrands($this->pvcType);
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;
        return $temp
            ->pvcType($this->getPvcType())
            ->getAll();
    }

    public function initPvcFittingBrands(PvcType $pvcType)
    {
        return $this->products->map(function ($pvcFittingBrand) {
            $pvcFitting = $pvcFittingBrand->productable;
            return $pvcFitting->getBrand();
        })->unique(function ($pvcFittingBrand) {
            return $pvcFittingBrand->getName();
        });
    }

    public function getPvcFittingBrands()
    {
        return $this->pvcFittingBrands->sortBy(function ($pvcFittingBrand) {
            return $pvcFittingBrand->getName();
        });
    }

    public function getPvcFittingBrandsCount()
    {
        return $this->pvcFittingBrands->count();
    }

    public function getPvcType()
    {
        return $this->pvcType;
    }

    public function getName()
    {
        return  $this->name;
    }
}
