<?php

namespace App\Modules\Reports\Accessory\PvcFittingReport\Controller;

use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Reports\Traits\HasLastUpdatedAt;
use App\Modules\Reports\Accessory\PvcFittingReport\Model\PvcFittingReport;
use CometOneSolutions\Common\Controllers\Controller;

class PvcFittingReportController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
        $this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
    }

    public function index()
    {
        $products = $this->productRepository->pvcFitting()->inStockOrHasPending()->getAll();

        if ($products->isEmpty()) {
            return redirect()->back()->with('status', 'No pvc fittings products available');
        }

        $report = new PvcFittingReport($this->productRepository);
        $lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::PVC);

        return view('pvc-fitting-report.index', compact('report', 'lastUpdatedAt'));
    }

    public function responsive()
    {
        $products = $this->productRepository->pvcFitting()->inStockOrHasPending()->getAll();

        if ($products->isEmpty()) {
            return redirect()->back()->with('status', 'No pvc fittings products available');
        }

        $report = new PvcFittingReport($this->productRepository);
        $lastUpdatedAt = HasLastUpdatedAt::getByProductTypeName(PurchaseOrder::PVC);

        return view('pvc-fitting-report.responsive', compact('report', 'lastUpdatedAt'));
    }
}
