@extends('reports.template-responsive')
@section('title', 'Pvc Fitting')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
<table>
    <thead>
        <tr>
            <th rowspan="2"  class="width-md fixed no-shade" >TYPE</th>
            <th rowspan="2"  class="width-md fixed no-shade" >BRAND</th>
            <th colspan="{{$report->getPvcDiameters()->count()}}">DIAMETER</th>
        </tr>
        <tr>
            @foreach($report->getPvcDiameters() as $pvcDiameter)
                <th class="width-sm">{{$pvcDiameter->getName()}}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        <tr>
        @foreach($report->getPvcTypes() as $pvcType)
            <td  rowspan="{{ $pvcType->getPvcFittingBrandsCount() }}"   align="center"  class="fixed no-shade width-md">{{ $pvcType->getName() }} </td>
            @foreach($pvcType->getPvcFittingBrands() as $pvcFittingBrand)
                <td    align="center" class="fixed width-md" >{{ $pvcFittingBrand->getName() }}</td>
                @foreach($report->getPvcDiameters() as $pvcDiameter)
                    @php
                    $products = $report->getProducts($pvcType, $pvcFittingBrand, $pvcDiameter)
                    @endphp
                    <td align="center"  class="width-sm">
                        @if($products->count() > 0)
                        {{ $report->getNumberFormattedStockQuantity($products) }}
                        @if($report->getNumberFormattedPendingQuantity($products) != '-')
                        /
                        <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                        @endif
                        @else
                        -
                        @endif
                    </td>
                @endforeach
            </tr>
            @endforeach
        @endforeach
    </tbody>
</table>
@endsection