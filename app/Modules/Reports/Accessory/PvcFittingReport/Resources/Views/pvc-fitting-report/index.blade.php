@extends('reports.template')
@section('title', 'Pvc Fitting')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
<table class="bordered">
    <thead class="bordered">
        <tr>
            <th style="width: 100px;" rowspan="2"  rowspan="2" class="fixed">TYPE</th>
            <th style="width: 100px;" rowspan="2"  rowspan="2" class="fixed">BRAND</th>
            <th style="width: 80px;" colspan="{{$report->getPvcDiameters()->count()}}" class="fixed">DIAMETER</th>
        </tr>
        <tr>
            @foreach($report->getPvcDiameters() as $pvcDiameter)
                <th style="width: 80px;">{{$pvcDiameter->getName()}}</th>
            @endforeach
        </tr>
    </thead>
    <tbody class="bordered">
        <tr>
        @foreach($report->getPvcTypes() as $pvcType)
            <td  rowspan="{{ $pvcType->getPvcFittingBrandsCount() }}"   align="center"  class="fixed no-shade">{{ $pvcType->getName() }} </td>
            @foreach($pvcType->getPvcFittingBrands() as $pvcFittingBrand)
                <td    align="center" class="fixed" >{{ $pvcFittingBrand->getName() }}</td>
                @foreach($report->getPvcDiameters() as $pvcDiameter)
                    @php
                    $products = $report->getProducts($pvcType, $pvcFittingBrand, $pvcDiameter)
                    @endphp
                    <td align="center"  >
                        @if($products->count() > 0)
                        {{ $report->getNumberFormattedStockQuantity($products) }}
                        @if($report->getNumberFormattedPendingQuantity($products) != '-')
                        /
                        <span class="red">{{ $report->getNumberFormattedPendingQuantity($products) }}</span>
                        @endif
                        @else
                        -
                        @endif
                    </td>
                @endforeach
            </tr>
            @endforeach
        @endforeach
    </tbody>
</table>
@endsection