<?php

namespace App\Modules\Reports\Aging\AccountsReceivableAging\Models;

use Carbon\Carbon;

class AccountsReceivableAgingReportPeriod
{
    protected $name;
    protected $startDayCount;
    protected $endDayCount;
    protected $startDate;
    protected $endDate;

    public function __construct(int $startDayCount, int $endDayCount = null)
    {
        $this->startDayCount = $startDayCount;
        $this->endDayCount = $endDayCount;
        $this->name = $this->assembleName($startDayCount, $endDayCount);
        $this->startDate = $this->computeStartDate($endDayCount);
        $this->endDate = $this->computeEndDate($startDayCount);
    }

    protected function computeStartDate(int $endDayCount = null)
    {
        return $endDayCount !== null ? now()->subDays($endDayCount) : Carbon::minValue();
    }

    protected function computeEndDate(int $startDayCount)
    {
        return now()->subDays($startDayCount);
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function getStartDayCount()
    {
        return $this->startDayCount;
    }

    public function getEndDayCount()
    {
        return $this->endDayCount;
    }

    public function getName()
    {
        return $this->name;
    }

    protected function assembleName(int $startDayCount, int $endDayCount = null)
    {
        return sprintf(
            '%s%s Days',
            $startDayCount,
            $endDayCount !== null ? '-' . $endDayCount : '+'
        );
    }
}
