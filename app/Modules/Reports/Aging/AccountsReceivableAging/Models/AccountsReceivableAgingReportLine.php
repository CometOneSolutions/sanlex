<?php

namespace App\Modules\Reports\Aging\AccountsReceivableAging\Models;

use App\Modules\Customer\Models\Customer;

class AccountsReceivableAgingReportLine
{
    protected $customer;
    protected $periods;
    protected $cells = [];
    protected $total = 0;

    public function __construct(Customer $customer, array $periods)
    {
        $this->customer = $customer;
        $this->periods = $periods;
        $this->initialize();
    }

    protected function initialize()
    {
        foreach ($this->periods as $period) {
            $this->cells[] = new AccountsReceivableAgingReportCell($this, $period);
        }
        $this->total = array_reduce($this->cells, function ($carry, $cell) {
            return $carry + $cell->getAmount();
        }, 0);
    }

    public function getCustomerName()
    {
        return $this->customer->getName();
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function getCells()
    {
        return collect($this->cells);
    }

    public function getTotal()
    {
        return $this->total;
    }
}
