<?php

namespace App\Modules\Reports\Aging\AccountsReceivableAging\Models;

class AccountsReceivableAgingReportCell
{
    protected $period;
    protected $amount;
    protected $line;

    public function __construct(
        AccountsReceivableAgingReportLine $line,
        AccountsReceivableAgingReportPeriod $period
    ) {
        $this->period = $period;
        $this->amount = $this->computeAmount($line, $period);
    }

    public function computeAmount(AccountsReceivableAgingReportLine $line, AccountsReceivableAgingReportPeriod $period)
    {
        $customer = $line->getCustomer();
        // TODO Try not to be too Laravel Specific;
        return $customer->sales()
            ->withBalance()
            ->from($period->getStartDate())
            ->to($period->getEndDate())
            ->sum('balance_amount');
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getPeriod()
    {
        return $this->period;
    }
}
