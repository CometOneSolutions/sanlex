<?php

namespace App\Modules\Reports\Aging\AccountsReceivableAging\Models;

use App\Modules\Customer\Repositories\CustomerRepository;
use Carbon\Carbon;

class AccountsReceivableAgingReport
{
    protected $customers;
    protected $periods;
    protected $lines = [];

    public function __construct(CustomerRepository $customers, array $periods = [])
    {
        // $this->date = Carbon::instance($date)->setTimezone('Asia/Manila');
        $this->customers = $customers;
        $this->periods = $periods;
        $this->lines = $this->makeLines($customers, $periods);
    }

    protected function makeLines(CustomerRepository $customers, array $periods = [])
    {
        return $customers->getAll()->map(function ($customer) use ($customers, $periods) {
            return new AccountsReceivableAgingReportLine($customer, $periods);
        });


    }

    public function getPeriods()
    {
        return collect($this->periods);

        // ->sortByDesc(function ($line) {
        //     return $line->getCustomer()->getName();
        // });
    }

    public function getLines()
    {
        return collect($this->lines);

        // ->sortByDesc(function ($line) {
        //     return $line->getCustomer()->getName();
        // });
    }
}
