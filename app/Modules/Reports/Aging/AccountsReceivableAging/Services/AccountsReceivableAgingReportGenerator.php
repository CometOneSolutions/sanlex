<?php

namespace App\Modules\Reports\Aging\AccountsReceivableAging\Services;

use App\Modules\Customer\Repositories\CustomerRepository;
use App\Modules\Reports\Aging\AccountsReceivableAging\Models\AccountsReceivableAgingReport;
use App\Modules\Reports\Aging\AccountsReceivableAging\Models\AccountsReceivableAgingReportPeriod;
use Carbon\Carbon;

class AccountsReceivableAgingReportGenerator
{
    protected $customers;
    protected $periods = [];

    public function __construct(CustomerRepository $customers)
    {
        $this->customers = $customers;
        $this->initializePeriods();
    }

    protected function initializePeriods()
    {
        $this->periods[] = new AccountsReceivableAgingReportPeriod(0, 30);
        $this->periods[] = new AccountsReceivableAgingReportPeriod(31, 60);
        $this->periods[] = new AccountsReceivableAgingReportPeriod(61, 90);
        $this->periods[] = new AccountsReceivableAgingReportPeriod(91);
    }

    public static function generate()
    {
        $self = resolve(self::class);
        return new AccountsReceivableAgingReport($self->customers, $self->periods);
    }

    public static function generateFileName()
    {
        $format = 'AccountsReceivableAging%s.xls';
        $current = Carbon::now()->format('YmdHs');
        return sprintf($format, $current);
    }
}
