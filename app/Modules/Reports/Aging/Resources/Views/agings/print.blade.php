<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    
    <style>
    @media print {
        .table-sale-content tr:nth-child(24) {
            
        }
    } 
    #pageNumber { content: counter(page) }
    @page { counter-increment: page }

    .center {
        text-align: center;
    } 
    .sale {
        max-width: 800px;
        margin: auto;
        padding: 30px;
      
        font-size: 14px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #000;
    }
    .center-align-text {
        text-align: center;
    }
    .right-align-text {
        text-align: right;
    }
    .left-align-text {
        text-align: left;
    }
    .image-header {
        width: 100%;
        margin-top: 40px;
        margin-bottom: 20px;
    }
    .uppercase {
        text-transform: uppercase;
    }
    .delivery-receipt {
        font-size: 1.6em;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    .table-sale-header {
        border-collapse: collapse;
        width: 100%;
    }
    .table-sale-content {
        padding: 2px;
        border-collapse: collapse;
        width: 100%;
    }
    .table-sale-content tr > td {
        padding: 2px;
    }

    
    </style>
</head>

<body>
    
    
    <div class="sale">
       
        <center><h3 class="uppercase">ACCOUNTS RECEIVABLE AGING</h3></center>
        <table class="table-sale-content" border="1" style="margin-top: 10px;">
            <tr>
                <th class="left-align-text"><br>Customer</th>
                <th class="right-align-text"><br>Total A/R</th>
                @foreach($aging->getPeriods() as $period)
                    <th class="right-align-text">{{$period->getName()}}</th>
                @endforeach
                
            </tr>
            @foreach($aging->getLines() as $line)
            <tr>
                <td class="left-align-text">{{$line->getCustomerName()}}</td>
                <td class="right-align-text">{{number_format($line->getTotal(), 2)}}</td>
                @foreach($line->getCells() as $cell)
                    <td class="right-align-text">{{number_format($cell->getAmount(), 2)}}</td>
                @endforeach
            </tr>
            @endforeach
        </table>

    </div>
    
</body>
</html>