<?php

Route::group(['prefix' => 'sales'], function () {
    Route::get('/', 'SalesMenuDashboardController@dashboard')->name('sales-menu-dashboard_index');

    Route::group(['prefix' => 'reports'], function () {
        Route::get('/aging/print', 'AgingReportController@print')->name('reports_aging_print');
    });

});