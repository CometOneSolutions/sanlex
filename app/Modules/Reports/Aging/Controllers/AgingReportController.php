<?php

namespace App\Modules\Reports\Aging\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\Reports\Aging\AccountsReceivableAging\Services\AccountsReceivableAgingReportGenerator;

class AgingReportController extends Controller
{
    // private $saleRecordService;

    // public function __construct()
    // {
    //     $this->saleRecordService = $saleRecordService;
    // }

    public function print()
    {
        $aging = AccountsReceivableAgingReportGenerator::generate();
        return view('agings.print', compact('aging'));
    }
}
