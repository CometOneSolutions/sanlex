<?php

namespace App\Modules\Reports\Coil\Export;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use CometOneSolutions\Common\Utils\Export;
use Maatwebsite\Excel\Concerns\WithEvents;

const ALPHABET = array(
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
    'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
    'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ'
);

class ThicknessExport extends Export implements FromView, WithTitle, WithEvents
{
    protected $view;
    protected $data;
    protected $range;
    protected $topRange;
    protected $leftRange;

    public function __construct($data, $view)
    {
        $this->data = $data;
        $this->view = $view;
        $this->getEndOfLine();
    }

    public function view(): View
    {

        return view($this->view, ["report" => $this->data]);
    }

    public function title(): String
    {
        return $this->data->getThicknessName();
    }

    public function registerEvents(): array
    {

        $range = $this->range;
        $left = $this->leftRange;
        $top = $this->topRange;

        return [
            AfterSheet::class    => function (AfterSheet $event) use ($range, $top, $left) {
                // All headers - set font size to 14
                // $cellRange = 'A1:W1';
                $event->sheet->getDelegate()->getStyle($top)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($left)->getFont()->setBold(true);

                // Apply array of styles to B2:G8 cell range
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],

                    ],
                    'font' => [
                        'name' => 'Arial'
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ];

                $event->sheet->getDelegate()->getStyle($range)->applyFromArray($styleArray);

                // Set first row to height 20
                // $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(20);


                $event->sheet->getDelegate()->getStyle($this->range)
                    ->getAlignment()->setWrapText(true);
            },
        ];
    }
    public function getEndOfLine()
    {
        $column = 0;
        foreach ($this->data->getColorColumns() as $colorColumn) {

            foreach ($colorColumn->getColorColumnWidths() as $colorColumnWidth) {
                $column += 2;
            }
        }

        $row = 0;


        foreach ($this->data->getSupplierRows() as $supplierRow) {

            for ($i = 0; $i < $productCount = $supplierRow->getMaxProductCount(); $i++) {
                if ($i == 0) {
                    $row += $productCount;
                    break;
                } else {
                    $row++;
                    break;
                }
            }
        }


        $this->range = 'A1:' . ALPHABET[$column] . ($row + 3);
        sprintf('A1:%s%s', ALPHABET[$column], $row + 3);
        $this->topRange = sprintf('A1:%s1', ALPHABET[$column]);
        $this->leftRange = sprintf('A1:A%s', $row + 3);
    }
}
