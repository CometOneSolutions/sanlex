@extends('reports.template-responsive')
@section('title', 'Damaged Coil')
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
<table>
    <thead>
        <tr>
            <th class="fixed no-shade width-md"></th>
            <th class="fixed no-shade width-md"></th>
            <th class="fixed no-shade width-md"></th>
            @foreach($report->getThicknessAndWidths() as $thicknessAndWidth)
            <th class="width-md">{{ $thicknessAndWidth->getName() }}</th>
            @endforeach
        </tr>

    </thead>
    <tbody>
        @foreach($report->getSuppliers() as $supplier)
        <tr>
            <td class="fixed no-shade width-md" align="center" rowspan="{{ $supplier->getColorsCount() }}"> {{ $supplier->getShortCode() }}</td>
            @foreach($supplier->getColors() as $color)
            <td class=" fixed no-shade width-md" align="center" rowspan="{{ $color->getCoatingsCount() }}"> {{ $color->getName() }}</td>
            @foreach($color->getCoatings() as $coating)
            <td class=" fixed width-md" align="center"> {{ $coating->getName() }}</td>
            @foreach($report->getThicknessAndWidths() as $thicknessAndWidth)

            @php
            $product = $report->getProduct($supplier, $color, $thicknessAndWidth, $coating);
            $coilNumbers = $report->getCoilNumbers($supplier, $color, $thicknessAndWidth, $coating);
            $index =sprintf('%d%d%d%d%d',$supplier->getSupplier()->getId(), $color->getColor()->getId(), $thicknessAndWidth->getThickness()->getId(), $thicknessAndWidth->getWidth()->getId(), $coating->getId());
            @endphp
            @if($product)
            <td align=" center" class="width-md ">
                <div onclick="openModal({{ $index }})" style=" cursor: pointer;">
                    {{ $product->getFormattedBadStockQuantity() }}
                </div>
                <div id="{{ $index }}" class="modal">
                    <div class="modal-content">
                        <div onclick="closeModal({{ $index }})" class="close">&times;</div>

                        <h4>
                            DAMAGED COIL NUMBERS
                        </h4>

                        @foreach($coilNumbers as $coilNumber)
                        <a href="{{ route('inventory_damaged_index', ['damaged_coil_number_or_slit_identifier' => $coilNumber]) }}" target=_blank style="padding: 0px 10px"> {{ $coilNumber }}</a>
                        @endforeach
                    </div>
                </div>
            </td>
            @else
            <td align="center" class="width-md ">
                --
            </td>
            @endif
            @endforeach
        </tr>
        @endforeach
        @endforeach
        @endforeach
    </tbody>
</table>
@endsection
