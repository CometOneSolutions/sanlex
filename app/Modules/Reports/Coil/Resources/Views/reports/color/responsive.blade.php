@extends('reports.template-responsive')
@section('title', $report->getColorName())
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())
@section('content')
<table>
    <thead>
        <tr>
            <th class="fixed no-shade width-lg"></th>
            @foreach($report->getThicknessWidths() as $thicknessWidth)
            <th align="center" class="width-md">
                {{ $thicknessWidth->getName() }}
            </th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($report->getSupplierCoatings() as $supplierCoating)
        <tr>
            <td align="center" class="fixed width-lg">{{ $supplierCoating->getName() }}</td>
            @foreach($report->getThicknessWidths() as $thicknessWidth)
            <div hidden>
                {{ $product = $thicknessWidth->getProduct($supplierCoating->getSupplier(), $supplierCoating->getCoating()) }}
            </div>
            <td align="center" class="width-md">
                @if($product)
                    {{ $product->getFormattedStockQuantity() }}
                    @foreach($product->purchaseOrderDetails()->inTransit()->get() as $purchaseOrderDetail)
                        /
                        <span style="color: {{$colors[$purchaseOrderDetail->purchase_order_id%count($colors)]}}">
                            {{ number_format($purchaseOrderDetail->ordered_quantity) }}
                        </span>
                    @endforeach
                @else
                    --
                @endif

            </td>
            @endforeach
        </tr>
        @endforeach
        <tr>
            <td align="center" class="fixed "><b>T O T A L</b></td>
            @foreach($report->getThicknessWidths() as $thicknessWidth)
            <td align="center">
                <b>{{ $thicknessWidth->getFormattedStockQuantity() }}</b>
                @foreach($thicknessWidth->getPendingPurchaseOrderDetails() as $purchaseOrderDetail)
                    /
                    <span style="font-weight: bold; color: {{$colors[$purchaseOrderDetail->purchase_order_id%count($colors)]}}">
                        {{ number_format($purchaseOrderDetail->ordered_quantity) }}
                    </span>
                @endforeach
            </td>
            @endforeach
        </tr>
    </tbody>
</table>
@endsection
