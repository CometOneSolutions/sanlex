@extends('app')
@section('breadcrumbs', Breadcrumbs::render('report-color.summary'))
@section('content')
<div class="card table-responsive">

    <div class="card-body">
        <div class="card" class="col-md-6">
            <div class="card-header">
                Available Coil Colors (In Stock / Pending)
            </div>
            @if($colors->count() > 0)
            <ul class="list-group list-group-flush">
                @foreach($colors as $color)
                <li class="list-group-item">
                    <a target="_blank" href="{{route('reports_coil_color-print', $color->getId())}}"><span style="font-size:1.3em; font-weight: bold;">{{strtoupper($color->getName())}}</span></a>
                </li>
                @endforeach
                <li class="list-group-item">
                    <a target="_blank" href="{{route('reports_coil_slitted-index', 'GI')}}"><span style="font-size:1.3em; font-weight: bold;">* SLITTED INVENTORY GI</span></a>
                </li>
                <li class="list-group-item">
                    <a target="_blank" href="{{route('reports_coil_slitted-index', 'COLORED')}}"><span style="font-size:1.3em; font-weight: bold;">* SLITTED INVENTORY COLORED</span></a>
                </li>
                <li class="list-group-item">
                    <a target="_blank" href="{{route('reports_coil_slitted-index', 'STAINLESS STEEL')}}"><span style="font-size:1.3em; font-weight: bold;">* SLITTED STAINLESS STEEL</span></a>
                </li>
            </ul>
            <br>
            <br>
            @else
            <div class="text-center">No items to display.</div>
            @endif
        </div>
    </div>
</div>
@stop