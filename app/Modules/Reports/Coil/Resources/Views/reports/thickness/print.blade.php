<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <title>Thickness</title>


    <style type="text/css">
        html {
            display: table;
            margin-left: 10px;
        }

        body {
            margin: 0 auto;
            font-size: 10px;
            width: 860px;
            font-family: 'Open Sans', sans-serif;
            margin-left: 10px;
            margin-right: 20px;
            background-color: white;
        }

        .red {
            color: red;
        }

        .gray {
            background: #CCC
        }

        .table-main {
            border-collapse: collapse;
            padding: 3px;
            width: 100%;
        }

        .text-center {
            text-align: center;
        }

        @media screen {
            html {
                width: 90%;
                /* margin: auto; */
            }

            body {
                /* margin: auto; */
                font-size: 20px;
                width: 100%;
                font-family: 'Open Sans', sans-serif;
                background-color: white;
            }

            .statement-left {
                font-size: 14px;
            }

            .statement-right {
                font-size: 14px;
            }
        }

        @media print {


            button {
                display: none;
            }

            body {
                margin: 0;
            }
        }
    </style>

</head>
<br><br><br>



<body>
    <h1 style="float:left;">{{$report->getThicknessName()}}</h1>
    <div style="float:right; margin-top: 30px">Updated as of: {{$lastUpdatedAt->toDayDateTimeString()}}</div>
    <div style="clear:both;"></div>

    <table class="table-main text-center" border=1>
        <thead>
            <tr>
                <th>&nbsp;</th>
                @foreach($report->getColorColumns() as $colorColumn)
                <th colspan="{{ count($colorColumn->getColorColumnWidths()) * 2 }}" style="text-align: center">
                    <strong>{{ $colorColumn->getColor()->getName() }}</strong>
                </th>
                @endforeach
            </tr>
            <tr>
                <th>&nbsp;</th>
                @foreach($report->getColorColumns() as $colorColumn)
                @foreach($colorColumn->getColorColumnWidths() as $colorColumnWidth)
                <th colspan="2">
                    {{ $colorColumnWidth->getWidth()->getName() }}
                </th>
                @endforeach
                @endforeach
            </tr>
            <tr>
                <th>TOTAL</th>
                @foreach($report->getColorColumns() as $colorColumn)

                @foreach($colorColumn->getColorColumnWidths() as $colorColumnWidth)

                <th colspan="2">

                    {{ $colorColumnWidth->getFormattedStockQuantity() }}
                    @if($colorColumnWidth->getFormattedPendingQuantity() != '-')
                    /
                    <span class="red">{{ $colorColumnWidth->getFormattedPendingQuantity() }}</span>
                    @endif
                </th>
                @endforeach
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($report->getSupplierRows() as $supplierRow)
            @for($i = 0; $i < $rows=$supplierRow->getMaxProductCount(); $i++ )
                <tr>
                    @if($i == 0)
                    <td rowspan="{{ $rows }}">
                        <strong>{{ $supplierRow->getSupplier()->getShortCode() }}</strong>
                    </td>
                    @endif
                    @foreach($supplierRow->getColorColumns() as $colorColumn)
                    @foreach($colorColumn->getColorColumnWidths() as $colorColumnWidth)

                    <div hidden>

                        {{ $product = $colorColumnWidth->getSupplierProductAtIndex($supplierRow->getSupplier(), $i) }}</div>

                    @if($product)
                    <td>{{ $product->getFormattedStockQuantity() }}
                        @if($product->getFormattedPendingQuantity() != '-')
                        /
                        <span class="red">{{ $product->getFormattedPendingQuantity() }}</span>
                        @endif
                    </td>
                    <td class="gray">{{ $product->getProductable()->getCoating()->getName() }}</td>
                    @else
                    <td></td>
                    <td class="gray"></td>
                    @endif
                    @endforeach
                    @endforeach
                </tr>
                @endfor
                @endforeach
        </tbody>

    </table>
</body>

</html>