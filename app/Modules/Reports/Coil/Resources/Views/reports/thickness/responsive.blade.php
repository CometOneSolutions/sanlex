@extends('reports.template-responsive')
@section('title', $report->getThicknessName())
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')

    <style>
        tbody>tr:nth-child(even) td {
            background-color: #FFFFFF
            
        }
        tbody>tr:nth-child(odd) td {
            background-color: #FFFFFF
        }
    </style>

<table >
    <thead>
        <tr>
            <th class="no-shade width-md fixed">&nbsp;</th>
            @foreach($report->getColorColumns() as $colorColumn)
            <th class="no-shade" colspan="{{ count($colorColumn->getColorColumnWidths()) }}">
                <strong>{{ $colorColumn->getColor()->getName() }}</strong>
            </th>
            @endforeach
        </tr>
        <tr>
            <th class="no-shade width-sm fixed">&nbsp;</th>
            @foreach($report->getColorColumns() as $colorColumn)
                @foreach($colorColumn->getColorColumnWidths() as $colorColumnWidth)
                <th class="no-shade width-md">
                    {{ $colorColumnWidth->getWidth()->getName() }}
                </th>
                @endforeach
            @endforeach
        </tr>
        <tr>
            <th class="fixed no-shade width-md">TOTAL</th>
            @foreach($report->getColorColumns() as $colorColumn)

            @foreach($colorColumn->getColorColumnWidths() as $colorColumnWidth)

            <th  class="no-shade width-lg">

                {{ $colorColumnWidth->getFormattedStockQuantity() }}
                @if($colorColumnWidth->getFormattedPendingQuantity() != '-')
                /
                <span class="red">{{ $colorColumnWidth->getFormattedPendingQuantity() }}</span>
                @endif
            </th>
            @endforeach
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($report->getSupplierRows() as $supplierRow)
            @for($i = 0; $i < $rows=$supplierRow->getMaxProductCount(); $i++ )
            <tr>
                @if($i == 0)
                <td class="fixed width-md no-shade" rowspan="{{ $rows }}" style="text-align:center">
                    <strong>{{ $supplierRow->getSupplier()->getShortCode() }}</strong>
                </td>
                @endif
                @foreach($supplierRow->getColorColumns() as $colorColumn)
                    @foreach($colorColumn->getColorColumnWidths() as $colorColumnWidth)
                        @php $product = $colorColumnWidth->getSupplierProductAtIndex($supplierRow->getSupplier(), $i) @endphp

                        @if($product)
                        <td class="no-shade width-lg">{{ $product->getFormattedStockQuantity() }}
                            @if($product->getFormattedPendingQuantity() != '-')
                            /
                            <span class="red">{{ $product->getFormattedPendingQuantity() }}</span>
                            @endif

                            <div style="width: 50%; background-color: #CCC; float: right; text-align:right">{{ $product->getProductable()->getCoating()->getName() }}</div>
                        </td>
                   
                        @else
                        <td class="width-lg">
                        </td>
                        @endif
                    @endforeach
                @endforeach
            </tr>
            @endfor
        @endforeach
     

    </tbody>
</table>
@endsection