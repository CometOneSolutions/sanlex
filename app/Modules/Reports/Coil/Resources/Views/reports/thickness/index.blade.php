@extends('app')
@section('breadcrumbs', Breadcrumbs::render('report-thickness.summary'))
@section('content')
<div class="card table-responsive">

    <div class="card-body">
        <div class="card" class="col-md-6">
            <div class="card-header">
                Thickness
            </div>
            @if($thicknesses->count() > 0)
            <ul class="list-group list-group-flush">
                @foreach($thicknesses as $item)
                <li class="list-group-item">
                    <a target="_blank"
                        href="{{route('reports_coil_thickness-print', $item->id)}}"><span
                            style="font-size:1.3em; font-weight: bold;">{{strtoupper($item->getName())}}</span>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="float-right" style="font-size:1.3em; font-weight: bold;" href="{{route('reports_coil_thickness-export', $item->id)}}"><i class="far fa-file-excel"></i></a>
                </li>
                @endforeach
            </ul>
            @else
            <div class="text-center">No items to display.</div>
            @endif
        </div>
    </div>
</div>
@stop