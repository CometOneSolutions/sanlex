<table class="table-main text-center">
    <thead>
        <tr> 
            <th>&nbsp;</th>
            @foreach($report->getColorColumns() as $colorColumn)
            <th colspan="{{ count($colorColumn->getColorColumnWidths()) * 2 }}" style="text-align: center">
                <strong>{{ $colorColumn->getColor()->getName() }}</strong>
            </th>
            @endforeach
        </tr>
        <tr>
            <th>&nbsp;</th>
            @foreach($report->getColorColumns() as $colorColumn)
            @foreach($colorColumn->getColorColumnWidths() as $colorColumnWidth)
            <th colspan="2">
                {{ $colorColumnWidth->getWidth()->getName() }}
            </th>
            @endforeach
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($report->getSupplierRows() as $supplierRow)
            @for($i = 0; $i < $rows=$supplierRow->getMaxProductCount(); $i++ )
            <tr>
                @if($i == 0)
                <td rowspan="{{ $rows }}">
                    <strong>{{ $supplierRow->getSupplier()->getShortCode() }}</strong>
                </td>
                @endif
                @foreach($supplierRow->getColorColumns() as $colorColumn)
                    @foreach($colorColumn->getColorColumnWidths() as $colorColumnWidth)
                        
                            @if($colorColumnWidth->getSupplierProductAtIndex($supplierRow->getSupplier(), $i))
                            <td>{{ $colorColumnWidth->getSupplierProductAtIndex($supplierRow->getSupplier(), $i)->getFormattedStockQuantity() }} / {{ $colorColumnWidth->getSupplierProductAtIndex($supplierRow->getSupplier(), $i)->getFormattedPendingQuantity() }}</td>
                            <td style="background-color:#C0C0C0">{{ $colorColumnWidth->getSupplierProductAtIndex($supplierRow->getSupplier(), $i)->getProductable()->getCoating()->getName() }}</td>
                            @else
                            <td>&nbsp;</td>
                            <td style="background-color:#C0C0C0">&nbsp;</td>
                            @endif
                    @endforeach
                @endforeach
            </tr>
            @endfor
            @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>TOTAL</th>
            @foreach($report->getColorColumns() as $colorColumn)
            @foreach($colorColumn->getColorColumnWidths() as $colorColumnWidth)
            <th colspan="2">
                {{ $colorColumnWidth->getFormattedStockQuantity() }} /
                {{ $colorColumnWidth->getFormattedPendingQuantity() }}
            </th>
            @endforeach
            @endforeach
        </tr>
    </tfoot>
</table>