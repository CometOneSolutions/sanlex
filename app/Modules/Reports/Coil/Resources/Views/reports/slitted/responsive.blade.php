@extends('reports.template-responsive')
@section('title', 'SLITTED INVENTORY '. $type)
@section('lastUpdatedAt', $lastUpdatedAt->toDayDateTimeString())

@section('content')
<table>
    <thead>
        <tr>
            <th class="fixed width-lg no-shade">&nbsp;</th>
            @foreach($report->getThicknessWidths() as $thicknessWidth)
            <th align="center" class="width-md">
                {{ $thicknessWidth->getName() }}
            </th>
            @endforeach
        </tr>
    </thead>
    <tbody>

        @foreach($report->getSupplierCoatingColors() as $supplierCoatingColor)
        <tr>
            <td align="center" class="fixed width-lg">{{ $supplierCoatingColor->getName() }}</td>
            @foreach($report->getThicknessWidths() as $thicknessWidth)
            <div hidden>
                {{ $product = $thicknessWidth->getProduct($supplierCoatingColor->getSupplier(), $supplierCoatingColor->getCoating(), $supplierCoatingColor->getColor()) }}
            </div>
            <td align="center" class="width-md">
                @if($product)
                {{ $product->getFormattedStockQuantity() }}

                @if($product->getFormattedPendingQuantity() != '-')
                /
                <span class="red">{{ $product->getFormattedPendingQuantity() }}</span>
                @endif
                @else
                --
                @endif
            </td>
            @endforeach
        </tr>
        @endforeach
        <tr>
            <td align="center" class="fixed"><b>T O T A L</b></td>
            @foreach($report->getThicknessWidths() as $thicknessWidth)
            <td align="center">
                <b>{{ $thicknessWidth->getFormattedStockQuantity() }}
                    @if($thicknessWidth->getFormattedPendingQuantity() != '-')
                    /
                    <span class="red">{{ $thicknessWidth->getFormattedPendingQuantity() }}</span>
                    @endif
                </b>
            </td>
            @endforeach
        </tr>
    </tbody>
</table>
@endsection