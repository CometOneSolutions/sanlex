@extends('app')
@section('breadcrumbs', Breadcrumbs::render('report-total.summary'))
@section('content')
<div class="card table-responsive">

    <div class="card-body">
        <div class="card" class="col-md-6">
            <div class="card-header">
                Total Stock
            </div>
            <table class="table table-bordered">
                @foreach($standardPrePaintedTotals as $color => $stock)
                    <tr>
                        <td class="p-2 font-xl font-weight-bold">
                            {{strtoupper($color)}}
                        </td>
                        <td class="p-2 font-xl text-right">
                            {{ number_format($stock, 3) }}
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td class="p-2 font-xl font-weight-bold">
                    </td>
                    <td class="p-2 font-xl font-weight-bold text-right">
                        {{ number_format($standardPrePaintedTotals->sum(), 3) }}
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td class="p-2 font-xl font-weight-bold">
                        GI
                    </td>
                    <td class="p-2 font-xl text-right">
                        {{ number_format($standardGITotal, 3) }}
                    </td>
                </tr>
                <tr>
                    <td class="p-2 font-xl font-weight-bold">
                        STAINLESS STEEL
                    </td>
                    <td class="p-2 font-xl text-right">
                        {{ number_format($standardStainlessSteelTotal, 3) }}
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td class="p-2 font-xl font-weight-bold">
                        SLITTED GI
                    </td>
                    <td class="p-2 font-xl text-right">
                        {{ number_format($slittedGITotal, 3) }}
                    </td>
                </tr>
                <tr>
                    <td class="p-2 font-xl font-weight-bold">
                        SLITTED COLORED
                    </td>
                    <td class="p-2 font-xl text-right">
                        {{ number_format($slittedPrePaintedTotal, 3) }}
                    </td>
                </tr>
                <tr>
                    <td class="p-2 font-xl font-weight-bold">
                        SLITTED STAINLESS STEEL
                    </td>
                    <td class="p-2 font-xl text-right">
                        {{ number_format($slittedStainlessSteelTotal, 3) }}
                    </td>
                </tr>

            </table>
        </div>
    </div>
</div>
@stop