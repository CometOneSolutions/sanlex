<?php

namespace App\Modules\Reports\Coil\Controller;

use CometOneSolutions\Common\Utils\Filter;
use CometOneSolutions\Common\Utils\Sorter;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\PurchaseOrder\Services\PurchaseOrderRecordService;
use App\Modules\Reports\Coil\Models\Damaged\CoilDamagedReport;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;

class DamagedCoilReportController extends Controller
{
    protected $productRepository;
    protected $purchaseOrderRecordService;
    protected $itemMovementRecordService;

    public function __construct(
        ProductRepository $productRepository,
        ItemMovementRecordService $itemMovementRecordService,
        PurchaseOrderRecordService $purchaseOrderRecordService
    ) {
        $this->productRepository = $productRepository;
        $this->purchaseOrderRecordService = $purchaseOrderRecordService;
        $this->itemMovementRecordService = $itemMovementRecordService;
        $this->middleware('permission:Read [REP] Reports')->only(['index']);
    }

    public function index()
    {
        if ($this->productRepository->coil()->hasDamagedStock()->count() <= 0) {
            return redirect()->back()->with('status', 'No Damaged Coil Available');
        }

        $report = new CoilDamagedReport(
            $this->productRepository
        );
        $lastUpdatedAt = $this->getLastUpdatedAt('Coils');
        return view('reports.damaged.print', compact('report', 'lastUpdatedAt'));
    }

    public function getLastUpdatedAt($value)
    {
        $filter = new Filter('product_type', $value);
        $updatedAtSorter = new Sorter('updated_at', 'DESC');

        $lastPurchaseOrder = $this->purchaseOrderRecordService->getFirst([$filter], [$updatedAtSorter]);
        $lastItemMovements = $this->itemMovementRecordService->getLastItemMovement($value);
        $lastUpdatedPurchaseOrder = null;
        $lastUpdatedItemMovement = null;
        $lastUpdated = null;
        if ($lastPurchaseOrder) {
            $lastUpdatedPurchaseOrder = $lastPurchaseOrder->updated_at->setTimezone('Asia/Manila');
        }
        if ($lastItemMovements->isNotEmpty()) {

            $lastUpdatedItemMovement = $lastItemMovements->first()->updated_at->setTimezone('Asia/Manila');
        }
        if ($lastUpdatedPurchaseOrder !== null) {
            if ($lastUpdatedItemMovement !== null) {
                $lastUpdated = ($lastUpdatedPurchaseOrder->greaterThan($lastUpdatedItemMovement)) ? $lastUpdatedPurchaseOrder : $lastUpdatedItemMovement;
            } else {
                $lastUpdated = $lastUpdatedPurchaseOrder;
            }
        } else {
            if ($lastUpdatedItemMovement !== null) {
                $lastUpdated = $lastUpdatedItemMovement;
            }
        }
        return $lastUpdated;
    }
}
