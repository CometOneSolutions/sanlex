<?php

namespace App\Modules\Reports\Coil\Controller;

use CometOneSolutions\Common\Controllers\Controller;

use App\Modules\Thickness\Services\ThicknessRecordService;
use App\Modules\Reports\Coil\Models\Thickness\CoilThicknessReport;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Color\Repositories\ColorRepository;
use App\Modules\Supplier\Repositories\SupplierRepository;
use CometOneSolutions\Common\Utils\Filter;
use CometOneSolutions\Common\Utils\Sorter;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\PurchaseOrder\Services\PurchaseOrderRecordService;
use App\Modules\Reports\Coil\Export\ThicknessExport;

class ThicknessReportController extends Controller
{
    protected $thicknessRecordService;
    protected $purchaseOrderRecordService;
    protected $itemMovementRecordService;
    protected $productRepository;
    protected $colorRepository;
    protected $supplierRepository;
    protected $exportView = 'reports.thickness.export';

    public function __construct(
        ThicknessRecordService $thicknessRecordService,
        PurchaseOrderRecordService $purchaseOrderRecordService,
        ItemMovementRecordService $itemMovementRecordService,
        ProductRepository $productRepository,
        ColorRepository $colorRepository,
        SupplierRepository $supplierRepository
    ) {
        $this->thicknessRecordService = $thicknessRecordService;
        $this->purchaseOrderRecordService = $purchaseOrderRecordService;
        $this->itemMovementRecordService = $itemMovementRecordService;
        $this->productRepository = $productRepository;
        $this->colorRepository = $colorRepository;
        $this->supplierRepository = $supplierRepository;
        $this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
    }

    public function index()
    {
        $products = $this->productRepository->inStockOrHasPending()->coil()->coilWidthIsStandard()->getAll();
        $thicknesses = $products->map(function ($product) {
            return $product->getProductable()->getThickness();
        })->unique(function ($thickness) {
            return $thickness->getId();
        });
        $thicknesses = $thicknesses->sortBy('name');
        return view('reports.thickness.index', compact('thicknesses'));
    }

    public function print($thicknessId)
    {
        $thickness = $this->thicknessRecordService->getById($thicknessId);
        $report = new CoilThicknessReport(
            $thickness,
            $this->productRepository,
            $this->colorRepository,
            $this->supplierRepository
        );
        $report = $this->sort($report);
        $lastUpdatedAt = $this->getLastUpdatedAt('COIL');
        return view('reports.thickness.print', compact('report', 'lastUpdatedAt'));
    }

    public function responsive($thicknessId)
    {
        $thickness = $this->thicknessRecordService->getById($thicknessId);
        $report = new CoilThicknessReport(
            $thickness,
            $this->productRepository,
            $this->colorRepository,
            $this->supplierRepository
        );
        $report = $this->sort($report);
        $lastUpdatedAt = $this->getLastUpdatedAt('COIL');
        return view('reports.thickness.responsive', compact('report', 'lastUpdatedAt'));
    }

    public function export($thicknessId)
    {
        $thickness = $this->thicknessRecordService->getById($thicknessId);
        $report = new CoilThicknessReport(
            $thickness,
            $this->productRepository,
            $this->colorRepository,
            $this->supplierRepository
        );
        $report = $this->sort($report);
        $filename = 'Summary:' . $thickness->getName() . '_' . time() . '.xlsx';

        return Excel::download(new ThicknessExport($report, $this->exportView), $filename);
    }
    private function sort($report)
    {
        $sorted = ($report->getSupplierRows()->sortBy(function ($row, $key) {
            return $row->getSupplier()->getShortCode();
        }));
        $report->setSupplierRows($sorted);
        $sortedColor = $report->getColorColumns()->sortBy(function ($row, $key) {
            return $row->getColor()->getName();
        });
        $report->setColorColumns($sortedColor);
        return $report;
    }

    public function getLastUpdatedAt($value)
    {
        $coilFilter = new Filter('product_type', 'Coils');
        $updatedAtSorter = new Sorter('updated_at', 'DESC');

        $lastPurchaseOrder = $this->purchaseOrderRecordService->getFirst([$coilFilter], [$updatedAtSorter]);
        $lastItemMovements = $this->itemMovementRecordService->getLastItemMovement($value);
        $lastUpdatedPurchaseOrder = null;
        $lastUpdatedItemMovement = null;
        $lastUpdated = null;
        if ($lastPurchaseOrder) {
            $lastUpdatedPurchaseOrder = $lastPurchaseOrder->updated_at->setTimezone('Asia/Manila');
        }
        if ($lastItemMovements->isNotEmpty()) {

            $lastUpdatedItemMovement = $lastItemMovements->first()->updated_at->setTimezone('Asia/Manila');
        }
        if ($lastUpdatedPurchaseOrder !== null) {
            if ($lastUpdatedItemMovement !== null) {
                $lastUpdated = ($lastUpdatedPurchaseOrder->greaterThan($lastUpdatedItemMovement)) ? $lastUpdatedPurchaseOrder : $lastUpdatedItemMovement;
            } else {
                $lastUpdated = $lastUpdatedPurchaseOrder;
            }
        } else {
            if ($lastUpdatedItemMovement !== null) {
                $lastUpdated = $lastUpdatedItemMovement;
            }
        }
        return $lastUpdated;
    }
}
