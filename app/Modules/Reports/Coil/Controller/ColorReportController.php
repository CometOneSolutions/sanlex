<?php

namespace App\Modules\Reports\Coil\Controller;

use CometOneSolutions\Common\Utils\Filter;
use CometOneSolutions\Common\Utils\Sorter;
use App\Modules\Color\Services\ColorRecordService;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Coil\Models\Color\CoilColorReport;
use App\Modules\Reports\Coil\Models\Slitted\CoilSlittedReport;
use App\Modules\PurchaseOrder\Services\PurchaseOrderRecordService;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;


class ColorReportController extends Controller
{
	protected $colorRecordService;
	protected $productRepository;
	protected $itemMovementRecordService;
	protected $purchaseOrderRecordService;

	public function __construct(ColorRecordService $colorRecordService, ItemMovementRecordService $itemMovementRecordService, PurchaseOrderRecordService $purchaseOrderRecordService, ProductRepository $productRepository)
	{
		$this->colorRecordService = $colorRecordService;
		$this->purchaseOrderRecordService = $purchaseOrderRecordService;
		$this->itemMovementRecordService = $itemMovementRecordService;
		$this->productRepository = $productRepository;
		$this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
	}

	public function index()
	{
		$products = $this->productRepository->inStockOrHasPending()->coil()->getAll();
		$colors = $products->map(function ($product) {
			return $product->getProductable()->getColor();
		})->unique(function ($color) {
			return $color->getId();
		});
		$colors = $colors->sortBy('name');
		return view('reports.color.index', compact('colors'));
	}

	public function print($colorId)
	{
		$color = $this->colorRecordService->getById($colorId);
		$report = new CoilColorReport($color, $this->productRepository);
		$lastUpdatedAt = $this->getLastUpdatedAt('Coils');


		return view('reports.color.print', compact('report', 'lastUpdatedAt'));
	}

	public function responsive($colorId)
	{
		$color = $this->colorRecordService->getById($colorId);
		$report = new CoilColorReport($color, $this->productRepository);
		$lastUpdatedAt = $this->getLastUpdatedAt('Coils');
        $colors = [
            'purple',
            'maroon',
            'fuchsia',
            'green',
            'lime',
            'navy',
            'teal',
            'olive',
            'lime',
            'red',
            'pink',
            'blue',
            'orange',
        ];
		return view('reports.color.responsive', compact('report', 'lastUpdatedAt', 'colors'));
	}

	public function slitted($type)
	{
		$report = new CoilSlittedReport($this->productRepository, $type);
		$lastUpdatedAt = $this->getLastUpdatedAt('Coils');
		return view('reports.slitted.print', compact('report', 'lastUpdatedAt', 'type'));
	}

	public function slittedResponsive($type)
	{
		$report = new CoilSlittedReport($this->productRepository, $type);
		$lastUpdatedAt = $this->getLastUpdatedAt('Coils');
		return view('reports.slitted.responsive', compact('report', 'lastUpdatedAt', 'type'));
	}

	public function getLastUpdatedAt($value)
	{
		$filter = new Filter('product_type', $value);
		$updatedAtSorter = new Sorter('updated_at', 'DESC');

		$lastPurchaseOrder = $this->purchaseOrderRecordService->getFirst([$filter], [$updatedAtSorter]);
		$lastItemMovements = $this->itemMovementRecordService->getLastItemMovement($value);
		$lastUpdatedPurchaseOrder = null;
		$lastUpdatedItemMovement = null;
		$lastUpdated = null;
		if ($lastPurchaseOrder) {
			$lastUpdatedPurchaseOrder = $lastPurchaseOrder->updated_at->setTimezone('Asia/Manila');
		}
		if ($lastItemMovements->isNotEmpty()) {

			$lastUpdatedItemMovement = $lastItemMovements->first()->updated_at->setTimezone('Asia/Manila');
		}
		if ($lastUpdatedPurchaseOrder !== null) {
			if ($lastUpdatedItemMovement !== null) {
				$lastUpdated = ($lastUpdatedPurchaseOrder->greaterThan($lastUpdatedItemMovement)) ? $lastUpdatedPurchaseOrder : $lastUpdatedItemMovement;
			} else {
				$lastUpdated = $lastUpdatedPurchaseOrder;
			}
		} else {
			if ($lastUpdatedItemMovement !== null) {
				$lastUpdated = $lastUpdatedItemMovement;
			}
		}
		return $lastUpdated;
	}
}
