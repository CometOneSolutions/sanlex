<?php

namespace App\Modules\Reports\Coil\Controller;

use App\Modules\Product\Models\ProductModel;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\Product\Repositories\ProductRepository;

class TotalStockReportController extends Controller
{
	protected $productRepository;

	public function __construct(ProductRepository $productRepository)
	{
		$this->productRepository = $productRepository;
		$this->middleware('permission:Read [REP] Reports')->only(['index', 'print']);
	}

	public function index()
	{
        $standardPrePaintedCoils = ProductModel::whereHas('coil', function ($query) {
            $query->whereHas('width', function ($query) {
                $query->standard();
            })->whereHas('color', function ($query) {
                $query->prePainted();
            });
        })->get();

        $standardPrePaintedTotals = $standardPrePaintedCoils->groupBy(function ($product) {
            return $product->productable->color->name;
        })->map(function($products) {
            return $products->sum(function($product){
                return $product->getGoodStockQuantity();
            });
        })->sortBy(function ($stock, $key) {
            return $key;
        });

        $standardGITotal = ProductModel::whereHas('coil', function ($query) {
            $query->whereHas('width', function ($query) {
                $query->standard();
            })->colorName('GI');
        })->get()->sum(function($product){
            return $product->getGoodStockQuantity();
        });

        $standardStainlessSteelTotal = ProductModel::whereHas('coil', function ($query) {
            $query->whereHas('width', function ($query) {
                $query->standard();
            })->colorName('STAINLESS STEEL');
        })->get()->sum(function($product){
            return $product->getGoodStockQuantity();
        });

        $slittedGITotal = ProductModel::whereHas('coil', function ($query) {
            $query->whereHas('width', function ($query) {
                $query->nonStandard();
            })->colorName('GI');
        })->get()->sum(function($product){
            return $product->getGoodStockQuantity();
        });

        $slittedPrePaintedTotal = ProductModel::whereHas('coil', function ($query) {
            $query->whereHas('width', function ($query) {
                $query->nonStandard();
            })->whereHas('color', function ($query) {
                $query->prePainted();
            });
        })->get()->sum(function($product){
            return $product->getGoodStockQuantity();
        });

        $slittedStainlessSteelTotal = ProductModel::whereHas('coil', function ($query) {
            $query->whereHas('width', function ($query) {
                $query->nonStandard();
            })->colorName('STAINLESS STEEL');
        })->get()->sum(function($product){
            return $product->getGoodStockQuantity();
        });


        return view('reports.total.index', compact(
            'standardPrePaintedTotals',
            'standardGITotal',
            'standardStainlessSteelTotal',
            'slittedPrePaintedTotal',
            'slittedGITotal',
            'slittedStainlessSteelTotal'
        ));
	}
}
