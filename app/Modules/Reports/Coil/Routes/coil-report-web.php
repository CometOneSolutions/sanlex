<?php

Route::group(['prefix' => 'reports'], function () {
    Route::get('total-stock', 'TotalStockReportController@index')->name('reports_coil_total_stock-index');
    
    Route::get('color', 'ColorReportController@index')->name('reports_coil_color-index');

    Route::get('/thickness', 'ThicknessReportController@index')->name('reports_coil_thickness-index');
    Route::get('/thickness/{thicknessId}/export', 'ThicknessReportController@export')->name('reports_coil_thickness-export');

    Route::get('color/{colorId}', 'ColorReportController@responsive')->name('reports_coil_color-print');
    Route::get('/thickness/{thicknessId}', 'ThicknessReportController@responsive')->name('reports_coil_thickness-print');
    Route::get('/slitted/{type}', 'ColorReportController@slittedResponsive')->name('reports_coil_slitted-index');

    Route::get('damaged', 'DamagedCoilReportController@index')->name('reports_coil_damaged-index');

    // Route::get('color/{colorId}/responsive', 'ColorReportController@responsive');
    // Route::get('/thickness/{thicknessId}/responsive', 'ThicknessReportController@responsive');
    // Route::get('/slitted/{type}/responsive', 'ColorReportController@slittedResponsive');
});
