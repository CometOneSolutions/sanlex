<?php

//Reports
Breadcrumbs::register('report-total.summary', function ($breadcrumbs) {
    $breadcrumbs->parent('report-menu-dashboard.index');
    $breadcrumbs->push('Total Stock', route('reports_coil_total_stock-index'));
});

Breadcrumbs::register('report-color.summary', function ($breadcrumbs) {
    $breadcrumbs->parent('report-menu-dashboard.index');
    $breadcrumbs->push('Color', route('reports_coil_color-index'));
});

Breadcrumbs::register('report-thickness.summary', function ($breadcrumbs) {
    $breadcrumbs->parent('report-menu-dashboard.index');
    $breadcrumbs->push('Thickness', route('reports_coil_thickness-index'));
});
