<?php

namespace App\Modules\Reports\Coil\Models\Thickness;

use App\Modules\Color\Models\Color;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;

class CoilColorColumn
{
    protected $color;
    protected $productRepository;
    protected $thickness;
    protected $colorColumnWidths;
    protected $products;

    public function __construct(Color $color, Thickness $thickness)
    {
        $this->color = $color;
        $this->thickness = $thickness;
        $this->productRepository = resolve(ProductRepository::class);
        $this->products = $this->initProducts($thickness);
        $this->colorColumnWidths = $this->initColorColumnWidths($this->products);
    }

    protected function initColorColumnWidths($products)
    {
        $color = $this->color;
        $thickness = $this->thickness;
        return $products->map(function ($product) {
            return $product->getProductable();
        })->unique(function ($coil) {
            return $coil->getWidthId();
        })->map(function ($coil) use ($color, $thickness) {
            return new CoilColorColumnWidth($coil->getWidth(), $thickness, $color);
        });
    }
    protected function initProducts(Thickness $thickness)
    {
        $this->productRepository = $this->productRepository->coilThickness($thickness)->coilWidthIsStandard()->inStockOrHasPending();
        return $this->productRepository->getAll();
    }
    public function getColorColumnWidths()
    {

        return $this->colorColumnWidths->sortBy(function ($row, $key) {
            return $row->getWidth()->getName();
        });
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getMaxProductCount(Supplier $supplier)
    {
        return $this->colorColumnWidths->max(function ($colorColumnWidth) use ($supplier) {
            return $colorColumnWidth->getSupplierProducts($supplier)->count();
        });
    }
    public function getTotalQuantity()
    {
        return ($this->getColorColumnWidths()->sum(function ($colorColumnWidth) {

            return ($colorColumnWidth->getFormattedStockQuantity() != '-') ? $colorColumnWidth->getFormattedStockQuantity() : 0;
        }));
    }
}
