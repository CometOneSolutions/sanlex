<?php

namespace App\Modules\Reports\Coil\Models\Thickness;

use App\Modules\Color\Models\Color;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;

class CoilColorColumnWidth
{
    protected $width;
    protected $thickness;
    protected $color;
    protected $productRepository;
    protected $products;

    public function __construct(Width $width, Thickness $thickness, Color $color)
    {
        $this->width = $width;
        $this->thickness = $thickness;
        $this->color = $color;
        // $this->productRepository = $productRepository->coilWidth($width);

        $this->productRepository = resolve(ProductRepository::class);
        $this->products = $this->initProducts($width, $thickness, $color);
    }
    protected function initProducts(Width $width, Thickness $thickness, Color $color)
    {

        return $this->productRepository->coilThickness($thickness)->coilWidthIsStandard()->coilWidth($width)->coilColor($color)->inStockOrHasPending()->getAll();
    }
    public function getWidth()
    {
        return $this->width;
    }

    public function getSupplierProducts(Supplier $supplier)
    {
        return (clone $this->productRepository->coilThickness($this->thickness)->coilWidthIsStandard()->coilWidth($this->width)->coilColor($this->color)->inStockOrHasPending())->supplier($supplier)->getAll();
    }

    public function getPendingQuantity()
    {
        return $this->products->reduce(function ($prev, $curr) {
            return $prev + $curr->getPendingQuantity();
        }, 0);
    }

    public function getStockQuantity()
    {
        return $this->products->reduce(function ($prev, $curr) {
            return $prev + $curr->getGoodStockQuantity();
            // return $prev + $curr->getStockQuantity();
        }, 0);
    }

    public function getFormattedPendingQuantity()
    {
        $qty = $this->getPendingQuantity();

        if ($qty == 0) {
            return '-';
        }

        return number_format($qty);
    }

    public function getFormattedStockQuantity()
    {
        $qty = $this->getStockQuantity();

        if ($qty == 0) {
            return '-';
        }

        return number_format($qty, 3);
    }

    public function getSupplierProductAtIndex(Supplier $supplier, $index)
    {
        $products = $this->getSupplierProducts($supplier);

        if ($products->count() > $index) {
            return $products[$index];
        }

        return null;
    }
}
