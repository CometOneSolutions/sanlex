<?php

namespace App\Modules\Reports\Coil\Models\Thickness;

use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Thickness\Models\Thickness;

class CoilThicknessReport
{
    protected $thickness;
    protected $productRepository;
    protected $products;

    protected $suppliers;

    protected $colorColumns;
    protected $supplierRows;

    public function __construct(
        Thickness $thickness,
        ProductRepository $productRepository
    ) {
        $this->thickness = $thickness;
        $this->productRepository = $productRepository->coilThickness($thickness)->coilWidthIsStandard()->inStockOrHasPending();
        $this->products = $productRepository->getAll();
        $this->suppliers = $this->initSuppliers($this->products);
        $this->colorColumns = $this->initColorColumns($this->products, $thickness);
        $this->supplierRows = $this->initSupplierRows($this->suppliers, $this->colorColumns);
    }

    public function getSuppliers()
    {
        return $this->suppliers;
    }

    public function getSupplierRows()
    {
        return $this->supplierRows;
    }

    public function setSupplierRows($supplierRows)
    {
        $this->supplierRows = $supplierRows;
    }

    public function getColorColumns()
    {
        return $this->colorColumns;
    }
    public function setColorColumns($colorColumns)
    {
        $this->colorColumns = $colorColumns;
    }
    public function getThicknessName()
    {
        return $this->thickness->getName();
    }

    protected function initColorColumns($products, Thickness $thickness)
    {
        return $products->map(function ($product) {
            return $product->getProductable();
        })->unique(function ($coil) {
            return $coil->getColorId();
        })->map(function ($coil) use ($thickness) {
            return new CoilColorColumn($coil->getColor(), $thickness);
        });
    }

    protected function initSuppliers($products)
    {
        return $products->unique(function ($product) {
            return $product->getSupplierId();
        })->map(function ($product) {
            return $product->getSupplier();
        });
    }

    protected function initSupplierRows($suppliers, $colorColumns)
    {
        return $suppliers->map(function ($supplier) use ($colorColumns) {
            return new CoilSupplierRow($supplier, $colorColumns);
        });
    }
}
