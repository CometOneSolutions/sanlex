<?php

namespace App\Modules\Reports\Coil\Models\Thickness;

use App\Modules\Supplier\Models\Supplier;

class CoilSupplierRow
{
    protected $supplier;
    protected $colorColumns;

    public function __construct(Supplier $supplier, $colorColumns)
    {
        $this->supplier = $supplier;
        $this->colorColumns = $colorColumns;
    }

    public function getMaxProductCount()
    {
        return $this->colorColumns->max(function ($colorColumn) {
            return $colorColumn->getMaxProductCount($this->supplier);
        });
    }

    public function getSupplier()
    {
        return $this->supplier;
    }

    public function getColorColumns()
    {

        return $this->colorColumns->sortBy(function ($row, $key) {
            return $row->getColor()->getName();
        });
    }
}
