<?php

namespace App\Modules\Reports\Coil\Models\Slitted;

use App\Modules\Coating\Models\Coating;
use App\Modules\Color\Models\Color;
use App\Modules\Supplier\Models\Supplier;

class CoilSupplierCoatingColor
{
    protected $supplier;
    protected $coating;
    protected $color;

    public function __construct(Supplier $supplier, Coating $coating, Color $color)
    {
        $this->supplier = $supplier;
        $this->coating = $coating;
        $this->color = $color;
    }

    public function getSupplier()
    {
        return $this->supplier;
    }

    public function getCoating()
    {
        return $this->coating;
    }
    public function getColor()
    {
        return $this->color;
    }
    public function getSupplierName()
    {
        return $this->supplier->getName();
    }

    public function getCoatingName()
    {
        return $this->coating->getName();
    }
    public function getColorName()
    {
        return $this->color->getName();
    }
    public function getSupplierShortCode()
    {
        return $this->supplier->getShortCode();
    }
    public function getName()
    {
        return sprintf('%s %s %s', $this->getSupplierShortCode(), $this->getCoatingName(), $this->getColorName());
    }
}
