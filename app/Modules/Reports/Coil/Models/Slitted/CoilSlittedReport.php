<?php

namespace App\Modules\Reports\Coil\Models\Slitted;

use App\Modules\Product\Repositories\ProductRepository;

class CoilSlittedReport
{

    protected $productRepository;
    protected $products;

    protected $thicknessWidths;
    protected $supplierCoatingColors;

    public function __construct(
        ProductRepository $productRepository,
        $type
    ) {
        $this->productRepository = $productRepository->inStockOrHasPending();

        if ($type === 'GI') {
            $this->products = $this->productRepository->GINonStandards()->getAll();
        } else if ($type === 'STAINLESS STEEL') {
            $this->products = $this->productRepository->stainlessSteelNonStandards()->getAll();
        } else {
            $this->products = $this->productRepository->coloredNonStandards()->getAll();
        }

        $this->thicknessWidths = $this->initThicknessWidths($this->products, $this->productRepository);
        $this->supplierCoatingColors = $this->initSupplierCoatingColors($this->products, $this->productRepository, $type);
    }


    public function getProducts()
    {
        return $this->products;
    }

    // TODO optimize by reversing map and unique?
    protected function initSupplierCoatingColors($products, ProductRepository $productRepository, $type)
    {
        return $products->map(function ($product) use ($productRepository) {
            $coil = $product->getProductable();
            return new CoilSupplierCoatingColor($product->getSupplier(), $coil->getCoating(), $coil->getColor());
        })->unique(function ($coilSupplierCoatingColor) use ($type) {
            return $coilSupplierCoatingColor->getName();
        });


        // ->unique(function ($coilSupplierCoatingColor) use ($type) {
        //     if ($type === 'GI') {
        //         if ($coilSupplierCoatingColor->getColor()->getName() === 'GI') {
        //             return $coilSupplierCoatingColor->getName();
        //         }
        //     } else {

        //         if ($coilSupplierCoatingColor->getColor()->getName() != 'GI') {
        //             return $coilSupplierCoatingColor->getName();
        //         }
        //         return null;
        //     }
        // });
    }

    // TODO optimize by reversing map and unique?
    protected function initThicknessWidths($products, ProductRepository $productRepository)
    {
        // $products = $products->load(['productable.width', 'productable.thickness']);
        // $data = $products->sortBy(function ($productable) {
        //     dd($productable);
        // });

        return $products->map(function ($product) use ($productRepository) {
            $coil = $product->getProductable();
            return new CoilSlittedThicknessWidth($coil->getThickness(), $coil->getWidth(), $productRepository);
        })->unique(function ($coilThicknessWidth) {
            return $coilThicknessWidth->getName();
        })->sortBy(function ($coilThicknessWidth) {

            return (float) ($coilThicknessWidth->getThickness()->getName() . str_pad($coilThicknessWidth->getWidth()->getName(), 4, '0', STR_PAD_LEFT));
        });
    }

    public function getThicknessWidths()
    {
        return $this->thicknessWidths;
    }

    public function getSupplierCoatingColors()
    {

        return $this->supplierCoatingColors->sortBy(function ($row, $key) {
            return $row->getName();
        });
    }
}
