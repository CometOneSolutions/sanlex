<?php

namespace App\Modules\Reports\Coil\Models\Slitted;

use App\Modules\Coating\Models\Coating;
use App\Modules\Color\Models\Color;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;

class CoilSlittedThicknessWidth
{
    protected $thickness;
    protected $width;
    protected $name;
    protected $productRepository;
    protected $products;

    public function __construct(
        Thickness $thickness,
        Width $width,
        ProductRepository $productRepository
    ) {
        $this->thickness = $thickness;
        $this->width = $width;
        $this->productRepository = clone $productRepository;
        $this->name = $this->initName($thickness, $width);
        $this->products = $this->initProducts($thickness, $width);
    }

    protected function initProducts(Thickness $thickness, Width $width)
    {
        return $this->productRepository->coilThickness($thickness)->coilWidth($width)->getAll();
    }

    protected function initName(Thickness $thickness, Width $width)
    {
        return sprintf('%s x %s', $thickness->getName(), $width->getName());
    }

    public function getThickness()
    {
        return $this->thickness;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPendingQuantity()
    {
        return $this->products->reduce(function ($prev, $curr) {
            return $prev + $curr->getPendingQuantity();
        }, 0);
    }

    public function getFormattedPendingQuantity()
    {
        $qty = $this->getPendingQuantity();

        if ($qty == 0) {
            return '-';
        }

        return number_format($qty);
    }

    public function getFormattedStockQuantity()
    {
        $qty = $this->getStockQuantity();

        if ($qty == 0) {
            return '-';
        }

        return number_format($qty, 3);
    }

    public function getStockQuantity()
    {
        return $this->products->reduce(function ($prev, $curr) {
            return $prev + $curr->getStockQuantity();
        }, 0);
    }

    public function getProduct(Supplier $supplier, Coating $coating, Color $color)
    {
        $temp = clone $this->productRepository;
        return $temp->supplier($supplier)->coilCoating($coating)->coilColor($color)->getFirst();
    }
}
