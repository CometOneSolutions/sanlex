<?php

namespace App\Modules\Reports\Coil\Models\Color;

use App\Modules\Color\Models\Color;
use App\Modules\Product\Repositories\ProductRepository;

class CoilColorReport
{
    protected $color;
    protected $productRepository;
    protected $products;

    protected $thicknessWidths;
    protected $supplierCoatings;

    public function __construct(
        Color $color,
        ProductRepository $productRepository
    ) {
        $this->color = $color;
        $this->productRepository = $productRepository->coilColor($color)->hasGoodStockOrHasPending();
        $this->products = $this->productRepository->coilWidthIsStandard()->getAll();
        $this->thicknessWidths = $this->initThicknessWidths($this->products, $this->productRepository);
        $this->supplierCoatings = $this->initSupplierCoatings($this->products, $this->productRepository);
    }


    public function getColorName()
    {
        return $this->color->getName();
    }

    public function getProducts()
    {
        return $this->products;
    }

    // TODO optimize by reversing map and unique?
    protected function initSupplierCoatings($products, ProductRepository $productRepository)
    {
        return $products->map(function ($product) use ($productRepository) {
            $coil = $product->getProductable();
            return new CoilSupplierCoating($product->getSupplier(), $coil->getCoating(), $productRepository);
        })->unique(function ($coilSupplierCoating) {
            return $coilSupplierCoating->getName();
        });
    }

    // TODO optimize by reversing map and unique?
    protected function initThicknessWidths($products, ProductRepository $productRepository)
    {
        // $products = $products->load(['productable.width', 'productable.thickness']);
        // $data = $products->sortBy(function ($productable) {
        //     dd($productable);
        // });

        return $products->map(function ($product) use ($productRepository) {
            $coil = $product->getProductable();
            return new CoilThicknessWidth($coil->getThickness(), $coil->getWidth(), $productRepository);
        })->unique(function ($coilThicknessWidth) {
            return $coilThicknessWidth->getName();
        })->sortBy(function ($coilThicknessWidth) {
            return (float) ($coilThicknessWidth->getThickness()->getName() . str_pad($coilThicknessWidth->getWidth()->getName(), 4, '0', STR_PAD_LEFT));
        });
    }

    public function getThicknessWidths()
    {
        return $this->thicknessWidths;
    }

    public function getSupplierCoatings()
    {
        return $this->supplierCoatings->sortBy(function ($row, $key) {
            return $row->getName();
        });
    }
}
