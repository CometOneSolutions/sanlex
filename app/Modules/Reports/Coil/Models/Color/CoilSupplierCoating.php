<?php

namespace App\Modules\Reports\Coil\Models\Color;

use App\Modules\Coating\Models\Coating;
use App\Modules\Supplier\Models\Supplier;

class CoilSupplierCoating
{
    protected $supplier;
    protected $coating;

    public function __construct(Supplier $supplier, Coating $coating)
    {
        $this->supplier = $supplier;
        $this->coating = $coating;
    }

    public function getSupplier()
    {
        return $this->supplier;
    }

    public function getCoating()
    {
        return $this->coating;
    }

    public function getSupplierName()
    {
        return $this->supplier->getName();
    }

    public function getCoatingName()
    {
        return $this->coating->getName();
    }
    public function getSupplierShortCode()
    {
        return $this->supplier->getShortCode();
    }
    public function getName()
    {
        return sprintf('%s %s', $this->getSupplierShortCode(), $this->getCoatingName());
    }
}
