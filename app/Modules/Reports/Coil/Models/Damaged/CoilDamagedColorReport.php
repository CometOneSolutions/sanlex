<?php

namespace App\Modules\Reports\Coil\Models\Damaged;


use Illuminate\Support\Collection;
use App\Modules\Color\Models\Color;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Supplier\Models\Supplier;

class CoilDamagedColorReport
{
    protected $color;
    protected $supplier;
    protected $coatings;
    protected $productRepository;

    public function __construct(
        Color $color,
        Supplier $supplier,
        ProductRepository $productRepository
    ) {
        $this->color = $color;
        $this->supplier = $supplier;
        $this->productRepository = $productRepository;
        $this->products = $this->initProducts();

        $this->coatings = $this->initCoatings($this->products);
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;
        return $temp
            ->supplier($this->supplier)
            ->coilColor($this->color)
            ->getAll();
    }

    protected function initCoatings(Collection $products)
    {
        return $products->map(function ($product) {
            $coil = $product->productable;
            return $coil->getCoating();
        })->unique(function ($coating) {
            return $coating->getName();
        });
    }

    public function getCoatings()
    {
        return $this->coatings->sortBy(function ($coating) {
            return $coating->getName();
        });
    }

    public function getCoatingsCount()
    {
        return $this->coatings->count();
    }

    public function getName()
    {
        return $this->color->getName();
    }

    public function getColor()
    {
        return $this->color;
    }
}
