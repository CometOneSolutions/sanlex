<?php

namespace App\Modules\Reports\Coil\Models\Damaged;

use App\Modules\Coating\Models\Coating;
use Illuminate\Support\Collection;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Reports\Coil\Models\Damaged\CoilDamagedColorReport;
use App\Modules\Reports\Coil\Models\Damaged\CoilDamagedSupplierReport;

class CoilDamagedReport
{
    protected $productRepository;
    protected $products;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
        $this->products = $this->productRepository->getAll();


        $this->thicknessAndWidths = $this->initThicknessAndWidths($this->products);
        $this->suppliers = $this->initSuppliers($this->products, $this->productRepository);
    }


    protected function initThicknessAndWidths(Collection $products)
    {
        return $products->map(function ($product) {
            $coil = $product->productable;
            return new CoilDamagedThicknessAndWidthReport($coil->getThickness(), $coil->getWidth());
        })->unique(function ($thicknessAndWidth) {
            return $thicknessAndWidth->getName();
        });
    }

    protected function initSuppliers(Collection $products)
    {
        return $products->map(function ($product) {
            $supplier =  $product->getSupplier();
            return new CoilDamagedSupplierReport($supplier, $this->productRepository);
        })->unique(function ($supplier) {
            return $supplier->getShortCode();
        });
    }

    public function getSuppliers()
    {
        return $this->suppliers->sortBy(function ($supplier) {
            return $supplier->getName();
        });
    }

    public function getThicknessAndWidths()
    {

        return $this->thicknessAndWidths->sortBy(function ($thicknessAndWidth) {
            return [floatval($thicknessAndWidth->getThickness()->getName()), intval($thicknessAndWidth->getWidth()->getName())];
        });
    }

    public function getProduct(CoilDamagedSupplierReport $coilDamagedSupplier, CoilDamagedColorReport $coilDamagedColor, CoilDamagedThicknessAndWidthReport $coilDamagedThicknessAndWidth,  Coating $coating)
    {
        $temp = clone $this->productRepository;

        return $temp
            ->coilColor($coilDamagedColor->getColor())
            ->coilThickness($coilDamagedThicknessAndWidth->getThickness())
            ->coilWidth($coilDamagedThicknessAndWidth->getWidth())
            ->supplier($coilDamagedSupplier->getSupplier())
            ->coilCoating($coating)
            ->getFirst();
    }

    public function getCoilNumbers(CoilDamagedSupplierReport $coilDamagedSupplier, CoilDamagedColorReport $coilDamagedColor, CoilDamagedThicknessAndWidthReport $coilDamagedThicknessAndWidth,  Coating $coating)
    {
        $temp = clone $this->productRepository;

        $product =  $temp
            ->coilColor($coilDamagedColor->getColor())
            ->coilThickness($coilDamagedThicknessAndWidth->getThickness())
            ->coilWidth($coilDamagedThicknessAndWidth->getWidth())
            ->supplier($coilDamagedSupplier->getSupplier())
            ->coilCoating($coating)
            ->getFirst();

        if ($product) {
            $badInventoryDetails =  $product->getInventory()->inventoryDetails()->condition('Bad')->get();
            return $badInventoryDetails->map(function ($inventoryDetail) {
                return $inventoryDetail->getReceiving() ? $inventoryDetail->getReceiving()->getCoilNumber() : $inventoryDetail->getSlitIdentifier();
            })->sort();
        }
    }
}
