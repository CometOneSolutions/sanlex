<?php

namespace App\Modules\Reports\Coil\Models\Damaged;


use Illuminate\Support\Collection;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Product\Repositories\ProductRepository;

class CoilDamagedSupplierReport
{
    protected $supplier;
    protected $productRepository;
    protected $colors;

    public function __construct(
        Supplier $supplier,
        ProductRepository $productRepository
    ) {
        $this->supplier = $supplier;
        $this->productRepository = $productRepository;
        $this->products = $this->initProducts();

        $this->colors = $this->initColors($this->products);
    }

    protected function initProducts()
    {
        $temp = clone $this->productRepository;
        return $temp
            ->supplier($this->supplier)
            ->getAll();
    }

    protected function initColors(Collection $products)
    {
        return $products->map(function ($product) {
            $coil = $product->productable;
            return new CoilDamagedColorReport($coil->getColor(), $product->getSupplier(), $this->productRepository);
        })->unique(function ($color) {
            return $color->getName();
        });
    }

    public function getColors()
    {
        return $this->colors->sortBy(function ($color) {
            return $color->getName();
        });
    }

    public function getColorsCount()
    {
        return $this->colors->sum(function ($color) {
            return $color->getCoatingsCount();
        });
    }

    public function getSupplier()
    {
        return $this->supplier;
    }

    public function getName()
    {
        return $this->supplier->getName();
    }

    public function getShortCode()
    {
        return $this->supplier->getShortCode();
    }
}
