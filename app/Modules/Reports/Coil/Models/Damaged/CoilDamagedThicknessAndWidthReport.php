<?php

namespace App\Modules\Reports\Coil\Models\Damaged;

use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;

class CoilDamagedThicknessAndWidthReport
{
    protected $thickness;
    protected $width;
    protected $name;

    public function __construct(
        Thickness $thickness,
        Width $width
    ) {
        $this->thickness = $thickness;
        $this->width = $width;
        $this->name = $this->initName($this->thickness, $this->width);
    }

    protected function initName(Thickness $thickness, Width $width)
    {
        return sprintf('%s x %s', $thickness->getName(), $width->getName());
    }

    public function getThickness()
    {
        return  $this->thickness;
    }

    public function getWidth()
    {
        return  $this->width;
    }

    public function getName()
    {
        return $this->name;
    }
}
