<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    {{-- <link href="{{ asset('css/accessory-report.css') }}" rel="stylesheet"> --}}
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') Inventory Summary</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
    integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
    crossorigin="anonymous"></script>
    <style>
        .container{
            height: 10vh
        }
        html {
            display: table;
            margin: 0 10px;
        }

        body {
            margin: 0 auto;
            width: 794px;
            font-size: 12px;
            font-family: 'Open Sans', sans-serif;
            margin-left: 10px;
            margin-right: 10px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        table .bordered tr,
        table .bordered th,
        table .bordered td {
            border: 1px solid black;

        }

        .bordered tbody>tr:nth-child(even) {
            background: #FFF
        }

        .bordered tbody>tr:nth-child(odd) {
            background: #DDD
        }

        .no-shade {
            background: #FFF
        }

        .red {
            color: red;
        }

        /* FOR SCROLLING */

        table.bordered {
            position: relative;
            overflow: hidden;
            border-collapse: collapse;
        }

         thead.bordered {
            position: relative;
            display: block;
            width: 794px;
            overflow: visible;
        }

         thead.bordered tr th {
            min-width: 100px;
            max-width: 100px;
        }

         thead.bordered tr th.fixed {
            position: relative;
            background-color: #ffffff;
        }

         tbody.bordered {
            position: relative;
            display: block;
            width: 794px;
            max-height: 75vh;
            min-height:  50vh;
            overflow: scroll;
        }

    
         tbody.bordered td {
            min-width: 100px;
            max-width: 100px;
        }

         tbody.bordered tr td.fixed {
            position: relative;
            min-height: 20px;
        }

        /* FOR SCROLLING */

        @media screen {
            html {
                margin: 0 auto;
            }

            body {
                font-size: 16px;
                width: 100%;
            }
        }

        @media print {
            button {
                display: none;
            }

            body {
   
                margin: 0;
            }

            table .bordered tr,
            table .bordered th,
            table .bordered td {
                border: 1px solid black;
            }

            tbody.bordered {
                overflow: visible;
            }

            table.bordered {
                overflow: visible;
            }


        }
    </style>
</head>
<body>
    <table>
            <tr>
                <td>
                    <div class="header-space print-margin-top">
                        <center>
                            <h1 style="float:left; text-transform:uppercase;">@yield('title')</h1>
                            <div style="float:right; margin-top: 30px">Updated as of: @yield('lastUpdatedAt')</div>
                            <div style="clear:both;"></div>
                        </center>
                    </div>
                </td>
            </tr>
                <tbody>
                    <tr>
                        <td>
                            @yield('content')
                            <br>
                            <br>
                        </td>
                    </tr>
                </tbody>
        </table>

    <script>
    $(document).ready(function () {
    $('tbody').scroll(function (e) { //detect a scroll event on the tbody
        $('thead.bordered').css("left", -$("tbody.bordered").scrollLeft()); //fix the thead relative to the body scrolling
        $('thead.bordered th.fixed').css("left", $("tbody.bordered").scrollLeft()); //fix the first cell of the header
        $('tbody.bordered td.fixed').css("left", $("tbody.bordered").scrollLeft()); //fix the first column of tdbody
    });
});
    </script>
</body>
</html>