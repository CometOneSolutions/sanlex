<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="{{ asset('css/report-responsive.css') }}" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') Inventory Summary</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            position: fixed;
            z-index: 1;
            padding-top: 100px;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgb(0, 0, 0);
            background-color: rgba(0, 0, 0, 0.4);
        }

        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px 15px 15px 15px;
            border: 1px solid #888;
            width: 40%;
        }

        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

    </style>
</head>

<body>
    <div class="main-wrapper">
        <div class="header-wrapper">
            <div class="button-wrapper">
                <h1 class="header-title">@yield('title')</h1>
                <button class="print-button">
                    PRINT
                </button>
            </div>
            <div class="header-timestamp">Updated as of: @yield('lastUpdatedAt')</div>
        </div>
        <div class="body-wrapper">
            @yield('content')
        </div>
        <script src="{{ mix('js/report.js') }}"></script>
        <script>
            function closeModal(element) {
                document.getElementById(element).style.display = 'none';
            }

            function openModal(element) {
                document.getElementById(element).style.display = "block";
            }

        </script>
</body>
</html>
