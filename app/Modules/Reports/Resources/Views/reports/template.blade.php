<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="{{ asset('css/accessory-report.css') }}" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') Inventory Summary</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
    integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
    crossorigin="anonymous"></script>

</head>
<body>
    <table>
        <tr>
            <td>
                <div class="header-space print-margin-top">
                    <center>
                        <h1 style="float:left; text-transform:uppercase;">@yield('title')</h1>
                        <div style="float:right; margin-top: 30px">Updated as of: @yield('lastUpdatedAt')</div>
                        <div style="clear:both;"></div>
                    </center>
                </div>
            </td>
        </tr>
            <tbody>
                <tr>
                    <td>
                        @yield('content')
                        <br>
                        <br>
                    </td>
                </tr>
            </tbody>
    </table>

</body>
</html>