<?php

namespace App\Modules\BackingSide\Controllers;

use App\Modules\BackingSide\Requests\StoreBackingSide;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\BackingSide\Services\BackingSideRecordService;
use App\Modules\BackingSide\Transformer\BackingSideTransformer;
use App\Modules\BackingSide\Requests\UpdateBackingSide;
use App\Modules\BackingSide\Requests\DestroyBackingSide;

class BackingSideApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        BackingSideRecordService $backingSideRecordService,
        BackingSideTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($backingSideRecordService, $transformer);
        $this->service = $backingSideRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreBackingSide $request)
    {
        $backingSide = \DB::transaction(function () use ($request) {
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->create(
                $name,

                $user
            );
        });
        return $this->response->item($backingSide, $this->transformer)->setStatusCode(201);
    }

    public function update($backingSideId, UpdateBackingSide $request)
    {
        $backingSide = \DB::transaction(function () use ($backingSideId, $request) {
            $backingSide   = $this->service->getById($backingSideId);
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->update(
                $backingSide,
                $name,

                $user
            );
        });
        return $this->response->item($backingSide, $this->transformer)->setStatusCode(200);
    }

    public function destroy($backingSideId, DestroyBackingSide $request)
    {
        $backingSide = $this->service->getById($backingSideId);
        $this->service->delete($backingSide);
        return $this->response->item($backingSide, $this->transformer)->setStatusCode(200);
    }
}
