<?php

namespace App\Modules\BackingSide\Controllers;

use JavaScript;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\BackingSide\Services\BackingSideRecordService;

class BackingSideController extends Controller
{
    private $service;

    public function __construct(BackingSideRecordService $backingSideRecordService)
    {
        $this->service = $backingSideRecordService;
        $this->middleware('permission:Read [MAS] Backing Side')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Backing Side')->only('create');
        $this->middleware('permission:Update [MAS] Backing Side')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/backing-sides'
        ]);
        return view('backing-sides.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('backing-sides.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $backingSide = $this->service->getById($id);
        return view('backing-sides.show', compact('backingSide'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $backingSide = $this->service->getById($id);
        return view('backing-sides.edit', compact('backingSide'));
    }
}
