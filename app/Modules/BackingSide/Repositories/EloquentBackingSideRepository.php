<?php

namespace App\Modules\BackingSide\Repositories;

use App\Modules\BackingSide\Models\BackingSide;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentBackingSideRepository extends EloquentRepository implements BackingSideRepository
{
    public function __construct(BackingSide $backingSide)
    {
        parent::__construct($backingSide);
    }

    public function save(BackingSide $backingSide)
    {
        return $backingSide->save();
    }

    public function delete(BackingSide $backingSide)
    {
        return $backingSide->delete();
    }
}
