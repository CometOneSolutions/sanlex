<?php

namespace App\Modules\BackingSide\Repositories;

use App\Modules\BackingSide\Models\BackingSide;
use CometOneSolutions\Common\Repositories\Repository;

interface BackingSideRepository extends Repository
{
    public function save(BackingSide $backingSide);
    public function delete(BackingSide $backingSide);
}
