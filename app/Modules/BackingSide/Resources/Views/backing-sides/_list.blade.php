<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Name</label>
            <p>@{{form.name }}</p>
        </div>
    </div>
</div>

@push('scripts')
<script src="{{ mix('js/backing-side.js') }}"></script>
@endpush