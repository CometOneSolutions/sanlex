<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'backing-sides'], function () {
        Route::get('/', 'BackingSideController@index')->name('backing-side_index');
        Route::get('/create', 'BackingSideController@create')->name('backing-side_create');
        Route::get('{backingSideId}/edit', 'BackingSideController@edit')->name('backing-side_edit');
        Route::get('{backingSideId}/print', 'BackingSideController@print')->name('backing-side_print');
        Route::get('{backingSideId}', 'BackingSideController@show')->name('backing-side_show');
    });
});
