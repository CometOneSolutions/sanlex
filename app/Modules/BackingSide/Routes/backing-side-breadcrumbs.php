<?php

Breadcrumbs::register('backing-side.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Backing Sides', route('backing-side_index'));
});
Breadcrumbs::register('backing-side.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backing-side.index');
    $breadcrumbs->push('Create', route('backing-side_create'));
});
Breadcrumbs::register('backing-side.show', function ($breadcrumbs, $backingSide) {
    $breadcrumbs->parent('backing-side.index');
    $breadcrumbs->push($backingSide->getName(), route('backing-side_show', $backingSide->getId()));
});
Breadcrumbs::register('backing-side.edit', function ($breadcrumbs, $backingSide) {
    $breadcrumbs->parent('backing-side.show', $backingSide);
    $breadcrumbs->push('Edit', route('backing-side_edit', $backingSide->getId()));
});
