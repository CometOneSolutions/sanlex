<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'backing-sides'], function () use ($api) {
        $api->get('/', 'App\Modules\BackingSide\Controllers\BackingSideApiController@index');
        $api->get('{backingSideId}', 'App\Modules\BackingSide\Controllers\BackingSideApiController@show');
        $api->post('/', 'App\Modules\BackingSide\Controllers\BackingSideApiController@store');
        $api->patch('{backingSideId}', 'App\Modules\BackingSide\Controllers\BackingSideApiController@update');
        $api->delete('{backingSideId}', 'App\Modules\BackingSide\Controllers\BackingSideApiController@destroy');
    });
});
