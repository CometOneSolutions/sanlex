<?php

namespace App\Modules\BackingSide\Models;



interface BackingSide
{
    public function getId();

    public function getName();

    public function setName($value);
}
