<?php

namespace App\Modules\BackingSide\Models;

use App\Modules\Insulation\Models\InsulationModel;
use App\Modules\MiscAccessory\Models\MiscAccessoryModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;


class BackingSideModel extends Model implements BackingSide, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'backing_sides';

    public function miscAccessories()
    {
        return $this->hasMany(MiscAccessoryModel::class, 'backing_side_id');
    }

    public function getMiscAccessories()
    {
        return $this->miscAccessories;
    }

    public function insulations()
    {
        return $this->hasMany(InsulationModel::class, 'backing_side_id');
    }

    public function getInsulations()
    {
        return $this->insulations;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
