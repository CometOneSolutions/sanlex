<?php

namespace App\Modules\BackingSide\Services;

use App\Modules\BackingSide\Models\BackingSide;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface BackingSideRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        BackingSide $backingSide,
        $name,
        User $user = null
    );

    public function delete(BackingSide $backingSide);
}
