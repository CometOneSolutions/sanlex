<?php

namespace App\Modules\BackingSide\Services;

use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\BackingSide\Repositories\BackingSideRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;
use App\Modules\Product\Traits\CanUpdateProductName;

class BackingSideRecordServiceImpl extends RecordServiceImpl implements BackingSideRecordService
{

    use CanUpdateProductName;

    private $backingSide;
    private $backingSides;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(BackingSideRepository $backingSides, BackingSide $backingSide)
    {
        $this->backingSides = $backingSides;
        $this->backingSide = $backingSide;
        parent::__construct($backingSides);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $backingSide = new $this->backingSide;
        $backingSide->setName($name);

        if ($user) {
            $backingSide->setUpdatedByUser($user);
        }
        $this->backingSides->save($backingSide);
        return $backingSide;
    }

    public function update(
        BackingSide $backingSide,
        $name,
        User $user = null
    ) {
        $tempBackingSide = clone $backingSide;
        $tempBackingSide->setName($name);
        if ($user) {
            $tempBackingSide->setUpdatedByUser($user);
        }
        $this->backingSides->save($tempBackingSide);

        $this->updateProductableProductNames([
            $tempBackingSide->getMiscAccessories(),
            $tempBackingSide->getInsulations()
        ]);

        return $tempBackingSide;
    }

    public function delete(BackingSide $backingSide)
    {
        $this->backingSides->delete($backingSide);
        return $backingSide;
    }
}
