<?php

namespace App\Modules\BackingSide\Transformer;

use App\Modules\BackingSide\Models\BackingSide;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class BackingSideTransformer extends UpdatableByUserTransformer
{
    public function transform(BackingSide $backingSide)
    {
        return [
            'id' => (int) $backingSide->getId(),
            'text' => $backingSide->getName(),
            'name' => $backingSide->getName(),
            'updatedAt' => $backingSide->updated_at,
            'editUri' => route('backing-side_edit', $backingSide->getId()),
            'showUri' => route('backing-side_show', $backingSide->getId()),
        ];
    }
}
