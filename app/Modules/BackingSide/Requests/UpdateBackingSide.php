<?php

namespace App\Modules\BackingSide\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateBackingSide extends BackingSideRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Backing Side');
    }
}
