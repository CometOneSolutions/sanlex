<?php

namespace App\Modules\BackingSide\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyBackingSide extends BackingSideRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Backing Side');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
