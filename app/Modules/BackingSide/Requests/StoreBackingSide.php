<?php

namespace App\Modules\BackingSide\Requests;

use Dingo\Api\Http\FormRequest;

class StoreBackingSide extends BackingSideRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Backing Side');
    }
}
