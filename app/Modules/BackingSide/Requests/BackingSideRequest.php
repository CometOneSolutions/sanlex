<?php

namespace App\Modules\BackingSide\Requests;

use Dingo\Api\Http\FormRequest;

class BackingSideRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'                  => 'required|unique:backing_sides,name,' . $this->route('backingSideId') ?? null,
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => 'Name is required',
            'name.unique'           => 'Name already exists'
        ];
    }

    public function getName()
    {
        return $this->input('name');
    }
}
