<?php

namespace App\Modules\BackingSide\Provider;

use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\BackingSide\Models\BackingSideModel;
use App\Modules\BackingSide\Repositories\BackingSideRepository;
use App\Modules\BackingSide\Repositories\EloquentBackingSideRepository;
use App\Modules\BackingSide\Services\BackingSideRecordService;
use App\Modules\BackingSide\Services\BackingSideRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class BackingSideServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/BackingSide/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(BackingSideRecordService::class, BackingSideRecordServiceImpl::class);
        $this->app->bind(BackingSideRepository::class, EloquentBackingSideRepository::class);
        $this->app->bind(BackingSide::class, BackingSideModel::class);
    }
}
