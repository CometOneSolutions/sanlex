<?php

namespace App\Modules\DeliveryReceiptDetails\Providers;

use App\Modules\DeliveryReceiptDetails\Models\DeliveryReceiptDetail;
use App\Modules\DeliveryReceiptDetails\Models\DeliveryReceiptDetailModel;
use App\Modules\DeliveryReceiptDetails\Repositories\DeliveryReceiptDetails;
use App\Modules\DeliveryReceiptDetails\Repositories\EloquentDeliveryReceiptDetails;
use App\Modules\DeliveryReceiptDetails\Services\DeliveryReceiptDetailRecordService;
use App\Modules\DeliveryReceiptDetails\Services\DeliveryReceiptDetailRecordServiceImpl;
use Illuminate\Support\ServiceProvider;

class DeliveryReceiptDetailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DeliveryReceiptDetail::class, DeliveryReceiptDetailModel::class);
        $this->app->bind(DeliveryReceiptDetailRecordService::class, DeliveryReceiptDetailRecordServiceImpl::class);
        $this->app->bind(DeliveryReceiptDetails::class, EloquentDeliveryReceiptDetails::class);
    }
}
