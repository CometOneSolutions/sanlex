<?php

namespace App\Modules\DeliveryReceiptDetails\Models;

use App\Modules\DeliveryReceipt\Models\DeliveryReceipt;
use App\Modules\InventoryDetail\Models\InventoryDetail;

interface DeliveryReceiptDetail
{
    public function setDeliveryReceipt(DeliveryReceipt $deliveryReceipt);

    public function getDeliveryReceipt();

    public function getDeliveryReceiptId();

    public function getOrderedQuantity();

    public function setOrderedQuantity($value);

    public function getInventoryDetail();

    public function setInventoryDetail(InventoryDetail $inventoryDetail);
}
