<?php

namespace App\Modules\DeliveryReceiptDetails\Models;

use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Inventory\Model\Inventoriable;
use App\Modules\DeliveryReceipt\Models\DeliveryReceipt;
use App\Modules\InventoryDetail\Models\InventoryDetail;
use CometOneSolutions\Inventory\Traits\HasItemMovements;
use App\Modules\DeliveryReceipt\Models\DeliveryReceiptModel;
use App\Modules\InventoryDetail\Models\InventoryDetailModel;

class DeliveryReceiptDetailModel extends Model implements DeliveryReceiptDetail, Inventoriable
{
    use HasItemMovements;

    protected $table = 'delivery_receipt_details';

    public function deliveryReceipt()
    {
        return $this->belongsTo(DeliveryReceiptModel::class, 'delivery_receipt_id');
    }

    public function inventoryDetail()
    {
        return $this->belongsTo(InventoryDetailModel::class, 'inventory_detail_id');
    }


    public function setDeliveryReceipt(DeliveryReceipt $deliveryReceipt)
    {
        $this->deliveryReceipt()->associate($deliveryReceipt);
        $this->delivery_receipt_id = $deliveryReceipt->getId();
        return $this;
    }

    public function scopeApproved($query)
    {
        return $query->whereHas('deliveryReceipt', function ($deliveryReceipt) {
            return $deliveryReceipt->whereStatus('Approved');
        });
    }

    public function getDeliveryReceipt()
    {
        return $this->deliveryReceipt;
    }

    public function getDeliveryReceiptId()
    {
        return $this->delivery_receipt_id;
    }

    public function getOrderedQuantity()
    {
        return $this->ordered_quantity;
    }

    public function getUnitPrice()
    {
        return $this->unit_price;
    }

    public function setOrderedQuantity($value)
    {
        $this->ordered_quantity = $value;
        return $this;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getInventoryDetailId()
    {
        return $this->inventory_detail_id;
    }

    public function getInventoryDetail()
    {
        return $this->inventoryDetail;
    }

    public function setInventoryDetail(InventoryDetail $inventoryDetail)
    {
        $this->inventoryDetail()->associate($inventoryDetail);
        $this->inventory_detail_id = $inventoryDetail->getId();
        return $this;
    }

    public function getInventoryRefNo()
    {
        return $this->getDeliveryReceipt()->getDrNumber();
    }

    public function getInventoryUri()
    {
        return route('delivery-receipt_show', $this->getDeliveryReceipt()->getId());
    }

    public function getApprovedDate()
    {
        return $this->getDeliveryReceipt()->getApprovedDate();
    }
}
