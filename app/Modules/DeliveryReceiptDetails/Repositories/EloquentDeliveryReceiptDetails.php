<?php

namespace App\Modules\DeliveryReceiptDetails\Repositories;

use App\Modules\DeliveryReceiptDetails\Models\DeliveryReceiptDetail;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentDeliveryReceiptDetails extends EloquentRepository implements DeliveryReceiptDetails
{
    public function __construct(DeliveryReceiptDetail $deliveryReceiptDetail)
    {
        parent::__construct($deliveryReceiptDetail);
    }

    public function save(DeliveryReceiptDetail $deliveryReceiptDetail)
    {
        $result = $deliveryReceiptDetail->save();
        return $result;
    }

    public function delete(DeliveryReceiptDetail $deliveryReceiptDetail)
    {
        return $deliveryReceiptDetail->delete();
    }
}
