<?php

namespace App\Modules\DeliveryReceiptDetails\Repositories;

use App\Modules\DeliveryReceiptDetails\Models\DeliveryReceiptDetail;
use CometOneSolutions\Common\Repositories\Repository;

interface DeliveryReceiptDetails extends Repository
{
    public function save(DeliveryReceiptDetail $deliveryReceiptDetail);

    public function delete(DeliveryReceiptDetail $deliveryReceiptDetail);
}
