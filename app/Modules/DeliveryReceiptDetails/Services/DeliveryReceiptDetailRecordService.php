<?php

namespace App\Modules\DeliveryReceiptDetails\Services;

use CometOneSolutions\Common\Services\RecordService;
use \DateTime;

interface DeliveryReceiptDetailRecordService extends RecordService
{
}

