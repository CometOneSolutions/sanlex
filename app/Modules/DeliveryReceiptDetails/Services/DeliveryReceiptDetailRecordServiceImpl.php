<?php

namespace App\Modules\DeliveryReceiptDetails\Services;

use App\Modules\DeliveryReceiptDetails\Models\DeliveryReceiptDetail;
use App\Modules\DeliveryReceiptDetails\Repositories\DeliveryReceiptDetails;
use CometOneSolutions\Common\Services\RecordServiceImpl;


class DeliveryReceiptDetailRecordServiceImpl extends RecordServiceImpl implements DeliveryReceiptDetailRecordService
{

    private $deliveryReceiptDetails;
    private $deliveryReceiptDetail;

    public function __construct(DeliveryReceiptDetails $deliveryReceiptDetails, DeliveryReceiptDetail $deliveryReceiptDetail)
    {
        parent::__construct($deliveryReceiptDetails);
        $this->deliveryReceiptDetails = $deliveryReceiptDetails;
        $this->deliveryReceiptDetail = $deliveryReceiptDetail;
    }

}
