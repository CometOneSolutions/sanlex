<?php

namespace App\Modules\DeliveryReceiptDetails\Transformers;

use App\Modules\DeliveryReceiptDetails\Models\DeliveryReceiptDetail;
use App\Modules\InventoryDetail\Transformers\InventoryDetailTransformer;
use League\Fractal;

class DeliveryReceiptDetailTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'inventoryDetail'
    ];

    public function transform(DeliveryReceiptDetail $deliveryReceiptDetail)
    {
        return [
            'id' => (int) $deliveryReceiptDetail->getId(),
            'orderedQuantity' => $deliveryReceiptDetail->getOrderedQuantity(),
            'inventoryDetailId' => $deliveryReceiptDetail->getInventoryDetailId(),
            'unit'  => $deliveryReceiptDetail->getInventoryDetail()->getUom()->getName(),
            'balance'  => $deliveryReceiptDetail->getInventoryDetail()->getQuantity() - $deliveryReceiptDetail->getOrderedQuantity(),
            'isNotValid' => false
        ];
    }

    public function includeInventoryDetail(DeliveryReceiptDetail $deliveryReceiptDetail)
    {
        return $this->item($deliveryReceiptDetail->getInventoryDetail(), new InventoryDetailTransformer);
    }
}
