<?php

namespace App\Modules\StainlessSteelType\Provider;

use App\Modules\StainlessSteelType\Models\StainlessSteelType;
use App\Modules\StainlessSteelType\Models\StainlessSteelTypeModel;
use App\Modules\StainlessSteelType\Repositories\EloquentStainlessSteelTypeRepository;
use App\Modules\StainlessSteelType\Repositories\StainlessSteelTypeRepository;
use App\Modules\StainlessSteelType\Services\StainlessSteelTypeRecordService;
use App\Modules\StainlessSteelType\Services\StainlessSteelTypeRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class StainlessSteelTypeServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/StainlessSteelType/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(StainlessSteelTypeRecordService::class, StainlessSteelTypeRecordServiceImpl::class);
        $this->app->bind(StainlessSteelTypeRepository::class, EloquentStainlessSteelTypeRepository::class);
        $this->app->bind(StainlessSteelType::class, StainlessSteelTypeModel::class);
    }
}
