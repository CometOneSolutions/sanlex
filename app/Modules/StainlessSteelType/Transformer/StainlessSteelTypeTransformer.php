<?php

namespace App\Modules\StainlessSteelType\Transformer;

use App\Modules\StainlessSteelType\Models\StainlessSteelType;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;
use League\Fractal;


class StainlessSteelTypeTransformer extends UpdatableByUserTransformer
{
    public function transform(StainlessSteelType $stainlessSteelType)
    {
        return [
            'id' => (int) $stainlessSteelType->getId(),
            'text' => $stainlessSteelType->getName(),
            'name' => $stainlessSteelType->getName(),
            'updatedAt' => $stainlessSteelType->updated_at,
            'editUri' => route('stainless-steel-type_edit', $stainlessSteelType->getId()),
            'showUri' => route('stainless-steel-type_show', $stainlessSteelType->getId()),
        ];
    }
}
