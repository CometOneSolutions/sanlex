<?php

namespace App\Modules\StainlessSteelType\Controllers;

use CometOneSolutions\Common\Controllers\ResourceApiController;
use Illuminate\Http\Request;

use App\Modules\StainlessSteelType\Services\StainlessSteelTypeRecordService;
use App\Modules\StainlessSteelType\Transformer\StainlessSteelTypeTransformer;
use App\Modules\StainlessSteelType\Requests\StoreStainlessSteelType;
use App\Modules\StainlessSteelType\Requests\UpdateStainlessSteelType;
use App\Modules\StainlessSteelType\Requests\DestroyStainlessSteelType;

class StainlessSteelTypeApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        StainlessSteelTypeRecordService $stainlessSteelTypeRecordService,
        StainlessSteelTypeTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($stainlessSteelTypeRecordService, $transformer);
        $this->service = $stainlessSteelTypeRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreStainlessSteelType $request)
    {
        $stainlessSteelType = \DB::transaction(function () use ($request) {
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->create(
                $name,
                $user
            );
        });
        return $this->response->item($stainlessSteelType, $this->transformer)->setStatusCode(201);
    }

    public function update($stainlessSteelTypeId, UpdateStainlessSteelType $request)
    {
        $stainlessSteelType = \DB::transaction(function () use ($stainlessSteelTypeId, $request) {
            $stainlessSteelType   = $this->service->getById($stainlessSteelTypeId);
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->update(
                $stainlessSteelType,
                $name,

                $user
            );
        });
        return $this->response->item($stainlessSteelType, $this->transformer)->setStatusCode(200);
    }

    public function destroy($stainlessSteelTypeId, DestroyStainlessSteelType $request)
    {
        $stainlessSteelType = $this->service->getById($stainlessSteelTypeId);
        $this->service->delete($stainlessSteelType);
        return $this->response->item($stainlessSteelType, $this->transformer)->setStatusCode(200);
    }
}
