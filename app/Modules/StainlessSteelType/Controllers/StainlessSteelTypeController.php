<?php

namespace App\Modules\StainlessSteelType\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\StainlessSteelType\Services\StainlessSteelTypeRecordService;


class StainlessSteelTypeController extends Controller
{
    private $service;

    public function __construct(StainlessSteelTypeRecordService $stainlessSteelTypeRecordService)
    {
        $this->service = $stainlessSteelTypeRecordService;
        $this->middleware('permission:Read [MAS] Stainless Steel Type')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Stainless Steel Type')->only('create');
        $this->middleware('permission:Update [MAS] Stainless Steel Type')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/stainless-steel-types'
        ]);
        return view('stainless-steel-types.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('stainless-steel-types.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $stainlessSteelType = $this->service->getById($id);
        return view('stainless-steel-types.show', compact('stainlessSteelType'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $stainlessSteelType = $this->service->getById($id);
        return view('stainless-steel-types.edit', compact('stainlessSteelType'));
    }
}
