<?php

namespace App\Modules\StainlessSteelType\Repositories;

use App\Modules\StainlessSteelType\Models\StainlessSteelType;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentStainlessSteelTypeRepository extends EloquentRepository implements StainlessSteelTypeRepository
{
    public function __construct(StainlessSteelType $stainlessSteelType)
    {
        parent::__construct($stainlessSteelType);
    }

    public function save(StainlessSteelType $stainlessSteelType)
    {
        return $stainlessSteelType->save();
    }

    public function delete(StainlessSteelType $stainlessSteelType)
    {
        return $stainlessSteelType->delete();
    }
}
