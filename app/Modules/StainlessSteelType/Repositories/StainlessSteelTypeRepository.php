<?php

namespace App\Modules\StainlessSteelType\Repositories;

use App\Modules\StainlessSteelType\Models\StainlessSteelType;
use CometOneSolutions\Common\Repositories\Repository;

interface StainlessSteelTypeRepository extends Repository
{
    public function save(StainlessSteelType $stainlessSteelType);
    public function delete(StainlessSteelType $stainlessSteelType);
}
