<?php

namespace App\Modules\StainlessSteelType\Models;

use App\Modules\StainlessSteel\Models\StainlessSteelModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Models\Users\User;

class StainlessSteelTypeModel extends Model implements StainlessSteelType, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'stainless_steel_types';

    public function stainlessSteels()
    {
        return $this->hasMany(StainlessSteelModel::class, 'stainless_steel_type_id');
    }

    public function getStainlessSteels()
    {
        return $this->stainlessSteels;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
