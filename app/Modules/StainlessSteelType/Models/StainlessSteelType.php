<?php

namespace App\Modules\StainlessSteelType\Models;



interface StainlessSteelType
{
    public function getId();

    public function getName();

    public function setName($value);
}
