<?php

namespace App\Modules\StainlessSteelType\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyStainlessSteelType extends StainlessSteelTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Stainless Steel Type');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
