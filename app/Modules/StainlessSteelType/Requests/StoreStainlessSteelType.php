<?php

namespace App\Modules\StainlessSteelType\Requests;

use Dingo\Api\Http\FormRequest;

class StoreStainlessSteelType extends StainlessSteelTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Stainless Steel Type');
    }
}
