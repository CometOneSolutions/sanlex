<?php

namespace App\Modules\StainlessSteelType\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateStainlessSteelType extends StainlessSteelTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Stainless Steel Type');
    }
}
