<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'stainless-steel-types'], function () {
        Route::get('/', 'StainlessSteelTypeController@index')->name('stainless-steel-type_index');
        Route::get('/create', 'StainlessSteelTypeController@create')->name('stainless-steel-type_create');
        Route::get('{stainlessSteelTypeId}/edit', 'StainlessSteelTypeController@edit')->name('stainless-steel-type_edit');
        Route::get('{stainlessSteelTypeId}/print', 'StainlessSteelTypeController@print')->name('stainless-steel-type_print');
        Route::get('{stainlessSteelTypeId}', 'StainlessSteelTypeController@show')->name('stainless-steel-type_show');
    });
});
