<?php

Breadcrumbs::register('stainless-steel-type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Stainless Steel Types', route('stainless-steel-type_index'));
});
Breadcrumbs::register('stainless-steel-type.create', function ($breadcrumbs) {
    $breadcrumbs->parent('stainless-steel-type.index');
    $breadcrumbs->push('Create', route('stainless-steel-type_create'));
});
Breadcrumbs::register('stainless-steel-type.show', function ($breadcrumbs, $stainlessSteelType) {
    $breadcrumbs->parent('stainless-steel-type.index');
    $breadcrumbs->push($stainlessSteelType->getName(), route('stainless-steel-type_show', $stainlessSteelType->getId()));
});
Breadcrumbs::register('stainless-steel-type.edit', function ($breadcrumbs, $stainlessSteelType) {
    $breadcrumbs->parent('stainless-steel-type.show', $stainlessSteelType);
    $breadcrumbs->push('Edit', route('stainless-steel-type_edit', $stainlessSteelType->getId()));
});
