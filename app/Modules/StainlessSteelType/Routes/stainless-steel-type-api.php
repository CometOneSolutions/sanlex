<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'stainless-steel-types'], function () use ($api) {
        $api->get('/', 'App\Modules\StainlessSteelType\Controllers\StainlessSteelTypeApiController@index');
        $api->get('{stainlessSteelTypeId}', 'App\Modules\StainlessSteelType\Controllers\StainlessSteelTypeApiController@show');
        $api->post('/', 'App\Modules\StainlessSteelType\Controllers\StainlessSteelTypeApiController@store');
        $api->patch('{stainlessSteelTypeId}', 'App\Modules\StainlessSteelType\Controllers\StainlessSteelTypeApiController@update');
        $api->delete('{stainlessSteelTypeId}', 'App\Modules\StainlessSteelType\Controllers\StainlessSteelTypeApiController@destroy');
    });
});
