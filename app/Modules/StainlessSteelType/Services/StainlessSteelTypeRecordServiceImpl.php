<?php

namespace App\Modules\StainlessSteelType\Services;

use App\Modules\Product\Traits\CanUpdateProductName;
use App\Modules\StainlessSteelType\Models\StainlessSteelType;
use App\Modules\StainlessSteelType\Repositories\StainlessSteelTypeRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class StainlessSteelTypeRecordServiceImpl extends RecordServiceImpl implements StainlessSteelTypeRecordService
{
	use CanUpdateProductName;

	private $stainlessSteelType;
	private $stainlessSteelTypes;

	protected $availableFilters = [
		["id" => "name", "text" => "Name"]
	];

	public function __construct(StainlessSteelTypeRepository $stainlessSteelTypes, StainlessSteelType $stainlessSteelType)
	{
		$this->stainlessSteelTypes = $stainlessSteelTypes;
		$this->length = $stainlessSteelType;
		parent::__construct($stainlessSteelTypes);
	}

	public function create(
		$name,
		User $user = null
	) {
		$stainlessSteelType = new $this->length;
		$stainlessSteelType->setName($name);

		if ($user) {
			$stainlessSteelType->setUpdatedByUser($user);
		}
		$this->stainlessSteelTypes->save($stainlessSteelType);
		return $stainlessSteelType;
	}

	public function update(
		StainlessSteelType $stainlessSteelType,
		$name,
		User $user = null
	) {
		$tempStainlessSteelType = clone $stainlessSteelType;
		$tempStainlessSteelType->setName($name);
		if ($user) {
			$tempStainlessSteelType->setUpdatedByUser($user);
		}
		$this->stainlessSteelTypes->save($tempStainlessSteelType);
		$this->updateProductableProductNames([
			$tempStainlessSteelType->getStainlessSteels(),
		]);

		return $tempStainlessSteelType;
	}

	public function delete(StainlessSteelType $stainlessSteelType)
	{
		$this->stainlessSteelTypes->delete($stainlessSteelType);
		return $stainlessSteelType;
	}
}
