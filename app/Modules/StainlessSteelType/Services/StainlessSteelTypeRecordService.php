<?php

namespace App\Modules\StainlessSteelType\Services;

use App\Modules\StainlessSteelType\Models\StainlessSteelType;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface StainlessSteelTypeRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        StainlessSteelType $stainlessSteelType,
        $name,
        User $user = null
    );

    public function delete(StainlessSteelType $stainlessSteelType);
}
