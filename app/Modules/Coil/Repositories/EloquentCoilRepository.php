<?php

namespace App\Modules\Coil\Repositories;

use App\Modules\Coil\Models\Coil;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentCoilRepository extends EloquentRepository implements CoilRepository
{
    public function __construct(Coil $coil)
    {
        parent::__construct($coil);
    }

    public function save(Coil $coil)
    {
        return $coil->save();
    }

    public function delete(Coil $coil)
    {
        $coil->delete();
        $coil->product->delete();
        return $coil;
    }

    protected function filterByColor($value)
    {
        return $this->model->whereHas('color', function ($color) use ($value) {
            return $color->where('name', 'LIKE', $value);
        });
    }

    protected function orderbyColorName($direction)
    {
        return $this->model->join('colors', 'colors.id', 'coils.color_id')
            ->select('colors.name as color_name', 'coils.*')
            ->orderBy('color_name', $direction);
    }

    protected function filterByProductName($value)
    {
        return $this->model->whereHas('product', function ($product) use ($value) {
            return $product->where('name', 'LIKE', $value);
        });
    }

    protected function orderByProductName($direction)
    {
        return $this->model->join('products', function ($product) {
            $product->on('products.productable_id', '=', 'coils.id')
                ->where('products.productable_type', 'App\Modules\Coil\Models\CoilModel');
        })
            ->select('products.name as product_name', 'coils.*')
            ->orderBy('product_name', $direction);
    }

    protected function filterByWidth($value)
    {
        return $this->model->whereHas('width', function ($width) use ($value) {
            return $width->where('name', 'LIKE', $value);
        });
    }

    protected function orderByWidthName($direction)
    {
        return $this->model->join('widths', 'widths.id', 'coils.width_id')
            ->select('widths.name as width_name', 'coils.*')
            ->orderBy('width_name', $direction);
    }

    protected function filterByCoating($value)
    {
        return $this->model->whereHas('coating', function ($coating) use ($value) {
            return $coating->where('name', 'LIKE', $value);
        });
    }

    protected function orderByCoatingName($direction)
    {
        return $this->model->join('coatings', 'coatings.id', 'coils.coating_id')
            ->select('coatings.name as coating_name', 'coils.*')
            ->orderBy('coating_name', $direction);
    }

    protected function filterByThickness($value)
    {
        return $this->model->whereHas('thickness', function ($thickness) use ($value) {
            return $thickness->where('name', 'LIKE', $value);
        });
    }

    protected function orderByThicknessName($direction)
    {
        return $this->model->join('thicknesses', 'thicknesses.id', 'coils.thickness_id')
            ->select('thicknesses.name as thickness_name', 'coils.*')
            ->orderBy('thickness_name', $direction);
    }
}
