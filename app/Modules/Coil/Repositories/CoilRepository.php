<?php

namespace App\Modules\Coil\Repositories;

use App\Modules\Coil\Models\Coil;
use CometOneSolutions\Common\Repositories\Repository;

interface CoilRepository extends Repository
{
    public function save(Coil $coil);
    public function delete(Coil $coil);
}
