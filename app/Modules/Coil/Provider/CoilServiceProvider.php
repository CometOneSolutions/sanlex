<?php

namespace App\Modules\Coil\Provider;

use App\Modules\Coil\Models\Coil;
use App\Modules\Coil\Models\CoilModel;
use App\Modules\Coil\Repositories\CoilRepository;
use App\Modules\Coil\Repositories\EloquentCoilRepository;
use App\Modules\Coil\Services\CoilRecordService;
use App\Modules\Coil\Services\CoilRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class CoilServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Coil/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(CoilRecordService::class, CoilRecordServiceImpl::class);
        $this->app->bind(CoilRepository::class, EloquentCoilRepository::class);
        $this->app->bind(Coil::class, CoilModel::class);
    }
}
