import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select2 from '@c1_common_js/components/Select2';
import Select3 from "@c1_common_js/components/Select3";
import { supplierService } from "@c1_module_js/Supplier/Resources/Assets/js/supplier-main";
import { colorService } from "@c1_module_js/Color/Resources/Assets/js/color-main";
import { widthService } from "@c1_module_js/Width/Resources/Assets/js/width-main";
import { coatingService } from "@c1_module_js/Coating/Resources/Assets/js/coating-main";
import { thicknessService } from "@c1_module_js/Thickness/Resources/Assets/js/thickness-main";

new Vue({
    el: '#coil',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select2, Select3,

        colorService,
        widthService,
        coatingService,
        thicknessService
    },
    data: {
        form: new Form({
            name: null,
            supplierId: null,
            colorId: null,
            widthId: null,
            coatingId: null,
            thicknessId: null
        }),
        dataInitialized: true,
        supplierUrl: '/api/suppliers?sort=name',
        defaultSelectedSupplier: {},
        supplierObj: {},

        colorUrl: '/api/colors?sort=name',
        defaultSelectedColor: {},
        colorObj: {},

        widthUrl: '/api/widths?sort=-name',
        defaultSelectedWidth: {},
        widthObj: {},

        coatingUrl: '/api/coatings?sort=name',
        defaultSelectedCoating: {},
        coatingObj: {},

        thicknessUrl: '/api/thicknesses?sort=name',
        defaultSelectedThickness: {},
        thicknessObj: {},

    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
        supplierId(val) {
            if (val) {

                this.form.get('/api/suppliers/' + val).then(response => {
                    this.supplierObj = response.data;

                }).catch(error => {
                    console.log(error);

                });

            }
        },
        colorId(val) {
            if (val) {

                this.form.get('/api/colors/' + val).then(response => {
                    this.colorObj = response.data;

                }).catch(error => {
                    console.log(error);

                });

            }
        },
        widthId(val) {
            if (val) {

                this.form.get('/api/widths/' + val).then(response => {
                    this.widthObj = response.data;

                }).catch(error => {
                    console.log(error);

                });

            }
        },
        coatingId(val) {
            if (val) {

                this.form.get('/api/coatings/' + val).then(response => {
                    this.coatingObj = response.data;

                }).catch(error => {
                    console.log(error);

                });

            }
        },
        thicknessId(val) {
            if (val) {

                this.form.get('/api/thicknesses/' + val).then(response => {
                    this.thicknessObj = response.data;

                }).catch(error => {
                    console.log(error);

                });

            }
        }

    },
    computed: {
        supplierId() {
            return this.form.supplierId;
        },
        colorId() {
            return this.form.colorId;
        },
        widthId() {
            return this.form.widthId;
        },
        coatingId() {
            return this.form.coatingId;
        },
        thicknessId() {
            return this.form.thicknessId
        },
        name() {
            if (this.supplierObj == undefined || this.colorObj == null || this.widthObj == null || this.coatingObj == null || this.thicknessObj == null) {
                return undefined;
            }
            return this.supplierObj.shortCode + '~' + this.widthObj.name + '~' + this.coatingObj.name + '~' + this.coatingObj.name + '~' + this.colorObj.name
        },
        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        destroy() {
            this.form.deleteWithConfirmation('/api/coils/' + this.form.id).then(response => {
                this.form.successModal('Coil was removed.').then(() =>
                    window.location = '/master-file/coils/'
                );
            });
        },

        store() {
            this.form.name = this.name;
            console.log(this.form.name);
            this.form.postWithModal('/api/coils', null, 'Coil was successfully saved.');
        },

        update() {
            this.form.name = this.name;
            console.log(this.form.name);
            this.form.patch('/api/coils/' + this.form.id).then(response => {
                this.form.successModal('Coil was updated.').then(() =>
                    window.location = '/master-file/coils/' + this.form.id
                );
            })
        },

        loadData(data) {

            this.form = new Form(data);
        },
    },

    created() {

        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/coils/' + id + '?include=product.supplier,color,width,coating,thickness')
                .then(response => {
                    this.loadData(response.data);
                    this.defaultSelectedSupplier = {
                        text: response.data.product.data.supplier.data.name,
                        id: response.data.supplierId,
                    };
                    this.defaultSelectedColor = {
                        text: response.data.color.data.name,
                        id: response.data.colorId,
                    };
                    this.defaultSelectedCoating = {
                        text: response.data.coating.data.name,
                        id: response.data.coatingId,
                    };
                    this.defaultSelectedThickness = {
                        text: response.data.thickness.data.name,
                        id: response.data.thicknessId,
                    };
                    this.defaultSelectedWidth = {
                        text: response.data.width.data.name,
                        id: response.data.widthId,
                    };
                    this.dataInitialized = true;
                });
        }
    },
    mounted() {
        console.log("Init coil script...");
    }
});