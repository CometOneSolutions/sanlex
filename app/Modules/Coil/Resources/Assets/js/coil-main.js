import {Form} from '@c1_common_js/components/Form';

export const coilService = new Vue({

    data: function() {
        return {
            form: new Form({}),
            uri: {
              coils: '/api/coils?limit=' + Number.MAX_SAFE_INTEGER
            },
        }
    },   

    methods: {

        getCoils(){
            return this.form.get(this.uri.coils);
        },
    }
});