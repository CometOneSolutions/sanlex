@extends('app')
@section('breadcrumbs', Breadcrumbs::render('coil.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Coils
            @if(auth()->user()->can('Create [MAS] Coil'))
                <div class="float-right">
                    <a href="{{route('coil_create')}}"><button type="button" class="btn btn-success btn-sm"><i
                                class="fa fa-plus" aria-hidden="true"></i> </button></a>
                </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sort-ascending="sortAscending" :sorter="sorter"
                v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('product-name')">
                                    Product <i class="fa" :class="getSortIcon('product-name')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('color-name')">
                                    Color <i class="fa" :class="getSortIcon('color-name')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('width-name')">
                                    Width <i class="fa" :class="getSortIcon('width-name')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('coating-name')">
                                    Coating <i class="fa" :class="getSortIcon('coating-name')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('thickness-name')">
                                    Thickness <i class="fa" :class="getSortIcon('thickness-name')"></i>
                                </a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{ item.product.data.name }}</a></td>
                            <td>@{{ item.color.data.name }}</td>
                            <td>@{{ item.width.data.name }}</td>
                            <td>@{{ item.coating.data.name }}</td>
                            <td>@{{ item.thickness.data.name }}</td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush