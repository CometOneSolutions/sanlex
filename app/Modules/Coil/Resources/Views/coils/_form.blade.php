<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
            @{{ error[0] }}
    </small>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="supplier">Supplier <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter supplier" v-model="form.supplierId"
                :selected="defaultSelectedSupplier" :url="supplierUrl" search="name" placeholder="Please select">
            </select3>
        </div>
    </div>
    <!--BLANK COL-4 DIV -->
    <div class="col-4"></div>
    <br>
    <div class="col-6">
        <div class="form-group">
            <label for="color">Color <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter color" v-model="form.colorId"
                :selected="defaultSelectedColor" :url="colorUrl" search="name" placeholder="Please select">
            </select3>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="supplier">Width <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter width" v-model="form.widthId"
                :selected="defaultSelectedWidth" :url="widthUrl" search="name" placeholder="Please select">
            </select3>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="supplier">Coating <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter coating" v-model="form.coatingId"
                :selected="defaultSelectedCoating" :url="coatingUrl" search="name" placeholder="Please select">
            </select3>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="supplier">Thickness <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter thickness" v-model="form.thicknessId"
                :selected="defaultSelectedThickness" :url="thicknessUrl" search="name" placeholder="Please select">
            </select3>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{ mix('js/coil.js') }}"></script>
@endpush