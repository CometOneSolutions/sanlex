
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Name</label>
            <p>@{{form.product.data.name}}</p>
        </div>  
    </div>

    <div class="col-3">
        <div class="form-group">
            <label for="name" class="small">Color</label>
            <p>@{{form.color.data.name}}</p>
        </div>  
    </div>

    <div class="col-3">
        <div class="form-group">
            <label for="name" class="small">Width</label>
            <p>@{{form.width.data.name}}</p>
        </div>  
    </div>

    <div class="col-3">
        <div class="form-group">
            <label for="name" class="small">Coat</label>
            <p>@{{form.coating.data.name}}</p>
        </div>  
    </div>

    <div class="col-3">
        <div class="form-group">
            <label for="name" class="small">Thickness</label>
            <p>@{{form.thickness.data.name}}</p>
        </div>  
    </div>
</div>
@push('scripts')
<script src="{{ mix('js/coil.js') }}"></script>
@endpush