<?php

namespace App\Modules\Coil\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Coil\Services\CoilRecordService;

class CoilController extends Controller
{
    private $service;

    public function __construct(CoilRecordService $coilRecordService)
    {
        $this->middleware('auth');
        $this->service = $coilRecordService;
        $this->middleware('permission:Read [MAS] Coil')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Coil')->only('create');
        $this->middleware('permission:Update [MAS] Coil')->only('edit');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/coils?include=product,color,width,coating,thickness'
        ]);
        return view('coils.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('coils.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $coil = $this->service->getById($id);
        return view('coils.show', compact('coil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $coil = $this->service->getById($id);
        return view('coils.edit', compact('coil'));
    }
}
