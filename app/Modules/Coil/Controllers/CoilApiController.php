<?php

namespace App\Modules\Coil\Controllers;

use App\Modules\Coil\Services\CoilRecordService;
use Illuminate\Http\Request;
use App\Modules\Coil\Transformer\CoilTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Coil\Requests\StoreCoil;
use App\Modules\Coil\Requests\UpdateCoil;
use App\Modules\Coil\Requests\DestroyCoil;

class CoilApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        CoilRecordService $coilRecordService,
        CoilTransformer $transformer
    ) {
        // $this->middleware('auth:api');
        $this->service = $coilRecordService;
        $this->transformer = $transformer;
        parent::__construct($coilRecordService, $transformer);
    }

    public function store(StoreCoil $request)
    {
        $coil = \DB::transaction(function () use ($request) {
            return $this->service->create(
                $request->getName(),
                $request->getSupplier(),
                $request->getColor(),
                $request->getWidth(),
                $request->getCoating(),
                $request->getThickness(),
                $request->user()
            );
        });
        return $this->response->item($coil, $this->transformer)->setStatusCode(201);
    }

    public function update(UpdateCoil $request)
    {
        $coil = \DB::transaction(function () use ($request) {
            return $this->service->update(
                $request->getCoil(),
                $request->getName(),
                $request->getSupplier(),
                $request->getColor(),
                $request->getWidth(),
                $request->getCoating(),
                $request->getThickness(),
                $request->user()
            );
        });
        return $this->response->item($coil, $this->transformer)->setStatusCode(200);
    }

    public function destroy($id, DestroyCoil $request)
    {
        $coil = \DB::transaction(function () use ($id) {
            $coil = $this->service->getById($id);
            $this->service->delete($coil);
            return $coil;
        });
        return $this->response->item($coil, $this->transformer)->setStatusCode(200);
    }
}
