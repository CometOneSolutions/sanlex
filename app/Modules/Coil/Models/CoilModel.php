<?php

namespace App\Modules\Coil\Models;

use App\Modules\Coating\Models\Coating;
use App\Modules\Coating\Models\CoatingModel;
use App\Modules\Color\Models\Color;
use App\Modules\Color\Models\ColorModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Product\Traits\HasProduct;
use App\Modules\Product\Traits\Productable;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Thickness\Models\ThicknessModel;
use App\Modules\Width\Models\Width;
use App\Modules\Width\Models\WidthModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;



class CoilModel extends Model implements Coil, UpdatableByUser, Productable
{
    use HasProduct;
    use HasUpdatedByUser;

    protected $table = 'coils';

    public function product()
    {
        return $this->morphOne(ProductModel::class, 'productable');
    }
    public function color()
    {
        return $this->belongsTo(ColorModel::class, 'color_id');
    }

    public function width()
    {
        return $this->belongsTo(WidthModel::class, 'width_id');
    }

    public function coating()
    {
        return $this->belongsTo(CoatingModel::class, 'coating_id');
    }

    public function thickness()
    {
        return $this->belongsTo(ThicknessModel::class, 'thickness_id');
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getColorId()
    {
        return $this->color_id;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getWidthId()
    {
        return $this->width_id;
    }

    public function getCoating()
    {
        return $this->coating;
    }

    public function getCoatingId()
    {
        return $this->coating_id;
    }

    public function getThickness()
    {
        return $this->thickness;
    }

    public function getThicknessId()
    {
        return $this->thickness_id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setColor(Color $color)
    {
        $this->color()->associate($color);
        $this->color_id = $color->getId();
        return $this;
    }

    public function setWidth(Width $width)
    {
        $this->width()->associate($width);
        $this->width_id = $width->getId();
        return $this;
    }

    public function setCoating(Coating $coating)
    {
        $this->coating()->associate($coating);
        $this->coating_id = $coating->getId();
        return $this;
    }

    public function setThickness(Thickness $thickness)
    {
        $this->thickness()->associate($thickness);
        $this->thickness_id = $thickness->getId();
        return $this;
    }

    public function isCoil()
    {
        return true;
    }

    public function scopeWidth($query, Width $width)
    {
        return $query->whereWidthId($width->getId());
    }

    public function scopeThickness($query, Thickness $thickness)
    {
        return $query->whereThicknessId($thickness->getId());
    }

    public function scopeColorName($query, $colorName)
    {
        return $query->whereHas('color', function ($query) use ($colorName) {
            return $query->whereName($colorName);
        });
    }

    public function scopeColor($query, Color $color)
    {
        return $query->whereColorId($color->getId());
    }

    public function scopeCoating($query, Coating $coating)
    {
        return $query->whereCoatingId($coating->getId());
    }

    public function getPendingQuantity()
    {
        return $this->getProduct()->getPendingQuantity();
    }

    public function scopeHasPending($query)
    {
        return $query->whereHas('product', function ($product) {
            return $product->hasPending();
        });
    }

    public function scopeSupplier($query, Supplier $supplier)
    {
        return $query->whereHas('product', function ($product) use ($supplier) {
            return $product->whereSupplierId($supplier->getId());
        });
    }

    public function getAssembledProductName()
    {
        $nameAttributes = [
            $this->getProduct()->getSupplier()->getShortCode(),
            $this->getCoating()->getName(),
            $this->getThickness()->getName(),
            $this->getWidth()->getName(),
            $this->getColor()->getName()
        ];

        return  strtoupper(implode("~", $nameAttributes));
    }
}
