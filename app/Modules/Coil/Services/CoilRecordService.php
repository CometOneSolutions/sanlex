<?php

namespace App\Modules\Coil\Services;

use App\Modules\Coating\Models\Coating;
use App\Modules\Coil\Models\Coil;
use App\Modules\Color\Models\Color;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface CoilRecordService extends RecordService
{
    public function create($name, Supplier $supplier, Color $color,  Width $width, Coating $coating, Thickness $thickness, User $user = null);
    public function update(Coil $coil, $name, Supplier $supplier, Color $color, Width $width, Coating $coating, Thickness $thickness, User $user = null);
    public function delete(Coil $coil);
}
