<?php

namespace App\Modules\Coil\Services;

use App\Modules\Coating\Models\Coating;
use App\Modules\Coil\Models\Coil;
use App\Modules\Coil\Repositories\CoilRepository;
use App\Modules\Color\Models\Color;
use App\Modules\Product\Models\Product;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Exceptions\GeneralApiException;
use CometOneSolutions\Common\Services\RecordServiceImpl;


class CoilRecordServiceImpl extends RecordServiceImpl implements CoilRecordService
{
    private $coil;
    private $coils;
    private $productRecordService;
    private $product;

    protected $availableFilters = [
        ["id" => "product-name", "text" => "Product"],
        ["id" => "color", "text" => "Color"],
        ["id" => "width", "text" => "Width"],
        ["id" => "coating", "text" => "Coating"],
        ["id" => "thickness", "text" => "Thickness"]
    ];

    public function __construct(CoilRepository $coils, Coil $coil, ProductRecordService $productRecordService, Product $product)
    {
        parent::__construct($coils);
        $this->coils = $coils;
        $this->coil = $coil;
        $this->productRecordService = $productRecordService;
        $this->product = $product;
    }

    public function create(
        $name,
        Supplier $supplier,
        Color $color,
        Width $width,
        Coating $coating,
        Thickness $thickness,
        User $user = null
    ) {
        $coil = new $this->coil;
        $coil->setColor($color);
        $coil->setWidth($width);
        $coil->setCoating($coating);
        $coil->setThickness($thickness);
        if ($user) {
            $coil->setUpdatedByUser($user);
        }
        $this->coils->save($coil);
        $this->productRecordService->create($name, $supplier, $coil, $user);
        return $coil;
    }


    public function update(
        Coil $coil,
        $name,
        Supplier $supplier,
        Color $color,
        Width $width,
        Coating $coating,
        Thickness $thickness,
        User $user = null
    ) {
        $tempCoil = clone $coil;
        $product = $tempCoil->getProduct();
        $tempCoil->setColor($color);
        $tempCoil->setWidth($width);
        $tempCoil->setCoating($coating);
        $tempCoil->setThickness($thickness);
        if ($user) {
            $tempCoil->setUpdatedByUser($user);
        }
        $this->coils->save($tempCoil);
        $this->productRecordService->update($product, $name, $supplier, $coil, $user);
        return $tempCoil;
    }

    public function delete(Coil $coil)
    {

        if($coil->product->receivings()->count() > 0)
        {
            throw new GeneralApiException('This product has receivings attached.');
        }
        if($coil->product->purchaseOrderDetails()->count() > 0)
        {
            throw new GeneralApiException('This product has purchase order details attached.');
        }
        if($coil->product->inventories()->count() > 0)
        {
            throw new GeneralApiException('This product has inventories attached.');
        }
        $this->coils->delete($coil);

        return $coil;
    }
}
