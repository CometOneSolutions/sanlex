<?php

namespace App\Modules\Coil\Transformer;

use App\Modules\Coating\Transformer\CoatingTransformer;
use App\Modules\Coil\Models\Coil;
use App\Modules\Color\Transformer\ColorTransformer;
use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\Thickness\Transformer\ThicknessTransformer;
use App\Modules\Width\Transformer\WidthTransformer;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class CoilTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'product', 'width', 'color', 'coating', 'thickness'
    ];

    public function transform(Coil $coil)
    {
        return [
            'id' => (int)$coil->getId(),
            'name' => $coil->getProduct() ? $coil->getProduct()->getName() : null,
            'supplierId' => $coil->getProduct() ? $coil->getProduct()->getSupplier()->getId() : null,
            'colorId' => $coil->getColorId(),
            'widthId' => $coil->getWidthId(),
            'coatingId' => $coil->getCoatingId(),
            'thicknessId' => $coil->getThicknessId(),
            'updatedAt' => $coil->updated_at,
            'createdAt' => $coil->created_at,
            'editUri' => route('coil_edit', $coil->getId()),
            'showUri' => route('coil_show', $coil->getId()),
        ];
    }

    public function includeProduct(Coil $coil)
    {
        $product = $coil->getProduct();
        if ($product) {
            return $this->item($product, new ProductTransformer);
        }
        return $this->null();
    }

    public function includeColor(Coil $coil)
    {
        $color = $coil->getColor();
        return $this->item($color, new ColorTransformer);
    }

    public function includeWidth(Coil $coil)
    {
        $width = $coil->getWidth();
        return $this->item($width, new WidthTransformer);
    }

    public function includeCoating(Coil $coil)
    {
        $coating = $coil->getCoating();
        return $this->item($coating, new CoatingTransformer);
    }

    public function includeThickness(Coil $coil)
    {
        $thickness = $coil->getThickness();
        return $this->item($thickness, new ThicknessTransformer);
    }
}
