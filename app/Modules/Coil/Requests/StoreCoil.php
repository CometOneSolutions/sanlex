<?php

namespace App\Modules\Coil\Requests;

use Dingo\Api\Http\FormRequest;

class StoreCoil extends CoilRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Coil');
    }

}
