<?php

namespace App\Modules\Coil\Requests;

use App\Modules\Coating\Services\CoatingRecordService;
use App\Modules\Coil\Services\CoilRecordService;
use App\Modules\Color\Services\ColorRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\Thickness\Services\ThicknessRecordService;
use App\Modules\Width\Services\WidthRecordService;
use Dingo\Api\Http\FormRequest;

class CoilRequest extends FormRequest
{
    private $supplierRecordService;
    private $colorRecordService;
    private $widthRecordService;
    private $coatingRecordService;
    private $thicknessRecordService;
    private $service;

    public function __construct(
        SupplierRecordService $supplierRecordService,
        ColorRecordService $colorRecordService,
        WidthRecordService $widthRecordService,
        CoatingRecordService $coatingRecordService,
        ThicknessRecordService $thicknessRecordService,
        CoilRecordService $coilRecordService
    ) {
        $this->supplierRecordService = $supplierRecordService;
        $this->colorRecordService = $colorRecordService;
        $this->widthRecordService = $widthRecordService;
        $this->coatingRecordService = $coatingRecordService;
        $this->thicknessRecordService = $thicknessRecordService;
        $this->service = $coilRecordService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $coil = $this->route('coilId') ? $this->service->getById($this->route('coilId')) : null;
        if ($coil) {
            return [
                'name'                  => 'required|unique:products,name,' . $coil->getProduct()->getId(),
                'supplierId'            => 'required',
                'colorId'               => 'required',
                'widthId'               => 'required',
                'coatingId'             => 'required',
                'thicknessId'           => 'required'
            ];
        } else {
            return [
                'name'                  => 'required|unique:products,name',
                'supplierId'            => 'required',
                'colorId'               => 'required',
                'widthId'               => 'required',
                'coatingId'             => 'required',
                'thicknessId'           => 'required'
            ];
        }
    }
    public function messages()
    {
        return [
            'name.required'         => 'Name is required.',
            'name.unique'           => 'Name already exists.',
            'supplierId.required'   => 'Supplier is required.',
            'colorId.required'      => 'Color is required.',
            'widthId.required'      => 'Width is required.',
            'coatingId.required'    => 'Coat is required.',
            'thicknessId.required'  => 'Thickness is required.'
        ];
    }


    public function getName()
    {
        $format = '%s~%s~%s~%s~%s';
        return sprintf($format, $this->getSupplier()->getShortCode(), $this->getCoating()->getName(), $this->getThickness()->getName(), $this->getWidth()->getName(),  $this->getColor()->getName());
    }

    public function getSupplier($index = 'supplierId')
    {
        return $this->supplierRecordService->getById($this->input($index));
    }

    public function getColor($index = 'colorId')
    {
        return $this->colorRecordService->getById($this->input($index));
    }

    public function getWidth($index = 'widthId')
    {
        return $this->widthRecordService->getById($this->input($index));
    }

    public function getCoating($index = 'coatingId')
    {
        return $this->coatingRecordService->getById($this->input($index));
    }

    public function getThickness($index = 'thicknessId')
    {
        return $this->thicknessRecordService->getById($this->input($index));
    }

    public function getCoil($index = 'id')
    {
        return $this->service->getById($this->input($index));
    }
}
