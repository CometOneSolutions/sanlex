<?php

namespace App\Modules\Coil\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateCoil extends CoilRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Coil');
    }

}