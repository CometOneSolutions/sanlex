<?php

namespace App\Modules\Coil\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyCoil extends CoilRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Coil');
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}