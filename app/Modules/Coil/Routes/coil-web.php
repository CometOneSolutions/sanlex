<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'coils'], function () {
        Route::get('/', 'CoilController@index')->name('coil_index');
        Route::get('/create', 'CoilController@create')->name('coil_create');
        Route::get('{coilId}/edit', 'CoilController@edit')->name('coil_edit');
        Route::get('{coilId}/print', 'CoilController@print')->name('coil_print');
        Route::get('{coilId}', 'CoilController@show')->name('coil_show');
    });

});
