<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'coils'], function () use ($api) {
        $api->get('/', 'App\Modules\Coil\Controllers\CoilApiController@index');
        $api->get('{coilId}', 'App\Modules\Coil\Controllers\CoilApiController@show');
        $api->post('/', 'App\Modules\Coil\Controllers\CoilApiController@store');
        $api->patch('{coilId}', 'App\Modules\Coil\Controllers\CoilApiController@update');
        $api->delete('{coilId}', 'App\Modules\Coil\Controllers\CoilApiController@destroy');
    });
});
