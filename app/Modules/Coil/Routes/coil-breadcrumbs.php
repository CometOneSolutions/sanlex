<?php

Breadcrumbs::register('coil.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Coils', route('coil_index'));
});
Breadcrumbs::register('coil.create', function ($breadcrumbs) {
    $breadcrumbs->parent('coil.index');
    $breadcrumbs->push('Create', route('coil_create'));
});
Breadcrumbs::register('coil.show', function ($breadcrumbs, $coil) {
    $breadcrumbs->parent('coil.index');
    $breadcrumbs->push($coil->getProduct()->getName(), route('coil_show', $coil->getId()));
});
Breadcrumbs::register('coil.edit', function ($breadcrumbs, $coil) {
    $breadcrumbs->parent('coil.show', $coil);
    $breadcrumbs->push('Edit', route('coil_edit', $coil->getId()));
});
