<?php

namespace App\Modules\PanelProfile\Requests;

use Dingo\Api\Http\FormRequest;

class UpdatePanelProfile extends PanelProfileRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Panel Profile');
    }

}