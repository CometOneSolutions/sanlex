<?php

namespace App\Modules\PanelProfile\Requests;

use Dingo\Api\Http\FormRequest;

class StorePanelProfile extends PanelProfileRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Panel Profile');
    }

}
