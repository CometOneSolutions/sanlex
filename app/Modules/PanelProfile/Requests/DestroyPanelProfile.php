<?php

namespace App\Modules\PanelProfile\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyPanelProfile extends PanelProfileRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Panel Profile');
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}