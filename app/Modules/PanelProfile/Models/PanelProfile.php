<?php

namespace App\Modules\PanelProfile\Models;


interface PanelProfile
{
    public function getId();

    public function getName();

    public function setName($value);
}
