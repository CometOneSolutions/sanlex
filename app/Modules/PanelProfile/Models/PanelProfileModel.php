<?php

namespace App\Modules\PanelProfile\Models;

use App\Modules\Skylight\Models\SkylightModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Models\Users\User;

class PanelProfileModel extends Model implements PanelProfile, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'panel_profiles';

    public function skylights()
    {
        return $this->hasMany(SkylightModel::class, 'panel_profile_id');
    }



    public function getSkylights()
    {
        return $this->skylights;
    }

    public function getSheetingReports()
    {
        return $this->sheetingReports;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
