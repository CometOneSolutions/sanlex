<?php

use Faker\Generator as Faker;
use App\Modules\PanelProfile\Models\PanelProfileModel;
use CometOneSolutions\Auth\UserModel;

$factory->define(PanelProfileModel::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->company,
        'updated_by_user_id' => $faker->randomElement(UserModel::pluck('id')->toArray()),
    ];
});
