<?php

namespace App\Modules\PanelProfile\Database\Seeds;

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

class PanelProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $suppliers = factory(PanelProfileModel::class, 5)->create();
    }
}
