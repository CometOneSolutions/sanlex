<?php

namespace App\Modules\PanelProfile\Repositories;

use App\Modules\PanelProfile\Models\PanelProfile;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentPanelProfileRepository extends EloquentRepository implements PanelProfileRepository
{
    public function __construct(PanelProfile $panelProfile)
    {
        parent::__construct($panelProfile);
    }

    public function save(PanelProfile $panelProfile)
    {
        return $panelProfile->save();
    }

    public function delete(PanelProfile $panelProfile)
    {
        return $panelProfile->delete();
    }

}
