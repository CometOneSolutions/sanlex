<?php

namespace App\Modules\PanelProfile\Repositories;

use App\Modules\PanelProfile\Models\PanelProfile;
use CometOneSolutions\Common\Repositories\Repository;

interface PanelProfileRepository extends Repository
{
    public function save(PanelProfile $panelProfile);
    public function delete(PanelProfile $panelProfile);
}
