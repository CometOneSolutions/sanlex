<?php

namespace App\Modules\PanelProfile\Provider;

use App\Modules\PanelProfile\Models\PanelProfile;
use App\Modules\PanelProfile\Models\PanelProfileModel;
use App\Modules\PanelProfile\Repositories\EloquentPanelProfileRepository;
use App\Modules\PanelProfile\Repositories\PanelProfileRepository;
use App\Modules\PanelProfile\Services\PanelProfileRecordService;
use App\Modules\PanelProfile\Services\PanelProfileRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class PanelProfileServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/PanelProfile/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(PanelProfileRecordService::class, PanelProfileRecordServiceImpl::class);
        $this->app->bind(PanelProfileRepository::class, EloquentPanelProfileRepository::class);
        $this->app->bind(PanelProfile::class, PanelProfileModel::class);
    }
}
