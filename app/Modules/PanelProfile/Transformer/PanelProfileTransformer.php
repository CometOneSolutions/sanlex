<?php

namespace App\Modules\PanelProfile\Transformer;

use App\Modules\PanelProfile\Models\PanelProfile;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class PanelProfileTransformer extends UpdatableByUserTransformer
{
    public function transform(PanelProfile $panelProfile)
    {
        return [
            'id' => (int)$panelProfile->getId(),
            'text' => $panelProfile->getName(),
            'name' => $panelProfile->getName(),
            'updatedAt' => $panelProfile->updated_at,
            'editUri' => route('panel-profile_edit', $panelProfile->getId()),
            'showUri' => route('panel-profile_show', $panelProfile->getId()),
        ];
    }
}
