<?php

namespace App\Modules\PanelProfile\Services;

use App\Modules\PanelProfile\Models\PanelProfile;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface PanelProfileRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        PanelProfile $panelProfile,
        $name,
        User $user = null
    );

    public function delete(PanelProfile $panelProfile);
}
