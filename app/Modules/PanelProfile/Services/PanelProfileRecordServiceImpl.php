<?php

namespace App\Modules\PanelProfile\Services;

use App\Modules\PanelProfile\Models\PanelProfile;
use App\Modules\PanelProfile\Repositories\PanelProfileRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class PanelProfileRecordServiceImpl extends RecordServiceImpl implements PanelProfileRecordService
{
    use CanUpdateProductName;

    private $panelProfile;
    private $panelProfiles;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(PanelProfileRepository $panelProfiles, PanelProfile $panelProfile)
    {
        $this->panelProfiles = $panelProfiles;
        $this->panelProfile = $panelProfile;
        parent::__construct($panelProfiles);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $panelProfile = new $this->panelProfile;
        $panelProfile->setName($name);
        if ($user) {
            $panelProfile->setUpdatedByUser($user);
        }
        $this->panelProfiles->save($panelProfile);
        return $panelProfile;
    }

    public function update(
        PanelProfile $panelProfile,
        $name,
        User $user = null
    ) {
        $tempPanelProfile = clone $panelProfile;
        $tempPanelProfile->setName($name);
        if ($user) {
            $tempPanelProfile->setUpdatedByUser($user);
        }
        $this->panelProfiles->save($tempPanelProfile);

        $this->updateProductableProductNames([
            $tempPanelProfile->getSkylights()
        ]);
        return $tempPanelProfile;
    }

    public function delete(PanelProfile $panelProfile)
    {
        $this->panelProfiles->delete($panelProfile);
        return $panelProfile;
    }
}
