<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'panel-profiles'], function () use ($api) {
        $api->get('/', 'App\Modules\PanelProfile\Controllers\PanelProfileApiController@index');
        $api->get('{panelProfileId}', 'App\Modules\PanelProfile\Controllers\PanelProfileApiController@show');
        $api->post('/', 'App\Modules\PanelProfile\Controllers\PanelProfileApiController@store');
        $api->patch('{panelProfileId}', 'App\Modules\PanelProfile\Controllers\PanelProfileApiController@update');
        $api->delete('{panelProfileId}', 'App\Modules\PanelProfile\Controllers\PanelProfileApiController@destroy');
    });
});
