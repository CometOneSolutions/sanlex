<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'panel-profiles'], function () {
        Route::get('/', 'PanelProfileController@index')->name('panel-profile_index');
        Route::get('/create', 'PanelProfileController@create')->name('panel-profile_create');
        Route::get('{panelProfileId}/edit', 'PanelProfileController@edit')->name('panel-profile_edit');
        Route::get('{panelProfileId}/print', 'PanelProfileController@print')->name('panel-profile_print');
        Route::get('{panelProfileId}', 'PanelProfileController@show')->name('panel-profile_show');
    });

});
