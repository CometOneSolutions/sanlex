<?php

Breadcrumbs::register('panel-profile.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Panel Profiles', route('panel-profile_index'));
});
Breadcrumbs::register('panel-profile.create', function ($breadcrumbs) {
    $breadcrumbs->parent('panel-profile.index');
    $breadcrumbs->push('Create', route('panel-profile_create'));
});
Breadcrumbs::register('panel-profile.show', function ($breadcrumbs, $panelProfile) {
    $breadcrumbs->parent('panel-profile.index');
    $breadcrumbs->push($panelProfile->getName(), route('panel-profile_show', $panelProfile->getId()));
});
Breadcrumbs::register('panel-profile.edit', function ($breadcrumbs, $panelProfile) {
    $breadcrumbs->parent('panel-profile.show', $panelProfile);
    $breadcrumbs->push('Edit', route('panel-profile_edit', $panelProfile->getId()));
});
