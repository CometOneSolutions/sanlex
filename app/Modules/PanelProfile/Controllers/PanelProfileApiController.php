<?php

namespace App\Modules\PanelProfile\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\PanelProfile\Services\PanelProfileRecordService;
use App\Modules\PanelProfile\Transformer\PanelProfileTransformer;
use App\Modules\PanelProfile\Requests\StorePanelProfile;
use App\Modules\PanelProfile\Requests\UpdatePanelProfile;
use App\Modules\PanelProfile\Requests\DestroyPanelProfile;

class PanelProfileApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        PanelProfileRecordService $panelProfileRecordService,
        PanelProfileTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($panelProfileRecordService, $transformer);
        $this->service = $panelProfileRecordService;
        $this->transformer = $transformer;
    }

    public function store(StorePanelProfile $request)
    {
        $panelProfile = \DB::transaction(function () use ($request) {
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->create(
                $name,
                $user
            );
        });
        return $this->response->item($panelProfile, $this->transformer)->setStatusCode(201);
    }

    public function update($panelProfileId, UpdatePanelProfile $request)
    {
        $panelProfile = \DB::transaction(function () use ($panelProfileId, $request) {
            $panelProfile   = $this->service->getById($panelProfileId);
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->update(
                $panelProfile,
                $name,
                $user
            );
        });
        return $this->response->item($panelProfile, $this->transformer)->setStatusCode(200);
    }

    public function destroy($panelProfileId, DestroyPanelProfile $request)
    {
        $panelProfile = $this->service->getById($panelProfileId);
        $this->service->delete($panelProfile);
        return $this->response->item($panelProfile, $this->transformer)->setStatusCode(200);
    }

}
