<?php

namespace App\Modules\PanelProfile\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\PanelProfile\Services\PanelProfileRecordService;


class PanelProfileController extends Controller
{
    private $service;

    public function __construct(PanelProfileRecordService $panelProfileRecordService)
    {
        $this->service = $panelProfileRecordService;
        $this->middleware('permission:Read [MAS] Panel Profile')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Panel Profile')->only('create');
        $this->middleware('permission:Update [MAS] Panel Profile')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/panel-profiles'
        ]);
        return view('panel-profiles.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('panel-profiles.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $panelProfile = $this->service->getById($id);
        return view('panel-profiles.show', compact('panelProfile'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $panelProfile = $this->service->getById($id);
        return view('panel-profiles.edit', compact('panelProfile'));
    }
}
