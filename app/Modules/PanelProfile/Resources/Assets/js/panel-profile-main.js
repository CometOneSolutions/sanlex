import {Form} from '@c1_common_js/components/Form';

export const panelProfileService = new Vue({

    data: function() {
        return {
            form: new Form({}),
            uri: {
              panelProfiles: '/api/panel-profiles?limit=' + Number.MAX_SAFE_INTEGER
            },
        }
    },   

    methods: {

        getPanelProfiles(){
            return this.form.get(this.uri.panelProfiles);
        },

        getPanelProfilesUsingId(id){
            return this.form.get(this.uri.panelProfiles + '&id=' + id);
        },
    }
});