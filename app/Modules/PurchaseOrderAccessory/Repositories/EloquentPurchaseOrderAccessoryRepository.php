<?php

namespace App\Modules\PurchaseOrderAccessory\Repositories;

use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;
use CometOneSolutions\Common\Repositories\EloquentRepository;


class EloquentPurchaseOrderAccessoryRepository extends EloquentRepository implements PurchaseOrderAccessoryRepository
{
	public function __construct(PurchaseOrderAccessory $purchaseOrderAccessory)
	{
		parent::__construct($purchaseOrderAccessory);
	}

	public function save(PurchaseOrderAccessory $purchaseOrderAccessory)
	{
		$result =  $purchaseOrderAccessory->save();
		$purchaseOrderAccessory->purchaseOrderAccessoryDetails()->sync($purchaseOrderAccessory->getPurchaseOrderAccessoryDetails());

		return $result;
	}

	public function delete(PurchaseOrderAccessory $purchaseOrderAccessory)
	{
		return $purchaseOrderAccessory->delete();
	}


	protected function filterBySupplierName($value)
	{
		return $this->model->whereHas('supplier', function ($supplier) use ($value) {
			return $supplier->where('name', 'LIKE', $value);
		});
	}

	protected function orderBySupplierName($direction)
	{
		return $this->model->join('suppliers', 'suppliers.id', 'purchase_order_accessories.supplier_id')
			->select('suppliers.name as supplier_name', 'purchase_order_accessories.*')
			->orderBy('supplier_name', $direction);
	}
}
