<?php

namespace App\Modules\PurchaseOrderAccessory\Repositories;

use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;
use CometOneSolutions\Common\Repositories\Repository;

interface PurchaseOrderAccessoryRepository extends Repository
{
	public function save(PurchaseOrderAccessory $purchaseOrderAccessory);
	public function delete(PurchaseOrderAccessory $purchaseOrderAccessory);
}
