@extends('app')
@section('breadcrumbs', Breadcrumbs::render('purchase-order-accessory.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Purchase Order Accessory
            @if(auth()->user()->can('Create [PUR] Purchase Order Accessory'))
            <div class="float-right">
                <a href="{{route('purchase-order-accessory_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
            </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending"
            v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
            <table class="table table-responsive-sm">
                <thead>
                    <tr>
                        <th>
                            <a v-on:click="setSorter('po_no')">
                                Purchase Order No. <i class="fas fa-sort" :class="getSortIcon('po_no')"></i>
                            </a>
                        </th>
                        <th>
                            <a v-on:click="setSorter('supplier-name')">
                                Supplier <i class="fas fa-sort" :class="getSortIcon('supplier-name')"></i>
                            </a>
                        </th>
                        <th>
                            <a v-on:click="setSorter('date')">
                                Date <i class="fas fa-sort" :class="getSortIcon('date')"></i>
                            </a></th>
                        <th class="text-center">
                            <a v-on:click="setSorter('status')">
                                Status <i class="fas fa-sort" :class="getSortIcon('status')"></i>
                            </a>
                        </th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="item in items" v-if="!isLoading">
                        <td><a :href="item.showUri">@{{ item.purchaseNumber }}</a></td>
                        <td>@{{ item.supplier.data.name }}</td>
                        <td>@{{ item.date }}</td>
                        <td class="text-center"><span :class="[item.isOpen ? 'badge badge-success' : 'badge badge-danger']">@{{item.status}}<span></td>
                        <td class="text-center">
                            <span v-if="item.isOpen">
                                <a :href="item.editUri" role="button" class="btn btn-outline-primary btn-sm"><i
                                    class="fas fa-pencil-alt"></i>
                                </a>
                            </span>
                            <span v-else>
                                <i class="far fa-times-circle text-danger"></i>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </index>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush