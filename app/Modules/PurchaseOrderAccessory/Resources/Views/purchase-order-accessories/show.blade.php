@extends('app')
@section('breadcrumbs', Breadcrumbs::render('purchase-order-accessory.show', $purchaseOrderAccessory))
@section('content')
<section id="purchase-order-accessory">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" role="tab" href="#main">General</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" role="tab" href="#receivings">Receivings</a>
                            </li>
                        </ul>
                    </div>

                <div class="card-body">
                        <div class="tab-content no-border">
                            <div class="tab-pane active" role="tabpanel" id="main">
                                <div class="text-right" >
                                    @if(auth()->user()->can('Update [PUR] Purchase Order Accessory'))
                                        <button v-if="form.isOpen" type="button" @click="close" class="btn btn-danger btn-sm"  :disabled="!form.hasReceivingAccessories || form.hasInTransitReceivingAccessories" data-toggle="tooltip"  title="Should have receiving accessories to close & receiving should be received"> 
                                            <i class="far fa-times-circle"></i> Close
                                        </button>
                                        <button v-else type="button" @click="open" class="btn btn-success btn-sm">
                                            <i class="far fa-check-circle"></i> Open
                                        </button>
        
                                        <a v-if="form.isOpen" :href="form.editUri"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> </button></a>
                                    @endif
                                </div>
                                
                                @include('purchase-order-accessories._list')
                            </div>

                            <div class="tab-pane" role="tabpanel" id="receivings">
                                @include("purchase-order-accessories.components.receiving-accessories")
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <timestamp :name="form.updatedByUser.data.name" :time="form.updatedAt"></timestamp>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection

@push('scripts')
<script src="{{ mix('js/purchase-order-accessory.js') }}"></script>
@endpush