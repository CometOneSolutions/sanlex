
<div v-for="group in groups.data" class="table-responsive-sm col-lg-12">
    <table class="table">
        <thead>
        <tr class="row">
            <th class="text-center col-12" colspan="3">@{{ group.purchaseOrderAccessoryDetail.productName }}</th>
            <template v-if="group.filteredReceivingAccessoryDetails.length > 0">
                <th class=" col-3">Reference No.</th>
                <th class=" col-3">Date</th>
                <th class=" col-3">Status</th>
                <th class=" col-3">Quantity</th>
            </template>
        </tr>
        </thead>
        <tbody>
        <template v-if="group.filteredReceivingAccessoryDetails.length > 0">
            <template  v-for="receivingAccessoryDetail in group.filteredReceivingAccessoryDetails">
                <tr class="row">
                    <td class="col-3">
                        <a :href="receivingAccessoryDetail.receivingAccessory.data.showUri">
                            @{{ receivingAccessoryDetail.receivingAccessory.data.referenceNo }}    
                        </a>
                    </td>
                    <td class="col-3">
                        @{{ receivingAccessoryDetail.receivingAccessory.data.date }}    
                    </td>
                    <td class="col-3">
                        <span :class="[receivingAccessoryDetail.receivingAccessory.data.isInTransit ? 'badge badge-warning' : 'badge badge-success']">
                            @{{ receivingAccessoryDetail.receivingAccessory.data.status }}
                        </span>    
                    </td>
                    <td class="col-3">
                        @{{receivingAccessoryDetail.quantity | numeric}}
                    </td>
                    
                </tr>
            </template>
        </template>
        <template v-else>
                <tr class="row">
                    <td class="col-12 text-center"> 
                        No Receivings found
                    </td>
                </tr>
            </template>
        </tbody>
    </table>
    <br><br>
</div>