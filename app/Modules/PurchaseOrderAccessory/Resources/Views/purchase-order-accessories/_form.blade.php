<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">PO Reference <span class="text text-danger">*</span></label>
        <input type="text" class="form-control" id="purchaseNumber" name="purchaseNumber" v-model="form.purchaseNumber" placeholder="" :class="{'is-invalid': form.errors.has('purchaseNumber') }" required data-msg-required="Enter PO reference">
    </div>

</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Date <span class="text text-danger">*</span></label>
        <datepicker :typeable="true" :input-class="{'is-invalid': form.errors.has('date'),'form-control': true}" v-model="form.date" :required="true" disabled></datepicker>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Supplier <span class="text text-danger">*</span></label>

        <select3 class="custom-select" required data-msg-required="Enter supplier" v-model="form.supplierId" :selected="defaultSelectedSupplier" :url="url" search="name" :disabled="editMode" @change="loadFilteredProduct">
        </select3>
    </div>
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Product Type <span class="text text-danger">*</span></label>
        <select class="form-control" :allow-custom="false" id="type" name="type" v-model="form.productType" required :class="{'is-invalid': form.errors.has('type') }" required data-msg-required="Enter supplier" @change="loadFilteredProduct" :disabled="editMode">
            <option disabled value='' selected>Please select</option>
            <option value="Accessories">Accessory</option>
            <option value="Fasteners">Fastener</option>
            <option value="Insulations">Insulation</option>
            <option value="Misc Accessories">Misc Accessory</option>
            <option value="Paint Adhesives">Paint Adhesive</option>
            <option value="Pvc Fittings">Pvc Fitting</option>
            <option value="Skylights">Skylight</option>
            <option value="Stainless Steels">Stainless Steel</option>
            <option value="Structural Steels">Structural Steel</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-12">
        <label for="inputEmail4" class="col-form-label">Remarks</label>
        <input type="text" class="form-control" id="remarks" v-model="form.remarks" placeholder="">
    </div>
</div>

<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr class="row">
                    <th class="col-8">Product</th>
                    <th class="col-3 text-right">Quantity</th>
                    <th class="col-1">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr v-if="form.purchaseOrderAccessoryDetails.data" is="purchase-order-accessory-detail" class="row" v-for="(detail,index) in form.purchaseOrderAccessoryDetails.data" :detail="detail" :index="index" :products="products" v-on:remove="form.purchaseOrderAccessoryDetails.data.splice(index, 1)">
                </tr>
                <tr class="row">
                    <td class="col-12" colspan="12" v-if="productsInitialized">
                        <button type="button" class="btn btn-success btn-sm" @click="add">Add New</button>
                    </td>
                    <td class="col-12" colspan="12" v-else>
                        Please select both supplier and product type first.
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

</div>


@push('scripts')
<script src="{{ mix('js/purchase-order-accessory.js') }}"></script>
@endpush
