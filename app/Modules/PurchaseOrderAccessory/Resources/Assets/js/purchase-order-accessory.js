import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm';
import Select2 from '@c1_common_js/components/Select2';
import Select3 from "@c1_common_js/components/Select3";
import Datepicker from 'vuejs-datepicker';

import { productService } from "@c1_module_js/Product/Resources/Assets/js/product-main";
import { supplierService } from "@c1_module_js/Supplier/Resources/Assets/js/supplier-main";

import PurchaseOrderAccessoryDetail from '../components/PurchaseOrderAccessoryDetail';

const COIL = 'Coils';
const ACCESSORY = 'Accessories';
const SKYLIGHT = 'Skylights';
const INSULATION = 'Insulations';
const SCREW = 'Screws';
const SS = 'Stainless Steels';
const MISC = 'Misc Accessories';
const ST = 'Structural Steels';
const PAINT = 'Paint Adhesives';
const FASTENER = 'Fasteners';
const PVC = 'Pvc Fittings';

new Vue({
    el: '#purchase-order-accessory',
    components: {
        SaveButton,
        DeleteButton,
        Timestamp,
        VForm,
        Select2,
        Select3,
        productService,
        supplierService,
        Datepicker,
        PurchaseOrderAccessoryDetail
    },

    data: {
        defaultSelectedSupplier: {},
        form: new Form({
            id: null,
            purchaseNumber: null,
            supplierId: '',
            date: moment(),
            remarks: null,
            status: 'In Transit',
            productType: '',
            purchaseOrderAccessoryDetails: {
                data: []
            },
        }),
        counter: 0,
        isSaving: false,
        isBusy: false,
        url: '/api/suppliers?sort=name',
        dataInitialized: true,
        products: [],
        productsInitialized: false,
        suppliers: [],
        suppliersInitialized: false,
        groups: {
            data: []
        },
        editMode: false

    },

    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },

    computed: {

        initializationComplete() {
            return this.dataInitialized;
        },
    },

    methods: {
        loadFilteredProduct() {
            if (!this.editMode) {
                this.form.purchaseOrderAccessoryDetails.data = [];
            }

            if (this.form.productType && this.form.supplierId) {
                if (this.form.productType == ACCESSORY) {
                    this.products = productService.getAccessoryOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == SS) {
                    this.products = productService.getStainlessSteelOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == SKYLIGHT) {
                    this.products = productService.getSkylightOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == INSULATION) {
                    this.products = productService.getInsulationOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == SCREW) {
                    this.products = productService.getScrewOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == MISC) {
                    this.products = productService.getMiscAccessoryOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == ST) {
                    this.products = productService.getStructuralSteelOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == PAINT) {
                    this.products = productService.getPaintAdhesiveOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == FASTENER) {
                    this.products = productService.getFastenerOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == PVC) {
                    this.products = productService.getPvcFittingOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
            }
        },
        add() {
            this.form.purchaseOrderAccessoryDetails.data.push({
                id: --this.counter,
                productId: null,
                quantity: '',
            });
        },
        destroy() {
            this.form.confirm().then((result) => {
                if (result.value) {

                    this.form.delete('/api/purchase-order-accessories/' + this.form.id).then(response => {
                        this.isDeleting = false;
                        this.isBusyDeleting = false;
                        this.$swal({
                            title: 'Success',
                            text: 'Purchase order was successfully deleted.',
                            type: 'success'
                        }).then(() => window.location = '/purchasing/purchase-order-accessories');

                    })
                }
            });
        },

        store() {
            this.form.post('/api/purchase-order-accessories').then(response => {
                this.$swal({
                    title: 'Success',
                    text: 'Purchase order was saved.',
                    type: 'success'
                });
                this.form.reset();
            });
        },

        approve() {
            this.form.affirm().then((result) => {
                if (result.value) {
                    this.form.patch('/api/purchase-order-accessories/approve/' + id).then(
                        response => {
                            this.$swal({
                                title: 'Success',
                                text: 'Purchase Order approved.',
                                type: 'success'
                            }).then(() => window.location = '/purchasing/purchase-order-accessories/' + id);
                        })
                }
            });
        },

        update() {

            this.form.patch('/api/purchase-order-accessories/' + this.form.id).then(response => {
                this.isSaving = false;
                this.isBusy = false;
                this.form.updated_by = response.data.updated_by;
                this.form.updated_at = response.data.updated_at;
                this.$swal({
                    title: 'Success',
                    text: 'Purchase order was successfully updated.',
                    type: 'success'
                }).then(() => window.location = '/purchasing/purchase-order-accessories/' + response.data.id);

            }).catch(error => {
                this.isSaving = false;
                this.isBusy = false;
            });
        },

        handleFileChange(e) {
            this.form.file = this.$refs.file.files[0];
        },
        addFile() {

            let formData = new FormData();
            formData.append('file', this.form.file);
            formData.append('userId', this.form.updatedByUser.data.id);
            this.isSaving = true;
            this.isBusy = true;

            this.form.postImage('/api/purchase-order-accessories/' + this.form.id + '/upload', formData)

                .then(response => {
                    this.isSaving = false;
                    this.isBusy = false;
                    this.$swal({
                        title: 'Success',
                        text: 'File was successfully uploaded.',
                        type: 'success'
                    }).then(() => {
                        window.location = '/purchasing/purchase-order-accessories/' + this.form.id;
                    });


                }).catch(error => {
                    this.isSaving = false;
                    this.isBusy = false;
                });


        },
        removeFile() {

            document.getElementById("uploadFile").value = "";
        },

        open() {
            this.form.warningGenericModal('Open Purchase Order?').then((result) => {
                if (result.value) {
                    this.form.patch('/api/purchase-order-accessories/open/' + id).then(
                        response => {
                            this.$swal({
                                title: 'Success',
                                text: 'Purchase Order Accessory Opened.',
                                type: 'success'
                            }).then(() => window.location = '/purchasing/purchase-order-accessories/' + id);
                        })
                }
            });
        },

        close() {
            this.form.warningGenericModal('Close Purchase Order?').then((result) => {
                if (result.value) {
                    this.form.patch('/api/purchase-order-accessories/close/' + id).then(
                        response => {
                            this.$swal({
                                title: 'Success',
                                text: 'Purchase Order Accessory Closed.',
                                type: 'success'
                            }).then(() => window.location = '/purchasing/purchase-order-accessories/' + id);
                        })
                }
            });
        },

        groupReceivingAccessoryDetailsByPurchaseAccessoryDetails() {
            let purchaseOrderAccessoryDetails = this.form.purchaseOrderAccessoryDetails.data;
            let receivingAccessoryDetails = this.form.receivingAccessoryDetails.data;
            let self = this;

            purchaseOrderAccessoryDetails.forEach(function (purchaseOrderAccessoryDetail) {
                let filteredReceivingAccessoryDetails = receivingAccessoryDetails.filter(function (receivingAccessoryDetail) {
                    return receivingAccessoryDetail.productId === purchaseOrderAccessoryDetail.productId;
                });

                self.groups.data.push({
                    purchaseOrderAccessoryDetail,
                    filteredReceivingAccessoryDetails
                });

            });

        },


        loadData(data) {
            this.form = new Form(data);
            this.groupReceivingAccessoryDetailsByPurchaseAccessoryDetails();

            this.loadFilteredProduct();
        },
    },
    created() {

        if (id != null) {
            this.dataInitialized = false;
            this.form.isInitializing = true;
            this.editMode = true;
            this.form.get('/api/purchase-order-accessories/' + id + '?include=supplier,purchaseOrderAccessoryDetails,purchaseOrderAccessoryDetails.product,receivingAccessoryDetails,receivingAccessoryDetails.receivingAccessory')
                .then(response => {
                    this.loadData(response.data);
                    this.defaultSelectedSupplier = {
                        text: response.data.supplier.data.name,
                        id: response.data.supplierId,
                    };
                    this.dataInitialized = true;
                });
        }

    },

    mounted() {
        console.log("Init purchase script...");
    }
});