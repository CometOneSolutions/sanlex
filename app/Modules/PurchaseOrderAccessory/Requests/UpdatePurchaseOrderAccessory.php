<?php

namespace App\Modules\PurchaseOrderAccessory\Requests;

use Dingo\Api\Http\FormRequest;

class UpdatePurchaseOrderAccessory extends PurchaseOrderAccessoryRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [PUR] Purchase Order Accessory');
	}
}
