<?php

namespace App\Modules\PurchaseOrderAccessory\Requests;

use App\Modules\Product\Services\ProductRecordService;
use App\Modules\PurchaseOrderAccessoryDetail\Models\PurchaseOrderAccessoryDetailModel;
use App\Modules\PurchaseOrderAccessoryDetail\Services\PurchaseOrderAccessoryDetailRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\Uom\Services\UomRecordService;
use Dingo\Api\Http\FormRequest;
use DateTime;

class PurchaseOrderAccessoryRequest extends FormRequest
{

	protected $supplierRecordService;
	protected $uomRecordService;
	protected $productRecordService;
	protected $purchaseOrderAccessoryDetailRecordService;

	public function __construct(
		SupplierRecordService $supplierRecordService,
		UomRecordService $uomRecordService,
		ProductRecordService $productRecordService,
		PurchaseOrderAccessoryDetailRecordService $purchaseOrderAccessoryDetailRecordService
	) {
		$this->supplierRecordService = $supplierRecordService;
		$this->uomRecordService = $uomRecordService;
		$this->purchaseOrderAccessoryDetailRecordService = $purchaseOrderAccessoryDetailRecordService;
		$this->productRecordService = $productRecordService;
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	public function getSupplier($index = 'supplierId')
	{
		return $this->supplierRecordService->getById($this->input($index));
	}

	public function getDate($index = 'date')
	{
		return new DateTime($this->input($index));
	}

	public function getRemarks($index = 'remarks')
	{
		return $this->input($index) ?? null;
	}

	public function getProductType($index = 'productType')
	{
		return $this->input($index);
	}

	public function getPONumber($index = 'purchaseNumber')
	{
		return $this->input($index);
	}

	public function getPurchaseOrderAccessoryDetails($index = 'purchaseOrderAccessoryDetails.data')
	{
		return array_map(function ($itemArray) {
			if (isset($itemArray['id']) && $itemArray['id'] > 0) {
				$purchaseOrderAccessoryDetail = $this->purchaseOrderAccessoryDetailRecordService->getById($itemArray['id']);
			} else {
				$purchaseOrderAccessoryDetail = new PurchaseOrderAccessoryDetailModel();
			}

			$product = $this->productRecordService->getById($itemArray['productId']);
			$uom = $product->getUom();
			$purchaseOrderAccessoryDetail->setUom($uom);
			$purchaseOrderAccessoryDetail->setProduct($product);
			$purchaseOrderAccessoryDetail->setOrderedQuantity($itemArray['quantity']);
			return $purchaseOrderAccessoryDetail;
		}, $this->sumQuantityOfDuplicateProducts($this->input($index)));
	}

	public function rules()
	{
		return [
			'purchaseNumber' => 'required|unique:purchase_order_accessories,po_no,' . $this->route('purchaseOrderAccessoryId') ?? null,
			'supplierId'     => 'required',
			'date'           => 'required'
		];
	}

	public function messages()
	{
		return [
			'purchaseNumber.required'           => 'PO reference is required.',
			'purchaseNumber.unique'             => 'PO reference already exists.',
			'supplierId'            => 'Supplier is required.',
			'date'                  => 'Date is required',
		];
	}

	private function sumQuantityOfDuplicateProducts(array $items)
	{
		foreach ($items as $outerLoopKey => &$outerLoopValue) {
			foreach ($items as $innerLoopKey => $innerLoopValue) {
				if (
					($outerLoopValue['productId'] === $innerLoopValue['productId']) &&
					$outerLoopKey !== $innerLoopKey
				) {
					$outerLoopValue['quantity'] += $innerLoopValue['quantity'];
					unset($items[$innerLoopKey]);
				}
			}
		}

		return $items;
	}
}
