<?php

namespace App\Modules\PurchaseOrderAccessory\Requests;

use Dingo\Api\Http\FormRequest;

class StorePurchaseOrderAccessory extends PurchaseOrderAccessoryRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [PUR] Purchase Order Accessory');
	}
}
