<?php

namespace App\Modules\PurchaseOrderAccessory\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyPurchaseOrderAccessory extends PurchaseOrderAccessoryRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [PUR] Purchase Order Accessory');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
