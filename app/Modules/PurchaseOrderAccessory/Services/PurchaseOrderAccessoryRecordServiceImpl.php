<?php

namespace App\Modules\PurchaseOrderAccessory\Services;

use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;
use App\Modules\PurchaseOrderAccessory\Repositories\PurchaseOrderAccessoryRepository;
use App\Modules\ReceivingAccessory\Models\ReceivingAccessory;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Exceptions\GeneralApiException;
use DateTime;

class PurchaseOrderAccessoryRecordServiceImpl extends RecordServiceImpl implements PurchaseOrderAccessoryRecordService
{
	private $purchaseOrderAccessory;
	private $purchaseOrderAccessories;

	protected $availableFilters = [
		['id' => 'po_no', 'text' => 'Purchase Order Number'],
		['id' => 'date', 'text' => 'Date'],
		['id' => 'supplier_name', 'text' => 'Supplier'],
		['id' => 'status', 'text' => 'Status'],
	];

	public function __construct(
		PurchaseOrderAccessoryRepository $purchaseOrderAccessories,
		PurchaseOrderAccessory $purchaseOrderAccessory
	) {
		$this->purchaseOrderAccessories = $purchaseOrderAccessories;
		$this->purchaseOrderAccessory = $purchaseOrderAccessory;
		parent::__construct($purchaseOrderAccessories);
	}

	public function create(
		$purchaseOrderNumber,
		Supplier $supplier,
		$productType,
		DateTime $date,
		$remarks,
		array $purchaseOrderAccessoryDetails = [],
		User $user = null
	) {
		$purchaseOrderAccessory = $this->purchaseOrderAccessory;
		$purchaseOrderAccessory->setPONumber($purchaseOrderNumber);
		$purchaseOrderAccessory->setSupplier($supplier);
		$purchaseOrderAccessory->setProductType($productType);
		$purchaseOrderAccessory->setDate($date);
		$purchaseOrderAccessory->setRemarks($remarks);
		$purchaseOrderAccessory->setStatus(PurchaseOrderAccessory::STATUS_OPEN);
		$purchaseOrderAccessory->setPurchaseOrderAccessoryDetails($purchaseOrderAccessoryDetails);

		if ($user) {
			$purchaseOrderAccessory->setUpdatedByUser($user);
		}

		$this->purchaseOrderAccessories->save($purchaseOrderAccessory);
		return $purchaseOrderAccessory;
	}

	public function update(
		PurchaseOrderAccessory $purchaseOrderAccessory,
		$purchaseOrderNumber,
		Supplier $supplier,
		$productType,
		DateTime $date,
		$remarks,
		array $purchaseOrderAccessoryDetails = [],
		User $user = null
	) {
		$tmpPurchaseOrderAccessory = clone $purchaseOrderAccessory;
		$tmpPurchaseOrderAccessory->setPONumber($purchaseOrderNumber);
		$tmpPurchaseOrderAccessory->setSupplier($supplier);
		$tmpPurchaseOrderAccessory->setProductType($productType);
		$tmpPurchaseOrderAccessory->setDate($date);
		$tmpPurchaseOrderAccessory->setPurchaseOrderAccessoryDetails($purchaseOrderAccessoryDetails);
		$tmpPurchaseOrderAccessory->setRemarks($remarks);

		if ($user) {
			$tmpPurchaseOrderAccessory->setUpdatedByUser($user);
		}

		$this->purchaseOrderAccessories->save($tmpPurchaseOrderAccessory);
		return $tmpPurchaseOrderAccessory;
	}

	public function delete(PurchaseOrderAccessory $purchaseOrderAccessory)
	{
		if ($this->canDelete($purchaseOrderAccessory)) {
			$purchaseOrderAccessory = $this->deleteReceivingAccessories($purchaseOrderAccessory);

			$this->purchaseOrderAccessories->delete($purchaseOrderAccessory);
			return $purchaseOrderAccessory;
		}
	}

	private function canDelete(PurchaseOrderAccessory $purchaseOrderAccessory)
	{
		if ($purchaseOrderAccessory->hasReceivedReceivingAccessories()) {
			throw new GeneralApiException('Already has received receivings.');
		}

		return true;
	}

	private function deleteReceivingAccessories(PurchaseOrderAccessory $purchaseOrderAccessory)
	{
		$purchaseOrderAccessory->receivingAccessories()->delete();
		return $purchaseOrderAccessory;
	}


	public function open(
		PurchaseOrderAccessory $purchaseOrderAccessory,
		User $user = null
	) {
		$tmpPurchaseOrder = clone $purchaseOrderAccessory;

		$tmpPurchaseOrder->setUpdatedByUser($user);
		$tmpPurchaseOrder->timestamps = false;
		$tmpPurchaseOrder->setStatus(PurchaseOrderAccessory::STATUS_OPEN);

		$this->purchaseOrderAccessories->save($tmpPurchaseOrder);

		return $tmpPurchaseOrder;
	}

	public function close(
		PurchaseOrderAccessory $purchaseOrderAccessory,
		User $user = null
	) {
		$tmpPurchaseOrder = clone $purchaseOrderAccessory;
		if ($user) {
			$tmpPurchaseOrder->setUpdatedByUser($user);
		}

		$tmpPurchaseOrder->timestamps = false;
		$tmpPurchaseOrder->setStatus(PurchaseOrderAccessory::STATUS_CLOSED);

		$this->purchaseOrderAccessories->save($tmpPurchaseOrder);

		return $tmpPurchaseOrder;
	}

	public function closeIfQuantitiesAreFulfilled(ReceivingAccessory $receivingAccessory)
	{
		$purchaseOrderAccessory = $receivingAccessory->getPurchaseOrderAccessory();

		$allPOAccessoryDetailsAreFulfilled = $purchaseOrderAccessory->getPurchaseOrderAccessoryDetails()->every(function ($purchaseOrderAccessoryDetail) use ($purchaseOrderAccessory) {
			return $purchaseOrderAccessory->getPurchaseOrderAccessoryDetailBalanceQuantity($purchaseOrderAccessoryDetail->getProductId()) <= 0;
		});

		if ($allPOAccessoryDetailsAreFulfilled) {
			$this->close($purchaseOrderAccessory);
		}
	}
}
