<?php

namespace App\Modules\PurchaseOrderAccessory\Services;

use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;
use DateTime;

interface PurchaseOrderAccessoryRecordService extends RecordService
{
	public function create(
		$purchaseOrderNumber,
		Supplier $supplier,
		$productType,
		DateTime $date,
		$remarks,
		array $purchaseOrderAccessoryDetails = [],
		User $user = null
	);

	public function update(
		PurchaseOrderAccessory $purchaseOrderAccessory,
		$purchaseOrderNumber,
		Supplier $supplier,
		$productType,
		DateTime $date,
		$remarks,
		array $purchaseOrderAccessoryDetails = [],
		User $user = null
	);

	public function delete(PurchaseOrderAccessory $purchaseOrderAccessory);
}
