<?php

namespace App\Modules\PurchaseOrderAccessory\Services;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\PurchaseOrderAccessory\Export\PurchaseOrderAccessoryTemplateExport;
use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;
use \DateTime;
use Carbon\Carbon;

class PurchaseOrderAccessoryReportGenerator
{

    protected $productAccessoryTemplateView = 'products.product-template.print-accessory';
    protected $productCoilTemplateView = 'products.product-template.print-coil';
    protected $productStainlessTemplateView = 'products.product-template.print-stainless';
    protected $productInsulationTemplateView = 'products.product-template.print-insulation';
    protected $productScrewTemplateView = 'products.product-template.print-screw';
    protected $productSkylightTemplateView = 'products.product-template.print-skylight';
    protected $productMiscAccessoryTemplateView = 'products.product-template.print-misc-accessory';
    protected $productStructuralTemplateView = 'products.product-template.print-structural';
    protected $productPaintAdhesiveTemplateView = 'products.product-template.print-paint-adhesive';
    protected $productFastenerTemplateView = 'products.product-template.print-fastener';
    protected $productPvcFittingTemplateView = 'products.product-template.print-pvc-fitting';
    protected $purchaseOrderAccessory;
    protected $templateView;
    protected $templateFileName;
    protected $productType;

    public function __construct(PurchaseOrderAccessory $purchaseOrderAccessory)
    {
        $this->purchaseOrderAccessory = $purchaseOrderAccessory;
        $this->productType = $purchaseOrderAccessory->getProductType();
        $this->templateView = $this->initTemplateView($this->purchaseOrderAccessory);
        $this->templateFileName = $this->initTemplateFileName($this->productType);
    }

    public static function generate(DateTime $date)
    {
        return new PurchaseOrderAccessoryTemplateExport($data);
    }

    protected function initTemplateView(PurchaseOrderAccessory $purchaseOrderAccessory)
    {
        switch ($purchaseOrderAccessory->getProductType()) {
            case PurchaseOrder::SS:
                return $this->productStainlessTemplateView;
                break;
            case PurchaseOrder::INSULATION:
                return $this->productInsulationTemplateView;
                break;
            case PurchaseOrder::SCREW:
                return $this->productScrewTemplateView;
                break;
            case PurchaseOrder::SKYLIGHT:
                return $this->productSkylightTemplateView;
                break;
            case PurchaseOrder::MISC:
                return $this->productMiscAccessoryTemplateView;
                break;
            case PurchaseOrder::ST:
                return $this->productStructuralTemplateView;
                break;
            case PurchaseOrder::PAINT:
                return $this->productPaintAdhesiveTemplateView;
                break;
            case PurchaseOrder::FASTENER:
                return $this->productFastenerTemplateView;
                break;
            case PurchaseOrder::PVC:
                return $this->productPvcFittingTemplateView;
                break;
        }
    }

    protected function initTemplateFileName($productType)
    {
        $name = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $productType);
        $format = $name . 'Product-Template%s.xls';
        $current = Carbon::now()->format('YmdHs');

        return sprintf($format, $current);
    }

    public function getTemplateView()
    {
        return $this->templateView;
    }

    public function getTemplateFileName()
    {
        return $this->templateFileName;
    }
}
