<?php

namespace App\Modules\PurchaseOrderAccessory\Transformer;

use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;
use App\Modules\PurchaseOrderAccessoryDetail\Transformer\PurchaseOrderAccessoryDetailTransformer;
use App\Modules\ReceivingAccessory\Transformer\ReceivingAccessoryTransformer;
use App\Modules\ReceivingAccessoryDetail\Transformer\ReceivingAccessoryDetailTransformer;
use App\Modules\Supplier\Transformer\SupplierTransformer;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class PurchaseOrderAccessoryTransformer extends UpdatableByUserTransformer
{
	protected $availableIncludes = [
		'purchaseOrderAccessoryDetails',
		'supplier',
		'receivingAccessories',
		'receivingAccessoryDetails'
	];

	public function __construct()
	{
		$this->defaultIncludes = array_merge($this->defaultIncludes, []);
	}

	public function transform(PurchaseOrderAccessory $purchaseOrderAccessory)
	{
		return [
			'id'						=> (int) $purchaseOrderAccessory->getId(),
			'text'						=> $purchaseOrderAccessory->getPONumber(),
			'date'						=> $purchaseOrderAccessory->getDate()->toDateString(),
			'supplierId'				=> $purchaseOrderAccessory->getSupplierId(),
			'status'					=> $purchaseOrderAccessory->getStatus(),
			'productType'				=> $purchaseOrderAccessory->getProductType(),
			'purchaseNumber'			=> $purchaseOrderAccessory->getPONumber(),
			'remarks'					=> $purchaseOrderAccessory->getRemarks(),
			'isOpen'					=> (bool) $purchaseOrderAccessory->isOpen(),
			'hasReceivingAccessories'	=> (bool) $purchaseOrderAccessory->hasReceivingAccessories(),
			'hasInTransitReceivingAccessories'	=> (bool) $purchaseOrderAccessory->hasInTransitReceivingAccessories(),
			'showUri'					=> route('purchase-order-accessory_show', $purchaseOrderAccessory->id),
			'editUri'					=> route('purchase-order-accessory_edit', $purchaseOrderAccessory->id),
			'updatedAt'					=> $purchaseOrderAccessory->updated_at,
		];
	}

	public function includePurchaseOrderAccessoryDetails(PurchaseOrderAccessory $purchaseOrderAccessory)
	{
		$purchaseOrderAccessoryDetails = $purchaseOrderAccessory->getPurchaseOrderAccessoryDetails();
		return $this->collection($purchaseOrderAccessoryDetails, new PurchaseOrderAccessoryDetailTransformer);
	}

	public function includeSupplier(PurchaseOrderAccessory $purchaseOrderAccessory)
	{
		$supplier = $purchaseOrderAccessory->getSupplier();
		return $this->item($supplier, new SupplierTransformer);
	}

	public function includeReceivingAccessories(PurchaseOrderAccessory $purchaseOrderAccessory)
	{
		$receivingAccessories = $purchaseOrderAccessory->getReceivingAccessories();
		return $this->collection($receivingAccessories, new ReceivingAccessoryTransformer);
	}

	public function includeReceivingAccessoryDetails(PurchaseOrderAccessory $purchaseOrderAccessory)
	{
		$receivingAccessoryDetails = $purchaseOrderAccessory->getReceivingAccessoryDetails();
		return $this->collection($receivingAccessoryDetails, new ReceivingAccessoryDetailTransformer);
	}
}
