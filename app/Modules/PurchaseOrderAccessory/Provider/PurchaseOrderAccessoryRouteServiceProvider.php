<?php

namespace App\Modules\PurchaseOrderAccessory\Provider;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class PurchaseOrderAccessoryRouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Modules\PurchaseOrderAccessory\Controllers';
    protected $routesPath = 'Modules/PurchaseOrderAccessory/Routes/';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapBreadcrumbRoutes();
    }

    protected function mapBreadcrumbRoutes()
    {
        Route::namespace($this->namespace)->group(app_path($this->routesPath . 'purchase-order-accessory-breadcrumbs.php'));
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(app_path($this->routesPath . 'purchase-order-accessory-web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(app_path($this->routesPath . 'purchase-order-accessory-api.php'));
    }
}
