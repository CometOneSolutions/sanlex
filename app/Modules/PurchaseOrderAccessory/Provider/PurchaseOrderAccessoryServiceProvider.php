<?php

namespace App\Modules\PurchaseOrderAccessory\Provider;

use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;
use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessoryModel;
use App\Modules\PurchaseOrderAccessory\Repositories\EloquentPurchaseOrderAccessoryRepository;
use App\Modules\PurchaseOrderAccessory\Repositories\PurchaseOrderAccessoryRepository;
use App\Modules\PurchaseOrderAccessory\Services\PurchaseOrderAccessoryRecordService;
use App\Modules\PurchaseOrderAccessory\Services\PurchaseOrderAccessoryRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class PurchaseOrderAccessoryServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/PurchaseOrderAccessory/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(PurchaseOrderAccessoryRecordService::class, PurchaseOrderAccessoryRecordServiceImpl::class);
		$this->app->bind(PurchaseOrderAccessoryRepository::class, EloquentPurchaseOrderAccessoryRepository::class);
		$this->app->bind(PurchaseOrderAccessory::class, PurchaseOrderAccessoryModel::class);
	}
}
