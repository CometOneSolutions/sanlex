<?php

namespace App\Modules\PurchaseOrderAccessory\Models;

use DateTime;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Common\Models\C1Model;
use App\Modules\Supplier\Models\SupplierModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use App\Modules\ReceivingAccessory\Models\ReceivingAccessoryModel;
use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;
use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetailModel;
use App\Modules\PurchaseOrderAccessoryDetail\Models\PurchaseOrderAccessoryDetailModel;

class PurchaseOrderAccessoryModel extends C1Model implements PurchaseOrderAccessory, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'purchase_order_accessories';

    protected $dates = ['date'];

    protected $purchaseOrderAccessoryDetailsToSet = null;

    public function receivingAccessoryDetails()
    {
        return $this->hasManyThrough(ReceivingAccessoryDetailModel::class, ReceivingAccessoryModel::class, 'purchase_order_accessory_id', 'receiving_accessory_id');
    }

    public function receivingAccessories()
    {
        return $this->hasMany(ReceivingAccessoryModel::class, 'purchase_order_accessory_id');
    }

    public function supplier()
    {
        return $this->belongsTo(SupplierModel::class);
    }

    public function purchaseOrderAccessoryDetails()
    {
        return $this->hasMany(PurchaseOrderAccessoryDetailModel::class, 'purchase_order_accessory_id');
    }

    public function getReceivingAccessoryDetails()
    {
        return $this->receivingAccessoryDetails;
    }

    public function getReceivingAccessories()
    {
        return $this->receivingAccessories;
    }

    public function setPurchaseOrderAccessoryDetails(array $purchaseOrderAccessoryDetails)
    {
        $this->purchaseOrderAccessoryDetailsToSet = $purchaseOrderAccessoryDetails;
        return $this;
    }

    public function getPurchaseOrderAccessoryDetails()
    {
        if ($this->purchaseOrderAccessoryDetailsToSet !== null) {
            return collect($this->purchaseOrderAccessoryDetailsToSet);
        }
        return $this->purchaseOrderAccessoryDetails;
    }

    public function setSupplier(Supplier $supplier)
    {
        $this->supplier()->associate($supplier);
        $this->supplier_id = $supplier->getId();
        return $this;
    }

    public function getSupplier()
    {
        return $this->supplier;
    }

    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDate($value)
    {
        $this->date = $value;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setPONumber($value)
    {
        $this->po_no = $value !== '' ? $value : null;
        return $this;
    }

    public function getPONumber()
    {
        return $this->po_no;
    }

    public function setRemarks($value)
    {
        $this->remarks = $value !== '' ? $value : null;
        return $this;
    }

    public function getRemarks()
    {
        return $this->remarks;
    }

    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setProductType($value)
    {
        $this->product_type = $value;
        return $this;
    }

    public function getProductType()
    {
        return $this->product_type;
    }

    public function isOpen()
    {
        return $this->status == PurchaseOrderAccessory::STATUS_OPEN;
    }

    public function isClosed()
    {
        return $this->status == PurchaseOrderAccessory::STATUS_CLOSED;
    }

    public function getReceivingAccessoryDetailIntransitQuantity($productId)
    {
        return $this->receivingAccessoryDetails()->inTransit()->product($productId)->sum('quantity');
    }

    public function getReceivingAccessoryDetailQuantity($productId)
    {
        return $this->receivingAccessoryDetails()->received()->product($productId)->sum('quantity');
    }

    public function getPurchaseOrderAccessoryDetailQuantity($productId)
    {
        return $this->purchaseOrderAccessoryDetails()->product($productId)->sum('ordered_quantity');
    }

    public function getPurchaseOrderAccessoryDetailBalanceQuantity($productId)
    {
        return $this->getPurchaseOrderAccessoryDetailQuantity($productId) - $this->getReceivingAccessoryDetailQuantity($productId);
    }

    public function hasReceivingAccessories()
    {
        return $this->receivingAccessories()->exists();
    }

    public function getInTransitReceivingAccessories()
    {
        return $this->getReceivingAccessories()->filter(function ($receivingAccessory) {
            return $receivingAccessory->isInTransit();
        });
    }

    public function getReceivedReceivingAccessories()
    {
        return $this->getReceivingAccessories()->filter(function ($receivingAccessory) {
            return $receivingAccessory->isReceived();
        });
    }

    public function hasReceivedReceivingAccessories()
    {
        return $this->getReceivedReceivingAccessories()->isNotEmpty();
    }

    public function hasInTransitReceivingAccessories()
    {
        return $this->getInTransitReceivingAccessories()->isNotEmpty();
    }

    public function scopeOpen($query)
    {
        return $query->whereStatus(PurchaseOrderAccessory::STATUS_OPEN);
    }
}
