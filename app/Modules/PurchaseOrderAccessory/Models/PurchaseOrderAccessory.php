<?php

namespace App\Modules\PurchaseOrderAccessory\Models;


use DateTime;

interface PurchaseOrderAccessory
{
	const STATUS_OPEN = 'OPEN';
	const STATUS_CLOSED = 'CLOSED';

	public function getId();

	public function setDate(DateTime $value);

	public function getDate();

	public function setPONumber($value);

	public function getPONumber();

	public function setRemarks($value);

	public function getRemarks();

	public function setStatus($value);

	public function getStatus();

	public function setProductType($value);

	public function getProductType();
}
