<?php

Breadcrumbs::register('purchase-order-accessory.index', function ($breadcrumbs) {
	$breadcrumbs->parent('purchasing-menu-dashboard.index');
	$breadcrumbs->push('Purchase Order Accessories', route('purchase-order-accessory_index'));
});
Breadcrumbs::register('purchase-order-accessory.create', function ($breadcrumbs) {
	$breadcrumbs->parent('purchase-order-accessory.index');
	$breadcrumbs->push('Create', route('purchase-order-accessory_create'));
});
Breadcrumbs::register('purchase-order-accessory.show', function ($breadcrumbs, $purchaseOrderAccessory) {
	$breadcrumbs->parent('purchase-order-accessory.index');
	$breadcrumbs->push($purchaseOrderAccessory->getPoNumber(), route('purchase-order-accessory_show', $purchaseOrderAccessory->getId()));
});
Breadcrumbs::register('purchase-order-accessory.edit', function ($breadcrumbs, $purchaseOrderAccessory) {
	$breadcrumbs->parent('purchase-order-accessory.show', $purchaseOrderAccessory);
	$breadcrumbs->push('Edit', route('purchase-order-accessory_edit', $purchaseOrderAccessory->getId()));
});
