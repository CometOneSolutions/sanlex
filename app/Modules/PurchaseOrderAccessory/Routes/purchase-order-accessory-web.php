<?php

Route::group(['prefix' => 'purchasing'], function () {
	Route::group(['prefix' => 'purchase-order-accessories'], function () {
		Route::get('/', 'PurchaseOrderAccessoryController@index')->name('purchase-order-accessory_index');
		Route::get('/open', 'PurchaseOrderAccessoryController@indexOpen')->name('purchase-order-accessory_open');
		Route::get('/create', 'PurchaseOrderAccessoryController@create')->name('purchase-order-accessory_create');
		Route::post('/generate-template/{receivingAccessoryId}', 'PurchaseOrderAccessoryController@generateTemplate')->name('purchase-order-accessory_generate_template');
		Route::get('{purchaseOrderAccessoryId}/edit', 'PurchaseOrderAccessoryController@edit')->name('purchase-order-accessory_edit');
		Route::get('{purchaseOrderAccessoryId}/print', 'PurchaseOrderAccessoryController@print')->name('purchase-order-accessory_print');
		Route::get('{purchaseOrderAccessoryId}', 'PurchaseOrderAccessoryController@show')->name('purchase-order-accessory_show');
	});
});
