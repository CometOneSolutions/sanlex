<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'purchase-order-accessories'], function () use ($api) {
		$api->get('/', 'App\Modules\PurchaseOrderAccessory\Controllers\PurchaseOrderAccessoryApiController@index');
		$api->get('{purchaseOrderAccessoryId}', 'App\Modules\PurchaseOrderAccessory\Controllers\PurchaseOrderAccessoryApiController@show');
		$api->post('/', 'App\Modules\PurchaseOrderAccessory\Controllers\PurchaseOrderAccessoryApiController@store');
		$api->patch('{purchaseOrderAccessoryId}', 'App\Modules\PurchaseOrderAccessory\Controllers\PurchaseOrderAccessoryApiController@update');
		$api->patch('open/{purchaseOrderAccessoryId}', 'App\Modules\PurchaseOrderAccessory\Controllers\PurchaseOrderAccessoryApiController@open');
		$api->patch('close/{purchaseOrderAccessoryId}', 'App\Modules\PurchaseOrderAccessory\Controllers\PurchaseOrderAccessoryApiController@close');
		$api->delete('{purchaseOrderAccessoryId}', 'App\Modules\PurchaseOrderAccessory\Controllers\PurchaseOrderAccessoryApiController@destroy');
	});
});
