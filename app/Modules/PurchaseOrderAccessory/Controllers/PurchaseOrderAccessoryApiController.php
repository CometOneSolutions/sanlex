<?php

namespace App\Modules\PurchaseOrderAccessory\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\PurchaseOrderAccessory\Services\PurchaseOrderAccessoryRecordService;
use App\Modules\PurchaseOrderAccessory\Transformer\PurchaseOrderAccessoryTransformer;
use App\Modules\PurchaseOrderAccessory\Requests\StorePurchaseOrderAccessory;
use App\Modules\PurchaseOrderAccessory\Requests\UpdatePurchaseOrderAccessory;
use App\Modules\PurchaseOrderAccessory\Requests\DestroyPurchaseOrderAccessory;

class PurchaseOrderAccessoryApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;

	public function __construct(
		PurchaseOrderAccessoryRecordService $purchaseOrderAccessoryRecordService,
		PurchaseOrderAccessoryTransformer $transformer
	) {
		parent::__construct($purchaseOrderAccessoryRecordService, $transformer);
		$this->service = $purchaseOrderAccessoryRecordService;
		$this->transformer = $transformer;
	}

	public function store(StorePurchaseOrderAccessory $request)
	{
		$purchaseOrderAccessory = \DB::transaction(function () use ($request) {
			return $this->service->create(
				$request->getPONumber(),
				$request->getSupplier(),
				$request->getProductType(),
				$request->getDate(),
				$request->getRemarks(),
				$request->getPurchaseOrderAccessoryDetails(),
				$request->user()
			);
		});
		return $this->response->item($purchaseOrderAccessory, $this->transformer)->setStatusCode(201);
	}

	public function update($purchaseOrderAccessoryId, UpdatePurchaseOrderAccessory $request)
	{
		$purchaseOrderAccessory = \DB::transaction(function () use ($purchaseOrderAccessoryId, $request) {
			$purchaseOrderAccessory   = $this->service->getById($purchaseOrderAccessoryId);

			return $this->service->update(
				$purchaseOrderAccessory,
				$request->getPONumber(),
				$request->getSupplier(),
				$request->getProductType(),
				$request->getDate(),
				$request->getRemarks(),
				$request->getPurchaseOrderAccessoryDetails(),
				$request->user()
			);
		});
		return $this->response->item($purchaseOrderAccessory, $this->transformer)->setStatusCode(200);
	}

	public function destroy($purchaseOrderAccessoryId, DestroyPurchaseOrderAccessory $request)
	{
		$purchaseOrderAccessory = $this->service->getById($purchaseOrderAccessoryId);
		$this->service->delete($purchaseOrderAccessory);
		return $this->response->item($purchaseOrderAccessory, $this->transformer)->setStatusCode(200);
	}

	public function open($purchaseOrderAccessoryId, UpdatePurchaseOrderAccessory $request)
	{
		$purchaseOrderAccessory = \DB::transaction(function () use ($request, $purchaseOrderAccessoryId) {
			$purchaseOrderAccessory = $this->service->open(
				$this->service->getByid($purchaseOrderAccessoryId),
				$request->user()
			);

			return $purchaseOrderAccessory;
		});

		return $this->response->item($purchaseOrderAccessory, $this->transformer)->setStatusCode(200);
	}

	public function close($purchaseOrderAccessoryId, UpdatePurchaseOrderAccessory $request)
	{
		$purchaseOrderAccessory = \DB::transaction(function () use ($request, $purchaseOrderAccessoryId) {
			$purchaseOrderAccessory = $this->service->close(
				$this->service->getByid($purchaseOrderAccessoryId),
				$request->user()
			);

			return $purchaseOrderAccessory;
		});

		return $this->response->item($purchaseOrderAccessory, $this->transformer)->setStatusCode(200);
	}
}
