<?php

namespace App\Modules\PurchaseOrderAccessory\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\PurchaseOrderAccessory\Export\PurchaseOrderAccessoryExportModel;
use App\Modules\PurchaseOrderAccessory\Services\PurchaseOrderAccessoryRecordService;
use App\Modules\PurchaseOrderAccessory\Export\PurchaseOrderAccessoryTemplateExport;
use App\Modules\PurchaseOrderAccessory\Services\PurchaseOrderAccessoryReportGenerator;
use App\Modules\ReceivingAccessory\Services\ReceivingAccessoryRecordService;



class PurchaseOrderAccessoryController extends Controller
{
	private $receivingAccessoryRecordService;

	public function __construct(
		PurchaseOrderAccessoryRecordService $purchaseOrderAccessoryRecordService,
		ReceivingAccessoryRecordService $receivingAccessoryRecordService
	) {
		$this->service = $purchaseOrderAccessoryRecordService;
		$this->receivingAccessoryRecordService = $receivingAccessoryRecordService;
		$this->middleware('permission:Read [PUR] Purchase Order Accessory')->only(['show', 'index']);
		$this->middleware('permission:Create [PUR] Purchase Order Accessory')->only('create');
		$this->middleware('permission:Update [PUR] Purchase Order Accessory')->only('edit');
	}


	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => '-status,-created_at',
			'sortAscending' => true,
			'baseUrl' => '/api/purchase-order-accessories?include=supplier'
		]);
		return view('purchase-order-accessories.index');
	}

	public function indexOpen()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'date',
			'sortAscending' => false,
			'baseUrl' => '/api/purchase-order-accessories?include=supplier&status=Open'
		]);
		return view('purchase-order-accessories.index');
	}


	public function create()
	{
		JavaScript::put(['id' => null, 'productType' => null, 'supplierId' => null]);
		return view('purchase-order-accessories.create');
	}

	public function show($id)
	{
		$purchaseOrderAccessory = $this->service->getById($id);

		JavaScript::put([
			'id' => $id,
			'productType' => null, 'supplierId' => null
		]);

		return view('purchase-order-accessories.show', compact('purchaseOrderAccessory'));
	}

	public function edit($id)
	{
		$purchaseOrderAccessory = $this->service->getById($id);

		JavaScript::put([
			'id' => $id,
			'productType' => $purchaseOrderAccessory->getProductType(), 'supplierId' => $purchaseOrderAccessory->getSupplierId()
		]);

		return view('purchase-order-accessories.edit', compact('purchaseOrderAccessory'));
	}


	public function generateTemplate($purchaseOrderAccessoryId)
	{
		// $receivingAccessory = $this->receivingAccessoryRecordService->getById($receivingAccessoryId);
		$purchaseOrderAccessory = $this->service->getById($purchaseOrderAccessoryId);
		$reportGenerator = new PurchaseOrderAccessoryReportGenerator($purchaseOrderAccessory);
		$data = new PurchaseOrderAccessoryExportModel($purchaseOrderAccessory);

		return Excel::download(new PurchaseOrderAccessoryTemplateExport($data), $reportGenerator->getTemplateFileName());
	}
}
