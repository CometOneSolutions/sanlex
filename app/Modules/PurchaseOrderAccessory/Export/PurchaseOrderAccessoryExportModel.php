<?php

namespace App\Modules\PurchaseOrderAccessory\Export;

use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;

use Illuminate\Support\Collection;

class PurchaseOrderAccessoryExportModel
{
    protected $purchaseOrderAccessory;
    protected $purchaseOrderAccessoryDetails;
    protected $accessories;
    protected $headers;
    protected $productType;

    public function __construct(PurchaseOrderAccessory $purchaseOrderAccessory)
    {
        $this->purchaseOrderAccessory = $purchaseOrderAccessory;

        $this->productType = $this->purchaseOrderAccessory->getProductType();
        $this->purchaseOrderAccessoryDetails = $this->purchaseOrderAccessory->getPurchaseOrderAccessoryDetails();
        $this->accessories = $this->initAccessories();
        $this->headers = $this->initHeaders();
    }
    protected function initAccessories()
    {
        $accessories =  $this->purchaseOrderAccessoryDetails->map(function ($purchaseOrderAccessoryDetail) {
            return new AccessoryExportModel($purchaseOrderAccessoryDetail, $this->productType);
        });

        return $this->removeAccessoriesWithNoBalance($accessories);
    }

    protected function initHeaders()
    {
        return $this->accessories->isNotEmpty() ? $this->accessories->first()->getData()->keys() : [];
    }

    public function getAccessories()
    {
        return $this->accessories;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    private function removeAccessoriesWithNoBalance(Collection $accessories)
    {
        return $accessories->filter(function ($accessory) {
            return $accessory->hasBalanceQuantity();
        })->values();
    }
}
