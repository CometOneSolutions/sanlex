<?php

namespace App\Modules\PurchaseOrderAccessory\Export;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\PurchaseOrderAccessoryDetail\Models\PurchaseOrderAccessoryDetail;

class AccessoryExportModel
{
    protected $purchaseOrderAccessoryDetail;
    protected $productType;
    protected $productable;
    protected $data;
    protected $purchaseOrderAccessoryQuantity;

    public function __construct(
        PurchaseOrderAccessoryDetail $purchaseOrderAccessoryDetail,
        $productType
    ) {
        $this->purchaseOrderAccessoryDetail = $purchaseOrderAccessoryDetail;
        $this->productType = $productType;
        $this->productable = $purchaseOrderAccessoryDetail->getProduct()->productable;
        $this->data = $this->initData();
    }

    public function initData()
    {
        switch ($this->productType) {
            case PurchaseOrder::SS:
                return $this->getStainlessSteelData();
                break;
            case PurchaseOrder::INSULATION:
                return $this->getInsulationData();
                break;
            case PurchaseOrder::SCREW:
                return $this->getScrewData();
                break;
            case PurchaseOrder::SKYLIGHT:
                return $this->getSkylightData();
                break;
            case PurchaseOrder::MISC:
                return $this->getMiscAccessoryData();
                break;
            case PurchaseOrder::ST:
                return $this->getStructuralSteelData();
                break;
            case PurchaseOrder::PAINT:
                return $this->getPaintAdhesiveData();
                break;
            case PurchaseOrder::FASTENER:
                return $this->getFastenerData();
                break;
            case PurchaseOrder::PVC:
                return $this->getPvcFittingData();
                break;
        }
    }

    public function getFastenerData()
    {
        $data = [];
        $balanceQuantity = $this->purchaseOrderAccessoryDetail->getPurchaseOrderAccessory()->getPurchaseOrderAccessoryDetailBalanceQuantity($this->purchaseOrderAccessoryDetail->getProductId());

        $data['SUPPLIER CODE']              = $this->productable->getProduct()->getSupplier()->getShortCode();
        $data['FASTENER TYPE']              = $this->productable->getFastenerType()->getName();
        $data['FINISH']                     = $this->productable->getFinish()->getName();
        $data['FASTENER DIMENSION']         = $this->productable->getFastenerDimension()->getName();
        $data['SIZE PER UNIT']              = $this->productable->getSize()->getName();
        $data['QUANTITY (IN PCS)']          = $balanceQuantity;

        return $data;
    }

    public function getInsulationData()
    {
        $data = [];
        $balanceQuantity = $this->purchaseOrderAccessoryDetail->getPurchaseOrderAccessory()->getPurchaseOrderAccessoryDetailBalanceQuantity($this->purchaseOrderAccessoryDetail->getProductId());

        $data['SUPPLIER CODE']              = $this->productable->getProduct()->getSupplier()->getShortCode();
        $data['TYPE']                       = $this->productable->getInsulationType()->getName();
        $data['DENSITY']                    = $this->productable->getInsulationDensity()->getName();
        $data['THICKNESS']                  = $this->productable->getThickness()->getName();
        $data['LENGTH']                     = $this->productable->getLength()->getName();
        $data['WIDTH']                      = $this->productable->getWidth()->getName();
        $data['BACKING']                    = $this->productable->getBackingSide()->getName();
        $data['QUANTITY (IN PCS)']          = $balanceQuantity;

        return $data;
    }

    public function getMiscAccessoryData()
    {
        $data = [];
        $balanceQuantity = $this->purchaseOrderAccessoryDetail->getPurchaseOrderAccessory()->getPurchaseOrderAccessoryDetailBalanceQuantity($this->purchaseOrderAccessoryDetail->getProductId());

        $data['SUPPLIER CODE']              = $this->productable->getProduct()->getSupplier()->getShortCode();
        $data['WIDTH']                      = $this->productable->getWidth()->getName();
        $data['LENGTH']                     = $this->productable->getLength()->getName();
        $data['BACKING SIDE']               = $this->productable->getBackingSide()->getName();
        $data['TYPE']                       = $this->productable->getMiscAccessoryType()->getName();
        $data['QUANTITY (IN PCS)']          = $balanceQuantity;

        return $data;
    }

    public function getPaintAdhesiveData()
    {
        $data = [];
        $balanceQuantity = $this->purchaseOrderAccessoryDetail->getPurchaseOrderAccessory()->getPurchaseOrderAccessoryDetailBalanceQuantity($this->purchaseOrderAccessoryDetail->getProductId());

        $data['SUPPLIER CODE']              = $this->productable->getProduct()->getSupplier()->getShortCode();
        $data['PAINT ADHESIVE TYPE']        = $this->productable->getPaintAdhesiveType()->getName();
        $data['BRAND']                      = $this->productable->getBrand()->getName();
        $data['COLOR']                      = $this->productable->getColor()->getName();
        $data['SIZE PER UNIT']              = $this->productable->getSize()->getName();
        $data['QUANTITY (IN PCS)']          = $balanceQuantity;

        return $data;
    }

    public function getPvcFittingData()
    {
        $data = [];
        $balanceQuantity = $this->purchaseOrderAccessoryDetail->getPurchaseOrderAccessory()->getPurchaseOrderAccessoryDetailBalanceQuantity($this->purchaseOrderAccessoryDetail->getProductId());

        $data['SUPPLIER CODE']              = $this->productable->getProduct()->getSupplier()->getShortCode();
        $data['TYPE']                       = $this->productable->getPvcType()->getName();
        $data['DIAMETER']                   = $this->productable->getPvcDiameter()->getName();
        $data['BRAND']                      = $this->productable->getBrand()->getName();
        $data['QUANTITY (IN PCS)']          = $balanceQuantity;

        return $data;
    }

    public function getSkylightData()
    {
        $data = [];
        $balanceQuantity = $this->purchaseOrderAccessoryDetail->getPurchaseOrderAccessory()->getPurchaseOrderAccessoryDetailBalanceQuantity($this->purchaseOrderAccessoryDetail->getProductId());

        $data['SUPPLIER CODE']              = $this->productable->getProduct()->getSupplier()->getShortCode();
        $data['PANEL PROFILE']              = $this->productable->getPanelProfile()->getName();
        $data['THICKNESS']                  = $this->productable->getThickness()->getName();
        $data['WIDTH']                      = $this->productable->getWidth()->getName();
        $data['LENGTH']                     = $this->productable->getLength()->getName();
        $data['CLASS']                      = $this->productable->getSkylightClass()->getName();
        $data['QUANTITY (IN PCS)']          = $balanceQuantity;

        return $data;
    }

    public function getStainlessSteelData()
    {
        $data = [];
        $balanceQuantity = $this->purchaseOrderAccessoryDetail->getPurchaseOrderAccessory()->getPurchaseOrderAccessoryDetailBalanceQuantity($this->purchaseOrderAccessoryDetail->getProductId());

        $data['SUPPLIER CODE']              = $this->productable->getProduct()->getSupplier()->getShortCode();
        $data['TYPE']                       = $this->productable->getStainlessSteelType()->getName();
        $data['THICKNESS']                  = $this->productable->getThickness()->getName();
        $data['WIDTH']                      = $this->productable->getWidth()->getName();
        $data['LENGTH']                     = $this->productable->getLength()->getName();
        $data['FINISH']                     = $this->productable->getFinish()->getName();
        $data['QUANTITY (IN PCS)']          = $balanceQuantity;

        return $data;
    }

    public function getStructuralSteelData()
    {
        $data = [];
        $balanceQuantity = $this->purchaseOrderAccessoryDetail->getPurchaseOrderAccessory()->getPurchaseOrderAccessoryDetailBalanceQuantity($this->purchaseOrderAccessoryDetail->getProductId());

        $data['SUPPLIER CODE']              = $this->productable->getProduct()->getSupplier()->getShortCode();
        $data['TYPE']                       = $this->productable->getStructuralSteelType()->getName();
        $data['FINISH']                     = $this->productable->getFinish()->getName();
        $data['THICKNESS']                  = $this->productable->getThickness()->getName();
        $data['DIMENSION X']                = $this->productable->getDimensionX()->getName();
        $data['DIMENSION Y']                = $this->productable->getDimensionY()->getName();
        $data['LENGTH']                     = $this->productable->getLength()->getName();
        $data['QUANTITY (IN PCS)']          = $balanceQuantity;

        return $data;
    }

    public function hasBalanceQuantity()
    {
        return $this->data['QUANTITY (IN PCS)'] > 0;
    }

    public function getByHeaderName($headerName)
    {
        return $this->data[$headerName];
    }

    public function getData()
    {
        return collect($this->data);
    }
}
