<?php

namespace App\Modules\PurchaseOrderAccessory\Export;

use CometOneSolutions\Common\Export;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use \PhpOffice\PhpSpreadsheet\Cell\DataType;


class PurchaseOrderAccessoryTemplateExport implements WithEvents, WithTitle
{
    protected $data;
    protected $columns;

    public function __construct($data)
    {
        $this->data = $data;
        $this->columns = range('A', 'Z');
    }

    public function title(): String
    {
        return 'Template';
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $this->setHeaders($event);
                $this->setRows($event);
            }
        ];
    }

    private function setHeaders($event)
    {
        if ($this->data->getHeaders()) {


            $this->data->getHeaders()->each(function ($value, $index) use ($event) {
                $rowPointer = '1';
                $columnPointer = $this->columns[$index];
                $cellCoordinate = sprintf('%s%s', $columnPointer, $rowPointer);

                $cell = $event->sheet->getCell($cellCoordinate);
                $cell->setValue($value);

                $event->sheet->getColumnDimension($columnPointer)->setAutoSize(true);
            });
        }
    }

    private function setRows($event)
    {
        $highestRow = $event->sheet->getHighestRow();
        $this->data->getAccessories()->each(function ($accessory, $accessoryIndex) use ($event, $highestRow) {
            $this->data->getHeaders()->each(function ($header, $headerIndex) use ($event, $accessory, $accessoryIndex, $highestRow) {

                $text = $accessory->getByHeaderName($header);
                $rowPointer = ($accessoryIndex + 1) + $highestRow;
                $columnPointer = $this->columns[$headerIndex];
                $cellCoordinate = sprintf('%s%s', $columnPointer, $rowPointer);

                $event->sheet->setCellValueExplicit($cellCoordinate, $text, DataType::TYPE_STRING);
            });
        });
    }
}
