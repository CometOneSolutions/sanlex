<?php

namespace App\Modules\Customer\Controllers;

use App\Modules\Customer\Services\CustomerRecordService;
use Illuminate\Http\Request;
use App\Modules\Customer\Transformer\CustomerTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Customer\Requests\StoreCustomer;
use App\Modules\Customer\Requests\UpdateCustomer;
use App\Modules\Customer\Requests\DestroyCustomer;


class CustomerApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;
    protected $contactRecordService;

    public function __construct(
        CustomerRecordService $customerRecordService,
        CustomerTransformer $transformer


    ) {
        $this->service = $customerRecordService;

        $this->transformer = $transformer;
        parent::__construct($customerRecordService, $transformer);
        $this->middleware(['auth:api']);
    }

    public function destroy($customerId, DestroyCustomer $request)
    {
        $customer = \DB::transaction(function () use ($customerId) {
            $customer = $this->service->getById($customerId);
            $this->service->delete($customer);
            return $customer;
        });
        return $this->response->item($customer, $this->transformer)->setStatusCode(200);
    }

    public function store(StoreCustomer $request)
    {
        $customer = \DB::transaction(function () use ($request) {
            $name = $request->getName();
            return $this->service->create(
                $name,
                $request->getContactPerson(),
                $request->getTelephone(),
                $request->getMobile(),
                $request->getEmail(),
                $request->getAddress(),
                $request->user()
            );

        });
        return $this->response->item($customer, $this->transformer)->setStatusCode(201);
    }

    public function update($customerId, UpdateCustomer $request)
    {
        $customer = \DB::transaction(function () use ($request) {
            $customer = $request->getCustomer();
            $name = $request->getName();
            return $this->service->update(
                $customer,
                $name,
                $request->getContactPerson(),
                $request->getTelephone(),
                $request->getMobile(),
                $request->getEmail(),
                $request->getAddress(),
                $request->user()
            );
        });
        return $this->response->item($customer, $this->transformer)->setStatusCode(200);
    }


}
