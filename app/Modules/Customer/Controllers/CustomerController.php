<?php

namespace App\Modules\Customer\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use Illuminate\Http\Request;
use JavaScript;
use App\Modules\Customer\Services\CustomerRecordService;

class CustomerController extends Controller
{
    private $service;

    public function __construct(CustomerRecordService $customerRecordService)
    {
        $this->service = $customerRecordService;
        $this->middleware('permission:Read [MAS] Customer')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Customer')->only('create');
        $this->middleware('permission:Update [MAS] Customer')->only('edit');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/customers'
        ]);
        return view("customers.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        JavaScript::put(['id' => null]);
        return view("customers.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $customer = $this->service->getById($id);
        return view("customers.show", compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $customer = $this->service->getById($id);
        return view("customers.edit", compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getDisplayedFilterable()
    {
        return [
            ["id" => "name", "text" => "Name"]
        ];
    }
}
