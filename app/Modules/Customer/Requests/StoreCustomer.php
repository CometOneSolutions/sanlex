<?php

namespace App\Modules\Customer\Requests;

use Dingo\Api\Http\FormRequest;
use App\Modules\Customer\Services\CustomerRecordService;

class StoreCustomer extends FormRequest
{
    private $service;

    public function __construct(CustomerRecordService $customerRecordService)
    {
        $this->service = $customerRecordService;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Customer');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:customers,name',
            'email' => 'nullable|email'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required.',
            'name.unique'   => 'Name already exists.',
            'email.email' => 'Enter valid email address'
        ];
    }

    public function getCustomer($index = 'id')
    {
        return $this->service->getById($this->input($index));
    }
    public function getName($index = 'name')
    {
        return $this->input($index);
    }
    public function getCreditLimit($index = 'creditLimit')
    {
        return $this->input($index);
    }
    public function getTin($index = 'tin')
    {
        return $this->input($index);
    }

    public function getContactPerson()
    {
        return $this->input('contactPerson');
    }

    public function getTelephone()
    {
        return $this->input('telephone');
    }

    public function getMobile()
    {
        return $this->input('mobile');
    }

    public function getEmail()
    {
        return $this->input('email');
    }

    public function getAddress()
    {
        return $this->input('address');
    }

    public function getUser()
    {
        return $this->user();
    }
}
