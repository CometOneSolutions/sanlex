<?php

namespace App\Modules\Customer\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyCustomer extends FormRequest
{

    public function __construct() {

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('Delete Customer');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }

    


}
