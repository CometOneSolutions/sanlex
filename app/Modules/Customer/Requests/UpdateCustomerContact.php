<?php

namespace App\Modules\Customer\Requests;

use Dingo\Api\Http\FormRequest;
use App\Modules\Contacts\Services\ContactRecordService;
use App\Modules\Customer\Services\CustomerRecordService;

class UpdateCustomerContact extends FormRequest
{
    protected $service;
    protected $contactRecordService;

    public function __construct(
        ContactRecordService $contactRecordService,
        CustomerRecordService $customerRecordService
    ) {
        $this->contactRecordService = $contactRecordService;
        $this->service = $customerRecordService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Customer');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required.'
        ];
    }

    public function getCustomer()
    {
        return $this->service->getById($this->input('id'));
    }

    private function getContact()
    {
        return $this->contactRecordService->getById($this->input('contact.id'));
    }

    public function getUpdatableContact()
    {
        $contact = $this->getContact();
        return $this->contactRecordService->update($contact, $this->getContactPerson(), $this->getPosition(), $this->getTelephone(), $this->getFax(), $this->getMobile(), $this->getEmail());
    }

    // public function getUpdatableContact($index = 'contact.id')
    // {
    //     //return $this->contactRecordService->update($this->getContactPerson(), $this->getPosition(), $this->getTelephone(), $this->getFax(), $this->getMobile(), $this->getEmail());
    //     return $this->contactRecordService->getById($index);
    // }

    private function getContactPerson($index = 'contact.contactPerson')
    {
        return $this->input($index);
    }

    private function getPosition($index = 'contact.position')
    {
        return $this->input($index);
    }

    private function getTelephone($index = 'contact.telephone')
    {
        return $this->input($index);
    }

    private function getFax($index = 'contact.fax')
    {
        return $this->input($index);
    }

    private function getMobile($index = 'contact.mobile')
    {
        return $this->input($index);
    }

    private function getEmail($index = 'contact.email')
    {
        return $this->input($index);
    }


}
