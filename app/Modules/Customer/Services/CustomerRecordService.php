<?php

namespace App\Modules\Customer\Services;

use App\Models\Address;
use App\Modules\Customer\Models\Customer;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordService;

interface CustomerRecordService extends RecordService
{
    public function create(
        string $name,
        $contactPerson,
        $telephone,
        $mobile,
        $email,
        $address,
        User $user = null
    );

    public function update(
        Customer $customer,
        string $name,
        $contactPerson,
        $telephone,
        $mobile,
        $email,
        $address,
        User $user = null
    );

    public function delete(Customer $customer);

    public function makeAddress($street, $city, $zip);
}
