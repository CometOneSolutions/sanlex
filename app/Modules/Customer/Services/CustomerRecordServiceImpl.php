<?php

namespace App\Modules\Customer\Services;

use App\Models\Address;
use App\Modules\Customer\Models\Customer;
use App\Modules\Customer\Repositories\CustomerRepository;
use App\Services\Traits\CanMakeAddress;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class CustomerRecordServiceImpl extends RecordServiceImpl implements CustomerRecordService
{
    use CanMakeAddress;


    private $customers;
    private $customer;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"],
        ['id' => 'telephone', 'text' => 'Telephone'],
        ['id' => 'mobile', 'text' => 'Mobile'],
        ['id' => 'contact_person', 'text' => 'Contact Person']
    ];

    public function __construct(CustomerRepository $customers, Customer $customer)
    {
        $this->customers = $customers;
        $this->customer = $customer;
        parent::__construct($customers);
    }



    public function create(
        string $name,
        $contactPerson,
        $telephone,
        $mobile,
        $email,
        $address,
        User $user = null
    ) {
        $customer = new $this->customer;
        $customer->balance = 0;
        $this->setFields(
            $customer,
            $name,
            $contactPerson,
            $telephone,
            $mobile,
            $email,
            $address,
            $user
        );
        $this->customers->save($customer);
        return $customer;
    }

    private function setFields(
        Customer $customer,
        string $name,
        $contactPerson,
        $telephone,
        $mobile,
        $email,
        $address,
        User $user = null
    ) {
        $customer->setName($name);
        $customer->setContactPerson($contactPerson);
        $customer->setTelephone($telephone);
        $customer->setMobile($mobile);
        $customer->setEmail($email);
        $customer->setAddress($address);
        if ($user) {
            $customer->setUpdatedByUser($user);
        }
    }

    public function update(
        Customer $customer,
        string $name,
        $contactPerson,
        $telephone,
        $mobile,
        $email,
        $address,
        User $user = null
    ) {
        $this->setFields(
            $customer,
            $name,
            $contactPerson,
            $telephone,
            $mobile,
            $email,
            $address,
            $user
        );
        $this->customers->save($customer);
        return $customer;
    }

    public function delete(Customer $customer)
    {
        // TODO delete checks
        $this->customers->delete($customer);
        return $customer;
    }
}
