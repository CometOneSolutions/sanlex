<?php

namespace App\Modules\Customer\Providers;

use App\Modules\Customer\Models\Customer;
use App\Modules\Customer\Models\CustomerModel;
use App\Modules\Customer\Repositories\CustomerRepository;
use App\Modules\Customer\Repositories\EloquentCustomerRepository;
use App\Modules\Customer\Services\CustomerRecordService;
use App\Modules\Customer\Services\CustomerRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class CustomerServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Customer/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(CustomerRecordService::class, CustomerRecordServiceImpl::class);
        $this->app->bind(CustomerRepository::class, EloquentCustomerRepository::class);
        $this->app->bind(Customer::class, CustomerModel::class);
    }
}
