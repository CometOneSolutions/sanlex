<?php

namespace App\Modules\Customer\Transformer;

use App\Modules\Contract\Transformer\ContractTransformer;
use App\Modules\Customer\Models\Customer;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;


class CustomerTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        // 'salesOrders',
        'inboundPayments',
        'contracts',
        'contractsWithBalance',
    ];
    public function __construct()
    {
        $this->defaultIncludes = array_merge($this->defaultIncludes, []);
    }
    public function transform(Customer $customer)
    {
        return [
            'id' => (int) $customer->getId(),
            'text' => $customer->getName(),
            'name' => $customer->getName(),
            'contactPerson' => $customer->getContactPerson(),
            'telephone' => $customer->getTelephone(),
            'mobile' => $customer->getMobile(),
            'email' => $customer->getEmail(),
            'address' => $customer->getAddress(),
            'hasContracts' => $customer->hasContracts(),
            'balance' => $customer->balance,
            'editUri' => route('customer_edit', $customer->getId()),
            'showUri' => route('customer_show', $customer->getId()),
            'updatedAt' => $customer->updated_at,

        ];
    }

    public function includeInboundPayments(Customer $customer)
    {
        $inboundPayments = $customer->getInboundPayments();
        return $this->collection($inboundPayments, new InboundPaymentTransformer);
    }

    public function includeContracts(Customer $customer)
    {
        return $this->collection($customer->contracts, new ContractTransformer);
    }

    public function includeContractsWithbalance(Customer $customer)
    {
        $contractsWithBalance = $customer->contracts()->where('balance', ">", 0);
        return $this->collection($contractsWithBalance, new ContractTransformer);
    }
}
