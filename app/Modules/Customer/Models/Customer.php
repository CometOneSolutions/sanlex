<?php

namespace App\Modules\Customer\Models;


use App\Models\Address;

interface Customer
{
    public function getId();

    public function setName($value);

    public function getName();

    public function setTelephone($telephone);

    public function getTelephone();

    public function setFax($fax);

    public function getFax();

    public function setEmail($email);

    public function getEmail();

    public function setContactPerson($contactPerson);

    public function getContactPerson();

    public function setTIN($tin);

    public function getTIN();

    public function setCreditLimit($creditLimit);

    public function getCreditLimit();

    public function getMobile();

    public function getAddress();

    public function setMobile($value);

    public function setAddress($value);

}
