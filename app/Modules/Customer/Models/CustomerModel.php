<?php

namespace App\Modules\Customer\Models;

use App\Modules\Contract\Models\ContractModel;
use App\Modules\InboundPayment\Models\InboundPaymentModel;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Models\UpdatableByUser;

class CustomerModel extends Model implements Customer, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'customers';

    public function contacts()
    {
        return $this->morphMany('App\ContactModel', 'contactable');
    }

    public function inboundPayments()
    {
        return $this->hasManyThrough(InboundPaymentModel::class, ContractModel::class, 'customer_id', 'contract_id');
    }

    public function contracts()
    {
        return $this->hasMany(ContractModel::class, 'customer_id');
    }

    public function hasContracts()
    {
        return $this->contracts()->exists();
    }

    public function getInboundPayments()
    {
        return $this->inboundPayments;
    }
    public function getContacts()
    {
        return $this->contacts;
    }
    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setFax($fax)
    {
        $this->fax = $fax;
        return $this;
    }

    public function getFax()
    {
        return $this->fax;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setContactPerson($contactPerson)
    {
        $this->contact_person = $contactPerson;
        return $this;
    }

    public function getContactPerson()
    {
        return $this->contact_person;
    }

    public function setTIN($tin)
    {
        $this->tin = $tin;
        return $this;
    }

    public function getTIN()
    {
        return $this->tin;
    }

    public function setCreditLimit($creditLimit)
    {
        $this->credit_limit = $creditLimit;
        return $this;
    }

    public function getCreditLimit()
    {
        return $this->credit_limit;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function setMobile($value)
    {
        $this->mobile = $value;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($value)
    {
        $this->address = $value;
        return $this;
    }

    public function updateBalance()
    {
        $this->balance = $this->contracts()->sum('balance');
        $this->save();
    }

}
