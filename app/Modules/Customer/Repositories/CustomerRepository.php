<?php

namespace App\Modules\Customer\Repositories;

use App\Modules\Customer\Models\Customer;
use CometOneSolutions\Common\Repositories\Repository;

interface CustomerRepository extends Repository
{
    public function save(Customer $customer);
    public function delete(Customer $customer);
}
