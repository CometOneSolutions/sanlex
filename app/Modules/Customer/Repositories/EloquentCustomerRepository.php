<?php

namespace App\Modules\Customer\Repositories;

use App\Modules\Customer\Models\Customer;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentCustomerRepository extends EloquentRepository implements CustomerRepository
{
    public function __construct(Customer $customer)
    {
        parent::__construct($customer);
    }

    public function save(Customer $customer)
    {
        return $customer->save();
    }

    public function delete(Customer $customer)
    {
        return $customer->delete();
    }

    protected function filterByMainAddress($address)
    {
        return $this->model->where('main_street_address', 'LIKE', $address)
            ->orWhere('main_city_address', 'LIKE', $address)
            ->orWhere('main_zip_address', 'LIKE', $address);
    }
}
