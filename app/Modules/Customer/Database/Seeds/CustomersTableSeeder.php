<?php

namespace App\Modules\Customer\Database\Seeds;

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $customers = factory(CustomerModel::class, 4)->create();
    }
}
