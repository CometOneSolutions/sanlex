<?php

use App\Modules\Customer\Models\CustomerModel;
use Faker\Generator as Faker;
use CometOneSolutions\Auth\UserModel;

$factory->define(CustomerModel::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->company,
        'tin' => $faker->bankAccountNumber,
        'credit_limit' => $faker->numberBetween(0, 99999),
        'main_street_address' => $faker->streetAddress,
        'main_zip_address' => $faker->postcode,
        'main_city_address' => $faker->city,
        'delivery_street_address' => $faker->streetAddress,
        'delivery_zip_address' => $faker->postcode,
        'delivery_city_address' => $faker->city,
        'updated_by_user_id' => function () {
            return factory(UserModel::class)->create()->getId();
        }
    ];
});
