<?php

Breadcrumbs::register('customer.master-list', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Customers', route('customer_index'));
});
Breadcrumbs::register('customer.master-list.create', function ($breadcrumbs) {
    $breadcrumbs->parent('customer.master-list');
    $breadcrumbs->push('Create', route('customer_create'));
});
Breadcrumbs::register('customer.master-list.show', function ($breadcrumbs, $customer) {
    $breadcrumbs->parent('customer.master-list');
    $breadcrumbs->push($customer->getName(), route('customer_show', $customer->getId()));
});
Breadcrumbs::register('customer.master-list.edit', function ($breadcrumbs, $customer) {
    $breadcrumbs->parent('customer.master-list.show', $customer);
    $breadcrumbs->push('Edit', route('customer_edit', $customer->getId()));
});
