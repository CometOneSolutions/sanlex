<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'customers'], function () use ($api) {
        $api->get('/', 'App\Modules\Customer\Controllers\CustomerApiController@index');
        // $api->get('/list', 'App\Http\Controllers\CustomerListApiController@index');
        $api->get('{customerId}', 'App\Modules\Customer\Controllers\CustomerApiController@show');
        $api->post('/', 'App\Modules\Customer\Controllers\CustomerApiController@store');
        $api->post('{customerId}/add-contact', 'App\Modules\Customer\Controllers\CustomerApiController@addContact');
        $api->patch('{customerId}/edit-contact/{contactId}', 'App\Modules\Customer\Controllers\CustomerApiController@editContact');
        $api->delete('{customerId}/delete-contact/{contactId}', 'App\Modules\Customer\Controllers\CustomerApiController@deleteContact');
        $api->patch('{customerId}', 'App\Modules\Customer\Controllers\CustomerApiController@update');
        $api->delete('{customerId}', 'App\Modules\Customer\Controllers\CustomerApiController@destroy');
    });
});
