<?php

Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'customers'], function () {
        Route::get('/', 'CustomerController@index')->name('customer_index');
        Route::get('create', 'CustomerController@create')->name('customer_create');
        Route::get('{customerId}', 'CustomerController@show')->name('customer_show');
        Route::get('{customerId}/edit', 'CustomerController@edit')->name('customer_edit');
    });
});
