import {Form} from '@c1_common_js/components/Form';
import { customerService } from '@c1_module_js/Customer/Resources/Assets/js/customer-main';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm';

new Vue({
    el: '#customer',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, 
        customerService
    },
    data: {
        form: new Form({
            id: null,
            name: null,
            telephone: null,
            contactPerson: null,
            mobile: null,
            email: null,
            address: null,
        }),
        isSaving: false,
        isBusy: false,
        isDeleting: false,
        isBusyDeleting: false,
        dataInitialized: true,
        
        
    },
    watch: {
        initializationComplete(val) {    
            this.form.isInitializing = false;
        }
        
    },

    computed: {

        initializationComplete() {
            if (this.dataInitialized)
            {
                this.form.isInitializing = false;
                return true;
            }
            return false;
        }
     
    },
    
    methods: {
        addContact() {
            this.isSaving = true;
            this.isBusy = true;
            this.form.post('/api/customers/' + this.form.id + '/add-contact')
            .then(response => {
                this.isSaving = false;
                this.isBusy = false;
                this.$swal({
                    title: 'Success',
                    text: this.form.contact.contactPerson + ' was saved.',
                    type: 'success'
                }).then(() => window.location = '/master-file/customers/' + this.form.id);
                
            })
            .catch(error => {
                this.isSaving = false;
                this.isBusy = false;
            });
        },

        destroy() {  
           this.form.confirm().then((result) => {
                if(result.value) {
                    this.isDeleting = true;
                    this.isBusyDeleting = true;
                    this.form.delete('/api/customers/' + this.form.id)
                    .then(response => {
                        this.isDeleting = false;
                        this.isDeleting = false;
                        this.$swal({
                            title: 'Success',
                            text: 'Customer was removed.',
                            type: 'success'
                        }).then(() => window.location = '/master-file/customers/');
                    }).catch(error => {
                        this.isDeleting = false;
                        this.isBusyDeleting = false;
                    });
                   
                }
                
            });             
        },
        
        store() {
            this.isSaving = true;
            this.isBusy = true;
            this.form.post('/api/customers')
            .then(response => {
                this.isSaving = false;
                this.isBusy = false;
                this.$swal({
                    title: 'Success',
                    text: this.form.name.toUpperCase() + ' was saved.',
                    type: 'success'
                });
                
                this.form.reset();
            })
            .catch(error => {
                this.isSaving = false;
                this.isBusy = false;
            });
            

        },

        update() {
            this.isSaving = true;
            this.isBusy = true;
            this.form.patch('/api/customers/' + this.form.id)
            .then(response => {
                this.isSaving = false;
                this.isBusy = false;
                this.$swal({
                    title: 'Success',
                    text: this.form.name.toUpperCase() + ' was updated.',
                    type: 'success',
                    confirmButtonColor: '#20a8d8',
                }).then(data => { 
                    
                    window.location = '/master-file/customers/' + this.form.id
                });  
            }).catch(error => {
                this.isSaving = false;
                this.isBusy = false;
            });
           
        },
       
        loadData(data) {
            this.form = new Form(data);
        },
    },
    created() {
        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/customers/' + id + '?include=contacts,contracts')
            .then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
            });
        }
        
    },
    mounted() {
    
        console.log("Init customer script...");
    }
});