import {Form} from '@c1_common_js/components/Form';

export const customerService = new Vue({
       
    data: function() {
        return {
            form: new Form({}),
            uri: {
                customers: '/api/customers?limit=' + Number.MAX_SAFE_INTEGER,
            }      
        }
    },   

    methods: {

        getCustomers(){
            
            return this.form.get(this.uri.customers);
        },
    }
});

