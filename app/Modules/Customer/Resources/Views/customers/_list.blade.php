<div class="form-row">
    <div class="form-group col-md-6">
        <label for="name" class="col-form-label">Company Name</label>
        <p>@{{ form . name }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="contactPerson" class="col-form-label">Contact Person</label>
        <p>@{{ form . contactPerson }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="telephone" class="col-form-label">Telephone</label>
        <p>@{{ form . telephone }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="mobile" class="col-form-label">Mobile</label>
        <p>@{{ form . mobile }}</p>
    </div>

    <div class="form-group col-md-6">
        <label for="email" class="col-form-label">E-mail</label>
        <p>@{{ form . email }}</p>
    </div>

    <div class="form-group col-md-6">
        <label for="address" class="col-form-label">Address</label>
        <p>@{{ form . address }}</p>
    </div>
</div>
<div class="row" v-if="form.hasContracts">
    <div class="col-12">
        <table class="table table-simple">
            <thead>
                <tr>
                    <th class="text-center" colspan="5">
                        Contracts
                    </th>
                </tr>
                <tr>
                    <th>Date</th>
                    <th>Number</th>
                    <th>Description</th>
                    <th class="text-right">Amount</th>
                    <th class="text-right">Balance</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="contract in form.contracts.data">
                    <td>@{{ contract . date }}</td>
                    <td><a :href="contract.showUri">@{{ contract . number }}</a></td>
                    <td>@{{ contract . description }}</td>
                    <td class="text-right">@{{ (contract . amount) | numeric }}</td>
                    <td class="text-right">@{{ (contract . balance) | numeric }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
