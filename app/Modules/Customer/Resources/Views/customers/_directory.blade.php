

    <table class="table table-hover">
        <thead class="thead-default">
        <tr>
            <th>Contact Person</th>
            <th>Position</th>
            <th>Telephone</th>
            <th>Fax</th>
            <th>Mobile</th>
            <th>Email</th>
            <th>Actions</th>
            
        </tr>
        </thead>
        <tbody>
        <tr v-for="contact in form.contacts.data">
            <td>@{{contact.contactPerson || "--"}}</td>
            <td>@{{contact.position || "--"}}</td>
            <td>@{{contact.telephone || "--"}}</td>
            <td>@{{contact.fax || "--"}}</td>
            <td>@{{contact.mobile || "--"}}</td>
            <td>@{{contact.email || "--"}}</td>
            <td>
                <button class="btn btn-outline-primary btn-sm" type="button" data-toggle="modal" :data-target="contact.anchor"><i class="fas fa-pencil-alt"></i></button>
                <button class="btn btn-sm btn-outline-danger" @click="deleteContact(contact)" role="button"><i class="fas fa-trash-alt"></i></button>
            </td>
            
        </tr>
        
        </tbody>
    </table>



<div v-for="contact in form.contacts.data">
    <div class="modal fade" :id="contact.id" tabindex="-1" role="dialog" :aria-labelledby="contact.anchor" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Contact</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <v-form @validate="editContact(contact)">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="contactPerson">Contact Person</label>
                                    <input type="text" class="form-control" id="contactPerson" name="contactPerson" v-model="contact.contactPerson" aria-describedby="contactPersonHelp" placeholder=""
                                    :class="{'is-invalid': form.errors.has('contactPerson')}" 
                                    required 
                                    data-msg-required="Enter name">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="position">Position</label>
                                    <input type="text" class="form-control" id="position" name="position" v-model="contact.position" aria-describedby="positionHelp" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="telephone">Telephone</label>
                                    <input type="text" class="form-control" id="telephone" name="telephone" v-model="contact.telephone" aria-describedby="telephoneHelp" placeholder="" >
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="fax">Fax</label>
                                    <input type="text" class="form-control" id="fax" name="fax" v-model="contact.fax" aria-describedby="faxHelp" placeholder="" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="mobile">Mobile</label>
                                    <input type="text" class="form-control" id="mobile" name="mobile" v-model="contact.mobile" aria-describedby="mobileHelp" placeholder="" >
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" name="email" v-model="contact.email" aria-describedby="emailHelp" placeholder="" >
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->

                        <button class="btn btn-primary btn-sm" type="submit" :disabled="isBusy">
                            <div v-if="isSaving">
                                <i class="fas fa-spinner fa-pulse"></i> Saving
                            </div>
                            <div v-if="!isSaving">
                                Save
                            </div>
                        </button>
                    </div>
                </v-form>
                
            </div>
        </div>
    </div>
</div>

