@extends('app')
@section('breadcrumbs', Breadcrumbs::render('customer.master-list'))
@section('content')
    <div id="index" v-cloak>
        <div class="card">
            <div class="card-header">
                Customer
                @if (auth()->user()->can('Create [MAS] Customer'))
                    <div class="float-right">
                        <a href="{{ route('customer_create') }}"><button type="button" class="btn btn-success btn-sm"><i
                                    class="fas fa-plus"></i> </button></a>
                    </div>
                @endif
            </div>
            <div class="card-body">
                <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending"
                    v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">

                    <table class="table table-responsive-sm">
                        <thead>
                            <tr>
                                <th>
                                    <a v-on:click="setSorter('name')">
                                        Name <i class="fa" :class="getSortIcon('name')"></i>
                                    </a>
                                </th>
                                <th class="text-right">
                                    <a v-on:click="setSorter('balance')">
                                        Balance <i class="fa" :class="getSortIcon('balance')"></i>
                                    </a>
                                </th>
                                <th class="text-right">
                                    <a v-on:click="setSorter('telephone')"> Telephone <i class="fa"
                                            :class="getSortIcon('telephone')"></i>
                                    </a>
                                </th>
                                <th class="text-right">
                                    <a v-on:click="setSorter('mobile')"> Mobile <i class="fa"
                                            :class="getSortIcon('mobile')"></i>
                                    </a>
                                </th>
                                <th>
                                    <a v-on:click="setSorter('contact_person')"> Contact Person <i class="fa"
                                            :class="getSortIcon('contact_person')"></i>
                                    </a>
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item in items" v-if="!isLoading">
                                <td><a :href="item.showUri">@{{ item . name }}</a></td>
                                <td class="text-right">@{{ (item . balance) | numeric }}</td>
                                <td class="text-right">@{{ item . telephone || '--' }}</td>
                                <td class="text-right">@{{ item . mobile || '--' }}</td>
                                <td>@{{ item . contactPerson || '--' }}</td>

                            </tr>
                        </tbody>
                    </table>
                </index>
            </div>
        </div>
    </div>
    <!-- End Watchlist-->
@stop
@push('scripts')
    <script src="{{ mix('js/index.js') }}"></script>
@endpush
