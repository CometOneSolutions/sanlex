<?php

namespace App\Modules\Color\Controllers;

use CometOneSolutions\Common\Controllers\ResourceApiController;
use Illuminate\Http\Request;

use App\Modules\Color\Services\ColorRecordService;
use App\Modules\Color\Transformer\ColorTransformer;
use App\Modules\Color\Requests\StoreColor;
use App\Modules\Color\Requests\UpdateColor;
use App\Modules\Color\Requests\DestroyColor;

class ColorApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        ColorRecordService $colorRecordService,
        ColorTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($colorRecordService, $transformer);
        $this->service = $colorRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreColor $request)
    {
        $color = \DB::transaction(function () use ($request) {
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->create(
                $name,
                $user
            );
        });
        return $this->response->item($color, $this->transformer)->setStatusCode(201);
    }

    public function update($colorId, UpdateColor $request)
    {
        $color = \DB::transaction(function () use ($colorId, $request) {
            $color   = $this->service->getById($colorId);
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->update(
                $color,
                $name,
                $user
            );
        });
        return $this->response->item($color, $this->transformer)->setStatusCode(200);
    }

    public function destroy($colorId, DestroyColor $request)
    {
        $color = $this->service->getById($colorId);
        $this->service->delete($color);
        return $this->response->item($color, $this->transformer)->setStatusCode(200);
    }

}
