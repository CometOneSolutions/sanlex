<?php

namespace App\Modules\Color\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Color\Services\ColorRecordService;


class ColorController extends Controller
{
    private $service;

    public function __construct(ColorRecordService $colorRecordService)
    {
        $this->service = $colorRecordService;
        $this->middleware('permission:Read [MAS] Color')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Color')->only('create');
        $this->middleware('permission:Update [MAS] Color')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/colors'
        ]);
        return view('colors.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('colors.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $color = $this->service->getById($id);
        return view('colors.show', compact('color'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $color = $this->service->getById($id);
        return view('colors.edit', compact('color'));
    }
}
