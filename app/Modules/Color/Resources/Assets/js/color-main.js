import {Form} from '@c1_common_js/components/Form';

export const colorService = new Vue({

    data: function() {
        return {
            form: new Form({}),
            uri: {
              colors: '/api/colors?limit=' + Number.MAX_SAFE_INTEGER
            },
        }
    },   

    methods: {

        getColors(){
            return this.form.get(this.uri.colors);
        },
    }
});