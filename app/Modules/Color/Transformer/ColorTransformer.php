<?php

namespace App\Modules\Color\Transformer;

use League\Fractal;
use App\Modules\Color\Models\Color;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class ColorTransformer extends UpdatableByUserTransformer
{
    public function transform(Color $color)
    {
        return [
            'id' => (int) $color->getId(),
            'text' => $color->getName(),
            'name' => $color->getName(),
            'updatedAt' => $color->updated_at,
            'editUri' => route('color_edit', $color->getId()),
            'showUri' => route('color_show', $color->getId()),
        ];
    }
}
