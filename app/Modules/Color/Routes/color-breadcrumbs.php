<?php

Breadcrumbs::register('color.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Colors', route('color_index'));
});
Breadcrumbs::register('color.create', function ($breadcrumbs) {
    $breadcrumbs->parent('color.index');
    $breadcrumbs->push('Create', route('color_create'));
});
Breadcrumbs::register('color.show', function ($breadcrumbs, $color) {
    $breadcrumbs->parent('color.index');
    $breadcrumbs->push($color->getName(), route('color_show', $color->getId()));
});
Breadcrumbs::register('color.edit', function ($breadcrumbs, $color) {
    $breadcrumbs->parent('color.show', $color);
    $breadcrumbs->push('Edit', route('color_edit', $color->getId()));
});
