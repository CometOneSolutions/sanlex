<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'colors'], function () use ($api) {
        $api->get('/', 'App\Modules\Color\Controllers\ColorApiController@index');
        $api->get('{colorId}', 'App\Modules\Color\Controllers\ColorApiController@show');
        $api->post('/', 'App\Modules\Color\Controllers\ColorApiController@store');
        $api->patch('{colorId}', 'App\Modules\Color\Controllers\ColorApiController@update');
        $api->delete('{colorId}', 'App\Modules\Color\Controllers\ColorApiController@destroy');
    });
});
