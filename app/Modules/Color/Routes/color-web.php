<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'colors'], function () {
        Route::get('/', 'ColorController@index')->name('color_index');
        Route::get('/create', 'ColorController@create')->name('color_create');
        Route::get('{colorId}/edit', 'ColorController@edit')->name('color_edit');
        Route::get('{colorId}/print', 'ColorController@print')->name('color_print');
        Route::get('{colorId}', 'ColorController@show')->name('color_show');
    });

});
