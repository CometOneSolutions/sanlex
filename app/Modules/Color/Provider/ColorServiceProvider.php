<?php

namespace App\Modules\Color\Provider;

use App\Modules\Color\Models\Color;
use App\Modules\Color\Models\ColorModel;
use App\Modules\Color\Repositories\ColorRepository;
use App\Modules\Color\Repositories\EloquentColorRepository;
use App\Modules\Color\Services\ColorRecordService;
use App\Modules\Color\Services\ColorRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ColorServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Color/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(ColorRecordService::class, ColorRecordServiceImpl::class);
        $this->app->bind(ColorRepository::class, EloquentColorRepository::class);
        $this->app->bind(Color::class, ColorModel::class);
    }
}
