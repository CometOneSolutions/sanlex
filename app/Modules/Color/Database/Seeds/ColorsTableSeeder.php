<?php

namespace App\Modules\Color\Database\Seeds;

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $colors = factory(ColorModel::class, 5)->create();
    }
}
