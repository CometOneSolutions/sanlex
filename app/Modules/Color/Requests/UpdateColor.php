<?php

namespace App\Modules\Color\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateColor extends ColorRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Color');
    }

}