<?php

namespace App\Modules\Color\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyColor extends ColorRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Color');
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}