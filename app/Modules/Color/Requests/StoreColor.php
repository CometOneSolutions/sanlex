<?php

namespace App\Modules\Color\Requests;

use Dingo\Api\Http\FormRequest;

class StoreColor extends ColorRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Color');
    }

}
