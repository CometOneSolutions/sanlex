<?php

namespace App\Modules\Color\Repositories;

use App\Modules\Color\Models\Color;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentColorRepository extends EloquentRepository implements ColorRepository
{
    public function __construct(Color $color)
    {
        parent::__construct($color);
    }

    public function save(Color $color)
    {
        return $color->save();
    }

    public function delete(Color $color)
    {
        return $color->delete();
    }

}
