<?php

namespace App\Modules\Color\Repositories;

use App\Modules\Color\Models\Color;
use CometOneSolutions\Common\Repositories\Repository;

interface ColorRepository extends Repository
{
    public function save(Color $color);
    public function delete(Color $color);
}
