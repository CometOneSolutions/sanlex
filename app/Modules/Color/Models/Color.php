<?php

namespace App\Modules\Color\Models;


interface Color
{
    public function getId();

    public function getName();

    public function setName($value);


}
