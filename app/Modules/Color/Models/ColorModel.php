<?php

namespace App\Modules\Color\Models;

use App\Modules\Coil\Models\CoilModel;
use App\Modules\PaintAdhesive\Models\PaintAdhesiveModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;

class ColorModel extends Model implements Color, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'colors';

    public function coils()
    {
        return $this->hasMany(CoilModel::class, 'color_id');
    }

    public function paintAdhesives()
    {
        return $this->hasMany(PaintAdhesiveModel::class, 'color_id');
    }

    public function getPaintAdhesives()
    {
        return $this->paintAdhesives;
    }

    public function getCoils()
    {
        return $this->coils;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    public function scopePrePainted($query)
    {
        return $query->where('name', '<>', 'STAINLESS STEEL')->where('name', '<>', 'GI');
    }
}
