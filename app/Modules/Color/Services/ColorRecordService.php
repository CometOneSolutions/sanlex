<?php

namespace App\Modules\Color\Services;

use App\Modules\Color\Models\Color;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface ColorRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        Color $color,
        $name,
        User $user = null
    );

    public function delete(Color $color);
}
