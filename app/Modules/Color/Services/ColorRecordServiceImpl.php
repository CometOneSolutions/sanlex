<?php

namespace App\Modules\Color\Services;

use App\Modules\Color\Models\Color;
use App\Modules\Color\Repositories\ColorRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Auth\Models\Users\User;

class ColorRecordServiceImpl extends RecordServiceImpl implements ColorRecordService
{
    use CanUpdateProductName;

    private $color;
    private $colors;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(ColorRepository $colors, Color $color)
    {
        $this->colors = $colors;
        $this->color = $color;
        parent::__construct($colors);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $color = new $this->color;
        $color->setName($name);
        if ($user) {
            $color->setUpdatedByUser($user);
        }
        $this->colors->save($color);
        return $color;
    }

    public function update(
        Color $color,
        $name,
        User $user = null
    ) {
        $tempColor = clone $color;
        $tempColor->setName($name);
        if ($user) {
            $tempColor->setUpdatedByUser($user);
        }
        $this->colors->save($tempColor);


        $this->updateProductableProductNames([
            $tempColor->getCoils(),
            $tempColor->getPaintAdhesives()
        ]);

        return $tempColor;
    }

    public function delete(Color $color)
    {
        $this->colors->delete($color);
        return $color;
    }
}
