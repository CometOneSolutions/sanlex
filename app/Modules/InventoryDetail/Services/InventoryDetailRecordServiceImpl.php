<?php

namespace App\Modules\InventoryDetail\Services;

use App\Modules\Product\Models\Product;
use App\Modules\JobOrders\Models\JobOrder;
use App\Modules\Inventory\Models\Inventory;
use CometOneSolutions\Auth\Models\Users\User;
use App\Modules\InventoryDetail\Models\InventoryDetail;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use App\Modules\Inventory\Repositories\InventoryRepository;
use App\Modules\Receiving\Repositories\ReceivingRepository;
use App\Modules\Adjustment\Services\AdjustmentRecordService;
use App\Modules\InventoryDetail\Models\InventoryDetailModel;
use App\Modules\InventoryDetail\Repositories\InventoryDetailRepository;
use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetail;


class InventoryDetailRecordServiceImpl extends RecordServiceImpl implements InventoryDetailRecordService
{
    private $inventoryDetails;
    private $inventoryDetail;
    private $receivingRepository;
    private $inventories;
    private $adjustmentRecordService;

    public function __construct(InventoryDetailRepository $inventoryDetails, InventoryDetail $inventoryDetail, ReceivingRepository $receivingRepository, InventoryRepository $inventories, AdjustmentRecordService $adjustmentRecordService)
    {
        parent::__construct($inventoryDetails);
        $this->inventoryDetails = $inventoryDetails;
        $this->inventoryDetail = $inventoryDetail;
        $this->receivingRepository = $receivingRepository;
        $this->inventories = $inventories;
        $this->adjustmentRecordService = $adjustmentRecordService;
    }

    protected $availableFilters = [];

    public function delete(InventoryDetail $inventoryDetail)
    {
        $this->inventoryDetails->delete($inventoryDetail);
        return $inventoryDetail;
    }

    private function getReceivingRecord($receivingId)
    {
        $repo = clone $this->receivingRepository;
        return $repo->where('id', '=', $receivingId)->getFirst();
    }

    public function create(
        InventoryDetail $inventoryDetail
    ) {
        $this->inventoryDetails->save($inventoryDetail);
        return $inventoryDetail;
    }

    public function createFromExistingAccessory(InventoryDetail $inventoryDetail, ReceivingAccessoryDetail $receivingAccessoryDetail)
    { }

    public function createFromRestocking(Product $product, Inventory $inventory, $quantity, $identifier)
    {
        $inventoryDetail = new InventoryDetailModel();
        $inventoryDetail->setInventory($inventory);
        $inventoryDetail->setUom($product->getUom());
        $inventoryDetail->setQuantity($quantity);
        $inventoryDetail->setCondition(InventoryDetail::GOOD);
        $inventoryDetail->setSlitIdentifier($identifier);
        $inventoryDetail->setReceivedQuantity($quantity);
        $this->create($inventoryDetail);
        $new_inventory = $inventory::find($inventory->getId());
        $new_inventory->updateStock();

        $this->inventories->save($new_inventory);

        return $inventoryDetail;
    }

    public function updateInventoryStockFromDeliveryReceiptDetails($deliveryReceiptDetails)
    {
        foreach ($deliveryReceiptDetails as $detail) {
            $inventoryDetail = $detail->getInventoryDetail();
            $inventoryDetail->updateStock();
            $this->inventoryDetails->save($inventoryDetail);
            $inventory = $inventoryDetail->getInventory();
            $inventory->updateStock();
            $this->inventories->save($inventory);
        }
        return $deliveryReceiptDetails;
    }

    public function updateInventoryStockFromJobOrder(JobOrder $jobOrder)
    {
        foreach ($jobOrder->getJobOrderDetails() as $detail) {
            $inventoryDetail = $detail->getInventoryDetail();
            $inventoryDetail->updateStock();
            $this->inventoryDetails->save($inventoryDetail);
            $inventory = $inventoryDetail->getInventory();
            $inventory->updateStock();
            $this->inventories->save($inventory);
        }
        return $jobOrder;
    }

    public function adjust(InventoryDetail $inventoryDetail, $adjustedQuantity, $remarks, User $user)
    {
        return $this->adjustmentRecordService->create($inventoryDetail, $adjustedQuantity, $remarks, $user);
    }

    public function toggleToDamaged(InventoryDetail $inventoryDetail)
    {
        $tmpInventoryDetail = clone $inventoryDetail;
        $tmpInventoryDetail->setCondition(InventoryDetail::BAD);
        $this->inventoryDetails->save($tmpInventoryDetail);
        return $inventoryDetail;
    }

    public function toggleToGood(InventoryDetail $inventoryDetail)
    {
        $tmpInventoryDetail = clone $inventoryDetail;
        $tmpInventoryDetail->setCondition(InventoryDetail::GOOD);
        $this->inventoryDetails->save($tmpInventoryDetail);
        return $inventoryDetail;
    }

    public function move(InventoryDetail $inventoryDetail, Inventory $inventory)
    {
        $previousInventory = $inventoryDetail->getInventory();
        $inventoryDetail->setInventory($inventory);
        $this->inventoryDetails->save($inventoryDetail);
        $previousInventory->updateStock();
        $this->inventories->save($previousInventory);
        $inventory->updateStock();
        $this->inventories->save($inventory);
        return $inventoryDetail;
    }
}
