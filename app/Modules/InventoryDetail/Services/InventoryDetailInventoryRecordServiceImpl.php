<?php

namespace App\Modules\InventoryDetail\Services;

use \DateTime;
use Carbon\Carbon;
use App\Modules\Product\Models\Product;
use App\Modules\JobOrders\Models\JobOrder;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Inventory\Model\Inventory;
use \App\Modules\Inventory\Models\Inventory as SanlexInventory;
use CometOneSolutions\Inventory\Model\Inventoriable;
use App\Modules\InventoryDetail\Models\InventoryDetail;
use CometOneSolutions\Inventory\Model\ItemMovementType;

use CometOneSolutions\Common\Services\DecoratorRecordServiceImpl;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetail;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
// In: Warehouses
// Out: Purchases

class InventoryDetailInventoryRecordServiceImpl extends DecoratorRecordServiceImpl implements InventoryDetailRecordService
{
    protected $inventoryDetailRecordService;
    protected $itemMovementRecordService;
    protected $locationRecordService;

    public function __construct(
        InventoryDetailRecordService $inventoryDetailRecordService,
        ItemMovementRecordService $itemMovementRecordService,
        LocationRecordService $locationRecordService
    ) {
        parent::__construct($inventoryDetailRecordService);
        $this->inventoryDetailRecordService = $inventoryDetailRecordService;
        $this->itemMovementRecordService = $itemMovementRecordService;
        $this->locationRecordService = $locationRecordService;
    }

    protected function createItemMovements(Inventoriable $inventoriable, DateTime $date, $quantity, Inventory $inventory)
    {
        $this->itemMovementRecordService->create(
            $inventoriable,
            $date,
            ItemMovementType::IN,
            $quantity,
            $this->locationRecordService->getByName('Warehouses'),
            $inventory
        );

        $this->itemMovementRecordService->create(
            $inventoriable,
            $date,
            ItemMovementType::OUT,
            $quantity,
            $this->locationRecordService->getByName('Purchases'),
            $inventory
        );
        return $this;
    }

    public function create(InventoryDetail $inventoryDetail)
    {
        $created = $this->inventoryDetailRecordService->create($inventoryDetail);
        $date = $created->created_at;
        $quantity = $inventoryDetail->getQuantity();

        if ($inventoryDetail->hasReceivingAccessoryDetail()) {
            $this->createItemMovements($created->getReceivingAccessoryDetail(), $date, $quantity, $created);
        } else {
            $this->createItemMovements($created->getReceiving(), $date, $quantity, $created);
        }

        return $created;
    }

    public function createFromExistingAccessory(InventoryDetail $inventoryDetail, ReceivingAccessoryDetail $receivingAccessoryDetail)
    {
        $date = $receivingAccessoryDetail->receivingAccessory->updated_at;
        $quantity = $receivingAccessoryDetail->getQuantity();

        $this->createItemMovements($receivingAccessoryDetail, $date, $quantity, $inventoryDetail);
    }

    public function createFromRestocking(Product $product, SanlexInventory $inventory, $quantity, $identifier)
    {
        $created = $this->inventoryDetailRecordService->createFromRestocking($product, $inventory, $quantity, $identifier);
        $date = $created->created_at;
        $this->createItemMovements($created, $date, $quantity, $created);
        return $created;
    }

    public function delete(InventoryDetail $inventoryDetail)
    {
        $deleted = $this->inventoryDetailRecordService->delete($inventoryDetail);
        $this->itemMovementRecordService->deleteFromInventoriable($inventoryDetail);
        return $deleted;
    }

    public function adjust(InventoryDetail $inventoryDetail, $adjustedQuantity, $remarks, User $user)
    {
        return $this->inventoryDetailRecordService->adjust($inventoryDetail, $adjustedQuantity, $remarks, $user);
    }

    public function updateInventoryStockFromDeliveryReceiptDetails($deliveryReceiptDetails)
    {
        return $this->inventoryDetailRecordService->updateInventoryStockFromDeliveryReceiptDetails($deliveryReceiptDetails);
    }

    public function updateInventoryStockFromJobOrder(JobOrder $jobOrder)
    {
        return $this->inventoryDetailRecordService->updateInventoryStockFromJobOrder($jobOrder);
    }

    public function toggleToDamaged(InventoryDetail $inventoryDetail)
    {
        return $this->inventoryDetailRecordService->toggleToDamaged($inventoryDetail);
    }

    public function toggleToGood(InventoryDetail $inventoryDetail)
    {
        return $this->inventoryDetailRecordService->toggleToGood($inventoryDetail);
    }

    public function move(InventoryDetail $inventoryDetail, SanlexInventory $inventory)
    {
        return $this->inventoryDetailRecordService->move($inventoryDetail, $inventory);
    }
}
