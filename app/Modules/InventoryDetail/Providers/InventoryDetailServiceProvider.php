<?php

namespace App\Modules\InventoryDetail\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use App\Modules\InventoryDetail\Repositories\InventoryDetailRepository;
use App\Modules\InventoryDetail\Repositories\EloquentInventoryDetailRepository;
use App\Modules\InventoryDetail\Models\InventoryDetail;
use App\Modules\InventoryDetail\Models\InventoryDetailModel;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordService;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordServiceImpl;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use App\Modules\InventoryDetail\Services\InventoryDetailInventoryRecordServiceImpl;


class InventoryDetailServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/InventoryDetail/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        // $this->app->bind(InventoryDetailRecordService::class, InventoryDetailRecordServiceImpl::class);
        $this->app->bind(InventoryDetailRepository::class, EloquentInventoryDetailRepository::class);
        $this->app->bind(InventoryDetail::class, InventoryDetailModel::class);

        $this->app->singleton(InventoryDetailRecordService::class, function ($app) {
            $inventoryDetailRecordService = $app->make(InventoryDetailRecordServiceImpl::class);
            $inventoryDetailInventoryRecordService = new InventoryDetailInventoryRecordServiceImpl(
                $inventoryDetailRecordService,
                $app->make(ItemMovementRecordService::class),
                $app->make(LocationRecordService::class)
            );
            return $inventoryDetailInventoryRecordService;
        });
    }
}
