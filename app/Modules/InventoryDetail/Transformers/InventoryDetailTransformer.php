<?php

namespace App\Modules\InventoryDetail\Transformers;

use App\Modules\Inventory\Transformers\InventoryTransformer;
use App\Modules\InventoryDetail\Models\InventoryDetail;
use App\Modules\Receiving\Transformer\ReceivingTransformer;
use App\Modules\ReceivingAccessoryDetail\Transformer\ReceivingAccessoryDetailTransformer;
use App\Modules\Uom\Transformer\UomTransformer;
use League\Fractal;

class InventoryDetailTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'inventory',
        'receiving',
        'uom',
        'receivingAccessoryDetail'
    ];

    public function transform(InventoryDetail $inventoryDetail)
    {
        return [
            'id'                    => (int) $inventoryDetail->getId(),
            'inventoryDetailId'     => (int) $inventoryDetail->getId(),
            'name'                  => $inventoryDetail->getName(),
            'productName'           => $inventoryDetail->getName2(),
            'text'                  => $inventoryDetail->getName(),
            'inventoryId'           => (int) $inventoryDetail->getInventoryId(),
            'quantity'              => (float) $inventoryDetail->getQuantity(),
            'balance'               => $inventoryDetail->getQuantity() - $inventoryDetail->getBalance(),
            'adjustedQuantity'      => (float) 0.000,
            'condition'             => $inventoryDetail->getCondition(),
            'slitIdentifier'        => $inventoryDetail->getSlitIdentifier(),
            'anchor'                => '#inv' . $inventoryDetail->getId(),
            'target'                => 'inv' . $inventoryDetail->getId(),
            'createdAt'              => $inventoryDetail->getCreatedAt()->toDateTimeString(),
            'type'                  => $inventoryDetail->getInventory()->getUom()->getName(),
            'transactionUri' => route('inventory-detail_transactions', $inventoryDetail->getId()),
            'moveUri'               => route("inventory_coil_move", $inventoryDetail->getId()),
            'remarks' => "",
            'others'                => $inventoryDetail->getOtherDraftJobOrderDetails()
        ];
    }

    public function includeInventory(InventoryDetail $inventoryDetail)
    {
        $inventory = $inventoryDetail->getInventory();
        return $this->item($inventory, new InventoryTransformer);
    }

    public function includeReceiving(InventoryDetail $inventoryDetail)
    {
        $receiving = $inventoryDetail->getReceiving();
        if ($receiving) {
            return $this->item($receiving, new ReceivingTransformer);
        }
        return $this->null();
    }

    public function includeReceivingAccessoryDetail(InventoryDetail $inventoryDetail)
    {
        $receivingAccessoryDetail = $inventoryDetail->getReceivingAccessoryDetail();
        if ($receivingAccessoryDetail) {
            return $this->item($receivingAccessoryDetail, new ReceivingAccessoryDetailTransformer);
        }
        return $this->null();
    }

    public function includeUom(InventoryDetail $inventoryDetail)
    {
        $uom = $inventoryDetail->getUom();
        return $this->item($uom, new UomTransformer);
    }
}
