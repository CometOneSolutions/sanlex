<?php

namespace App\Modules\InventoryDetail\Repositories;

use App\Modules\InventoryDetail\Models\InventoryDetail;
use CometOneSolutions\Common\Repositories\Repository;


interface InventoryDetailRepository extends Repository
{
    public function save(InventoryDetail $inventoryDetail);
    public function delete(InventoryDetail $inventoryDetail);
}
