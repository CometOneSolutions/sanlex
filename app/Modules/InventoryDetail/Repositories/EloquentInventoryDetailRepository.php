<?php

namespace App\Modules\InventoryDetail\Repositories;

use App\Modules\Accessory\Models\AccessoryModel;
use App\Modules\BackingSide\Models\BackingSideModel;
use App\Modules\Coil\Models\CoilModel;
use App\Modules\Fastener\Models\FastenerModel;
use App\Modules\Insulation\Models\InsulationModel;
use App\Modules\InventoryDetail\Models\InventoryDetail;
use App\Modules\MiscAccessory\Models\MiscAccessoryModel;
use App\Modules\PaintAdhesive\Models\PaintAdhesiveModel;
use App\Modules\PvcFitting\Models\PvcFittingModel;
use App\Modules\Screw\Models\ScrewModel;
use App\Modules\Skylight\Models\SkylightModel;
use App\Modules\StainlessSteel\Models\StainlessSteelModel;
use App\Modules\StructuralSteel\Models\StructuralSteelModel;
use CometOneSolutions\Common\Repositories\EloquentRepository;


class EloquentInventoryDetailRepository extends EloquentRepository implements InventoryDetailRepository
{
    protected $inventory;

    public function __construct(InventoryDetail $inventoryDetail)
    {
        parent::__construct($inventoryDetail);
        $this->inventoryDetail = $inventoryDetail;
    }

    public function save(InventoryDetail $inventoryDetail)
    {
        $result = $inventoryDetail->save();
        return $result;
    }

    public function delete(InventoryDetail $inventoryDetail)
    {
        return $inventoryDetail->delete();
    }

    public function filterByUom($name)
    {
        return $this->model->whereHas('uom', function ($uom) use ($name) {
            return $uom->where('name', 'LIKE', $name);
        });
    }

    public function filterByStock($unit)
    {
        return $this->model->whereHas('inventory', function ($inventory) use ($unit) {
            return $inventory->where('stock', 'LIKE', $unit);
        });
    }

    public function filterByProduct($name)
    {
        return $this->model->whereHas('inventory', function ($inventory) use ($name) {
            return $inventory->whereHas('product', function ($product) use ($name) {
                return $product->where('name', 'LIKE', $name);
            });
        });
    }

    public function filterByProductId($id)
    {
        return $this->model->whereHas('inventory', function ($inventory) use ($id) {
            return $inventory->where('product_id', $id);
        });
    }

    public function filterByCoilNumber($coilNumber)
    {
        return $this->model->whereHas('receiving', function ($receiving) use ($coilNumber) {
            return $receiving->where('coil_number', 'LIKE', $coilNumber);
        });
    }

    public function filterByDamagedCoilNumberOrSlitIdentifier($coilNumber)
    {
        return $this->model
            ->whereHas('receiving', function ($receiving) use ($coilNumber) {
                return $receiving->where('coil_number', 'LIKE', $coilNumber);
            })->orWhere('slit_identifier', 'LIKE', $coilNumber)
            ->where('condition', '=', 'Bad');
    }

    public function filterBySupplierName($supplierName)
    {
        return $this->model->whereHas('inventory', function ($inventory) use ($supplierName) {
            return $inventory->whereHas('product', function ($product) use ($supplierName) {
                return $product->whereHas('supplier', function ($supplier) use ($supplierName) {
                    return $supplier->where('name', 'LIKE', $supplierName);
                });
            });
        });
    }

    public function filterBySupplierShortCode($supplierShortCode)
    {
        return $this->model->whereHas('inventory', function ($inventory) use ($supplierShortCode) {
            return $inventory->whereHas('product', function ($product) use ($supplierShortCode) {
                return $product->whereHas('supplier', function ($supplier) use ($supplierShortCode) {
                    return $supplier->where('short_code', 'LIKE', $supplierShortCode);
                });
            });
        });
    }

    public function filterByCoatingName($coatingName)
    {
        return $this->model->whereHas('inventory', function ($inventory) use ($coatingName) {
            return $inventory->whereHas('product', function ($product) use ($coatingName) {
                return $product->whereHas('coil', function ($coil) use ($coatingName) {
                    return $coil->whereHas('coating', function ($coating) use ($coatingName) {
                        return $coating->where('name', 'LIKE', $coatingName);
                    });
                });
            });
        });
    }

    public function filterByThicknessName($thicknessName)
    {
        return $this->model->whereHas('inventory', function ($inventory) use ($thicknessName) {
            return $inventory->whereHas('product', function ($product) use ($thicknessName) {
                return $product->whereHas('coil', function ($coil) use ($thicknessName) {
                    return $coil->whereHas('thickness', function ($thickness) use ($thicknessName) {
                        return $thickness->where('name', 'LIKE', $thicknessName);
                    });
                });
            });
        });
    }

    public function filterByWidthName($widthName)
    {
        return $this->model->whereHas('inventory', function ($inventory) use ($widthName) {
            return $inventory->whereHas('product', function ($product) use ($widthName) {
                return $product->whereHas('coil', function ($coil) use ($widthName) {
                    return $coil->whereHas('width', function ($width) use ($widthName) {
                        return $width->where('name', 'LIKE', $widthName);
                    });
                });
            });
        });
    }

    public function filterByColorName($colorName)
    {
        return $this->model->whereHas('inventory', function ($inventory) use ($colorName) {
            return $inventory->whereHas('product', function ($product) use ($colorName) {
                return $product->whereHas('coil', function ($coil) use ($colorName) {
                    return $coil->whereHas('color', function ($color) use ($colorName) {
                        return $color->where('name', 'LIKE', $colorName);
                    });
                });
            });
        });
    }

    public function filterByType($type)
    {
        return $this->model->whereHas('inventory', function ($inventory) use ($type) {
            return $inventory->whereHas('product', function ($product) use ($type) {
                return $product->where('productable_type', 'LIKE', '%' . $type . '%');
            });
        });
    }

    public function filterByName($name)
    {
        return $this->model->whereHas('inventory', function ($inventory) use ($name) {
            return $inventory->whereHas('product', function ($product) use ($name) {
                return $product->where('name', 'LIKE', $name);
            });
        })->orWhereHas('receiving', function ($receiving) use ($name) {
            return $receiving->where('coil_number', 'LIKE', $name);
        })->orWhere('slit_identifier', 'LIKE', $name);
    }

    protected function orderByProductName($direction)
    {
        return $this->model->join('inventories', 'inventories.id', 'inventory_details.inventory_id')
            ->join('products', 'products.id', 'inventories.product_id')
            ->select('products.name as product_name', 'inventory_details.*')
            ->orderBy('product_name', $direction);
    }

    protected function filterByCoilProductsOnly($value)
    {
        return $this->model->whereHas('inventory', function ($inv) use ($value) {
            return $inv->whereHas('product', function ($prod) {
                $prod->where('productable_type', CoilModel::class);
            });
        });
    }

    protected function filterByAccessoryProductsOnly($value)
    {
        return $this->model->whereHas('inventory', function ($inv) use ($value) {
            return $inv->whereHas('product', function ($prod) {
                $prod
                    ->where('productable_type', AccessoryModel::class)
                    ->orWhere('productable_type', BackingSideModel::class)
                    ->orWhere('productable_type', SkylightModel::class)
                    ->orWhere('productable_type', InsulationModel::class)
                    ->orWhere('productable_type', ScrewModel::class)
                    ->orWhere('productable_type', MiscAccessoryModel::class)
                    ->orWhere('productable_type', StainlessSteelModel::class)
                    ->orWhere('productable_type', StructuralSteelModel::class)
                    ->orWhere('productable_type', PvcFittingModel::class)
                    ->orWhere('productable_type', FastenerModel::class)
                    ->orWhere('productable_type', PaintAdhesiveModel::class);
            });
        });
    }

    protected function orderByUomName($direction)
    {
        return $this->model->join('uoms', 'uoms.id', 'inventory_details.uom_id')
            ->select('uoms.name as uom_name', 'inventory_details.*')
            ->orderBy('uom_name', $direction);
    }

    protected function orderByCoilNumber($direction)
    {
        return $this->model->join('receivings', 'receivings.id', 'inventory_details.receiving_id')
            ->orderBy('coil_number', $direction);
    }
}
