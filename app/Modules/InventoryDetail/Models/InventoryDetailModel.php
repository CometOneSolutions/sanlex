<?php

namespace App\Modules\InventoryDetail\Models;

use App\Modules\Uom\Models\Uom;
use App\Modules\Uom\Models\UomModel;
use Illuminate\Database\Eloquent\Model;
use App\Modules\JobOrders\Models\JobOrder;
use App\Modules\Inventory\Models\Inventory;
use App\Modules\Receiving\Models\Receiving;
use App\Modules\Inventory\Models\InventoryModel;
use App\Modules\Receiving\Models\ReceivingModel;
use App\Modules\Adjustment\Models\AdjustmentModel;
use CometOneSolutions\Inventory\Model\Inventoriable;
use App\Modules\JobOrders\Models\JobOrderDetailModel;
use CometOneSolutions\Inventory\Traits\HasItemMovements;

use CometOneSolutions\Inventory\Model\Inventory as C1Inventory;
use App\Modules\DeliveryReceiptDetails\Models\DeliveryReceiptDetailModel;
use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetail;
use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetailModel;

class InventoryDetailModel extends Model implements InventoryDetail, Inventoriable, C1Inventory
{
    use HasItemMovements;

    protected $table = 'inventory_details';

    public function inventory()
    {
        return $this->belongsTo(InventoryModel::class, 'inventory_id');
    }

    public function receiving()
    {
        return $this->belongsTo(ReceivingModel::class, 'receiving_id');
    }

    public function receivingAccessoryDetail()
    {
        return $this->belongsTo(ReceivingAccessoryDetailModel::class, 'receiving_accessory_detail_id');
    }

    public function adjustments()
    {
        return $this->hasMany(AdjustmentModel::class, 'inventory_detail_id');
    }

    public function deliveryReceiptDetails()
    {
        return $this->hasMany(DeliveryReceiptDetailModel::class, 'inventory_detail_id');
    }

    public function uom()
    {
        return $this->belongsTo(UomModel::class, 'uom_id');
    }

    public function jobOrderDetails()
    {
        return $this->hasMany(JobOrderDetailModel::class, 'inventory_detail_id');
    }

    public function hasReceivingAccessoryDetail()
    {
        return $this->receivingAccessoryDetail()->exists();
    }

    public function setReceivingAccessoryDetail(ReceivingAccessoryDetail $receivingAccessoryDetail)
    {
        $this->receivingAccessoryDetail()->associate($receivingAccessoryDetail);
        $this->receiving_accessory_detail_id = $receivingAccessoryDetail->getId();
        return $this;
    }

    public function getReceivingAccessoryDetail()
    {
        return $this->receivingAccessoryDetail;
    }

    public function getJobOrderDetails()
    {
        return $this->jobOrderDetails;
    }

    public function setInventory(Inventory $inventory)
    {
        $this->inventory()->associate($inventory);
        $this->inventory_id = $inventory->getId();
        return $this;
    }

    public function getInventory()
    {
        return $this->inventory;
    }

    public function getInventoryId()
    {
        return $this->inventory_id;
    }

    public function setReceiving(Receiving $receiving)
    {
        $this->receiving()->associate($receiving);
        $this->receiving_id = $receiving->getId();
        return $this;
    }

    public function unsetReceiving()
    {
        $this->receiving()->dissociate();
        $this->receiving_id = null;
        return $this;
    }


    public function setReceivedQuantity($value)
    {
        $this->received_quantity = $value;
        return $this;
    }

    public function getReceivedQuantity()
    {
        return $this->received_quantity;
    }

    public function getReceiving()
    {
        return $this->receiving;
    }

    public function getReceivingId()
    {
        return $this->receiving_id;
    }

    public function setUom(Uom $uom)
    {
        $this->uom()->associate($uom);
        $this->uom_id = $uom->getId();
        return $this;
    }

    public function getUom()
    {
        return $this->uom;
    }

    public function getUomId()
    {
        return $this->uom_id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setQuantity($value)
    {
        // if ($value < 0) {
        //     throw new GeneralApiException('Insufficient Stock from Inventory.', 'Error', 400, true);
        // }
        $this->quantity = $value;
        return $this;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setCondition($value)
    {
        $this->condition = $value;
        return $this;
    }

    public function getCondition()
    {
        return $this->condition;
    }

    public function getSlitIdentifier()
    {
        return $this->slit_identifier;
    }

    public function setSlitIdentifier($value)
    {
        $this->slit_identifier = $value;
        return $this;
    }

    public function updateStock()
    {
        $this->setQuantity($this->computeStock());
        return $this;
    }

    public function updateAccessoryStock()
    {
        $this->setQuantity($this->computeAccessoryStock());
        $this->setReceivedQuantity($this->getInventory()->getProduct()->receivingAccessoryDetails()->received()->sum('quantity'));
        return $this;
    }

    private function computeStock()
    {
        $received = round(floatval($this->getReceivedQuantity()), 3);
        $adjustment = round(floatval($this->adjustments()->sum('adjusted_quantity')), 3);
        $deliveryReceipts = round(floatval($this->deliveryReceiptDetails()->approved()->sum('ordered_quantity')), 3);
        $jobOrders = round(floatval($this->jobOrderDetails()->approved()->sum('weight')), 3);
        $stock = round(($received - $jobOrders - $deliveryReceipts) + ($adjustment), 3);
        $stock = ($stock == 0) ? 0 : $stock;

        return $stock;
    }

    private function computeAccessoryStock()
    {
        $received = round(floatval($this->getInventory()->getProduct()->receivingAccessoryDetails()->received()->sum('quantity')), 2);
        $adjustment = round(floatval($this->adjustments()->sum('adjusted_quantity')), 2);
        $deliveryReceipts = round(floatval($this->deliveryReceiptDetails()->approved()->sum('ordered_quantity')), 2);
        $stock = round(($received - $deliveryReceipts) + ($adjustment), 2);
        $stock = ($stock == 0) ? 0 : $stock;
        return $stock;
    }

    public function isCoil()
    {
        if ($this->getInventory()->getUom()->getName() == Uom::MT) {
            return true;
        }
        return false;
    }

    public function getName()
    {
        return ($this->getInventory()->getUom()->getName() == Uom::MT)
            ? '(' . (($this->getReceiving() != null)
                ? $this->getReceiving()->getCoilNumber()
                : $this->getSlitIdentifier() . '*') . ') '
            . $this->getInventory()->getProduct()->getName()
            : $this->getInventory()->getProduct()->getName();
    }

    public function getName2()
    {
        return $this->getInventory()->getProduct()->getName();
    }

    public function getInventoryRefNo()
    {
        return 'MANUAL RESTOCKING';
    }

    public function getInventoryUri()
    {
        return null;
    }

    public function getCreatedAt()
    {
        $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'], 'UTC');
        return $date->setTimezone('Asia/Manila');
    }

    public function getOtherDraftJobOrderDetails()
    {
        $this->load(['jobOrderDetails.jobOrder']);
        $others = collect();
        foreach ($this->getJobOrderDetails() as $jobOrderDetail) {
            if ($jobOrderDetail->getJobOrder()->getStatus() == JobOrder::DRAFT) {
                $others->push($jobOrderDetail);
            }
        }

        return $others->sortBy(function ($other, $key) {
            return $other->created_at;
        });
    }

    public function getBalance()
    {
        return $this->getOtherDraftJobOrderDetails()->sum('weight');
    }

    public function scopeCondition($query, $condition)
    {
        return $query->whereCondition($condition);
    }
}
