<?php

namespace App\Modules\InventoryDetail\Models;

use App\Modules\Uom\Models\Uom;
use App\Modules\Inventory\Models\Inventory;
use App\Modules\Receiving\Models\Receiving;

interface InventoryDetail
{
    const GOOD = 'Good';
    const BAD = 'Bad';

    public function getId();

    public function setInventory(Inventory $inventory);

    public function getInventory();

    public function getInventoryId();

    public function setReceiving(Receiving $receiving);

    public function getReceiving();

    public function getReceivingId();

    public function setUom(Uom $uom);

    public function getUom();

    public function getUomId();

    // public function setLinearMeter($value);

    // public function getLinearMeter();

    public function getQuantity();

    public function setCondition($value);

    public function getCondition();
}
