<?php

namespace App\Modules\InventoryDetail\Controllers;

use JavaScript;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordService;

class InventoryDetailController extends Controller
{
    private $service;

    public function __construct(InventoryDetailRecordService $inventoryDetailRecordService)
    {
        $this->middleware(['auth', 'permission:Read [INV] Inventory']);
        $this->service = $inventoryDetailRecordService;
    }

    public function transactions($id)
    {
        $inventoryDetail = $this->service->getById($id);

        JavaScript::put([
            'filterable' => [],
            'toLastPage' => true,
            'sorter' => 'id',
            'id' => $id,
            'baseUrl' => '/api/inventory-details/' . $id . '/transactions?include=inventoriable',
        ]);

        return view('inventory-details.transactions', compact('inventoryDetail'));
    }
}
