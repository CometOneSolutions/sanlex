<?php

namespace App\Modules\InventoryDetail\Controllers;


use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordService;
use App\Modules\InventoryDetail\Transformers\InventoryDetailTransformer;
use CometOneSolutions\Common\Utils\Filter;
use CometOneSolutions\Common\Utils\Sorter;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use CometOneSolutions\Inventory\Transformers\ItemMovementTransformer;

class InventoryDetailApiController extends ResourceApiController
{
    private $locationRecordService;
    private $itemMovementRecordService;
    private $itemMovementTransformer;
    protected $service;

    public function __construct(
        InventoryDetailRecordService $inventoryDetailRecordService,
        LocationRecordService $locationRecordService,
        ItemMovementRecordService $itemMovementRecordService,
        ItemMovementTransformer $itemMovementTransformer,
        InventoryDetailTransformer $inventoryDetailTransformer
    ) {
        parent::__construct($inventoryDetailRecordService, $inventoryDetailTransformer);
        $this->service = $inventoryDetailRecordService;
        $this->locationRecordService = $locationRecordService;
        $this->itemMovementRecordService = $itemMovementRecordService;
        $this->itemMovementTransformer = $itemMovementTransformer;
    }

    public function getTransactions($id)
    {
        $product = $this->service->getById($id);

        $inventoryFilter = new Filter('inventory_id', $id);
        // TODO fix other hardcoded like warehouses
        $locationFilter = new Filter('location_id', $this->locationRecordService->getByName('Warehouses')->getId());
        $dateSorter = new Sorter('date');
        $transactions = $this->itemMovementRecordService->getPaginated(self::INDEX_LIMIT, [$inventoryFilter, $locationFilter], [$dateSorter]);
        // Transaction count before the current page
        $prevTransactionsCount = self::INDEX_LIMIT * ($transactions->currentPage() - 1);

        $pageStartingBalanceValue = 0;

        if ($prevTransactionsCount > 0) {
            // $pageStartingBalanceValue = $this->itemMovementRecordService->computeBalance($product, null, $prevTransactionsCount);
            $prevTransactions = $this->itemMovementRecordService->getAll([$inventoryFilter, $locationFilter], [$dateSorter], [], $prevTransactionsCount);
            $pageStartingBalanceValue = $prevTransactions->sum('in') - $prevTransactions->sum('out');
        }
        return $this->response->paginator($transactions, $this->itemMovementTransformer)
            ->setMeta([
                'totals' => [
                    'startingBalance' => [
                        'amount' => $pageStartingBalanceValue,
                    ]
                ],
            ])
            ->setStatusCode(200);
    }
}
