import { Form } from "@c1_common_js/components/Form";

export const inventoryDetailService = new Vue({
    data: function() {
        return {
            form: new Form({}),
            uri: {
                inventoryDetails: '/api/inventory-details?include=inventory.product&quantity>0&condition=Good',
                inventoryDetails2: '/api/inventory-details',
                coilInventoryDetails: '/api/inventory-details?include=inventory.product&quantity>0&uom=MT&condition=Good'
            }
        };
    },
    methods: {
        getInventoryDetails() {
            return this.form.get(this.uri.inventoryDetails + '&limit=10000');
        },
        getCoilInventoryDetails() {
            return this.form.get(this.uri.coilInventoryDetails + '&limit=10000'); 
        },
        getInventoryDetailsWithCoils() {
            return this.form.get(this.uri.inventoryDetails2 + '?quantity>0&include=inventory.product&limit=10000');
        },
        getInventoryDetailsWhereProductsAreAccessory() {
            return this.form.get(this.uri.inventoryDetails2 + '?accessory-products-only=1&quantity>0&include=inventory.product&limit=10000');
        },
    }
});
