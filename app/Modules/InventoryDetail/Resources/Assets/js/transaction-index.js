import Index from "@c1_common_js/components/Index";
import { Form } from '@c1_common_js/components/Form';
import VForm from '@c1_common_js/components/VForm';
import Select3 from "@c1_common_js/components/Select3";
import SaveButton from '@c1_common_js/components/SaveButton';

const MT = 'MT';
const PIECE = 'PIECE';
new Vue({
    el: "#index",
    components: { Index, VForm, Select3, SaveButton },
    data: {
        filterable: filterable,
        baseUrl: baseUrl,
        items: [],
        totals: {},
        isLoading: true,
        toLastPage: typeof toLastPage !== "undefined" ? toLastPage : false,
        sorter: sorter,
        sortAscending: typeof sortAscending !== "undefined" ? sortAscending : true,
        isSaving: false,

        form: new Form({
            remarks: null,
        }),
        moneyConfig: {
            precision: 3
        },
    },
    methods: {

        setSorter(sorter) {
            if (sorter == this.sorter) this.sortAscending = !this.sortAscending;
            else this.sortAscending = true;
            this.sorter = sorter;
        },

        getSortIcon(column) {
            return {
                "fas fa-sort-up": column == this.sorter && this.sortAscending,
                "fas fa-sort-down": column == this.sorter && !this.sortAscending,
                "fas fa-sort": column != this.sorter
            };
        },

        adjust() {
            let url = '/inventory/stock/';
            if (this.form.type == MT) {
                url += 'coil'
            } else {
                url += 'accessory'
            }

            this.form.adjustedQuantity = this.form.adjustedQuantity;
            this.form.post('/api/inventories/' + this.form.id + '/adjust')
                .then(response => {
                    this.$swal({
                        title: 'Success',
                        text: 'Inventory balance was adjusted.',
                        type: 'success'
                    }).then(() => window.location = '/inventory-details/' + this.form.id + '/transactions');

                })
        },

        loadData(data) {
            this.form = new Form(data);
        },

    },

    created() {
        if (id != null) {
            this.form.get('/api/inventory-details/' + id + '?include=receiving')
                .then(response => {
                    this.loadData(response.data);
                    this.dataInitialized = true;
                });
        }
    }
})
