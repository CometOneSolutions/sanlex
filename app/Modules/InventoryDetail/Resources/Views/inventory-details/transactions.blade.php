@extends('app')
@section('breadcrumbs', Breadcrumbs::render('inventory-detail.transactions', $inventoryDetail))
@section('content')
    <div id="index" v-cloak>
        <div class="card">
            <div class="card-header">
                {{ $inventoryDetail->getInventory()->getProduct()->getName() }} @if ($inventoryDetail->getReceiving())
                    ({{ $inventoryDetail->getReceiving()->getCoilNumber() }})
                @endif

                @if (auth()->user()->can('Adjust [INV] Inventory'))
                    <button type="button" class="btn btn-outline-primary btn-sm float-right" data-toggle="modal"
                        :data-target="form.anchor"><i class="fas fa-pencil-alt"></i></button>
                @endif

            </div>
            <div class="card-body">

                <index :base-url="baseUrl" :sorter="sorter" :to-last-page="true" :sort-ascending="sortAscending"
                    v-on:update-totals="(val) => totals = val" v-on:update-items="(val) => items = val"
                    v-on:update-loading="(val) => isLoading = val">

                    <div v-show="false">
                        @{{ runningBalance = totals.startingBalance ? totals.startingBalance.amount : 0.000 }}
                    </div>

                    <table class="table table-responsive-sm">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Ref No</th>
                                <th class="text-right">IN</th>
                                <th class="text-right">OUT</th>
                                <th class="text-right">Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item in items" v-if="!isLoading">
                                <td> @{{ item.date }}</td>
                                {{-- <td>
                                @{{ item.isDeliveryReceipt ? item.approvedDate : item.date }}
                                <span v-if="item.isDeliveryReceipt" class="badge badge-primary">Approved Date</span>
                            </td> --}}
                                <td>
                                    <a :href="item.inventoriable.data.showUri">
                                        @{{ item.inventoriable.data.refNo }}
                                    </a>
                                </td>
                                <td class="text-right"><label v-if="item.isIn">@{{ item.in }}</label></td>
                                <td class="text-right"><label v-if="item.isOut">@{{ item.out }}</label></td>
                                <td class="text-right">@{{ runningBalance += (item.in - item.out) | numeric(3) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </index>
            </div>

            <div class="modal fade" :id="form.target" role="dialog" aria-labelledby="downpaymentLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleDownpaymentlLabel">Edit</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <v-form @validate="adjust()">
                            <div class="modal-body">
                                <div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger"
                                    role="alert">
                                    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
                                        @{{ error[0] }}
                                    </small>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">From</span>
                                            </div>
                                            <money class="form-control text-right" v-model.number="form.quantity" disabled
                                                v-bind="moneyConfig"></money>
                                        </div>
                                    </div>

                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">To</span>
                                            </div>
                                            <money class="form-control text-right" v-model.number="form.adjustedQuantity"
                                                v-bind="moneyConfig"></money>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Remarks</span>
                                                <input type="text" class="form-control" placeholder=""
                                                    v-model="form.remarks">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving">
                                </save-button>
                            </div>
                        </v-form>
                    </div>
                </div>
            </div>


        </div>
    </div>

@stop
@push('scripts')
    <script src="{{ mix('js/transaction-index.js') }}"></script>
@endpush
