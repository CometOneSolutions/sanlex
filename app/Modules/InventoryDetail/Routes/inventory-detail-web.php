<?php

Route::group(['prefix' => 'inventory-details'], function () {
    Route::get('{inventoryDetailId}/transactions', 'InventoryDetailController@transactions')->name('inventory-detail_transactions');
});
