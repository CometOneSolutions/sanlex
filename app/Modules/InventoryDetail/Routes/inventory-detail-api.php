<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'inventory-details'], function () use ($api) {
        $api->get('/', 'App\Modules\InventoryDetail\Controllers\InventoryDetailApiController@index');
        // $api->get('/list', 'App\Http\Controllers\InventoryApiController@index');
        $api->get('{inventoryDetailId}', 'App\Modules\InventoryDetail\Controllers\InventoryDetailApiController@show');
        $api->get('{inventoryDetailId}/transactions', 'App\Modules\InventoryDetail\Controllers\InventoryDetailApiController@getTransactions');
        // $api->post('/', 'App\Modules\InventoryDetail\Controllers\InventoryApiController@store');
        // $api->patch('{inventoryId}', 'App\Modules\InventoryDetail\Controllers\InventoryApiController@update');
        // $api->delete('{inventoryId}', 'App\Modules\InventoryDetail\Controllers\InventoryApiController@destroy');
    });
});
