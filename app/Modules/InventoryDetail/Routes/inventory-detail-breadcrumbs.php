<?php

Breadcrumbs::register('inventory-detail.transactions', function ($breadcrumbs, $inventoryDetail) {
    if ($inventoryDetail->isCoil()) {
        $breadcrumbs->parent('inventory.coil.index');
    } else {
        $breadcrumbs->parent('inventory.accessory.index');
    }
    $breadcrumbs->push($inventoryDetail->getInventory()->getProduct()->getName());
    $breadcrumbs->push('Transactions');
});
