<?php

namespace App\Modules\DimensionX\Provider;

use App\Modules\DimensionX\Models\DimensionX;
use App\Modules\DimensionX\Models\DimensionXModel;
use App\Modules\DimensionX\Repositories\DimensionXRepository;
use App\Modules\DimensionX\Repositories\EloquentDimensionXRepository;
use App\Modules\DimensionX\Services\DimensionXRecordService;
use App\Modules\DimensionX\Services\DimensionXRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class DimensionXServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/DimensionX/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(DimensionXRecordService::class, DimensionXRecordServiceImpl::class);
		$this->app->bind(DimensionXRepository::class, EloquentDimensionXRepository::class);
		$this->app->bind(DimensionX::class, DimensionXModel::class);
	}
}
