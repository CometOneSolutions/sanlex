<?php

namespace App\Modules\DimensionX\Models;


interface DimensionX
{
	public function getId();

	public function getName();

	public function setName($value);
}
