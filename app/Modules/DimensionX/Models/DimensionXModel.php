<?php

namespace App\Modules\DimensionX\Models;

use App\Modules\StructuralSteel\Models\StructuralSteelModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;

class DimensionXModel extends Model implements DimensionX, UpdatableByUser
{
	use HasUpdatedByUser;

	protected $table = 'dimension_x';

	public function structuralSteels()
	{
		return $this->hasMany(StructuralSteelModel::class, 'dimension_x_id');
	}

	public function getStructuralSteels()
	{
		return $this->structuralSteels;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($value)
	{
		$this->name = $value;
		return $this;
	}
}
