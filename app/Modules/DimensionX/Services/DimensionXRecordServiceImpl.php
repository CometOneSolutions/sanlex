<?php

namespace App\Modules\DimensionX\Services;

use App\Modules\DimensionX\Models\DimensionX;
use App\Modules\DimensionX\Repositories\DimensionXRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class DimensionXRecordServiceImpl extends RecordServiceImpl implements DimensionXRecordService
{
	use CanUpdateProductName;

	private $dimensionX;
	private $dimensionXs;

	protected $availableFilters = [
		["id" => "name", "text" => "Name"]
	];

	public function __construct(DimensionXRepository $dimensionXs, DimensionX $dimensionX)
	{
		$this->dimensionXs = $dimensionXs;
		$this->dimensionX = $dimensionX;
		parent::__construct($dimensionXs);
	}

	public function create(
		$name,
		User $user = null
	) {
		$dimensionX = new $this->dimensionX;
		$dimensionX->setName($name);

		if ($user) {
			$dimensionX->setUpdatedByUser($user);
		}
		$this->dimensionXs->save($dimensionX);
		return $dimensionX;
	}

	public function update(
		DimensionX $dimensionX,
		$name,
		User $user = null
	) {
		$tempDimensionX = clone $dimensionX;
		$tempDimensionX->setName($name);

		if ($user) {
			$tempDimensionX->setUpdatedByUser($user);
		}
		$this->dimensionXs->save($tempDimensionX);

		$this->updateProductableProductNames([
			$tempDimensionX->getStructuralSteels()
		]);

		return $tempDimensionX;
	}

	public function delete(DimensionX $dimensionX)
	{
		$this->dimensionXs->delete($dimensionX);
		return $dimensionX;
	}
}
