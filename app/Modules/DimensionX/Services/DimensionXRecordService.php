<?php

namespace App\Modules\DimensionX\Services;

use App\Modules\DimensionX\Models\DimensionX;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface DimensionXRecordService extends RecordService
{
	public function create(
		$name,
		User $user = null
	);

	public function update(
		DimensionX $dimensionX,
		$name,
		User $user = null
	);

	public function delete(DimensionX $dimensionX);
}
