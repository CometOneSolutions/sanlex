<?php

Breadcrumbs::register('dimension-x.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Dimension X', route('dimension-x_index'));
});
Breadcrumbs::register('dimension-x.create', function ($breadcrumbs) {
	$breadcrumbs->parent('dimension-x.index');
	$breadcrumbs->push('Create', route('dimension-x_create'));
});
Breadcrumbs::register('dimension-x.show', function ($breadcrumbs, $dimensionX) {
	$breadcrumbs->parent('dimension-x.index');
	$breadcrumbs->push($dimensionX->getName(), route('dimension-x_show', $dimensionX->getId()));
});
Breadcrumbs::register('dimension-x.edit', function ($breadcrumbs, $dimensionX) {
	$breadcrumbs->parent('dimension-x.show', $dimensionX);
	$breadcrumbs->push('Edit', route('dimension-x_edit', $dimensionX->getId()));
});
