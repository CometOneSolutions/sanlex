<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'dimension-x'], function () use ($api) {
		$api->get('/', 'App\Modules\DimensionX\Controllers\DimensionXApiController@index');
		$api->get('{dimensionXId}', 'App\Modules\DimensionX\Controllers\DimensionXApiController@show');
		$api->post('/', 'App\Modules\DimensionX\Controllers\DimensionXApiController@store');
		$api->patch('{dimensionXId}', 'App\Modules\DimensionX\Controllers\DimensionXApiController@update');
		$api->delete('{dimensionXId}', 'App\Modules\DimensionX\Controllers\DimensionXApiController@destroy');
	});
});
