<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'dimension-x'], function () {
		Route::get('/', 'DimensionXController@index')->name('dimension-x_index');
		Route::get('/create', 'DimensionXController@create')->name('dimension-x_create');
		Route::get('{dimensionXId}/edit', 'DimensionXController@edit')->name('dimension-x_edit');
		Route::get('{dimensionXId}/print', 'DimensionXController@print')->name('dimension-x_print');
		Route::get('{dimensionXId}', 'DimensionXController@show')->name('dimension-x_show');
	});
});
