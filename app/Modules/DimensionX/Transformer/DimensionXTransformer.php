<?php

namespace App\Modules\DimensionX\Transformer;

use App\Modules\DimensionX\Models\DimensionX;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class DimensionXTransformer extends UpdatableByUserTransformer
{
	public function transform(DimensionX $dimensionX)
	{
		return [
			'id' => (int) $dimensionX->getId(),
			'text' => $dimensionX->getName(),
			'name' => $dimensionX->getName(),
			'updatedAt' => $dimensionX->updated_at,
			'editUri' => route('dimension-x_edit', $dimensionX->getId()),
			'showUri' => route('dimension-x_show', $dimensionX->getId()),
		];
	}
}
