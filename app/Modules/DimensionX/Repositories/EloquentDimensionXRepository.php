<?php

namespace App\Modules\DimensionX\Repositories;

use App\Modules\DimensionX\Models\DimensionX;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentDimensionXRepository extends EloquentRepository implements DimensionXRepository
{
	public function __construct(DimensionX $dimensionX)
	{
		parent::__construct($dimensionX);
	}

	public function save(DimensionX $dimensionX)
	{
		return $dimensionX->save();
	}

	public function delete(DimensionX $dimensionX)
	{
		return $dimensionX->delete();
	}
}
