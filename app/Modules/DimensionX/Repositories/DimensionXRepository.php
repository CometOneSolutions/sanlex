<?php

namespace App\Modules\DimensionX\Repositories;

use App\Modules\DimensionX\Models\DimensionX;
use CometOneSolutions\Common\Repositories\Repository;

interface DimensionXRepository extends Repository
{
	public function save(DimensionX $width);
	public function delete(DimensionX $width);
}
