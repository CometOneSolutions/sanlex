<?php

namespace App\Modules\DimensionX\Requests;

use Dingo\Api\Http\FormRequest;

class StoreDimensionX extends DimensionXRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [MAS] Dimension X');
	}
}
