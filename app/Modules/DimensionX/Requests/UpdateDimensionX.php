<?php

namespace App\Modules\DimensionX\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateDimensionX extends DimensionXRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] Dimension X');
	}
}
