<?php

namespace App\Modules\DimensionX\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyDimensionX extends DimensionXRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Dimension X');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
