@extends('app')
@section('breadcrumbs', Breadcrumbs::render('dimension-x.index'))
@section('content')   
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Dimension X
            @if(auth()->user()->can('Create [MAS] Dimension X'))
                <div class="float-right">
                    <a href="{{route('dimension-x_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
                </div>
            @endif
        </div>
        <div class="card-body">
            <index 
                :filterable="filterable"
                :base-url="baseUrl"
                :sort-ascending="sortAscending"
                :sorter="sorter"
                v-on:update-loading="(val) => isLoading = val"
                v-on:update-items="(val) => items = val">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <a v-on:click="setSorter('name')">
                                Name <i class="fa" :class="getSortIcon('name')"></i>
                            </a>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="item in items" v-if="!isLoading">
                        <td><a :href="item.showUri">@{{ item.name }}</a></td>
                    </tr>
                    </tbody>
                </table> 
            </index>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush