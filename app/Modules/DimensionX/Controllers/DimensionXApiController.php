<?php

namespace App\Modules\DimensionX\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\DimensionX\Services\DimensionXRecordService;
use App\Modules\DimensionX\Transformer\DimensionXTransformer;
use App\Modules\DimensionX\Requests\StoreDimensionX;
use App\Modules\DimensionX\Requests\UpdateDimensionX;
use App\Modules\DimensionX\Requests\DestroyDimensionX;

class DimensionXApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;

	public function __construct(
		DimensionXRecordService $dimensionXRecordService,
		DimensionXTransformer $transformer
	) {
		$this->middleware('auth:api');
		parent::__construct($dimensionXRecordService, $transformer);
		$this->service = $dimensionXRecordService;
		$this->transformer = $transformer;
	}

	public function store(StoreDimensionX $request)
	{
		$dimensionX = \DB::transaction(function () use ($request) {
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->create(
				$name,

				$user
			);
		});
		return $this->response->item($dimensionX, $this->transformer)->setStatusCode(201);
	}

	public function update($dimensionXId, UpdateDimensionX $request)
	{
		$dimensionX = \DB::transaction(function () use ($dimensionXId, $request) {
			$dimensionX   = $this->service->getById($dimensionXId);
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->update(
				$dimensionX,
				$name,

				$user
			);
		});
		return $this->response->item($dimensionX, $this->transformer)->setStatusCode(200);
	}

	public function destroy($dimensionXId, DestroyDimensionX $request)
	{
		$dimensionX = $this->service->getById($dimensionXId);
		$this->service->delete($dimensionX);
		return $this->response->item($dimensionX, $this->transformer)->setStatusCode(200);
	}
}
