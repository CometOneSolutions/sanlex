<?php

namespace App\Modules\DimensionX\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\DimensionX\Services\DimensionXRecordService;


class DimensionXController extends Controller
{
	private $service;

	public function __construct(DimensionXRecordService $dimensionXRecordService)
	{
		$this->service = $dimensionXRecordService;
		$this->middleware('permission:Read [MAS] Dimension X')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Dimension X')->only('create');
		$this->middleware('permission:Update [MAS] Dimension X')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'name',
			'sortAscending' => true,
			'baseUrl' => '/api/dimension-x'
		]);
		return view('dimension-x.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('dimension-x.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$dimensionX = $this->service->getById($id);
		return view('dimension-x.show', compact('dimensionX'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$dimensionX = $this->service->getById($id);
		return view('dimension-x.edit', compact('dimensionX'));
	}
}
