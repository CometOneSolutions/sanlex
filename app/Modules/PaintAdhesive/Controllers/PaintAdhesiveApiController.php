<?php

namespace App\Modules\PaintAdhesive\Controllers;

use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\PaintAdhesive\Requests\DestroyPaintAdhesive;
use App\Modules\PaintAdhesive\Services\PaintAdhesiveRecordService;
use App\Modules\PaintAdhesive\Transformer\PaintAdhesiveTransformer;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\PaintAdhesive\Requests\StorePaintAdhesive;
use App\Modules\PaintAdhesive\Requests\UpdatePaintAdhesive;

class PaintAdhesiveApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;
    protected $productRecordService;

    public function __construct(
        PaintAdhesiveRecordService $paintAdhesiveRecordService,
        PaintAdhesiveTransformer $transformer,
        ProductRecordService $productRecordService
    ) {
        $this->middleware('auth:api');
        parent::__construct($paintAdhesiveRecordService, $transformer);
        $this->service = $paintAdhesiveRecordService;
        $this->transformer = $transformer;
        $this->productRecordService = $productRecordService;
    }

    public function store(StorePaintAdhesive $request)
    {
        $paintAdhesive = \DB::transaction(function () use ($request) {
            $name = $request->getName();

            return $this->service->create(
                $name,
                $request->getSupplier(),
                $request->getPaintAdhesiveType(),
                $request->getBrand(),
                $request->getColor(),
                $request->getSize(),
                $request->user()
            );
        });
        return $this->response->item($paintAdhesive, $this->transformer)->setStatusCode(201);
    }

    public function update($paintAdhesiveId, UpdatePaintAdhesive $request)
    {
        $paintAdhesive = \DB::transaction(function () use ($request) {
            $name = $request->getName();

            return $this->service->update(
                $request->getPaintAdhesive(),
                $name,
                $request->getSupplier(),
                $request->getPaintAdhesiveType(),
                $request->getBrand(),
                $request->getColor(),
                $request->getSize(),
                $request->user()
            );
        });
        return $this->response->item($paintAdhesive, $this->transformer)->setStatusCode(200);
    }

    public function destroy($paintAdhesiveId, DestroyPaintAdhesive $request)
    {
        $paintAdhesive = $this->service->getById($paintAdhesiveId);
        $this->service->delete($paintAdhesive);
        return $this->response->item($paintAdhesive, $this->transformer)->setStatusCode(200);
    }
}
