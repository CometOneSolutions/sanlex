<?php

namespace App\Modules\PaintAdhesive\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\PaintAdhesive\Services\PaintAdhesiveRecordService;

class PaintAdhesiveController extends Controller
{
    private $service;

    public function __construct(PaintAdhesiveRecordService $paintAdhesiveRecordService)
    {
        $this->service = $paintAdhesiveRecordService;
        $this->middleware('permission:Read [MAS] Paint Adhesive')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Paint Adhesive')->only('create');
        $this->middleware('permission:Update [MAS] Paint Adhesive')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/paint-adhesives?include=product,paintAdhesiveType,color,size,brand'
        ]);
        return view('paint-adhesives.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('paint-adhesives.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $paintAdhesive = $this->service->getById($id);
        return view('paint-adhesives.show', compact('paintAdhesive'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $paintAdhesive = $this->service->getById($id);
        return view('paint-adhesives.edit', compact('paintAdhesive'));
    }
}
