<?php

namespace App\Modules\PaintAdhesive\Repositories;

use App\Modules\PaintAdhesive\Models\PaintAdhesive;
use CometOneSolutions\Common\Repositories\Repository;

interface PaintAdhesiveRepository extends Repository
{
	public function save(PaintAdhesive $paintAdhesive);
	public function delete(PaintAdhesive $paintAdhesive);
}
