<?php

namespace App\Modules\PaintAdhesive\Repositories;

use App\Modules\PaintAdhesive\Models\PaintAdhesive;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentPaintAdhesiveRepository extends EloquentRepository implements PaintAdhesiveRepository
{
	public function __construct(PaintAdhesive $paintAdhesive)
	{
		parent::__construct($paintAdhesive);
	}

	public function save(PaintAdhesive $paintAdhesive)
	{
		return $paintAdhesive->save();
	}

	public function delete(PaintAdhesive $paintAdhesive)
	{
		$product = $paintAdhesive->getProduct();
		$product->delete();
		return $paintAdhesive->delete();
	}
	public function filterByName($productName)
	{
		return $this->model->whereHas('product', function ($product) use ($productName) {
			return $product->where('name', 'like', '%' . $productName . '%');
		});
	}
	public function filterBySupplierName($supplierName)
	{
		return $this->model->whereHas('product', function ($product) use ($supplierName) {
			return $product->whereHas('supplier', function ($supplier) use ($supplierName) {
				return $supplier->where('name', 'like', '%' . $supplierName . '%');
			});
		});
	}
	public function inStock()
	{
		$this->model = $this->model->inStock();
		return $this;
	}
}
