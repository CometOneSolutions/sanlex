<?php

namespace App\Modules\PaintAdhesive\Provider;

use App\Modules\PaintAdhesive\Models\PaintAdhesive;
use App\Modules\PaintAdhesive\Models\PaintAdhesiveModel;
use App\Modules\PaintAdhesive\Repositories\EloquentPaintAdhesiveRepository;
use App\Modules\PaintAdhesive\Repositories\PaintAdhesiveRepository;
use App\Modules\PaintAdhesive\Services\PaintAdhesiveRecordService;
use App\Modules\PaintAdhesive\Services\PaintAdhesiveRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class PaintAdhesiveServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/PaintAdhesive/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(PaintAdhesiveRecordService::class, PaintAdhesiveRecordServiceImpl::class);
		$this->app->bind(PaintAdhesiveRepository::class, EloquentPaintAdhesiveRepository::class);
		$this->app->bind(PaintAdhesive::class, PaintAdhesiveModel::class);
	}
}
