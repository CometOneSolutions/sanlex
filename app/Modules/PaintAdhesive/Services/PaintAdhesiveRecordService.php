<?php

namespace App\Modules\PaintAdhesive\Services;

use App\Modules\Brand\Models\Brand;
use App\Modules\Color\Models\Color;
use App\Modules\PaintAdhesive\Models\PaintAdhesive;
use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use App\Modules\Size\Models\Size;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordService;

interface PaintAdhesiveRecordService extends RecordService
{
    public function create(
        $name,
        Supplier $supplier,
        PaintAdhesiveType $paintAdhesiveType,
        Brand $brand,
        Color $color,
        Size $size,
        User $user = null
    );

    public function update(
        PaintAdhesive $paintAdhesive,
        $name,
        Supplier $supplier,
        PaintAdhesiveType $paintAdhesiveType,
        Brand $brand,
        Color $color,
        Size $size,
        User $user = null
    );

    public function delete(PaintAdhesive $paintAdhesive);
}
