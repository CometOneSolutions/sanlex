<?php

namespace App\Modules\PaintAdhesive\Services;

use App\Modules\Brand\Models\Brand;
use App\Modules\Color\Models\Color;
use App\Modules\PaintAdhesive\Models\PaintAdhesive;
use App\Modules\PaintAdhesive\Repositories\PaintAdhesiveRepository;
use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Size\Models\Size;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class PaintAdhesiveRecordServiceImpl extends RecordServiceImpl implements PaintAdhesiveRecordService
{
    private $paintAdhesive;
    private $paintAdhesives;
    private $productRecordService;

    protected $availableFilters = [
        ['id' => 'name', 'text' => 'Name'],
    ];

    public function __construct(PaintAdhesiveRepository $paintAdhesives, PaintAdhesive $paintAdhesive, ProductRecordService $productRecordService)
    {
        $this->paintAdhesives = $paintAdhesives;
        $this->paintAdhesive = $paintAdhesive;
        $this->productRecordService = $productRecordService;
        parent::__construct($paintAdhesives);
    }

    public function create(
        $name,
        Supplier $supplier,
        PaintAdhesiveType $paintAdhesiveType,
        Brand $brand,
        Color $color,
        Size $size,
        User $user = null
    ) {
        $paintAdhesive = new $this->paintAdhesive;
        $paintAdhesive->setColor($color);
        $paintAdhesive->setSize($size);
        $paintAdhesive->setBrand($brand);
        $paintAdhesive->setPaintAdhesiveType($paintAdhesiveType);

        if ($user) {
            $paintAdhesive->setUpdatedByUser($user);
        }
        $this->paintAdhesives->save($paintAdhesive);
        $this->productRecordService->create($name, $supplier, $paintAdhesive, $user);
        return $paintAdhesive;
    }

    public function update(
        PaintAdhesive $paintAdhesive,
        $name,
        Supplier $supplier,
        PaintAdhesiveType $paintAdhesiveType,
        Brand $brand,
        Color $color,
        Size $size,
        User $user = null
    ) {
        $tempPaintAdhesive = clone $paintAdhesive;
        $product = $tempPaintAdhesive->getProduct();
        $tempPaintAdhesive->setColor($color);
        $tempPaintAdhesive->setBrand($brand);
        $tempPaintAdhesive->setSize($size);
        $tempPaintAdhesive->setPaintAdhesiveType($paintAdhesiveType);
        if ($user) {
            $tempPaintAdhesive->setUpdatedByUser($user);
        }
        $this->paintAdhesives->save($tempPaintAdhesive);
        $this->productRecordService->update($product, $name, $supplier, $tempPaintAdhesive, $user);
        return $tempPaintAdhesive;
    }

    public function delete(PaintAdhesive $paintAdhesive)
    {
        $this->paintAdhesives->delete($paintAdhesive);
        return $paintAdhesive;
    }

    public function inStock()
    {
        $this->paintAdhesives = $this->paintAdhesives->inStock();
        return $this;
    }
}
