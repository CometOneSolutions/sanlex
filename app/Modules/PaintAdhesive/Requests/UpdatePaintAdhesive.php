<?php

namespace App\Modules\PaintAdhesive\Requests;

use Dingo\Api\Http\FormRequest;

class UpdatePaintAdhesive extends PaintAdhesiveRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] Paint Adhesive');
	}
}
