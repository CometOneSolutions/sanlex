<?php

namespace App\Modules\PaintAdhesive\Requests;

class StorePaintAdhesive extends PaintAdhesiveRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Paint Adhesive');
    }
}
