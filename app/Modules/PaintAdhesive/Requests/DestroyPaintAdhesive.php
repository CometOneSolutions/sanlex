<?php

namespace App\Modules\PaintAdhesive\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyPaintAdhesive extends PaintAdhesiveRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Paint Adhesive');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
