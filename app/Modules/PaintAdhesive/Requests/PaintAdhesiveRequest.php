<?php

namespace App\Modules\PaintAdhesive\Requests;

use App\Modules\Brand\Services\BrandRecordService;
use App\Modules\Color\Services\ColorRecordService;
use App\Modules\PaintAdhesive\Services\PaintAdhesiveRecordService;
use App\Modules\PaintAdhesiveType\Services\PaintAdhesiveTypeRecordService;
use App\Modules\Size\Services\SizeRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use Dingo\Api\Http\FormRequest;

class PaintAdhesiveRequest extends FormRequest
{
    private $service;
    private $supplierRecordService;
    private $colorRecordService;
    private $sizeRecordService;
    private $paintAdhesiveTypeRecordService;
    private $brandRecordService;

    public function __construct(
        SupplierRecordService $supplierRecordService,
        PaintAdhesiveRecordService $paintAdhesiveRecordService,
        ColorRecordService $colorRecordService,
        SizeRecordService $sizeRecordService,
        PaintAdhesiveTypeRecordService $paintAdhesiveTypeRecordService,
        BrandRecordService $brandRecordService
    ) {
        $this->supplierRecordService = $supplierRecordService;
        $this->service = $paintAdhesiveRecordService;
        $this->colorRecordService = $colorRecordService;
        $this->sizeRecordService = $sizeRecordService;
        $this->paintAdhesiveTypeRecordService = $paintAdhesiveTypeRecordService;
        $this->brandRecordService = $brandRecordService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $paintAdhesive = $this->route('paintAdhesiveId') ? $this->service->getById($this->route('paintAdhesiveId')) : null;

        if ($paintAdhesive) {
            return [
                'name' => 'required|unique:products,name,'.$paintAdhesive->getProduct()->getId(),
                'supplierId' => 'required',
                'colorId' => 'required',
                'sizeId' => 'required',
                'paintAdhesiveTypeId' => 'required',
                'brandId' => 'required',
            ];
        } else {
            return [
                'name' => 'required|unique:products,name',
                'supplierId' => 'required',
                'colorId' => 'required',
                'sizeId' => 'required',
                'paintAdhesiveTypeId' => 'required',
                'paintAdhesiveTypeId' => 'required',
                'brandId' => 'required',
            ];
        }
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required.',
            'name.unique' => 'Name already exists.',
            'supplierId.required' => 'Supplier is required.',
            'colorId.required' => 'Color is required.',
            'sizeId.required' => 'Size is required.',
            'paintAdhesiveTypeId.required' => 'Paint adhesive type is required.',
            'brandId.required' => 'Brand is required.'
        ];
    }

    public function getName($index = 'name')
    {
        return $this->input($index);
    }

    public function getSupplier($index = 'supplierId')
    {
        return $this->supplierRecordService->getById($this->input($index));
    }

    public function getPaintAdhesive($index = 'id')
    {
        return $this->service->getById($this->input($index));
    }

    public function getColor($index = 'colorId')
    {
        return $this->colorRecordService->getById($this->input($index));
    }

    public function getBrand($index = 'brandId')
    {
        return $this->brandRecordService->getById($this->input($index));
    }

    public function getSize($index = 'sizeId')
    {
        return $this->sizeRecordService->getById($this->input($index));
    }

    public function getPaintAdhesiveType($index = 'paintAdhesiveTypeId')
    {
        return $this->paintAdhesiveTypeRecordService->getById($this->input($index));
    }
}
