<?php

namespace App\Modules\PaintAdhesive\Models;



interface PaintAdhesive
{
	public function getId();

	public function getDescription();

	public function setDescription($value);
}
