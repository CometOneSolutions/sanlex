<?php

namespace App\Modules\PaintAdhesive\Models;

use App\Modules\Brand\Models\Brand;
use App\Modules\Brand\Models\BrandModel;
use App\Modules\Color\Models\Color;
use App\Modules\Color\Models\ColorModel;
use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveTypeModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Product\Traits\Productable;
use App\Modules\Size\Models\Size;
use App\Modules\Size\Models\SizeModel;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use Illuminate\Database\Eloquent\Model;

class PaintAdhesiveModel extends Model implements PaintAdhesive, UpdatableByUser, Productable
{
    use HasUpdatedByUser;

    protected $table = 'paint_adhesives';

    public function product()
    {
        return $this->morphOne(ProductModel::class, 'productable');
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

    public function paintAdhesiveType()
    {
        return $this->belongsTo(PaintAdhesiveTypeModel::class, 'paint_adhesive_type_id');
    }

    public function getPaintAdhesiveTypeId()
    {
        return (int) $this->paint_adhesive_type_id;
    }

    public function getPaintAdhesiveType()
    {
        return $this->paintAdhesiveType;
    }

    public function setPaintAdhesiveType(PaintAdhesiveType $paintAdhesiveType)
    {
        $this->paintAdhesiveType()->associate($paintAdhesiveType);
        return $this;
    }

    public function color()
    {
        return $this->belongsTo(ColorModel::class, 'color_id');
    }

    public function brand()
    {
        return $this->belongsTo(BrandModel::class, 'brand_id');
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand(Brand $brand)
    {
        $this->brand()->associate($brand);
        return $this;
    }

    public function getBrandId()
    {
        return (int) $this->brand_id;
    }

    public function getColorId()
    {
        return (int) $this->color_id;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor(Color $color)
    {
        $this->color()->associate($color);
        return $this;
    }

    public function size()
    {
        return $this->belongsTo(SizeModel::class, 'size_id');
    }

    public function getSizeId()
    {
        return (int) $this->size_id;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize(Size $size)
    {
        $this->size()->associate($size);
        return $this;
    }

    public function isCoil()
    {
        return false;
    }

    public function scopeInStock($query)
    {
        return $query->whereHas('product', function ($product) {
            return $product->whereHas('inventory', function ($inventory) {
                return $inventory->where('stock', '>', 0);
            });
        });
    }

    public function getAssembledProductName()
    {
        $nameAttributes = [
            $this->getProduct()->getSupplier()->getShortCode(),
            $this->getPaintAdhesiveType()->getName(),
            $this->getColor()->getName(),
            $this->getSize()->getName(),
            $this->getBrand()->getName()
        ];

        return  strtoupper(implode("~", $nameAttributes));
    }

    public function scopeSize($query, Size $size)
    {
        return $query->whereSizeId($size->getId());
    }

    public function scopeColor($query, Color $color)
    {
        return $query->whereColorId($color->getId());
    }

    public function scopeBrand($query, Brand $brand)
    {
        return $query->whereBrandId($brand->getId());
    }

    public function scopePaintAdhesiveType($query, PaintAdhesiveType $paintAdhesiveType)
    {
        return $query->wherePaintAdhesiveTypeId($paintAdhesiveType->getId());
    }
}
