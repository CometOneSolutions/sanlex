import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select3 from "@c1_common_js/components/Select3";


new Vue({
    el: '#paint-adhesive',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select3
    },
    data: {
        form: new Form({
            name: null,
            supplierId: null,
            shortCode: null,
            colorId: null,
            colorText: null,
            brandId: null,
            brandText: null,
            sizeId: null,
            sizeText: null,
            paintAdhesiveTypeId: null,
            paintAdhesiveTypeText: null,
        }),
        dataInitialized: true,
        supplierUrl: '/api/suppliers?sort=name',
        defaultSelectedSupplier: {},
        colorUrl: '/api/colors?sort=name',
        defaultSelectedColor: {},
        brandUrl: '/api/brands?sort=name',
        defaultSelectedBrand: {},
        sizeUrl: '/api/sizes?sort=name',
        defaultSelectedSize: {},
        paintAdhesiveTypeUrl: '/api/paint-adhesive-types?sort=name',
        defaultSelectedPaintAdhesiveType: {}

    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },
    computed: {
        // selectedSupplier() {
        //     if(this.form.supplierId == null)
        //     {
        //         return undefined;
        //     }
        //     return this.supplierSelections.find(supplier => supplier.id == this.form.supplierId);
        // },
        name() {
            if (this.form.supplierId === undefined || this.form.dimensionXId === null || this.form.dimensionYId === null || this.form.lengthId === null || this.form.structuralSteelTypeId === null || this.form.thicknessId === null || this.form.finishId === null) {
                return undefined;
            }
            return this.form.shortCode + '~' + this.form.paintAdhesiveTypeText.toUpperCase() + '~' + this.form.colorText.toUpperCase()
                + '~' + this.form.sizeText.toUpperCase() + '~' + this.form.brandText.toUpperCase();
        },
        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        setShortCode(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("shortCode")) {
                this.form.shortCode = selectedObjects[0].shortCode;
            }
        },
        setPaintAdhesiveType(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.paintAdhesiveTypeText = selectedObjects[0].name;
            }

        },
        setColor(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.colorText = selectedObjects[0].name;
            }

        },
        setBrand(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.brandText = selectedObjects[0].name;
            }

        },
        setSize(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.sizeText = selectedObjects[0].name;
            }

        },

        destroy() {
            this.form.deleteWithConfirmation('/api/paint-adhesives/' + this.form.id).then(response => {
                this.form.successModal('Paint adhesive was removed.').then(() =>
                    window.location = '/master-file/paint-adhesives/'
                );
            });
        },

        store() {
            this.form.name = this.name;
            this.form.postWithModal('/api/paint-adhesives', null, 'Paint adhesive was saved.');
        },

        update() {
            this.form.name = this.name;
            this.form.patch('/api/paint-adhesives/' + this.form.id).then(response => {
                this.form.successModal('Paint adhesive was updated.').then(() =>
                    window.location = '/master-file/paint-adhesives/' + this.form.id
                );
            })
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {


        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/paint-adhesives/' + id + '?include=product.supplier,paintAdhesiveType,color,size,brand')
                .then(response => {
                    this.loadData(response.data);

                    this.form.shortCode = response.data.product.data.supplier.data.shortCode;
                    this.form.paintAdhesiveTypeText = response.data.paintAdhesiveType.data.name;
                    this.form.colorText = response.data.color.data.name;
                    this.form.brandText = response.data.brand.data.name;
                    this.form.sizeText = response.data.size.data.name;


                    this.defaultSelectedSupplier = {
                        text: response.data.product.data.supplier.data.name,
                        id: response.data.product.data.supplier.data.id,
                    };
                    this.defaultSelectedPaintAdhesiveType = {
                        text: response.data.paintAdhesiveType.data.name,
                        id: response.data.paintAdhesiveType.data.id,
                    };
                    this.defaultSelectedColor = {
                        text: response.data.color.data.name,
                        id: response.data.color.data.id,
                    };
                    this.defaultSelectedBrand = {
                        text: response.data.brand.data.name,
                        id: response.data.brand.data.id,
                    };
                    this.defaultSelectedSize = {
                        text: response.data.size.data.name,
                        id: response.data.size.data.id,
                    };
                    this.dataInitialized = true;
                });
        }
    },
    mounted() {
        console.log("Init paint adhesive script...");
    }
});