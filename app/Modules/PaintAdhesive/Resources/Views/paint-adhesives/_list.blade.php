<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Name</label>
            <p>@{{form.product.data.name }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Supplier</label>
            <p>@{{form.product.data.supplier.data.name }}</p>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Paint Adhesive Type</label>
            <p>@{{form.paintAdhesiveType.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Brand</label>
            <p>@{{form.brand.data.name || "--" }}</p>
        </div>
    </div>
	<div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Color</label>
            <p>@{{form.color.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Size</label>
            <p>@{{form.size.data.name || "--" }}</p>
        </div>
    </div>
  
</div>
@push('scripts')
<script src="{{ mix('js/paint-adhesive.js') }}"></script>
@endpush