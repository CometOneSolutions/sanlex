<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Supplier <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter supplier" v-model="form.supplierId" :selected="defaultSelectedSupplier" :url="supplierUrl" search="name" placeholder="Please select" @change="setShortCode" id="supplier" name="supplier">
            </select3>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Paint Adhesive Type <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter paint adhesive type" v-model="form.paintAdhesiveTypeId" :selected="defaultSelectedPaintAdhesiveType" :url="paintAdhesiveTypeUrl" search="name" placeholder="Please select" @change="setPaintAdhesiveType" id="paintAdhesiveType" name="paintAdhesiveType">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Brand <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter brand" v-model="form.brandId" :selected="defaultSelectedBrand" :url="brandUrl" search="name" placeholder="Please select" @change="setBrand" id="brand" name="brand">
            </select3>
        </div>
    </div>
	<div class="col-12">
        <div class="form-group">
            <label for="supplier">Color <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter color" v-model="form.colorId" :selected="defaultSelectedColor" :url="colorUrl" search="name" placeholder="Please select" @change="setColor" id="color" name="color">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Size <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter size" v-model="form.sizeId" :selected="defaultSelectedSize" :url="sizeUrl" search="name" placeholder="Please select" @change="setSize" id="size" name="size">
            </select3>
        </div>
    </div>
    
</div>
@push('scripts')
<script src="{{ mix('js/paint-adhesive.js') }}"></script>
@endpush