@extends('app')
@section('breadcrumbs', Breadcrumbs::render('paint-adhesive.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Paint Adhesives
            @if(auth()->user()->can('Create [MAS] Paint Adhesive'))
            <div class="float-right">
                <a href="{{route('paint-adhesive_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
            </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sort-ascending="sortAscending" :sorter="sorter" v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('id')">
                                    Product <i class="fa" :class="getSortIcon('id')"></i>
                                </a>
                            </th>
                            <th>Paint Adhesive Type</th>
                            <th>Brand</th>
							<th>Color</th>
                            <th>Size</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{ item.product.data.name }}</a></td>
                            <td>@{{item.paintAdhesiveType.data.name}}</td>
                            <td>@{{item.brand.data.name}}</td>
							<td>@{{item.color.data.name}}</td>
                            <td>@{{item.size.data.name}}</td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush