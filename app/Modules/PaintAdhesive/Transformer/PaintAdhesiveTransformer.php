<?php

namespace App\Modules\PaintAdhesive\Transformer;

use App\Modules\Brand\Transformer\BrandTransformer;
use App\Modules\Color\Transformer\ColorTransformer;
use App\Modules\PaintAdhesive\Models\PaintAdhesive;
use App\Modules\PaintAdhesiveType\Transformer\PaintAdhesiveTypeTransformer;
use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\Size\Transformer\SizeTransformer;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class PaintAdhesiveTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'product', 'color', 'size', 'paintAdhesiveType', 'brand'
    ];

    public function transform(PaintAdhesive $paintAdhesive)
    {
        return [
            'id' => (int) $paintAdhesive->getId(),
            'name' => $paintAdhesive->getProduct() ? $paintAdhesive->getProduct()->getName() : null,
            'supplierId' => $paintAdhesive->getProduct() ? $paintAdhesive->getProduct()->getSupplier()->getId() : null,
            'sizeId' => $paintAdhesive->getSizeId(),
            'sizeText' => null,
            'brandId' => $paintAdhesive->getBrandId(),
            'brandText' => null,
            'colorId' => $paintAdhesive->getColorId(),
            'colorText' => null,
            'paintAdhesiveTypeId' => $paintAdhesive->getPaintAdhesiveTypeId(),
            'paintAdhesiveTypeText' => null,
            'updatedAt' => $paintAdhesive->updated_at,
            'editUri' => route('paint-adhesive_edit', $paintAdhesive->getId()),
            'showUri' => route('paint-adhesive_show', $paintAdhesive->getId()),
        ];
    }

    public function includeProduct(PaintAdhesive $paintAdhesive)
    {
        $product = $paintAdhesive->getProduct();
        return $this->item($product, new ProductTransformer);
    }

    public function includeColor(PaintAdhesive $paintAdhesive)
    {
        $color = $paintAdhesive->getColor();
        return $this->item($color, new ColorTransformer);
    }

    public function includeBrand(PaintAdhesive $paintAdhesive)
    {
        $brand = $paintAdhesive->getBrand();
        return $this->item($brand, new BrandTransformer);
    }

    public function includeSize(PaintAdhesive $paintAdhesive)
    {
        $size = $paintAdhesive->getSize();
        return $this->item($size, new SizeTransformer);
    }

    public function includePaintAdhesiveType(PaintAdhesive $paintAdhesive)
    {
        $paintAdhesiveType = $paintAdhesive->getPaintAdhesiveType();
        return $this->item($paintAdhesiveType, new PaintAdhesiveTypeTransformer);
    }
}
