<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'paint-adhesives'], function () use ($api) {
		$api->get('/', 'App\Modules\PaintAdhesive\Controllers\PaintAdhesiveApiController@index');
		$api->get('{paintAdhesiveId}', 'App\Modules\PaintAdhesive\Controllers\PaintAdhesiveApiController@show');
		$api->post('/', 'App\Modules\PaintAdhesive\Controllers\PaintAdhesiveApiController@store');
		$api->patch('{paintAdhesiveId}', 'App\Modules\PaintAdhesive\Controllers\PaintAdhesiveApiController@update');
		$api->delete('{paintAdhesiveId}', 'App\Modules\PaintAdhesive\Controllers\PaintAdhesiveApiController@destroy');
	});
});
