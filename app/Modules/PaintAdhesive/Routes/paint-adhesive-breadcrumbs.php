<?php

Breadcrumbs::register('paint-adhesive.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Paint Adhesives', route('paint-adhesive_index'));
});
Breadcrumbs::register('paint-adhesive.create', function ($breadcrumbs) {
	$breadcrumbs->parent('paint-adhesive.index');
	$breadcrumbs->push('Create', route('paint-adhesive_create'));
});
Breadcrumbs::register('paint-adhesive.show', function ($breadcrumbs, $structuralSteel) {
	$breadcrumbs->parent('paint-adhesive.index');
	$breadcrumbs->push($structuralSteel->getProduct()->getName(), route('paint-adhesive_show', $structuralSteel->getId()));
});
Breadcrumbs::register('paint-adhesive.edit', function ($breadcrumbs, $structuralSteel) {
	$breadcrumbs->parent('paint-adhesive.show', $structuralSteel);
	$breadcrumbs->push('Edit', route('paint-adhesive_edit', $structuralSteel->getId()));
});
