<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'paint-adhesives'], function () {
		Route::get('/', 'PaintAdhesiveController@index')->name('paint-adhesive_index');
		Route::get('/create', 'PaintAdhesiveController@create')->name('paint-adhesive_create');
		Route::get('{paintAdhesiveId}/edit', 'PaintAdhesiveController@edit')->name('paint-adhesive_edit');
		Route::get('{paintAdhesiveId}/print', 'PaintAdhesiveController@print')->name('paint-adhesive_print');
		Route::get('{paintAdhesiveId}', 'PaintAdhesiveController@show')->name('paint-adhesive_show');
	});
});
