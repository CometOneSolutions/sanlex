<?php

namespace App\Modules\Fastener\Controllers;

use JavaScript;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\Fastener\Services\FastenerRecordService;

class FastenerController extends Controller
{
    private $service;

    public function __construct(FastenerRecordService $fastenerRecordService)
    {
        $this->service = $fastenerRecordService;
        $this->middleware('permission:Read [MAS] Fastener')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Fastener')->only('create');
        $this->middleware('permission:Update [MAS] Fastener')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/fasteners?include=product,fastenerDimension,fastenerType,finish,size'
        ]);
        return view('fasteners.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('fasteners.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $fastener = $this->service->getById($id);
        return view('fasteners.show', compact('fastener'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $fastener = $this->service->getById($id);
        return view('fasteners.edit', compact('fastener'));
    }
}
