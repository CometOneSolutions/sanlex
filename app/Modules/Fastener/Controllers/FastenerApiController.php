<?php

namespace App\Modules\Fastener\Controllers;

use App\Modules\Fastener\Requests\StoreFastener;
use App\Modules\Fastener\Requests\UpdateFastener;
use App\Modules\Fastener\Requests\DestroyFastener;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Fastener\Services\FastenerRecordService;
use App\Modules\Fastener\Transformer\FastenerTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;

class FastenerApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;
    protected $productRecordService;

    public function __construct(
        FastenerRecordService $fastenerRecordService,
        FastenerTransformer $transformer,
        ProductRecordService $productRecordService
    ) {
        $this->middleware('auth:api');
        parent::__construct($fastenerRecordService, $transformer);
        $this->service = $fastenerRecordService;
        $this->transformer = $transformer;
        $this->productRecordService = $productRecordService;
    }

    public function store(StoreFastener $request)
    {
        $fastener = \DB::transaction(function () use ($request) {
            $name = $request->getName();

            return $this->service->create(
                $name,
                $request->getSupplier(),
                $request->getFastenerType(),
                $request->getFastenerDimension(),
                $request->getFinish(),
                $request->getSize(),
                $request->user()
            );
        });
        return $this->response->item($fastener, $this->transformer)->setStatusCode(201);
    }

    public function update($fastenerId, UpdateFastener $request)
    {
        $fastener = \DB::transaction(function () use ($request) {
            $name = $request->getName();

            return $this->service->update(
                $request->getFastener(),
                $name,
                $request->getSupplier(),
                $request->getFastenerType(),
                $request->getFastenerDimension(),
                $request->getFinish(),
                $request->getSize(),
                $request->user()
            );
        });
        return $this->response->item($fastener, $this->transformer)->setStatusCode(200);
    }

    public function destroy($fastenerId, DestroyFastener $request)
    {
        $fastener = $this->service->getById($fastenerId);
        $this->service->delete($fastener);
        return $this->response->item($fastener, $this->transformer)->setStatusCode(200);
    }
}
