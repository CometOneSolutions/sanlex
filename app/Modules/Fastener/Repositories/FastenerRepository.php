<?php

namespace App\Modules\Fastener\Repositories;

use App\Modules\Fastener\Models\Fastener;
use CometOneSolutions\Common\Repositories\Repository;


interface FastenerRepository extends Repository
{
    public function save(Fastener $fastener);

    public function delete(Fastener $fastener);
}
