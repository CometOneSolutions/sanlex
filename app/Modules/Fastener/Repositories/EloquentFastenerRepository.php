<?php

namespace App\Modules\Fastener\Repositories;

use App\Modules\Fastener\Models\Fastener;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentFastenerRepository extends EloquentRepository implements FastenerRepository
{
    public function __construct(Fastener $fastener)
    {
        parent::__construct($fastener);
    }

    public function save(Fastener $fastener)
    {
        return $fastener->save();
    }

    public function delete(Fastener $fastener)
    {
        $product = $fastener->getProduct();
        $product->delete();
        return $fastener->delete();
    }

    public function filterByName($productName)
    {
        return $this->model->whereHas('product', function ($product) use ($productName) {
            return $product->where('name', 'like', '%' . $productName . '%');
        });
    }

    public function filterBySupplierName($supplierName)
    {
        return $this->model->whereHas('product', function ($product) use ($supplierName) {
            return $product->whereHas('supplier', function ($supplier) use ($supplierName) {
                return $supplier->where('name', 'like', '%' . $supplierName . '%');
            });
        });
    }

    public function inStock()
    {
        $this->model = $this->model->inStock();
        return $this;
    }
}
