<?php

namespace App\Modules\Fastener\Provider;

use Illuminate\Support\ServiceProvider;
use App\Modules\Fastener\Models\Fastener;
use Illuminate\Database\Eloquent\Factory;
use App\Modules\Fastener\Models\FastenerModel;
use App\Modules\Fastener\Services\FastenerRecordService;
use App\Modules\Fastener\Repositories\FastenerRepository;
use App\Modules\Fastener\Services\FastenerRecordServiceImpl;
use App\Modules\Fastener\Repositories\EloquentFastenerRepository;

class FastenerServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Fastener/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(FastenerRecordService::class, FastenerRecordServiceImpl::class);
        $this->app->bind(FastenerRepository::class, EloquentFastenerRepository::class);
        $this->app->bind(Fastener::class, FastenerModel::class);
    }
}
