<?php

namespace App\Modules\Fastener\Models;

interface Fastener
{
    public function getId();

    public function getDescription();

    public function setDescription($value);
}
