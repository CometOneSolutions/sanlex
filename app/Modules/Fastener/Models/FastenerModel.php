<?php

namespace App\Modules\Fastener\Models;

use App\Modules\Size\Models\Size;
use App\Modules\Finish\Models\Finish;
use App\Modules\Size\Models\SizeModel;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Finish\Models\FinishModel;
use App\Modules\Product\Traits\Productable;
use App\Modules\Product\Models\ProductModel;
use App\Modules\FastenerType\Models\FastenerType;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use App\Modules\FastenerType\Models\FastenerTypeModel;
use App\Modules\FastenerDimension\Models\FastenerDimension;
use App\Modules\FastenerDimension\Models\FastenerDimensionModel;

class FastenerModel extends Model implements Fastener, UpdatableByUser, Productable
{
    use HasUpdatedByUser;

    protected $table = 'fasteners';

    public function product()
    {
        return $this->morphOne(ProductModel::class, 'productable');
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

    public function fastenerDimension()
    {
        return $this->belongsTo(FastenerDimensionModel::class, 'fastener_dimension_id');
    }

    public function getFastenerDimensionId()
    {
        return (int) $this->fastener_dimension_id;
    }

    public function getFastenerDimension()
    {
        return $this->fastenerDimension;
    }

    public function setFastenerDimension(FastenerDimension $fastenerDimension)
    {
        $this->fastenerDimension()->associate($fastenerDimension);
        return $this;
    }

    public function finish()
    {
        return $this->belongsTo(FinishModel::class, 'finish_id');
    }

    public function getFinishId()
    {
        return (int) $this->finish_id;
    }

    public function getFinish()
    {
        return $this->finish;
    }

    public function setFinish(Finish $finish)
    {
        $this->finish()->associate($finish);
        return $this;
    }

    public function fastenerType()
    {
        return $this->belongsTo(FastenerTypeModel::class, 'fastener_type_id');
    }

    public function getFastenerTypeId()
    {
        return (int) $this->fastener_type_id;
    }

    public function getFastenerType()
    {
        return $this->fastenerType;
    }

    public function setFastenerType(FastenerType $fastenerType)
    {
        $this->fastenerType()->associate($fastenerType);
        return $this;
    }

    public function size()
    {
        return $this->belongsTo(SizeModel::class, 'size_id');
    }

    public function getSizeId()
    {
        return (int) $this->size_id;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize(Size $size)
    {
        $this->size()->associate($size);
        return $this;
    }

    public function isCoil()
    {
        return false;
    }

    public function scopeInStock($query)
    {
        return $query->whereHas('product', function ($product) {
            return $product->whereHas('inventory', function ($inventory) {
                return $inventory->where('stock', '>', 0);
            });
        });
    }

    public function getAssembledProductName()
    {
        $nameAttributes = [
            $this->getProduct()->getSupplier()->getShortCode(),
            $this->getFastenerType()->getName(),
            $this->getFinish()->getName(),
            $this->getFastenerDimension()->getName(),
            $this->getSize()->getName()
        ];

        return  strtoupper(implode("~", $nameAttributes));
    }

    public function scopeFinish($query, Finish $finish)
    {
        return $query->whereFinishId($finish->getId());
    }

    public function scopeSize($query, Size $size)
    {
        return $query->whereSizeId($size->getId());
    }

    public function scopeFastenerDimension($query, FastenerDimension $fastenerDimension)
    {
        return $query->whereFastenerDimensionId($fastenerDimension->getId());
    }

    public function scopeFastenerType($query, FastenerType $fastenerType)
    {
        return $query->whereFastenerTypeId($fastenerType->getId());
    }
}
