<?php

namespace App\Modules\Fastener\Services;

use App\Modules\Fastener\Models\Fastener;
use App\Modules\FastenerDimension\Models\FastenerDimension;
use App\Modules\FastenerType\Models\FastenerType;
use App\Modules\Finish\Models\Finish;
use App\Modules\Size\Models\Size;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordService;

interface FastenerRecordService extends RecordService
{
    public function create(
        $name,
        Supplier $supplier,
        FastenerType $fastenerType,
        FastenerDimension $fastenerDimension,
        Finish $finish,
        Size $size,
        User $user = null
    );

    public function update(
        Fastener $fastener,
        $name,
        Supplier $supplier,
        FastenerType $fastenerType,
        FastenerDimension $fastenerDimension,
        Finish $finish,
        Size $size,
        User $user = null
    );

    public function delete(Fastener $fastener);
}
