<?php

namespace App\Modules\Fastener\Services;

use App\Modules\Fastener\Models\Fastener;
use App\Modules\Fastener\Repositories\FastenerRepository;
use App\Modules\FastenerDimension\Models\FastenerDimension;
use App\Modules\FastenerType\Models\FastenerType;
use App\Modules\Finish\Models\Finish;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Size\Models\Size;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class FastenerRecordServiceImpl extends RecordServiceImpl implements FastenerRecordService
{
    private $fastener;
    private $fasteners;
    private $productRecordService;

    protected $availableFilters = [
        ['id' => 'name', 'text' => 'Name'],
    ];

    public function __construct(FastenerRepository $fasteners, Fastener $fastener, ProductRecordService $productRecordService)
    {
        $this->fasteners = $fasteners;
        $this->fastener = $fastener;
        $this->productRecordService = $productRecordService;
        parent::__construct($fasteners);
    }

    public function create(
        $name,
        Supplier $supplier,
        FastenerType $fastenerType,
        FastenerDimension $fastenerDimension,
        Finish $finish,
        Size $size,
        User $user = null
    ) {
        $fastener = new $this->fastener;

        $fastener->setFastenerDimension($fastenerDimension);
        $fastener->setFinish($finish);
        $fastener->setSize($size);
        $fastener->setFastenerType($fastenerType);

        if ($user) {
            $fastener->setUpdatedByUser($user);
        }
        $this->fasteners->save($fastener);
        $this->productRecordService->create($name, $supplier, $fastener, $user);
        return $fastener;
    }

    public function update(
        Fastener $fastener,
        $name,
        Supplier $supplier,
        FastenerType $fastenerType,
        FastenerDimension $fastenerDimension,
        Finish $finish,
        Size $size,
        User $user = null
    ) {
        $tempFastener = clone $fastener;
        $product = $tempFastener->getProduct();
        $tempFastener->setFastenerDimension($fastenerDimension);
        $tempFastener->setFinish($finish);
        $tempFastener->setSize($size);
        $tempFastener->setFastenerType($fastenerType);
        if ($user) {
            $tempFastener->setUpdatedByUser($user);
        }
        $this->fasteners->save($tempFastener);
        $this->productRecordService->update($product, $name, $supplier, $tempFastener, $user);

        return $tempFastener;
    }

    public function delete(Fastener $fastener)
    {
        $this->fasteners->delete($fastener);
        return $fastener;
    }

    public function inStock()
    {
        $this->fasteners = $this->fasteners->inStock();
        return $this;
    }
}
