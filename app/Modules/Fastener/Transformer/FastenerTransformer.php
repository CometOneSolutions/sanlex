<?php

namespace App\Modules\Fastener\Transformer;

use App\Modules\Fastener\Models\Fastener;
use App\Modules\FastenerDimension\Transformer\FastenerDimensionTransformer;
use App\Modules\FastenerType\Transformer\FastenerTypeTransformer;
use App\Modules\Finish\Transformer\FinishTransformer;
use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\Size\Transformer\SizeTransformer;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class FastenerTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'product', 'finish', 'size', 'fastenerType', 'fastenerDimension'
    ];

    public function transform(Fastener $fastener)
    {
        return [
            'id' => (int) $fastener->getId(),
            'name' => $fastener->getProduct() ? $fastener->getProduct()->getName() : null,
            'supplierId' => $fastener->getProduct() ? $fastener->getProduct()->getSupplier()->getId() : null,
            'sizeId' => $fastener->getSizeId(),
            'sizeText' => null,
            'finishId' => $fastener->getFinishId(),
            'finishText' => null,
            'fastenerTypeId' => $fastener->getFastenerTypeId(),
            'fastenerTypeText' => null,
            'fastenerDimensionId' => $fastener->getFastenerDimensionId(),
            'fastenerDimensionText' => null,
            'updatedAt' => $fastener->updated_at,
            'editUri' => route('fastener_edit', $fastener->getId()),
            'showUri' => route('fastener_show', $fastener->getId()),
        ];
    }

    public function includeProduct(Fastener $fastener)
    {
        $product = $fastener->getProduct();
        return $this->item($product, new ProductTransformer);
    }

    public function includeFinish(Fastener $fastener)
    {
        $finsih = $fastener->getFinish();
        return $this->item($finsih, new FinishTransformer);
    }

    public function includeSize(Fastener $fastener)
    {
        $size = $fastener->getSize();
        return $this->item($size, new SizeTransformer);
    }

    public function includeFastenerType(Fastener $fastener)
    {
        $fastenerType = $fastener->getFastenerType();
        return $this->item($fastenerType, new FastenerTypeTransformer);
    }

    public function includeFastenerDimension(Fastener $fastener)
    {
        $fastenerDimension = $fastener->getFastenerDimension();
        return $this->item($fastenerDimension, new FastenerDimensionTransformer);
    }
}
