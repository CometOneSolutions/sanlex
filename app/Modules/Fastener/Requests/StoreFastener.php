<?php

namespace App\Modules\Fastener\Requests;

class StoreFastener extends FastenerRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Fastener');
    }
}
