<?php

namespace App\Modules\Fastener\Requests;

class UpdateFastener extends FastenerRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Fastener');
    }
}
