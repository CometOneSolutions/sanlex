<?php

namespace App\Modules\Fastener\Requests;

use Dingo\Api\Http\FormRequest;
use App\Modules\Size\Services\SizeRecordService;
use App\Modules\Finish\Services\FinishRecordService;
use App\Modules\Fastener\Services\FastenerRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\FastenerType\Services\FastenerTypeRecordService;
use App\Modules\FastenerDimension\Services\FastenerDimensionRecordService;

class FastenerRequest extends FormRequest
{
    private $service;
    private $supplierRecordService;
    private $fastenerDimensionRecordService;
    private $sizeRecordService;
    private $fastenerTypeRecordService;
    private $finishRecordService;

    public function __construct(
        SupplierRecordService $supplierRecordService,
        FastenerRecordService $fastenerRecordService,
        FastenerDimensionRecordService $fastenerDimensionRecordService,
        SizeRecordService $sizeRecordService,
        FinishRecordService $finishRecordService,
        FastenerTypeRecordService $fastenerTypeRecordService
    ) {
        $this->supplierRecordService = $supplierRecordService;
        $this->service = $fastenerRecordService;

        $this->fastenerDimensionRecordService = $fastenerDimensionRecordService;
        $this->sizeRecordService = $sizeRecordService;
        $this->fastenerTypeRecordService = $fastenerTypeRecordService;
        $this->finishRecordService = $finishRecordService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $fastener = $this->route('fastenerId') ? $this->service->getById($this->route('fastenerId')) : null;

        if ($fastener) {
            return [
                'name' => 'required|unique:products,name,' . $fastener->getProduct()->getId(),
                'supplierId' => 'required',
                'fastenerDimensionId' => 'required',
                'sizeId' => 'required',
                'finishId' => 'required',
                'fastenerTypeId' => 'required'
            ];
        } else {
            return [
                'name' => 'required|unique:products,name',
                'supplierId' => 'required',
                'fastenerDimensionId' => 'required',
                'finishId' => 'required',
                'sizeId' => 'required',
                'fastenerTypeId' => 'required',
            ];
        }
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required.',
            'name.unique' => 'Name already exists.',
            'supplierId.required' => 'Supplier is required.',
            'fastenerDimensionId.required' => 'Fastener dimension is required.',
            'finishId.required' => 'Finish is required.',
            'sizeId.required' => 'Size is required.',
            'fastenerTypeId.required' => 'Fastener type is required.'
        ];
    }

    public function getName($index = 'name')
    {
        return $this->input($index);
    }

    public function getSupplier($index = 'supplierId')
    {
        return $this->supplierRecordService->getById($this->input($index));
    }

    public function getFastener($index = 'id')
    {
        return $this->service->getById($this->input($index));
    }

    public function getFinish($index = 'finishId')
    {
        return $this->finishRecordService->getById($this->input($index));
    }

    public function getSize($index = 'sizeId')
    {
        return $this->sizeRecordService->getById($this->input($index));
    }

    public function getFastenerType($index = 'fastenerTypeId')
    {
        return $this->fastenerTypeRecordService->getById($this->input($index));
    }

    public function getFastenerDimension($index = 'fastenerDimensionId')
    {
        return $this->fastenerDimensionRecordService->getById($this->input($index));
    }
}
