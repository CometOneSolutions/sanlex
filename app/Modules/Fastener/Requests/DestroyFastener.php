<?php

namespace App\Modules\Fastener\Requests;

class DestroyFastener extends FastenerRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Fastener');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
