<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'fasteners'], function () use ($api) {
        $api->get('/', 'App\Modules\Fastener\Controllers\FastenerApiController@index');
        $api->get('{fastenerId}', 'App\Modules\Fastener\Controllers\FastenerApiController@show');
        $api->post('/', 'App\Modules\Fastener\Controllers\FastenerApiController@store');
        $api->patch('{fastenerId}', 'App\Modules\Fastener\Controllers\FastenerApiController@update');
        $api->delete('{fastenerId}', 'App\Modules\Fastener\Controllers\FastenerApiController@destroy');
    });
});
