<?php

Breadcrumbs::register('fastener.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Fasteners', route('fastener_index'));
});
Breadcrumbs::register('fastener.create', function ($breadcrumbs) {
    $breadcrumbs->parent('fastener.index');
    $breadcrumbs->push('Create', route('fastener_create'));
});
Breadcrumbs::register('fastener.show', function ($breadcrumbs, $fastener) {
    $breadcrumbs->parent('fastener.index');
    $breadcrumbs->push($fastener->getProduct()->getName(), route('fastener_show', $fastener->getId()));
});
Breadcrumbs::register('fastener.edit', function ($breadcrumbs, $fastener) {
    $breadcrumbs->parent('fastener.show', $fastener);
    $breadcrumbs->push('Edit', route('fastener_edit', $fastener->getId()));
});
