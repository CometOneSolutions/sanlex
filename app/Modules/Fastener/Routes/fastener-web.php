<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'fasteners'], function () {
        Route::get('/', 'FastenerController@index')->name('fastener_index');
        Route::get('/create', 'FastenerController@create')->name('fastener_create');
        Route::get('{paintAdhesiveId}/edit', 'FastenerController@edit')->name('fastener_edit');
        Route::get('{paintAdhesiveId}/print', 'FastenerController@print')->name('fastener_print');
        Route::get('{paintAdhesiveId}', 'FastenerController@show')->name('fastener_show');
    });
});
