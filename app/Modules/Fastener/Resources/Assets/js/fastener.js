import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select3 from "@c1_common_js/components/Select3";


new Vue({
    el: '#fastener',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select3
    },
    data: {
        form: new Form({
            name: null,
            supplierId: null,
            shortCode: null,
            fastenerTypeId: null,
            fastenerTypeText: null,
            finishId: null,
            finishText: null,
            sizeId: null,
            sizeText: null,
            fastenerDimensionId: null,
            fastenerDimensionText: null,
        }),
        dataInitialized: true,
        supplierUrl: '/api/suppliers?sort=name',
        defaultSelectedSupplier: {},
        fastenerDimensionUrl: '/api/fastener-dimensions?sort=name',
        defaultSelectedFastenerDimension: {},
        sizeUrl: '/api/sizes?sort=name',
        defaultSelectedSize: {},
        finishUrl: '/api/finishes?sort=name',
        defaultSelectedFinish: {},
        fastenerTypeUrl: '/api/fastener-types?sort=name',
        defaultSelectedFastenerType: {}

    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },
    computed: {
        // selectedSupplier() {
        //     if(this.form.supplierId == null)
        //     {
        //         return undefined;
        //     }
        //     return this.supplierSelections.find(supplier => supplier.id == this.form.supplierId);
        // },
        name() {
            if (this.form.supplierId === undefined || this.form.fastenerTypeId === null || this.form.finishId === null || this.form.fastenerDimensionId === null || this.form.sizeId === null) {
                return undefined;
            }
            return this.form.shortCode + '~' + this.form.fastenerTypeText.toUpperCase() + '~' + this.form.finishText.toUpperCase()
                + '~' + this.form.fastenerDimensionText.toUpperCase() + '~' + this.form.sizeText.toUpperCase();
        },
        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        setShortCode(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("shortCode")) {
                this.form.shortCode = selectedObjects[0].shortCode;
            }
        },
        setFastenerType(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.fastenerTypeText = selectedObjects[0].name;
            }

        },
        setFastenerDimension(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.fastenerDimensionText = selectedObjects[0].name;
            }

        },
        setFinish(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.finishText = selectedObjects[0].name;
            }

        },
        setSize(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.sizeText = selectedObjects[0].name;
            }

        },

        destroy() {
            this.form.deleteWithConfirmation('/api/fasteners/' + this.form.id).then(response => {
                this.form.successModal('Fastener was removed.').then(() =>
                    window.location = '/master-file/fasteners/'
                );
            });
        },

        store() {
            this.form.name = this.name;
            this.form.postWithModal('/api/fasteners', null, 'Fastener was saved.');
        },

        update() {
            this.form.name = this.name;
            this.form.patch('/api/fasteners/' + this.form.id).then(response => {
                this.form.successModal('Fastener was updated.').then(() =>
                    window.location = '/master-file/fasteners/' + this.form.id
                );
            })
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {


        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/fasteners/' + id + '?include=product.supplier,fastenerType,fastenerDimension,finish,size')
                .then(response => {
                    this.loadData(response.data);

                    this.form.shortCode = response.data.product.data.supplier.data.shortCode;
                    this.form.fastenerTypeText = response.data.fastenerType.data.name;
                    this.form.fastenerDimensionText = response.data.fastenerDimension.data.name;
                    this.form.finishText = response.data.finish.data.name;
                    this.form.sizeText = response.data.size.data.name;


                    this.defaultSelectedSupplier = {
                        text: response.data.product.data.supplier.data.name,
                        id: response.data.product.data.supplier.data.id,
                    };
                    this.defaultSelectedFastenerType = {
                        text: response.data.fastenerType.data.name,
                        id: response.data.fastenerType.data.id,
                    };
                    this.defaultSelectedFastenerDimension = {
                        text: response.data.fastenerDimension.data.name,
                        id: response.data.fastenerDimension.data.id,
                    };
                    this.defaultSelectedFinish = {
                        text: response.data.finish.data.name,
                        id: response.data.finish.data.id,
                    };
                    this.defaultSelectedSize = {
                        text: response.data.size.data.name,
                        id: response.data.size.data.id,
                    };
                    this.dataInitialized = true;
                });
        }
    },
    mounted() {
        console.log("Init fastener script...");
    }
});