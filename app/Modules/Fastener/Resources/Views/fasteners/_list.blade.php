<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Name</label>
            <p>@{{form.product.data.name }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Supplier</label>
            <p>@{{form.product.data.supplier.data.name }}</p>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Fastener Type</label>
            <p>@{{form.fastenerType.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Fastener Dimension</label>
            <p>@{{form.fastenerDimension.data.name || "--" }}</p>
        </div>
	</div>
	<div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Finish</label>
            <p>@{{form.finish.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Size</label>
            <p>@{{form.size.data.name || "--" }}</p>
        </div>
    </div>
  
</div>
@push('scripts')
<script src="{{ mix('js/fastener.js') }}"></script>
@endpush