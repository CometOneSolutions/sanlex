<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Supplier <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter supplier" v-model="form.supplierId" :selected="defaultSelectedSupplier" :url="supplierUrl" search="name" placeholder="Please select" @change="setShortCode" id="supplier" name="supplier">
            </select3>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Fastener Type <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter fastener type" v-model="form.fastenerTypeId" :selected="defaultSelectedFastenerType" :url="fastenerTypeUrl" search="name" placeholder="Please select" @change="setFastenerType" id="fastenerType" name="fastenerType">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Fastener Dimension <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter fastener dimension" v-model="form.fastenerDimensionId" :selected="defaultSelectedFastenerDimension" :url="fastenerDimensionUrl" search="name" placeholder="Please select" @change="setFastenerDimension" id="fastenerDimension" name="fastenerDimension">
            </select3>
        </div>
	</div>
	<div class="col-12">
        <div class="form-group">
            <label for="supplier">Finish <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter finish" v-model="form.finishId" :selected="defaultSelectedFinish" :url="finishUrl" search="name" placeholder="Please select" @change="setFinish" id="finish" name="finish">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Size <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter size" v-model="form.sizeId" :selected="defaultSelectedSize" :url="sizeUrl" search="name" placeholder="Please select" @change="setSize" id="size" name="size">
            </select3>
        </div>
    </div>
    
</div>
@push('scripts')
<script src="{{ mix('js/fastener.js') }}"></script>
@endpush