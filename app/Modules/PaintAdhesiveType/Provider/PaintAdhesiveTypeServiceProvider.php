<?php

namespace App\Modules\PaintAdhesiveType\Provider;

use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveTypeModel;
use App\Modules\PaintAdhesiveType\Repositories\EloquentPaintAdhesiveTypeRepository;
use App\Modules\PaintAdhesiveType\Repositories\PaintAdhesiveTypeRepository;
use App\Modules\PaintAdhesiveType\Services\PaintAdhesiveTypeRecordService;
use App\Modules\PaintAdhesiveType\Services\PaintAdhesiveTypeRecordServiceImpl;
use Illuminate\Support\ServiceProvider;


class PaintAdhesiveTypeServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/PaintAdhesiveType/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(PaintAdhesiveTypeRecordService::class, PaintAdhesiveTypeRecordServiceImpl::class);
		$this->app->bind(PaintAdhesiveTypeRepository::class, EloquentPaintAdhesiveTypeRepository::class);
		$this->app->bind(PaintAdhesiveType::class, PaintAdhesiveTypeModel::class);
	}
}
