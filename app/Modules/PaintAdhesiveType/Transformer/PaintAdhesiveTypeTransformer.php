<?php

namespace App\Modules\PaintAdhesiveType\Transformer;

use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class PaintAdhesiveTypeTransformer extends UpdatableByUserTransformer
{
	public function transform(PaintAdhesiveType $paintAdhesiveType)
	{
		return [
			'id' => (int) $paintAdhesiveType->getId(),
			'text' => $paintAdhesiveType->getName(),
			'name' => $paintAdhesiveType->getName(),
			'updatedAt' => $paintAdhesiveType->updated_at,
			'editUri' => route('paint-adhesive-type_edit', $paintAdhesiveType->getId()),
			'showUri' => route('paint-adhesive-type_show', $paintAdhesiveType->getId()),
		];
	}
}
