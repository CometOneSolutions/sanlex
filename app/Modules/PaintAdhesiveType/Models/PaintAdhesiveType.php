<?php

namespace App\Modules\PaintAdhesiveType\Models;



interface PaintAdhesiveType
{
	public function getId();

	public function getName();

	public function setName($value);
}
