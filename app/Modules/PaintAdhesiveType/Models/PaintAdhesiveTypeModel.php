<?php

namespace App\Modules\PaintAdhesiveType\Models;

use App\Modules\Brand\Models\BrandModel;
use App\Modules\PaintAdhesive\Models\PaintAdhesiveModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Models\Users\User;

class PaintAdhesiveTypeModel extends Model implements PaintAdhesiveType, UpdatableByUser
{
	use HasUpdatedByUser;

	protected $table = 'paint_adhesive_types';

	public function paintAdhesives()
	{
		return $this->hasMany(PaintAdhesiveModel::class, 'paint_adhesive_type_id');
	}


	public function paintAdhesiveBrands()
	{
		return $this->belongsToMany(BrandModel::class, 'paint_adhesives', 'paint_adhesive_type_id', 'brand_id')
			->withTimestamps();
	}

	public function getPaintAdhesiveBrands()
	{
		return $this->paintAdhesiveBrands;
	}

	public function getPaintAdhesives()
	{
		return $this->paintAdhesives;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($value)
	{
		$this->name = $value;
		return $this;
	}
}
