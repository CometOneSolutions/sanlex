<?php

namespace App\Modules\PaintAdhesiveType\Repositories;

use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentPaintAdhesiveTypeRepository extends EloquentRepository implements PaintAdhesiveTypeRepository
{
	public function __construct(PaintAdhesiveType $paintAdhesiveType)
	{
		parent::__construct($paintAdhesiveType);
	}

	public function save(PaintAdhesiveType $paintAdhesiveType)
	{
		return $paintAdhesiveType->save();
	}

	public function delete(PaintAdhesiveType $paintAdhesiveType)
	{
		return $paintAdhesiveType->delete();
	}
}
