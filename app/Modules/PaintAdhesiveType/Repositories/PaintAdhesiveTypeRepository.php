<?php

namespace App\Modules\PaintAdhesiveType\Repositories;

use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use CometOneSolutions\Common\Repositories\Repository;

interface PaintAdhesiveTypeRepository extends Repository
{
	public function save(PaintAdhesiveType $paintAdhesiveType);
	public function delete(PaintAdhesiveType $paintAdhesiveType);
}
