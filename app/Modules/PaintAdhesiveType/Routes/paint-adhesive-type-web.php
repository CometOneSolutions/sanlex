<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'paint-adhesive-types'], function () {
		Route::get('/', 'PaintAdhesiveTypeController@index')->name('paint-adhesive-type_index');
		Route::get('/create', 'PaintAdhesiveTypeController@create')->name('paint-adhesive-type_create');
		Route::get('{paintAdhesiveTypeId}/edit', 'PaintAdhesiveTypeController@edit')->name('paint-adhesive-type_edit');
		Route::get('{paintAdhesiveTypeId}/print', 'PaintAdhesiveTypeController@print')->name('paint-adhesive-type_print');
		Route::get('{paintAdhesiveTypeId}', 'PaintAdhesiveTypeController@show')->name('paint-adhesive-type_show');
	});
});
