<?php

Breadcrumbs::register('paint-adhesive-type.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Paint Adhesive Types', route('paint-adhesive-type_index'));
});
Breadcrumbs::register('paint-adhesive-type.create', function ($breadcrumbs) {
	$breadcrumbs->parent('paint-adhesive-type.index');
	$breadcrumbs->push('Create', route('paint-adhesive-type_create'));
});
Breadcrumbs::register('paint-adhesive-type.show', function ($breadcrumbs, $paintAdhesiveType) {
	$breadcrumbs->parent('paint-adhesive-type.index');
	$breadcrumbs->push($paintAdhesiveType->getName(), route('paint-adhesive-type_show', $paintAdhesiveType->getId()));
});
Breadcrumbs::register('paint-adhesive-type.edit', function ($breadcrumbs, $paintAdhesiveType) {
	$breadcrumbs->parent('paint-adhesive-type.show', $paintAdhesiveType);
	$breadcrumbs->push('Edit', route('paint-adhesive-type_edit', $paintAdhesiveType->getId()));
});
