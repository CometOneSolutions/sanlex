<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'paint-adhesive-types'], function () use ($api) {
		$api->get('/', 'App\Modules\PaintAdhesiveType\Controllers\PaintAdhesiveTypeApiController@index');
		$api->get('{paintAdhesiveTypeId}', 'App\Modules\PaintAdhesiveType\Controllers\PaintAdhesiveTypeApiController@show');
		$api->post('/', 'App\Modules\PaintAdhesiveType\Controllers\PaintAdhesiveTypeApiController@store');
		$api->patch('{paintAdhesiveTypeId}', 'App\Modules\PaintAdhesiveType\Controllers\PaintAdhesiveTypeApiController@update');
		$api->delete('{paintAdhesiveTypeId}', 'App\Modules\PaintAdhesiveType\Controllers\PaintAdhesiveTypeApiController@destroy');
	});
});
