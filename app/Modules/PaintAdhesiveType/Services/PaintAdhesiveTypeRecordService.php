<?php

namespace App\Modules\PaintAdhesiveType\Services;

use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface PaintAdhesiveTypeRecordService extends RecordService
{
	public function create(
		$name,
		User $user = null
	);

	public function update(
		PaintAdhesiveType $paintAdhesiveType,
		$name,
		User $user = null
	);

	public function delete(PaintAdhesiveType $paintAdhesiveType);
}
