<?php

namespace App\Modules\PaintAdhesiveType\Services;

use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use App\Modules\PaintAdhesiveType\Repositories\PaintAdhesiveTypeRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class PaintAdhesiveTypeRecordServiceImpl extends RecordServiceImpl implements PaintAdhesiveTypeRecordService
{
	use CanUpdateProductName;

	private $paintAdhesive;
	private $paintAdhesives;

	protected $availableFilters = [
		["id" => "name", "text" => "Name"]
	];

	public function __construct(PaintAdhesiveTypeRepository $paintAdhesives, PaintAdhesiveType $paintAdhesive)
	{
		$this->paintAdhesives = $paintAdhesives;
		$this->paintAdhesive = $paintAdhesive;
		parent::__construct($paintAdhesives);
	}

	public function create(
		$name,
		User $user = null
	) {
		$paintAdhesive = new $this->paintAdhesive;
		$paintAdhesive->setName($name);

		if ($user) {
			$paintAdhesive->setUpdatedByUser($user);
		}
		$this->paintAdhesives->save($paintAdhesive);
		return $paintAdhesive;
	}

	public function update(
		PaintAdhesiveType $paintAdhesive,
		$name,
		User $user = null
	) {
		$tempPaintAdhesiveType = clone $paintAdhesive;
		$tempPaintAdhesiveType->setName($name);
		if ($user) {
			$tempPaintAdhesiveType->setUpdatedByUser($user);
		}
		$this->paintAdhesives->save($tempPaintAdhesiveType);

		$this->updateProductableProductNames([
			$tempPaintAdhesiveType->getPaintAdhesives()
		]);

		return $tempPaintAdhesiveType;
	}

	public function delete(PaintAdhesiveType $paintAdhesive)
	{
		$this->paintAdhesives->delete($paintAdhesive);
		return $paintAdhesive;
	}
}
