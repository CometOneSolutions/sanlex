<?php

namespace App\Modules\PaintAdhesiveType\Requests;

use Dingo\Api\Http\FormRequest;

class UpdatePaintAdhesiveType extends PaintAdhesiveTypeRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] Paint Adhesive Type');
	}
}
