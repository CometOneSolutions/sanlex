<?php

namespace App\Modules\PaintAdhesiveType\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyPaintAdhesiveType extends PaintAdhesiveTypeRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Paint Adhesive Type');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
