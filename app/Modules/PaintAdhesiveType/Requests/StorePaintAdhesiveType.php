<?php

namespace App\Modules\PaintAdhesiveType\Requests;

use Dingo\Api\Http\FormRequest;

class StorePaintAdhesiveType extends PaintAdhesiveTypeRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [MAS] Paint Adhesive Type');
	}
}
