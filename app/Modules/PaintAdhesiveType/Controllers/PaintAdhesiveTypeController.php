<?php

namespace App\Modules\PaintAdhesiveType\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\PaintAdhesiveType\Services\PaintAdhesiveTypeRecordService;


class PaintAdhesiveTypeController extends Controller
{
	private $service;

	public function __construct(PaintAdhesiveTypeRecordService $paintAdhesiveTypeRecordService)
	{
		$this->service = $paintAdhesiveTypeRecordService;
		$this->middleware('permission:Read [MAS] Paint Adhesive Type')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Paint Adhesive Type')->only('create');
		$this->middleware('permission:Update [MAS] Paint Adhesive Type')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'name',
			'sortAscending' => true,
			'baseUrl' => '/api/paint-adhesive-types'
		]);
		return view('paint-adhesive-types.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('paint-adhesive-types.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$paintAdhesiveType = $this->service->getById($id);
		return view('paint-adhesive-types.show', compact('paintAdhesiveType'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$paintAdhesiveType = $this->service->getById($id);
		return view('paint-adhesive-types.edit', compact('paintAdhesiveType'));
	}
}
