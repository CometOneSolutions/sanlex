<?php

namespace App\Modules\PaintAdhesiveType\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\PaintAdhesiveType\Services\PaintAdhesiveTypeRecordService;
use App\Modules\PaintAdhesiveType\Transformer\PaintAdhesiveTypeTransformer;
use App\Modules\PaintAdhesiveType\Requests\StorePaintAdhesiveType;
use App\Modules\PaintAdhesiveType\Requests\UpdatePaintAdhesiveType;
use App\Modules\PaintAdhesiveType\Requests\DestroyPaintAdhesiveType;

class PaintAdhesiveTypeApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;

	public function __construct(
		PaintAdhesiveTypeRecordService $paintAdhesiveTypeRecordService,
		PaintAdhesiveTypeTransformer $transformer
	) {
		$this->middleware('auth:api');
		parent::__construct($paintAdhesiveTypeRecordService, $transformer);
		$this->service = $paintAdhesiveTypeRecordService;
		$this->transformer = $transformer;
	}

	public function store(StorePaintAdhesiveType $request)
	{
		$paintAdhesiveType = \DB::transaction(function () use ($request) {
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->create(
				$name,

				$user
			);
		});
		return $this->response->item($paintAdhesiveType, $this->transformer)->setStatusCode(201);
	}

	public function update($paintAdhesiveTypeId, UpdatePaintAdhesiveType $request)
	{
		$paintAdhesiveType = \DB::transaction(function () use ($paintAdhesiveTypeId, $request) {
			$paintAdhesiveType   = $this->service->getById($paintAdhesiveTypeId);
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->update(
				$paintAdhesiveType,
				$name,

				$user
			);
		});
		return $this->response->item($paintAdhesiveType, $this->transformer)->setStatusCode(200);
	}

	public function destroy($paintAdhesiveTypeId, DestroyPaintAdhesiveType $request)
	{
		$paintAdhesiveType = $this->service->getById($paintAdhesiveTypeId);
		$this->service->delete($paintAdhesiveType);
		return $this->response->item($paintAdhesiveType, $this->transformer)->setStatusCode(200);
	}
}
