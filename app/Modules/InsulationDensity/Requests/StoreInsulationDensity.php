<?php

namespace App\Modules\InsulationDensity\Requests;

use Dingo\Api\Http\FormRequest;

class StoreInsulationDensity extends InsulationDensityRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Insulation Type');
    }
}
