<?php

namespace App\Modules\InsulationDensity\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyInsulationDensity extends InsulationDensityRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Insulation Type');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
