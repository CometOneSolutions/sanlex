<?php

namespace App\Modules\InsulationDensity\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateInsulationDensity extends InsulationDensityRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Insulation Type');
    }
}
