<?php

namespace App\Modules\InsulationDensity\Transformer;

use App\Modules\InsulationDensity\Models\InsulationDensity;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class InsulationDensityTransformer extends UpdatableByUserTransformer
{
    public function transform(InsulationDensity $insulationDensity)
    {
        return [
            'id' => (int) $insulationDensity->getId(),
            'text' => $insulationDensity->getName(),
            'name' => $insulationDensity->getName(),
            'updatedAt' => $insulationDensity->updated_at,
            'editUri' => route('insulation-density_edit', $insulationDensity->getId()),
            'showUri' => route('insulation-density_show', $insulationDensity->getId()),
        ];
    }
}
