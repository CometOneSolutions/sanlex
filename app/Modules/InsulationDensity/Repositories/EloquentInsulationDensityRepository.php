<?php

namespace App\Modules\InsulationDensity\Repositories;

use App\Modules\InsulationDensity\Models\InsulationDensity;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentInsulationDensityRepository extends EloquentRepository implements InsulationDensityRepository
{
    public function __construct(InsulationDensity $insulationDensity)
    {
        parent::__construct($insulationDensity);
    }

    public function save(InsulationDensity $insulationDensity)
    {
        return $insulationDensity->save();
    }

    public function delete(InsulationDensity $insulationDensity)
    {
        return $insulationDensity->delete();
    }
}
