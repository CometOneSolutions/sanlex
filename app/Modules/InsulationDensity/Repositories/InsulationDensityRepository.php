<?php

namespace App\Modules\InsulationDensity\Repositories;

use App\Modules\InsulationDensity\Models\InsulationDensity;
use CometOneSolutions\Common\Repositories\Repository;

interface InsulationDensityRepository extends Repository
{
    public function save(InsulationDensity $insulationDensity);
    public function delete(InsulationDensity $insulationDensity);
}
