@extends('app')
@section('breadcrumbs', Breadcrumbs::render('insulation-density.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Insulation Density
            @if(auth()->user()->can('Create [MAS] Insulation Type'))
            <div class="float-right">
                <a href="{{route('insulation-density_create')}}"><button density="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
            </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sort-ascending="sortAscending" :sorter="sorter" v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('name')">
                                    Name <i class="fa" :class="getSortIcon('name')"></i>
                                </a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{ item.name }}</a></td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush