import {Form} from '@c1_common_js/components/Form';

export const widthService = new Vue({

    data: function() {
        return {
            form: new Form({}),
            uri: {
              widths: '/api/widths?limit=' + Number.MAX_SAFE_INTEGER
            },
        }
    },   

    methods: {

        getWidths(){
            return this.form.get(this.uri.widths);
        },
    }
});