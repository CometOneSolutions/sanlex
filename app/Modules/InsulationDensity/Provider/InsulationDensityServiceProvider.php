<?php

namespace App\Modules\InsulationDensity\Provider;

use App\Modules\InsulationDensity\Models\InsulationDensity;
use App\Modules\InsulationDensity\Models\InsulationDensityModel;
use App\Modules\InsulationDensity\Repositories\EloquentInsulationDensityRepository;
use App\Modules\InsulationDensity\Repositories\InsulationDensityRepository;
use App\Modules\InsulationDensity\Services\InsulationDensityRecordService;
use App\Modules\InsulationDensity\Services\InsulationDensityRecordServiceImpl;
use Illuminate\Support\ServiceProvider;


class InsulationDensityServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/InsulationDensity/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(InsulationDensityRecordService::class, InsulationDensityRecordServiceImpl::class);
        $this->app->bind(InsulationDensityRepository::class, EloquentInsulationDensityRepository::class);
        $this->app->bind(InsulationDensity::class, InsulationDensityModel::class);
    }
}
