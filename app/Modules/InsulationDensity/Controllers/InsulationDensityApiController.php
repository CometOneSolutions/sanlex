<?php

namespace App\Modules\InsulationDensity\Controllers;


use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\InsulationDensity\Services\InsulationDensityRecordService;
use App\Modules\InsulationDensity\Transformer\InsulationDensityTransformer;
use App\Modules\InsulationDensity\Requests\StoreInsulationDensity;
use App\Modules\InsulationDensity\Requests\UpdateInsulationDensity;
use App\Modules\InsulationDensity\Requests\DestroyInsulationDensity;

class InsulationDensityApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        InsulationDensityRecordService $insulationDensityRecordService,
        InsulationDensityTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($insulationDensityRecordService, $transformer);
        $this->service = $insulationDensityRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreInsulationDensity $request)
    {
        $insulationDensity = \DB::transaction(function () use ($request) {
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->create(
                $name,

                $user
            );
        });
        return $this->response->item($insulationDensity, $this->transformer)->setStatusCode(201);
    }

    public function update($insulationDensityId, UpdateInsulationDensity $request)
    {
        $insulationDensity = \DB::transaction(function () use ($insulationDensityId, $request) {
            $insulationDensity   = $this->service->getById($insulationDensityId);
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->update(
                $insulationDensity,
                $name,

                $user
            );
        });
        return $this->response->item($insulationDensity, $this->transformer)->setStatusCode(200);
    }

    public function destroy($insulationDensityId, DestroyInsulationDensity $request)
    {
        $insulationDensity = $this->service->getById($insulationDensityId);
        $this->service->delete($insulationDensity);
        return $this->response->item($insulationDensity, $this->transformer)->setStatusCode(200);
    }
}
