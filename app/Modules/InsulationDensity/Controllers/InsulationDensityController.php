<?php

namespace App\Modules\InsulationDensity\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\InsulationDensity\Services\InsulationDensityRecordService;


class InsulationDensityController extends Controller
{
    private $service;

    public function __construct(InsulationDensityRecordService $insulationDensityRecordService)
    {
        $this->service = $insulationDensityRecordService;
        $this->middleware('permission:Read [MAS] Insulation Density')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Insulation Density')->only('create');
        $this->middleware('permission:Update [MAS] Insulation Density')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/insulation-densities'
        ]);
        return view('insulation-densities.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('insulation-densities.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $insulationDensity = $this->service->getById($id);
        return view('insulation-densities.show', compact('insulationDensity'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $insulationDensity = $this->service->getById($id);
        return view('insulation-densities.edit', compact('insulationDensity'));
    }
}
