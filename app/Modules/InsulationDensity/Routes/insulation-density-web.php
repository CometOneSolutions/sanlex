<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'insulation-densities'], function () {
        Route::get('/', 'InsulationDensityController@index')->name('insulation-density_index');
        Route::get('/create', 'InsulationDensityController@create')->name('insulation-density_create');
        Route::get('{insulationDensityId}/edit', 'InsulationDensityController@edit')->name('insulation-density_edit');
        Route::get('{insulationDensityId}/print', 'InsulationDensityController@print')->name('insulation-density_print');
        Route::get('{insulationDensityId}', 'InsulationDensityController@show')->name('insulation-density_show');
    });
});
