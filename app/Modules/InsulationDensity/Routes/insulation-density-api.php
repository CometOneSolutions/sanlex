<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'insulation-densities'], function () use ($api) {
        $api->get('/', 'App\Modules\InsulationDensity\Controllers\InsulationDensityApiController@index');
        $api->get('{insulationDensityId}', 'App\Modules\InsulationDensity\Controllers\InsulationDensityApiController@show');
        $api->post('/', 'App\Modules\InsulationDensity\Controllers\InsulationDensityApiController@store');
        $api->patch('{insulationDensityId}', 'App\Modules\InsulationDensity\Controllers\InsulationDensityApiController@update');
        $api->delete('{insulationDensityId}', 'App\Modules\InsulationDensity\Controllers\InsulationDensityApiController@destroy');
    });
});
