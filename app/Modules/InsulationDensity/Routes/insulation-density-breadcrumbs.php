<?php

Breadcrumbs::register('insulation-density.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Insulation Densities', route('insulation-density_index'));
});
Breadcrumbs::register('insulation-density.create', function ($breadcrumbs) {
    $breadcrumbs->parent('insulation-density.index');
    $breadcrumbs->push('Create', route('insulation-density_create'));
});
Breadcrumbs::register('insulation-density.show', function ($breadcrumbs, $insulationDensity) {
    $breadcrumbs->parent('insulation-density.index');
    $breadcrumbs->push($insulationDensity->getName(), route('insulation-density_show', $insulationDensity->getId()));
});
Breadcrumbs::register('insulation-density.edit', function ($breadcrumbs, $insulationDensity) {
    $breadcrumbs->parent('insulation-density.show', $insulationDensity);
    $breadcrumbs->push('Edit', route('insulation-density_edit', $insulationDensity->getId()));
});
