<?php

namespace App\Modules\InsulationDensity\Services;

use App\Modules\InsulationDensity\Models\InsulationDensity;
use App\Modules\InsulationDensity\Repositories\InsulationDensityRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class InsulationDensityRecordServiceImpl extends RecordServiceImpl implements InsulationDensityRecordService
{
    use CanUpdateProductName;


    private $insulationDensity;
    private $insulationDensitys;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(InsulationDensityRepository $insulationDensitys, InsulationDensity $insulationDensity)
    {
        $this->insulationDensities = $insulationDensitys;
        $this->insulationDensity = $insulationDensity;
        parent::__construct($insulationDensitys);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $insulationDensity = new $this->insulationDensity;
        $insulationDensity->setName($name);

        if ($user) {
            $insulationDensity->setUpdatedByUser($user);
        }
        $this->insulationDensities->save($insulationDensity);



        return $insulationDensity;
    }

    public function update(
        InsulationDensity $insulationDensity,
        $name,
        User $user = null
    ) {
        $tempInsulationDensity = clone $insulationDensity;
        $tempInsulationDensity->setName($name);
        if ($user) {
            $tempInsulationDensity->setUpdatedByUser($user);
        }
        $this->insulationDensities->save($tempInsulationDensity);

        $this->updateProductableProductNames([
            $tempInsulationDensity->getInsulations()
        ]);

        return $tempInsulationDensity;
    }

    public function delete(InsulationDensity $insulationDensity)
    {
        $this->insulationDensities->delete($insulationDensity);
        return $insulationDensity;
    }
}
