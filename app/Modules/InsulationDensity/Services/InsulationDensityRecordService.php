<?php

namespace App\Modules\InsulationDensity\Services;

use App\Modules\InsulationDensity\Models\InsulationDensity;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface InsulationDensityRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        InsulationDensity $insulationDensity,
        $name,
        User $user = null
    );

    public function delete(InsulationDensity $insulationDensity);
}
