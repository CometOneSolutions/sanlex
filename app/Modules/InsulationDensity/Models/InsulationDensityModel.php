<?php

namespace App\Modules\InsulationDensity\Models;

use App\Modules\Insulation\Models\InsulationModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;


class InsulationDensityModel extends Model implements InsulationDensity, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'insulation_densities';

    public function insulations()
    {
        return $this->hasMany(InsulationModel::class, 'insulation_density_id');
    }

    public function getInsulations()
    {
        return $this->insulations;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
