<?php

namespace App\Modules\InsulationDensity\Models;



interface InsulationDensity
{
    public function getId();

    public function getName();

    public function setName($value);
}
