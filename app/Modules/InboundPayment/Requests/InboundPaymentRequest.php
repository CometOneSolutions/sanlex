<?php

namespace App\Modules\InboundPayment\Requests;

use App\Modules\Contract\Services\ContractRecordService;
use DateTime;
use Dingo\Api\Http\FormRequest;

class InboundPaymentRequest extends FormRequest
{
    private $contractRecordService;

    public function __construct(ContractRecordService $contractRecordService)
    {
        $this->contractRecordService = $contractRecordService;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contractId' => 'required',
            'amount' => 'required',
            'date' => 'required',
            'type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'contractId.required' => 'Name is required.',
            'amount.required' => 'Amount is required',
            'date.required' => "Date is required",
            'type.required' => 'Type is required'
        ];
    }

    public function getContract()
    {
        return $this->contractRecordService->getById($this->input('contractId'));
    }

    public function getAmount()
    {
        return $this->input('amount');
    }

    public function getType()
    {
        return $this->input('type');
    }

    public function getDate()
    {
        return new DateTime($this->input('date'));
    }

    public function getCheckDate()
    {
        return $this->input('chkDate') !== "null" ? new DateTime($this->input('chkDate')) : null;
    }

    public function getCheckBank()
    {
        return $this->input('chkBank');
    }

    public function getCheckBranch()
    {
        return $this->input('chkBranch');
    }

    public function getCheckNumber()
    {
        return $this->input('chkNumber');
    }

    public function getFiles()
    {
        if (!$this->files->get('files')) {
            return [];
        }
        return $this->files->get('files');
    }

    public function getCleared()
    {
        if ($this->input('cleared') == "false") {
            return false;
        }
        return true;
    }

    public function getAccount()
    {
        return $this->input('account');
    }
}
