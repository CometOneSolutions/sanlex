<?php

namespace App\Modules\InboundPayment\Requests;

use App\Modules\InboundPayment\Services\InboundPaymentRecordService;

class UpdateInboundPayment extends InboundPaymentRequest
{

    public function authorize()
    {
        return $this->user()->can('Update [COL] Inbound Payment');
    }


}
