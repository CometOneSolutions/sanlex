<?php

namespace App\Modules\InboundPayment\Requests;

use App\Modules\InboundPayment\Services\InboundPaymentRecordService;

class StoreInboundPayment extends InboundPaymentRequest
{

    public function authorize()
    {
        return $this->user()->can('Create [COL] Inbound Payment');
    }

}
