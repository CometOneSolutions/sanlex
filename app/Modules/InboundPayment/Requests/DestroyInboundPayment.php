<?php

namespace App\Modules\InboundPayment\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyInboundPayment extends FormRequest
{


    public function authorize()
    {
        return $this->user()->can('Delete [COL] Inbound Payment');
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }

    


}
