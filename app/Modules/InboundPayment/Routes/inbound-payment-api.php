<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'inbound-payments'], function () use ($api) {
        $api->get('/', '\App\Modules\InboundPayment\Controllers\InboundPaymentApiController@index');
        $api->get('{inboundPaymentId}', '\App\Modules\InboundPayment\Controllers\InboundPaymentApiController@show');
        $api->post('/', '\App\Modules\InboundPayment\Controllers\InboundPaymentApiController@store');
        $api->post('{inboundPaymentId}', '\App\Modules\InboundPayment\Controllers\InboundPaymentApiController@update');
        $api->delete('{inboundPaymentId}', '\App\Modules\InboundPayment\Controllers\InboundPaymentApiController@destroy');
    });
});
