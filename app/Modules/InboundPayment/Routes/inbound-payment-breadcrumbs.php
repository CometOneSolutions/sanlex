<?php

Breadcrumbs::register('inbound-payment.collection', function ($breadcrumbs) {
    $breadcrumbs->parent('collection.index');
    $breadcrumbs->push('Inbound Payments', route('inbound-payment_index'));
});
Breadcrumbs::register('inbound-payment.collection.create', function ($breadcrumbs) {
    $breadcrumbs->parent('inbound-payment.collection');
    $breadcrumbs->push('Create', route('inbound-payment_create'));
});
Breadcrumbs::register('inbound-payment.collection.show', function ($breadcrumbs, $inboundPayment) {
    $breadcrumbs->parent('inbound-payment.collection');
    $breadcrumbs->push($inboundPayment->id, route('inbound-payment_show', $inboundPayment->getId()));
});
Breadcrumbs::register('inbound-payment.collection.edit', function ($breadcrumbs, $inboundPayment) {
    $breadcrumbs->parent('inbound-payment.collection.show', $inboundPayment);
    $breadcrumbs->push('Edit', route('inbound-payment_edit', $inboundPayment->getId()));
});
