<?php

Route::group(['prefix' => 'collections'], function () {


    Route::group(['prefix' => 'inbound-payments'], function () {
        Route::get('/', 'InboundPaymentController@index')->name('inbound-payment_index');
        Route::get('create', 'InboundPaymentController@create')->name('inbound-payment_create');
        Route::get('{inboundPaymentId}', 'InboundPaymentController@show')->name('inbound-payment_show');
        Route::get('{inboundPaymentId}/edit', 'InboundPaymentController@edit')->name('inbound-payment_edit');
    });
});
