<?php

namespace App\Modules\InboundPayment\Services;

use App\Modules\Contract\Models\Contract;
use App\Modules\InboundPayment\Models\InboundPayment;
use App\Modules\InboundPayment\Repositories\InboundPaymentRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use DateTime;
use CometOneSolutions\Auth\Models\Users\User;

class InboundPaymentRecordServiceImpl extends RecordServiceImpl implements InboundPaymentRecordService
{

    private $inboundPayments;
    private $inboundPayment;

    protected $availableFilters = [
        ["id" => "date", "text" => "Date"],
        ['id' => 'contract_name', 'text' => 'Contract'],
        ['id' => 'customer_name', 'text' => 'Customer'],
        ['id' => 'chk_date', 'text' => 'Check Date'],
        ['id' => 'chk_bank', 'text' => 'Check Bank'],
        ['id' => 'chk_branch', 'text' => 'Check Branch'],
        ['id' => 'chk_number', 'text' => 'Check Number']
    ];

    public function __construct(InboundPaymentRepository $inboundPayments, InboundPayment $inboundPayment)
    {
        $this->inboundPayments = $inboundPayments;
        $this->inboundPayment = $inboundPayment;
        parent::__construct($inboundPayments);
    }

    public function create(
        Contract $contract,
        $amount,
        DateTime $date,
        $type,
        $chkDate = null,
        $chkBank = null,
        $chkBranch = null,
        $chkNumber = null,
        $cleared,
        $files = [],
        User $user = null

    ){
        $inboundPayment = new $this->inboundPayment;
        $inboundPayment->contract()->associate($contract);
        $inboundPayment->contract_id = $contract->id;
        $inboundPayment->amount = $amount;
        $inboundPayment->date = $date;
        $inboundPayment->type = $type;
        $inboundPayment->cleared = (bool)$cleared;
        if($type == "Check"){
            $inboundPayment->chk_date = $chkDate;
            $inboundPayment->chk_bank = $chkBank;
            $inboundPayment->chk_branch = $chkBranch;
            $inboundPayment->chk_number = $chkNumber;
        }
        if($user){
            $inboundPayment->setUpdatedByUser($user);
        }

        if(count($files)){
            foreach($files as $file){
                $inboundPayment->addMedia($file->getRealPath())
                    ->usingFileName($file->getClientOriginalName())->usingName($file->getClientOriginalName())->toMediaCollection();
            }
        }
        $this->inboundPayments->save($inboundPayment);
        $contract->updateBalance();
        return $inboundPayment;
    }

    public function update(
        InboundPayment $inboundPayment,
        Contract $contract,
        $amount,
        DateTime $date,
        $type,
        $chkDate = null,
        $chkBank = null,
        $chkBranch = null,
        $chkNumber = null,
        $cleared,
        $files = [],
        User $user = null
    ){
        $tmpInboundPayment = clone $inboundPayment;
        $tmpInboundPayment->contract()->associate($contract);
        $tmpInboundPayment->contract_id = $contract->id;
        $tmpInboundPayment->amount = $amount;
        $tmpInboundPayment->date = $date;
        $tmpInboundPayment->type = $type;
        $tmpInboundPayment->cleared = (bool)$cleared;
        if($type == "Check"){
            $tmpInboundPayment->chk_date = $chkDate;
            $tmpInboundPayment->chk_bank = $chkBank;
            $tmpInboundPayment->chk_branch = $chkBranch;
            $tmpInboundPayment->chk_number = $chkNumber;
        }
        if($user){
            $tmpInboundPayment->setUpdatedByUser($user);
        }
        if(count($files)){
            foreach($files as $file){
                $tmpInboundPayment->addMedia($file->getRealPath())
                    ->usingFileName($file->getClientOriginalName())->usingName($file->getClientOriginalName())->toMediaCollection();
            }
        }
        $this->inboundPayments->save($tmpInboundPayment);
        $contract->updateBalance();
        return $this->getById($inboundPayment->id);
    }

    public function delete(InboundPayment $inboundPayment)
    {
        // TODO delete checks
        $contract = $inboundPayment->contract;
        $this->inboundPayments->delete($inboundPayment);
        $contract->updateBalance();
        return $inboundPayment;
    }
}
