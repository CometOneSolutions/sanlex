<?php

namespace App\Modules\InboundPayment\Services;

use App\Modules\Contract\Models\Contract;
use App\Modules\InboundPayment\Models\InboundPayment;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordService;
use DateTime;

interface InboundPaymentRecordService extends RecordService
{
    public function create(
        Contract $contract,
        $amount,
        DateTime $date,
        $type,
        $chkDate = null,
        $chkBank = null,
        $chkBranch = null,
        $chkNumber = null,
        $cleared,
        $files,
        User $user = null
    );

    public function update(
        InboundPayment $inboundPayment,
        Contract $contract,
        $amount,
        DateTime $date,
        $type,
        $chkDate = null,
        $chkBank = null,
        $chkBranch = null,
        $chkNumber = null,
        $cleared,
        $files,
        User $user = null
    );

    public function delete(InboundPayment $inboundPayment);

}
