<?php

namespace App\Modules\InboundPayment\Controllers;

use App\Modules\InboundPayment\Services\InboundPaymentRecordService;
use App\Modules\InboundPayment\Transformer\InboundPaymentTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\InboundPayment\Requests\StoreInboundPayment;
use App\Modules\InboundPayment\Requests\UpdateInboundPayment;
use App\Modules\InboundPayment\Requests\DestroyInboundPayment;


class InboundPaymentApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;
    protected $contactRecordService;

    public function __construct(
        InboundPaymentRecordService $inboundPaymentRecordService,
        InboundPaymentTransformer $transformer
    ) {
        $this->service = $inboundPaymentRecordService;
        $this->transformer = $transformer;
        parent::__construct($inboundPaymentRecordService, $transformer);
        $this->middleware(['auth:api']);
    }

    public function destroy($inboundPaymentId, DestroyInboundPayment $request)
    {
        $inboundPayment = \DB::transaction(function () use ($inboundPaymentId) {
            $inboundPayment = $this->service->getById($inboundPaymentId);
            $this->service->delete($inboundPayment);
            return $inboundPayment;
        });
        return $this->response->item($inboundPayment, $this->transformer)->setStatusCode(200);
    }

    public function store(StoreInboundPayment $request)
    {
        $inboundPayment = \DB::transaction(function () use ($request) {
            return $this->service->create(
                $request->getContract(),
                $request->getAmount(),
                $request->getDate(),
                $request->getType(),
                $request->getCheckDate(),
                $request->getCheckBank(),
                $request->getCheckBranch(),
                $request->getCheckNumber(),
                $request->getCleared(),
                $request->getFiles(),
                $request->user()
            );

        });
        return $this->response->item($inboundPayment, $this->transformer)->setStatusCode(201);
    }

    public function update($inboundPaymentId, UpdateInboundPayment $request)
    {

        $inboundPayment = \DB::transaction(function () use ($request, $inboundPaymentId) {
            $inboundPayment = $this->service->getById($inboundPaymentId);
            return $this->service->update(
                $inboundPayment,
                $request->getContract(),
                $request->getAmount(),
                $request->getDate(),
                $request->getType(),
                $request->getCheckDate(),
                $request->getCheckBank(),
                $request->getCheckBranch(),
                $request->getCheckNumber(),
                $request->getCleared(),
                $request->getFiles(),
                $request->user()
            );
        });
        return $this->response->item($inboundPayment, $this->transformer)->setStatusCode(200);
    }


}
