<?php

namespace App\Modules\InboundPayment\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use Illuminate\Http\Request;
use JavaScript;
use App\Modules\InboundPayment\Services\InboundPaymentRecordService;

class InboundPaymentController extends Controller
{
    private $service;

    public function __construct(InboundPaymentRecordService $inboundPaymentRecordService)
    {
        $this->service = $inboundPaymentRecordService;
        $this->middleware('permission:Read [COL] Inbound Payment')->only(['show', 'index']);
        $this->middleware('permission:Create [COL] Inbound Payment')->only('create');
        $this->middleware('permission:Update [COL] Inbound Payment')->only('edit');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'date',
            'sortAscending' => true,
            'baseUrl' => '/api/inbound-payments?include=contract.customer'
        ]);
        return view("inbound-payments.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        JavaScript::put(['id' => null]);
        return view("inbound-payments.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $inboundPayment = $this->service->getById($id);
        return view("inbound-payments.show", compact('inboundPayment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $inboundPayment = $this->service->getById($id);
        return view("inbound-payments.edit", compact('inboundPayment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getDisplayedFilterable()
    {
        return [
            ["id" => "name", "text" => "Name"]
        ];
    }
}
