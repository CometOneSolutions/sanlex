import {Form} from '@c1_common_js/components/Form';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm';
import Select3 from "@c1_common_js/components/Select3";
import Datepicker from 'vuejs-datepicker';

new Vue({
    el: '#inbound-payment',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select3, Datepicker
    },
    data: {
        form: new Form({
            id: null,
            contractId: null,
            amount: 0,
            date: moment(),
            type: null,
            chkDate: moment(),
            chkBank: null,
            chkBranch: null,
            chkAccount: null,
            chkNumber: null,
            cleared: false,
        }),
        customerId: null,
        customerUrl: `/api/customers`,
        isSaving: false,
        isBusy: false,
        isDeleting: false,
        isBusyDeleting: false,
        dataInitialized: true,
        contractUrl: null,
        files: [],
        defaultSelectedContract: {},
        defaultSelectedCustomer: {},
        paymentTypes: ['Cash', 'Check', 'Transfer'],
        customerSelected: false,
    },
    watch: {
        initializationComplete(val) {    
            this.form.isInitializing = false;
        },
        customerId(val){
            if(val){
                if(this.form.id){
                    this.setCustomerSelectedFalse().then(() => {
                        this.contractUrl = `/api/contracts?customer_id=${val}&hasBalanceAndIncludeSelected=${this.form.contractId}`;
                        this.customerSelected = true;
                    });
                }else{
                    this.setCustomerSelectedFalse().then(() => {
                        this.contractUrl = `/api/contracts?hasBalance=1&customer_id=${val}`;
                        this.customerSelected = true;
                    });
                }
            }else{
                this.customerSelected = false;
            }
        }
    },

    computed: {
        initializationComplete() {
            if (this.dataInitialized)
            {
                this.form.isInitializing = false;
                return true;
            }
            return false;
        }
        
    },
    methods: {
        setCustomerSelectedFalse(){
            return new Promise((resolve,reject) => {
                this.customerSelected = false;
                resolve();
            });
        },
        deleteFile(id) {  
            this.form.confirm().then((result) => {
                 if(result.value) {
                     this.isDeleting = true;
                     this.isBusyDeleting = true;
                     this.form.delete('/files/' + id)
                     .then(response => {
                         this.isDeleting = false;
                         this.isDeleting = false;
                        this.reloadData()
                     }).catch(error => {
                         this.isDeleting = false;
                         this.isBusyDeleting = false;
                     });
                    
                 }
                 
             });             
         },
        destroy() {  
           this.form.confirm().then((result) => {
                if(result.value) {
                    this.isDeleting = true;
                    this.isBusyDeleting = true;
                    this.form.delete('/api/inbound-payments/' + this.form.id)
                    .then(response => {
                        this.isDeleting = false;
                        this.isDeleting = false;
                        this.$swal({
                            title: 'Success',
                            text: 'Payment was removed.',
                            type: 'success'
                        }).then(() => window.location = '/collections/inbound-payments/');
                    }).catch(error => {
                        this.isDeleting = false;
                        this.isBusyDeleting = false;
                    });
                   
                }
                
            });             
        },
        
        store() {
            this.isSaving = true;
            this.isBusy = true;
            this.arrangeFiles().then(result => {
                let formData = result;
                formData.append('contractId', this.form.contractId);
                formData.append('amount', this.form.amount);
                formData.append('date', this.form.date);
                formData.append('type', this.form.type);
                formData.append('chkDate', this.form.chkDate);
                formData.append('chkBank', this.form.chkBank);
                formData.append('chkBranch', this.form.chkBranch);
                formData.append('chkAccount', this.form.chkAccount);
                formData.append('chkNumber', this.form.chkNumber);
                formData.append('cleared', this.form.cleared);
                this.form.submitImage('/api/inbound-payments', formData, 'post')
                .then(response => {
                    this.isSaving = false;
                    this.isBusy = false;
                    this.$swal({
                        title: 'Success',
                        text: 'Payment was saved.',
                        type: 'success'
                    });
                    
                    this.form.reset();
                })
                .catch(error => {
                    console.log(error.message);
                    this.isSaving = false;
                    this.isBusy = false;
                });
            }).catch(() => {
                this.isSaving = false;
                this.isBusy = false;
                this.$swal({
                    title: 'File too large',
                    text: 'maximum of 10MB only',
                    type: 'error',
                    confirmButtonColor: '#20a8d8',
                });
            });
        },

        uploadFile (event) {
            this.files = event.target.files
        },

        arrangeFiles(){
            return new Promise((resolve,reject) => {
                let formData = new FormData();
                this.files.forEach(file => {
                    if(file.size > 1024 * 1024 * 10){
                        reject();
                    }
                    formData.append('files[]', file);
                });
                resolve(formData);
            });
        },

        update() {
            this.isSaving = true;
            this.isBusy = true;
            this.arrangeFiles().then(result =>{
                let formData = result;
                formData.append('contractId', this.form.contractId);
                formData.append('amount', this.form.amount);
                formData.append('date', this.form.date);
                formData.append('type', this.form.type);
                formData.append('chkDate', this.form.chkDate);
                formData.append('chkBank', this.form.chkBank);
                formData.append('chkBranch', this.form.chkBranch);
                formData.append('chkAccount', this.form.chkAccount);
                formData.append('chkNumber', this.form.chkNumber);
                formData.append('cleared', this.form.cleared);
                this.form.submitImage(`/api/inbound-payments/${this.form.id}`, formData, 'post')
                .then(response => {
                    this.isSaving = false;
                    this.isBusy = false;
                    this.$swal({
                        title: 'Success',
                        text: 'Payment was updated.',
                        type: 'success',
                        confirmButtonColor: '#20a8d8',
                    }).then(data => { 
                        
                        window.location = '/collections/inbound-payments/' + this.form.id
                    });  
                }).catch(error => {
                    console.log(error);
                    this.isSaving = false;
                    this.isBusy = false;
                });
            }).catch(() => {
                this.isSaving = false;
                this.isBusy = false;
                this.$swal({
                    title: 'File too large',
                    text: 'maximum of 10MB only',
                    type: 'error',
                    confirmButtonColor: '#20a8d8',
                });
            });
           
        },
       
        loadData(data) {
            this.form = new Form(data);
        },
        reloadData(){
            this.dataInitialized = false;
            this.form.get('/api/inbound-payments/' + this.form.id + '?include=files,contract.customer')
            .then(response => {
                this.loadData(response.data);
                this.contractUrl = `/api/contracts?hasBalanceAndIncludeSelected=${response.data.contractId}`;
                this.defaultSelectedCustomer = response.data.contract.data.customer.data;
                this.defaultSelectedContract = response.data.contract.data;
                this.dataInitialized = true;
            });
        },
    },
    created() {
        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/inbound-payments/' + id + '?include=files,contract.customer')
            .then(response => {
                this.loadData(response.data);
                this.contractUrl = `/api/contracts?hasBalanceAndIncludeSelected=${response.data.contractId}`;
                this.defaultSelectedCustomer = response.data.contract.data.customer.data;
                this.defaultSelectedContract = response.data.contract.data;
                this.dataInitialized = true;
            });
        }
        
    },
    mounted() {
    
        console.log("Init contract script...");
    }
});