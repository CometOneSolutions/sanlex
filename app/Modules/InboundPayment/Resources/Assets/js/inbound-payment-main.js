import {Form} from '@c1_common_js/components/Form';

export const contractService = new Vue({
       
    data: function() {
        return {
            form: new Form({}),
            uri: {
                inboundPayment: '/api/inbound-payments?limit=' + Number.MAX_SAFE_INTEGER,
            }      
        }
    },   

    methods: {
        getInboundPayments(){
            return this.form.get(this.uri.inboundPayment);
        },
    }
});

