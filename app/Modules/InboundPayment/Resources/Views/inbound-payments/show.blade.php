@extends('app')
@section('breadcrumbs', Breadcrumbs::render('inbound-payment.collection.show', $inboundPayment))
@section('content')
    <section id="inbound-payment" v-cloak>
        <div>
            <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                <div class="col-12 text-center h4">
                    <i class="fas fa-cog fa-spin"></i> Initializing
                </div>
            </div>
            <div v-else>
                <div class="card">
                    <div class="card-header">
                        Inbound Payment Record
                        @if (auth()->user()->can('Update [COL] Inbound Payment'))
                            <div class="float-right">
                                <a :href="form.editUri" class="btn btn-sm btn-primary" role="button"><i
                                        class="fas fa-pencil-alt"></i></a>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <form>
                            @include("inbound-payments._list")
                        </form>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <timestamp :name="form.updatedByUser.data.name || 'System'" :time="form.updatedAt"></timestamp>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </section>
@endsection
@push('scripts')
    <script src="{{ mix('js/inbound-payment.js') }}"></script>
@endpush
