@extends('app')
@section('breadcrumbs', Breadcrumbs::render('inbound-payment.collection.create'))
@section('content')
    <section id="inbound-payment">
        <div class="card table-responsive">
            <div class="card-header">
                Create New Inbound Payment
            </div>
            <v-form @validate="store">
                <div class="card-body">

                    @include("inbound-payments._form")

                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <button class="btn btn-success btn-sm" type="submit" :disabled="isBusy">
                            <div v-if="isSaving">
                                <i class="fas fa-spinner fa-pulse"></i> Saving
                            </div>
                            <div v-if="!isSaving">
                                Create
                            </div>
                        </button>
                    </div>


                </div>
            </v-form>
        </div>
    </section>
@endsection
