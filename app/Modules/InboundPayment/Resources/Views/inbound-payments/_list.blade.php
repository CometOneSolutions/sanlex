<div class="form-row">
    <div class="form-group col-md-6">
        <label for="name" class="col-form-label">Contract</label>
        <p><a :href="form.contract.data.showUri">@{{ form . contract . data . text }}</a></p>
    </div>
    <div class="form-group col-md-6">
        <label for="contactPerson" class="col-form-label">Date</label>
        <p>@{{ form . date }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="telephone" class="col-form-label">Payment Method</label>
        <p>@{{ form . type }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="mobile" class="col-form-label">Amount</label>
        <p>@{{ (form . amount) | numeric }}</p>
    </div>

    <div v-if="form.type == 'Check'" class="form-group col-md-6">
        <label for="email" class="col-form-label">Check Date</label>
        <p>@{{ form . chkDate }}</p>
    </div>

    <div v-if="form.type == 'Check'" class="form-group col-md-6">
        <label for="cleared" class="col-form-label">Cleared?</label>
        <p v-if="form.cleared">Yes</p>
        <p v-else>No</p>
    </div>

    <div v-if="form.type == 'Check'" class="form-group col-md-6">
        <label for="address" class="col-form-label">Bank</label>
        <p>@{{ form . chkBank }}</p>
    </div>

    <div v-if="form.type == 'Check'" class="form-group col-md-6">
        <label for="address" class="col-form-label">Branch</label>
        <p>@{{ form . chkBranch }}</p>
    </div>

    <div v-if="form.type == 'Check'" class="form-group col-md-6">
        <label for="address" class="col-form-label">Number</label>
        <p>@{{ form . chkNumber }}</p>
    </div>
</div>

<div class="col-12" v-if="form.files.data.length > 0">
    <table class="table table-simple">
        <thead>
            <tr>
                <th>Name</th>
                <th class="text-right">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="file in form.files.data">
                <td>@{{ file . name }}</td>
                <td class="text-right">
                    <a target="_blank" :href="file.viewUrl">
                        <button type="button" class="btn btn-sm btn-primary">
                            <i class="far fa-eye"></i>
                        </button>
                    </a>
                    <a target="_blank" :href="file.downloadUrl">
                        <button type="button" class="btn btn-sm btn-secondary">
                            <i class="fas fa-download"></i>
                        </button>
                    </a>
                    <button type="button" @click="deleteFile(file.id)" class="btn btn-sm btn-danger">
                        <i class="fas fa-trash"></i>
                    </button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
