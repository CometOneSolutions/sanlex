@extends('app')
@section('breadcrumbs', Breadcrumbs::render('inbound-payment.collection'))
@section('content')
    <div id="index" v-cloak>
        <div class="card">
            <div class="card-header">
                Inbound Payments
                @if (auth()->user()->can('Create [COL] Inbound Payment'))
                    <div class="float-right">
                        <a href="{{ route('inbound-payment_create') }}"><button type="button"
                                class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
                    </div>
                @endif
            </div>
            <div class="card-body">
                <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending"
                    v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                    <table class="table table-responsive-sm">
                        <thead>
                            <tr>
                                <th>
                                    <a v-on:click="setSorter('date')">
                                        Date <i class="fa" :class="getSortIcon('date')"></i>
                                    </a>
                                </th>
                                <th>
                                    <a v-on:click="setSorter('contract_number')">
                                        Contract <i class="fa" :class="getSortIcon('contract_number')"></i>
                                    </a>
                                </th>
                                <th>
                                    Customer
                                </th>
                                <th>
                                    <a v-on:click="setSorter('chk_date')">
                                        Check Date <i class="fa" :class="getSortIcon('chk_date')"></i>
                                    </a>
                                </th>
                                <th>
                                    <a v-on:click="setSorter('chk_bank')">
                                        Check Bank <i class="fa" :class="getSortIcon('chk_bank')"></i>
                                    </a>
                                </th>
                                <th>
                                    <a v-on:click="setSorter('chk_branch')">
                                        Check Branch <i class="fa" :class="getSortIcon('chk_branch')"></i>
                                    </a>
                                </th>
                                <th>
                                    <a v-on:click="setSorter('chk_number')">
                                        Check Number <i class="fa" :class="getSortIcon('chk_number')"></i>
                                    </a>
                                </th>
                                <th>
                                    <a v-on:click="setSorter('cleared')">
                                        Cleared <i class="fa" :class="getSortIcon('cleared')"></i>
                                    </a>
                                </th>
                                <th class="text-right">
                                    <a v-on:click="setSorter('amount')"> Amount <i class="fa"
                                            :class="getSortIcon('amount')"></i>
                                    </a>
                                </th>
                                <th>
                                    <a v-on:click="setSorter('type')"> Type <i class="fa"
                                            :class="getSortIcon('type')"></i>
                                    </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item in items" v-if="!isLoading">
                                <td><a :href="item.showUri">@{{ item . date }}</a></td>
                                <td><a :href="item.contract.data.showUri">@{{ item . contract . data . indexText }}</a>
                                </td>
                                <td><a
                                        :href="item.contract.data.customer.data.showUri">@{{ item . contract . data . customer . data . text }}</a>
                                </td>
                                <td>@{{ item . chkDate || '--' }}</td>
                                <td>@{{ item . chkBank || '--' }}</td>
                                <td>@{{ item . chkBranch || '--' }}</td>
                                <td>@{{ item . chkNumber || '--' }}</td>
                                <td>@{{ item . type == 'Check' ? (item . cleared ? 'Yes' : 'No') : '--' }}</td>
                                <td class="text-right">@{{ (item . amount) | numeric }}</td>
                                <td>@{{ item . type }}</td>

                            </tr>
                        </tbody>
                    </table>
                </index>
            </div>
        </div>
    </div>
    <!-- End Watchlist-->
@stop
@push('scripts')
    <script src="{{ mix('js/index.js') }}"></script>
@endpush
