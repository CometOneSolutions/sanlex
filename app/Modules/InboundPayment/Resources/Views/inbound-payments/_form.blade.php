<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="form-group">
    <label for="inputEmail4" class="col-form-label">Customer <span class="text text-danger">*</span></label>
    <select3 class="custom-select" required data-msg-required="Enter Customer" v-model="customerId" search="name"
        :selected="defaultSelectedCustomer" :disabled="form.id" :url="customerUrl">
    </select3>
</div>
<div class="form-group">
    <label for="inputEmail4" class="col-form-label">Contract <span class="text text-danger">*</span></label>
    <input class="form-control" v-if="!customerSelected" type="text" disabled>
    <select3 v-else class="custom-select" required data-msg-required="Enter Contract" v-model="form.contractId"
        search="number" :selected="defaultSelectedContract" :url="contractUrl">
    </select3>
</div>

<div class="form-group">
    <label for="date">Date <span class="text text-danger">*</span></label>
    <datepicker :typeable="true" :input-class="{'is-invalid': form.errors.has('date'),'form-control': true}"
        v-model="form.date" :required="true"></datepicker>
</div>

<div class="form-group">
    <label for="inputEmail4" class="col-form-label">Payment Method <span class="text text-danger">*</span></label>
    <select3 class="custom-select" required data-msg-required="Enter Payment Method" v-model="form.type">
        <option v-for="type in paymentTypes" :value="type">@{{ type }}</option>
    </select3>
</div>

<div class="form-group">
    <label for="amount" class="col-form-label">Amount</label>
    <money class="form-control text-right" v-model.number="form.amount"></money>
</div>

<div class="form-group">
    <label for="Files" class="col-form-label">Files</label>
    <input type="file" @change="uploadFile" multiple>
</div>

<div v-if="form.type == 'Check'">
    <div class="form-group">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" v-model="form.cleared" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">
                Cleared
            </label>
        </div>
    </div>
    <div class="form-group">
        <label for="chkDate">Check Date <span class="text text-danger">*</span></label>
        <datepicker :typeable="true" :input-class="{'is-invalid': form.errors.has('chkDate'),'form-control': true}"
            v-model="form.chkDate" :required="true"></datepicker>
    </div>

    <div class="form-group">
        <label for="chkBank" class="col-form-label">Bank</label>
        <input type="text" class="form-control" placeholder="" v-model="form.chkBank"
            :class="{'is-invalid': form.errors.has('chkBank') }" data-msg-required="Enter Bank">
    </div>
    <div class="form-group">
        <label for="chkBranch" class="col-form-label">Branch</label>
        <input type="text" class="form-control" placeholder="" v-model="form.chkBranch"
            :class="{'is-invalid': form.errors.has('chkBranch') }" data-msg-required="Enter Branch">
    </div>

    <div class="form-group">
        <label for="chkNumber" class="col-form-label">Check Number</label>
        <input type="text" class="form-control" placeholder="" v-model="form.chkNumber"
            :class="{'is-invalid': form.errors.has('chkNumber') }" data-msg-required="Enter Number">
    </div>
</div>


@push('scripts')
    <script src="{{ mix('js/inbound-payment.js') }}"></script>
@endpush
