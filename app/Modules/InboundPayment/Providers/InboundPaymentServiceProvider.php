<?php

namespace App\Modules\InboundPayment\Providers;

use App\Modules\InboundPayment\Models\InboundPayment;
use App\Modules\InboundPayment\Models\InboundPaymentModel;
use App\Modules\InboundPayment\Repositories\InboundPaymentRepository;
use App\Modules\InboundPayment\Repositories\EloquentInboundPaymentRepository;
use App\Modules\InboundPayment\Services\InboundPaymentRecordService;
use App\Modules\InboundPayment\Services\InboundPaymentRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class InboundPaymentServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/InboundPayment/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(InboundPaymentRecordService::class, InboundPaymentRecordServiceImpl::class);
        $this->app->bind(InboundPaymentRepository::class, EloquentInboundPaymentRepository::class);
        $this->app->bind(InboundPayment::class, InboundPaymentModel::class);
    }
}
