<?php

namespace App\Modules\InboundPayment\Transformer;

use App\Modules\Contract\Transformer\ContractTransformer;
use App\Modules\Contract\Transformer\FileTransformer;
use App\Modules\InboundPayment\Models\InboundPayment;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;


class InboundPaymentTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'contract', 'files'
    ];
    public function __construct()
    {
        $this->defaultIncludes = array_merge($this->defaultIncludes, []);
    }
    public function transform(InboundPayment $inboundPayment)
    {
        return [
            'id' => (int) $inboundPayment->id,
            'contractId' => $inboundPayment->contract_id,
            'date' => $inboundPayment->date->toDateString(),
            'type' => $inboundPayment->type,
            'amount' => $inboundPayment->amount,
            'chkDate' => $inboundPayment->chk_date,
            'chkBank' => $inboundPayment->chk_bank,
            'chkBranch' => $inboundPayment->chk_branch,
            'chkNumber' => $inboundPayment->chk_number,
            'account' => $inboundPayment->account,
            'cleared' => $inboundPayment->cleared,
            'updatedAt' => $inboundPayment->updated_at,
            'editUri' => route('inbound-payment_edit', $inboundPayment->id),
            'showUri' => route('inbound-payment_show', $inboundPayment->id),
        ];
    }

    public function includeContract(InboundPayment $inboundPayment)
    {
        return $this->item($inboundPayment->contract, new ContractTransformer);
    }

    public function includeFiles(InboundPayment $inboundPayment)
    {
        return $this->collection($inboundPayment->getMedia(), new FileTransformer);
    }
}