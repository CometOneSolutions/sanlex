<?php

namespace App\Modules\InboundPayment\Models;

use App\Modules\Contract\Models\ContractModel;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class InboundPaymentModel extends Model implements InboundPayment, UpdatableByUser, HasMedia
{
    use HasUpdatedByUser;
    use HasMediaTrait;

    protected $table = 'inbound_payments';

    protected $dates = ['date'];

    public function getId()
    {
        return $this->id;
    }

    public function contract()
    {
        return $this->belongsTo(ContractModel::class, 'contract_id');
    }

}
