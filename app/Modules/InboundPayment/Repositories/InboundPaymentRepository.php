<?php

namespace App\Modules\InboundPayment\Repositories;

use App\Modules\InboundPayment\Models\InboundPayment;
use CometOneSolutions\Common\Repositories\Repository;

interface InboundPaymentRepository extends Repository
{
    public function save(InboundPayment $inboundPayment);
    public function delete(InboundPayment $inboundPayment);
}
