<?php

namespace App\Modules\InboundPayment\Repositories;

use App\Modules\InboundPayment\Models\InboundPayment;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentInboundPaymentRepository extends EloquentRepository implements InboundPaymentRepository
{
    public function __construct(InboundPayment $inboundPayment)
    {
        parent::__construct($inboundPayment);
    }

    public function save(InboundPayment $inboundPayment)
    {
        return $inboundPayment->save();
    }

    public function delete(InboundPayment $inboundPayment)
    {
        return $inboundPayment->delete();
    }

    public function filterByContractNumber($value)
    {
        return $this->model->whereHas('contract', function($contract) use ($value){
            return $contract->where('number', $value);
        });
    }

    public function filterByCustomerName($value)
    {
        return $this->model->whereHas('contract', function($contract) use ($value){
            return $contract->whereHas('customer', function($customer) use ($value){
                return $customer->where('name', 'like', "%$value%");
            });
        });
    }

    protected function orderbyContractNumber($direction)
    {
        return $this->model->join('contracts', 'contracts.id', 'inbound_payments.contract_id')
            ->select('contracts.number as customer_number', 'inbound_payments.*')
            ->orderBy('customer_number', $direction);
    }


}
