<?php

namespace App\Modules\ReceivingAccessory\Transformer;

use App\Modules\PurchaseOrderAccessory\Transformer\PurchaseOrderAccessoryTransformer;
use App\Modules\ReceivingAccessory\Models\ReceivingAccessory;
use App\Modules\ReceivingAccessoryDetail\Transformer\ReceivingAccessoryDetailTransformer;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class ReceivingAccessoryTransformer extends UpdatableByUserTransformer
{
	protected $availableIncludes = [
		'purchaseOrderAccessory',
		'receivingAccessoryDetails'
	];

	public function __construct()
	{
		$this->defaultIncludes = array_merge($this->defaultIncludes, []);
	}

	public function transform(ReceivingAccessory $receivingAccessory)
	{
		return [
			'id'						=> (int) $receivingAccessory->getId(),
			'referenceNo'				=> $receivingAccessory->getReferenceNo(),
			'date'						=> $receivingAccessory->getDate()->toDateString(),
			'purchaseOrderAccessoryId'  => $receivingAccessory->getPurchaseOrderAccessoryId(),
			'status'					=> $receivingAccessory->getStatus(),
			'remarks'					=> $receivingAccessory->getRemarks(),
			'isInTransit'				=> $receivingAccessory->isInTransit(),
			'isPurchaseOrderAccessoryOpen' => $receivingAccessory->isPurchaseOrderAccessoryOpen(),
			'showUri'					=> route('receiving-accessory_show', $receivingAccessory->id),
			'editUri'					=> route('receiving-accessory_edit', $receivingAccessory->id),
			'updatedAt'					=> $receivingAccessory->updated_at,
		];
	}

	public function includePurchaseOrderAccessory(ReceivingAccessory $receivingAccessory)
	{
		$purchaseOrderAccessory = $receivingAccessory->getPurchaseOrderAccessory();
		return $this->item($purchaseOrderAccessory, new PurchaseOrderAccessoryTransformer);
	}

	public function includeReceivingAccessoryDetails(ReceivingAccessory $receivingAccessory)
	{
		$receivingAccessoryDetails = $receivingAccessory->getReceivingAccessoryDetails();
		return $this->collection($receivingAccessoryDetails, new ReceivingAccessoryDetailTransformer);
	}
}
