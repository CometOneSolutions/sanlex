<?php

namespace App\Modules\ReceivingAccessory\Services;

use App\Modules\Inventory\Services\InventoryRecordService;
use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;
use App\Modules\PurchaseOrderAccessory\Services\PurchaseOrderAccessoryRecordService;
use App\Modules\ReceivingAccessory\Models\ReceivingAccessory;
use App\Modules\ReceivingAccessory\Repositories\ReceivingAccessoryRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;
use DateTime;

class ReceivingAccessoryRecordServiceImpl extends RecordServiceImpl implements ReceivingAccessoryRecordService
{
	private $receivingAccessory;
	private $receivingAccessories;
	private $inventoryRecordService;
	private $purchaseOrderAccessoryRecordService;


	protected $availableFilters = [
		['id' => 'reference_no', 'text' => 'Reference No.'],
		['id' => 'purchase_order_accessory_number', 'text' => 'Purchase Order Accessory No.'],
		['id' => 'date', 'text' => 'Date'],
		['id' => 'status', 'text' => 'Status'],
	];
	public function __construct(
		ReceivingAccessoryRepository $receivingAccessories,
		ReceivingAccessory $receivingAccessory,
		InventoryRecordService $inventoryRecordService,
		PurchaseOrderAccessoryRecordService $purchaseOrderAccessoryRecordService
	) {
		$this->receivingAccessories = $receivingAccessories;
		$this->receivingAccessory = $receivingAccessory;
		$this->inventoryRecordService = $inventoryRecordService;
		$this->purchaseOrderAccessoryRecordService = $purchaseOrderAccessoryRecordService;
		parent::__construct($receivingAccessories);
	}

	public function create(
		$referenceNo,
		DateTime $date,
		$remarks,
		PurchaseOrderAccessory $purchaseOrderAccessory,
		array $receivingAccessoryDetails,
		User $user = null
	) {
		$receivingAccessory = $this->receivingAccessory;
		$receivingAccessory->setReferenceNo($referenceNo);
		$receivingAccessory->setDate($date);
		$receivingAccessory->setRemarks($remarks);
		$receivingAccessory->setStatus(ReceivingAccessory::STATUS_IN_TRANSIT);
		$receivingAccessory->setPurchaseOrderAccessory($purchaseOrderAccessory);
		$receivingAccessory->setReceivingAccessoryDetails($receivingAccessoryDetails);

		if ($user) {
			$receivingAccessory->setUpdatedByUser($user);
		}

		$this->receivingAccessories->save($receivingAccessory);

		return $receivingAccessory;
	}

	public function update(
		ReceivingAccessory $receivingAccessory,
		$referenceNo,
		DateTime $date,
		$remarks,
		PurchaseOrderAccessory $purchaseOrderAccessory,
		array $receivingAccessoryDetails,
		User $user = null
	) {
		$tmpReceivingAccessory = clone $receivingAccessory;
		$tmpReceivingAccessory->setReferenceNo($referenceNo);
		$tmpReceivingAccessory->setDate($date);
		$tmpReceivingAccessory->setRemarks($remarks);
		$tmpReceivingAccessory->setPurchaseOrderAccessory($purchaseOrderAccessory);
		$tmpReceivingAccessory->setReceivingAccessoryDetails($receivingAccessoryDetails);

		if ($user) {
			$tmpReceivingAccessory->setUpdatedByUser($user);
		}

		$this->receivingAccessories->save($tmpReceivingAccessory);

		return $tmpReceivingAccessory;
	}

	public function receive(
		ReceivingAccessory $receivingAccessory,
		User $user = null
	) {
		$tmpReceivingAccessory = clone $receivingAccessory;
		if ($user) {
			$tmpReceivingAccessory->setUpdatedByUser($user);
			$tmpReceivingAccessory->setStatus(ReceivingAccessory::STATUS_RECEIVED);
		}

		$this->receivingAccessories->save($tmpReceivingAccessory);

		$this->inventoryRecordService->createInventoryFromReceivingAccessory($tmpReceivingAccessory, $user);
		$this->purchaseOrderAccessoryRecordService->closeIfQuantitiesAreFulfilled($receivingAccessory);
		return $this->getById($receivingAccessory->getId());
	}



	public function delete(ReceivingAccessory $receivingAccessory)
	{
		$this->receivingAccessories->delete($receivingAccessory);
		return $receivingAccessory;
	}
}
