<?php

namespace App\Modules\ReceivingAccessory\Services;

use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;
use App\Modules\ReceivingAccessory\Models\ReceivingAccessory;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;
use DateTime;

interface ReceivingAccessoryRecordService extends RecordService
{
	public function create(
		$referenceNo,
		DateTime $date,
		$remarks,
		PurchaseOrderAccessory $purchaseOrderAccessory,
		array $receivingAccessoryDetails,
		User $user = null
	);

	public function update(
		ReceivingAccessory $receivingAccessory,
		$referenceNo,
		DateTime $date,
		$remarks,
		PurchaseOrderAccessory $purchaseOrderAccessory,
		array $receivingAccessoryDetails,
		User $user = null
	);

	public function delete(ReceivingAccessory $receivingAccessory);
}
