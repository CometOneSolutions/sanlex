<?php

Breadcrumbs::register('receiving-accessory.index', function ($breadcrumbs) {
	$breadcrumbs->parent('purchasing-menu-dashboard.index');
	$breadcrumbs->push('Receiving Accessories', route('receiving-accessory_index'));
});
Breadcrumbs::register('receiving-accessory.create', function ($breadcrumbs) {
	$breadcrumbs->parent('receiving-accessory.index');
	$breadcrumbs->push('Create', route('receiving-accessory_create'));
});
Breadcrumbs::register('receiving-accessory.show', function ($breadcrumbs, $receivingAccessory) {
	$breadcrumbs->parent('receiving-accessory.index');
	$breadcrumbs->push($receivingAccessory->getReferenceNo(), route('receiving-accessory_show', $receivingAccessory->getId()));
});
Breadcrumbs::register('receiving-accessory.edit', function ($breadcrumbs, $receivingAccessory) {
	$breadcrumbs->parent('receiving-accessory.show', $receivingAccessory);
	$breadcrumbs->push('Edit', route('receiving-accessory_edit', $receivingAccessory->getId()));
});
