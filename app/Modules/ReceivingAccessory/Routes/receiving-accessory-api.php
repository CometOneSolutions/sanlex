<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'receiving-accessories'], function () use ($api) {
		$api->get('/', 'App\Modules\ReceivingAccessory\Controllers\ReceivingAccessoryApiController@index');
		$api->get('{receivingAccessoryId}', 'App\Modules\ReceivingAccessory\Controllers\ReceivingAccessoryApiController@show');
		$api->post('/', 'App\Modules\ReceivingAccessory\Controllers\ReceivingAccessoryApiController@store');
		$api->patch('{receivingAccessoryId}', 'App\Modules\ReceivingAccessory\Controllers\ReceivingAccessoryApiController@update');
		$api->patch('receive/{receivingAccessoryId}', 'App\Modules\ReceivingAccessory\Controllers\ReceivingAccessoryApiController@receive');
		$api->delete('{receivingAccessoryId}', 'App\Modules\ReceivingAccessory\Controllers\ReceivingAccessoryApiController@destroy');
		$api->post('{receivingAccessoryId}/upload', 'App\Modules\ReceivingAccessory\Controllers\ReceivingAccessoryApiController@import');
	});
});
