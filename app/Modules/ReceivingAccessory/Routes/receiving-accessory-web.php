<?php

Route::group(['prefix' => 'purchasing'], function () {
    Route::group(['prefix' => 'receiving-accessories'], function () {
        Route::get('/', 'ReceivingAccessoryController@index')->name('receiving-accessory_index');
        Route::get('/create', 'ReceivingAccessoryController@create')->name('receiving-accessory_create');
        Route::get('{receivingAccessoryId}/edit', 'ReceivingAccessoryController@edit')->name('receiving-accessory_edit');
        Route::get('{receivingAccessoryId}/print', 'ReceivingAccessoryController@print')->name('receiving-accessory_print');
        Route::get('{receivingAccessoryId}', 'ReceivingAccessoryController@show')->name('receiving-accessory_show');
    });
});
