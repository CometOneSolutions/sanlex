<?php

namespace App\Modules\ReceivingAccessory\Requests;

use Dingo\Api\Http\FormRequest;

class StoreReceivingAccessory extends ReceivingAccessoryRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [PUR] Receiving Accessory');
	}
}
