<?php

namespace App\Modules\ReceivingAccessory\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyReceivingAccessory extends ReceivingAccessoryRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [PUR] Receiving Accessory');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
