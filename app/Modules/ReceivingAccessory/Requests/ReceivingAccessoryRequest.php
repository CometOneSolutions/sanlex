<?php

namespace App\Modules\ReceivingAccessory\Requests;

use DateTime;
use Dingo\Api\Http\FormRequest;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetailModel;
use App\Modules\PurchaseOrderAccessory\Services\PurchaseOrderAccessoryRecordService;
use App\Modules\ReceivingAccessoryDetail\Services\ReceivingAccessoryDetailRecordService;

class ReceivingAccessoryRequest extends FormRequest
{

	protected $purchaseOrderAccessoryRecordService;
	protected $receivingAccessoryDetailRecordService;
	protected $productRecordService;

	public function __construct(
		PurchaseOrderAccessoryRecordService $purchaseOrderAccessoryRecordService,
		ReceivingAccessoryDetailRecordService $receivingAccessoryDetailRecordService,
		ProductRecordService $productRecordService
	) {
		$this->purchaseOrderAccessoryRecordService = $purchaseOrderAccessoryRecordService;
		$this->receivingAccessoryDetailRecordService = $receivingAccessoryDetailRecordService;
		$this->productRecordService = $productRecordService;
	}

	public function authorize()
	{
		return true;
	}

	public function getPurchaseOrderAccessory($index = 'purchaseOrderAccessoryId')
	{
		return $this->purchaseOrderAccessoryRecordService->getById($this->input($index));
	}

	public function getDate($index = 'date')
	{
		return new DateTime($this->input($index));
	}

	public function getRemarks($index = 'remarks')
	{
		return $this->input($index) ?? null;
	}

	public function getReferenceNo($index = 'referenceNo')
	{
		return $this->input($index);
	}

	public function getReceivingAccessoryDetails($index = 'receivingAccessoryDetails.data')
	{
		return array_map(function ($itemArray) {
			if (isset($itemArray['id']) && $itemArray['id'] > 0) {
				$receivingAccessoryDetail = $this->receivingAccessoryDetailRecordService->getById($itemArray['id']);
			} else {
				$receivingAccessoryDetail = new ReceivingAccessoryDetailModel();
			}

			$product = $this->productRecordService->getById($itemArray['productId']);

			$receivingAccessoryDetail->setProduct($product);
			$receivingAccessoryDetail->setQuantity($itemArray['quantity']);
			return $receivingAccessoryDetail;
		}, $this->input($index));
	}

	public function rules()
	{
		return [
			'referenceNo' => 'required|unique:receiving_accessories,reference_no,' . $this->route('receivingAccessoryId') ?? null,
			'purchaseOrderAccessoryId'     => 'required',
			'date'           => 'required'
		];
	}

	public function messages()
	{
		return [
			'referenceNo.required'           => 'Reference no is required.',
			'referenceNo.unique'             => 'Reference no already exists.',
			'purchaseOrderAccessoryId'            => 'Purchase Order Accessory is required.',
			'date'                  => 'Date is required',
		];
	}
}
