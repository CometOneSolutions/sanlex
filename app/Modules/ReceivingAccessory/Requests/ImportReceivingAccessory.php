<?php

namespace App\Modules\ReceivingAccessory\Requests;

use Dingo\Api\Http\FormRequest;

class ImportReceivingAccessory extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
    public function messages()
    {
        return [];
    }
}
