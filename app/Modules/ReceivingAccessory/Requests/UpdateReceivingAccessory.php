<?php

namespace App\Modules\ReceivingAccessory\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateReceivingAccessory extends ReceivingAccessoryRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [PUR] Receiving Accessory');
	}
}
