<?php

namespace App\Modules\ReceivingAccessory\Models;



interface ReceivingAccessory
{

	const STATUS_IN_TRANSIT = 'In Transit';
	const STATUS_RECEIVED = 'Received';

	public function getId();

	public function setReferenceNo($value);

	public function getReferenceNo();

	public function setDate($value);

	public function getDate();

	public function setRemarks($value);

	public function getRemarks();

	public function setStatus($value);

	public function getStatus();
}
