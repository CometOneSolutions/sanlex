<?php

namespace App\Modules\ReceivingAccessory\Models;

use DateTime;
use CometOneSolutions\Common\Models\C1Model;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use App\Modules\ReceivingAccessory\Models\ReceivingAccessory;
use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessory;
use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessoryModel;
use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetailModel;

class ReceivingAccessoryModel extends C1Model implements ReceivingAccessory, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'receiving_accessories';

    protected $dates = ['date'];

    protected $receivingAccessoryDetailsToSet = null;


    public function purchaseOrderAccessory()
    {
        return $this->belongsTo(PurchaseOrderAccessoryModel::class, 'purchase_order_accessory_id');
    }

    public function receivingAccessoryDetails()
    {
        return $this->hasMany(ReceivingAccessoryDetailModel::class, 'receiving_accessory_id');
    }

    public function setReceivingAccessoryDetails(array $receivingAccessoryDetails)
    {
        $this->receivingAccessoryDetailsToSet = $receivingAccessoryDetails;
        return $this;
    }

    public function getReceivingAccessoryDetails()
    {
        if ($this->receivingAccessoryDetailsToSet !== null) {
            return collect($this->receivingAccessoryDetailsToSet);
        }
        return $this->receivingAccessoryDetails;
    }

    public function setPurchaseOrderAccessory(PurchaseOrderAccessory $purchaseOrderAccessory)
    {
        $this->purchaseOrderAccessory()->associate($purchaseOrderAccessory);
        $this->purchase_order_accessory_id = $purchaseOrderAccessory->getId();
        return $this;
    }

    public function getPurchaseOrderAccessory()
    {
        return $this->purchaseOrderAccessory;
    }

    public function getPurchaseOrderAccessoryId()
    {
        return $this->purchase_order_accessory_id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDate($value)
    {
        $this->date = $value;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setReferenceNo($value)
    {
        $this->reference_no = $value;
        return $this;
    }

    public function getReferenceNo()
    {
        return $this->reference_no;
    }

    public function setRemarks($value)
    {
        $this->remarks = $value;
        return $this;
    }

    public function getRemarks()
    {
        return $this->remarks;
    }

    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }


    public function isInTransit()
    {
        return $this->status == ReceivingAccessory::STATUS_IN_TRANSIT;
    }

    public function isReceived()
    {
        return $this->status == ReceivingAccessory::STATUS_RECEIVED;
    }

    public function scopeInTransit($query)
    {
        return $query->whereStatus(ReceivingAccessory::STATUS_IN_TRANSIT);
    }

    public function scopeReceived($query)
    {
        return $query->whereStatus(ReceivingAccessory::STATUS_RECEIVED);
    }

    public function isPurchaseOrderAccessoryOpen()
    {
        return $this->getPurchaseOrderAccessory()->isOpen();
    }

    public function scopePurchaseOrderAccessoryNumber($query, $purchaseOrderAccessoryNumber)
    {
        return $query->whereHas('purchaseOrderAccessory', function ($purchaseOrderAccessory) use ($purchaseOrderAccessoryNumber) {
            return $purchaseOrderAccessory->where('po_no', 'LIKE', $purchaseOrderAccessoryNumber);
        });
    }

    public function getSupplierId()
    {
        return $this->getPurchaseOrderAccessory()->getSupplierId();
    }
}
