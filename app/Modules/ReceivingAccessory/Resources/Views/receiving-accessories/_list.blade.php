<div class="form-row">
    <div class="form-group col-md-4">
        <label for="inputEmail4" class="col-form-label">Reference No.</label>
        <p>@{{form.referenceNo}}</p>
    </div>
    <div class="form-group col-md-4">
        <label for="inputEmail4" class="col-form-label">Purchase Order Accessory</label>
        <p>@{{form.purchaseOrderAccessory.data.purchaseNumber}}</p>
    </div>

    <div class="form-group col-md-4">
        <label for="inputEmail4" class="col-form-label">Status</label>
        <p><span :class="form.badgeClass">@{{form.status}}</span></p>
    </div>

    <div class="form-group col-md-4">
        <label for="inputEmail4" class="col-form-label">Date</label>
        <p>@{{form.date}}</p>
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Remarks:</label>
        <p>@{{form.remarks}}</p>
    </div>
</div>
