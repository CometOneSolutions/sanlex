@extends('app')
@section('breadcrumbs', Breadcrumbs::render('receiving-accessory.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Receiving Accessory
            @if(auth()->user()->can('Create [PUR] Receiving Accessory'))
            <div class="float-right">
                <a href="{{route('receiving-accessory_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
            </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending"
            v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            <a v-on:click="setSorter('reference_no')">
                                Reference No. <i class="fas fa-sort" :class="getSortIcon('reference_no')"></i>
                            </a>
                        </th>
                        <th>
                            <a v-on:click="setSorter('purchase_order_accessory_number')">
                                Purchase Order Accessory  <i class="fas fa-sort" :class="getSortIcon('purchase_order_accessory_number')"></i>
                            </a>
                        </th>
                        <th>
                            <a v-on:click="setSorter('date')">
                                Date <i class="fas fa-sort" :class="getSortIcon('date')"></i>
                            </a></th>
                        <th class="text-center">
                            <a v-on:click="setSorter('status')">
                                Status <i class="fas fa-sort" :class="getSortIcon('status')"></i>
                            </a>
                        </th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="item in items" v-if="!isLoading">
                        <td><a :href="item.showUri">@{{ item.referenceNo }}</a></td>
                        <td>@{{ item.purchaseOrderAccessory.data.purchaseNumber }}</td>
                        <td>@{{ item.date }}</td>
                        <td class="text-center"><span :class="[item.isInTransit ? 'badge badge-warning' : 'badge badge-success']">@{{item.status}}<span></td>
                        <td class="text-center">
                            <span v-if="item.isInTransit">
                                <a :href="item.editUri" role="button" class="btn btn-outline-primary btn-sm"><i
                                        class="fas fa-pencil-alt"></i></a>
                            </span>
                            <span v-else>
                                <i class="far fa-check-circle text-success"></i>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </index>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush