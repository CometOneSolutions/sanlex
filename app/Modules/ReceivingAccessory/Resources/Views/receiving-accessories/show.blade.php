@extends('app')
@section('breadcrumbs', Breadcrumbs::render('receiving-accessory.show', $receivingAccessory))
@section('content')
<section id="receiving-accessory">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" role="tab" href="#main">General</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" role="tab" href="#upload">Upload</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content no-border">
                            <div class="tab-pane active" role="tabpanel" id="main">
                                <div class="text-right" v-if="form.isInTransit">
                                    @if(auth()->user()->can('Approve [OUT] Receiving Accessory'))
                                    <button type="button" @click="receive" class="btn btn-warning btn-sm" :disabled="form.receivingAccessoryDetails.data.length == 0 || form.isBusy"><i class="far fa-thumbs-up"></i>
                                        Receive</button>
                                    @endif
                                    @if(auth()->user()->can('Update [PUR] Receiving Accessory'))
                                    <a v-if="form.isPurchaseOrderAccessoryOpen" :href="form.editUri" class="btn btn-sm btn-outline-primary" role="button"><i class="fas fa-pencil-alt"></i></a>
                                    @endif
                                </div>

                                @include("receiving-accessories._list")
                            </div>

                            <div class="tab-pane" role="tabpanel" id="upload">
                                @include("receiving-accessories.components.purchase-order-accessory-upload")
                                @include("receiving-accessories.components.details")
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <timestamp :name="form.updatedByUser.data.name" :time="form.updatedAt"></timestamp>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection

@push('scripts')
<script src="{{ mix('js/receiving-accessory.js') }}"></script>
@endpush
