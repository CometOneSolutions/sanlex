<div class="row mb-3" v-if="form.isInTransit">
    <form class="d-inline" action="{{route('purchase-order-accessory_generate_template', $receivingAccessory->getPurchaseOrderAccessoryId())}}" method="POST" target="_blank">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-outline-primary btn-sm" data-toggle="tooltip" data-placement="left" title="Download {{$receivingAccessory->getPurchaseOrderAccessory()->getProductType()}} template" formtarget="_self">
            <i class="fas fa-download"></i> Download Template
        </button>
    </form>
    <div class="ml-auto col-3 text-right">
        <a href="#" class="btn btn-outline-primary btn-sm" role="button" data-toggle="modal" data-target="#add_file">&nbsp;<i class="fas fa-file-upload"></i>&nbsp;
        </a>
    </div>
</div>

<div class="row" v-if="form.isInTransit">
    <div class="alert alert-warning col-4" role="alert">
        <small class="text-uppercase">PLEASE UPLOAD A "{{$receivingAccessory->getPurchaseOrderAccessory()->getProductType()}}" TEMPLATE.</small>
    </div>
</div>

<div class="text-right">
    <div class="modal fade" id="add_file" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upload File</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="input-group col-12">
                                <input type="file" id="uploadFile" class=".form-control-file" accept="application/xlsx|application/xls" ref="file" v-on:change="handleFileChange">
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" @click="removeFile" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary btn-sm" type="button" :disabled="isBusy" @click="addFile">
                        <div v-if="isSaving">
                            <i class="fas fa-spinner fa-pulse"></i> Saving
                        </div>
                        <div v-if="!isSaving">
                            Save
                        </div>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
