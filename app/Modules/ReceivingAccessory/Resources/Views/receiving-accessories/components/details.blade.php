<div class="form-row" v-if="form.receivingAccessoryDetails.data.length > 0">
    <div class="table-responsive-sm col-lg-12">
        <table class="table">
            <thead>
            <tr class="row">
                <th class="center col-1">#</th>
                <th class="col-9">Product</th>
                <th class="col-2 text-right">Quantity</th>
            </tr>
            </thead>

            <tbody>
            <tr class="row" v-for="(item,index) in form.receivingAccessoryDetails.data">
                <td class="center col-1">@{{index+1}}</td>
                <td class="col-9">
                    @{{item.product.data.name}}
                </td>
                <td class="center col-2 text-right">
                    @{{ item.quantity | numeric}}
                </td>
            </tr>
           
            </tbody>
        </table>
    </div>
</div>