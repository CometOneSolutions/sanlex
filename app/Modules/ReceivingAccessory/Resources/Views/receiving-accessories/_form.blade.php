<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Reference No.<span class="text text-danger">*</span></label>
        <input type="text" class="form-control" id="referenceNo" name="referenceNo" v-model="form.referenceNo" placeholder="" :class="{'is-invalid': form.errors.has('referenceNo') }" data-msg-required="Enter Reference No.">
    </div>
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Date <span class="text text-danger">*</span></label>
        <datepicker :typeable="true" :input-class="{'is-invalid': form.errors.has('date'),'form-control': true}" v-model="form.date" :required="true" disabled></datepicker>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Supplier <span class="text text-danger">*</span></label>

        <select3 class="custom-select" required data-msg-required="Enter supplier" v-model="supplierId" :selected="defaultSupplier" :url="supplierApi" search="name">
        </select3>
    </div>
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">
            Purchase Order Accessory <span class="text text-danger">*</span>
        </label>

        <select3 v-if="supplierIsLoaded" class="custom-select" required data-msg-required="Enter Purchase order accessory" v-model="form.purchaseOrderAccessoryId" :selected="defaultPurchaseOrderAccessory" :url="purchaseOrderAccessoryApi" search="po_no">
        </select3>
        <select v-else class="form-control">
            <option value="" disabled selected>Select Supplier First</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-12">
        <label for="inputEmail4" class="col-form-label">Remarks</label>
        <input type="text" class="form-control" id="remarks" v-model="form.remarks" placeholder="">
    </div>
</div>
<br>

<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr class="row">
                    <th class="col-6">Product</th>
                    <th class="col-3">UOM</th>
                    <th class="col-2 text-right">Quantity</th>
                    <th class="col-1">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr is="receiving-accessory-detail" class="row" v-for="(detail,index) in form.receivingAccessoryDetails.data" :detail="detail" :index="index" :products="products" v-on:remove="form.receivingAccessoryDetails.data.splice(index, 1)">
                </tr>
                <tr class="row">
                    <td class="col-12" colspan="12" v-if="productsInitialized">
                        <button type="button" class="btn btn-success btn-sm" @click="add">Add New</button>
                    </td>
                    <td class="col-12" colspan="12" v-else>
                        Please select purchase order accessory first.
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@push('scripts')
<script src="{{ mix('js/receiving-accessory.js') }}"></script>
@endpush
