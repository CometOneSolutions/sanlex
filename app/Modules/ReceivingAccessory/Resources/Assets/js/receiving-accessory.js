import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm';
import Select3 from "@c1_common_js/components/Select3";
import Datepicker from 'vuejs-datepicker';
import ReceivingAccessoryDetail from '../components/ReceivingAccessoryDetail'

import { productService } from "@c1_module_js/Product/Resources/Assets/js/product-main";

new Vue({
    el: '#receiving-accessory',
    components: {
        SaveButton,
        DeleteButton,
        Timestamp,
        VForm,
        Select3,
        Datepicker,
        ReceivingAccessoryDetail
    },

    data: {
        defaultPurchaseOrderAccessory: {},
        defaultSupplier: {},

        form: new Form({
            id: null,
            referenceNo: null,
            purchaseOrderAccessoryId: null,
            date: moment(),
            remarks: null,
            receivingAccessoryDetails: {
                data: []
            }
        }),
        supplierId: null,
        isSaving: false,
        isBusy: false,

        supplierApi: '/api/suppliers?sort=name',
        purchaseOrderAccessoryApi: '',
        dataInitialized: true,
        templateUri: '',
        products: [],
        productsInitialized: false,
        counter: 0,
        supplierIsLoaded: false
    },

    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },

        "form.purchaseOrderAccessoryId": function (newVal, oldVal) {
            this.loadFilteredProduct(newVal);
            this.templateUri = `/purchasing/purchase-order-accessories/generate-template/${newVal}`;
            console.log(newVal, oldVal);
            if (oldVal != null && oldVal != newVal) {
                this.form.receivingAccessoryDetails = {
                    data: []
                }
            }
        },

        supplierId(newVal, oldVal) {
            this.purchaseOrderAccessoryApi = `/api/purchase-order-accessories?status=Open&supplier_id=${this.supplierId}`;
            if (oldVal != null) {
                this.supplierIsLoaded = false;
                this.$nextTick(() => {
                    this.form.receivingAccessoryDetails = {
                        data: []
                    }
                    this.defaultPurchaseOrderAccessory = {}
                    this.supplierIsLoaded = true;
                });
            }
        }
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized;
        },

    },

    methods: {
        loadFilteredProduct(val) {
            this.products = productService.getProductsByPurchaseOrderAccessory(val).then(response => {
                this.products = response.data;
                this.productsInitialized = true;
            });
        },

        add() {
            this.form.receivingAccessoryDetails.data.push({
                id: --this.counter,
                productId: null,
                quantity: '',
            });
        },

        destroy() {
            this.form.confirm().then((result) => {
                if (result.value) {
                    this.form.delete('/api/receiving-accessories/' + this.form.id).then(response => {
                        this.isDeleting = false;
                        this.isBusyDeleting = false;
                        this.$swal({
                            title: 'Success',
                            text: 'Purchase order was successfully deleted.',
                            type: 'success'
                        }).then(() => window.location = '/purchasing/receiving-accessories');

                    })
                }
            });
        },

        store() {
            if (this.validateDetails()) {
                this.form.post('/api/receiving-accessories').then(response => {
                    this.$swal({
                        title: 'Success',
                        text: 'Receiving Accessory was saved.',
                        type: 'success'
                    }).then(() => window.location = '/purchasing/receiving-accessories/create');;
                    // this.supplierId = null;
                    // this.form.reset();
                });
            }
        },

        approve() {
            this.form.affirm().then((result) => {
                if (result.value) {
                    this.form.patch('/api/receiving-accessories/approve/' + id).then(
                        response => {
                            this.$swal({
                                title: 'Success',
                                text: 'Receiving Accessory approved.',
                                type: 'success'
                            }).then(() => window.location = '/purchasing/receiving-accessories/' + id);
                        })
                }
            });
        },

        update() {
            if (this.validateDetails()) {
                this.form.patch('/api/receiving-accessories/' + this.form.id).then(response => {
                    this.isSaving = false;
                    this.isBusy = false;
                    this.form.updated_by = response.data.updated_by;
                    this.form.updated_at = response.data.updated_at;
                    this.$swal({
                        title: 'Success',
                        text: 'Receiving Accessory was successfully updated.',
                        type: 'success'
                    }).then(() => window.location = '/purchasing/receiving-accessories/' + response.data.id);

                }).catch(error => {
                    this.isSaving = false;
                    this.isBusy = false;
                });
            }
        },

        handleFileChange(e) {
            this.form.file = this.$refs.file.files[0];
        },

        addFile() {
            let formData = new FormData();
            formData.append('file', this.form.file);
            formData.append('userId', this.form.updatedByUser.data.id);
            this.isSaving = true;
            this.isBusy = true;

            this.form.postImage('/api/receiving-accessories/' + this.form.id + '/upload', formData)
                .then(response => {
                    this.isSaving = false;
                    this.isBusy = false;
                    this.$swal({
                        title: 'Success',
                        text: 'File was successfully uploaded.',
                        type: 'success'
                    }).then(() => {
                        window.location = '/purchasing/receiving-accessories/' + this.form.id;
                    });
                }).catch(error => {
                    this.isSaving = false;
                    this.isBusy = false;
                });
        },

        removeFile() {
            document.getElementById("uploadFile").value = "";
        },

        receive() {
            this.form.affirm().then((result) => {
                if (result.value) {
                    this.form.patch('/api/receiving-accessories/receive/' + id).then(
                        response => {
                            this.$swal({
                                title: 'Success',
                                text: 'Receiving has been received.',
                                type: 'success'
                            }).then(() => window.location = '/purchasing/receiving-accessories/' + id);
                        })
                }
            });
        },

        loadData(data) {
            this.defaultPurchaseOrderAccessory = data.purchaseOrderAccessory.data;
            this.defaultSupplier = data.purchaseOrderAccessory.data.supplier.data;
            this.supplierIsLoaded = true;
            this.form = new Form(data);

        },

        validateDetails() {
            let ids = [];
            this.form.receivingAccessoryDetails.data.forEach(detail => {
                ids.push(detail.productId);
            });

            let distinctIds = [...new Set(ids)];

            if (distinctIds.length != ids.length) {
                this.$swal({
                    title: 'Error',
                    text: 'Duplicate products found.',
                    type: 'error'
                });
                return false;
            }

            return true;
        }
    },
    created() {
        if (id != null) {
            this.loadFilteredProduct();
            this.dataInitialized = false;
            this.form.isInitializing = true;
            this.form.get('/api/receiving-accessories/' + id + '?include=purchaseOrderAccessory,purchaseOrderAccessory.supplier,receivingAccessoryDetails,receivingAccessoryDetails.product,receivingAccessoryDetails.product.uom')
                .then(response => {
                    this.loadData(response.data);
                    this.dataInitialized = true;
                });
        } else {
            this.supplierId = '';
            this.form.purchaseOrderAccessoryId = '';
        }

    },

    mounted() {
        console.log("Init receiving accessory script...");
    }
});