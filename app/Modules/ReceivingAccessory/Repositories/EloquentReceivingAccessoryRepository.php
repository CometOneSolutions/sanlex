<?php

namespace App\Modules\ReceivingAccessory\Repositories;

use App\Modules\ReceivingAccessory\Models\ReceivingAccessory;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentReceivingAccessoryRepository extends EloquentRepository implements ReceivingAccessoryRepository
{
	public function __construct(ReceivingAccessory $receivingAccessory)
	{
		parent::__construct($receivingAccessory);
	}

	public function save(ReceivingAccessory $receivingAccessory)
	{
		$result =  $receivingAccessory->save();
		$receivingAccessory->receivingAccessoryDetails()->sync($receivingAccessory->getReceivingAccessoryDetails());

		return $result;
	}

	public function delete(ReceivingAccessory $receivingAccessory)
	{
		return $receivingAccessory->delete();
	}

	protected function filterByPurchaseOrderAccessoryNumber($purchaseOrderAccessoryNumber)
	{
		return $this->model->purchaseOrderAccessoryNumber($purchaseOrderAccessoryNumber);
	}

	protected function orderByPurchaseOrderAccessoryNumber($direction)
	{
		return $this->model->join('purchase_order_accessories', 'purchase_order_accessories.id', 'receiving_accessories.purchase_order_accessory_id')
			->select('purchase_order_accessories.po_no as purchase_order_accessory_number', 'receiving_accessories.*')
			->orderBy('purchase_order_accessory_number', $direction);
	}
}
