<?php

namespace App\Modules\ReceivingAccessory\Repositories;

use App\Modules\ReceivingAccessory\Models\ReceivingAccessory;
use CometOneSolutions\Common\Repositories\Repository;

interface ReceivingAccessoryRepository extends Repository
{
	public function save(ReceivingAccessory $receivingAccessory);
	public function delete(ReceivingAccessory $receivingAccessory);
}
