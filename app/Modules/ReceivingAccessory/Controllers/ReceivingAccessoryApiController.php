<?php

namespace App\Modules\ReceivingAccessory\Controllers;

use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\ReceivingAccessory\Services\ReceivingAccessoryRecordService;
use App\Modules\ReceivingAccessory\Transformer\ReceivingAccessoryTransformer;
use App\Modules\ReceivingAccessory\Requests\StoreReceivingAccessory;
use App\Modules\ReceivingAccessory\Requests\UpdateReceivingAccessory;
use App\Modules\ReceivingAccessory\Requests\DestroyReceivingAccessory;
use CometOneSolutions\Common\Exceptions\GeneralApiException;
use Illuminate\Support\Facades\Validator;
use App\Modules\ReceivingAccessory\Requests\ImportReceivingAccessory;
use App\Modules\ReceivingAccessory\Imports\ReceivingAccessoryImport;
use Maatwebsite\Excel\Facades\Excel;

class ReceivingAccessoryApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;

	public function __construct(
		ReceivingAccessoryRecordService $receivingAccessoryRecordService,
		ReceivingAccessoryTransformer $transformer
	) {
		$this->middleware('auth:api');
		parent::__construct($receivingAccessoryRecordService, $transformer);
		$this->service = $receivingAccessoryRecordService;
		$this->transformer = $transformer;
	}

	public function store(StoreReceivingAccessory $request)
	{
		$receivingAccessory = \DB::transaction(function () use ($request) {
			return $this->service->create(
				$request->getReferenceNo(),
				$request->getDate(),
				$request->getRemarks(),
				$request->getPurchaseOrderAccessory(),
				$request->getReceivingAccessoryDetails(),
				$request->user()
			);
		});
		return $this->response->item($receivingAccessory, $this->transformer)->setStatusCode(201);
	}

	public function update($receivingAccessoryId, UpdateReceivingAccessory $request)
	{
		$receivingAccessory = \DB::transaction(function () use ($receivingAccessoryId, $request) {
			$receivingAccessory   = $this->service->getById($receivingAccessoryId);

			return $this->service->update(
				$receivingAccessory,
				$request->getReferenceNo(),
				$request->getDate(),
				$request->getRemarks(),
				$request->getPurchaseOrderAccessory(),
				$request->getReceivingAccessoryDetails(),
				$request->user()
			);
		});
		return $this->response->item($receivingAccessory, $this->transformer)->setStatusCode(200);
	}

	public function destroy($receivingAccessoryId, DestroyReceivingAccessory $request)
	{
		$receivingAccessory = $this->service->getById($receivingAccessoryId);
		$this->service->delete($receivingAccessory);
		return $this->response->item($receivingAccessory, $this->transformer)->setStatusCode(200);
	}

	public function receive($receivingAccessoryId, UpdateReceivingAccessory $request)
	{
		$receivingAccessory = \DB::transaction(function () use ($request, $receivingAccessoryId) {
			$receivingAccessory = $this->service->receive(
				$this->service->getByid($receivingAccessoryId),
				$request->user()
			);
			return $receivingAccessory;
		});

		return $this->response->item($receivingAccessory, $this->transformer)->setStatusCode(200);
	}

	public function import($receivingAccessoryId, ImportReceivingAccessory $request)
	{
		$validator = Validator::make(
			[
				'file'      => $request->file,
				'extension' => strtolower($request->file->getClientOriginalExtension()),
			],
			[
				'file'          => 'required',
				'extension'      => 'required|in:csv,xlsx,xls',
			]
		);

		if ($validator->fails()) {
			throw new GeneralApiException('File type is not supported.', 'Error', 400, true);
		}

		\DB::transaction(function () use ($request, $receivingAccessoryId) {
			Excel::import(new ReceivingAccessoryImport($this->service->getById($receivingAccessoryId), $request->user()), $request->file('file'));
		});
	}
}
