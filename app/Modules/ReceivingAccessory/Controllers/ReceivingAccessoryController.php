<?php

namespace App\Modules\ReceivingAccessory\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\ReceivingAccessory\Services\ReceivingAccessoryRecordService;


class ReceivingAccessoryController extends Controller
{
	private $service;

	public function __construct(ReceivingAccessoryRecordService $receivingAccessoryRecordService)
	{
		$this->service = $receivingAccessoryRecordService;
		$this->middleware('permission:Read [PUR] Receiving Accessory')->only(['show', 'index']);
		$this->middleware('permission:Create [PUR] Receiving Accessory')->only('create');
		$this->middleware('permission:Update [PUR] Receiving Accessory')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'status,-created_at',
			'sortAscending' => true,
			'baseUrl' => '/api/receiving-accessories?include=purchaseOrderAccessory'
		]);
		return view('receiving-accessories.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null]);
		return view('receiving-accessories.create');
	}

	public function show($id)
	{
		$receivingAccessory = $this->service->getById($id);

		JavaScript::put([
			'id' => $id
		]);

		return view('receiving-accessories.show', compact('receivingAccessory'));
	}

	public function edit($id)
	{
		$receivingAccessory = $this->service->getById($id);

		JavaScript::put([
			'id' => $id,
		]);

		return view('receiving-accessories.edit', compact('receivingAccessory'));
	}
}
