<?php

namespace App\Modules\ReceivingAccessory\Provider;

use App\Modules\ReceivingAccessory\Models\ReceivingAccessory;
use App\Modules\ReceivingAccessory\Models\ReceivingAccessoryModel;
use App\Modules\ReceivingAccessory\Repositories\EloquentReceivingAccessoryRepository;
use App\Modules\ReceivingAccessory\Repositories\ReceivingAccessoryRepository;
use App\Modules\ReceivingAccessory\Services\ReceivingAccessoryRecordService;
use App\Modules\ReceivingAccessory\Services\ReceivingAccessoryRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ReceivingAccessoryServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/ReceivingAccessory/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(ReceivingAccessoryRecordService::class, ReceivingAccessoryRecordServiceImpl::class);
		$this->app->bind(ReceivingAccessoryRepository::class, EloquentReceivingAccessoryRepository::class);
		$this->app->bind(ReceivingAccessory::class, ReceivingAccessoryModel::class);
	}
}
