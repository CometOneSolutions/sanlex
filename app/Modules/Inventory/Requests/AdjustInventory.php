<?php

namespace App\Modules\Inventory\Requests;



class AdjustInventory extends InventoryRequest
{

    public function getAdjustedQuantity($index = 'adjustedQuantity')
    {
        return $this->input($index);
    }

    public function getRemarks($index = 'remarks')
    {
        return $this->input($index);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('Adjust [INV] Inventory');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'adjustedQuantity'      => 'required',

        ];
    }

    public function messages()
    {
        return [
            'adjustedQuantity.required'        => 'Adjusted quantity is required.',
        ];
    }
}
