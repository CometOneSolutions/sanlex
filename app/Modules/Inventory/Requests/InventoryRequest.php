<?php

namespace App\Modules\Inventory\Requests;

use App\Modules\Inventory\Services\InventoryRecordService;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordService;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Uom\Services\UomRecordService;
use Dingo\Api\Http\FormRequest;



class InventoryRequest extends FormRequest
{
    protected $service;
    protected $productRecordService;
    protected $uomRecordService;
    protected $inventoryDetailRecordService;

    public function __construct(
        InventoryRecordService $inventoryRecordService,
        ProductRecordService $productRecordService,
        UomRecordService $uomRecordService,
        InventoryDetailRecordService $inventoryDetailRecordService
    ) {
        $this->service = $inventoryRecordService;
        $this->productRecordService = $productRecordService;
        $this->uomRecordService = $uomRecordService;
        $this->inventoryDetailRecordService = $inventoryDetailRecordService;
    }

    public function getInventory($index = 'id')
    {
        return $this->service->getById($this->input($index));
    }

    public function getProduct($index = 'productId')
    {
        return $this->productRecordService->getById($this->input($index));
    }
    public function getQuantity($index = 'quantity')
    {
        return $this->input($index);
    }
    public function getIdentifier($index = 'identifier')
    {
        return $this->input($index);
    }
    public function getInventoryDetail($inventoryDetailId)
    {
        return $this->inventoryDetailRecordService->getById($inventoryDetailId);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'name'          => 'required|unique:products,name,' . $this->route('productId') ?? null,
            // 'paperTypeId'   => 'required',
            // 'variant'       => 'required'
        ];
    }
    public function messages()
    {
        return [
            // 'name.required'        => 'Product name is a required field',
            // 'name.unique'          => 'Product name already exist',
            // 'paperTypeId.required'          => 'Paper type is required',
            // 'variant.required'      => 'Please enter variant'
        ];
    }


}
