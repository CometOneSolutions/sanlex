<?php

namespace App\Modules\Inventory\Requests;

class StoreInventory extends InventoryRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('Create [INV] Inventory');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'productId' => 'required',
            'details.*.quantity' => 'required',
            'details.*.identifier' => 'required|unique:inventory_details,slit_identifier'
            // 'quantity'              => 'required',
            // 'identifier'            => 'required|unique:inventory_details,slit_identifier'
        ];
    }

    public function messages()
    {
        $messages = [
            'productId.required' => 'Product is required.',
            // 'quantity.required'         => 'Quantity is required.',
            // 'identifier.required'       => 'Identifier is required.',
            // 'identifier.unique'         => 'Identifier already exists.',
        ];
        foreach ($this->get('details') as $key => $val) {
            $index = $key + 1;
            $messages["details.$key.quantity.required"] = "Item {$index} quantity is required.";
            $messages["details.$key.identifier.required"] = "Item {$index} identifier is required.";

            $messages["details.$key.identifier.unique"] = "{$val['identifier']} already exists.";
        }
        return $messages;
    }
}
