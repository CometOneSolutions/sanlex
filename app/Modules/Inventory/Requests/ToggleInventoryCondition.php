<?php

namespace App\Modules\Inventory\Requests;



class ToggleInventoryCondition extends InventoryRequest
{
    public function authorize()
    {
        return $this->user()->can('Toggle-Condition [INV] Inventory');
    }
}
