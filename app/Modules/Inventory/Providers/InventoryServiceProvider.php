<?php

namespace App\Modules\Inventory\Providers;

use App\Modules\Inventory\Models\Inventory;
use App\Modules\Inventory\Models\InventoryModel;
use App\Modules\Inventory\Repositories\EloquentInventoryRepository;
use App\Modules\Inventory\Repositories\InventoryRepository;
use App\Modules\Inventory\Services\InventoryRecordService;
use App\Modules\Inventory\Services\InventoryRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class InventoryServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Inventory/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(InventoryRecordService::class, InventoryRecordServiceImpl::class);
        $this->app->bind(InventoryRepository::class, EloquentInventoryRepository::class);
        $this->app->bind(Inventory::class, InventoryModel::class);
    }
}
