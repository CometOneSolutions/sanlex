<?php

namespace App\Modules\Inventory\Controllers;

use JavaScript;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use CometOneSolutions\Common\Utils\Filter;
use App\Modules\Color\Services\ColorRecordService;
use App\Modules\Width\Services\WidthRecordService;
use CometOneSolutions\Common\Utils\UriParserHelper;
use CometOneSolutions\Common\Controllers\Controller;
use CometOneSolutions\Common\Services\RecordService;
use App\Modules\Coating\Services\CoatingRecordService;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\Inventory\Services\InventoryRecordService;
use App\Modules\Thickness\Services\ThicknessRecordService;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordService;


class InventoryController extends Controller
{
    use UriParserHelper;
    private $service;
    private $inventoryDetailRecordService;
    private $productRecordService;
    private $coatingRecordService;
    private $thicknessRecordService;
    private $widthRecordService;
    private $colorRecordService;
    private $supplierRecordService;

    public function __construct(
        InventoryRecordService $inventoryRecordService,
        InventoryDetailRecordService $inventoryDetailRecordService,
        ProductRecordService $productRecordService,
        CoatingRecordService $coatingRecordService,
        ThicknessRecordService $thicknessRecordService,
        WidthRecordService $widthRecordService,
        ColorRecordService $colorRecordService,
        SupplierRecordService $supplierRecordService
    ) {
        $this->middleware('auth');
        $this->service = $inventoryRecordService;
        $this->inventoryDetailRecordService = $inventoryDetailRecordService;
        $this->productRecordService = $productRecordService;
        $this->coatingRecordService = $coatingRecordService;
        $this->thicknessRecordService = $thicknessRecordService;
        $this->widthRecordService = $widthRecordService;
        $this->colorRecordService = $colorRecordService;
        $this->supplierRecordService = $supplierRecordService;

        $this->middleware('permission:Read [INV] Inventory')->only([
            'dashboard', 'coilIndex', 'coilIndexArchived', 'accessoryIndex', 'accessoryIndexArchived', 'indexOfDamaged'
        ]);
        $this->middleware('permission:Transfer-SKU [INV] Inventory')->only('moveCoil');
        $this->middleware('permission:Transfer-SKU [INV] Inventory')->only('transferCoil');
    }

    public function dashboard()
    {
        return view('master.inventory.dashboard');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function coilIndex()
    {
        $filters = $this->service->getAvailableFilters();
        $filters[] = ['id' => 'coil_number', 'text' => 'Coil Number'];
        $filters[] = ['id' => 'slit_identifier', 'text' => 'Slit Identifier'];
        $filters[] = ['id' => 'supplier_short_code', 'text' => 'Supplier', 'type' => 'Select', 'options' => $this->getFields($this->supplierRecordService,  "short_code")];
        $filters[] = ['id' => 'coating_name', 'text' => 'Coating', 'type' => 'Select', 'options' => $this->getFields($this->coatingRecordService,  "name")];
        $filters[] = ['id' => 'thickness_name', 'text' => 'Thickness', 'type' => 'Select', 'options' => $this->getFields($this->thicknessRecordService,  "name")];
        $filters[] = ['id' => 'width_name', 'text' => 'Width', 'type' => 'Select', 'options' => $this->getFields($this->widthRecordService,  "name")];
        $filters[] = ['id' => 'color_name', 'text' => 'Color', 'type' => 'Select', 'options' => $this->getFields($this->colorRecordService,  "name")];
        JavaScript::put([
            'filterable' => $filters,
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/inventory-details?include=inventory.product,receiving&uom=MT&inventory_details.quantity>0&condition=Good'
        ]);
        // Inventory (Coil Stock Page)
        return view('inventories.coil.index');
    }

    public function coilIndexArchived()
    {
        $filters = $this->service->getAvailableFilters();
        $filters[] = ['id' => 'coil_number', 'text' => 'Coil Number'];
        $filters[] = ['id' => 'slit_identifier', 'text' => 'Slit Identifier'];
        $filters[] = ['id' => 'supplier_short_code', 'text' => 'Supplier', 'type' => 'Select', 'options' => $this->getFields($this->supplierRecordService, "short_code")];
        $filters[] = ['id' => 'coating_name', 'text' => 'Coating', 'type' => 'Select', 'options' => $this->getFields($this->coatingRecordService, "name")];
        $filters[] = ['id' => 'thickness_name', 'text' => 'Thickness', 'type' => 'Select', 'options' => $this->getFields($this->thicknessRecordService, "name")];
        $filters[] = ['id' => 'width_name', 'text' => 'Width', 'type' => 'Select', 'options' => $this->getFields($this->widthRecordService, "name")];
        $filters[] = ['id' => 'color_name', 'text' => 'Color', 'type' => 'Select', 'options' => $this->getFields($this->colorRecordService, "name")];

        JavaScript::put([
            'filterable' => $filters,
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/inventory-details?include=inventory.product,receiving&uom=MT&quantity=0&condition=Good'
        ]);
        // Inventory (Coil Stock (0) Page)
        return view('inventories.coil.index');
    }

    public function accessoryIndex()
    {
        $filters = $this->service->getAvailableFilters();

        $filters[] = ['id' => 'product', 'text' => 'Product'];
        $filters[] = ['id' => 'type', 'text' => 'Type'];

        JavaScript::put([
            'filterable' => $filters,
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/inventory-details?include=inventory.product,receivingAccessoryDetail&uom=PIECE&quantity>0&condition=Good'
        ]);
        // Inventory (Accessory Stock Page)
        return view('inventories.accessory.index');
    }

    public function accessoryIndexArchived()
    {
        $filters = $this->service->getAvailableFilters();
        $filters[] = ['id' => 'product', 'text' => 'Product'];
        $filters[] = ['id' => 'type', 'text' => 'Type'];

        JavaScript::put([
            'filterable' => $filters,
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/inventory-details?include=inventory.product,receivingAccessoryDetail&uom=PIECE&quantity<=0'
        ]);
        // Inventory (Accessory Stock (0) Page)
        return view('inventories.accessory.index');
    }

    public function indexOfDamaged(Request $request)
    {

        $filterable[] = ['id' => 'damaged_coil_number_or_slit_identifier', 'text' => 'Coil Number / Slit Identifier'];
        $filters = $this->getFilterArraysFromRequest($request, 'field');

        JavaScript::put([
            'filterable' => $filterable,
            'sorter' => 'id',
            'filters' => $filters,
            'sortAscending' => true,
            'baseUrl' => '/api/inventory-details?include=inventory.uom,inventory.product,receiving&condition=Bad'
        ]);
        // Inventory (Damaged Stock Page)
        return view('inventories.damaged.index');
    }

    // Move to another SKU Show form
    public function moveCoil($inventoryDetailId)
    {
        $inventoryDetail = $this->inventoryDetailRecordService->getById($inventoryDetailId);
        $productFilter = new Filter('id', $inventoryDetail->getInventory()->getProduct()->getId(), '!=');
        $raw = $this->productRecordService->getAll([$productFilter]);

        $products = $raw->sortBy(function ($product, $key) {
            return $product->getName();
        });

        if ($products->isEmpty()) {
            return redirect()->route('inventory_coil_index')->with('status', 'No product SKU/s available!');
        }
        return view('inventories.coil.move', compact('inventoryDetail', 'products'));
    }

    // Move to another SKU Handle POST Request
    public function transferCoil($inventoryDetailId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'productId' => 'required'
        ], ['productId.required' => 'Destination SKU is required.']);

        if ($validator->fails()) {
            return redirect()
                ->route('inventory_coil_move', $inventoryDetailId)
                ->withErrors($validator, 'inventory')
                ->withInput();
        }

        // $newInventory = $this->service->getById($request->input('inventoryId'));
        $product = $this->productRecordService->getById($request->input('productId'));
        $inventoryDetail = $this->inventoryDetailRecordService->getById($inventoryDetailId);
        $inventory = $this->service->findOrCreate($product, $request->user());

        $this->inventoryDetailRecordService->move($inventoryDetail, $inventory);

        return redirect()->route('inventory_coil_index')->with('status', 'Product was successfully moved!');
    }

    private function getFields(RecordService $recordService, $field = 'name')
    {
        $namesCollection = $recordService->getAll()->sortBy($field)
            ->map(function ($item) use ($field) {
                if ($field == 'name') {
                    return $item->getName();
                }

                if ($field = 'short_code') {
                    return $item->getShortCode();
                }
            });

        return array_values($namesCollection->toArray());
    }
}
