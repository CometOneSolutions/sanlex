<?php

namespace App\Modules\Inventory\Controllers;

use App\Modules\Inventory\Requests\AdjustInventory;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordService;
use App\Modules\Inventory\Services\InventoryRecordService;
use App\Modules\Inventory\Transformers\InventoryTransformer;
use App\Modules\Inventory\Requests\StoreInventory;
use App\Modules\Inventory\Requests\ToggleInventoryCondition;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use CometOneSolutions\Common\Exceptions\GeneralApiException;

class InventoryApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;
    protected $inventoryDetailRecordService;

    public function __construct(
        InventoryRecordService $inventoryRecordService,
        InventoryTransformer $transformer,
        InventoryDetailRecordService $inventoryDetailRecordService
    ) {
        parent::__construct($inventoryRecordService, $transformer);

        $this->service = $inventoryRecordService;
        $this->transformer = $transformer;
        $this->inventoryDetailRecordService = $inventoryDetailRecordService;
    }

    public function store(StoreInventory $request)
    {
        $details = $request->input('details');
        $inventory = null;
        if ($details !== null && count($details) === 0) {
            throw new GeneralApiException('List is empty.');
        }

        $inventory = \DB::transaction(function () use ($request, $details) {
            $product = $request->getProduct();
            $inventory = $this->service->getInventoryRecord($product->getId());
            if (!$inventory) {
                $inventory = $this->service->createInventory($product, $request->user());
            }
            foreach ($details as $detail) {
                $quantity = $detail['quantity'];
                $identifier = $detail['identifier'];
                $this->inventoryDetailRecordService->createFromRestocking($product, $inventory, $quantity, $identifier);
            }
            return $inventory;
        });
        return $this->response->item($inventory, $this->transformer)->setStatusCode(201);
    }

    public function adjust($inventoryDetailId, AdjustInventory $request)
    {
        $inventory = \DB::transaction(function () use ($request, $inventoryDetailId) {
            $inventoryDetail = $request->getInventoryDetail($inventoryDetailId);
            $adjustedQuantity = $request->getAdjustedQuantity();
            $remarks = $request->getRemarks();

            $this->inventoryDetailRecordService->adjust($inventoryDetail, $adjustedQuantity, $remarks, $request->user());
            return $inventoryDetail->getInventory();
        });
        return $this->response->item($inventory, $this->transformer)->setStatusCode(201);
    }

    public function toggleToDamaged($inventoryDetailId, ToggleInventoryCondition $request)
    {
        $inventory = \DB::transaction(function () use ($request, $inventoryDetailId) {
            $inventoryDetail = $request->getInventoryDetail($inventoryDetailId);

            $this->inventoryDetailRecordService->toggleToDamaged($inventoryDetail, $request->user());
            return $inventoryDetail->getInventory();
        });
        return $this->response->item($inventory, $this->transformer)->setStatusCode(201);
    }

    public function toggleToGood($inventoryDetailId, ToggleInventoryCondition $request)
    {
        $inventory = \DB::transaction(function () use ($request, $inventoryDetailId) {
            $inventoryDetail = $request->getInventoryDetail($inventoryDetailId);

            $this->inventoryDetailRecordService->toggleToGood($inventoryDetail, $request->user());
            return $inventoryDetail->getInventory();
        });
        return $this->response->item($inventory, $this->transformer)->setStatusCode(201);
    }
}
