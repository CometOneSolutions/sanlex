<?php

namespace App\Modules\Inventory;

use App\Modules\InventoryDetail\Models\InventoryDetailModel;

trait HasInventoryDetailable
{
    public function inventoryDetailable()
    {
        return $this->morphMany(InventoryDetailModel::class, 'inventory_detailable');
    }
}
