<?php

namespace App\Modules\Inventory\Models;

use App\Modules\Product\Models\Product;
use App\Modules\Uom\Models\Uom;

interface Inventory
{
    public static function new(Product $product, Uom $uom, $stock);

    public function getId();

    public function getInventoryDetails();

    public function setProduct(Product $product);

    public function getProduct();

    public function setUom(Uom $uom);

    public function getUom();

    public function getStock();
}
