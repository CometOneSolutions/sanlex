<?php

namespace App\Modules\Inventory\Models;

use App\Modules\InventoryDetail\Models\InventoryDetailModel;
use App\Modules\Product\Models\Product;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Uom\Models\Uom;
use App\Modules\Uom\Models\UomModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Common\Models\C1Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class InventoryModel extends C1Model implements Inventory, UpdatableByUser
{
	use HasUpdatedByUser;

	protected $table = 'inventories';

	public function getId()
	{
		return $this->id;
	}

	public function product()
	{
		return $this->belongsTo(ProductModel::class, 'product_id');
	}

	public function uom()
	{
		return $this->belongsTo(UomModel::class, 'uom_id');
	}

	public function inventoryDetails()
	{
		return $this->hasMany(InventoryDetailModel::class, 'inventory_id');
	}

	public function getInventoryDetails()
	{
		return $this->inventoryDetails;
	}

	public function setProduct(Product $product)
	{
		$this->product()->associate($product);
		$this->product_id = $product->getId();
		return $this;
	}

	public function getProduct()
	{
		return $this->product;
	}

	public function getProductId()
	{
		return $this->product_id;
	}

	public function setUom(Uom $uom)
	{
		$this->uom()->associate($uom);
		$this->uom_id = $uom->getId();
		return $this;
	}

	public function getUom()
	{
		return $this->uom;
	}

	public function getUomId()
	{
		return $this->uom_id;
	}

	private function setStock($value)
	{
		$this->stock = $value;
		return $this;
	}
	public function initializeStock()
	{
		$this->setStock(0);
		return $this;
	}

	public function updateStock()
	{
		$this->setStock($this->computeStock());
		return $this;
	}

	private function computeStock()
	{
		return $this->getInventoryDetails()->reduce(function ($carry, $inventoryDetail) {
			return $carry + $inventoryDetail->getQuantity();
		}, 0);
	}

	public function getStock()
	{
		return $this->stock;
	}

	public static function new(Product $product, Uom $uom, $stock)
	{
		$newInventory = new static;
		$newInventory->setProduct($product);
		$newInventory->setUom($uom);
		$newInventory->setStock($stock);
		return $newInventory;
	}

	public function getPendingQuantity()
	{
		return $this->getProduct()->getPendingQuantity();
	}

	public function scopeHasPending($query)
	{
		return $query->whereHas('product', function ($product) {
			return $product->hasPending();
		});
	}

	public function scopeAccessories($query)
	{
		return $query->whereHas('product', function ($product) {
			return $product->where('productable_type', '!=', 'App\\Modules\\Coil\\Models\\CoilModel');
		});
	}

	public function getBadStock()
	{
		return $this->inventoryDetails()->condition('Bad')->get()->sum(function ($inventoryDetail) {
			return $inventoryDetail->getQuantity();
		});
	}

	public function scopeHasGoodStock($query)
	{
		// return $query->inventoryDetails()->condition('Good')->where('quantity', '>', 0);

		return $query->whereHas('inventoryDetails', function($inventoryDetails) {
			return $inventoryDetails->condition('Good')->where('quantity', '>', 0);
		});
	}
}
