<?php

namespace App\Modules\Inventory\Transformers;

use App\Modules\Inventory\Models\Inventory;
use App\Modules\InventoryDetail\Transformers\InventoryDetailTransformer;
use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\Uom\Transformer\UomTransformer;
use League\Fractal;

class InventoryTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'inventoryDetails',
        'product',
        'uom'

    ];

    public function transform(Inventory $inventory)
    {
        return [
            'id' => (int) $inventory->getId(),
            'stock' => $inventory->getStock(),
            'badStock' => $inventory->getBadStock()
        ];
    }
    public function includeUom(Inventory $inventory)
    {
        $uom = $inventory->getUom();
        return $this->item($uom, new UomTransformer);
    }
    public function includeProduct(Inventory $inventory)
    {
        $product = $inventory->getProduct();
        return $this->item($product, new ProductTransformer);
    }

    public function includeInventoryDetails(Inventory $inventory)
    {
        $inventoryDetails = $inventory->getInventoryDetails();
        return $this->collection($inventoryDetails, new InventoryDetailTransformer);
    }
}
