<?php

Breadcrumbs::register('inventory-menu-dashboard.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Inventory', route('inventory-menu-dashboard_index'));
});

Breadcrumbs::register('inventory.coil.index', function ($breadcrumbs) {
    $breadcrumbs->parent('inventory-menu-dashboard.index');
    $breadcrumbs->push('Coil', route('inventory_coil_index'));
});

Breadcrumbs::register('inventory.accessory.index', function ($breadcrumbs) {
    $breadcrumbs->parent('inventory-menu-dashboard.index');
    $breadcrumbs->push('Accessory', route('inventory_accessory_index'));
});

Breadcrumbs::register('inventory.damaged.index', function ($breadcrumbs) {
    $breadcrumbs->parent('inventory-menu-dashboard.index');
    $breadcrumbs->push('Damaged', route('inventory_damaged_index'));
});

Breadcrumbs::register('inventory_create', function ($breadcrumbs) {
    $breadcrumbs->parent('inventory_index');
    $breadcrumbs->push('Create', route('inventory_create'));
});

Breadcrumbs::register('inventory_show', function ($breadcrumbs, $name) {
    $breadcrumbs->parent('inventory_index');
    $breadcrumbs->push($name, route('inventory_show', 1));
});
