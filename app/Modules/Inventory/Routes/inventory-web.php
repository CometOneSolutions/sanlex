<?php

Route::group(['prefix' => 'inventory'], function () {
    Route::get('/', 'InventoryController@dashboard')->name('inventory-menu-dashboard_index');

    Route::group(['prefix' => 'stock'], function () {
        Route::get('coil', 'InventoryController@coilIndex')->name('inventory_coil_index');
        Route::get('coil/archived', 'InventoryController@coilIndexArchived')->name('inventory_coil_index_archived');
        Route::get('coil/{inventoryDetailId}', 'InventoryController@moveCoil')->name('inventory_coil_move');
        Route::post('coil/{inventoryDetailId}', 'InventoryController@transferCoil')->name('inventory_coil_move_post');
        Route::get('accessory', 'InventoryController@accessoryIndex')->name('inventory_accessory_index');
        Route::get('accessory/archived', 'InventoryController@accessoryIndexArchived')->name('inventory_accessory_index_archived');
        Route::get('damaged', 'InventoryController@indexOfDamaged')->name('inventory_damaged_index');
        Route::get('create', 'InventoryController@create')->name('inventory_create');

        Route::get('{inventory}/adjustments', '\App\Modules\Adjustment\Controllers\AdjustmentController@index')->name('adjustment_index');
        Route::get('{inventory}/adjustments/create', '\App\Modules\Adjustment\Controllers\AdjustmentController@create')->name('adjustment_create');
    Route::get('{inventory}/conversations', '\App\Http\Controllers\ConversionController@index')->name('conversion_index');
        Route::get('{inventory}/conversations/create', '\App\Http\Controllers\ConversionController@create')->name('conversion_create');
    });


    Route::group(['prefix' => 'pricing'], function () {
        Route::get('/', 'InventoryController@price')->name('inventory-price_index');
    });
});

Route::group(['prefix' => 'reports'], function () {
    Route::get('/', '\App\Http\Controllers\ReportMenuDashboardController@dashboard')->name('report-menu-dashboard_index');
});

Route::group(['prefix' => 'out'], function () {
    Route::get('/', '\App\Http\Controllers\OutMenuDashboardController@dashboard')->name('out-menu-dashboard_index');

    Route::group(['prefix' => 'delivery-receipts'], function () {
        Route::get('/', '\App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptController@index')->name('delivery-receipt_index');
        Route::get('/pending', '\App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptController@indexPending')->name('delivery-receipt_pending');
        Route::get('/export', '\App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptController@export')->name('delivery-receipt_export');
        Route::get('/create', '\App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptController@create')->name('delivery-receipt_create');
        Route::get('{saleId}/edit', '\App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptController@edit')->name('delivery-receipt_edit');
        Route::get('{saleId}/print', '\App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptController@print')->name('delivery-receipt_print');
        Route::get('{saleId}', '\App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptController@show')->name('delivery-receipt_show');
    });

    Route::group(['prefix' => 'job-orders'], function () {
        Route::get('/', '\App\Modules\JobOrders\Controllers\JobOrderController@index')->name('job-order_index');
        Route::get('/pending', '\App\Modules\JobOrders\Controllers\JobOrderController@indexPending')->name('job-order_pending');
        Route::get('create', '\App\Modules\JobOrders\Controllers\JobOrderController@create')->name('job-order_create');
        Route::get('{jobreportorder}/edit', '\App\Modules\JobOrders\Controllers\JobOrderController@edit')->name('job-order_edit');
        Route::get('{jobreportorder}', '\App\Modules\JobOrders\Controllers\JobOrderController@show')->name('job-order_show');
    });
});
