<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'inventories'], function () use ($api) {
        $api->get('/', 'App\Modules\Inventory\Controllers\InventoryApiController@index');
        // $api->get('/list', 'App\Http\Controllers\InventoryApiController@index');
        $api->get('{inventoryId}', 'App\Modules\Inventory\Controllers\InventoryApiController@show');
        $api->post('/', 'App\Modules\Inventory\Controllers\InventoryApiController@store');
        $api->post('{inventoryId}/adjust', 'App\Modules\Inventory\Controllers\InventoryApiController@adjust');
        $api->patch('{inventoryId}', 'App\Modules\Inventory\Controllers\InventoryApiController@update');
        $api->patch('{inventoryId}/damaged', 'App\Modules\Inventory\Controllers\InventoryApiController@toggleToDamaged');
        $api->patch('{inventoryId}/good', 'App\Modules\Inventory\Controllers\InventoryApiController@toggleToGood');
        $api->delete('{inventoryId}', 'App\Modules\Inventory\Controllers\InventoryApiController@destroy');
    });
});
