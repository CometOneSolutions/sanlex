<?php

namespace App\Modules\Inventory\Services;

use App\Modules\Inventory\Models\Inventory;
use App\Modules\Inventory\Repositories\InventoryRepository;
use App\Modules\InventoryDetail\Models\InventoryDetailModel;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordService;
use App\Modules\Product\Models\Product;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Receiving\Models\Receiving;
use App\Modules\ReceivingAccessory\Models\ReceivingAccessory;
use App\Modules\Uom\Models\Uom;
use App\Modules\Uom\Services\UomRecordService;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Utils\Filter;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class InventoryRecordServiceImpl extends RecordServiceImpl implements InventoryRecordService
{
    private $inventories;
    private $inventory;
    private $uomRecordService;
    private $productRecordService;
    private $inventoryRepository;
    private $inventoryDetailRecordService;

    protected $availableFilters = [];

    public function __construct(InventoryRepository $inventories, Inventory $inventory, ProductRecordService $productRecordService, UomRecordService $uomRecordService, InventoryRepository $inventoryRepository, InventoryDetailRecordService $inventoryDetailRecordService)
    {
        parent::__construct($inventories);
        $this->inventories = $inventories;
        $this->inventory = $inventory;
        $this->uomRecordService = $uomRecordService;
        $this->productRecordService = $productRecordService;
        $this->inventoryRepository = $inventoryRepository;
        $this->inventoryDetailRecordService = $inventoryDetailRecordService;
    }

    public function getUomMT()
    {
        $findMTFilter = new Filter('name', 'MT');
        return $this->uomRecordService->getFirst([$findMTFilter]);
    }

    public function getUomPiece()
    {
        $findPieceFilter = new Filter('name', 'PIECE');
        return $this->uomRecordService->getFirst([$findPieceFilter]);
    }

    public function getInventoryRecord($productId)
    {
        $productIdFilter = new Filter('product_id', $productId);
        return $this->getFirst([$productIdFilter]);
    }

    public function createInventory(
        Product $product,
        User $user = null
    ) {
        $inventory = new $this->inventory;
        $inventory->initializeStock();
        $inventory->setProduct($product);
        $inventory->setUom($product->getUom());

        if ($user) {
            $inventory->setUpdatedByUser($user);
        }
        $this->inventories->save($inventory);
        return $inventory;
        // return $inventory;
    }

    public function createInventoryFromPurchaseOrder(
        PurchaseOrder $purchaseOrder,
        User $user = null
    ) {
        $receivings = $purchaseOrder->getReceivings();
        foreach ($receivings as $receiving) {
            // $this->create() // 11 PCS QTY
            // $inventory = $this->inventoryRecordService-> //Check if exisitng in inventory
            // //if yes then to add to its details
            // //If no create one inventory

            $inventoryDetail = new InventoryDetailModel();
            $inventoryDetail->setReceiving($receiving);
            $inventoryDetail->setUom($receiving->getProduct()->getUom());
            if ($receiving->getPurchaseOrder()->getProductType() == PurchaseOrder::COIL) {
                $inventoryDetail->setQuantity($receiving->getNetWeight());
            } else {
                $inventoryDetail->setQuantity($receiving->getQuantity());
            }
            $inventoryDetail->setReceivedQuantity($inventoryDetail->getQuantity());
            $inventoryDetail->setCondition(Receiving::GOOD);
            // $inventoryDetail->setInventory($inventory);
            // $this->invenoryDetailRecordService->save($inventoryDetail);
            $inventory = $this->getInventoryRecord($receiving->getProductId());

            // $inventory = $this->getInventoryRecord($receiving->getProductId());
            if (!$inventory) {
                $inventory = $this->createInventory($receiving->getProduct(), $user);
            }

            if ($receiving->getPurchaseOrder()->getProductType() == PurchaseOrder::COIL) {
                $inventoryDetail->setInventory($inventory);
                $this->inventoryDetailRecordService->create($inventoryDetail);
            } else {
                $detail = $inventory->getInventoryDetails()->first();
                if ($detail) {
                    $detail->updateAccessoryStock();
                    $detail->save();
                    $this->inventoryDetailRecordService->createFromExistingAccessory($detail, $receiving);
                } else {
                    $inventoryDetail->setInventory($inventory);
                    $this->inventoryDetailRecordService->create($inventoryDetail);
                    $inventory = $this->getInventoryRecord($receiving->getProductId());
                }
            }

            $inventory->updateStock();
            $this->inventories->save($inventory);
        }
        // $this->inventories->save($inventory);
        // return $inventory;
    }

    public function createInventoryFromReceivingAccessory(
        ReceivingAccessory $receivingAccessory,
        User $user = null
    ) {
        $receivingAccessoryDetails = $receivingAccessory->getReceivingAccessoryDetails();

        foreach ($receivingAccessoryDetails as $receivingAccessoryDetail) {
            $inventoryDetail = new InventoryDetailModel();
            $inventoryDetail->setReceivingAccessoryDetail($receivingAccessoryDetail);
            $inventoryDetail->setUom($receivingAccessoryDetail->getProduct()->getUom());
            $inventoryDetail->setQuantity($receivingAccessoryDetail->getQuantity());
            $inventoryDetail->setReceivedQuantity($inventoryDetail->getQuantity());
            $inventoryDetail->setCondition(Receiving::GOOD);

            $inventory = $this->getInventoryRecord($receivingAccessoryDetail->getProductId());

            if (!$inventory) {
                $inventory = $this->createInventory($receivingAccessoryDetail->getProduct(), $user);
            }

            $detail = $inventory->getInventoryDetails()->first();

            if ($detail) {
                $detail->updateAccessoryStock();
                $detail->save();
                $this->inventoryDetailRecordService->createFromExistingAccessory($detail, $receivingAccessoryDetail);
            } else {
                $inventoryDetail->setInventory($inventory);
                $this->inventoryDetailRecordService->create($inventoryDetail);
                $inventory = $this->getInventoryRecord($receivingAccessoryDetail->getProductId());
            }

            $inventory->updateStock();
            $this->inventories->save($inventory);
        }
    }

    public function update(
        Inventory $inventory,
        Product $product,
        Uom $uom,
        $stock,
        User $user = null
    ) {
        $tempInventory = clone $product;
        $tempInventory->updateStock();
        $tempInventory->setProduct($product);
        $tempInventory->setUom($uom);
        if ($user) {
            $tempInventory->setUpdatedByUser($user);
        }
        $this->inventories->save($tempInventory);
        return $tempProduct;
    }

    public function delete(Inventory $inventory)
    {
        $this->inventories->delete($inventory);
        return $inventory;
    }

    public function getSummary()
    {
        return ($this->inventories->getSummary());
    }

    public function findOrCreate(Product $product, User $user = null)
    {
        $productFilter = new Filter('product_id', $product->getId(), '=');
        $inventory = $this->getFirst([$productFilter]);

        if (!$inventory) {
            $inventory = $this->createInventory($product, $user);
        }
        return $inventory;
    }

    public function getAllInventoriesWithDuplicates()
    {
        $inventories = $this->inventory->accessories()->get();
        // $numbers = [];
        foreach ($inventories as $inventory) {
            // if ($inventory->getInventoryDetails()->count() > 1) {
            // 	$numbers[] = $inventory->getId();
            // }
            foreach ($inventory->getInventoryDetails() as $detail) {
                $detail->updateAccessoryStock();
                $detail->save();
            }
        }
    }
}
