<?php

namespace App\Modules\Inventory\Services;

use App\Modules\Inventory\Models\Inventory;
use App\Modules\Product\Models\Product;
use App\Modules\Uom\Models\Uom;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface InventoryRecordService extends RecordService
{
    // public function create($product, Uom $uom, $stock, User $user = null);
    public function createInventory(Product $product, User $user = null);
    public function update(Inventory $inventory, Product $product, Uom $uom, $stock, User $user = null);
    public function delete(Inventory $inventory);
    public function getSummary();
    public function findOrCreate(Product $product, User $user = null);
}
