<?php

namespace App\Modules\Inventory\Repositories;



use CometOneSolutions\Common\Repositories\Repository;
use App\Modules\Inventory\Models\Inventory;

interface InventoryRepository extends Repository
{
    public function save(Inventory $inventory);
    public function delete(Inventory $inventory);
}
