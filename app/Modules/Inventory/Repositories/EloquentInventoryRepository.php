<?php

namespace App\Modules\Inventory\Repositories;


use App\Modules\Color\Models\Color;
use App\Modules\Inventory\Models\Inventory;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use CometOneSolutions\Common\Repositories\EloquentRepository;
use Illuminate\Support\Facades\DB;

class EloquentInventoryRepository extends EloquentRepository implements InventoryRepository
{
    public function __construct(Inventory $inventory)
    {
        parent::__construct($inventory);
    }

    public function save(Inventory $inventory)
    {
        $inventory->save();
        // $inventory->inventoryDetails()->sync($inventory->getInventoryDetails());
        return $inventory;
    }

    public function delete(Inventory $inventory)
    {
        return $inventory->delete();
    }

    // public function coil()
    // {
    //     $this->model = $this->model->whereHas('product', function ($product) {
    //         return $product->coil();
    //     });
    //     return $this;
    // }

    public function supplier(Supplier $supplier)
    {
        $this->model = $this->model->whereHas('product', function ($product) use ($supplier) {
            return $product->supplier($supplier);
        });
        return $this;
    }

    public function inStockOrHasPending()
    {
        $this->model = $this->model->hasPending()->orWhere('stock', '>', 0);
        return $this;
    }

    public function coilColor(Color $color)
    {
        $this->model = $this->model->whereHas('product', function ($product) use ($color) {
            return $product->coilColor($color);
        });
        return $this;
    }

    public function coilThickness(Thickness $thickness)
    {
        $this->model = $this->model->whereHas('product', function ($product) use ($thickness) {
            return $product->coilThickness($thickness);
        });
        return $this;
    }

    public function getSummary()
    {
        $pending = DB::table("inventories")
            ->select(DB::raw("
                                            inventories.id as id,
                                            purchase_order_details.received_quantity as pending
                                        "))
            ->join('products', 'products.id', '=', 'inventories.product_id')
            ->join('purchase_order_details', 'purchase_order_details.product_id', '=', 'inventories.product_id')
            ->join('purchase_orders', 'purchase_order_details.purchase_order_id', '=', 'purchase_orders.id')
            ->where("purchase_orders.status", "=", "In Transit")
            ->where("purchase_orders.product_type", "=", "Coils")

            ->get();


        $in_stock = DB::table("coils")
            ->select(DB::raw("
                                            inventories.id as id,
                                            widths.name as width,
                                            coatings.name as coating,
                                            thicknesses.name as thickness,
                                            colors.name as color,
                                            suppliers.short_code as supplier,
                                            inventories.stock as stock,
                                            0 as pending
                                            
                                        "))
            ->join('products', 'products.productable_id', '=', 'coils.id')
            ->join('suppliers', 'products.supplier_id', '=', 'suppliers.id')
            ->join('purchase_orders', 'purchase_orders.supplier_id', '=', 'suppliers.id')
            ->join('widths', 'coils.width_id', '=', 'widths.id')
            ->join('coatings', 'coils.coating_id', '=', 'coatings.id')
            ->join('thicknesses', 'coils.thickness_id', '=', 'thicknesses.id')
            ->join('colors', 'coils.color_id', '=', 'colors.id')
            ->join('inventories', 'products.id', '=', 'inventories.product_id')
            ->join('inventory_details', 'inventories.id', '=', 'inventory_details.inventory_id')
            ->where("inventories.stock", ">", 0)
            ->where("purchase_orders.product_type", "=", "Coils")
            ->where("inventory_details.condition", "=", "Good")
            ->groupBy('id')
            ->get();


        $data = ($this->collate($in_stock, $pending));
        return $data;
    }

    private function collate($in_stock, $pending)
    {
        foreach ($in_stock as $item) {
            $item->pending = $this->getPendingSum($item->id, $pending);
        }
        return ($in_stock);
    }

    public function filterByProductName($productName)
    {
        return $this->model->whereHas('product', function ($product) use ($productName) {
            return $product->where('name', 'like', '%' . $productName . '%');
        });
    }



    private function getPendingSum($id, $pending)
    {
        $filtered = $pending->filter(function ($item) use ($id) {
            return $item->id == $id;
        });
        return ($filtered->sum('pending'));
    }
}
