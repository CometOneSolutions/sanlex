import {Form} from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm';
import Select2 from '@c1_common_js/components/Select2';


new Vue({
    el: '#inventory',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select2
    },
    data: {
        form: new Form({
           id: null
        }),
        inventories: [],
        isSaving: false,
        isBusy: false,
        isDeleting: false,
        isBusyDeleting: false,
        isFetching: false,
        dataInitialized: true, 
    },
 
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
        
        
    },

    computed: {

        initializationComplete() {
            return this.dataInitialized;
        }
       
    },
    methods: {
        store(inventory) {
            this.form.id = inventory.id;
            this.form.price = inventory.unitPrice;
            
            this.form.post('/api/inventories')
            .then(response => {
                this.$swal({
                    title: 'Success',
                    text: 'Price has been set.',
                    type: 'success'
                }).then(() => window.location = '/inventory');
                
            }).catch(error => {
                this.isSaving = false;
                this.isBusy = false;
            });
        },
        loadData(data) {
            this.form = new Form({id: null, price: 0});
            this.inventories = data;
        },
    },
    created() {    
        
        this.dataInitialized = false;
        // Fetch supplier
        this.form.get('/api/inventories').then(response => {
            this.loadData(response.data);
            this.dataInitialized = true;
        });
        
    },
    mounted() {
    
        console.log("Init inventory script...");
        


    }
});