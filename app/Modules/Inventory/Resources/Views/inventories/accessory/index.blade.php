@extends('app')
@section('breadcrumbs', Breadcrumbs::render('inventory.accessory.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Inventory
        </div>
        <div class="card-body">

            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending" v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('product_name')">
                                    Product <i class="fas fa-sort" :class="getSortIcon('product_name')"></i>
                                </a>
                            </th>
                            <th class="text-center">Type</th>
                            <th><a v-on:click="setSorter('created_at')">
                                    Created At <i class="fas fa-sort" :class="getSortIcon('created_at')"></i>
                                </a></th>
                            <th class="text-right"> <a v-on:click="setSorter('quantity')">
                                    Stock <i class="fas fa-sort" :class="getSortIcon('quantity')"></i>
                                </a></th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>

                    <tbody v-if="items.length > 0">

                        <tr v-for="item in items" v-if="!isLoading">
                            <td>@{{item.receivingAccessoryDetail.data.productName}}</td>
                            <td class="text-center">@{{item.inventory.data.product.data.type}}</td>
                            <td>@{{item.createdAt}}</td>
                            <td class="text-right">@{{ item.quantity | numeric(2) }}</td>
                            <td class="text-center">
                                <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" :data-target="item.anchor"><i class="fas fa-pencil-alt"></i></button>

                                <div class="modal fade" :id="item.target" role="dialog" aria-labelledby="downpaymentLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleDownpaymentlLabel">Edit
                                                    @{{item.receivingAccessoryDetail.data.productName}} <span class="text-warning" v-if="item.slitIdentifier != null"><b>*</b></span></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <v-form @validate="adjust(item)">
                                                <div class="modal-body">
                                                    <div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
                                                        <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
                                                            @{{ error[0] }}
                                                        </small>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1">From</span>
                                                                </div>
                                                                <money class="form-control text-right" v-model.number="item.quantity" disabled v-bind="moneyConfig"></money>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <br>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1">To</span>
                                                                </div>
                                                                <money class="form-control text-right" v-model.number="item.adjustedQuantity" v-bind="moneyConfig"></money>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1">Remarks</span>
                                                                    <input type="text" class="form-control" placeholder="" v-model="form.remarks">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                                    <save-button :is-busy="form.isBusy" :is-saving="form.isSaving">
                                                    </save-button>
                                                </div>
                                            </v-form>
                                        </div>
                                    </div>
                                </div>

                                <button type="button" class="btn btn-outline-warning btn-sm" @click="toggleToDamaged(item)"><i class="far fa-trash-alt"></i></button>
                                <a :href="item.transactionUri" class="btn btn-outline-secondary btn-sm"><i class="fas fa-history"></i></a>

                            </td>
                        </tr>

                    </tbody>

                </table>
            </index>

        </div>
    </div>
</div>

<!-- End Watchlist-->
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush