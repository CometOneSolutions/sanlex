@extends('app')
@section('breadcrumbs', Breadcrumbs::render('inventory.coil.index'))
@section('content')
<div id="index" v-cloak>

    <div class="card">

        <div class="card-header">
            Inventory
            <div class="float-right">
                <button type="button" data-toggle="modal" data-target="#restock" class="btn btn-success btn-sm"><i class="fas fa-plus"></i></a>
            </div>
            <div class="modal fade" id="restock" role="dialog" aria-labelledby="restock" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Manual Restocking</i></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <v-form @validate="store">
                            <div class="modal-body">
                                <div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
                                    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
                                        @{{ error[0] }}
                                    </small>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="coil" class="col-form-label">Product <span class="text text-danger">*</span></label>
                                            <select3 class="custom-select" required data-msg-required="Enter product" v-model="form.productId" id="product" name="product" :url="url" search="name">
                                            </select3>
                                        </div>
                                    </div>
                                </div>


                                <table class="table table-condensed table-bordered">
                                    <tr>
                                        <th>Quantity</th>
                                        <th>Identifier</th>
                                        <th class="text-center">Action</th>

                                    </tr>
                                    <tr v-for="(detail,index) in this.form.details">
                                        <td>
                                            <money class="form-control text-right" v-model.number="detail.quantity" v-bind="moneyConfig" required></money>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" v-model="detail.identifier" required placeholder="Coil Number + A,B,C...">

                                        </td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-outline-danger btn-sm" @click="removeFromDetails(index)"><i class="fa fa-times" aria-hidden="true"></i></button>
                                        </td>
                                    </tr>
                                </table>
                                <div class="row">
                                    <div class="col-12">
                                        <button type="button" class="btn btn-success btn-sm" @click="addToInventory">Add New</button>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                            </div>
                        </v-form>

                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">
            @if (session('status'))
            <div class="alert alert-danger">
                {{ session('status') }}
            </div>
            @endif
            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending" v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('product_name')">
                                    Product <i class="fas fa-sort" :class="getSortIcon('product_name')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('coil_number')">
                                    Coil Number <i class="fas fa-sort" :class="getSortIcon('coil_number')"></i>
                                </a>
                            </th>
                            <th>

                                Slit Identifier

                            </th>
                            <th>
                                <a v-on:click="setSorter('created_at')">
                                    Created At <i class="fas fa-sort" :class="getSortIcon('created_at')"></i>
                                </a>
                            </th>
                            <th class="text-right">
                                <a v-on:click="setSorter('quantity')">
                                    Stock <i class="fas fa-sort" :class="getSortIcon('quantity')"></i>
                                </a>
                            </th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>

                    <tbody v-if="items.length > 0">

                        <tr v-for="item in items" v-if="!isLoading">
                            <td>@{{item.inventory.data.product.data.name}}</td>
                            <td>@{{item.receiving.data.coilNumber || '--'}}</td>
                            <td>@{{item.slitIdentifier || '--'}} <span class="text-warning" v-if="item.slitIdentifier != null"><b>*</b></span></td>
                            <td>@{{item.createdAt}}</td>
                            <td class="text-right">@{{ item.quantity | numeric(3) }}</td>
                            <td class="text-center">
                                @if(auth()->user()->can('Adjust [INV] Inventory'))
                                <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" :data-target="item.anchor"><i class="fas fa-pencil-alt"></i></button>
                                @endif
                                <div class="modal fade" :id="item.target" role="dialog" aria-labelledby="downpaymentLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleDownpaymentlLabel">Edit
                                                    @{{item.receiving.data.coilNumber || item.slitIdentifier}} <span class="text-warning" v-if="item.slitIdentifier != null"><b>*</b></span></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <v-form @validate="adjust(item)">
                                                <div class="modal-body">
                                                    <div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
                                                        <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
                                                            @{{ error[0] }}
                                                        </small>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1">From</span>
                                                                </div>
                                                                <money class="form-control text-right" v-model.number="item.quantity" disabled v-bind="moneyConfig"></money>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <br>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1">To</span>
                                                                </div>
                                                                <money class="form-control text-right" v-model.number="item.adjustedQuantity" v-bind="moneyConfig"></money>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1">Remarks</span>
                                                                    <input type="text" class="form-control" placeholder="" v-model="form.remarks">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                                    <save-button :is-busy="form.isBusy" :is-saving="form.isSaving">
                                                    </save-button>
                                                </div>
                                            </v-form>
                                        </div>
                                    </div>
                                </div>
                                @if(auth()->user()->can('Toggle-Condition [INV] Inventory'))
                                <button type="button" class="btn btn-outline-warning btn-sm" @click="toggleToDamaged(item)"><i class="far fa-trash-alt"></i></button>
                                @endif
                                @if(auth()->user()->can('Transfer-SKU [INV] Inventory'))
                                <a :href="item.moveUri" class="btn btn-outline-success btn-sm"><i class="far fa-share-square"></i></a>
                                @endif
                                @if(auth()->user()->can('Read [INV] Inventory'))
                                <a :href="item.transactionUri" class="btn btn-outline-secondary btn-sm"><i class="fas fa-history"></i></a>
                                @endif
                            </td>

                        </tr>

                    </tbody>

                </table>
            </index>

        </div>
    </div>
</div>

<!-- End Watchlist-->
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush