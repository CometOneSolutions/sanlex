@extends('app')
@section('breadcrumbs', Breadcrumbs::render('inventory.coil.index'))
@section('content')
<div>

    <div class="card">

        <div class="card-header">
            Move Inventory
        </div>
        <form action="{{route('inventory_coil_move_post', $inventoryDetail->getId())}}" method="POST">
            {{ csrf_field() }}
            <div class="card-body">
                <span class="text-danger">{{ $errors->inventory->first('productId') }}</span><br>
                <div class="row">
                    <div class="col-6">
                        Move {{$inventoryDetail->getInventory()->getProduct()->getName() }} ({{($inventoryDetail->getReceiving()) ? $inventoryDetail->getReceiving()->getCoilNumber() : $inventoryDetail->getSlitIdentifier()}}) to:
                    </div>
                    <div class="col-6">
                        <select class="form-control select-two" id="productId" name="productId" :allow-custom="false">
                            <option :value="null" disabled selected>Please select product sku</option>
                            @foreach($products as $product)
                            <option value="{{$product->getId()}}">{{$product->getName()}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


            </div>
            <div class="card-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- End Watchlist-->
@stop
@push('scripts')
<script>
    $('.select-two').select2({
        theme: "bootstrap"
    });
</script>
@endpush