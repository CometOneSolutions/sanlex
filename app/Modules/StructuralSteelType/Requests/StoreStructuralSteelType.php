<?php

namespace App\Modules\StructuralSteelType\Requests;

use Dingo\Api\Http\FormRequest;

class StoreStructuralSteelType extends StructuralSteelTypeRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [MAS] Structural Steel Type');
	}
}
