<?php

namespace App\Modules\StructuralSteelType\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyStructuralSteelType extends StructuralSteelTypeRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Structural Steel Type');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
