@extends('app')
@section('breadcrumbs', Breadcrumbs::render('structural-steel-type.show', $structuralSteelType))
@section('content')
<section id="structural-steel-type">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Structural Steel Type Record
                        @if(auth()->user()->can('Update [MAS] Structural Steel Type'))
                        <div class="float-right">
                            <a :href="form.editUri"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> </button></a>
                        </div>
                        @endif
                    </div>
                    <div class="card-body">
                        @include('structural-steel-types._list')
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <timestamp :name="form.updatedByUser.data.name" :time="form.updatedAt"></timestamp>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection