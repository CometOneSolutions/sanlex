<?php

namespace App\Modules\StructuralSteelType\Services;

use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface StructuralSteelTypeRecordService extends RecordService
{
	public function create(
		$name,
		User $user = null
	);

	public function update(
		StructuralSteelType $structuralSteelType,
		$name,
		User $user = null
	);

	public function delete(StructuralSteelType $structuralSteelType);
}
