<?php

namespace App\Modules\StructuralSteelType\Services;

use App\Modules\Product\Traits\CanUpdateProductName;
use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use App\Modules\StructuralSteelType\Repositories\StructuralSteelTypeRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class StructuralSteelTypeRecordServiceImpl extends RecordServiceImpl implements StructuralSteelTypeRecordService
{
	use CanUpdateProductName;

	private $structuralSteelType;
	private $structuralSteelTypes;

	protected $availableFilters = [
		["id" => "name", "text" => "Name"]
	];

	public function __construct(StructuralSteelTypeRepository $structuralSteelTypes, StructuralSteelType $structuralSteelType)
	{
		$this->structuralSteelTypes = $structuralSteelTypes;
		$this->structuralSteelType = $structuralSteelType;
		parent::__construct($structuralSteelTypes);
	}

	public function create(
		$name,
		User $user = null
	) {
		$structuralSteelType = new $this->structuralSteelType;
		$structuralSteelType->setName($name);

		if ($user) {
			$structuralSteelType->setUpdatedByUser($user);
		}
		$this->structuralSteelTypes->save($structuralSteelType);
		return $structuralSteelType;
	}

	public function update(
		StructuralSteelType $structuralSteelType,
		$name,
		User $user = null
	) {
		$tempStructuralSteelType = clone $structuralSteelType;
		$tempStructuralSteelType->setName($name);
		if ($user) {
			$tempStructuralSteelType->setUpdatedByUser($user);
		}
		$this->structuralSteelTypes->save($tempStructuralSteelType);

		$this->updateProductableProductNames([
			$tempStructuralSteelType->getStructuralSteels()
		]);

		return $tempStructuralSteelType;
	}

	public function delete(StructuralSteelType $structuralSteelType)
	{
		$this->structuralSteelTypes->delete($structuralSteelType);
		return $structuralSteelType;
	}
}
