<?php

namespace App\Modules\StructuralSteelType\Repositories;

use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use CometOneSolutions\Common\Repositories\Repository;

interface StructuralSteelTypeRepository extends Repository
{
	public function save(StructuralSteelType $structuralSteelType);
	public function delete(StructuralSteelType $structuralSteelType);
}
