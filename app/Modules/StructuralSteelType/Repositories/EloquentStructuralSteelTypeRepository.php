<?php

namespace App\Modules\StructuralSteelType\Repositories;

use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentStructuralSteelTypeRepository extends EloquentRepository implements StructuralSteelTypeRepository
{
	public function __construct(StructuralSteelType $stainlessSteelType)
	{
		parent::__construct($stainlessSteelType);
	}

	public function save(StructuralSteelType $stainlessSteelType)
	{
		return $stainlessSteelType->save();
	}

	public function delete(StructuralSteelType $stainlessSteelType)
	{
		return $stainlessSteelType->delete();
	}
}
