<?php

namespace App\Modules\StructuralSteelType\Models;

use App\Modules\DimensionX\Models\DimensionXModel;
use App\Modules\DimensionY\Models\DimensionYModel;
use App\Modules\Length\Models\LengthModel;
use App\Modules\StructuralSteel\Models\StructuralSteelModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Models\Users\User;

class StructuralSteelTypeModel extends Model implements StructuralSteelType, UpdatableByUser
{
	use HasUpdatedByUser;

	protected $table = 'structural_steel_types';

	public function structuralSteelDimensionXs()
	{
		return $this->belongsToMany(DimensionXModel::class, 'structural_steels', 'structural_steel_type_id', 'dimension_x_id');
	}

	public function structuralSteelDimensionYs()
	{
		return $this->belongsToMany(DimensionYModel::class, 'structural_steels', 'structural_steel_type_id', 'dimension_y_id');
	}

	public function structuralSteelLengths()
	{
		return $this->belongsToMany(LengthModel::class, 'structural_steels', 'structural_steel_type_id', 'length_id');
	}

	public function getStructuralSteelDimensionXs()
	{
		return $this->structuralSteelDimensionXs;
	}

	public function getStructuralSteelDimensionYs()
	{
		return $this->structuralSteelDimensionYs;
	}

	public function getStructuralSteelLengths()
	{
		return $this->structuralSteelLengths;
	}


	public function structuralSteels()
	{
		return $this->hasMany(StructuralSteelModel::class, 'structural_steel_type_id');
	}

	public function getStructuralSteels()
	{
		return $this->structuralSteels;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($value)
	{
		$this->name = $value;
		return $this;
	}
}
