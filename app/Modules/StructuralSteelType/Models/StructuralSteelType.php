<?php

namespace App\Modules\StructuralSteelType\Models;



interface StructuralSteelType
{
	public function getId();

	public function getName();

	public function setName($value);
}
