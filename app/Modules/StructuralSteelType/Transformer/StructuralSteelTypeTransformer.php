<?php

namespace App\Modules\StructuralSteelType\Transformer;

use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class StructuralSteelTypeTransformer extends UpdatableByUserTransformer
{
	public function transform(StructuralSteelType $structuralSteelType)
	{
		return [
			'id' => (int) $structuralSteelType->getId(),
			'text' => $structuralSteelType->getName(),
			'name' => $structuralSteelType->getName(),
			'updatedAt' => $structuralSteelType->updated_at,
			'editUri' => route('structural-steel-type_edit', $structuralSteelType->getId()),
			'showUri' => route('structural-steel-type_show', $structuralSteelType->getId()),
		];
	}
}
