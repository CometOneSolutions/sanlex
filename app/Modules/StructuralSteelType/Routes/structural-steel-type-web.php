<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'structural-steel-types'], function () {
		Route::get('/', 'StructuralSteelTypeController@index')->name('structural-steel-type_index');
		Route::get('/create', 'StructuralSteelTypeController@create')->name('structural-steel-type_create');
		Route::get('{structuralSteelTypeId}/edit', 'StructuralSteelTypeController@edit')->name('structural-steel-type_edit');
		Route::get('{structuralSteelTypeId}/print', 'StructuralSteelTypeController@print')->name('structural-steel-type_print');
		Route::get('{structuralSteelTypeId}', 'StructuralSteelTypeController@show')->name('structural-steel-type_show');
	});
});
