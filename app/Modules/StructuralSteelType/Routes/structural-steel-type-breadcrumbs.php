<?php

Breadcrumbs::register('structural-steel-type.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Structural Steel Types', route('structural-steel-type_index'));
});
Breadcrumbs::register('structural-steel-type.create', function ($breadcrumbs) {
	$breadcrumbs->parent('structural-steel-type.index');
	$breadcrumbs->push('Create', route('structural-steel-type_create'));
});
Breadcrumbs::register('structural-steel-type.show', function ($breadcrumbs, $structuralSteelType) {
	$breadcrumbs->parent('structural-steel-type.index');
	$breadcrumbs->push($structuralSteelType->getName(), route('structural-steel-type_show', $structuralSteelType->getId()));
});
Breadcrumbs::register('structural-steel-type.edit', function ($breadcrumbs, $structuralSteelType) {
	$breadcrumbs->parent('structural-steel-type.show', $structuralSteelType);
	$breadcrumbs->push('Edit', route('structural-steel-type_edit', $structuralSteelType->getId()));
});
