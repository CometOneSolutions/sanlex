<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'structural-steel-types'], function () use ($api) {
		$api->get('/', 'App\Modules\StructuralSteelType\Controllers\StructuralSteelTypeApiController@index');
		$api->get('{structuralSteelTypeId}', 'App\Modules\StructuralSteelType\Controllers\StructuralSteelTypeApiController@show');
		$api->post('/', 'App\Modules\StructuralSteelType\Controllers\StructuralSteelTypeApiController@store');
		$api->patch('{structuralSteelTypeId}', 'App\Modules\StructuralSteelType\Controllers\StructuralSteelTypeApiController@update');
		$api->delete('{structuralSteelTypeId}', 'App\Modules\StructuralSteelType\Controllers\StructuralSteelTypeApiController@destroy');
	});
});
