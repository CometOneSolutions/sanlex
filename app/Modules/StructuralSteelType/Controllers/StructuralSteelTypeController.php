<?php

namespace App\Modules\StructuralSteelType\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\StructuralSteelType\Services\StructuralSteelTypeRecordService;


class StructuralSteelTypeController extends Controller
{
	private $service;

	public function __construct(StructuralSteelTypeRecordService $structuralSteelTypeRecordService)
	{
		$this->service = $structuralSteelTypeRecordService;
		$this->middleware('permission:Read [MAS] Structural Steel Type')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Structural Steel Type')->only('create');
		$this->middleware('permission:Update [MAS] Structural Steel Type')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'name',
			'sortAscending' => true,
			'baseUrl' => '/api/structural-steel-types'
		]);
		return view('structural-steel-types.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('structural-steel-types.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$structuralSteelType = $this->service->getById($id);
		return view('structural-steel-types.show', compact('structuralSteelType'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$structuralSteelType = $this->service->getById($id);
		return view('structural-steel-types.edit', compact('structuralSteelType'));
	}
}
