<?php

namespace App\Modules\StructuralSteelType\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\StructuralSteelType\Services\StructuralSteelTypeRecordService;
use App\Modules\StructuralSteelType\Transformer\StructuralSteelTypeTransformer;
use App\Modules\StructuralSteelType\Requests\StoreStructuralSteelType;
use App\Modules\StructuralSteelType\Requests\UpdateStructuralSteelType;
use App\Modules\StructuralSteelType\Requests\DestroyStructuralSteelType;

class StructuralSteelTypeApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;

	public function __construct(
		StructuralSteelTypeRecordService $structuralSteelTypeRecordService,
		StructuralSteelTypeTransformer $transformer
	) {
		$this->middleware('auth:api');
		parent::__construct($structuralSteelTypeRecordService, $transformer);
		$this->service = $structuralSteelTypeRecordService;
		$this->transformer = $transformer;
	}

	public function store(StoreStructuralSteelType $request)
	{
		$structuralSteelType = \DB::transaction(function () use ($request) {
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->create(
				$name,
				$user
			);
		});
		return $this->response->item($structuralSteelType, $this->transformer)->setStatusCode(201);
	}

	public function update($structuralSteelTypeId, UpdateStructuralSteelType $request)
	{
		$structuralSteelType = \DB::transaction(function () use ($structuralSteelTypeId, $request) {
			$structuralSteelType   = $this->service->getById($structuralSteelTypeId);
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->update(
				$structuralSteelType,
				$name,

				$user
			);
		});
		return $this->response->item($structuralSteelType, $this->transformer)->setStatusCode(200);
	}

	public function destroy($structuralSteelTypeId, DestroyStructuralSteelType $request)
	{
		$structuralSteelType = $this->service->getById($structuralSteelTypeId);
		$this->service->delete($structuralSteelType);
		return $this->response->item($structuralSteelType, $this->transformer)->setStatusCode(200);
	}
}
