<?php

namespace App\Modules\StructuralSteelType\Provider;

use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use App\Modules\StructuralSteelType\Models\StructuralSteelTypeModel;
use App\Modules\StructuralSteelType\Repositories\EloquentStructuralSteelTypeRepository;
use App\Modules\StructuralSteelType\Repositories\StructuralSteelTypeRepository;
use App\Modules\StructuralSteelType\Services\StructuralSteelTypeRecordService;
use App\Modules\StructuralSteelType\Services\StructuralSteelTypeRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class StructuralSteelTypeServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/StructuralSteelType/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(StructuralSteelTypeRecordService::class, StructuralSteelTypeRecordServiceImpl::class);
		$this->app->bind(StructuralSteelTypeRepository::class, EloquentStructuralSteelTypeRepository::class);
		$this->app->bind(StructuralSteelType::class, StructuralSteelTypeModel::class);
	}
}
