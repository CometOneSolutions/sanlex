<?php

namespace App\Modules\Thickness\Models;

use App\Modules\Coil\Models\CoilModel;
use App\Modules\Insulation\Models\InsulationModel;
use App\Modules\Skylight\Models\SkylightModel;
use App\Modules\StainlessSteel\Models\StainlessSteelModel;
use App\Modules\StructuralSteel\Models\StructuralSteelModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Models\Users\User;

class ThicknessModel extends Model implements Thickness, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'thicknesses';

    public function coils()
    {
        return $this->hasMany(CoilModel::class, 'thickness_id');
    }

    public function structuralSteels()
    {
        return $this->hasMany(StructuralSteelModel::class, 'thickness_id');
    }

    public function stainlessSteels()
    {
        return $this->hasMany(StainlessSteelModel::class, 'thickness_id');
    }

    public function skylights()
    {
        return $this->hasMany(SkylightModel::class, 'thickness_id');
    }

    public function insulations()
    {
        return $this->hasMany(InsulationModel::class, 'thickness_id');
    }

    public function getCoils()
    {
        return $this->coils;
    }

    public function getInsulations()
    {
        return $this->insulations;
    }

    public function getSkylights()
    {
        return $this->skylights;
    }

    public function getStainlessSteels()
    {
        return $this->stainlessSteels;
    }

    public function getStructuralSteels()
    {
        return $this->structuralSteels;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
