<?php

namespace App\Modules\Thickness\Models;



interface Thickness
{
    public function getId();

    public function getName();

    public function setName($value);


}
