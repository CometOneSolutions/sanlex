<?php

namespace App\Modules\Thickness\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Thickness\Services\ThicknessRecordService;

class ThicknessController extends Controller
{
    private $service;

    public function __construct(ThicknessRecordService $thicknessRecordService)
    {
        $this->service = $thicknessRecordService;
        $this->middleware('permission:Read [MAS] Thickness')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Thickness')->only('create');
        $this->middleware('permission:Update [MAS] Thickness')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/thicknesses'
        ]);
        return view('thicknesses.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('thicknesses.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $thickness = $this->service->getById($id);
        return view('thicknesses.show', compact('thickness'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $thickness = $this->service->getById($id);
        return view('thicknesses.edit', compact('thickness'));
    }
}
