<?php

namespace App\Modules\Thickness\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Thickness\Services\ThicknessRecordService;
use App\Modules\Thickness\Transformer\ThicknessTransformer;
use App\Modules\Thickness\Requests\StoreThickness;
use App\Modules\Thickness\Requests\UpdateThickness;
use App\Modules\Thickness\Requests\DestroyThickness;

class ThicknessApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        ThicknessRecordService $thicknessRecordService,
        ThicknessTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($thicknessRecordService, $transformer);
        $this->service = $thicknessRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreThickness $request)
    {
        $thickness = \DB::transaction(function () use ($request) {
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->create(
                $name,
                $user
            );
        });
        return $this->response->item($thickness, $this->transformer)->setStatusCode(201);
    }

    public function update($thicknessId, UpdateThickness $request)
    {
        $thickness = \DB::transaction(function () use ($thicknessId, $request) {
            $thickness   = $this->service->getById($thicknessId);
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->update(
                $thickness,
                $name,
                $user
            );
        });
        return $this->response->item($thickness, $this->transformer)->setStatusCode(200);
    }

    public function destroy($thicknessId, DestroyThickness $request)
    {
        $thickness = $this->service->getById($thicknessId);
        $this->service->delete($thickness);
        return $this->response->item($thickness, $this->transformer)->setStatusCode(200);
    }

}
