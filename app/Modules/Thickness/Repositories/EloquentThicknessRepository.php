<?php

namespace App\Modules\Thickness\Repositories;

use App\Modules\Thickness\Models\Thickness;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentThicknessRepository extends EloquentRepository implements ThicknessRepository
{
    public function __construct(Thickness $thickness)
    {
        parent::__construct($thickness);
    }

    public function save(Thickness $thickness)
    {
        return $thickness->save();
    }

    public function delete(Thickness $thickness)
    {
        return $thickness->delete();
    }

}
