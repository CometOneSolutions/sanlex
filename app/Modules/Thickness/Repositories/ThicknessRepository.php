<?php

namespace App\Modules\Thickness\Repositories;

use App\Modules\Thickness\Models\Thickness;
use CometOneSolutions\Common\Repositories\Repository;

interface ThicknessRepository extends Repository
{
    public function save(Thickness $thickness);
    public function delete(Thickness $thickness);
}
