@extends('app')
@section('breadcrumbs', Breadcrumbs::render('thickness.create'))
@section('content')
<section id="thickness">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Create New Thickness
                    </div>
                   
                    <v-form
                        @validate="store">

                        <div class="card-body">
                            @include('thicknesses._form')
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                            </div>
                        </div>
                    </vform>
                </div>  
            </div>
        </div>
    </div>
</section>
<br/>
@endsection
                                