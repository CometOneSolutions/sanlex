import {Form} from '@c1_common_js/components/Form';

export const thicknessService = new Vue({

    data: function() {
        return {
            form: new Form({}),
            uri: {
              thicknesses: '/api/thicknesses?limit=' + Number.MAX_SAFE_INTEGER
            },
        }
    },   

    methods: {

        getThicknesses(){
            return this.form.get(this.uri.thicknesses);
        },
    }
});