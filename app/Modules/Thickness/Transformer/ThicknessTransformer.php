<?php

namespace App\Modules\Thickness\Transformer;

use App\Modules\Thickness\Models\Thickness;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class ThicknessTransformer extends UpdatableByUserTransformer
{
    public function transform(Thickness $thickness)
    {
        return [
            'id' => (int)$thickness->getId(),
            'text' => $thickness->getName(),
            'name' => $thickness->getName(),
            'updatedAt' => $thickness->updated_at,
            'editUri' => route('thickness_edit', $thickness->getId()),
            'showUri' => route('thickness_show', $thickness->getId()),
        ];
    }
}
