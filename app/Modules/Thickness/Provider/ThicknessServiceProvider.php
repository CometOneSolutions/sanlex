<?php

namespace App\Modules\Thickness\Provider;

use App\Modules\Thickness\Models\Thickness;
use App\Modules\Thickness\Models\ThicknessModel;
use App\Modules\Thickness\Repositories\EloquentThicknessRepository;
use App\Modules\Thickness\Repositories\ThicknessRepository;
use App\Modules\Thickness\Services\ThicknessRecordService;
use App\Modules\Thickness\Services\ThicknessRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ThicknessServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Thickness/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(ThicknessRecordService::class, ThicknessRecordServiceImpl::class);
        $this->app->bind(ThicknessRepository::class, EloquentThicknessRepository::class);
        $this->app->bind(Thickness::class, ThicknessModel::class);
    }
}
