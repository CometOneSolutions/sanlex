<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'thicknesses'], function () {
        Route::get('/', 'ThicknessController@index')->name('thickness_index');
        Route::get('/create', 'ThicknessController@create')->name('thickness_create');
        Route::get('{thicknessId}/edit', 'ThicknessController@edit')->name('thickness_edit');
        Route::get('{thicknessId}/print', 'ThicknessController@print')->name('thickness_print');
        Route::get('{thicknessId}', 'ThicknessController@show')->name('thickness_show');
    });
});
