<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'thicknesses'], function () use ($api) {
        $api->get('/', 'App\Modules\Thickness\Controllers\ThicknessApiController@index');
        $api->get('{thicknessId}', 'App\Modules\Thickness\Controllers\ThicknessApiController@show');
        $api->post('/', 'App\Modules\Thickness\Controllers\ThicknessApiController@store');
        $api->patch('{thicknessId}', 'App\Modules\Thickness\Controllers\ThicknessApiController@update');
        $api->delete('{thicknessId}', 'App\Modules\Thickness\Controllers\ThicknessApiController@destroy');
    });
});
