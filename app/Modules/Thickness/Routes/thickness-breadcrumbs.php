<?php

Breadcrumbs::register('thickness.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Thicknesses', route('thickness_index'));
});
Breadcrumbs::register('thickness.create', function ($breadcrumbs) {
    $breadcrumbs->parent('thickness.index');
    $breadcrumbs->push('Create', route('thickness_create'));
});
Breadcrumbs::register('thickness.show', function ($breadcrumbs, $thickness) {
    $breadcrumbs->parent('thickness.index');
    $breadcrumbs->push($thickness->getName(), route('thickness_show', $thickness->getId()));
});
Breadcrumbs::register('thickness.edit', function ($breadcrumbs, $thickness) {
    $breadcrumbs->parent('thickness.show', $thickness);
    $breadcrumbs->push('Edit', route('thickness_edit', $thickness->getId()));
});
