<?php

namespace App\Modules\Thickness\Requests;

use Dingo\Api\Http\FormRequest;

class StoreThickness extends ThicknessRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Thickness');
    }

}
