<?php

namespace App\Modules\Thickness\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyThickness extends ThicknessRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Thickness');
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}