<?php

namespace App\Modules\Thickness\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateThickness extends ThicknessRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Thickness');
    }

}