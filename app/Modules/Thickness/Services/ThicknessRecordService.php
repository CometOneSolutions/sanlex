<?php

namespace App\Modules\Thickness\Services;

use App\Modules\Thickness\Models\Thickness;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface ThicknessRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        Thickness $thickness,
        $name,
        User $user = null
    );

    public function delete(Thickness $thickness);
}
