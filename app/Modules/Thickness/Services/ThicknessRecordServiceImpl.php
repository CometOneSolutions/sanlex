<?php

namespace App\Modules\Thickness\Services;

use App\Modules\Product\Traits\CanUpdateProductName;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Thickness\Repositories\ThicknessRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class ThicknessRecordServiceImpl extends RecordServiceImpl implements ThicknessRecordService
{
    use CanUpdateProductName;

    private $thickness;
    private $thicknesses;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(ThicknessRepository $thicknesses, Thickness $thickness)
    {
        $this->thicknesses = $thicknesses;
        $this->thickness = $thickness;
        parent::__construct($thicknesses);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $thickness = new $this->thickness;
        $thickness->setName($name);
        if ($user) {
            $thickness->setUpdatedByUser($user);
        }
        $this->thicknesses->save($thickness);
        return $thickness;
    }

    public function update(
        Thickness $thickness,
        $name,
        User $user = null
    ) {
        $tempThickness = clone $thickness;
        $tempThickness->setName($name);
        if ($user) {
            $tempThickness->setUpdatedByUser($user);
        }
        $this->thicknesses->save($tempThickness);

        $this->updateProductableProductNames([
            $tempThickness->structuralSteels(),
            $tempThickness->stainlessSteels(),
            $tempThickness->skylights(),
            $tempThickness->insulations(),
            $tempThickness->coils(),
        ]);

        return $tempThickness;
    }

    public function delete(Thickness $thickness)
    {
        $this->thicknesses->delete($thickness);
        return $thickness;
    }
}
