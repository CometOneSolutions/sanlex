<?php

namespace App\Modules\PurchaseOrderAccessoryDetail\Transformer;

use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\PurchaseOrder\Transformer\PurchaseOrderTransformer;
use App\Modules\PurchaseOrderAccessoryDetail\Models\PurchaseOrderAccessoryDetail;
use App\Modules\ReceivingAccessory\Transformer\ReceivingAccessoryTransformer;
use League\Fractal;

class PurchaseOrderAccessoryDetailTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'purchaseOrder', 'product', 'receivingAccessories'
    ];

    public function transform(PurchaseOrderAccessoryDetail $purchaseOrderAccessoryDetail)
    {

        return [
            'id' => (int) $purchaseOrderAccessoryDetail->id,
            'productId' =>  $purchaseOrderAccessoryDetail->getProduct()->getId(),
            'productName' => $purchaseOrderAccessoryDetail->getProduct()->getName(),
            'quantity' =>  $purchaseOrderAccessoryDetail->getOrderedQuantity(),
            'receivedQuantity' => $purchaseOrderAccessoryDetail->getPurchaseOrderAccessory()->getReceivingAccessoryDetailQuantity($purchaseOrderAccessoryDetail->product_id),
            'balanceQuantity' => $purchaseOrderAccessoryDetail->getPurchaseOrderAccessory()->getPurchaseOrderAccessoryDetailBalanceQuantity($purchaseOrderAccessoryDetail->product_id),
            'uomId' => (int) $purchaseOrderAccessoryDetail->getUomId(),
            'uomName' => $purchaseOrderAccessoryDetail->getUom()->getName(),
        ];
    }

    public function includePurchaseOrder(PurchaseOrderAccessoryDetail $purchaseOrderAccessoryDetail)
    {
        $purchaseOrder = $purchaseOrderAccessoryDetail->getPurchaseOrder();
        return $this->item($purchaseOrder, new PurchaseOrderTransformer);
    }

    public function includeProduct(PurchaseOrderAccessoryDetail $purchaseOrderAccessoryDetail)
    {
        $product = $purchaseOrderAccessoryDetail->getProduct();
        return $this->item($product, new ProductTransformer);
    }


    public function includeReceivingAccessories(PurchaseOrderAccessoryDetail $purchaseOrderAccessoryDetail)
    {
        $receivingAccessories = $purchaseOrderAccessoryDetail->getReceivingAccessories();
        return $this->collection($receivingAccessories, new ReceivingAccessoryTransformer);
    }
}
