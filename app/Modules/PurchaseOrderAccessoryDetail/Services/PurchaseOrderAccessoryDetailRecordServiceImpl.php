<?php

namespace App\Modules\PurchaseOrderAccessoryDetail\Services;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\PurchaseOrderAccessoryDetail\Models\PurchaseOrderAccessoryDetail;
use App\Modules\PurchaseOrderAccessoryDetail\Repositories\PurchaseOrderAccessoryDetails;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Common\Utils\Filter;

class PurchaseOrderAccessoryDetailRecordServiceImpl extends RecordServiceImpl implements PurchaseOrderAccessoryDetailRecordService
{
    private $purchaseOrderAccessoryDetails;

    public function __construct(PurchaseOrderAccessoryDetails $purchaseOrderAccessoryDetails)
    {
        parent::__construct($purchaseOrderAccessoryDetails);
        $this->purchaseOrderAccessoryDetails = $purchaseOrderAccessoryDetails;
    }
    public function getFirstWithPurchaseOrderAndProductId(PurchaseOrder $purchaseOrder, $productId)
    {
        $purchaseOrderFilter = new Filter('purchase_order_id', $purchaseOrder->getId());
        $productIdFilter = new Filter('product_id', $productId);
        return $this->getFirst([$purchaseOrderFilter, $productIdFilter]);
    }
    public function updateInventoryQuantity(PurchaseOrderAccessoryDetail $purchaseOrderAccessoryDetail, $quantity)
    {
        $tmpPurchaseOrderAccessoryDetail = clone $purchaseOrderAccessoryDetail;
        $tmpPurchaseOrderAccessoryDetail->setInventoryQuantity($quantity);
        $this->purchaseOrderAccessoryDetails->save($tmpPurchaseOrderAccessoryDetail);
        return $tmpPurchaseOrderAccessoryDetail;
    }

    public function updateReceivedQuantity(PurchaseOrderAccessoryDetail $purchaseOrderAccessoryDetail, $quantity)
    {
        $tmpPurchaseOrderAccessoryDetail = clone $purchaseOrderAccessoryDetail;
        $tmpPurchaseOrderAccessoryDetail->setReceivedQuantity($quantity);
        $tmpPurchaseOrderAccessoryDetail->setPackingUnlistedQuantity(abs($tmpPurchaseOrderAccessoryDetail->getOrderedQuantity() - $tmpPurchaseOrderAccessoryDetail->getReceivedQuantity()));
        $this->purchaseOrderAccessoryDetails->save($tmpPurchaseOrderAccessoryDetail);
        return $tmpPurchaseOrderAccessoryDetail;
    }
}
