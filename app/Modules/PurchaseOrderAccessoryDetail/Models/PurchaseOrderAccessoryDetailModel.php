<?php

namespace App\Modules\PurchaseOrderAccessoryDetail\Models;

use App\Modules\Product\Models\Product;
use App\Modules\Product\Models\ProductModel;
use App\Modules\PurchaseOrderAccessory\Models\PurchaseOrderAccessoryModel;
use App\Modules\Uom\Models\Uom;
use App\Modules\Uom\Models\UomModel;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrderAccessoryDetailModel extends Model implements PurchaseOrderAccessoryDetail
{
    protected $table = 'purchase_order_accessory_details';

    public function product()
    {
        return $this->belongsTo(ProductModel::class, 'product_id');
    }

    public function purchaseOrderAccessory()
    {
        return $this->belongsTo(PurchaseOrderAccessoryModel::class, 'purchase_order_accessory_id');
    }

    public function getPurchaseOrderAccessory()
    {
        return $this->purchaseOrderAccessory;
    }

    public function uom()
    {
        return $this->belongsTo(UomModel::class, 'uom_id');
    }

    public function setProduct(Product $product)
    {
        $this->product()->associate($product);
        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getProductId()
    {
        return $this->product_id;
    }

    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    public function setUom(Uom $uom)
    {
        $this->uom()->associate($uom);
        return $this;
    }

    public function getUom()
    {
        return $this->uom;
    }

    public function getUomId()
    {
        return $this->uom_id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setOrderedQuantity($value)
    {
        $this->ordered_quantity = $value;
        return $this;
    }

    public function getOrderedQuantity()
    {
        return $this->ordered_quantity;
    }


    public function setReceivedQuantity($value)
    {
        $this->received_quantity = $value;
        return $this;
    }

    public function getReceivedQuantity()
    {
        return $this->received_quantity;
    }

    public function scopeProduct($query, $productId)
    {
        return $query->whereProductId($productId);
    }

    public function scopeOpen($query)
    {
        return $query->whereHas('purchaseOrderAccessory', function ($purchaseOrderAccessory) {
            return $purchaseOrderAccessory->open();
        });
    }
}
