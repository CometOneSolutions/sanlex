<?php

namespace App\Modules\PurchaseOrderAccessoryDetail\Providers;

use App\Modules\PurchaseOrderAccessoryDetail\Models\PurchaseOrderAccessoryDetail;
use App\Modules\PurchaseOrderAccessoryDetail\Models\PurchaseOrderAccessoryDetailModel;
use App\Modules\PurchaseOrderAccessoryDetail\Repositories\EloquentPurchaseOrderAccessoryDetails;
use App\Modules\PurchaseOrderAccessoryDetail\Repositories\PurchaseOrderAccessoryDetails;
use App\Modules\PurchaseOrderAccessoryDetail\Services\PurchaseOrderAccessoryDetailRecordService;
use App\Modules\PurchaseOrderAccessoryDetail\Services\PurchaseOrderAccessoryDetailRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class PurchaseOrderAccessoryDetailServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/PurchaseOrderAccessoryDetail/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(PurchaseOrderAccessoryDetailRecordService::class, PurchaseOrderAccessoryDetailRecordServiceImpl::class);
        $this->app->bind(PurchaseOrderAccessoryDetails::class, EloquentPurchaseOrderAccessoryDetails::class);
        $this->app->bind(PurchaseOrderAccessoryDetail::class, PurchaseOrderAccessoryDetailModel::class);
    }
}
