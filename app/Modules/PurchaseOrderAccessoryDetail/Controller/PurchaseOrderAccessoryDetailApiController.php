<?php

namespace App\Modules\PurchaseOrderAccessoryDetail\Controller;

use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\PurchaseOrderAccessoryDetail\Services\PurchaseOrderAccessoryDetailRecordService;
use App\Modules\PurchaseOrderAccessoryDetail\Transformer\PurchaseOrderAccessoryDetailTransformer;

class PurchaseOrderAccessoryDetailApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        PurchaseOrderAccessoryDetailRecordService $purchaseOrderRecordService,
        PurchaseOrderAccessoryDetailTransformer $transformer
    ) {
        parent::__construct($purchaseOrderRecordService, $transformer);
        $this->service = $purchaseOrderRecordService;
        $this->transformer = $transformer;
    }
}
