<?php

namespace App\Modules\PurchaseOrderAccessoryDetail\Repositories;

use CometOneSolutions\Common\Repositories\Repository;

interface PurchaseOrderAccessoryDetails extends Repository
{
    public function getTransit();
    public function getStock();
    public function getOut();
    public function renew();
}
