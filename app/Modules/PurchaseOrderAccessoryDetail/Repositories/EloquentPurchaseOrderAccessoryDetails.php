<?php

namespace App\Modules\PurchaseOrderAccessoryDetail\Repositories;

use App\Modules\PurchaseOrderAccessoryDetail\Models\PurchaseOrderAccessoryDetail;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentPurchaseOrderAccessoryDetails extends EloquentRepository implements PurchaseOrderAccessoryDetails
{
    protected static $filterable = [
        'in_stock', 'referenced_by_sales_order'
    ];

    protected $model;

    public function __construct(PurchaseOrderAccessoryDetail $purchaseOrderAccessoryDetail)
    {
        parent::__construct($purchaseOrderAccessoryDetail);
    }

    function renew()
    {
        if (!$this->model instanceof PurchaseOrderAccessoryDetail) {
            $this->model = new PurchaseOrderAccessoryDetail();
        }
        return $this;
    }

    public function getTransit()
    {
        return PurchaseOrderAccessoryDetail::TRANSIT;
    }

    public function getStock()
    {
        return PurchaseOrderAccessoryDetail::STOCK;
    }

    public function getOut()
    {
        return PurchaseOrderAccessoryDetail::OUT;
    }

    public function filterByInStock($query, $value = true)
    {
        $query->where('quantity_balance', '>', 0)->where('status', '=', 'IN_STOCK');
    }

    public function filterByReferencedBySalesOrder($query, $value)
    {
        $query->orWhereHas('salesOrderDetails', function ($salesOrderDetail) use ($value) {
            return $salesOrderDetail->whereSalesOrderId($value);
        });
    }

    public function save(PurchaseOrderAccessoryDetail $purchaseOrderAccessoryDetail)
    {
        return $purchaseOrderAccessoryDetail->save();
    }

    public function delete(PurchaseOrderAccessoryDetail $purchaseOrderAccessoryDetail)
    {
        return $purchaseOrderAccessoryDetail->delete();
    }

    // public function getById($id)
    // {
    //     $query->where('quantity_balance', '>', 0)->where('status', '=', 'IN_STOCK');
    // }
}
