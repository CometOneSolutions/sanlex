<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'purchase-order-accessory-details'], function () use ($api) {
        $api->get('/', 'App\Modules\PurchaseOrderAccessoryDetail\Controllers\PurchaseOrderAccessoryDetailApiController@index');
    });
});
