<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'adjustments'], function () use ($api) {
        $api->get('/', 'App\Modules\Adjustment\Controllers\AdjustmentApiController@index');
        $api->get('{adjustmentId}', 'App\Modules\Adjustment\Controllers\AdjustmentApiController@show');
    });
});
