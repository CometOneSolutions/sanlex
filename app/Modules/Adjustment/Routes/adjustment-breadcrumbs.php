<?php

Breadcrumbs::register('adjustment.index', function ($breadcrumbs) {
    $breadcrumbs->parent('inventory-menu-dashboard.index');
    $breadcrumbs->push('Adjustments', route('adjustment_index'));
});

Breadcrumbs::register('adjustment.show', function ($breadcrumbs, $adjustment) {
    $breadcrumbs->parent('adjustment.index');
    $breadcrumbs->push($adjustment->getId(), route('adjustment_show', $adjustment->getId()));
});
