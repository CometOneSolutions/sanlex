<?php
Route::group(['prefix' => 'inventory'], function () {

    Route::group(['prefix' => 'adjustments'], function () {
        Route::get('/', 'AdjustmentController@index')->name('adjustment_index');
        Route::get('{adjustmentId}', 'AdjustmentController@show')->name('adjustment_show');
    });

});