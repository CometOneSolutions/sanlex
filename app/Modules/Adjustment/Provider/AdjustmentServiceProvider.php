<?php

namespace App\Modules\Adjustment\Provider;

use App\Modules\Adjustment\Models\Adjustment;
use App\Modules\Adjustment\Models\AdjustmentModel;
use App\Modules\Adjustment\Repositories\AdjustmentRepository;
use App\Modules\Adjustment\Repositories\EloquentAdjustmentRepository;
use App\Modules\Adjustment\Services\AdjustmentRecordService;
use App\Modules\Adjustment\Services\AdjustmentRecordServiceImpl;
use App\Modules\Adjustment\Services\InventoryAdjustmentRecordServiceImpl;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use Illuminate\Support\ServiceProvider;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;


class AdjustmentServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Adjustment/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AdjustmentRecordService::class, AdjustmentRecordServiceImpl::class);
        $this->app->bind(AdjustmentRepository::class, EloquentAdjustmentRepository::class);
        $this->app->bind(Adjustment::class, AdjustmentModel::class);

        $this->app->singleton(AdjustmentRecordService::class, function ($app) {
            $adjustmentRecordServiceImpl = $app->make(AdjustmentRecordServiceImpl::class);
            $inventoryAdjustmentRecordServiceImpl = new InventoryAdjustmentRecordServiceImpl(
                $adjustmentRecordServiceImpl,
                $app->make(ItemMovementRecordService::class),
                $app->make(LocationRecordService::class)
            );

            return $inventoryAdjustmentRecordServiceImpl;
        });
    }
}
