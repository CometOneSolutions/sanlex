<?php

namespace App\Modules\Adjustment\Transformer;

use App\Modules\Adjustment\Models\Adjustment;
use App\Modules\InventoryDetail\Transformers\InventoryDetailTransformer;
use App\Modules\Receiving\Transformer\ReceivingTransformer;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class AdjustmentTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'inventoryDetail', 'receiving'
    ];

    public function transform(Adjustment $adjustment)
    {
        return [
            'id' => (int)$adjustment->getId(),
            'updatedAt' => $adjustment->updated_at,
            'adjustedQuantity' => $adjustment->getAdjustedQuantity(),
            'remarks'   => $adjustment->getRemarks(),
            'showUri' => route('adjustment_show', $adjustment->getId()),
            'date' => $adjustment->created_at->toDateString()
        ];
    }

    public function includeInventoryDetail(Adjustment $adjustment)
    {
        $inventoryDetail = $adjustment->getInventoryDetail();
        return $this->item($inventoryDetail, new InventoryDetailTransformer);
    }

    public function includeReceiving(Adjustment $adjustment)
    {
        $receiving = $adjustment->getReceiving();
        return $this->item($receiving, new ReceivingTransformer);
    }
}
