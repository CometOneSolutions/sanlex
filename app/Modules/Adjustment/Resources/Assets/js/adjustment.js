import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'

new Vue({
    el: '#adjustment',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm
    },
    data: {
        form: new Form({
            adjustedQuantity: null,
            remarks: null
        }),
        dataInitialized: true,
    },
    watch: {
        initializationComplete(val) {      
            this.form.isInitializing = !val;
        }
    },
    computed: {
        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
       
        loadData(data) {
            this.form = new Form(data);
        },
    },

    created(){
        
        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/adjustments/' + id + '?include=inventoryDetail.inventory.product')
            .then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
            });
        }
    },
    mounted() {
        console.log("Init adjustment script...");
    }
});