@extends('app')
@section('breadcrumbs', Breadcrumbs::render('adjustment.show', $adjustment))
@section('content')
<section id="adjustment">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Details
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="name" class="small">Product</label>
                                    <p>@{{form.inventoryDetail.data.inventory.data.product.data.name }}</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="name" class="small">Current Inventory Stock</label>
                                    <p>@{{form.inventoryDetail.data.inventory.data.stock }}</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="name" class="small">Date Adjusted</label>
                                    <p>@{{form.date || "--" }}</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="name" class="small">Adjusted Quantity</label>
                                    <p>@{{form.adjustedQuantity || "--" }}</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="name" class="small">Remarks</label>
                                    <p>@{{form.remarks || "--" }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <timestamp :name="form.updatedByUser.data.name" :time="form.updatedAt"></timestamp>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection
@push('scripts')
<script src="{{ mix('js/adjustment.js') }}"></script>
@endpush