@extends('app')
@section('breadcrumbs', Breadcrumbs::render('adjustment.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Adjustments
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sort-ascending="sortAscending" :sorter="sorter"
                v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('id')">
                                    ID <i class="fas fa-sort" :class="getSortIcon('id')"></i>
                                </a>
                            </th>
                            <th>Product</th>
                            <th>Date</th>
                            <th class="text-right">Adjusted Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{ item.id }}</a></td>
                            <td>@{{ item.inventoryDetail.data.inventory.data.product.data.name }}</td>
                            <td>@{{ item.date }}</td>
                            <td class="text-right">@{{ item.adjustedQuantity }}</td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush