<?php

namespace App\Modules\Adjustment\Models;

use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Inventory\Model\Inventoriable;
use App\Modules\InventoryDetail\Models\InventoryDetail;
use CometOneSolutions\Inventory\Traits\HasItemMovements;
use App\Modules\InventoryDetail\Models\InventoryDetailModel;



class AdjustmentModel extends Model implements Adjustment, UpdatableByUser, Inventoriable
{
    use HasUpdatedByUser;
    use HasItemMovements;

    protected $table = 'adjustments';

    public function inventoryDetail()
    {
        return $this->belongsTo(InventoryDetailModel::class, 'inventory_detail_id');
    }

    public function getId()
    {
        return $this->id;
    }

    public function setInventoryDetail(InventoryDetail $inventoryDetail)
    {
        $this->inventoryDetail()->associate($inventoryDetail);
        return $this;
    }

    public function getInventoryDetail()
    {
        return $this->inventoryDetail;
    }

    public function setAdjustedQuantity($value)
    {
        $this->adjusted_quantity = $value;
        return $this;
    }

    public function getAdjustedQuantity()
    {
        return $this->adjusted_quantity;
    }

    public function setRemarks($value)
    {
        $this->remarks = $value;
        return $this;
    }

    public function getRemarks()
    {
        return $this->remarks;
    }

    public function getDate()
    {
        return $this->created_at;
    }

    public function getInventoryRefNo()
    {
        return 'ADJ# ' . $this->getId();
    }

    public function getInventoryUri()
    {
        return route('adjustment_show', $this->getId());
    }
}
