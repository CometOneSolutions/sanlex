<?php

namespace App\Modules\Adjustment\Models;

use App\Modules\InventoryDetail\Models\InventoryDetail;

interface Adjustment
{
    public function getId();

    public function setInventoryDetail(InventoryDetail $inventory);

    public function getInventoryDetail();

    public function setAdjustedQuantity($value);

    public function getAdjustedQuantity();

    public function setRemarks($value);

    public function getRemarks();

    public function getDate();
}
