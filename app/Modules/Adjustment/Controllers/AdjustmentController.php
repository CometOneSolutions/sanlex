<?php

namespace App\Modules\Adjustment\Controllers;

use JavaScript;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\Adjustment\Services\AdjustmentRecordService;

class AdjustmentController extends Controller
{
    private $service;

    public function __construct(AdjustmentRecordService $adjustmentRecordService)
    {
        $this->middleware('auth');
        $this->service = $adjustmentRecordService;
        $this->middleware('permission:Read [INV] Inventory')->only(['show', 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/adjustments?include=inventoryDetail.inventory.product'
        ]);
        return view('adjustments.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $adjustment = $this->service->getById($id);
        return view('adjustments.show', compact('adjustment'));
    }
}
