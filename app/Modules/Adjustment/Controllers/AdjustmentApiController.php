<?php

namespace App\Modules\Adjustment\Controllers;

use App\Modules\Adjustment\Services\AdjustmentRecordService;
use App\Modules\Adjustment\Transformer\AdjustmentTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;

class AdjustmentApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        AdjustmentRecordService $adjustmentRecordService,
        AdjustmentTransformer $transformer
    ) {
        // $this->middleware('auth:api');
        $this->service = $adjustmentRecordService;
        $this->transformer = $transformer;
        parent::__construct($adjustmentRecordService, $transformer);
    }
}
