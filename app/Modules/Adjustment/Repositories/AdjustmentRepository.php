<?php

namespace App\Modules\Adjustment\Repositories;

use App\Modules\Adjustment\Models\Adjustment;
use CometOneSolutions\Common\Repositories\Repository;

interface AdjustmentRepository extends Repository
{
    public function save(Adjustment $adjustment);
}
