<?php

namespace App\Modules\Adjustment\Repositories;

use App\Modules\Adjustment\Models\Adjustment;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentAdjustmentRepository extends EloquentRepository implements AdjustmentRepository
{
    protected $adjustment;

    public function __construct(Adjustment $adjustment)
    {
        parent::__construct($adjustment);
        $this->adjustment = $adjustment;
    }

    public function save(Adjustment $adjustment)
    {
        $result = $adjustment->save();
        return $result;
    }
}
