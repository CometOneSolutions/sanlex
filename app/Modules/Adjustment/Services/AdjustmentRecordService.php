<?php

namespace App\Modules\Adjustment\Services;


use App\Modules\InventoryDetail\Models\InventoryDetail;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordService;

interface AdjustmentRecordService extends RecordService
{

    public function create(
        InventoryDetail $inventoryDetail,
        $adjustedQuantity,
        $remarks,
        User $user = null
    );
}
