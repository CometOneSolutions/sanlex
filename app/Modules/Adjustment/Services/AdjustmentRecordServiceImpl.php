<?php

namespace App\Modules\Adjustment\Services;

use App\Modules\Adjustment\Models\Adjustment;
use App\Modules\Adjustment\Repositories\AdjustmentRepository;
use App\Modules\Inventory\Repositories\InventoryRepository;
use App\Modules\InventoryDetail\Models\InventoryDetail;
use App\Modules\InventoryDetail\Repositories\InventoryDetailRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class AdjustmentRecordServiceImpl extends RecordServiceImpl implements AdjustmentRecordService
{
    private $adjustments;
    private $adjustment;

    public function __construct(
        AdjustmentRepository $adjustments,
        Adjustment $adjustment,
        InventoryDetailRepository $inventoryDetails,
        InventoryRepository $inventories
    ) {
        parent::__construct($adjustments);
        $this->adjustments = $adjustments;
        $this->adjustment = $adjustment;
        $this->inventories = $inventories;
        $this->inventoryDetails = $inventoryDetails;
    }

    public function create(
        InventoryDetail $inventoryDetail,
        $targetQuantity,
        $remarks,
        User $user = null
    ) {
        $adjustmentQuantity = $targetQuantity - $inventoryDetail->getQuantity();
        $adjustment = new $this->adjustment;
        $adjustment->setInventoryDetail($inventoryDetail);
        $adjustment->setAdjustedQuantity($adjustmentQuantity);
        $adjustment->setRemarks($remarks);
        if ($user) {
            $adjustment->setUpdatedByUser($user);
        }

        $this->adjustments->save($adjustment);
        $inventoryDetail->updateStock();
        $this->inventoryDetails->save($inventoryDetail);
        $inventory = $inventoryDetail->getInventory();
        $inventory->updateStock();
        $this->inventories->save($inventory);

        return $adjustment;
    }
}
