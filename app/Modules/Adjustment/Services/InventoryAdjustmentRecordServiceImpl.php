<?php

namespace App\Modules\Adjustment\Services;

use App\Modules\InventoryDetail\Models\InventoryDetail;
use CometOneSolutions\Inventory\Model\Inventoriable;
use CometOneSolutions\Inventory\Model\ItemMovementType;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use \DateTime;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\DecoratorRecordServiceImpl;

// In: Warehouse
// Out: Adjustments

class InventoryAdjustmentRecordServiceImpl extends DecoratorRecordServiceImpl implements AdjustmentRecordService
{
    protected $adjustmentRecordService;
    protected $itemMovementRecordService;
    protected $locationRecordService;

    public function __construct(
        AdjustmentRecordService $adjustmentRecordService,
        ItemMovementRecordService $itemMovementRecordService,
        LocationRecordService $locationRecordService
    ) {
        parent::__construct($adjustmentRecordService);
        $this->adjustmentRecordService = $adjustmentRecordService;
        $this->itemMovementRecordService = $itemMovementRecordService;
        $this->locationRecordService = $locationRecordService;
    }

    protected function createItemMovements(Inventoriable $inventoriable, InventoryDetail $inventoryDetail, DateTime $date, $quantity)
    {
        $this->itemMovementRecordService->create(
            $inventoriable,
            $date,
            ItemMovementType::IN,
            $quantity,
            $this->locationRecordService->getByName('Warehouses'),
            $inventoryDetail
        );

        $this->itemMovementRecordService->create(
            $inventoriable,
            $date,
            ItemMovementType::OUT,
            $quantity,
            $this->locationRecordService->getByName('Adjustments'),
            $inventoryDetail
        );
        return $this;
    }

    public function create(InventoryDetail $inventoryDetail, $adjustedQuantity, $remarks, User $user = null)
    {
        $created = $this->adjustmentRecordService->create($inventoryDetail, $adjustedQuantity,$remarks, $user);
        $date = new DateTime($created->getDate()->toDateTimeString());
        $this->createItemMovements($created, $inventoryDetail, $date, $created->getAdjustedQuantity());
        return $created;
    }
}
