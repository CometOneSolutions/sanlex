<?php

namespace App\Modules\Supplier\Transformer;

use App\Modules\Supplier\Models\Supplier;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class SupplierTransformer extends UpdatableByUserTransformer
{
    public function transform(Supplier $supplier)
    {
        return [
            'id' => (int)$supplier->getId(),
            'text' => $supplier->getName(),
            'name' => $supplier->getName(),
            'shortCode' => $supplier->getShortCode(),
            'contactPerson' => $supplier->getContactPerson(),
            'telephone' => $supplier->getTelephone(),
            'mobile' => $supplier->getMobile(),
            'email' => $supplier->getEmail(),
            'address' => $supplier->getAddress(),
            'updatedAt' => $supplier->updated_at,
            'editUri' => route('supplier_edit', $supplier->getId()),
            'showUri' => route('supplier_show', $supplier->getId()),
        ];
    }
}