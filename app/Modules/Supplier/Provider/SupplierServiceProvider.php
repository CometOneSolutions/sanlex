<?php

namespace App\Modules\Supplier\Provider;

use App\Modules\Supplier\Models\Supplier;
use App\Modules\Supplier\Models\SupplierModel;
use App\Modules\Supplier\Repositories\EloquentSupplierRepository;
use App\Modules\Supplier\Repositories\SupplierRepository;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\Supplier\Services\SupplierRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class SupplierServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Supplier/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(SupplierRecordService::class, SupplierRecordServiceImpl::class);
        $this->app->bind(SupplierRepository::class, EloquentSupplierRepository::class);
        $this->app->bind(Supplier::class, SupplierModel::class);
    }
}
