<?php

namespace App\Modules\Supplier\Models;

use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Models\Users\User;

class SupplierModel extends Model implements Supplier, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'suppliers';

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    public function getShortCode()
    {
        return $this->short_code;
    }

    public function setShortCode($value)
    {
        $this->short_code = $value;
        return $this;
    }

    public function getContactPerson()
    {
        return $this->contact_person;
    }

    public function setContactPerson($value)
    {
        $this->contact_person = $value;
        return $this;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone($value)
    {
        $this->telephone = $value;
        return $this;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function setMobile($value)
    {
        $this->mobile = $value;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($value)
    {
        $this->email = $value;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($value)
    {
        $this->address = $value;
        return $this;
    }
}
