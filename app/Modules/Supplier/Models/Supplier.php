<?php

namespace App\Modules\Supplier\Models;



interface Supplier
{
    public function getId();

    public function getName();

    public function setName($value);

    public function getShortCode();

    public function setShortCode($value);
}
