<?php

namespace App\Modules\Supplier\Requests;

use Dingo\Api\Http\FormRequest;

class DestroySupplier extends SupplierRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Supplier');
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}