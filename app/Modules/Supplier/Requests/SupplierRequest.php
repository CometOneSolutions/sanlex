<?php

namespace App\Modules\Supplier\Requests;

use Dingo\Api\Http\FormRequest;

class SupplierRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'                  => 'required|unique:suppliers,name,' . $this->route('supplierId') ?? null,
            'shortCode'             => 'required|unique:suppliers,short_code,' . $this->route('supplierId') ?? null,
            'email' => 'nullable|email',
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => 'Name is required',
            'name.unique'           => 'Name already exists',
            'shortCode.required'    => 'Short code is required',
            'shortCode.unique'      => 'Short code already exists',
            'email.email' => "Enter valid email address"
        ];
    }

    public function getName()
    {
        return $this->input('name');
    }

    public function getShortCode()
    {
        return $this->input('shortCode');
    }

    public function getContactPerson()
    {
        return $this->input('contactPerson');
    }

    public function getTelephone()
    {
        return $this->input('telephone');
    }

    public function getMobile()
    {
        return $this->input('mobile');
    }

    public function getEmail()
    {
        return $this->input('email');
    }

    public function getAddress()
    {
        return $this->input('address');
    }

}
