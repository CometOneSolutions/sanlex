<?php

namespace App\Modules\Supplier\Requests;

use Dingo\Api\Http\FormRequest;

class StoreSupplier extends SupplierRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Supplier');
    }

}
