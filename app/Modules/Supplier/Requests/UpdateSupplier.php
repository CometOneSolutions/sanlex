<?php

namespace App\Modules\Supplier\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateSupplier extends SupplierRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Supplier');
    }

}