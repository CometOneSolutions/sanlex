<?php

Breadcrumbs::register('supplier.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Suppliers', route('supplier_index'));
});
Breadcrumbs::register('supplier.create', function ($breadcrumbs) {
    $breadcrumbs->parent('supplier.index');
    $breadcrumbs->push('Create', route('supplier_create'));
});
Breadcrumbs::register('supplier.show', function ($breadcrumbs, $supplier) {
    $breadcrumbs->parent('supplier.index');
    $breadcrumbs->push($supplier->getName(), route('supplier_show', $supplier->getId()));
});
Breadcrumbs::register('supplier.edit', function ($breadcrumbs, $supplier) {
    $breadcrumbs->parent('supplier.show', $supplier);
    $breadcrumbs->push('Edit', route('supplier_edit', $supplier->getId()));
});
