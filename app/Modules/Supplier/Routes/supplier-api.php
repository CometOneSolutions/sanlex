<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'suppliers'], function () use ($api) {
        $api->get('/', 'App\Modules\Supplier\Controllers\SupplierApiController@index');
        $api->get('{supplierId}', 'App\Modules\Supplier\Controllers\SupplierApiController@show');
        $api->post('/', 'App\Modules\Supplier\Controllers\SupplierApiController@store');
        $api->patch('{supplierId}', 'App\Modules\Supplier\Controllers\SupplierApiController@update');
        $api->delete('{supplierId}', 'App\Modules\Supplier\Controllers\SupplierApiController@destroy');
    });
});
