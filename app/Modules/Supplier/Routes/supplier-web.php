<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'suppliers'], function () {
        Route::get('/', 'SupplierController@index')->name('supplier_index');
        Route::get('/create', 'SupplierController@create')->name('supplier_create');
        Route::get('{supplierId}/edit', 'SupplierController@edit')->name('supplier_edit');
        Route::get('{supplierId}/print', 'SupplierController@print')->name('supplier_print');
        Route::get('{supplierId}', 'SupplierController@show')->name('supplier_show');
    });

});
