<?php

namespace App\Modules\Supplier\Repositories;

use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Common\Repositories\Repository;

interface SupplierRepository extends Repository
{
    public function save(Supplier $supplier);
    public function delete(Supplier $supplier);
}
