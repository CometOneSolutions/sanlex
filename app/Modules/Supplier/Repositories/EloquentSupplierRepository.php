<?php

namespace App\Modules\Supplier\Repositories;

use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentSupplierRepository extends EloquentRepository implements SupplierRepository
{
    public function __construct(Supplier $supplier)
    {
        parent::__construct($supplier);
    }

    public function save(Supplier $supplier)
    {
        return $supplier->save();
    }

    public function delete(Supplier $supplier)
    {
        return $supplier->delete();
    }

}
