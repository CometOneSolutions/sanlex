<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="form-group">
    <label for="name">Name <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="name" name="name" v-model="form.name" aria-describedby="nameHelp"
        placeholder="" :class="{'is-invalid': form.errors.has('name')}" required
        data-msg-required="Enter supplier name">
</div>
<div class="form-group">
    <label for="address">Short Code <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="shortCode" name="shortCode" v-model="form.shortCode"
        aria-describedby="shortCodeHelp" placeholder="" :class="{'is-invalid': form.errors.has('shortCode')}" required
        data-msg-required="Enter short code">
</div>
<div class="form-group">
    <label for="contactPerson" class="col-form-label">Contact Person</label>
    <input type="text" class="form-control" placeholder="" v-model="form.contactPerson"
        :class="{'is-invalid': form.errors.has('contactPerson') }" data-msg-required="Enter Contact Person">
</div>

<div class="form-group">
    <label for="telephone" class="col-form-label">Telephone</label>
    <input type="text" class="form-control" placeholder="" v-model="form.telephone"
        :class="{'is-invalid': form.errors.has('telephone') }" data-msg-required="Enter Telephone">
</div>

<div class="form-group">
    <label for="mobile" class="col-form-label">Mobile</label>
    <input type="text" class="form-control" placeholder="" v-model="form.mobile"
        :class="{'is-invalid': form.errors.has('mobile') }" data-msg-required="Enter Mobile">
</div>

<div class="form-group">
    <label for="email" class="col-form-label">E-mail</label>
    <input type="email" class="form-control" placeholder="" v-model="form.email"
        :class="{'is-invalid': form.errors.has('email') }" data-msg-required="Enter E-mail">
</div>

<div class="form-group">
    <label for="address" class="col-form-label">Address</label>
    <input type="address" class="form-control" placeholder="" v-model="form.address"
        :class="{'is-invalid': form.errors.has('address') }" data-msg-required="Enter Address">
</div>
@push('scripts')
    <script src="{{ mix('js/supplier.js') }}"></script>
@endpush
