<div class="form-row">
    <div class="form-group col-md-6">
        <label for="name" class="col-form-label">Name</label>
        <p>@{{ form . name }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="shortCode" class="col-form-label">Short Code</label>
        <p>@{{ form . shortCode || '--' }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="contactPerson" class="col-form-label">Contact Person</label>
        <p>@{{ form . contactPerson }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="telephone" class="col-form-label">Telephone</label>
        <p>@{{ form . telephone }}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="mobile" class="col-form-label">Mobile</label>
        <p>@{{ form . mobile }}</p>
    </div>

    <div class="form-group col-md-6">
        <label for="email" class="col-form-label">E-mail</label>
        <p>@{{ form . email }}</p>
    </div>

    <div class="form-group col-md-6">
        <label for="address" class="col-form-label">Address</label>
        <p>@{{ form . address }}</p>
    </div>
</div>
@push('scripts')
    <script src="{{ mix('js/supplier.js') }}"></script>
@endpush
