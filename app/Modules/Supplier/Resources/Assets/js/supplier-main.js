import {Form} from '@c1_common_js/components/Form';

export const supplierService = new Vue({

    data: function() {
        return {
            form: new Form({}),
            uri: {
              suppliers: '/api/suppliers?limit=' + Number.MAX_SAFE_INTEGER
            },
        }
    },   

    methods: {

        getSuppliers(){
            return this.form.get(this.uri.suppliers);
        },
    }
});