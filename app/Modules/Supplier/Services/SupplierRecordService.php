<?php

namespace App\Modules\Supplier\Services;

use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface SupplierRecordService extends RecordService
{
    public function create(
        $name,
        $shortCode,
        $contactPerson,
        $telephone,
        $mobile,
        $email,
        $address,
        User $user = null
    );

    public function update(
        Supplier $supplier,
        $name,
        $shortCode,
        $contactPerson,
        $telephone,
        $mobile,
        $email,
        $address,
        User $user = null
    );

    public function delete(Supplier $supplier);

}
