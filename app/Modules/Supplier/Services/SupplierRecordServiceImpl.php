<?php

namespace App\Modules\Supplier\Services;

use App\Modules\Supplier\Models\Supplier;
use App\Modules\Supplier\Repositories\SupplierRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class SupplierRecordServiceImpl extends RecordServiceImpl implements SupplierRecordService
{

    private $supplier;
    private $suppliers;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"],
        ["id" => "short_code", "text" => "Short Code"],
        ['id' => 'telephone', 'text' => 'Telephone'],
        ['id' => 'mobile', 'text' => 'Mobile'],
        ['id' => 'contact_person', 'text' => 'Contact Person']
    ];

    public function __construct(SupplierRepository $suppliers, Supplier $supplier)
    {
        $this->suppliers = $suppliers;
        $this->supplier = $supplier;
        parent::__construct($suppliers);
    }

    public function create(
        $name,
        $shortCode,
        $contactPerson,
        $telephone,
        $mobile,
        $email,
        $address,
        User $user = null
    ) {
        $supplier = new $this->supplier;
        $supplier->setName($name);
        $supplier->setShortCode($shortCode);
        $supplier->setContactPerson($contactPerson);
        $supplier->setTelephone($telephone);
        $supplier->setMobile($mobile);
        $supplier->setEmail($email);
        $supplier->setAddress($address);
        if ($user) {
            $supplier->setUpdatedByUser($user);
        }
        $this->suppliers->save($supplier);
        return $supplier;
    }

    public function update(
        Supplier $supplier,
        $name,
        $shortCode,
        $contactPerson,
        $telephone,
        $mobile,
        $email,
        $address,
        User $user = null
    ) {
        $tempSupplier = clone $supplier;
        $tempSupplier->setName($name);
        $tempSupplier->setShortCode($shortCode);
        $tempSupplier->setContactPerson($contactPerson);
        $tempSupplier->setTelephone($telephone);
        $tempSupplier->setMobile($mobile);
        $tempSupplier->setEmail($email);
        $tempSupplier->setAddress($address);
        if ($user) {
            $tempSupplier->setUpdatedByUser($user);
        }
        $this->suppliers->save($tempSupplier);
        return $tempSupplier;
    }

    public function delete(Supplier $supplier)
    {
        $this->suppliers->delete($supplier);
        return $supplier;
    }
}
