<?php

namespace App\Modules\Supplier\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\Supplier\Transformer\SupplierTransformer;
use App\Modules\Supplier\Requests\StoreSupplier;
use App\Modules\Supplier\Requests\UpdateSupplier;
use App\Modules\Supplier\Requests\DestroySupplier;

class SupplierApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        SupplierRecordService $supplierRecordService,
        SupplierTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($supplierRecordService, $transformer);
        $this->service = $supplierRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreSupplier $request)
    {
        $supplier = \DB::transaction(function () use ($request) {
            $name       = $request->getName();
            $shortCode  = $request->getShortCode();
            $user       = $request->user();

            return $this->service->create(
                $name,
                $shortCode,
                $request->getContactPerson(),
                $request->getTelephone(),
                $request->getMobile(),
                $request->getEmail(),
                $request->getAddress(),
                $user
            );
        });
        return $this->response->item($supplier, $this->transformer)->setStatusCode(201);
    }

    public function update($supplierId, UpdateSupplier $request)
    {
        $supplier = \DB::transaction(function () use ($supplierId, $request) {
            $supplier   = $this->service->getById($supplierId);
            $name       = $request->getName();
            $shortCode  = $request->getShortCode();
            $user       = $request->user();

            return $this->service->update(
                $supplier,
                $name,
                $shortCode,
                $request->getContactPerson(),
                $request->getTelephone(),
                $request->getMobile(),
                $request->getEmail(),
                $request->getAddress(),
                $user
            );
        });
        return $this->response->item($supplier, $this->transformer)->setStatusCode(200);
    }

    public function destroy($supplierId, DestroySupplier $request)
    {
        $supplier = $this->service->getById($supplierId);
        $this->service->delete($supplier);
        return $this->response->item($supplier, $this->transformer)->setStatusCode(200);
    }

}
