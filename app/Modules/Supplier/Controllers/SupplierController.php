<?php

namespace App\Modules\Supplier\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Supplier\Services\SupplierRecordService;


class SupplierController extends Controller
{
    private $service;

    public function __construct(SupplierRecordService $supplierRecordService)
    {
        $this->service = $supplierRecordService;
        $this->middleware('permission:Read [MAS] Supplier')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Supplier')->only('create');
        $this->middleware('permission:Update [MAS] Supplier')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/suppliers'
        ]);
        return view('suppliers.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('suppliers.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $supplier = $this->service->getById($id);
        return view('suppliers.show', compact('supplier'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $supplier = $this->service->getById($id);
        return view('suppliers.edit', compact('supplier'));
    }
}
