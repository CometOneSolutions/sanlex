import {Form} from '@c1_common_js/components/Form';

export const coatingService = new Vue({

    data: function() {
        return {
            form: new Form({}),
            uri: {
              coatings: '/api/coatings?limit=' + Number.MAX_SAFE_INTEGER
            },
        }
    },   

    methods: {

        getCoatings(){
            return this.form.get(this.uri.coatings);
        },
    }
});