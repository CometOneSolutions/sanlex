<?php

namespace App\Modules\Coating\Models;


interface Coating
{
    public function getId();

    public function getName();

    public function setName($value);
}
