<?php

namespace App\Modules\Coating\Models;

use App\Modules\Coil\Models\CoilModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Models\Users\User;

class CoatingModel extends Model implements Coating, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'coatings';

    public function coils()
    {
        return $this->hasMany(CoilModel::class, 'coating_id');
    }

    public function getCoils()
    {
        return $this->coils;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

}
