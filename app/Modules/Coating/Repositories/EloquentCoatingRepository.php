<?php

namespace App\Modules\Coating\Repositories;

use App\Modules\Coating\Models\Coating;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentCoatingRepository extends EloquentRepository implements CoatingRepository
{
    public function __construct(Coating $coating)
    {
        parent::__construct($coating);
    }

    public function save(Coating $coating)
    {
        return $coating->save();
    }

    public function delete(Coating $coating)
    {
        return $coating->delete();
    }

}
