<?php

namespace App\Modules\Coating\Repositories;

use App\Modules\Coating\Models\Coating;
use CometOneSolutions\Common\Repositories\Repository;

interface CoatingRepository extends Repository
{
    public function save(Coating $coating);
    public function delete(Coating $coating);
}
