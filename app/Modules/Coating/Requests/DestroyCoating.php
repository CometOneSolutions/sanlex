<?php

namespace App\Modules\Coating\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyCoating extends CoatingRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Coating');
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}