<?php

namespace App\Modules\Coating\Requests;

use Dingo\Api\Http\FormRequest;

class StoreCoating extends CoatingRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Coating');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }

}
