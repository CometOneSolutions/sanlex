<?php

namespace App\Modules\Coating\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateCoating extends CoatingRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Coating');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }

}