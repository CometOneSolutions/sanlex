<?php

namespace App\Modules\Coating\Services;

use App\Modules\Coating\Models\Coating;
use App\Modules\Coating\Repositories\CoatingRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class CoatingRecordServiceImpl extends RecordServiceImpl implements CoatingRecordService
{
    use CanUpdateProductName;


    private $coating;
    private $coatings;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(CoatingRepository $coatings, Coating $coating)
    {
        $this->coatings = $coatings;
        $this->coating = $coating;
        parent::__construct($coatings);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $coating = new $this->coating;
        $coating->setName($name);
        if ($user) {
            $coating->setUpdatedByUser($user);
        }
        $this->coatings->save($coating);
        return $coating;
    }

    public function update(
        Coating $coating,
        $name,
        User $user = null
    ) {
        $tempCoating = clone $coating;
        $tempCoating->setName($name);
        if ($user) {
            $tempCoating->setUpdatedByUser($user);
        }
        $this->coatings->save($tempCoating);

        $this->updateProductableProductNames([
            $tempCoating->getCoils(),
        ]);

        return $tempCoating;
    }

    public function delete(Coating $coating)
    {
        $this->coatings->delete($coating);
        return $coating;
    }
}
