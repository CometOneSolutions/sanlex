<?php

namespace App\Modules\Coating\Services;

use App\Modules\Coating\Models\Coating;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface CoatingRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        Coating $coating,
        $name,
        User $user = null
    );

    public function delete(Coating $coating);
}
