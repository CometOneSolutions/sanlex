<?php

namespace App\Modules\Coating\Provider;

use App\Modules\Coating\Models\Coating;
use App\Modules\Coating\Models\CoatingModel;
use App\Modules\Coating\Repositories\CoatingRepository;
use App\Modules\Coating\Repositories\EloquentCoatingRepository;
use App\Modules\Coating\Services\CoatingRecordService;
use App\Modules\Coating\Services\CoatingRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class CoatingServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Coating/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(CoatingRecordService::class, CoatingRecordServiceImpl::class);
        $this->app->bind(CoatingRepository::class, EloquentCoatingRepository::class);
        $this->app->bind(Coating::class, CoatingModel::class);
    }
}
