<?php

namespace App\Modules\Coating\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Coating\Services\CoatingRecordService;
use App\Modules\Coating\Transformer\CoatingTransformer;
use App\Modules\Coating\Requests\StoreCoating;
use App\Modules\Coating\Requests\UpdateCoating;
use App\Modules\Coating\Requests\DestroyCoating;

class CoatingApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        CoatingRecordService $coatingRecordService,
        CoatingTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($coatingRecordService, $transformer);
        $this->service = $coatingRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreCoating $request)
    {
        $coating = \DB::transaction(function () use ($request) {
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->create(
                $name,
                $user
            );
        });
        return $this->response->item($coating, $this->transformer)->setStatusCode(201);
    }

    public function update($coatingId, UpdateCoating $request)
    {
        $coating = \DB::transaction(function () use ($coatingId, $request) {
            $coating   = $this->service->getById($coatingId);
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->update(
                $coating,
                $name,
                $user
            );
        });
        return $this->response->item($coating, $this->transformer)->setStatusCode(200);
    }

    public function destroy($coatingId, DestroyCoating $request)
    {
        $coating = $this->service->getById($coatingId);
        $this->service->delete($coating);
        return $this->response->item($coating, $this->transformer)->setStatusCode(200);
    }

}
