<?php

namespace App\Modules\Coating\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Coating\Services\CoatingRecordService;


class CoatingController extends Controller
{
    private $service;

    public function __construct(CoatingRecordService $coatingRecordService)
    {
        $this->service = $coatingRecordService;
        $this->middleware('permission:Read [MAS] Coating')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Coating')->only('create');
        $this->middleware('permission:Update [MAS] Coating')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/coatings'
        ]);
        return view('coatings.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('coatings.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $coating = $this->service->getById($id);
        return view('coatings.show', compact('coating'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $coating = $this->service->getById($id);
        return view('coatings.edit', compact('coating'));
    }
}
