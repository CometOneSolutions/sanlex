<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'coatings'], function () {
        Route::get('/', 'CoatingController@index')->name('coating_index');
        Route::get('/create', 'CoatingController@create')->name('coating_create');
        Route::get('{coatingId}/edit', 'CoatingController@edit')->name('coating_edit');
        Route::get('{coatingId}/print', 'CoatingController@print')->name('coating_print');
        Route::get('{coatingId}', 'CoatingController@show')->name('coating_show');
    });

});
