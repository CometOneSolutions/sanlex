<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'coatings'], function () use ($api) {
        $api->get('/', 'App\Modules\Coating\Controllers\CoatingApiController@index');
        $api->get('{coatingId}', 'App\Modules\Coating\Controllers\CoatingApiController@show');
        $api->post('/', 'App\Modules\Coating\Controllers\CoatingApiController@store');
        $api->patch('{coatingId}', 'App\Modules\Coating\Controllers\CoatingApiController@update');
        $api->delete('{coatingId}', 'App\Modules\Coating\Controllers\CoatingApiController@destroy');
    });
});
