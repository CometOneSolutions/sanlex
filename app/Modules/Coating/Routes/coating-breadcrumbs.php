<?php

Breadcrumbs::register('coating.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Coatings', route('coating_index'));
});
Breadcrumbs::register('coating.create', function ($breadcrumbs) {
    $breadcrumbs->parent('coating.index');
    $breadcrumbs->push('Create', route('coating_create'));
});
Breadcrumbs::register('coating.show', function ($breadcrumbs, $coating) {
    $breadcrumbs->parent('coating.index');
    $breadcrumbs->push($coating->getName(), route('coating_show', $coating->getId()));
});
Breadcrumbs::register('coating.edit', function ($breadcrumbs, $coating) {
    $breadcrumbs->parent('coating.show', $coating);
    $breadcrumbs->push('Edit', route('coating_edit', $coating->getId()));
});
