<?php

use Faker\Generator as Faker;
use App\Modules\Supplier\Models\SupplierModel;
use CometOneSolutions\Auth\UserModel;

$factory->define(SupplierModel::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->company,
        'updated_by_user_id' => $faker->randomElement(UserModel::pluck('id')->toArray()),
    ];
});
