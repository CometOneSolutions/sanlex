<?php

namespace App\Modules\Coating\Database\Seeds;

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

class CoatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $coatings = factory(CoatingModel::class, 5)->create();
    }
}
