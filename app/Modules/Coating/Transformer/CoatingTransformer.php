<?php

namespace App\Modules\Coating\Transformer;

use App\Modules\Coating\Models\Coating;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class CoatingTransformer extends UpdatableByUserTransformer
{
    public function transform(Coating $coating)
    {
        return [
            'id' => (int)$coating->getId(),
            'text' => $coating->getName(),
            'name' => $coating->getName(),
            'updatedAt' => $coating->updated_at,
            'editUri' => route('coating_edit', $coating->getId()),
            'showUri' => route('coating_show', $coating->getId()),
        ];
    }
}
