<?php

Route::group(['prefix' => 'production'], function () {
    Route::get('/', 'ProductionMenuDashboardController@dashboard')->name('production-menu-dashboard_index');

    Route::group(['prefix' => 'job-orders'], function () {
        Route::get('/', 'JobOrderController@index')->name('job-order_index');
        Route::get('/', 'JobOrderController@indexPending')->name('job-order_index');
        Route::get('create', 'JobOrderController@create')->name('job-order_create');

        Route::get('{jobreportorder}/edit', 'JobOrderController@edit')->name('job-order_edit');
        Route::get('{jobreportorder}', 'JobOrderController@show')->name('job-order_show');
    });
});
