<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'job-orders'], function () use ($api) {
        $api->get('/', 'App\Modules\JobOrders\Controllers\JobOrderApiController@index');
        // $api->get('/list', 'App\Modules\JobOrders\Controllers\JobOrderApiController@index');
        $api->get('{jobOrderId}', 'App\Modules\JobOrders\Controllers\JobOrderApiController@show');
        $api->post('/', 'App\Modules\JobOrders\Controllers\JobOrderApiController@store');
        $api->post('approve/all', 'App\Modules\JobOrders\Controllers\JobOrderApiController@approveAll');
        $api->patch('approve/{jobOrderId}', 'App\Modules\JobOrders\Controllers\JobOrderApiController@approve');
        $api->patch('{jobOrderId}', 'App\Modules\JobOrders\Controllers\JobOrderApiController@update');
        $api->delete('{jobOrderId}', 'App\Modules\JobOrders\Controllers\JobOrderApiController@destroy');
    });
});
