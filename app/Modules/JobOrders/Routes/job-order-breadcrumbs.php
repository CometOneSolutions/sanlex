<?php

Breadcrumbs::register('job-order_index', function ($breadcrumbs) {
    $breadcrumbs->parent('out-menu-dashboard.index');
    $breadcrumbs->push('Job Orders', route('job-order_index'));
});

Breadcrumbs::register('job-order_pending', function ($breadcrumbs) {
    $breadcrumbs->parent('out-menu-dashboard.index');
    $breadcrumbs->push('Pending Job Orders', route('job-order_pending'));
});

Breadcrumbs::register('job-order_create', function ($breadcrumbs) {
    $breadcrumbs->parent('job-order_index');
    $breadcrumbs->push('Create', route('job-order_create'));
});

Breadcrumbs::register('job-order_show', function ($breadcrumbs, $jobOrder) {
    $breadcrumbs->parent('job-order_index');
    $breadcrumbs->push($jobOrder->getJobOrderNumber(), route('job-order_show', $jobOrder->getId()));
});

Breadcrumbs::register('job-order_edit', function ($breadcrumbs, $jobOrder) {
    $breadcrumbs->parent('job-order_show', $jobOrder);
    $breadcrumbs->push('Edit', route('job-order_edit', $jobOrder->getId()));
});
