<?php

namespace App\Modules\JobOrders\Transformer;

use App\Modules\JobOrders\Models\JobOrder;
use App\Modules\Customer\Transformer\CustomerTransformer;
use App\Modules\JobOrders\Transformer\JobOrderDetailTransformer;
use App\Modules\PanelProfile\Transformer\PanelProfileTransformer;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class JobOrderTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'customer',
        'jobOrderDetails',
        'panelProfile'
    ];

    public function transform(JobOrder $jobOrder)
    {
        return [
            'id'                    => (int) $jobOrder->getId(),
            'jobOrderNumber'        => $jobOrder->getJobOrderNumber(),
            'customerId'            => $jobOrder->getCustomerId(),
            'date'                  => $jobOrder->getDate()->toDateString(),
            'panelProfileId' => $jobOrder->getPanelProfileId(),
            'operator'              => $jobOrder->getOperator(),
            'remarks' => $jobOrder->getRemarks(),
            'status'                => $jobOrder->getStatus(),
            'isDraft'               => $jobOrder->isDraft(),
            'showUri'               => route('job-order_show', $jobOrder->getId()),
            'editUri'               => route('job-order_edit', $jobOrder->getId()),
            'updatedAt'             => $jobOrder->updated_at,
        ];
    }


    public function includeJobOrderDetails(JobOrder $jobOrder)
    {
        $jobOrderDetails = $jobOrder->getJobOrderDetails();

        return $this->collection($jobOrderDetails, new JobOrderDetailTransformer);
    }

    public function includeCustomer(JobOrder $jobOrder)
    {
        $customer = $jobOrder->getCustomer();
        return $this->item($customer, new CustomerTransformer);
    }
    public function includePanelProfile(JobOrder $jobOrder)
    {
        $panelProfile = $jobOrder->getPanelProfile();
        return $this->item($panelProfile, new PanelProfileTransformer);
    }
    // public function includePivot(JobOrder $jobOrder)
    // {
    //     $pivot = $jobOrder->pivot;
    //     return $this->item($pivot, new PoPaymentMappingTransformer);
    // }

    // public function includeOutboundPayments(JobOrder $jobOrder)
    // {
    //     return $this->collection($jobOrder->outboundPayments, new OutboundPaymentTransformer);
    // }
}
