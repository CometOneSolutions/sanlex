<?php

namespace App\Modules\JobOrders\Transformer;

use App\Modules\InventoryDetail\Transformers\InventoryDetailTransformer;
use App\Modules\JobOrders\Models\JobOrderDetail;
use League\Fractal;

class JobOrderDetailTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'inventoryDetail'
    ];

    public function transform(JobOrderDetail $jobOrderDetail)
    {
        return [
            'id'                    => (int) $jobOrderDetail->getId(),
            'inventoryDetailId'     => $jobOrderDetail->getInventoryDetailId(),
            'inventoryDetail'       => $jobOrderDetail->getInventoryDetail()->getInventory()->getProduct()->getName(),
            'reference'             => $jobOrderDetail->getInventoryDetailReference(),
            'length'                => $jobOrderDetail->getLength(),
            'weight'                => $jobOrderDetail->getWeight(),
            'factor'                => $jobOrderDetail->getFactor(),
            'originalBalance'       => $jobOrderDetail->getInventoryDetail()->getQuantity(),
            'balance'               => $jobOrderDetail->getInventoryDetail()->getQuantity() - $jobOrderDetail->getInventoryDetail()->getBalance(),
            'others'                => $jobOrderDetail->getOtherDraftJobOrderDetails()
        ];
    }

    public function includeInventoryDetail(JobOrderDetail $jobOrderDetail)
    {

        $inventoryDetail = $jobOrderDetail->getInventoryDetail();
        return $this->item($inventoryDetail, new InventoryDetailTransformer);
    }
}
