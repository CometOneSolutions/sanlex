@extends('app')
@section('breadcrumbs', Breadcrumbs::render('job-order_show', $jobOrder))
@section('content')
<section id="job-order">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Job Order
                        @if($jobOrder->isDraft())
                        <span class="badge badge-warning">DRAFT</span>
                        @else
                        <span class="badge badge-success">APPROVED</span>
                        @endif
                        <div class="float-right" v-if="form.isDraft">
                            @if(auth()->user()->can('Update [OUT] Job Order'))
                            <a :href="form.editUri"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> </button></a>
                            @endif
                            @if(auth()->user()->can('Approve [OUT] Job Order') && auth()->user()->hasRole('ADMIN'))
                            <button type="button" @click="approve" class="btn btn-warning btn-sm" :disabled="form.isBusy"><i class="far fa-thumbs-up"></i>
                                Approve</button>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        @include('job-orders._list')
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <timestamp :name="form.updatedByUser.data.name" :time="form.updatedAt"></timestamp>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection
