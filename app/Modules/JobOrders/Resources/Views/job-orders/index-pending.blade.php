@extends('app')
@section('breadcrumbs', Breadcrumbs::render('job-order_pending'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Pending Job Orders
            <div class="float-right">
                <a href="{{route('job-order_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
            </div>
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending" v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('job_order_no')">
                                    JO No. <i class="fas fa-sort" :class="getSortIcon('job_order_no')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('operator')">
                                    Operator <i class="fas fa-sort" :class="getSortIcon('operator')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('date')">
                                    Date <i class="fas fa-sort" :class="getSortIcon('date')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('panel_profile_name')">
                                    Panel Profile <i class="fas fa-sort" :class="getSortIcon('panel_profile_name')"></i>
                                </a>
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{item.jobOrderNumber}}</a></td>
                            <td>@{{item.operator}}</td>
                            <td>@{{item.date}}</td>
                            <td>@{{item.panelProfile.data.name}}</td>

                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>

<!-- End Watchlist-->
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush