@extends('app')
@section('breadcrumbs', Breadcrumbs::render('job-order_create'))
@section('content')
<section id="job-order">
    <div class="row">
        <div class="col-12">
            <div class="card"  v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Create New Job Order
                    </div>
                    <!-- <form @submit.prevent="store" v-on:keydown="form.errors.clear($event.target.name)" autocomplete="off"> -->
                    <v-form
                        @validate="store">

                        <div class="card-body">
                            @include('job-orders._form')
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                               
                                    <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                               
                            </div>
                        </div>
                    </vform>
                </div>  
            </div>
        </div>
    </div>
</section>
<br/>
@endsection


