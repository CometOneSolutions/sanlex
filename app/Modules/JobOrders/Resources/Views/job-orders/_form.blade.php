<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="form-row">
    <div class="form-group col-md-4">
        <label for="inputEmail4" class="col-form-label">Job Order No. <span class="text-danger">*</span></label>
        <input type="text" class="form-control" name="jobOrderNumber" id="jobOrderNumber" v-model="form.jobOrderNumber" placeholder="" required data-msg-required="Enter JO No.">
    </div>
    <div class="form-group col-md-4">
        <label for="inputEmail4" class="col-form-label">Date <span class="text-danger">*</span></label>
        <datepicker :typeable="true" :input-class="{'is-invalid': form.errors.has('date'),'form-control': true}" v-model="form.date" :required="true"></datepicker>
    </div>
    <div class="form-group ml-auto col-md-4" v-if="form.id">
        <label for="inputEmail4" class="col-form-label">Status</label>
        <select class="form-control select2-single" name="status" id="status" v-model="form.status" disabled>
            <option value="" disabled>Please Select</option>
            <option value="Draft">Draft</option>
            <option value="Approved">Approved</option>
        </select>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1" class="col-form-label">Customer <span class="text text-danger">*</span></label>
        <select3 class="custom-select" required data-msg-required="Enter customer" v-model="form.customerId" :selected="defaultSelectedCustomer" :url="customerUrl" search="name">
        </select3>
    </div>
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Remarks:</label>
        <input type="text" class="form-control" name="remarks" id="remarks" v-model="form.remarks" placeholder="">
    </div>
</div>
<div class="form-row">
    <div class="col-6">
        <label for="panelProfileId" class="col-form-label">Panel Profile <span class="text text-danger">*</span></label>
        <select3 class="custom-select" required data-msg-required="Please select Panel Profile" v-model="form.panelProfileId" required data-msg-required="Plase select Panel Profile" :selected="defaultSelectedPanelProfile" :url="panelProfileUrl" search="name">
        </select3>
    </div>

    <div class="col-6">
        <div class="form-group">
            <label for="inputEmail4" class="col-form-label">Operator <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="operator" id="operator" v-model="form.operator" placeholder="" required data-msg-required="Enter Operator">
        </div>
    </div>
</div>

<div class="form-row mt-3">

    <div class="table-responsive-sm col-lg-12">
        <table class="table">
            <thead>
                <tr class="row">
                    <th class="center col-1">#</th>
                    <th class="col-3">Inventory</th>
                    <th class="right col-2 text-right">Balance</th>
                    <th class="right col-2 text-right">Length</th>
                    <th class="right col-2 text-right">Usage (MT)</th>
                    <th class="right col-1 text-right">Factor</th>
                    <th class="col-1 center">&nbsp;</th>



                </tr>
            </thead>
            <tbody>
                <tr is="job-order-detail" v-for="(detail,index) in form.jobOrderDetails.data" :detail="detail" :index="index" v-on:remove="form.jobOrderDetails.data.splice(index, 1)" :key="detail.id">
                </tr>

                <tr class="row">


                    <td class="col-12" colspan="6">
                        <button type="button" class="btn btn-outline-success btn-sm" @click="add">Add New</button>
                    </td>
                    <!-- <td class="col-12 text-center" colspan="6" v-else>
                        Please select a COIL SKU.
                    </td> -->
                </tr>
            </tbody>
        </table>
    </div>
</div>

<!-- <div class="form-row">
<div class="col-lg-4 col-sm-5">
    <label for="inputEmail4" class="col-form-label">Remarks</label>
    <textarea style="resize: none" class="form-control" name="remarks" id="remarks" v-model="form.remarks" rows="4" ></textarea>
</div>
<div class="col-lg-4 col-sm-5">
    <label for="inputEmail4" class="col-form-label">Total Output</label>
    <money style="resize: none" class="form-control" name="output" id="output" :value="totalOutput" readonly></money>
</div>
<div class="col-lg-4 col-sm-5">
    <label for="inputEmail4" class="col-form-label">Yield</label>
    <textarea style="resize: none" class="form-control" name="yield" id="yield" v-model="yield" rows="4" readonly></textarea>
</div> -->





@push('scripts')
<script src="{{ mix('js/job-order.js') }}"></script>
@endpush
