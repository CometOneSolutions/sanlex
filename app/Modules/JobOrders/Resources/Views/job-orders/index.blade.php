@extends('app')
@section('breadcrumbs', Breadcrumbs::render('job-order_index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Job Orders
            @if(auth()->user()->can('Create [OUT] Job Order'))
            <div class="float-right">
                @if(auth()->user()->hasRole('ADMIN'))
                <button v-if="items.length > 0" type="button" class="btn btn-warning btn-sm" @click="approveAllJo" :disabled="form.isBusy"><i class="fas fa-check-double"></i> </button>
                @endif
                <a href="{{route('job-order_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>

            </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending" v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('job_order_no')">
                                    JO No. <i class="fas fa-sort" :class="getSortIcon('job_order_no')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('date')">
                                    Date <i class="fas fa-sort" :class="getSortIcon('date')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('customer')">
                                    Customer <i class="fas fa-sort" :class="getSortIcon('customer')"></i>
                                </a>
                            </th>

                            <th>
                                <a v-on:click="setSorter('remarks')">
                                    Remarks <i class="fas fa-sort" :class="getSortIcon('remarks')"></i>
                                </a>
                            </th>

                            <th>
                                <a v-on:click="setSorter('status')">
                                    Status <i class="fas fa-sort" :class="getSortIcon('status')"></i>
                                </a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{item.jobOrderNumber}}</a></td>
                            <td>@{{item.date}}</td>
                            <td>@{{item.customer.data.name}}</td>

                            <td>@{{item.remarks}}</td>

                            <td>
                                <span v-if="item.isDraft">
                                    <span class="badge badge-warning">@{{item.status}}</span>
                                </span>
                                <span v-else>
                                    <span class="badge badge-success">@{{item.status}}</span>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>

<!-- End Watchlist-->
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush
