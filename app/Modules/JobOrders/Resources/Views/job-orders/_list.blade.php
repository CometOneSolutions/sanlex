<div class="form-row">
    <div class="form-group col-md-3">
        <label for="inputEmail4" class="col-form-label">Job Order Number</label>
        <p>@{{form.jobOrderNumber}}</p>
    </div>

    <div class="form-group col-md-3 ml-auto">
        <label for="inputEmail4" class="col-form-label">Status</label>
        <p>@{{form.status}}</p>
    </div>
    <div class="form-group col-md-3">
        <label for="inputEmail4" class="col-form-label">Date</label>
        <p>@{{form.date}}</p>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-3">
        <label for="inputEmail4" class="col-form-label">Customer</label>
        <p>@{{form.customer.data.name}}</p>
    </div>
    <div class="form-group col-md-3">
        <label for="inputEmail4" class="col-form-label">Operator</label>
        <p>@{{form.operator}}</p>
    </div>
    <div class="form-group col-md-3">
        <label for="inputEmail4" class="col-form-label">Panel Profile</label>
        <p>@{{form.panelProfile.data.name}}</p>
    </div>

</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label for="inputEmail4" class="col-form-label">Remarks</label>
        <p>@{{form.remarks}}</p>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr>
                    <th class="center">#</th>
                    <th>Inventory</th>
                    <th>Coil Number</th>
                    <th class="right  text-right">Length</th>
                    <th class="right  text-right">Balance</th>
                    <th class="right  text-right">Usage (MT)</th>
                    <th class="center text-center">Factor</th>
                    <th class="center">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(detail,index) in form.jobOrderDetails.data">
                    <td class="center">
                        @{{index+1}}
                    </td>
                    <td>
                        @{{detail.inventoryDetail.data.productName}}
                    </td>
                    <td>
                        @{{detail.reference}}
                    </td>
                    <td class="right  text-right">@{{detail.length | numeric }}</td>
                    <td class="right  text-right">@{{detail.originalBalance | weight }}</td>
                    <td class="right  text-right">
                        Current - (@{{detail.weight | weight}})<br>
                        <span v-for="other in detail.others" v-if="form.status == 'Draft'">
                            @{{other.job_order.job_order_no}} - (@{{other.weight | weight}})<br>
                        </span>
                        <span v-if="form.status == 'Draft'" class=" text-danger">@{{computeBalance(detail) | weight}}</span>
                    </td>
                    <td class="right  text-center">@{{detail.factor | numeric}}</td>
                    <td class="center">&nbsp;</td>
                </tr>

                <tr>



                </tr>
            </tbody>
        </table>
    </div>
</div>
@push('scripts')
<script src="{{ mix('js/job-order.js') }}"></script>
@endpush