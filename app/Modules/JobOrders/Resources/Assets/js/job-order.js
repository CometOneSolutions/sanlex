import { Form } from '@c1_common_js/components/Form';
import { inventoryDetailService } from "@c1_module_js/InventoryDetail/Resources/Assets/js/inventory-detail-main";
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm';
import Datepicker from 'vuejs-datepicker';
import Select2 from '@c1_common_js/components/Select2';
import Select3 from "@c1_common_js/components/Select3";
import JobOrderDetail from "./JobOrderDetail";

new Vue({
    el: '#job-order',
    components: {
        SaveButton,
        DeleteButton,
        Timestamp,
        VForm,
        inventoryDetailService,
        Select2,
        Select3,
        Datepicker,
        JobOrderDetail
    },
    data: {
        counter: 0,
        customerUrl: '/api/customers',
        
        panelProfileUrl: '/api/panel-profiles',
        defaultSelectedCustomer: {},
        
        defaultSelectedPanelProfile: {},
        form: new Form({
            id: null,
            jobOrderNumber: null,
            operator: null,
            customerId: null,
            panelProfileId: null,
            date: moment(),
            remarks: null,
            status: '',
            jobOrderDetails: {
                data: []
            },

        }),
        isEdit: false,
        
        dataInitialized: true

    },

    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }


    },

    computed: {

        initializationComplete() {
            return this.dataInitialized;
        },

    },
    methods: {
        // fetchInventories() {
        //     if (this.form.productId != null) {
        //         this.form.get('/api/inventory-details?quantity>0&product_id=' + this.form.productId)
        //             .then(response => {
        //                 this.inventorySelections = response.data;
        //                 this.inventoryInitialized = true;
        //             });
        //     }
        // },
        computeBalance(jobOrderDetail)
        {
            let bal = parseFloat(jobOrderDetail.originalBalance);
            var sum = parseFloat(jobOrderDetail.weight);
            jobOrderDetail.others.forEach(function(other) {
                sum += parseFloat(other.weight);
            });
            return (bal - sum);
        },

        add() {
            this.form.jobOrderDetails.data.push({
                id: --this.counter,
                inventoryDetailId: 0,
                factor: '--',
                balance: 0,
                length: 0,
                weight: 0,
                others: []
            });
        },
        destroy() {
            this.form.confirm().then((result) => {
                if (result.value) {
                    this.form.delete('/api/job-orders/' + this.form.id).then(response => {
                        this.$swal({
                            title: 'Success',
                            text: 'Job Order deleted.',
                            type: 'success'
                        }).then(() => window.location = '/out/job-orders');
                    })
                }
            });
        },
        approve() {
            this.form.affirm().then((result) => {
                if (result.value) {
                    this.form.patch('/api/job-orders/approve/' + id).then(
                        response => {
                            this.$swal({
                                title: 'Success',
                                text: 'JO approved.',
                                type: 'success'
                            }).then(() => window.location = '/out/job-orders/' + id);
                        })
                }
            });
        },
        store() {
            let isValid = true;
            this.form.jobOrderDetails.data.forEach(function(jobOrderDetail, index) {
                if(jobOrderDetail.weight == 0) 
                {
                    this.$swal({
                        title: 'Oops',
                        text: `Item row ${index + 1} contains weight of 0.`,
                        type: 'error'
                    });
                    isValid = false;
                }
                
            }.bind(this));
            if(isValid) {
                this.form.post('/api/job-orders')
                .then(response => {
                    this.$swal({
                        title: 'Success',
                        text: 'Job Order was successfully created.',
                        type: 'success'
                    }).then(() => window.location = '/out/job-orders/');
                })
            }
            
        },

        update() {
            this.form.patch('/api/job-orders/' + id)
                .then(response => {
                    this.$swal({
                        title: 'Success',
                        text: 'Job Order was successfully updated.',
                        type: 'success'
                    }).then(() => window.location = '/out/job-orders/' + id);
                })
        },

        loadData(data) {
            this.defaultSelectedCustomer = {
                text: data.customer.data.name,
                id: data.customerId,
            };
            this.defaultSelectedPanelProfile = {
                text: data.panelProfile.data.name,
                id: data.customerId,
            };
            // this.defaultSelectedProduct = {
            //     text: data.product.data.name,
            //     id: data.productId,
            // };
            this.form = new Form(data);

        },


    },
    created() {


        if (id != null) {
            this.dataInitialized = false;
            // Fetch supplier
            this.form.isInitializing = true;
            this.form.get('/api/job-orders/' + id +
                '?include=customer,jobOrderDetails.inventoryDetail,panelProfile').then(response => {
                this.loadData(response.data);
                
                this.form.isInitializing = false;
                this.dataInitialized = true;
            });
        }
        if (this.form.id == null) {
            this.form.status = 'Draft';
        }
        if (action != null && action == 'edit') {
            this.isEdit = true;
        }
    },
    mounted() {

        console.log("Init JO script...");

    }
});