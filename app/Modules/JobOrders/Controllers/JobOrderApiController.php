<?php

namespace App\Modules\JobOrders\Controllers;

use App\Modules\JobOrders\Services\JobOrderRecordService;
use App\Modules\JobOrders\Transformer\JobOrderTransformer;
use App\Modules\JobOrders\Requests\StoreJobOrder;
use App\Modules\JobOrders\Requests\UpdateJobOrder;
use App\Modules\JobOrders\Requests\ApproveJobOrder;
use CometOneSolutions\Common\Controllers\ResourceApiController;

class JobOrderApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        JobOrderRecordService $jobOrderRecordService,
        JobOrderTransformer $transformer
    ) {
        parent::__construct($jobOrderRecordService, $transformer);
        $this->service = $jobOrderRecordService;
        $this->transformer = $transformer;
        $this->middleware('auth:api');
    }

    public function store(StoreJobOrder $request)
    {
        $jobOrder = \DB::transaction(function () use ($request) {
            $jobOrderNumber = $request->getJobOrderNumber();

            $panelProfile = $request->getPanelProfile();
            $operator = $request->getOperator();
            $date = $request->getDate();
            $status = $request->getStatus();
            $customer = $request->getCustomer();
            $jobOrderDetails = $request->getJobOrderDetails();

            $jobOrder = $this->service->create(
                $jobOrderNumber,
                $operator,
                $status,
                $date,
                $customer,
                $panelProfile,
                $jobOrderDetails,
                $request->getRemarks(),
                $request->user()
            );
            return $jobOrder;
        });

        return $this->response->item($jobOrder, $this->transformer)->setStatusCode(201);
    }

    public function update($jobOrderId, UpdateJobOrder $request)
    {

        $jobOrder = \DB::transaction(function () use ($jobOrderId, $request) {
            $jobOrder = $this->service->getById($jobOrderId);
            return $this->service->update(
                $jobOrder,
                $request->getJobOrderNumber(),
                $request->getDate(),
                $request->getRemarks(),
                $request->getOperator(),
                $request->getJobOrderDetails(),
                $request->getCustomer(),
                $request->getPanelProfile()
            );
        });
        return $this->response->item($jobOrder, $this->transformer)->setStatusCode(200);
    }
    public function approveAll()
    {
        $data = $this->service->approveAll();
        return $this->response->array(['message' => $data['response']])->setStatusCode(200);
    }
    public function approve($jobOrderId, ApproveJobOrder $request)
    {
        $jobOrder = \DB::transaction(function () use ($jobOrderId) {
            $jobOrder = $this->service->getById($jobOrderId);
            $this->service->approve($jobOrder);
            return $jobOrder;
        });
        return $this->response->item($jobOrder, $this->transformer)->setStatusCode(200);
    }
    public function destroy($id)
    {
        $jobOrder = \DB::transaction(function () use ($id) {
            $jobOrder = $this->service->getById($id);
            return $this->service->delete($jobOrder);
        });
        return $this->response->item($jobOrder, $this->transformer)->setStatusCode(200);
    }
}
