<?php

namespace App\Modules\JobOrders\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\JobOrders\Services\JobOrderRecordService;

class JobOrderController extends Controller
{
    private $jobOrderRecordService;

    public function __construct(JobOrderRecordService $jobOrderRecordService)
    {
        $this->jobOrderRecordService = $jobOrderRecordService;
        $this->middleware('permission:Read [OUT] Job Order')->only(['show', 'index', 'indexPending']);
        $this->middleware('permission:Create [OUT] Job Order')->only('create');
        $this->middleware('permission:Update [OUT] Job Order')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->jobOrderRecordService->getAvailableFilters(),
            'sorter' => 'status,-created_at',
            'sortAscending' => false,
            'baseUrl' => '/api/job-orders?include=customer'
        ]);

        return view('job-orders.index');
    }

    public function indexPending()
    {
        JavaScript::put([
            'filterable' => $this->availableFiltersForPendingIndex,
            'sorter' => 'status,-created_at',
            'sortAscending' => true,
            'baseUrl' => '/api/job-orders?status=draft&include=customer,panelProfile'
        ]);

        return view('job-orders.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, 'action' => 'create']);
        return view('job-orders.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id, 'action' => 'show']);

        $jobOrder = $this->jobOrderRecordService->getById($id);

        return view('job-orders.show', compact('jobOrder'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id, 'action' => 'edit']);

        $jobOrder = $this->jobOrderRecordService->getById($id);

        return view('job-orders.edit', compact('jobOrder'));
    }

    protected $availableFiltersForPendingIndex = [
        ["id" => "job_order_no", "text" => "Job Order No."],
        ['id' => 'date', 'text' => 'Date'],
        ['id' => 'customer', 'text' => 'Customer'],
        ["id" => "remarks", "text" => "Remarks"],

    ];
}
