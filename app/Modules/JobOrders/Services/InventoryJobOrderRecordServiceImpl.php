<?php

namespace App\Modules\JobOrders\Services;


use App\Modules\JobOrders\Models\JobOrder;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\DecoratorRecordServiceImpl;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use App\Modules\InventoryDetail\Models\InventoryDetail;
use CometOneSolutions\Inventory\Model\Inventoriable;
use App\Modules\Customer\Models\Customer;
use CometOneSolutions\Inventory\Model\ItemMovementType;
use App\Modules\PanelProfile\Models\PanelProfile;
use DateTime;


// In: Job Orders
// Out: Warehouses

class InventoryJobOrderRecordServiceImpl extends DecoratorRecordServiceImpl implements JobOrderRecordService
{
    protected $jobOrderRecordService;
    protected $itemMovementRecordService;
    protected $locationRecordService;

    public function __construct(
        JobOrderRecordService $jobOrderRecordService,
        ItemMovementRecordService $itemMovementRecordService,
        LocationRecordService $locationRecordService
    ) {
        parent::__construct($jobOrderRecordService);
        $this->jobOrderRecordService = $jobOrderRecordService;
        $this->itemMovementRecordService = $itemMovementRecordService;
        $this->locationRecordService = $locationRecordService;
    }

    protected function createItemMovements(Inventoriable $inventoriable, DateTime $date, $quantity, InventoryDetail $inventoryDetail)
    {
        $this->itemMovementRecordService->create(
            $inventoriable,
            $date,
            ItemMovementType::IN,
            $quantity,
            $this->locationRecordService->getByName('Job Orders'),
            $inventoryDetail
        );

        $this->itemMovementRecordService->create(
            $inventoriable,
            $date,
            ItemMovementType::OUT,
            $quantity,
            $this->locationRecordService->getByName('Warehouses'),
            $inventoryDetail
        );
        return $this;
    }

    public function create(
        $jobOrderNumber,
        $operator,
        $status,
        DateTime $date,
        Customer $customer,
        PanelProfile $panelProfile,
        array $jobOrderDetails,
        $remarks = null,
        User $user = null
    ) {
        return $this->jobOrderRecordService->create($jobOrderNumber, $operator, $status, $date, $customer, $panelProfile, $jobOrderDetails, $remarks, $user);
    }

    public function update(
        JobOrder $jobOrder,
        $jobOrderNumber,
        $date,
        $remarks,
        $operator,
        array $jobOrderDetails,
        Customer $customer,
        PanelProfile $panelProfile
    ) {
        return $this->jobOrderRecordService->update($jobOrder, $jobOrderNumber, $date, $remarks, $operator, $jobOrderDetails, $customer, $panelProfile);
    }

    public function approve(JobOrder $jobOrder)
    {
        $created = $this->jobOrderRecordService->approve($jobOrder);
        foreach ($created->getJobOrderDetails() as $detail) {
            $date = $created->getDate();
            $quantity = $detail->getWeight();
            $inventoryDetail = $detail->getInventoryDetail();
            $this->createItemMovements($detail, $date, $quantity, $inventoryDetail);
        }
        return $created;
    }

    public function delete(JobOrder $jobOrder)
    {
        return $this->jobOrderRecordService->delete($jobOrder);
    }

    public function approveAll()
    {
        $data = $this->jobOrderRecordService->approveAll();
        foreach ($data['jobOrdersCreated'] as $created) {
            foreach ($created->getJobOrderDetails() as $detail) {
                $date = $created->getDate();
                $quantity = $detail->getWeight();
                $inventoryDetail = $detail->getInventoryDetail();
                $this->createItemMovements($detail, $date, $quantity, $inventoryDetail);
            }
        }

        return $data;
    }
}
