<?php

namespace App\Modules\JobOrders\Services;

use App\Modules\Customer\Models\Customer;
use App\Modules\PanelProfile\Models\PanelProfile;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use App\Modules\JobOrders\Repositories\JobOrders;
use App\Modules\JobOrders\Models\JobOrder;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordService;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Utils\Filter;
use \DateTime;

class JobOrderRecordServiceImpl extends RecordServiceImpl implements JobOrderRecordService
{
    protected $jobOrders;
    protected $jobOrder;
    private $inventoryDetailRecordService;

    protected $availableFilters = [
        ["id" => "job_order_no", "text" => "Job Order No."],
        ['id' => 'date', 'text' => 'Date'],
        ['id' => 'customer', 'text' => 'Customer'],
        ["id" => "remarks", "text" => "Remarks"],


    ];

    public function __construct(JobOrders $jobOrders, JobOrder $jobOrder, InventoryDetailRecordService $inventoryDetailRecordService)
    {
        parent::__construct($jobOrders);
        $this->jobOrders = $jobOrders;
        $this->inventoryDetailRecordService = $inventoryDetailRecordService;
        $this->jobOrder = $jobOrder;
    }

    public function create(
        $jobOrderNumber,
        $operator,
        $status,
        DateTime $date,
        Customer $customer,
        PanelProfile $panelProfile,
        array $jobOrderDetails,
        $remarks = null,
        User $user = null
    ) {
        $jobOrder = new $this->jobOrder;
        $jobOrder->setJobOrderNumber($jobOrderNumber);
        $jobOrder->setOperator($operator);
        $jobOrder->setDate($date);

        $jobOrder->setPanelprofile($panelProfile);
        $jobOrder->setCustomer($customer);
        $jobOrder->setStatus($status);
        $jobOrder->setJobOrderDetails($jobOrderDetails);
        $jobOrder->setRemarks($remarks);

        if ($user != null) {
            $jobOrder->setUpdatedByUser($user);
        }
        $this->jobOrders->save($jobOrder);
        return $jobOrder;
    }

    public function update(
        JobOrder $jobOrder,
        $jobOrderNumber,
        $date,
        $remarks,
        $operator,
        array $jobOrderDetails,
        Customer $customer,
        PanelProfile $panelProfile
    ) {
        $tmpJobOrder = clone $jobOrder;
        $tmpJobOrder->setJobOrderNumber($jobOrderNumber);
        $tmpJobOrder->setDate($date);
        $tmpJobOrder->setRemarks($remarks);
        $tmpJobOrder->setOperator($operator);
        $tmpJobOrder->setJobOrderDetails($jobOrderDetails);
        $tmpJobOrder->setCustomer($customer);
        $tmpJobOrder->setPanelprofile($panelProfile);
        $this->jobOrders->save($tmpJobOrder);
        return $tmpJobOrder;
    }
    public function approveAll()
    {
        $filter = new Filter('status', JobOrder::DRAFT);
        $draftJobOrders = $this->getAll([$filter]);
        $approved = 0;
        $messages = [];
        $jobOrdersCreated = [];

        foreach ($draftJobOrders as $jobOrder) {
            if ($this->verifyIfWithStock($jobOrder, $messages)) {
                $jobOrdersCreated[] = $this->approve($jobOrder);
                $approved++;
            }
        }

        $response = sprintf("Successfully approved <strong>%s</strong> JO/s. <br><br>", $approved);
        if (count($messages) > 0) {
            $response = $response . "The following has insufficient stock: <br>";
            for ($i = 0; $i < count($messages); $i++) {
                $response = (count($messages) != $i + 1) ? $response . $messages[$i] . ", <br>" : $response . $messages[$i];
            }
        }

        return [
            'response' => $response,
            'jobOrdersCreated' => $jobOrdersCreated
        ];
    }
    public function approve(
        JobOrder $jobOrder
    ) {
        $tmpJobOrder = clone $jobOrder;
        $tmpJobOrder->setStatus(JobOrder::APPROVED);
        $this->jobOrders->save($tmpJobOrder);
        $this->inventoryDetailRecordService->updateInventoryStockFromJobOrder($tmpJobOrder);
        // return $this->getById($jobOrder->getId());
        return $tmpJobOrder;
    }

    public function delete(JobOrder $jobOrder)
    {
        // TODO delete checks
        $this->jobOrders->delete($jobOrder);
        $this->inventoryDetailRecordService->updateInventoryStockFromJobOrder($jobOrder);
        return $jobOrder;
    }

    public function verifyIfWithStock(JobOrder $jobOrder, &$messages)
    {
        $isValid = true;
        foreach ($jobOrder->getJobOrderDetails() as $jobOrderDetail) {
            if ($jobOrderDetail->getInventoryDetail()->getQuantity() < $jobOrderDetail->getWeight()) {
                $isValid = false;
                if (!in_array($jobOrder->getJobOrderNumber(), $messages)) {
                    $messages[] = $jobOrder->getJobOrderNumber();
                }
            }
        }
        return $isValid;
    }
}
