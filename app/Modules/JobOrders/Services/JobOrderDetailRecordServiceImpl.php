<?php

namespace App\Modules\JobOrders\Services;

use App\Modules\JobOrders\Repositories\JobOrderDetails;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class JobOrderDetailRecordServiceImpl extends RecordServiceImpl implements JobOrderDetailRecordService
{
    public function __construct(JobOrderDetails $jobOrderDetails)
    {
        parent::__construct($jobOrderDetails);
    }

    public function make()
    {
        // TODO
    }
}
