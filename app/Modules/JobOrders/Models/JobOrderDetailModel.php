<?php

namespace App\Modules\JobOrders\Models;

use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Inventory\Model\Inventoriable;
use App\Modules\InventoryDetail\Models\InventoryDetail;
use CometOneSolutions\Inventory\Traits\HasItemMovements;
use App\Modules\InventoryDetail\Models\InventoryDetailModel;

class JobOrderDetailModel extends Model implements JobOrderDetail, Inventoriable
{
    use HasItemMovements;

    protected $table = 'job_order_details';

    public function jobOrder()
    {
        return $this->belongsTo(JobOrderModel::class, 'job_order_id');
    }

    public function inventoryDetail()
    {
        return $this->belongsTo(InventoryDetailModel::class, 'inventory_detail_id');
    }

    public function getId()
    {
        return (int) $this->id;
    }

    public function getJobOrder()
    {
        return $this->jobOrder;
    }

    public function setJobOrder(JobOrder $jobOrder)
    {
        $this->jobOrder()->associate($jobOrder);
        $this->job_order_id = $jobOrder->getId();
        return $this;
    }

    public function scopeApproved($query)
    {
        return $query->whereHas('jobOrder', function ($jobOrder) {
            return $jobOrder->whereStatus('Approved');
        });
    }

    public function getInventoryDetail()
    {
        return $this->inventoryDetail;
    }
    public function getInventoryDetailId()
    {
        return $this->inventory_detail_id;
    }
    public function getInventoryDetailReference()
    {
        return ($this->getInventoryDetail()->getSlitIdentifier() != null)
            ? $this->getInventoryDetail()->getSlitIdentifier()
            : $this->getInventoryDetail()->getReceiving()->getCoilNumber();
    }
    public function setInventoryDetail(InventoryDetail $inventoryDetail)
    {
        $this->inventoryDetail()->associate($inventoryDetail);
        $this->inventory_detail_id = $inventoryDetail->getId();
        return $this;
    }
    public function setLength($value)
    {
        $this->length = $value;
        return $this;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function setWeight($value)
    {
        $this->weight = $value;
        return $this;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getInventoryRefNo()
    {
        return $this->getJobOrder()->getJobOrderNumber();
    }

    public function getInventoryUri()
    {
        return route('job-order_show', $this->getJobOrder()->getId());
    }
    public function getFactor()
    {
        if ($this->getWeight() == 0) {
            return '--';
        }
        return round(($this->getLength() / $this->getWeight()), 2);
    }
    public function getOtherDraftJobOrderDetails()
    {
        $this->load(['inventoryDetail.jobOrderDetails.jobOrder']);
        $others = collect();
        foreach ($this->getInventoryDetail()->getJobOrderDetails() as $jobOrderDetail) {
            if ($jobOrderDetail->getId() != $this->getId() && $jobOrderDetail->getJobOrder()->getStatus() == JobOrder::DRAFT) {
                $others->push($jobOrderDetail);
            }
        }

        return $others->sortBy(function ($other, $key) {
            return $other->created_at;
        });
    }
}
