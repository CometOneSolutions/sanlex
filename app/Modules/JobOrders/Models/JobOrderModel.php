<?php

namespace App\Modules\JobOrders\Models;

use App\Modules\Customer\Models\Customer;
use App\Modules\Customer\Models\CustomerModel;
use App\Modules\PanelProfile\Models\PanelProfile;
use App\Modules\PanelProfile\Models\PanelProfileModel;
use App\Modules\Product\Models\Product;
use App\Modules\Product\Models\ProductModel;
use CometOneSolutions\Common\Models\C1Model;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use App\Modules\Inventory\Inventoriable;

class JobOrderModel extends C1Model implements JobOrder, UpdatableByUser, Inventoriable
{
    use HasUpdatedByUser;

    protected $table = 'job_orders';
    protected $dates = ['date'];
    private $jobOrderDetailsToSet = null;

    public function jobOrderDetails()
    {
        return $this->hasMany(JobOrderDetailModel::class, 'job_order_id');
    }
    public function product()
    {
        return $this->belongsTo(ProductModel::class, 'product_id');
    }
    public function panelProfile()
    {
        return $this->belongsTo(PanelProfileModel::class, 'panel_profile_id');
    }
    public function customer()
    {
        return $this->belongsTo(CustomerModel::class, 'customer_id');
    }
    public function getProduct()
    {
        return $this->product;
    }
    public function setProduct(Product $product)
    {
        $this->product()->associate($product);
        return $this;
    }
    public function getPanelProfile()
    {
        return $this->panelProfile;
    }

    public function getPanelProfileId()
    {
        return (int)$this->panel_profile_id;
    }

    public function setPanelProfile(PanelProfile $panelProfile)
    {
        $this->panelProfile()->associate($panelProfile);
        return $this;
    }
    public function getCustomer()
    {
        return $this->customer;
    }
    public function setCustomer(Customer $customer)
    {
        $this->customer()->associate($customer);
        return $this;
    }
    public function getId()
    {
        return (int)$this->id;
    }
    public function getCustomerId()
    {
        return (int)$this->customer_id;
    }
    public function getProductId()
    {
        return (int)$this->product_id;
    }
    public function setJobOrderNumber($value)
    {
        $this->job_order_no = $value;
        return $this;
    }

    public function getJobOrderNumber()
    {
        return $this->job_order_no;
    }

    public function setDate($value)
    {
        $this->date = $value;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setRemarks($value)
    {
        $this->remarks = $value;
        return $this;
    }

    public function getRemarks()
    {
        return $this->remarks;
    }

    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function isDraft()
    {
        return $this->getStatus() == JobOrder::DRAFT;
    }

    public function setOperator($value)
    {
        $this->operator = $value;
        return $this;
    }

    public function getOperator()
    {
        return $this->operator;
    }

    public function setJobOrderDetails(array $jobOrderDetails)
    {
        $this->jobOrderDetailsToSet = $jobOrderDetails;
        return $this;
    }

    public function getJobOrderDetails()
    {
        if ($this->jobOrderDetailsToSet !== null) {
            return collect($this->jobOrderDetailsToSet);
        }
        return $this->jobOrderDetails;
    }
}
