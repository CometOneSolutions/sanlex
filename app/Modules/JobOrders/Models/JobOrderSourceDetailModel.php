<?php

namespace App\Modules\JobOrders\Models;

use App\Modules\InventoryDetail\Models\InventoryDetail;
use App\Modules\InventoryDetail\Models\InventoryDetailModel;
use Illuminate\Database\Eloquent\Model;

class JobOrderSourceDetailModel extends Model implements JobOrderSourceDetail
{
    protected $table = 'sheeting_report_source_details';

    public function inventoryDetail()
    {
        return $this->belongsTo(InventoryDetailModel::class, 'inventory_detail_id');
    }

    public function jobOrder()
    {
        return $this->belongsTo(JobOrderModel::class, 'sheeting_report_id');
    }
    public function getId()
    {
        return (int)$this->id;
    }
    public function getInventoryDetailId()
    {
        return (int)$this->inventory_detail_id;
    }
    public function getInventoryDetail()
    {
        return $this->inventoryDetail;
    }

    public function setInventoryDetail(InventoryDetail $inventoryDetail)
    {
        $this->inventoryDetail()->associate($inventoryDetail);
        $this->inventory_detail_id = $inventoryDetail->getId();
        return $this;
    }

    public function getJobOrder()
    {
        return $this->jobOrder;
    }

    public function setJobOrder(JobOrder $jobOrder)
    {
        $this->jobOrder()->associate($jobOrder);
        $this->sheeting_report_id = $jobOrder->getId();
        return $this;
    }

    public function setRemainingQuantity($value)
    {
        // $this->setConsumedQuantity($this->getInventoryDetail()->getQuantity() - $value);
        $this->remaining_quantity = $value;
        return $this;
    }

    public function getRemainingQuantity()
    {
        return $this->remaining_quantity;
    }

    public function setConsumedQuantity($value)
    {
        $this->consumed_quantity = $value;
        return $this;
    }

    public function getConsumedQuantity()
    {
        return $this->consumed_quantity;
    }
}
