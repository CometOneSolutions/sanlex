<?php

namespace App\Modules\JobOrders\Models;

use App\Modules\Customer\Models\Customer;
use App\Modules\PanelProfile\Models\PanelProfile;
use App\Modules\Product\Models\Product;

interface JobOrder
{
    const DRAFT = 'Draft';
    const APPROVED = 'Approved';

    public function getId();

    public function setJobOrderNumber($value);

    public function getJobOrderNumber();

    public function setDate($value);

    public function getDate();

    public function setOperator($value);

    public function getOperator();

    public function setStatus($value);

    public function getStatus();

    public function getProduct();

    public function setProduct(Product $product);

    public function getCustomer();

    public function setCustomer(Customer $customer);

    public function getPanelProfile();

    public function setPanelProfile(PanelProfile $panelProfile);
}
