<?php

namespace App\Modules\JobOrders\Repositories;

use CometOneSolutions\Common\Repositories\Repository;

interface JobOrderDetails extends Repository
{
    public function getTransit();
    public function getStock();
    public function getOut();
    public function renew();
}
