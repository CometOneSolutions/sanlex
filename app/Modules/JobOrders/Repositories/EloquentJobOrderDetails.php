<?php

namespace App\Modules\JobOrders\Repositories;


use App\Modules\JobOrders\Models\JobOrderDetail;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentJobOrderDetails extends EloquentRepository implements JobOrderDetails
{
    protected static $filterable = [
        'in_stock', 'referenced_by_sales_order'
    ];

    protected $model;

    public function __construct(JobOrderDetail $model)
    {
        $this->model = $model;
    }

    public function renew()
    {
        if (!$this->model instanceof JobOrderDetail) {
            $this->model = new JobOrderDetail();
        }
        return $this;
    }

    public function getTransit()
    {
        return JobOrderDetail::TRANSIT;
    }

    public function getStock()
    {
        return JobOrderDetail::STOCK;
    }

    public function getOut()
    {
        return JobOrderDetail::OUT;
    }

    public function filterByInStock($query, $value = true)
    {
        $query->where('quantity_balance', '>', 0)->where('status', '=', 'IN_STOCK');
    }

    public function filterByReferencedBySalesOrder($query, $value)
    {
        $query->orWhereHas('salesOrderDetails', function ($salesOrderDetail) use ($value) {
            return $salesOrderDetail->whereSalesOrderId($value);
        });
    }

    public function save(JobOrderDetail $JobOrderDetail)
    {
        return $JobOrderDetail->save();
    }

    public function delete(JobOrderDetail $JobOrderDetail)
    {
        return $JobOrderDetail->delete();
    }
}
