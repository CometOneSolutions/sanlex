<?php

namespace App\Modules\JobOrders\Repositories;

use App\Modules\JobOrders\Models\JobOrder;
use App\Modules\JobOrders\Models\JobOrderModel;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentJobOrders extends EloquentRepository implements JobOrders
{
    public function __construct(JobOrder $jobOrder)
    {
        parent::__construct($jobOrder);
    }

    public function save(JobOrder $jobOrder)
    {
        $result = $jobOrder->save();
        $jobOrder->jobOrderDetails()->sync($jobOrder->getJobOrderDetails());

        return $result;
    }

    private function getNextCodeString()
    {
        $lastNumber = $this->getLastCodeWithPrefix();
        return 'SR' . str_pad(($lastNumber + 1), 5, '0', STR_PAD_LEFT);
    }

    public function filterByPanelProfile($name)
    {
        return $this->model->whereHas('panelProfile', function ($panelProfile) use ($name) {
            return $panelProfile->where('name', 'LIKE', $name);
        });
    }

    public function filterByCustomer($name)
    {
        return $this->model->whereHas('customer', function ($customer) use ($name) {
            return $customer->where('name', 'LIKE', $name);
        });
    }

    private function getLastCodeWithPrefix()
    {
        $lastPO = JobOrderModel::orderBy('sheeting_report_no', 'desc')->first();
        if (!$lastPO) {
            return 0;
        }
        $lastPoNum = (int)ltrim(str_replace('SR', '', $lastPO->getJobOrderNumber()), '0');

        return $lastPoNum;
    }

    protected function orderByPanelProfileName($direction)
    {
        return $this->model->join('panel_profiles', 'panel_profiles.id', 'sheeting_reports.panel_profile_id')
            ->select('panel_profiles.name as panel_profile_name', 'sheeting_reports.*')
            ->orderBy('panel_profile_name', $direction);
    }

    protected function orderByProductName($direction)
    {
        return $this->model->join('products', 'products.id', 'sheeting_reports.product_id')
            ->select('products.name as product_name', 'sheeting_reports.*')
            ->orderBy('product_name', $direction);
    }

    public function delete(JobOrder $jobOrder)
    {
        return $jobOrder->delete();
    }
}
