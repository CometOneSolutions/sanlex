<?php

namespace App\Modules\JobOrders\Repositories;

use CometOneSolutions\Common\Repositories\Repository;
use App\Modules\JobOrders\Models\JobOrder;

interface JobOrders extends Repository
{
    public function save(JobOrder $jobOrder);
    public function delete(JobOrder $jobOrder);
}
