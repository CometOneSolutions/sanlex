<?php

namespace App\Modules\JobOrders\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateJobOrder extends JobOrderRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [OUT] Job Order');
    }
}
