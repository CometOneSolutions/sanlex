<?php

namespace App\Modules\JobOrders\Requests;

use App\Modules\JobOrders\Models\JobOrderDetailModel;
use Dingo\Api\Http\FormRequest;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordService;
use App\Modules\Customer\Services\CustomerRecordService;
use App\Modules\PanelProfile\Services\PanelProfileRecordService;
use \DateTime;
use App\Modules\JobOrders\Services\JobOrderDetailRecordService;

class JobOrderRequest extends FormRequest
{
    private $inventoryDetailRecordService;
    // private $productRecordService;
    private $customerRecordService;
    private $panelProfileRecordService;
    private $jobOrderDetailRecordService;

    public function __construct(
        InventoryDetailRecordService $inventoryDetailRecordService,
        CustomerRecordService $customerRecordService,
        JobOrderDetailRecordService $jobOrderDetailRecordService,
        PanelProfileRecordService $panelProfileRecordService
    ) {
        $this->inventoryDetailRecordService = $inventoryDetailRecordService;
        // $this->productRecordService = $productRecordService;
        $this->jobOrderDetailRecordService = $jobOrderDetailRecordService;
        $this->customerRecordService = $customerRecordService;
        $this->panelProfileRecordService = $panelProfileRecordService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'jobOrderNumber' => 'required|unique:job_orders,job_order_no,' . $this->route('jobOrderId') ?? null,
            'date' => 'required',
            'customerId'  => 'required',
            'panelProfileId'    => 'required',
            'operator'          => 'required'
        ];
    }
    public function messages()
    {
        return [
            'jobOrderNumber.required'       => 'Job Order No. is required.',
            'jobOrderNumber.unique'         => 'Job Order No. must be unique.',
            'date.required'                 => 'Date must be required.',
            'customerId.required'           => 'Customer is required.',
            'panelProfileId.required'       => 'Panel Profile is required.',
            'operator.required'             => 'Operator is required.'
        ];
    }
    public function getJobOrderNumber($index = 'jobOrderNumber')
    {
        return $this->input($index);
    }

    // public function getProduct($index = 'productId')
    // {
    //     return $this->productRecordService->getById($this->input($index));
    // }
    public function getPanelProfile($index = 'panelProfileId')
    {
        return $this->panelProfileRecordService->getById($this->input($index));
    }
    public function getDate($index = 'date')
    {
        return new DateTime($this->input($index));
    }

    public function getStatus($index = 'status')
    {
        return $this->input($index);
    }

    public function getOperator($index = 'operator')
    {
        return $this->input($index);
    }

    public function getCustomer($index = 'customerId')
    {
        return $this->customerRecordService->getById($this->input($index));
    }

    public function getJobOrderDetails($fieldName = 'jobOrderDetails.data')
    {
        return array_map(function ($itemArray) {
            if (isset($itemArray['id']) && $itemArray['id'] > 0) {
                $jobOrderDetail = $this->jobOrderDetailRecordService->getById($itemArray['id']);
            } else {
                $jobOrderDetail = new JobOrderDetailModel();
            }

            $jobOrderDetail->setInventoryDetail($this->inventoryDetailRecordService->getById($itemArray['inventoryDetailId']));
            $jobOrderDetail->setLength($itemArray['length']);
            $jobOrderDetail->setWeight($itemArray['weight']);
            return $jobOrderDetail;
        }, $this->input($fieldName));
    }

    public function getRemarks()
    {
        return $this->input('remarks');
    }
}
