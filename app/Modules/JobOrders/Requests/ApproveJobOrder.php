<?php

namespace App\Modules\JobOrders\Requests;

use Dingo\Api\Http\FormRequest;

class ApproveJobOrder extends FormRequest
{

    public function authorize()
    {
        return $this->user()->can('Approve [OUT] Job Order');
    }

    public function rules()
    {
        return [

        ];
    }
    
}
