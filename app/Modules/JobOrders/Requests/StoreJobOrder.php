<?php

namespace App\Modules\JobOrders\Requests;


class StoreJobOrder extends JobOrderRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [OUT] Job Order');
    }
}
