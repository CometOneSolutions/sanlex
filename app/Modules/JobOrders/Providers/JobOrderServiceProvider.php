<?php

namespace App\Modules\JobOrders\Providers;

use App\Modules\JobOrders\Models\JobOrder;
use App\Modules\JobOrders\Models\JobOrderDetail;
use App\Modules\JobOrders\Models\JobOrderDetailModel;
use App\Modules\JobOrders\Models\JobOrderModel;
use App\Modules\JobOrders\Repositories\EloquentJobOrderDetails;
use App\Modules\JobOrders\Repositories\EloquentJobOrders;
use App\Modules\JobOrders\Repositories\JobOrderDetails;
use App\Modules\JobOrders\Repositories\JobOrders;
use App\Modules\JobOrders\Services\InventoryJobOrderRecordServiceImpl;
use App\Modules\JobOrders\Services\JobOrderDetailRecordService;
use App\Modules\JobOrders\Services\JobOrderDetailRecordServiceImpl;
use App\Modules\JobOrders\Services\JobOrderRecordService;
use App\Modules\JobOrders\Services\JobOrderRecordServiceImpl;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;

class JobOrderServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/JobOrder/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        // $this->app->bind(JobOrderRecordService::class, JobOrderRecordServiceImpl::class);
        $this->app->bind(JobOrders::class, EloquentJobOrders::class);

        $this->app->bind(JobOrder::class, JobOrderModel::class);

        $this->app->singleton(JobOrderRecordService::class, function ($app) {

            $jobOrderRecordServiceImpl = $app->make(JobOrderRecordServiceImpl::class);

            $inventoryRecordServiceImpl = new InventoryJobOrderRecordServiceImpl(
                $jobOrderRecordServiceImpl,
                $app->make(ItemMovementRecordService::class),
                $app->make(LocationRecordService::class)
            );

            return $inventoryRecordServiceImpl;
        });

        $this->app->bind(JobOrderDetailRecordService::class, JobOrderDetailRecordServiceImpl::class);
        $this->app->bind(JobOrderDetails::class, EloquentJobOrderDetails::class);
        $this->app->bind(JobOrderDetail::class, JobOrderDetailModel::class);

        // $this->app->bind(JobOrderSourceRecordService::class, JobOrderSourceRecordServiceImpl::class);
        // $this->app->bind(JobOrderSources::class, EloquentJobOrderSources::class);
        // $this->app->bind(JobOrderSource::class, JobOrderSourceModel::class);
    }
}
