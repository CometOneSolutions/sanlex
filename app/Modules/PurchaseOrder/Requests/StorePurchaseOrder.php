<?php

namespace App\Modules\PurchaseOrder\Requests;

class StorePurchaseOrder extends PurchaseOrderRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [PUR] Purchase Order');
    }
}
