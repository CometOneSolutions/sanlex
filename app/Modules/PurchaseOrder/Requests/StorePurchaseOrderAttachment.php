<?php

namespace App\Modules\PurchaseOrder\Requests;

use Dingo\Api\Http\FormRequest;

class StorePurchaseOrderAttachment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getDescription()
    {
        return $this->input('description');
    }

    public function getFile()
    {
        return $this->file('file');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
}
