<?php

namespace App\Modules\PurchaseOrder\Requests;

use Dingo\Api\Http\FormRequest;

class ImportReceiving extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }
    public function messages()
    {
        return [
        ];
    }
}
