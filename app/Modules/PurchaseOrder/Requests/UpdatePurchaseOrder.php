<?php

namespace App\Modules\PurchaseOrder\Requests;

class UpdatePurchaseOrder extends PurchaseOrderRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [PUR] Purchase Order');
    }
}
