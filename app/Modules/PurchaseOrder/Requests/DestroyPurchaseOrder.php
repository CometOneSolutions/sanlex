<?php

namespace App\Modules\PurchaseOrder\Requests;

use Dingo\Api\Http\FormRequest;


class DestroyPurchaseOrder extends FormRequest
{
   
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('Delete [PUR] Purchase Order');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }
    public function messages()
    {
        return [

        ];
    }


}
