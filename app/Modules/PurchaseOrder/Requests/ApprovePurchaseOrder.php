<?php

namespace App\Modules\PurchaseOrder\Requests;

class ApprovePurchaseOrder extends PurchaseOrderRequest
{
    public function authorize()
    {
        return $this->user()->can('Approve [OUT] Purchase Order');
    }
}
