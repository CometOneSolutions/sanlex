<?php

namespace App\Modules\PurchaseOrder\Requests;

use App\Modules\PurchaseOrderDetail\Models\PurchaseOrderDetailModel;
use Dingo\Api\Http\FormRequest;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\PurchaseOrder\Services\PurchaseOrderRecordService;
use \DateTime;
use App\Modules\Uom\Services\UomRecordService;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\PurchaseOrderDetail\Services\PurchaseOrderDetailRecordService;




class PurchaseOrderRequest extends FormRequest
{
    protected $supplierRecordService;

    protected $uomRecordService;
    protected $productRecordService;
    protected $purchaseOrderDetailRecordService;

    protected $purchaseOrderRecordService;

    public function __construct(
        SupplierRecordService $supplierRecordService,
        UomRecordService $uomRecordService,
        ProductRecordService $productRecordService,
        PurchaseOrderDetailRecordService $purchaseOrderDetailRecordService,
        PurchaseOrderRecordService $purchaseOrderRecordService
    ) {
        $this->supplierRecordService = $supplierRecordService;
        $this->uomRecordService = $uomRecordService;
        $this->purchaseOrderDetailRecordService = $purchaseOrderDetailRecordService;
        $this->productRecordService = $productRecordService;
        $this->purchaseOrderRecordService = $purchaseOrderRecordService;
    }

    public function getSupplier($index = 'supplierId')
    {
        return $this->supplierRecordService->getById($this->input($index));
    }

    public function getDate($index = 'date')
    {
        return new DateTime($this->input($index));
    }

    public function getRemarks($index = 'remarks')
    {
        return $this->input($index) ?? null;
    }

    public function getStatus($index = 'status')
    {
        return $this->input($index);
    }

    public function getProductType($index = 'productType')
    {
        return $this->input($index);
    }
    public function getPONumber($index = 'purchaseNumber')
    {
        return $this->input($index);
    }

    public function getPurchaseOrder($index = 'id')
    {
        return $this->purchaseOrderRecordService->getById($this->input($index));
    }


    public function getPurchaseOrderDetails($index = 'purchaseOrderDetails.data')
    {
        return array_map(function ($itemArray) {
            // @Ernest
            // if ($itemArray['id'] != null) {

            if (isset($itemArray['id']) && $itemArray['id'] > 0) {
                $purchaseOrderDetail = $this->purchaseOrderDetailRecordService->getById($itemArray['id']);
            } else {
                $purchaseOrderDetail = new PurchaseOrderDetailModel();
            }

            $product = $this->productRecordService->getById($itemArray['productId']);
            $uom = $product->getUom();
            $purchaseOrderDetail->setUom($uom);
            $purchaseOrderDetail->setProduct($product);
            $purchaseOrderDetail->setOrderedQuantity($itemArray['quantity']);
            return $purchaseOrderDetail;
        }, $this->input($index));
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'shippingMark'  => 'required|unique:purchase_orders,shipping_mark,' . $this->route('purchaseOrderId') ?? null,
            'purchaseNumber'            => 'required|unique:purchase_orders,po_no,' . $this->route('purchaseOrderId') ?? null,
            'supplierId'    => 'required',
            'date'          => 'required'
            // 'proformaNumber' => Rule::unique('purchase_orders',"proforma_no")->whereNotNull('proforma_no'),
            // 'supplierOrderCode' => 'required',
            // 'creditLimit' => 'alpha'
        ];
    }

    public function messages()
    {
        return [
            'purchaseNumber.required'           => 'PO reference is required.',
            'purchaseNumber.unique'             => 'PO reference already exists.',
            'supplierId'            => 'Supplier is required.',
            'date'                  => 'Date is required',
            // 'shippingMark.required' => 'Shipping mark is required.',
            // 'shippingMark.unique'   => 'Shipping mark must be unique.'
            // 'name.numeric' => 'Name must be numeric.',
            // 'creditLimit.alpha' => 'Credit must be in alpha.'
        ];
    }
}
