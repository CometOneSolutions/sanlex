<?php

namespace App\Modules\PurchaseOrder\Export;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\PurchaseOrderDetail\Models\PurchaseOrderDetail;

class CoilExportModel
{
    protected $purchaseOrderDetail;
    protected $productType;
    protected $productable;
    protected $data;

    public function __construct(
        PurchaseOrderDetail $purchaseOrderDetail,
        PurchaseOrder $purchaseOrder
    ) {
        $this->purchaseOrderDetail = $purchaseOrderDetail;
        $this->purchaseOrder = $purchaseOrder;

        $this->productType = $purchaseOrder->getProductType();
        $this->productable = $purchaseOrderDetail->getProduct()->productable;
        $this->data = $this->initData();
    }

    public function initData()
    {
        switch ($this->productType) {
            case PurchaseOrder::COIL:
                return $this->getCoilData();
                break;
        }
    }

    public function getCoilData()
    {
        $data = [];

        $data['SUPPLIER CODE']      = $this->productable->getProduct()->getSupplier()->getShortCode();
        $data['COIL NUMBER']        = '';
        $data['COATING']            = $this->productable->getCoating()->getName();
        $data['WIDTH']              = $this->productable->getWidth()->getName();
        $data['THICKNESS']          = $this->productable->getThickness()->getName();
        $data['NW (MT)']            = $this->purchaseOrderDetail->getOrderedQuantity();
        $data['COLOR']              = $this->productable->getColor()->getName();
        $data['LENGTH (LIN MTR)']   = '';

        return $data;
    }

    public function getByHeaderName($headerName)
    {
        return $this->data[$headerName];
    }

    public function getData()
    {
        return collect($this->data);
    }
}
