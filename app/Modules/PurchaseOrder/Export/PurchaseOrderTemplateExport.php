<?php

namespace App\Modules\PurchaseOrder\Export;


use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use \PhpOffice\PhpSpreadsheet\Cell\DataType;


class PurchaseOrderTemplateExport implements WithEvents, WithTitle
{
    protected $data;
    protected $columns;

    public function __construct($data)
    {
        $this->data = $data;
        $this->columns = range('A', 'Z');
    }

    public function title(): String
    {
        return 'Template';
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $this->setHeaders($event);
                $this->setRows($event);
            }
        ];
    }

    private function setHeaders($event)
    {
        $this->data->getHeaders()->each(function ($value, $index) use ($event) {
            $rowPointer = '1';
            $columnPointer = $this->columns[$index];
            $cellCoordinate = sprintf('%s%s', $columnPointer, $rowPointer);

            $cell = $event->sheet->getCell($cellCoordinate);
            $cell->setValue($value);

            $event->sheet->getColumnDimension($columnPointer)->setAutoSize(true);
        });
    }

    private function setRows($event)
    {
        $highestRow = $event->sheet->getHighestRow();

        $this->data->getCoils()->each(function ($coil, $coilIndex) use ($event, $highestRow) {
            $this->data->getHeaders()->each(function ($header, $headerIndex) use ($event, $coil, $coilIndex, $highestRow) {

                $text = $coil->getByHeaderName($header);
                $rowPointer = ($coilIndex + 1) + $highestRow;
                $columnPointer = $this->columns[$headerIndex];
                $cellCoordinate = sprintf('%s%s', $columnPointer, $rowPointer);

                $event->sheet->setCellValueExplicit($cellCoordinate, $text, DataType::TYPE_STRING);
            });
        });
    }
}
