<?php

namespace App\Modules\PurchaseOrder\Export;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;

class PurchaseOrderExportModel
{
    protected $purchaseOrder;
    protected $purchaseOrderDetails;
    protected $coils;
    protected $receivings;

    public function __construct(PurchaseOrder $purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
        $this->purchaseOrderDetails = $purchaseOrder->getPurchaseOrderDetails();
        $this->receivings = $purchaseOrder->getReceivings();
        $this->productType = $purchaseOrder->getProductType();

        $this->coils = $this->initCoils();
        $this->headers = $this->initHeaders();
    }

    protected function initCoils()
    {
        if ($this->purchaseOrder->hasReceivings()) {
            return $this->receivings->map(function ($receiving) {
                return new CoilWithReceivingExportModel($receiving, $this->purchaseOrder);
            });
        } else {
            return $this->purchaseOrderDetails->map(function ($purchaseOrderDetail) {
                return new CoilExportModel($purchaseOrderDetail, $this->purchaseOrder);
            });
        }
    }

    protected function initHeaders()
    {
        return $this->coils->first()->getData()->keys();
    }

    public function getCoils()
    {
        return $this->coils;
    }

    public function getHeaders()
    {
        return $this->headers;
    }
}
