<?php

namespace App\Modules\PurchaseOrder\Repositories;


use App\Modules\Inventory\Services\InventoryRecordService;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentPurchaseOrders extends EloquentRepository implements PurchaseOrders
{
    protected $outboundPayments;


    public function __construct(
        PurchaseOrder $purchaseOrder,
        InventoryRecordService $inventoryRecordService
    ) {
        parent::__construct($purchaseOrder);
    }

    public function save(PurchaseOrder $purchaseOrder)
    {
        // if ($purchaseOrder->getPONumber() == '') {
        //     $nextPoNumber = $this->getNextCodeString();
        //     $purchaseOrder->setPONumber($nextPoNumber);
        // }

        $result = $purchaseOrder->save();
        $purchaseOrder->purchaseOrderDetails()->sync($purchaseOrder->getPurchaseOrderDetails());

        return $result;
    }


    protected function filterBySupplierName($value)
    {
        return $this->model->whereHas('supplier', function ($supplier) use ($value) {
            return $supplier->where('name', 'LIKE', $value);
        });
    }

    protected function orderBySupplierName($direction)
    {
        return $this->model->join('suppliers', 'suppliers.id', 'purchase_orders.supplier_id')
            ->select('suppliers.name as supplier_name', 'purchase_orders.*')
            ->orderBy('supplier_name', $direction);
    }


    protected function filterByHasNoPackingList($value)
    {
        return $this->model->whereDoesntHave('packingList');
    }

    private function getNextCodeString()
    {
        $lastNumber = $this->getLastCodeWithPrefix();
        return 'PO' . str_pad(($lastNumber + 1), 5, '0', STR_PAD_LEFT);
    }

    private function getLastCodeWithPrefix()
    {
        $lastPO = PurchaseOrderModel::orderBy('po_no', 'desc')->first();
        if (!$lastPO) {
            return 0;
        }
        $lastPoNum = (int)ltrim(str_replace('PO', '', $lastPO->getPONumber()), '0');
        return $lastPoNum;
    }

    public function delete(PurchaseOrder $purchaseOrder)
    {
        return $purchaseOrder->delete();
    }

    // protected function syncOutboundPayments(PurchaseOrder $purchaseOrder)
    // {
    //     $this->saveNewOutboundPayments($purchaseOrder);
    //     // $this->syncExistingOutboundPayments($purchaseOrder);
    //     return $this;
    // }

    // protected function saveNewOutboundPayments(PurchaseOrder $purchaseOrder)
    // {
    //     $purchaseOrder->getOutboundPayments()->filter(function ($outboundPayment) {
    //         return $outboundPayment->getId() == null;
    //     })->values()->each(function ($outboundPayment) use ($purchaseOrder) {
    //         $purchaseOrder->outboundPayments()->save($outboundPayment, [
    //             'amount' => $outboundPayment->getAppliedAmount()
    //         ]);
    //     });
    //     return $this;
    // }

    // protected function syncExistingOutboundPayments(PurchaseOrder $purchaseOrder)
    // {
    //     // Sync the existing ones (attach and detach)
    //     $existingOutboundPayments = $purchaseOrder->getOutboundPayments()->filter(function ($outboundPayment) {
    //         return $outboundPayment->getId() != null;
    //     })->values();

    //     $outboundPaymentMappings = $this->assembleOutboundPaymentMappings($existingOutboundPayments);

    //     $result = $purchaseOrder->outboundPayments()->sync($outboundPaymentMappings);

    //     $this->outboundPayments->whereIn('outbound_payments.id', $result['detached'])->deleteAll();

    //     return $this;
    // }

    // protected function assembleOutboundPaymentMappings(Collection $outboundPayments)
    // {
    //     return $outboundPayments->reduce(function ($prev, $curr) {
    //         $prev[$curr->getId()] = [
    //             'amount' => $curr->getAppliedAmount(),
    //         ];
    //         return $prev;
    //     }, []);
    // }
}
