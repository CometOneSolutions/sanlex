<?php

namespace App\Modules\PurchaseOrder\Repositories;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use CometOneSolutions\Common\Repositories\Repository;

interface PurchaseOrders extends Repository
{
    public function save(PurchaseOrder $purchaseOrder);
    public function delete(PurchaseOrder $purchaseOrder);
}
