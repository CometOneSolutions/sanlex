<?php

Breadcrumbs::register('purchase-order_index', function ($breadcrumbs) {
    $breadcrumbs->parent('purchasing-menu-dashboard.index');
    $breadcrumbs->push('Purchase Orders', route('purchase-order_index'));
});

Breadcrumbs::register('purchase-order_create', function ($breadcrumbs) {
    $breadcrumbs->parent('purchase-order_index');
    $breadcrumbs->push('Create', route('purchase-order_create'));
});

Breadcrumbs::register('purchase-order.show', function ($breadcrumbs, $purchaseOrder) {
    $breadcrumbs->parent('purchase-order_index');
    $breadcrumbs->push($purchaseOrder->getPONumber(), route('purchase-order_show', $purchaseOrder->getId()));
});

Breadcrumbs::register('purchase-order.edit', function ($breadcrumbs, $purchaseOrder) {
    $breadcrumbs->parent('purchase-order.show', $purchaseOrder);
    $breadcrumbs->push('Edit', route('purchase-order_edit', $purchaseOrder->getId()));
});

Breadcrumbs::register('proForma.edit', function ($breadcrumbs, $purchaseOrder) {
    $breadcrumbs->parent('purchase-order.show', $purchaseOrder);
    $breadcrumbs->push('ProForma Invoice');
});

Breadcrumbs::register('downpayments.edit', function ($breadcrumbs, $purchaseOrder) {
    $breadcrumbs->parent('purchase-order.show', $purchaseOrder);
    $breadcrumbs->push('Downpayments');
});

Breadcrumbs::register('purchase-order.attachment', function ($breadcrumbs, $purchaseOrder) {
    $breadcrumbs->parent('purchase-order.show', $purchaseOrder);
    $breadcrumbs->push('Attachment', route('purchase-order_attachment', $purchaseOrder->getId()));
});
