<?php

Route::group(['prefix' => 'purchasing'], function () {
    Route::group(['prefix' => 'purchase-orders'], function () {
        Route::get('/', 'PurchaseOrderController@index')->name('purchase-order_index');
        Route::get('/pending', 'PurchaseOrderController@indexPending')->name('purchase-order_pending');
        Route::post('{templateType}/generate-template', 'PurchaseOrderController@generate')->name('purchase-order_generate_template');
        Route::get('create', 'PurchaseOrderController@create')->name('purchase-order_create');
        Route::get('{purchaseorder}/edit', 'PurchaseOrderController@edit')->name('purchase-order_edit');
        Route::get('/{purchaseorder}/attachments', 'PurchaseOrderController@attachment')->name('purchase-order_attachment');
        Route::get('{purchaseorder}/pro-forma-invoice', 'PurchaseOrderController@editPfi')->name('proForma_edit');
        Route::get('{purchaseorder}/downpayments', 'PurchaseOrderController@editDownpayments')->name('downpayments_edit');
        Route::get('{purchaseorder}', 'PurchaseOrderController@show')->name('purchase-order_show');
    });
});
