<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'purchase-orders'], function () use ($api) {
        $api->get('/', 'App\Modules\PurchaseOrder\Controllers\PurchaseOrderApiController@index');
        $api->get('{purchaseOrderId}', 'App\Modules\PurchaseOrder\Controllers\PurchaseOrderApiController@show');
        $api->post('/', 'App\Modules\PurchaseOrder\Controllers\PurchaseOrderApiController@store');
        $api->patch('{purchaseOrderId}', 'App\Modules\PurchaseOrder\Controllers\PurchaseOrderApiController@update');
        $api->patch('approve/{purchaseOrderId}', 'App\Modules\PurchaseOrder\Controllers\PurchaseOrderApiController@approve');
        $api->delete('{purchaseOrderId}', 'App\Modules\PurchaseOrder\Controllers\PurchaseOrderApiController@destroy');
        $api->post('{purchaseOrderId}/upload', 'App\Modules\PurchaseOrder\Controllers\PurchaseOrderApiController@import');
    });
});
