<?php

namespace App\Modules\PurchaseOrder;

class PurchaseOrderStatus
{
    const IN_TRANSIT = 'In Transit';
    const RECEIVED = 'Received';
}
