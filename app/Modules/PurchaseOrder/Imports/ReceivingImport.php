<?php

namespace App\Modules\PurchaseOrder\Imports;

use App\Modules\Product\Models\Product;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Receiving\Models\ReceivingModel;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Supplier\Services\SupplierRecordService;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Exceptions\GeneralApiException;
use CometOneSolutions\Common\Utils\Filter;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class ReceivingImport implements WithHeadingRow, ToCollection
{
    protected $purchaseOrder;
    protected $user;
    protected $supplierRecordService;
    protected $productRecordService;
    protected $coils;
    protected $accessories;
    protected $receiving;

    public function __construct(
        PurchaseOrder $purchaseOrder,
        User $user = null
    ) {
        $this->purchaseOrder = $purchaseOrder;
        $this->user = $user;
        $this->supplierRecordService = resolve(SupplierRecordService::class);
        $this->productRecordService = resolve(ProductRecordService::class);
        $this->receiving = resolve(ReceivingModel::class);
        $this->coils = [];
        $this->accessories = [];
    }

    public function collection(Collection $rows)
    {
        $this->purgeReceivings();
        if ($this->purchaseOrder->getProductType() == PurchaseOrder::COIL) {
            if ($this->verifyCoilFieldsFromRows($rows)) {
                $this->persistCoils();
            }
        } elseif ($this->purchaseOrder->getProductType() == PurchaseOrder::ACCESSORY) {
            if ($this->verifyAccessoryFieldsFromRows($rows)) {
                $this->persistAccessories();
            }
        } elseif ($this->purchaseOrder->getProductType() == PurchaseOrder::SS) {
            if ($this->verifyStainlessFieldsFromRows($rows)) {
                $this->persistAccessories();
            }
        } elseif ($this->purchaseOrder->getProductType() == PurchaseOrder::INSULATION) {
            if ($this->verifyInsulationFieldsFromRows($rows)) {
                $this->persistAccessories();
            }
        } elseif ($this->purchaseOrder->getProductType() == PurchaseOrder::SCREW) {
            if ($this->verifyScrewFieldsFromRows($rows)) {
                $this->persistAccessories();
            }
        } elseif ($this->purchaseOrder->getProductType() == PurchaseOrder::SKYLIGHT) {
            if ($this->verifySkylightFieldsFromRows($rows)) {
                $this->persistAccessories();
            }
        } elseif ($this->purchaseOrder->getProductType() == PurchaseOrder::MISC) {
            if ($this->verifyMiscAccessoryFieldsFromRows($rows)) {
                $this->persistAccessories();
            }
        } elseif ($this->purchaseOrder->getProductType() == PurchaseOrder::ST) {
            if ($this->verifyStructuralFieldsFromRows($rows)) {
                $this->persistAccessories();
            }
        } elseif ($this->purchaseOrder->getProductType() == PurchaseOrder::PAINT) {
            if ($this->verifyPaintAdhesiveFieldsFromRows($rows)) {
                $this->persistAccessories();
            }
        } elseif ($this->purchaseOrder->getProductType() == PurchaseOrder::FASTENER) {
            if ($this->verifyFastenerFieldsFromRows($rows)) {
                $this->persistAccessories();
            }
        } elseif ($this->purchaseOrder->getProductType() == PurchaseOrder::PVC) {
            if ($this->verifyPvcFittingFieldsFromRows($rows)) {
                $this->persistAccessories();
            }
        } else {
            throw new GeneralApiException('Product type not supported.');
        }
    }

    private function verifyCoilFieldsFromRows($rows)
    {
        if (!$rows->first()->has('COIL NUMBER')) {
            throw new GeneralApiException('Incorrect heading format.');
        }
        $uniqueCoilNumberCount = $rows->unique('COIL NUMBER')->count();
        $currentVerifiedCount = 0;

        if ($uniqueCoilNumberCount != $rows->count()) {
            $coilArr = [];
            $dupsFound = 0;
            $coilErr = 'Duplicate coil number/s found: ';

            foreach ($rows->all() as $row) {
                if (!in_array($row['COIL NUMBER'], $coilArr)) {
                    $coilArr[] = $row['COIL NUMBER'];
                } else {
                    $coilErr = ($dupsFound == 0) ? $coilErr . $row['COIL NUMBER'] : $coilErr . ', ' . $row['COIL NUMBER'];
                    $dupsFound++;
                }
            }

            throw new GeneralApiException($coilErr);
        }

        $verArr = [];
        $verErr = 'Coil/s not found in master list: ';
        $verCount = 0;

        foreach ($rows->all() as $row) {
            $supplierField = $row['SUPPLIER CODE'];
            $coilNumberField = $row['COIL NUMBER'];
            $coatingField = $row['COATING'];
            $widthField = $row['WIDTH'];
            $thicknessField = $row['THICKNESS'];
            $colorField = $row['COLOR'];
            $netWeightField = $row['NW (MT)'];
            $linearMeterField = $row['LENGTH (LIN MTR)'];

            if ($supplierField == null || $coatingField == null || $widthField == null || $thicknessField == null || $colorField == null || $coilNumberField == null || $netWeightField == null || $linearMeterField == null) {
                throw new GeneralApiException('Blank fields found. Please correct this and reupload.');
            }
            if ($this->receiving->whereCoilNumber($coilNumberField)->count()) {
                throw new GeneralApiException("Coil no. {$coilNumberField} already exists.");
            }
            // if($this->receiving->whereCoilNumber($coilNumberField)->count())

            if ($this->isSupplierProductExists($supplierField, $widthField, $thicknessField, $coatingField, $colorField, null, $netWeightField, $linearMeterField, null, $coilNumberField)) {
                $currentVerifiedCount++;
            } else {
                $name = $this->generateCoilName($this->getSupplier($supplierField), $widthField, $thicknessField, $coatingField, $colorField);
                $verArr[] = $name;
                $verErr = ($verCount == 0) ? $verErr . $name : $verErr . ', ' . $name;
                $verCount++;
            }
        }
        if ($currentVerifiedCount != $rows->count()) {
            throw new GeneralApiException($verErr);
        }
        return true;
    }

    private function isSupplierProductExists($supplierField, $widthField, $thicknessField, $coatingField, $colorField, $descriptionField, $netWeightField, $linearMeterField, $quantityField, $coilNumberField)
    {
        $supplier = $this->getSupplier($supplierField);
        if ($supplier) {
            $supplier = $this->getSupplier($supplierField);
            $productName = ($descriptionField == null)
                ? $this->generateCoilName($supplier, $widthField, $thicknessField, $coatingField, $colorField)
                : $this->generateAccessoryName($supplier, $descriptionField);

            $product = $this->getProduct($productName);

            if ($product) {
                if (!$this->isProductExistsInPurchaseOrder($product)) {
                    throw new GeneralApiException($product->getName() . ' does not exist in this purchase order.');
                }
                if ($descriptionField == null) {
                    $this->coils[] = [
                        'product' => $product,
                        'linear_meter' => $linearMeterField,
                        'net_weight' => $netWeightField,
                        'coil_number' => $coilNumberField
                    ];
                } else {
                    $this->accessories[] = [
                        'product' => $product,
                        'quantity' => $quantityField
                    ];
                }
                return true;
            } else {
                throw new GeneralApiException($productName . ' does not exist in the MASTER LIST.');
            }
        }
        return false;
    }

    private function isSupplierScrewProductExists($supplierField, $typeField, $lengthField, $quantityField)
    {
        $supplier = $this->getSupplier($supplierField);
        if ($supplier) {
            $supplier = $this->getSupplier($supplierField);
            $productName = $this->generateScrewName($supplier, $typeField, $lengthField);

            $product = $this->getProduct($productName);

            if ($product) {
                if (!$this->isProductExistsInPurchaseOrder($product)) {
                    throw new GeneralApiException($product->getName() . ' does not exist in this purchase order.');
                }
                $this->accessories[] = [
                    'product' => $product,
                    'quantity' => $quantityField
                ];
                return true;
            } else {
                throw new GeneralApiException($productName . ' does not exist in the MASTER LIST.');
            }
        }
        return false;
    }

    private function isProductExistsInPurchaseOrder(Product $product)
    {
        return in_array($product->getId(), $this->purchaseOrder->getPurchaseOrderDetails()->pluck('product_id')->toArray());
    }

    private function isSupplierInsulationProductExists($supplierField, $typeField, $densityField, $thicknessField, $lengthField, $widthField, $backingField, $quantityField)
    {
        $supplier = $this->getSupplier($supplierField);
        if ($supplier) {
            $supplier = $this->getSupplier($supplierField);
            $productName = $this->generateInsulationName($supplier, $typeField, $densityField, $thicknessField, $lengthField, $widthField, $backingField);

            $product = $this->getProduct($productName);

            if ($product) {
                if (!$this->isProductExistsInPurchaseOrder($product)) {
                    throw new GeneralApiException($product->getName() . ' does not exist in this purchase order.');
                }
                $this->accessories[] = [
                    'product' => $product,
                    'quantity' => $quantityField
                ];
                return true;
            } else {
                throw new GeneralApiException($productName . ' does not exist in the MASTER LIST.');
            }
        }
        return false;
    }

    private function isSupplierStainlessProductExists($supplierField, $typeField, $thicknessField, $widthField, $lengthField, $finishField, $quantityField)
    {
        $supplier = $this->getSupplier($supplierField);
        if ($supplier) {
            $supplier = $this->getSupplier($supplierField);
            $productName = $this->generateStainlessName($supplier, $typeField, $thicknessField, $widthField, $lengthField, $finishField);

            $product = $this->getProduct($productName);

            if ($product) {
                if (!$this->isProductExistsInPurchaseOrder($product)) {
                    throw new GeneralApiException($product->getName() . ' does not exist in this purchase order.');
                }
                $this->accessories[] = [
                    'product' => $product,
                    'quantity' => $quantityField
                ];
                return true;
            } else {
                throw new GeneralApiException($productName . ' does not exist in the MASTER LIST.');
            }
        }
        return false;
    }

    private function isSupplierStructuralProductExists($supplierField, $typeField, $finishField, $thicknessField, $dimensionXField, $dimensionYField, $lengthField, $quantityField)
    {
        $supplier = $this->getSupplier($supplierField);
        if ($supplier) {
            $supplier = $this->getSupplier($supplierField);
            $productName = $this->generateStructuralName($supplier, $typeField, $finishField, $thicknessField, $dimensionXField, $dimensionYField, $lengthField);

            $product = $this->getProduct($productName);

            if ($product) {
                if (!$this->isProductExistsInPurchaseOrder($product)) {
                    throw new GeneralApiException($product->getName() . ' does not exist in this purchase order.');
                }
                $this->accessories[] = [
                    'product' => $product,
                    'quantity' => $quantityField
                ];
                return true;
            } else {
                throw new GeneralApiException($productName . ' does not exist in the MASTER LIST.');
            }
        }
        return false;
    }

    private function isSupplierPaintAdhesiveProductExists($supplierField, $typeField, $colorField, $sizePerUnitField, $brandField, $quantityField)
    {
        $supplier = $this->getSupplier($supplierField);
        if ($supplier) {
            $supplier = $this->getSupplier($supplierField);
            $productName = $this->generatePaintAdhesiveName($supplier, $typeField, $colorField, $sizePerUnitField, $brandField);

            $product = $this->getProduct($productName);

            if ($product) {
                if (!$this->isProductExistsInPurchaseOrder($product)) {
                    throw new GeneralApiException($product->getName() . ' does not exist in this purchase order.');
                }
                $this->accessories[] = [
                    'product' => $product,
                    'quantity' => $quantityField
                ];
                return true;
            } else {
                throw new GeneralApiException($productName . ' does not exist in the MASTER LIST.');
            }
        }
        return false;
    }

    private function isSupplierPvcFittingProductExists($supplierField, $typeField, $diameterField, $brandField, $quantityField)
    {
        $supplier = $this->getSupplier($supplierField);

        if ($supplier) {
            $supplier = $this->getSupplier($supplierField);
            $productName = $this->generatePvcFittingName($supplier, $typeField, $diameterField, $brandField);

            $product = $this->getProduct($productName);

            if ($product) {
                if (!$this->isProductExistsInPurchaseOrder($product)) {
                    throw new GeneralApiException($product->getName() . ' does not exist in this purchase order.');
                }
                $this->accessories[] = [
                    'product' => $product,
                    'quantity' => $quantityField
                ];
                return true;
            } else {
                throw new GeneralApiException($productName . ' does not exist in the MASTER LIST.');
            }
        }
        return false;
    }

    private function isSupplierFastenerProductExists($supplierField, $typeField, $finishField, $dimensionField, $sizePerUnitField, $quantityField)
    {
        $supplier = $this->getSupplier($supplierField);
        if ($supplier) {
            $supplier = $this->getSupplier($supplierField);
            $productName = $this->generateFastenerName($supplier, $typeField, $finishField, $dimensionField, $sizePerUnitField);

            $product = $this->getProduct($productName);

            if ($product) {
                if (!$this->isProductExistsInPurchaseOrder($product)) {
                    throw new GeneralApiException($product->getName() . ' does not exist in this purchase order.');
                }
                $this->accessories[] = [
                    'product' => $product,
                    'quantity' => $quantityField
                ];
                return true;
            } else {
                throw new GeneralApiException($productName . ' does not exist in the MASTER LIST.');
            }
        }
        return false;
    }

    private function isSupplierSkylightProductExists($supplierField, $panelProfileField, $thicknessField, $widthField, $lengthField, $classField, $quantityField)
    {
        $supplier = $this->getSupplier($supplierField);
        if ($supplier) {
            $supplier = $this->getSupplier($supplierField);
            $productName = $this->generateSkylightName($supplier, $panelProfileField, $thicknessField, $widthField, $lengthField, $classField);

            $product = $this->getProduct($productName);

            if ($product) {
                if (!$this->isProductExistsInPurchaseOrder($product)) {
                    throw new GeneralApiException($product->getName() . ' does not exist in this purchase order.');
                }
                $this->accessories[] = [
                    'product' => $product,
                    'quantity' => $quantityField
                ];
                return true;
            } else {
                throw new GeneralApiException($productName . ' does not exist in the MASTER LIST.');
            }
        }
        return false;
    }

    private function isSupplierMiscAccessoryProductExists($supplierField, $widthField, $lengthField, $backingSideField, $typeField, $quantityField)
    {
        $supplier = $this->getSupplier($supplierField);
        if ($supplier) {
            $supplier = $this->getSupplier($supplierField);
            $productName = $this->generateMiscAccessoryName($supplier, $widthField, $lengthField, $backingSideField, $typeField);

            $product = $this->getProduct($productName);

            if ($product) {
                if (!$this->isProductExistsInPurchaseOrder($product)) {
                    throw new GeneralApiException($product->getName() . ' does not exist in this purchase order.');
                }
                $this->accessories[] = [
                    'product' => $product,
                    'quantity' => $quantityField
                ];
                return true;
            } else {
                throw new GeneralApiException($productName . ' does not exist in the MASTER LIST.');
            }
        }
        return false;
    }

    private function verifyAccessoryFieldsFromRows($rows)
    {
        if (!$rows->first()->has('DESCRIPTION')) {
            throw new GeneralApiException('Incorrect heading format.');
        }
        $uniqueDescriptionCount = $rows->unique('DESCRIPTION')->count();

        if ($uniqueDescriptionCount != $rows->count()) {
            throw new GeneralApiException('There are duplicate accessory entries.');
        }

        $currentVerifiedCount = 0;

        foreach ($rows->all() as $row) {
            $supplierField = $row['SUPPLIER CODE'];
            $descriptionField = $row['DESCRIPTION'];
            $quantityField = $row['QUANTITY (IN PCS)'];

            if ($supplierField == null || $descriptionField == null || $quantityField == null) {
                throw new GeneralApiException('Blank fields found. Please correct this and reupload.');
            }

            if ($this->isSupplierProductExists($supplierField, null, null, null, null, $descriptionField, null, null, $quantityField, null)) {
                $currentVerifiedCount++;
            }
        }
        if ($currentVerifiedCount != $rows->count()) {
            throw new GeneralApiException('There is an accessory entry mismatch.');
        }
        return true;
    }

    private function verifySkylightFieldsFromRows($rows)
    {
        if (
            !$rows->first()->has('SUPPLIER CODE') ||
            !$rows->first()->has('PANEL PROFILE') ||
            !$rows->first()->has('THICKNESS') ||
            !$rows->first()->has('WIDTH') ||
            !$rows->first()->has('LENGTH') ||
            !$rows->first()->has('CLASS') ||
            !$rows->first()->has('QUANTITY (IN PCS)')
        ) {
            throw new GeneralApiException('Incorrect heading format.');
        }
        $details = [];
        $dupsFound = 0;
        $err = '';

        foreach ($rows->all() as $index => $row) {
            $combination = $row['SUPPLIER CODE'] . '~' . $row['PANEL PROFILE'] . '~' . $row['THICKNESS'] . '~' . $row['WIDTH'] . '~' . $row['LENGTH'] . '~' . $row['CLASS'];
            if (!in_array($combination, $details)) {
                $details[] = $combination;
            } else {
                $err = ($dupsFound == 0) ? $err . 'Row ' . ($index + 2) : $err . ', ' . 'Row ' . ($index + 2);
                $dupsFound++;
            }
        }

        if (count($details) != $rows->count()) {
            throw new GeneralApiException('There are duplicate skylight entries. In row: ' . $err);
        }

        $currentVerifiedCount = 0;

        foreach ($rows->all() as $row) {
            $supplierField = $row['SUPPLIER CODE'];
            $panelProfileField = $row['PANEL PROFILE'];
            $thicknessField = $row['THICKNESS'];
            $widthField = $row['WIDTH'];
            $lengthField = $row['LENGTH'];
            $classField = $row['CLASS'];
            $quantityField = $row['QUANTITY (IN PCS)'];

            if ($supplierField == null || $classField == null || $thicknessField == null || $widthField == null || $quantityField == null || $lengthField == null) {
                throw new GeneralApiException('Blank fields found. Please correct this and reupload.');
            }

            if ($this->isSupplierSkylightProductExists($supplierField, $panelProfileField, $thicknessField, $widthField, $lengthField, $classField, $quantityField)) {
                $currentVerifiedCount++;
            }
        }
        if ($currentVerifiedCount != $rows->count()) {
            throw new GeneralApiException('There is an skylight entry mismatch.');
        }
        return true;
    }

    private function verifyMiscAccessoryFieldsFromRows($rows)
    {
        if (
            !$rows->first()->has('SUPPLIER CODE') ||
            !$rows->first()->has('WIDTH') ||
            !$rows->first()->has('LENGTH') ||
            !$rows->first()->has('BACKING SIDE') ||
            !$rows->first()->has('TYPE') ||
            !$rows->first()->has('QUANTITY (IN PCS)')
        ) {
            throw new GeneralApiException('Incorrect heading format.');
        }
        $details = [];
        $dupsFound = 0;
        $err = '';

        foreach ($rows->all() as $index => $row) {
            $combination = $row['SUPPLIER CODE'] . '~' . $row['WIDTH'] . '~' . $row['LENGTH'] . '~' . $row['BACKING SIDE'] . '~' . $row['TYPE'];
            if (!in_array($combination, $details)) {
                $details[] = $combination;
            } else {
                $err = ($dupsFound == 0) ? $err . 'Row ' . ($index + 2) : $err . ', ' . 'Row ' . ($index + 2);
                $dupsFound++;
            }
        }

        if (count($details) != $rows->count()) {
            throw new GeneralApiException('There are duplicate misc accessory entries. In row: ' . $err);
        }

        $currentVerifiedCount = 0;

        foreach ($rows->all() as $row) {
            $supplierField = $row['SUPPLIER CODE'];
            $widthField = $row['WIDTH'];
            $lengthField = $row['LENGTH'];
            $backingSideField = $row['BACKING SIDE'];
            $typeField = $row['TYPE'];
            $quantityField = $row['QUANTITY (IN PCS)'];

            if ($supplierField == null || $typeField == null || $backingSideField == null || $widthField == null || $quantityField == null || $lengthField == null) {
                throw new GeneralApiException('Blank fields found. Please correct this and reupload.');
            }

            if ($this->isSupplierMiscAccessoryProductExists($supplierField, $widthField, $lengthField, $backingSideField, $typeField, $quantityField)) {
                $currentVerifiedCount++;
            }
        }
        if ($currentVerifiedCount != $rows->count()) {
            throw new GeneralApiException('There is a misc accessory entry mismatch.');
        }
        return true;
    }

    private function verifyStainlessFieldsFromRows($rows)
    {
        if (
            !$rows->first()->has('SUPPLIER CODE') ||
            !$rows->first()->has('TYPE') ||
            !$rows->first()->has('THICKNESS') ||
            !$rows->first()->has('WIDTH') ||
            !$rows->first()->has('LENGTH') ||
            !$rows->first()->has('FINISH') ||
            !$rows->first()->has('QUANTITY (IN PCS)')
        ) {
            throw new GeneralApiException('Incorrect heading format.');
        }
        $details = [];
        $dupsFound = 0;
        $err = '';

        foreach ($rows->all() as $index => $row) {
            $combination = $row['SUPPLIER CODE'] . '~' . $row['TYPE'] . '~' . $row['THICKNESS'] . '~' . $row['WIDTH'] . '~' . $row['LENGTH'] . '~' . $row['FINISH'];
            if (!in_array($combination, $details)) {
                $details[] = $combination;
            } else {
                $err = ($dupsFound == 0) ? $err . 'Row ' . ($index + 2) : $err . ', ' . 'Row ' . ($index + 2);
                $dupsFound++;
            }
        }

        if (count($details) != $rows->count()) {
            throw new GeneralApiException('There are duplicate stainless steel entries. In row: ' . $err);
        }

        $currentVerifiedCount = 0;

        foreach ($rows->all() as $row) {
            $supplierField = $row['SUPPLIER CODE'];
            $typeField = $row['TYPE'];
            $thicknessField = $row['THICKNESS'];
            $widthField = $row['WIDTH'];
            $lengthField = $row['LENGTH'];
            $finishField = $row['FINISH'];
            $quantityField = $row['QUANTITY (IN PCS)'];

            if ($supplierField == null || $typeField == null || $thicknessField == null || $widthField == null || $quantityField == null || $lengthField == null || $finishField == null) {
                throw new GeneralApiException('Blank fields found. Please correct this and reupload.');
            }

            if ($this->isSupplierStainlessProductExists($supplierField, $typeField, $thicknessField, $widthField, $lengthField, $finishField, $quantityField)) {
                $currentVerifiedCount++;
            }
        }
        if ($currentVerifiedCount != $rows->count()) {
            throw new GeneralApiException('There is an stainless steel entry mismatch.');
        }
        return true;
    }

    private function verifyStructuralFieldsFromRows($rows)
    {
        if (
            !$rows->first()->has('SUPPLIER CODE') ||
            !$rows->first()->has('TYPE') ||
            !$rows->first()->has('THICKNESS') ||
            !$rows->first()->has('DIMENSION X') ||
            !$rows->first()->has('DIMENSION Y') ||
            !$rows->first()->has('LENGTH') ||
            !$rows->first()->has('FINISH') ||
            !$rows->first()->has('QUANTITY (IN PCS)')
        ) {
            throw new GeneralApiException('Incorrect heading format.');
        }
        $details = [];
        $dupsFound = 0;
        $err = '';

        foreach ($rows->all() as $index => $row) {
            $combination = $row['SUPPLIER CODE'] . '~' . $row['TYPE'] . '~' . $row['FINISH'] . '~' . $row['THICKNESS'] . '~' . $row['DIMENSION X'] . '~' . $row['DIMENSION Y'] . '~' . $row['LENGTH'];
            if (!in_array($combination, $details)) {
                $details[] = $combination;
            } else {
                $err = ($dupsFound == 0) ? $err . 'Row ' . ($index + 2) : $err . ', ' . 'Row ' . ($index + 2);
                $dupsFound++;
            }
        }

        if (count($details) != $rows->count()) {
            throw new GeneralApiException('There are duplicate structural steel entries. In row: ' . $err);
        }

        $currentVerifiedCount = 0;

        foreach ($rows->all() as $row) {
            $supplierField = $row['SUPPLIER CODE'];
            $typeField = $row['TYPE'];
            $thicknessField = $row['THICKNESS'];
            $dimensionXField = $row['DIMENSION X'];
            $dimensionYField = $row['DIMENSION Y'];
            $lengthField = $row['LENGTH'];
            $finishField = $row['FINISH'];
            $quantityField = $row['QUANTITY (IN PCS)'];

            if ($supplierField == null || $typeField == null || $thicknessField == null || $dimensionXField == null || $dimensionYField == null || $quantityField == null || $lengthField == null || $finishField == null) {
                throw new GeneralApiException('Blank fields found. Please correct this and reupload.');
            }

            if ($this->isSupplierStructuralProductExists($supplierField, $typeField, $finishField, $thicknessField, $dimensionXField, $dimensionYField, $lengthField, $quantityField)) {
                $currentVerifiedCount++;
            }
        }
        if ($currentVerifiedCount != $rows->count()) {
            throw new GeneralApiException('There is an structural steel entry mismatch.');
        }
        return true;
    }

    private function verifyPaintAdhesiveFieldsFromRows($rows)
    {
        if (
            !$rows->first()->has('SUPPLIER CODE') ||
            !$rows->first()->has('PAINT ADHESIVE TYPE') ||
            !$rows->first()->has('BRAND') ||
            !$rows->first()->has('COLOR') ||
            !$rows->first()->has('SIZE PER UNIT') ||
            !$rows->first()->has('QUANTITY (IN PCS)')
        ) {
            throw new GeneralApiException('Incorrect heading format.');
        }
        $details = [];
        $dupsFound = 0;
        $err = '';

        foreach ($rows->all() as $index => $row) {
            $combination = $row['SUPPLIER CODE'] . '~' . $row['PAINT ADHESIVE TYPE'] . '~' . $row['COLOR'] . '~' . $row['SIZE PER UNIT'] . '~' . $row['BRAND'];
            if (!in_array($combination, $details)) {
                $details[] = $combination;
            } else {
                $err = ($dupsFound == 0) ? $err . 'Row ' . ($index + 2) : $err . ', ' . 'Row ' . ($index + 2);
                $dupsFound++;
            }
        }

        if (count($details) != $rows->count()) {
            throw new GeneralApiException('There are duplicate paint adhesive entries. In row: ' . $err);
        }

        $currentVerifiedCount = 0;

        foreach ($rows->all() as $row) {
            $supplierField = $row['SUPPLIER CODE'];
            $typeField = $row['PAINT ADHESIVE TYPE'];
            $brandField = $row['BRAND'];
            $colorField = $row['COLOR'];
            $sizePerUnitField = $row['SIZE PER UNIT'];

            $quantityField = $row['QUANTITY (IN PCS)'];

            if ($supplierField == null || $typeField == null || $brandField == null || $colorField == null || $sizePerUnitField == null) {
                throw new GeneralApiException('Blank fields found. Please correct this and reupload.');
            }

            if ($this->isSupplierPaintAdhesiveProductExists($supplierField, $typeField, $colorField, $sizePerUnitField, $brandField, $quantityField)) {
                $currentVerifiedCount++;
            }
        }
        if ($currentVerifiedCount != $rows->count()) {
            throw new GeneralApiException('There is a paint adhesive entry mismatch.');
        }
        return true;
    }

    private function verifyPvcFittingFieldsFromRows($rows)
    {
        if (
            !$rows->first()->has('SUPPLIER CODE') ||
            !$rows->first()->has('TYPE') ||
            !$rows->first()->has('DIAMETER') ||
            !$rows->first()->has('BRAND') ||
            !$rows->first()->has('QUANTITY (IN PCS)')
        ) {
            throw new GeneralApiException('Incorrect heading format.');
        }
        $details = [];
        $dupsFound = 0;
        $err = '';

        foreach ($rows->all() as $index => $row) {
            $combination = $row['SUPPLIER CODE'] . '~' . $row['TYPE'] . '~' . $row['DIAMETER'] . '~' . $row['BRAND'];
            if (!in_array($combination, $details)) {
                $details[] = $combination;
            } else {
                $err = ($dupsFound == 0) ? $err . 'Row ' . ($index + 2) : $err . ', ' . 'Row ' . ($index + 2);
                $dupsFound++;
            }
        }

        if (count($details) != $rows->count()) {
            throw new GeneralApiException('There are duplicate pvc fitting entries. In row: ' . $err);
        }

        $currentVerifiedCount = 0;

        foreach ($rows->all() as $row) {
            $supplierField = $row['SUPPLIER CODE'];
            $typeField = $row['TYPE'];
            $diameterField = $row['DIAMETER'];
            $brandField = $row['BRAND'];

            $quantityField = $row['QUANTITY (IN PCS)'];

            if ($supplierField == null || $typeField == null || $diameterField == null || $brandField == null) {
                throw new GeneralApiException('Blank fields found. Please correct this and reupload.');
            }

            if ($this->isSupplierPvcFittingProductExists($supplierField, $typeField, $diameterField, $brandField, $quantityField)) {
                $currentVerifiedCount++;
            }
        }

        if ($currentVerifiedCount != $rows->count()) {
            throw new GeneralApiException('There is a pvc fitting entry mismatch.');
        }
        return true;
    }

    private function verifyFastenerFieldsFromRows($rows)
    {
        if (
            !$rows->first()->has('SUPPLIER CODE') ||
            !$rows->first()->has('FASTENER TYPE') ||
            !$rows->first()->has('FINISH') ||
            !$rows->first()->has('FASTENER DIMENSION') ||
            !$rows->first()->has('SIZE PER UNIT') ||
            !$rows->first()->has('QUANTITY (IN PCS)')
        ) {
            throw new GeneralApiException('Incorrect heading format.');
        }
        $details = [];
        $dupsFound = 0;
        $err = '';

        foreach ($rows->all() as $index => $row) {
            $combination = $row['SUPPLIER CODE'] . '~' . $row['FASTENER TYPE'] . '~' . $row['FINISH'] . '~' . $row['FASTENER DIMENSION'] . '~' . $row['SIZE PER UNIT'];
            if (!in_array($combination, $details)) {
                $details[] = $combination;
            } else {
                $err = ($dupsFound == 0) ? $err . 'Row ' . ($index + 2) : $err . ', ' . 'Row ' . ($index + 2);
                $dupsFound++;
            }
        }

        if (count($details) != $rows->count()) {
            throw new GeneralApiException('There are duplicate fastener entries. In row: ' . $err);
        }

        $currentVerifiedCount = 0;

        foreach ($rows->all() as $row) {
            $supplierField = $row['SUPPLIER CODE'];
            $typeField = $row['FASTENER TYPE'];
            $dimensionField = $row['FASTENER DIMENSION'];
            $finishField = $row['FINISH'];

            $sizePerUnitField = $row['SIZE PER UNIT'];

            $quantityField = $row['QUANTITY (IN PCS)'];

            if ($supplierField == null || $typeField == null || $dimensionField == null || $finishField == null || $sizePerUnitField == null) {
                throw new GeneralApiException('Blank fields found. Please correct this and reupload.');
            }

            if ($this->isSupplierFastenerProductExists($supplierField, $typeField, $dimensionField, $finishField, $sizePerUnitField, $quantityField)) {
                $currentVerifiedCount++;
            }
        }
        if ($currentVerifiedCount != $rows->count()) {
            throw new GeneralApiException('There is a fastener entry mismatch.');
        }
        return true;
    }

    private function verifyScrewFieldsFromRows($rows)
    {
        if (
            !$rows->first()->has('SUPPLIER CODE') ||
            !$rows->first()->has('TYPE') ||
            !$rows->first()->has('LENGTH') ||
            !$rows->first()->has('QUANTITY (IN PCS)')
        ) {
            throw new GeneralApiException('Incorrect heading format.');
        }
        $details = [];
        $dupsFound = 0;
        $err = '';

        foreach ($rows->all() as $index => $row) {
            $combination = $row['SUPPLIER CODE'] . '~' . $row['TYPE'] . '~' . $row['LENGTH'];
            if (!in_array($combination, $details)) {
                $details[] = $combination;
            } else {
                $err = ($dupsFound == 0) ? $err . 'Row ' . ($index + 2) : $err . ', ' . 'Row ' . ($index + 2);
                $dupsFound++;
            }
        }

        if (count($details) != $rows->count()) {
            throw new GeneralApiException('There are duplicate screw entries. In row: ' . $err);
        }

        $currentVerifiedCount = 0;

        foreach ($rows->all() as $row) {
            $supplierField = $row['SUPPLIER CODE'];
            $typeField = $row['TYPE'];
            $lengthField = $row['LENGTH'];
            $quantityField = $row['QUANTITY (IN PCS)'];

            if ($supplierField == null || $typeField == null || $quantityField == null || $lengthField == null) {
                throw new GeneralApiException('Blank fields found. Please correct this and reupload.');
            }

            if ($this->isSupplierScrewProductExists($supplierField, $typeField, $lengthField, $quantityField)) {
                $currentVerifiedCount++;
            }
        }
        if ($currentVerifiedCount != $rows->count()) {
            throw new GeneralApiException('There is an stainless steel entry mismatch.');
        }
        return true;
    }

    private function verifyInsulationFieldsFromRows($rows)
    {
        if (
            !$rows->first()->has('SUPPLIER CODE') ||
            !$rows->first()->has('TYPE') ||
            !$rows->first()->has('DENSITY') ||
            !$rows->first()->has('THICKNESS') ||
            !$rows->first()->has('LENGTH') ||
            !$rows->first()->has('WIDTH') ||
            !$rows->first()->has('BACKING') ||
            !$rows->first()->has('QUANTITY (IN PCS)')
        ) {
            throw new GeneralApiException('Incorrect heading format.');
        }
        $details = [];
        $dupsFound = 0;
        $err = '';

        foreach ($rows->all() as $index => $row) {
            $combination = $row['SUPPLIER CODE'] . '~' . $row['TYPE'] . '~' . $row['DENSITY'] . '~' . $row['THICKNESS'] . '~' . $row['LENGTH'] . '~' . $row['WIDTH'] . '~' . $row['BACKING'];
            if (!in_array($combination, $details)) {
                $details[] = $combination;
            } else {
                $err = ($dupsFound == 0) ? $err . 'Row ' . ($index + 2) : $err . ', ' . 'Row ' . ($index + 2);
                $dupsFound++;
            }
        }

        if (count($details) != $rows->count()) {
            throw new GeneralApiException('There are duplicate insulation entries. In row: ' . $err);
        }

        $currentVerifiedCount = 0;

        foreach ($rows->all() as $row) {
            $supplierField = $row['SUPPLIER CODE'];
            $typeField = $row['TYPE'];
            $densityField = $row['DENSITY'];
            $thicknessField = $row['THICKNESS'];
            $lengthField = $row['LENGTH'];
            $widthField = $row['WIDTH'];
            $backingField = $row['BACKING'];
            $quantityField = $row['QUANTITY (IN PCS)'];

            if ($supplierField == null || $typeField == null || $thicknessField == null || $backingField == null || $quantityField == null || $lengthField == null || $widthField == null || $densityField == null) {
                throw new GeneralApiException('Blank fields found. Please correct this and reupload.');
            }

            if ($this->isSupplierInsulationProductExists($supplierField, $typeField, $densityField, $thicknessField, $lengthField, $widthField, $backingField, $quantityField)) {
                $currentVerifiedCount++;
            }
        }
        if ($currentVerifiedCount != $rows->count()) {
            throw new GeneralApiException('There is an insulation entry mismatch.');
        }
        return true;
    }

    private function getSupplier($supplierCodeField)
    {
        $shortCodeFilter = new Filter('short_code', $supplierCodeField);
        return $this->supplierRecordService->getFirst([$shortCodeFilter]);
    }

    private function getProduct($productName)
    {
        $nameFilter = new Filter('name', $productName);
        return $this->productRecordService->getFirst([$nameFilter]);
    }

    private function persistCoils()
    {
        $data = [];
        foreach ($this->coils as $coil) {
            $receiving = new ReceivingModel();

            $receiving->setProduct($coil['product']);
            $receiving->setPurchaseOrder($this->purchaseOrder);
            $receiving->setUpdatedByUser($this->user);
            $receiving->setLinearMeter($coil['linear_meter']);
            $receiving->setNetWeight($coil['net_weight']);
            $receiving->setCoilNumber($coil['coil_number']);
            $receiving->setQuantity($coil['net_weight']);
            $data[] = $receiving;
        }
        $this->purchaseOrder->receivings()->saveMany($data);
    }

    private function persistAccessories()
    {
        $data = [];
        foreach ($this->accessories as $accessory) {
            $receiving = new ReceivingModel();
            $receiving->setProduct($accessory['product']);
            $receiving->setQuantity($accessory['quantity']);
            $receiving->setUpdatedByUser($this->user);
            $data[] = $receiving;
        }
        $this->purchaseOrder->receivings()->saveMany($data);
    }

    // end of getUploadedFileDetailsAndSaveAsAccessoryToReceivings()

    private function purgeReceivings()
    {
        if ($this->purchaseOrder->getReceivings()) {
            $this->purchaseOrder->receivings()->delete();
        }
    }

    private function generateCoilName(Supplier $supplier, $widthField, $thicknessField, $coatingField, $colorField)
    {
        $format = '%s~%s~%s~%s~%s';
        return sprintf($format, $supplier->getShortCode(), $coatingField, $thicknessField, $widthField, $colorField);
    }

    private function generateAccessoryName(Supplier $supplier, $descriptionField)
    {
        $format = '%s~%s';
        return sprintf($format, $supplier->getShortCode(), $descriptionField);
    }

    private function generateStainlessName(Supplier $supplier, $typeField, $thicknessField, $widthField, $lengthField, $finishField)
    {
        $format = '%s~%s~%s~%s~%s~%s';
        return sprintf($format, $supplier->getShortCode(), $typeField, $thicknessField, $widthField, $lengthField, $finishField);
    }

    private function generateStructuralName(Supplier $supplier, $typeField, $finishField, $thicknessField, $dimensionXField, $dimensionYField, $lengthField)
    {
        $format = '%s~%s~%s~%s~%s~%s~%s';
        return sprintf($format, $supplier->getShortCode(), $typeField, $finishField, $thicknessField, $dimensionXField, $dimensionYField, $lengthField);
    }

    private function generatePaintAdhesiveName(Supplier $supplier, $typeField, $colorField, $sizePerUnitField, $brandField)
    {
        $format = '%s~%s~%s~%s~%s';
        return sprintf($format, $supplier->getShortCode(), $typeField, $colorField, $sizePerUnitField, $brandField);
    }

    private function generatePvcFittingName(Supplier $supplier, $typeField, $diameterField, $brandField)
    {
        $format = '%s~%s~%s~%s';
        return sprintf($format, $supplier->getShortCode(), $typeField, $diameterField, $brandField);
    }

    private function generateFastenerName(Supplier $supplier, $typeField, $dimensionField, $finishField, $sizePerUnitField)
    {
        $format = '%s~%s~%s~%s~%s';
        return sprintf($format, $supplier->getShortCode(), $typeField, $finishField, $dimensionField, $sizePerUnitField);
    }

    private function generateInsulationName(Supplier $supplier, $typeField, $densityField, $thicknessField, $lengthField, $widthField, $backingField)
    {
        $format = '%s~%s~%s~%s~%s~%s~%s';
        return sprintf($format, $supplier->getShortCode(), $typeField, $densityField, $thicknessField, $lengthField, $widthField, $backingField);
    }

    private function generateScrewName(Supplier $supplier, $typeField, $lengthField)
    {
        $format = '%s~%s~%s';
        return sprintf($format, $supplier->getShortCode(), $typeField, $lengthField);
    }

    private function generateSkylightName(Supplier $supplier, $typeField, $thicknessField, $widthField, $lengthField, $classField)
    {
        $format = '%s~%s~%s~%s~%s~%s~%s';
        return sprintf($format, $supplier->getShortCode(), 'SKYLIGHT', $typeField, $thicknessField, $widthField, $lengthField, $classField);
    }

    private function generateMiscAccessoryName(Supplier $supplier, $widthField, $lengthField, $backingSideField, $typeField)
    {
        $format = '%s~%s~%s~%s~%s';
        return sprintf($format, $supplier->getShortCode(), $widthField, $lengthField, $backingSideField, $typeField);
    }
}
