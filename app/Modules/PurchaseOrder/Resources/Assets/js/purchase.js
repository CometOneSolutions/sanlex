import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm';
import Select2 from '@c1_common_js/components/Select2';
import Select3 from "@c1_common_js/components/Select3";
import PurchaseDetail from '@c1_module_js/PurchaseOrderDetail/Resources/Assets/js/PurchaseDetail'
import Datepicker from 'vuejs-datepicker';

import { productService } from "@c1_module_js/Product/Resources/Assets/js/product-main";
import { supplierService } from "@c1_module_js/Supplier/Resources/Assets/js/supplier-main";

const COIL = 'Coils';
const ACCESSORY = 'Accessories';
const SKYLIGHT = 'Skylights';
const INSULATION = 'Insulations';
const SCREW = 'Screws';
const SS = 'Stainless Steels';
const MISC = 'Misc Accessories';
const ST = 'Structural Steels';
const PAINT = 'Paint Adhesives';
const FASTENER = 'Fasteners';
const PVC = 'Pvc Fittings';

new Vue({
    el: '#purchase',
    components: {
        SaveButton,
        DeleteButton,
        Timestamp,
        VForm,
        PurchaseDetail,
        Select2,
        Select3,
        productService,
        supplierService,
        Datepicker
    },

    data: {
        defaultSelectedSupplier: {},
        form: new Form({
            id: null,
            purchaseNumber: null,
            supplierId: '',
            date: moment(),
            remarks: null,
            status: 'In Transit',
            productType: 'Coils',
            purchaseOrderDetails: {
                data: [
                    // {
                    //     id: null,
                    //     productId: null,
                    //     orderedQuantity: null,
                    //     uomId: null
                    // }
                ]
            },
            // createdAt: null


        }),
        counter: 0,
        isSaving: false,
        isBusy: false,
        url: '/api/suppliers?sort=name',
        dataInitialized: true,
        products: [],
        productsInitialized: false,
        suppliers: [],
        suppliersInitialized: false,
        editMode: false


    },

    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },

    computed: {

        initializationComplete() {
            return this.dataInitialized;
        },
    },

    methods: {

        loadFilteredProduct() {
            if (!this.editMode) {
                this.form.purchaseOrderDetails.data = [];
            }

            if (this.form.productType && this.form.supplierId) {
                if (this.form.productType == '' || this.form.supplierId == '') {
                    return undefined;
                }
                if (this.form.productType == COIL) {
                    this.products = productService.getCoilOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == ACCESSORY) {
                    this.products = productService.getAccessoryOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == SS) {
                    this.products = productService.getStainlessSteelOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == SKYLIGHT) {
                    this.products = productService.getSkylightOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == INSULATION) {
                    this.products = productService.getInsulationOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == SCREW) {
                    this.products = productService.getScrewOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == MISC) {
                    this.products = productService.getMiscAccessoryOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == ST) {
                    this.products = productService.getStructuralSteelOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == PAINT) {
                    this.products = productService.getPaintAdhesiveOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == FASTENER) {
                    this.products = productService.getFastenerOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
                else if (this.form.productType == PVC) {
                    this.products = productService.getPvcFittingOnlyProducts(this.form.supplierId).then(response => {
                        this.products = response.data;
                        this.productsInitialized = true;
                    });
                }
            }
        },
        add() {
            this.form.purchaseOrderDetails.data.push({
                id: --this.counter,
                productId: null,
                quantity: '',
            });
        },
        destroy() {
            this.form.confirm().then((result) => {
                if (result.value) {

                    this.form.delete('/api/purchase-orders/' + this.form.id).then(response => {
                        this.isDeleting = false;
                        this.isBusyDeleting = false;
                        this.$swal({
                            title: 'Success',
                            text: 'Purchase order was successfully deleted.',
                            type: 'success'
                        }).then(() => window.location = '/purchasing/purchase-orders');

                    })
                }
            });
        },

        store() {
            let ids = [];
            this.form.purchaseOrderDetails.data.forEach(detail => {
                ids.push(detail.productId);
            });

            let distinctIds = [...new Set(ids)];

            if (distinctIds.length != ids.length) {
                this.$swal({
                    title: 'Error',
                    text: 'Duplicate products found.',
                    type: 'error'
                });
            }

            this.form.post('/api/purchase-orders').then(response => {
                this.$swal({
                    title: 'Success',
                    text: 'Purchase order was saved.',
                    type: 'success'
                });
                this.form.reset();
            });
        },

        approve() {
            this.form.affirm().then((result) => {
                if (result.value) {
                    this.form.patch('/api/purchase-orders/approve/' + id).then(
                        response => {
                            this.$swal({
                                title: 'Success',
                                text: 'Purchase Order approved.',
                                type: 'success'
                            }).then(() => window.location = '/purchasing/purchase-orders/' + id);
                        })
                }
            });
        },

        update() {

            this.form.patch('/api/purchase-orders/' + this.form.id).then(response => {
                this.isSaving = false;
                this.isBusy = false;
                this.form.updated_by = response.data.updated_by;
                this.form.updated_at = response.data.updated_at;
                this.$swal({
                    title: 'Success',
                    text: 'Purchase order was successfully updated.',
                    type: 'success'
                }).then(() => window.location = '/purchasing/purchase-orders/' + response.data.id);

            }).catch(error => {
                this.isSaving = false;
                this.isBusy = false;
            });
        },

        handleFileChange(e) {
            this.form.file = this.$refs.file.files[0];
        },

        addFile() {
            let formData = new FormData();
            formData.append('file', this.form.file);
            formData.append('userId', this.form.updatedByUser.data.id);
            this.isSaving = true;
            this.isBusy = true;
            this.form.postImage('/api/purchase-orders/' + this.form.id + '/upload', formData)
                .then(response => {
                    console.log(response);
                    this.isSaving = false;
                    this.isBusy = false;
                    this.$swal({
                        title: 'Success',
                        text: 'File was successfully uploaded.',
                        type: 'success'
                    }).then(() => {
                        window.location = '/purchasing/purchase-orders/' + this.form.id;
                    });


                }).catch(error => {
                    console.log(error);
                    this.isSaving = false;
                    this.isBusy = false;
                });

        },

        removeFile() {

            document.getElementById("uploadFile").value = "";
        },

        loadData(data) {
            this.form = new Form(data);

        },


    },
    created() {
        if (productType != null && supplierId != null) {
            this.form.supplierId = supplierId;
            this.form.productType = productType;
            this.loadFilteredProduct();
        }

        if (id != null) {
            this.dataInitialized = false;
            // Fetch supplier
            this.form.isInitializing = true;
            this.editMode = true;
            this.form.get('/api/purchase-orders/' + id + '?include=supplier,purchaseOrderDetails,receivings')
                .then(response => {
                    this.loadData(response.data);
                    this.defaultSelectedSupplier = {
                        text: response.data.supplier.data.name,
                        id: response.data.supplierId,
                    };
                    this.dataInitialized = true;
                });
        }

    },

    mounted() {
        console.log("Init purchase script...");
    }
});