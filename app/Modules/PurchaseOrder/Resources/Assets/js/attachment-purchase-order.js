import { Form } from '@c1_common_js/components/Form';
import { serviceBus } from '@c1_common_js/main';
import { purchaseOrderService } from '@c1_module_js/PurchaseOrder/Resources/Assets/js/purchase-order-main';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'


new Vue({
    el: '#thumbnail',
    components: {
        SaveButton, DeleteButton, Timestamp, serviceBus, purchaseOrderService
    },
    data: {
        form: new Form({
            id: null,
            file: null
        }),
        dataInitialized: true,
        images: [],
        imageInitialized: false,

    },

    watch: {
        initializationComplete(val) {

            this.form.isInitializing = false;
        },



    },

    computed: {

        initializationComplete() {
            if (this.dataInitialized) {
                this.form.isInitializing = false;
                return true;
            }
            return false;
        }




    },
    methods: {

        handleFileChange(e) {
            this.form.file = this.$refs.file.files[0];

        },
        store() {

            let formData = new FormData();
            formData.append('file', this.form.file);

            // this.form.postImage(serviceBus.uri.getItems+'/'+this.form.id+'/images', formData)
            this.form.postImage(purchaseOrderService.uri.base + '/' + this.form.id + '/attachments', formData)
                .then(response => {
                    this.$swal({
                        title: 'Success',
                        text: 'File was saved.',
                        type: 'success'
                    }).then(() => {
                        this.imageInitialized = false;
                        this.reloadImages(this.form.id);
                    });
                    this.form.file = null;

                })
                .catch(error => {
                    serviceBus.errorMessage(error);
                    this.imageInitialized = true;
                });

        },

        destroy(image) {
            this.form.confirm("Delete " + image.fileName + "?").then((result) => {
                if (result.value) {
                    this.form.delete(purchaseOrderService.uri.base + '/' + this.form.id + '/attachments/' + image.id)
                    // this.form.delete(serviceBus.uri.getItems + '/' + this.form.id + '/images/' + image.id)
                        .then(response => {

                            this.$swal({
                                title: 'Success',
                                text: image.fileName.toUpperCase() + ' was removed.',
                                type: 'success'
                            }).then(() => this.reloadImages(this.form.id));
                        })
                        .catch(error => {
                            serviceBus.errorMessage(error);
                        });
                }
            });
        },
        reloadImages(id) {
            purchaseOrderService.getPurchaseOrderWithAttachmentsById(id).then(data => {
                this.loadData(data);
                this.images = data.attachments.data;
                this.imageInitialized = true;
                // this.images = data;
                // this.imageInitialized = true;
            });
        },

        loadData(data) {
            this.form = new Form(data);
        },


    },
    created() {

        if (id != null) {
            this.dataInitialized = false;
            this.form.id = id;
            this.reloadImages(id);
            this.dataInitialized = true;

        }

    },
    mounted() {

        console.log("Init purchase order attachment script...");






    }
});