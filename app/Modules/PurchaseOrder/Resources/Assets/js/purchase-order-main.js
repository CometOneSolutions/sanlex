import {Form} from '@c1_common_js/components/Form';

export const purchaseOrderService = new Vue({
       
    data: function() {
        return {
            form: new Form({}),
            uri: {
                purchaseOrders: '/api/purchase-orders?limit=' + Number.MAX_SAFE_INTEGER,
                base: '/api/purchase-orders'
            }      
        }
    },   

    methods: {

        getPurchaseOrders(){
            return this.form.get(this.uri.purchaseOrders);
        },
        getProForma() {
            let url = this.uri.purchaseOrders + '&proforma_no!=null&has-no-packing-list=1';
            return this.form.get(url);
        },
        getPurchaseOrderWithAttachmentsById(purchaseOrderId) {
            let url =
                this.uri.base +
                "/" +
                purchaseOrderId +
                "?include=attachments";
            return this.form
                .get(url)
                .then(response => {
                    return response.data;
                })
                .catch(error => this.errorMessage(error));
        },
    }
});

