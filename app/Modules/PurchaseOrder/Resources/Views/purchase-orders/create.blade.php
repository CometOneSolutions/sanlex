@extends('app')
@section('breadcrumbs', Breadcrumbs::render('purchase-order_create'))
@section('content')

<section id="purchase">
    <div class="row">
        <div class="col-12">
            <div class="card"  v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Create New Purchase Order
                    </div>
                    <!-- <form @submit.prevent="store" v-on:keydown="form.errors.clear($event.target.name)" autocomplete="off"> -->
                    <v-form
                        @validate="store">
                        <div class="card-body">
                            @include('purchase-orders._form')
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                            </div>
                        </div>
                    </vform>
                </div>  
            </div>
        </div>
    </div>
</section>
<br/>
@endsection
@push('scripts')
<script src="{{ mix('js/purchase.js') }}"></script>
@endpush

