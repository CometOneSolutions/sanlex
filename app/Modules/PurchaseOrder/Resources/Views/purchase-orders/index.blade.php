@extends('app')
@section('breadcrumbs', Breadcrumbs::render('purchase-order_index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Purchase Order
            @if(auth()->user()->can('Create [PUR] Purchase Order'))
                <div class="float-right">
                    <a href="{{route('purchase-order_create')}}"><button type="button" class="btn btn-success btn-sm"><i
                                class="fa fa-plus" aria-hidden="true"></i> </button></a>
                </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending"
                v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('po_no')">
                                    Purchase Order No. <i class="fas fa-sort" :class="getSortIcon('po_no')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('supplier-name')">
                                    Supplier <i class="fas fa-sort" :class="getSortIcon('supplier-name')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('date')">
                                    Date <i class="fas fa-sort" :class="getSortIcon('date')"></i>
                                </a></th>
                            <th class="text-center">
                                <a v-on:click="setSorter('status')">
                                    Status <i class="fas fa-sort" :class="getSortIcon('status')"></i>
                                </a>
                            </th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{ item.purchaseNumber }}</a>&nbsp;<span
                                    v-if="item.balanceAmount < 0" class="badge badge-danger">Overpayment</span></td>
                            <td>@{{ item.supplier.data.name }}</td>
                            <td>@{{ item.date }}</td>
                            <td class="text-center"><span :class="item.badgeClass">@{{item.status}}</span></td>

                            <td class="text-center">
                                <span v-if="item.isInTransit">
                                    <a :href="item.editUri" role="button" class="btn btn-outline-primary btn-sm"><i
                                            class="fas fa-pencil-alt"></i></a>
                                </span>
                                <span v-else>
                                    <i class="far fa-check-circle text-success"></i>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>






<!-- End Watchlist-->
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush