<div class="form-row">
    <div class="form-group col-md-4">
        <label for="inputEmail4" class="col-form-label">Supplier</label>
        <p>@{{form.supplier.data.name}}</p>
    </div>

    <div class="form-group col-md-4">
        <label for="inputEmail4" class="col-form-label">Status</label>
        <p><span :class="form.badgeClass">@{{form.status}}</span></p>
    </div>

    <div class="form-group col-md-4">
        <label for="inputEmail4" class="col-form-label">Date</label>
        <p>@{{form.date}}</p>
    </div>
</div>
<br/>

<div class="form-row">
    <div class="table-responsive-sm col-lg-12">
        <table class="table">
            <thead>
            <tr class="row">
                <th class="center col-1">#</th>
                <th class="col-5">Product</th>
                <th class="col-2 text-center">UOM</th>
                <th class="center col-2 text-right">Ordered Quantity</th>
                <th class="center col-2 text-right table-success">Received Quantity</th>
            </tr>
            </thead>
            <tbody>
            <tr class="row" v-for="(item,index) in form.purchaseOrderDetails.data">
                <td class="center col-1">@{{index+1}}</td>
                <td class="left col-5">
                    @{{item.productName}}
                </td>
                <td class="center col-2 text-center">
                    @{{item.uomName}}
                </td>
                <td class="center col-2 text-right">
                    @{{ item.quantity | weight}}
                </td>
                <td class="center col-2 text-right table-success">
                    @{{ item.receivedQuantity | weight}}
                </td>


            </tr>
            <tr class="row">
                <td colspan="6" class="col-12">
                    &nbsp;

                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Remarks:</label>
        <p>@{{form.remarks}}</p>
    </div>
</div>
