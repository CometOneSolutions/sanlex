 <div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
     <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
         @{{ error[0] }}
     </small>
 </div>
 <div class="row">
     <div class="form-group col-md-6">
         <label for="inputEmail4" class="col-form-label">PO Reference <span class="text text-danger">*</span></label>
         <input type="text" class="form-control" id="purchaseNumber" name="purchaseNumber" v-model="form.purchaseNumber" placeholder="" :class="{'is-invalid': form.errors.has('purchaseNumber') }" required data-msg-required="Enter PO reference">
     </div>

 </div>
 <div class="row">
     <div class="form-group col-md-6">
         <label for="inputEmail4" class="col-form-label">Date <span class="text text-danger">*</span></label>


         <datepicker :typeable="true" :input-class="{'is-invalid': form.errors.has('date'),'form-control': true}" v-model="form.date" :required="true" disabled></datepicker>
     </div>
     <div class="form-group col-md-6" v-if="form.id">
         <label for="inputEmail4" class="col-form-label">Status <span class="text text-danger">*</span></label>
         <select class="form-control" :allow-custom="false" id="status" name="status" v-model="form.status" required placeholder="Please select" disabled>
             <option disabled selected value=''>Please select</option>
             <option value="In Transit">In Transit</option>
         </select>
     </div>
 </div>
 <div class="row">
     <div class="form-group col-md-6">
         <label for="inputEmail4" class="col-form-label">Supplier <span class="text text-danger">*</span></label>
         <select3 class="custom-select" required data-msg-required="Enter supplier" v-model="form.supplierId" :selected="defaultSelectedSupplier" :url="url" search="name" @change="loadFilteredProduct" :disabled="editMode">
         </select3>
     </div>
     <div class="form-group col-md-6">
         <label for="inputEmail4" class="col-form-label">Product Type <span class="text text-danger">*</span></label>
         <select class="form-control" :allow-custom="false" id="type" name="type" v-model="form.productType" required :class="{'is-invalid': form.errors.has('type') }" required data-msg-required="Enter supplier" :disabled="editMode">
             <option value="Coils" selected>Coils</option>
         </select>


     </div>
 </div>
 <div class="row">
     <div class="form-group col-md-12">
         <label for="inputEmail4" class="col-form-label">Remarks</label>
         <input type="text" class="form-control" id="remarks" v-model="form.remarks" placeholder="">
     </div>
 </div>

 <div class="row">
     <div class="col-12">
         <table class="table">
             <thead>
                 <tr class="row">
                     <th class="col-6">Product</th>
                     <th class="col-3">UOM</th>
                     <th class="col-2 text-right">Quantity</th>
                     <th class="col-1">&nbsp;</th>


                 </tr>
             </thead>
             <tbody>
                 <tr is="purchase-detail" class="row" v-for="(detail,index) in form.purchaseOrderDetails.data" :detail="detail" :index="index" :products="products" v-on:remove="form.purchaseOrderDetails.data.splice(index, 1)">
                 </tr>
                 <tr class="row">
                     <td class="col-12" colspan="12" v-if="productsInitialized">
                         <button type="button" class="btn btn-success btn-sm" @click="add">Add New</button>
                     </td>
                     <td class="col-12" colspan="12" v-else>
                         Please select both supplier and product type first.
                     </td>
                 </tr>
             </tbody>
         </table>
     </div>

 </div>
