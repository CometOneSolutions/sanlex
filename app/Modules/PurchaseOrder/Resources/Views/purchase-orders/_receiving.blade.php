<div class="form-row" v-if="form.receivings.data.length > 0">
    <div class="table-responsive-sm col-lg-12">
        <table class="table" v-if="form.productType == 'Coils'">
            <thead>
                <tr class="row">
                    <th class="center col-1">#</th>
                    <th class="col-2">Coil Number</th>
                    <th class="col-5">Product</th>
                    <th class="col-2 text-right">Net Weight</th>
                    <th class="col-2 text-right">Linear Meter</th>
                </tr>
            </thead>
            <tbody>
                <tr class="row" v-for="(item,index) in form.receivings.data">
                    <td class="center col-1">@{{index+1}}</td>
                    <td class="col-2">
                        @{{item.coilNumber}}
                    </td>
                    <td class="col-5">
                        @{{item.productName}}
                    </td>
                    <td class="center col-2 text-right">
                        @{{ item.netWeight | weight}}
                    </td>
                    <td class="center col-2 text-right">
                        @{{ item.linearMeter | weight}}
                    </td>
                </tr>

            </tbody>
        </table>
        <table class="table" v-else>
            <thead>
                <tr class="row">
                    <th class="center col-1">#</th>
                    <th class="col-9">Product</th>
                    <th class="col-2 text-right">Quantity</th>
                </tr>
            </thead>
            <tbody>
                <tr class="row" v-for="(item,index) in form.receivings.data">
                    <td class="center col-1">@{{index+1}}</td>

                    <td class="col-9">
                        @{{item.productName}}
                    </td>

                    <td class="center col-2 text-right">
                        @{{ item.quantity | numeric}}
                    </td>

                </tr>

            </tbody>
        </table>
    </div>
</div>
