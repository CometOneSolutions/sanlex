@extends('app')
@section('breadcrumbs', Breadcrumbs::render('purchase-order.show', $purchaseOrder))
@section('content')
<section id="purchase">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" role="tab" href="#main">General</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" role="tab" href="#upload">Upload</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        <div class="tab-content no-border">
                            <div class="tab-pane active" role="tabpanel" id="main">
                                <div class="text-right" v-if="form.isInTransit">
                                    @if(auth()->user()->can('Approve [OUT] Purchase Order'))
                                    <button type="button" @click="approve" class="btn btn-warning btn-sm" :disabled="form.receivings.data.length == 0 || form.isBusy"><i class="far fa-thumbs-up"></i>
                                        Receive</button>
                                    @endif
                                    @if(auth()->user()->can('Update [PUR] Purchase Order'))
                                    <a :href="form.editUri" class="btn btn-sm btn-outline-primary" role="button"><i class="fas fa-pencil-alt"></i></a>
                                    @endif
                                </div>
                                @include("purchase-orders._list")
                            </div>
                            <div class="tab-pane" role="tabpanel" id="upload">
                                <div class="row mb-3" v-if="form.isInTransit">
                                    <form class="d-inline" action="{{route('purchase-order_generate_template', $purchaseOrder->getId())}}" method="POST" target="_blank">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-outline-primary btn-sm" data-toggle="tooltip" data-placement="left" title="Download {{$purchaseOrder->getTemplateType()}} template" formtarget="_self">
                                            <i class="fas fa-download"></i> Download Template
                                        </button>
                                    </form>

                                    <div class="ml-auto col-3 text-right">
                                        <a href="#" class="btn btn-outline-primary btn-sm" role="button" data-toggle="modal" data-target="#add_file">&nbsp;<i class="fas fa-file-upload"></i>&nbsp;
                                        </a>
                                    </div>
                                </div>
                                <div class="row" v-if="form.isInTransit">
                                    <div class="alert alert-warning col-4" role="alert">
                                        <small>PLEASE UPLOAD A "{{$purchaseOrder->getTemplateType()}}" TEMPLATE.</small>
                                    </div>
                                </div>

                                @include("purchase-orders._receiving")
                                <div class="text-right">

                                    <div class="modal fade" id="add_file" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Upload File</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form>
                                                        <div class="row">

                                                            <div class="input-group col-12">
                                                                <input type="file" id="uploadFile" class=".form-control-file" accept="application/xlsx|application/xls" ref="file" v-on:change="handleFileChange">

                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-sm btn-secondary" @click="removeFile" data-dismiss="modal">Close</button>

                                                    <button class="btn btn-primary btn-sm" type="button" :disabled="isBusy" @click="addFile">
                                                        <div v-if="isSaving">
                                                            <i class="fas fa-spinner fa-pulse"></i> Saving
                                                        </div>
                                                        <div v-if="!isSaving">
                                                            Save
                                                        </div>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <timestamp :name="form.updatedByUser.data.name || 'System'" :time="form.updatedAt">
                            </timestamp>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@push('scripts')
<script src="{{ mix('js/purchase.js') }}"></script>
@endpush
