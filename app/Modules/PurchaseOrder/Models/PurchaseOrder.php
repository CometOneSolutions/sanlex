<?php

namespace App\Modules\PurchaseOrder\Models;

use App\Modules\Supplier\Models\Supplier;
use \DateTime;

interface PurchaseOrder
{
    const COIL = 'Coils';
    const ACCESSORY = 'Accessories';
    const SS = 'Stainless Steels';
    const INSULATION = 'Insulations';
    const SCREW = 'Screws';
    const SKYLIGHT = 'Skylights';
    const MISC = 'Misc Accessories';
    const ST = 'Structural Steels';
    const PAINT = 'Paint Adhesives';
    const FASTENER = 'Fasteners';
    const PVC = 'Pvc Fittings';

    public static function new($purchaseOrderNumber, Supplier $supplier, $productType, $date, $remarks);

    public function getId();

    public function setPONumber($value);

    public function getPONumber();

    public function setDate(DateTime $value);

    public function getDate();

    public function getSupplierId();

    public function setSupplier(Supplier $supplier);

    public function getSupplier();

    public function setRemarks($value);

    public function getRemarks();

    public function setStatus($value);

    public function getStatus();

    public function setProductType($value);

    public function getProductType();
}
