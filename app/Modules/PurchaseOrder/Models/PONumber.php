<?php

namespace App\Modules\PurchaseOrder\Models;

class PONumber
{
    private $purchaseOrder;

    const FORMAT = 'PO%s';

    public function __construct(PurchaseOrder $purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    }

    public function getCode()
    {
        $name = $this->purchaseOrder->getId();
        $paperTypeName = $this->purchaseOrder->getPaperType()->getName();
        $gsm = $this->purchaseOrder->getGSM();
        $variant = $this->purchaseOrder->getVariant();
        return sprintf(self::FORMAT, $name, $paperTypeName, $gsm, $variant);
    }

    public static function generate(PurchaseOrder $purchaseOrder)
    {
        $sku = new static($purchaseOrder);
        return $sku->getCode();
    }
}