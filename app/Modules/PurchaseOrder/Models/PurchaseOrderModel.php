<?php

namespace App\Modules\PurchaseOrder\Models;

use App\Modules\Product\Models\ProductModel;
use App\Modules\PurchaseOrder\PurchaseOrderStatus;
use App\Modules\PurchaseOrderDetail\Models\PurchaseOrderDetailModel;
use App\Modules\Receiving\Models\ReceivingModel;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Supplier\Models\SupplierModel;
use CometOneSolutions\Common\Models\C1Model;
use \DateTime;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;


class PurchaseOrderModel extends C1Model implements PurchaseOrder, UpdatableByUser
{
    use HasUpdatedByUser;


    protected $attachmentsDirectory = 'purchase-orders';

    protected $dates = ['date'];

    protected $purchaseOrderDetailsToSet = null;

    protected $outboundPaymentsToSet = null;

    protected $table = 'purchase_orders';

    public function receivings()
    {
        return $this->hasMany(ReceivingModel::class, 'purchase_order_id');
    }

    public function supplier()
    {
        return $this->belongsTo(SupplierModel::class);
    }

    public function products()
    {
        return $this->belongsToMany(ProductModel::class, 'purchase_order_details')

            ->withPivot([
                'rolls', 'quantity', 'uom_id', 'unit_price', 'unit_shipping_cost', 'total_unit_cost', 'line_total'
            ])->withTimestamps();
    }

    public function purchaseOrderDetails()
    {
        return $this->hasMany(PurchaseOrderDetailModel::class, 'purchase_order_id');
    }

    public function hasReceivings()
    {
        return $this->receivings()->exists();
    }

    public static function new($purchaseOrderNumber, Supplier $supplier, $productType, $date, $remarks)
    {
        $newPurchaseOrder = new static;
        $newPurchaseOrder->setPONumber($purchaseOrderNumber);
        $newPurchaseOrder->setSupplier($supplier);
        $newPurchaseOrder->setProductType($productType);
        $newPurchaseOrder->setDate($date);
        $newPurchaseOrder->setRemarks($remarks);
        $newPurchaseOrder->setStatus(PurchaseOrderStatus::IN_TRANSIT);
        return $newPurchaseOrder;
    }

    public function getReceivings()
    {
        return $this->receivings;
    }

    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    public function getId()
    {
        return (int) $this->id;
    }

    public function setPONumber($value)
    {
        $this->po_no = $value !== '' ? $value : null;
        return $this;
    }

    public function getPONumber()
    {
        return $this->po_no;
    }

    public function setDate(DateTime $value)
    {
        $this->date = $value;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setSupplier(Supplier $supplier)
    {
        // $this->supplier()->associate($value);
        // return $this;

        $this->supplier()->associate($supplier);
        $this->supplier_id = $supplier->getId();
        return $this;
    }

    public function getSupplier()
    {
        return $this->supplier;
    }

    public function setRemarks($value)
    {
        $this->remarks = $value !== '' ? $value : null;
        return $this;
    }

    public function getRemarks()
    {
        return $this->remarks;
    }

    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setProductType($value)
    {
        $this->product_type = $value;
        return $this;
    }

    public function getProductType()
    {
        return $this->product_type;
    }

    public function getTemplateType()
    {
        $type = null;
        switch ($this->product_type) {
            case PurchaseOrder::COIL:
                $type = 'COIL';
                break;
            case PurchaseOrder::SS:
                $type = 'STAINLESS';
                break;
            case PurchaseOrder::SCREW:
                $type = 'SCREW';
                break;
            case PurchaseOrder::SKYLIGHT:
                $type = 'SKYLIGHT';
                break;
            case PurchaseOrder::INSULATION:
                $type = 'INSULATION';
                break;
            case PurchaseOrder::MISC:
                $type = 'MISC ACCESSORY';
                break;
            case PurchaseOrder::ST:
                $type = 'STRUCTURAL';
                break;
            case PurchaseOrder::PAINT:
                $type = 'PAINT ADHESIVE';
                break;
            case PurchaseOrder::FASTENER:
                $type = 'FASTENER';
                break;
            case PurchaseOrder::PVC:
                $type = 'PVC FITTING';
                break;
            default:
                $type = 'ACCESSORY';
        }
        return $type;
    }

    public function getPurchaseOrderDetailsToSet()
    {
        return $this->purchaseOrderDetailsToSet;
    }

    public function setPurchaseOrderDetails(array $purchaseOrderDetails)
    {
        $this->purchaseOrderDetailsToSet = $purchaseOrderDetails;
        return $this;
    }

    public function getPurchaseOrderDetails()
    {
        if ($this->purchaseOrderDetailsToSet !== null) {
            return collect($this->purchaseOrderDetailsToSet);
        }
        return $this->purchaseOrderDetails;
    }

    public function isInTransit()
    {
        return ($this->getStatus() == PurchaseOrderStatus::IN_TRANSIT);
    }

    public function scopeSupplierPurchaseOrder($query, $supplierId)
    {
        return $query->whereSupplierId($supplierId);
    }

    public function scopeInTransit($query)
    {
        return $query->whereStatus(PurchaseOrderStatus::IN_TRANSIT);
    }

    public function scopeReceived($query)
    {
        return $query->whereStatus(PurchaseOrderStatus::RECEIVED);
    }

    public function getReceivingNetWeight($productId)
    {
        return $this->receivings()->product($productId)->sum('net_weight');
    }

    public function getReceivingQuantity($productId)
    {
        return $this->receivings()->product($productId)->sum('quantity');
    }
}
