<?php

namespace App\Modules\PurchaseOrder\Models;



use Illuminate\Database\Eloquent\Relations\Pivot;

class PurchasePaymentModel extends Pivot implements PurchasePayment
{
    protected $table = 'purchase_order_payments';

    public function getId()
    {
        return $this->id;
    }



    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrderModel::class, 'purchase_order_id');
    }

    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    public function setPurchaseOrder(PurchaseOrder $purchaseOrder)
    {
        $this->purchaseOrder()->associate($purchaseOrder);
        $this->purchase_order_id = $purchaseOrder->getId();
        return $this;
    }

    public function getOutboundPaymentId()
    {
        return $this->outbound_payment_id;
    }

    public function getPurchaseOrderId()
    {
        return $this->purchase_order_id;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function getAmount()
    {
        return (float) $this->amount;
    }
}
