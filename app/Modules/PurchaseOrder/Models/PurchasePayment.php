<?php

namespace App\Modules\PurchaseOrder\Models;

interface PurchasePayment
{
    public function getId();

    public function getOutboundPaymentId();

    public function getPurchaseOrderId();

    public function getAmount();
}
