<?php

namespace App\Modules\PurchaseOrder\Controllers;

use App\Modules\PurchaseOrder\Services\PurchaseOrderRecordService;
use App\Modules\PurchaseOrder\Transformer\PurchaseOrderTransformer;

use App\Modules\PurchaseOrder\Requests\StorePurchaseOrder;
use App\Modules\PurchaseOrder\Requests\UpdatePurchaseOrder;
use App\Modules\PurchaseOrder\Requests\DestroyPurchaseOrder;
use App\Modules\PurchaseOrder\Requests\ImportReceiving;
use App\Modules\PurchaseOrder\Imports\ReceivingImport;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use CometOneSolutions\Common\Exceptions\GeneralApiException;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\PurchaseOrder\Requests\ApprovePurchaseOrder;

class PurchaseOrderApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        PurchaseOrderRecordService $purchaseOrderRecordService,
        PurchaseOrderTransformer $transformer

    ) {
        parent::__construct($purchaseOrderRecordService, $transformer);
        $this->service = $purchaseOrderRecordService;
        $this->transformer = $transformer;
        $this->middleware(['auth:api']);
    }

    public function store(StorePurchaseOrder $request)
    {
        $purchaseOrder = \DB::transaction(function () use ($request) {
            $purchaseOrder = $this->service->create(
                $request->getPONumber(),
                $request->getSupplier(),
                $request->getProductType(),
                $request->getDate(),
                $request->getRemarks(),
                $request->getPurchaseOrderDetails(),
                $request->user()
            );

            return $purchaseOrder;
        });

        return $this->response->item($purchaseOrder, $this->transformer)->setStatusCode(201);
    }

    public function update($purchaseOrderId, UpdatePurchaseOrder $request)
    {
        $purchaseOrder = \DB::transaction(function () use ($request) {
            $purchaseOrder = $request->getPurchaseOrder();
            $purchaseOrder = $this->service->update(
                $purchaseOrder,
                $request->getPONumber(),
                $request->getSupplier(),
                $request->getProductType(),
                $request->getDate(),
                $request->getRemarks(),
                $request->getPurchaseOrderDetails(),
                $request->user()
            );
            return $purchaseOrder;
        });

        return $this->response->item($purchaseOrder, $this->transformer)->setStatusCode(200);
    }

    public function approve($purchaseOrderId, ApprovePurchaseOrder $request)
    {
        $purchaseOrder = \DB::transaction(function () use ($request, $purchaseOrderId) {
            $purchaseOrder = $this->service->approve(
                $this->service->getByid($purchaseOrderId),
                $request->user()
            );
            return $purchaseOrder;
        });

        return $this->response->item($purchaseOrder, $this->transformer)->setStatusCode(200);
    }


    public function destroy($id, DestroyPurchaseOrder $request)
    {
        $purchaseOrder = \DB::transaction(function () use ($id) {
            $purchaseOrder = $this->service->getById($id);
            return $this->service->delete($purchaseOrder);
        });

        return $this->response->item($purchaseOrder, $this->transformer)->setStatusCode(200);
    }

    public function import($purchaseOrderId, ImportReceiving $request)
    {
        $validator = Validator::make(
            [
                'file'      => $request->file,
                'extension' => strtolower($request->file->getClientOriginalExtension()),
            ],
            [
                'file'          => 'required',
                'extension'      => 'required|in:csv,xlsx,xls',
            ]
        );

        if ($validator->fails()) {
            throw new GeneralApiException('File type is not supported.', 'Error', 400, true);
        }
        \DB::transaction(function () use ($request, $purchaseOrderId) {
            Excel::import(new ReceivingImport($this->service->getById($purchaseOrderId), $request->user()), $request->file('file'));
        });
    }
}
