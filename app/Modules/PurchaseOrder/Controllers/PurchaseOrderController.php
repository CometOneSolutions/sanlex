<?php

namespace App\Modules\PurchaseOrder\Controllers;

use JavaScript;
use \Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\PurchaseOrder\Export\PurchaseOrderExportModel;
use App\Modules\PurchaseOrder\Export\PurchaseOrderTemplateExport;
use App\Modules\PurchaseOrder\Services\PurchaseOrderRecordService;

class PurchaseOrderController extends Controller
{
    private $purchaseOrderRecordService;
    private $products;
    // protected $productAccessoryTemplateView = 'products.product-template.print-accessory';
    protected $productCoilTemplateView = 'products.product-template.print-coil';
    // protected $productStainlessTemplateView = 'products.product-template.print-stainless';
    // protected $productInsulationTemplateView = 'products.product-template.print-insulation';
    // protected $productScrewTemplateView = 'products.product-template.print-screw';
    // protected $productSkylightTemplateView = 'products.product-template.print-skylight';
    // protected $productMiscAccessoryTemplateView = 'products.product-template.print-misc-accessory';
    // protected $productStructuralTemplateView = 'products.product-template.print-structural';
    // protected $productPaintAdhesiveTemplateView = 'products.product-template.print-paint-adhesive';
    // protected $productFastenerTemplateView = 'products.product-template.print-fastener';
    // protected $productPvcFittingTemplateView = 'products.product-template.print-pvc-fitting';

    public function __construct(PurchaseOrderRecordService $purchaseOrderRecordService, ProductRepository $products)
    {
        $this->purchaseOrderRecordService = $purchaseOrderRecordService;
        $this->products = $products;
        $this->middleware('permission:Read [PUR] Purchase Order')->only(['show', 'index', 'indexPending']);
        $this->middleware('permission:Create [PUR] Purchase Order')->only('create');
        $this->middleware('permission:Update [PUR] Purchase Order')->only('edit');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        JavaScript::put([
            'filterable' => $this->purchaseOrderRecordService->getAvailableFilters(),
            'sorter' => 'status,-created_at',
            'sortAscending' => true,
            'baseUrl' => '/api/purchase-orders?include=supplier'
        ]);
        return view('purchase-orders.index');
    }

    public function indexPending()
    {
        JavaScript::put([
            'filterable' => $this->purchaseOrderRecordService->getAvailableFilters(),
            'sorter' => 'date',
            'sortAscending' => false,
            'baseUrl' => '/api/purchase-orders?status=In%20Transit&include=supplier'
        ]);
        return view('purchase-orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        JavaScript::put(['id' => null, 'productType' => null, 'supplierId' => null]);
        return view('purchase-orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(['id' => $id, 'productType' => null, 'supplierId' => null]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER
        $purchaseOrder = $this->purchaseOrderRecordService->getById($id);
        return view('purchase-orders.show', compact('purchaseOrder'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $purchaseOrder = $this->purchaseOrderRecordService->getById($id);
        JavaScript::put(['id' => $id, 'productType' => $purchaseOrder->getProductType(), 'supplierId' => $purchaseOrder->getSupplierId()]);
        //INTERNAL API CALL TO GET NAME OF CUSTOMER

        //$purchaseOrder->getProductType();
        return view('purchase-orders.edit', compact('purchaseOrder'));
    }

    public function editPfi($id)
    {
        JavaScript::put(['id' => $id]);
        $purchaseOrder = $this->purchaseOrderRecordService->getById($id);
        return view('purchase-orders.editpfi', compact('purchaseOrder'));
    }

    public function editDownpayments($id)
    {
        JavaScript::put(['id' => $id]);
        $purchaseOrder = $this->purchaseOrderRecordService->getById($id);
        return view('purchase-orders.editdownpayments', compact('purchaseOrder'));
    }

    public function generate($purchaseOrderId)
    {
        $purchaseOrder = $this->purchaseOrderRecordService->getById($purchaseOrderId);

        $data = new PurchaseOrderExportModel($purchaseOrder);
        return Excel::download(new PurchaseOrderTemplateExport($data), $this->generateFileName(PurchaseOrder::COIL));
    }

    public function generateFileName($customerName)
    {
        $name = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $customerName);
        $format = $name . 'Product-Template%s.xls';
        $current = Carbon::now()->format('YmdHs');
        return sprintf($format, $current);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function attachment($id)
    {
        JavaScript::put(['id' => $id]);
        $purchaseOrder = $this->purchaseOrderRecordService->getById($id);
        return view('purchase-orders.attachment', compact('purchaseOrder'));
    }

    private function getDisplayedFilterable()
    {
        return [
            ['id' => 'name', 'text' => 'Name']
        ];
    }

    public function retrieveProductsWithProductTypeOf($productType, $supplier)
    {
        $repo = clone $this->products;
        return $repo->supplier($supplier)->getAllProductsWithProductType($productType)->get();
    }
}
