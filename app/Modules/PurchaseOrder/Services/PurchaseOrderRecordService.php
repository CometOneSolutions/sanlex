<?php

namespace App\Modules\PurchaseOrder\Services;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Common\Services\RecordService;
use \DateTime;
use CometOneSolutions\Auth\Models\Users\User;

interface PurchaseOrderRecordService extends RecordService
{
    public function create(
        $purchaseOrderNumber,
        Supplier $supplier,
        $productType,
        DateTime $date,
        $remarks,
        array $purchaseOrderDetails = [],
        User $user = null
    );

    public function update(
        PurchaseOrder $purchaseOrder,
        $purchaseOrderNumber,
        Supplier $supplier,
        $productType,
        DateTime $date,
        $remarks,
        array $purchaseOrderDetails = [],
        User $user = null
    );

    public function delete(PurchaseOrder $purchaseOrder);
}
