<?php

namespace App\Modules\PurchaseOrder\Services;

use App\Modules\Inventory\Services\InventoryRecordService;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\PurchaseOrder\PurchaseOrderStatus;
use App\Modules\PurchaseOrder\Repositories\PurchaseOrders;
use App\Modules\PurchaseOrder\Transformer\PurchaseOrderTransformer;
use App\Modules\PurchaseOrderDetail\Repositories\PurchaseOrderDetails;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use \DateTime;
use CometOneSolutions\Auth\Models\Users\User;


class PurchaseOrderRecordServiceImpl extends RecordServiceImpl implements PurchaseOrderRecordService
{
    protected $purchaseOrders;
    protected $purchaseOrderDetails;
    protected $purchaseOrder;
    protected $purchaseOrderTransformer;

    protected $inventoryRecordService;

    protected $availableFilters = [
        ['id' => 'po_no', 'text' => 'Purchase Order Number'],
        ['id' => 'date', 'text' => 'Date'],
        ['id' => 'supplier_name', 'text' => 'Supplier'],
        ['id' => 'status', 'text' => 'Status'],
    ];

    public function __construct(
        PurchaseOrders $purchaseOrders,
        PurchaseOrder $purchaseOrder,
        PurchaseOrderTransformer $purchaseOrderTransformer,
        PurchaseOrderDetails $purchaseOrderDetails,
        InventoryRecordService $inventoryRecordService
    ) {
        parent::__construct($purchaseOrders, $purchaseOrderTransformer);
        $this->purchaseOrders = $purchaseOrders;
        $this->purchaseOrder = $purchaseOrder;
        $this->purchaseOrderDetails = $purchaseOrderDetails;
        $this->inventoryRecordService = $inventoryRecordService;
    }

    public function create(
        $purchaseOrderNumber,
        Supplier $supplier,
        $productType,
        DateTime $date,
        $remarks,
        array $purchaseOrderDetails = [],
        User $user = null
    ) {
        $purchaseOrder = $this->purchaseOrder::new($purchaseOrderNumber, $supplier, $productType, $date, $remarks);
        $purchaseOrder->setPurchaseOrderDetails($purchaseOrderDetails);

        if ($user) {
            $purchaseOrder->setUpdatedByUser($user);
        }

        $this->purchaseOrders->save($purchaseOrder);
        return $purchaseOrder;
    }

    public function update(
        PurchaseOrder $purchaseOrder,
        $purchaseOrderNumber,
        Supplier $supplier,
        $productType,
        DateTime $date,
        $remarks,
        array $purchaseOrderDetails = [],
        User $user = null
    ) {

        // TODO cannot update if there are already pfi? deliveries?
        $tmpPurchaseOrder = clone $purchaseOrder;
        $tmpPurchaseOrder->setPONumber($purchaseOrderNumber);
        $tmpPurchaseOrder->setSupplier($supplier);
        $tmpPurchaseOrder->setProductType($productType);
        $tmpPurchaseOrder->setDate($date);
        $tmpPurchaseOrder->setPurchaseOrderDetails($purchaseOrderDetails);
        $tmpPurchaseOrder->setRemarks($remarks);
        if ($user) {
            $tmpPurchaseOrder->setUpdatedByUser($user);
        }

        $this->purchaseOrders->save($tmpPurchaseOrder);

        return $tmpPurchaseOrder;
    }

    public function approve(PurchaseOrder $purchaseOrder, User $user = null)
    {
        $tmpPurchaseOrder = clone $purchaseOrder;
        if ($user) {
            $tmpPurchaseOrder->setUpdatedByUser($user);
            $tmpPurchaseOrder->setStatus(PurchaseOrderStatus::RECEIVED);
            $this->inventoryRecordService->createInventoryFromPurchaseOrder($tmpPurchaseOrder, $user);
        }
        $this->purchaseOrders->save($tmpPurchaseOrder);
        return $this->getById($purchaseOrder->getId());
    }


    public function delete(PurchaseOrder $purchaseOrder)
    {
        // TODO delete checks
        $this->purchaseOrders->delete($purchaseOrder);
        return $purchaseOrder;
    }
}
