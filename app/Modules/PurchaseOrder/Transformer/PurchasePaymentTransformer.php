<?php

namespace App\Modules\PurchaseOrder\Transformer;

use App\Modules\PurchaseOrder\Models\PurchasePayment;
use League\Fractal;

class PurchasePaymentTransformer extends Fractal\TransformerAbstract
{
    public function transform(PurchasePayment $purchasePayment)
    {
        return [
            'id' => $purchasePayment->getId(),
            'amount' => $purchasePayment->getAmount(),
        ];
    }
}
