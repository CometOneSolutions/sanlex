<?php

namespace App\Modules\PurchaseOrder\Transformer;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\PurchaseOrderDetail\Transformer\PurchaseOrderDetailTransformer;
use App\Modules\Receiving\Transformer\ReceivingTransformer;
use App\Modules\Supplier\Transformer\SupplierTransformer;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class PurchaseOrderTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'purchaseOrderDetails',
        'supplier',
        'receivings'
    ];
    public function __construct()
    {
        $this->defaultIncludes = array_merge($this->defaultIncludes, []);
    }
    public function transform(PurchaseOrder $purchaseOrder)
    {
        return [
            'id' => (int) $purchaseOrder->getId(),
            'purchaseOrderId' => (int) $purchaseOrder->getId(),
            'date' => $purchaseOrder->getDate()->toDateString(),
            'supplierId' => $purchaseOrder->getSupplierId(),
            'status' => $purchaseOrder->getStatus(),
            'productType' => $purchaseOrder->getProductType(),
            'purchaseNumber' => $purchaseOrder->getPONumber(),
            'remarks' => $purchaseOrder->getRemarks(),
            'isInTransit' => $purchaseOrder->isInTransit(),
            'badgeClass' => ($purchaseOrder->isInTransit()) ? 'badge badge-warning' : 'badge badge-success',
            'showUri' => route('purchase-order_show', $purchaseOrder->id),
            'editUri' => route('purchase-order_edit', $purchaseOrder->id),
            'updatedAt' => $purchaseOrder->updated_at,
            // 'createdAt' => $purchaseOrder->created_at
        ];
    }

    public function includePurchaseOrderDetails(PurchaseOrder $purchaseOrder)
    {
        $purchaseOrderDetails = $purchaseOrder->getPurchaseOrderDetails();
        return $this->collection($purchaseOrderDetails, new PurchaseOrderDetailTransformer);
    }
    public function includeSupplier(PurchaseOrder $purchaseOrder)
    {
        $supplier = $purchaseOrder->getSupplier();
        return $this->item($supplier, new SupplierTransformer);
    }
    public function includeReceivings(PurchaseOrder $purchaseOrder)
    {
        $receivings = $purchaseOrder->getReceivings();
        return $this->collection($receivings, new ReceivingTransformer);
    }
}
