<?php

namespace App\Modules\PurchaseOrder\Providers;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\PurchaseOrder\Models\PurchaseOrderModel;
use App\Modules\PurchaseOrder\Repositories\EloquentPurchaseOrders;
use App\Modules\PurchaseOrder\Repositories\PurchaseOrders;
use App\Modules\PurchaseOrder\Services\PurchaseOrderRecordService;
use App\Modules\PurchaseOrder\Services\PurchaseOrderRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class PurchaseOrderServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/PurchaseOrder/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(PurchaseOrderRecordService::class, PurchaseOrderRecordServiceImpl::class);
        $this->app->bind(PurchaseOrders::class, EloquentPurchaseOrders::class);
        $this->app->bind(PurchaseOrder::class, PurchaseOrderModel::class);
    }
}
