<?php

namespace App\Modules\InsulationType\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateInsulationType extends InsulationTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Insulation Type');
    }
}
