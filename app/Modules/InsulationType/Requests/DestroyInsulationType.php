<?php

namespace App\Modules\InsulationType\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyInsulationType extends InsulationTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Insulation Type');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
