<?php

namespace App\Modules\InsulationType\Requests;

use Dingo\Api\Http\FormRequest;

class StoreInsulationType extends InsulationTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Insulation Type');
    }
}
