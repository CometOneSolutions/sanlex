<?php

namespace App\Modules\InsulationType\Services;

use App\Modules\InsulationType\Models\InsulationType;
use App\Modules\InsulationType\Repositories\InsulationTypeRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class InsulationTypeRecordServiceImpl extends RecordServiceImpl implements InsulationTypeRecordService
{
    use CanUpdateProductName;

    private $insulationType;
    private $insulationTypes;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(InsulationTypeRepository $insulationTypes, InsulationType $insulationType)
    {
        $this->insulationTypes = $insulationTypes;
        $this->insulationType = $insulationType;
        parent::__construct($insulationTypes);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $insulationType = new $this->insulationType;
        $insulationType->setName($name);

        if ($user) {
            $insulationType->setUpdatedByUser($user);
        }
        $this->insulationTypes->save($insulationType);
        return $insulationType;
    }

    public function update(
        InsulationType $insulationType,
        $name,
        User $user = null
    ) {
        $tempInsulationType = clone $insulationType;
        $tempInsulationType->setName($name);
        if ($user) {
            $tempInsulationType->setUpdatedByUser($user);
        }
        $this->insulationTypes->save($tempInsulationType);

        $this->updateProductableProductNames([
            $tempInsulationType->getInsulations()
        ]);

        return $tempInsulationType;
    }

    public function delete(InsulationType $insulationType)
    {
        $this->insulationTypes->delete($insulationType);
        return $insulationType;
    }
}
