<?php

namespace App\Modules\InsulationType\Services;

use App\Modules\InsulationType\Models\InsulationType;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface InsulationTypeRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        InsulationType $insulationType,
        $name,
        User $user = null
    );

    public function delete(InsulationType $insulationType);
}
