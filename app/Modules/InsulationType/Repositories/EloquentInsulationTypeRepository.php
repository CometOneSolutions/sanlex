<?php

namespace App\Modules\InsulationType\Repositories;

use App\Modules\InsulationType\Models\InsulationType;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentInsulationTypeRepository extends EloquentRepository implements InsulationTypeRepository
{
    public function __construct(InsulationType $insulationType)
    {
        parent::__construct($insulationType);
    }

    public function save(InsulationType $insulationType)
    {
        return $insulationType->save();
    }

    public function delete(InsulationType $insulationType)
    {
        return $insulationType->delete();
    }
}
