<?php

namespace App\Modules\InsulationType\Repositories;

use App\Modules\InsulationType\Models\InsulationType;
use CometOneSolutions\Common\Repositories\Repository;

interface InsulationTypeRepository extends Repository
{
    public function save(InsulationType $insulationType);
    public function delete(InsulationType $insulationType);
}
