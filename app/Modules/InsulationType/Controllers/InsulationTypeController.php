<?php

namespace App\Modules\InsulationType\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\InsulationType\Services\InsulationTypeRecordService;


class InsulationTypeController extends Controller
{
    private $service;

    public function __construct(InsulationTypeRecordService $insulationTypeRecordService)
    {
        $this->service = $insulationTypeRecordService;
        $this->middleware('permission:Read [MAS] Insulation Type')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Insulation Type')->only('create');
        $this->middleware('permission:Update [MAS] Insulation Type')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/insulation-types'
        ]);
        return view('insulation-types.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('insulation-types.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $insulationType = $this->service->getById($id);
        return view('insulation-types.show', compact('insulationType'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $insulationType = $this->service->getById($id);
        return view('insulation-types.edit', compact('insulationType'));
    }
}
