<?php

namespace App\Modules\InsulationType\Controllers;


use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\InsulationType\Services\InsulationTypeRecordService;
use App\Modules\InsulationType\Transformer\InsulationTypeTransformer;
use App\Modules\InsulationType\Requests\StoreInsulationType;
use App\Modules\InsulationType\Requests\UpdateInsulationType;
use App\Modules\InsulationType\Requests\DestroyInsulationType;

class InsulationTypeApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        InsulationTypeRecordService $insulationTypeRecordService,
        InsulationTypeTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($insulationTypeRecordService, $transformer);
        $this->service = $insulationTypeRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreInsulationType $request)
    {
        $insulationType = \DB::transaction(function () use ($request) {
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->create(
                $name,

                $user
            );
        });
        return $this->response->item($insulationType, $this->transformer)->setStatusCode(201);
    }

    public function update($insulationTypeId, UpdateInsulationType $request)
    {
        $insulationType = \DB::transaction(function () use ($insulationTypeId, $request) {
            $insulationType   = $this->service->getById($insulationTypeId);
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->update(
                $insulationType,
                $name,

                $user
            );
        });
        return $this->response->item($insulationType, $this->transformer)->setStatusCode(200);
    }

    public function destroy($insulationTypeId, DestroyInsulationType $request)
    {
        $insulationType = $this->service->getById($insulationTypeId);
        $this->service->delete($insulationType);
        return $this->response->item($insulationType, $this->transformer)->setStatusCode(200);
    }
}
