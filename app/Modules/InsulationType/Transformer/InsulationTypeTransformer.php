<?php

namespace App\Modules\InsulationType\Transformer;

use App\Modules\InsulationType\Models\InsulationType;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class InsulationTypeTransformer extends UpdatableByUserTransformer
{
    public function transform(InsulationType $insulationType)
    {
        return [
            'id' => (int) $insulationType->getId(),
            'text' => $insulationType->getName(),
            'name' => $insulationType->getName(),
            'updatedAt' => $insulationType->updated_at,
            'editUri' => route('insulation-type_edit', $insulationType->getId()),
            'showUri' => route('insulation-type_show', $insulationType->getId()),
        ];
    }
}
