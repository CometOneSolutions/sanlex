<?php

namespace App\Modules\InsulationType\Provider;

use App\Modules\InsulationType\Models\InsulationType;
use App\Modules\InsulationType\Models\InsulationTypeModel;
use App\Modules\InsulationType\Repositories\EloquentInsulationTypeRepository;
use App\Modules\InsulationType\Repositories\InsulationTypeRepository;
use App\Modules\InsulationType\Services\InsulationTypeRecordService;
use App\Modules\InsulationType\Services\InsulationTypeRecordServiceImpl;
use Illuminate\Support\ServiceProvider;


class InsulationTypeServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/InsulationType/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(InsulationTypeRecordService::class, InsulationTypeRecordServiceImpl::class);
        $this->app->bind(InsulationTypeRepository::class, EloquentInsulationTypeRepository::class);
        $this->app->bind(InsulationType::class, InsulationTypeModel::class);
    }
}
