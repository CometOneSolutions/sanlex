<?php

namespace App\Modules\InsulationType\Models;

interface InsulationType
{
    public function getId();

    public function getName();

    public function setName($value);
}
