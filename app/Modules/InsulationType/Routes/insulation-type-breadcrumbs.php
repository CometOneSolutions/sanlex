<?php

Breadcrumbs::register('insulation-type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Insulation Types', route('insulation-type_index'));
});
Breadcrumbs::register('insulation-type.create', function ($breadcrumbs) {
    $breadcrumbs->parent('insulation-type.index');
    $breadcrumbs->push('Create', route('insulation-type_create'));
});
Breadcrumbs::register('insulation-type.show', function ($breadcrumbs, $insulationType) {
    $breadcrumbs->parent('insulation-type.index');
    $breadcrumbs->push($insulationType->getName(), route('insulation-type_show', $insulationType->getId()));
});
Breadcrumbs::register('insulation-type.edit', function ($breadcrumbs, $insulationType) {
    $breadcrumbs->parent('insulation-type.show', $insulationType);
    $breadcrumbs->push('Edit', route('insulation-type_edit', $insulationType->getId()));
});
