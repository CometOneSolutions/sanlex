<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'insulation-types'], function () {
        Route::get('/', 'InsulationTypeController@index')->name('insulation-type_index');
        Route::get('/create', 'InsulationTypeController@create')->name('insulation-type_create');
        Route::get('{insulationTypeId}/edit', 'InsulationTypeController@edit')->name('insulation-type_edit');
        Route::get('{insulationTypeId}/print', 'InsulationTypeController@print')->name('insulation-type_print');
        Route::get('{insulationTypeId}', 'InsulationTypeController@show')->name('insulation-type_show');
    });
});
