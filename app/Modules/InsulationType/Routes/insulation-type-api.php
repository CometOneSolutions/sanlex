<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'insulation-types'], function () use ($api) {
        $api->get('/', 'App\Modules\InsulationType\Controllers\InsulationTypeApiController@index');
        $api->get('{insulationTypeId}', 'App\Modules\InsulationType\Controllers\InsulationTypeApiController@show');
        $api->post('/', 'App\Modules\InsulationType\Controllers\InsulationTypeApiController@store');
        $api->patch('{insulationTypeId}', 'App\Modules\InsulationType\Controllers\InsulationTypeApiController@update');
        $api->delete('{insulationTypeId}', 'App\Modules\InsulationType\Controllers\InsulationTypeApiController@destroy');
    });
});
