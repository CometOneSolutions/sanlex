<?php

namespace App\Modules\Width\Services;

use App\Modules\Product\Traits\CanUpdateProductName;
use App\Modules\Width\Models\Width;
use App\Modules\Width\Repositories\WidthRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class WidthRecordServiceImpl extends RecordServiceImpl implements WidthRecordService
{
    use CanUpdateProductName;

    private $width;
    private $widths;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(WidthRepository $widths, Width $width)
    {
        $this->widths = $widths;
        $this->width = $width;
        parent::__construct($widths);
    }

    public function create(
        $name,
        $isStandard,
        User $user = null
    ) {
        $width = new $this->width;
        $width->setName($name);
        $width->setIsStandard($isStandard);
        if ($user) {
            $width->setUpdatedByUser($user);
        }
        $this->widths->save($width);
        return $width;
    }

    public function update(
        Width $width,
        $name,
        $isStandard,
        User $user = null
    ) {
        $tempWidth = clone $width;
        $tempWidth->setName($name);
        $tempWidth->setIsStandard($isStandard);
        if ($user) {
            $tempWidth->setUpdatedByUser($user);
        }
        $this->widths->save($tempWidth);

        $this->updateProductableProductNames([
            $tempWidth->stainlessSteels(),
            $tempWidth->skylights(),
            $tempWidth->miscAccessories(),
            $tempWidth->insulations()
        ]);

        return $tempWidth;
    }

    public function delete(Width $width)
    {
        $this->widths->delete($width);
        return $width;
    }
}
