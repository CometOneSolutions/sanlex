<?php

namespace App\Modules\Width\Services;

use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface WidthRecordService extends RecordService
{
    public function create(
        $name,
        $isStandard,
        User $user = null
    );

    public function update(
        Width $width,
        $name,
        $isStandard,
        User $user = null
    );

    public function delete(Width $width);
}
