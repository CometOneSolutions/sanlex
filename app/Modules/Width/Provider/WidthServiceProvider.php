<?php

namespace App\Modules\Width\Provider;

use App\Modules\Width\Models\Width;
use App\Modules\Width\Models\WidthModel;
use App\Modules\Width\Repositories\EloquentWidthRepository;
use App\Modules\Width\Repositories\WidthRepository;
use App\Modules\Width\Services\WidthRecordService;
use App\Modules\Width\Services\WidthRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class WidthServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Width/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(WidthRecordService::class, WidthRecordServiceImpl::class);
        $this->app->bind(WidthRepository::class, EloquentWidthRepository::class);
        $this->app->bind(Width::class, WidthModel::class);
    }
}
