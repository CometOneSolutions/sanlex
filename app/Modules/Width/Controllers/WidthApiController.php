<?php

namespace App\Modules\Width\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Width\Services\WidthRecordService;
use App\Modules\Width\Transformer\WidthTransformer;
use App\Modules\Width\Requests\StoreWidth;
use App\Modules\Width\Requests\UpdateWidth;
use App\Modules\Width\Requests\DestroyWidth;

class WidthApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        WidthRecordService $widthRecordService,
        WidthTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($widthRecordService, $transformer);
        $this->service = $widthRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreWidth $request)
    {
        $width = \DB::transaction(function () use ($request) {
            $name       = $request->getName();
            $isStandard = $request->getIsStandard();
            $user       = $request->user();

            return $this->service->create(
                $name,
                $isStandard,
                $user
            );
        });
        return $this->response->item($width, $this->transformer)->setStatusCode(201);
    }

    public function update($widthId, UpdateWidth $request)
    {
        $width = \DB::transaction(function () use ($widthId, $request) {
            $width   = $this->service->getById($widthId);
            $name       = $request->getName();
            $isStandard = $request->getIsStandard();
            $user       = $request->user();

            return $this->service->update(
                $width,
                $name,
                $isStandard,
                $user
            );
        });
        return $this->response->item($width, $this->transformer)->setStatusCode(200);
    }

    public function destroy($widthId, DestroyWidth $request)
    {
        $width = $this->service->getById($widthId);
        $this->service->delete($width);
        return $this->response->item($width, $this->transformer)->setStatusCode(200);
    }

}
