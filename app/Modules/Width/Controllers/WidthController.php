<?php

namespace App\Modules\Width\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Width\Services\WidthRecordService;


class WidthController extends Controller
{
    private $service;

    public function __construct(WidthRecordService $widthRecordService)
    {
        $this->service = $widthRecordService;
        $this->middleware('permission:Read [MAS] Width')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Width')->only('create');
        $this->middleware('permission:Update [MAS] Width')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/widths'
        ]);
        return view('widths.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('widths.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $width = $this->service->getById($id);
        return view('widths.show', compact('width'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $width = $this->service->getById($id);
        return view('widths.edit', compact('width'));
    }
}
