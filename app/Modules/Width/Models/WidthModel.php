<?php

namespace App\Modules\Width\Models;

use App\Modules\Coil\Models\CoilModel;
use App\Modules\Insulation\Models\InsulationModel;
use App\Modules\Length\Models\LengthModel;
use App\Modules\MiscAccessory\Models\MiscAccessoryModel;
use App\Modules\Skylight\Models\SkylightModel;
use App\Modules\StainlessSteel\Models\StainlessSteelModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Models\Users\User;

class WidthModel extends Model implements Width, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'widths';

    public function miscAccessoryLengths()
    {
        return $this->belongsToMany(LengthModel::class, 'misc_accessories', 'width_id', 'length_id');
    }

    public function getMiscAccessoryLengths()
    {
        return $this->miscAccessoryLengths;
    }

    public function coils()
    {
        return $this->hasMany(CoilModel::class, 'width_id');
    }

    public function stainlessSteels()
    {
        return $this->hasMany(StainlessSteelModel::class, 'width_id');
    }
    public function skylights()
    {
        return $this->hasMany(SkylightModel::class, 'width_id');
    }
    public function miscAccessories()
    {
        return $this->hasMany(MiscAccessoryModel::class, 'width_id');
    }
    public function insulations()
    {
        return $this->hasMany(InsulationModel::class, 'width_id');
    }

    public function getInsulations()
    {
        return $this->insulations;
    }
    public function getMiscAccessories()
    {
        return $this->miscAccessories;
    }

    public function getSkylights()
    {
        return $this->skylights;
    }
    public function getStainlessSteels()
    {
        return $this->stainlessSteels;
    }


    public function getCoils()
    {
        return $this->coils;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    public function setIsStandard(bool $value)
    {
        $this->is_standard = $value;
        return $this;
    }

    public function getIsStandard()
    {
        return (bool) $this->is_standard;
    }

    public function scopeStandard($query)
    {
       return $query->where('is_standard', true);
    }

    public function scopeNonStandard($query)
    {
        return $query->where('is_standard', false);
    }

}
