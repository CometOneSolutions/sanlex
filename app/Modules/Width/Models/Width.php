<?php

namespace App\Modules\Width\Models;



interface Width
{
    public function getId();

    public function getName();

    public function setName($value);

    public function setIsStandard(bool $value);

    public function getIsStandard();
}
