<?php

namespace App\Modules\Width\Repositories;

use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentWidthRepository extends EloquentRepository implements WidthRepository
{
    public function __construct(Width $width)
    {
        parent::__construct($width);
    }

    public function save(Width $width)
    {
        return $width->save();
    }

    public function delete(Width $width)
    {
        return $width->delete();
    }
}
