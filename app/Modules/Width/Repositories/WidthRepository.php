<?php

namespace App\Modules\Width\Repositories;

use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Repositories\Repository;

interface WidthRepository extends Repository
{
    public function save(Width $width);
    public function delete(Width $width);
}
