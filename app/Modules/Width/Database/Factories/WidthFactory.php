<?php

use Faker\Generator as Faker;
use App\Modules\Width\Models\WidthModel;
use CometOneSolutions\Auth\UserModel;

$factory->define(WidthModel::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->company,
        'is_standard' => true,
        'updated_by_user_id' => $faker->randomElement(UserModel::pluck('id')->toArray()),
    ];
});
