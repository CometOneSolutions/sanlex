
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Name</label>
            <p>@{{form.name }}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Is Standard</label>
            <p>@{{form.isStandardStatus }}</p>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{ mix('js/width.js') }}"></script>
@endpush