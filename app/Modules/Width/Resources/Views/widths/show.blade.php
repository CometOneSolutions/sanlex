@extends('app')
@section('breadcrumbs', Breadcrumbs::render('width.show', $width))
@section('content')
<section id="width">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Width Record
                        @if(auth()->user()->can('Update [MAS] Width'))
                            <div class="float-right">
                                <a :href="form.editUri"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> </button></a>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        @include('widths._list')
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <timestamp :name="form.updatedByUser.data.name" :time="form.updatedAt"></timestamp>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</section>
<br/>
@endsection
                    