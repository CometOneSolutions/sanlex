<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
            @{{ error[0] }}
    </small>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="name">Name <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="name" name="name" v-model="form.name" aria-describedby="nameHelp" placeholder=""
            :class="{'is-invalid': form.errors.has('name')}" 
            required 
            data-msg-required="Enter width name">
        </div>
    </div>
</div>
<div class="form-check">
    <input class="form-check-input" type="checkbox" id="isStandardCheck" v-model="form.isStandard">
    <label class="form-check-label" for="isStandardCheck">
        Is Standard?
    </label>
</div>

@push('scripts')
<script src="{{ mix('js/width.js') }}"></script>
@endpush