<?php

namespace App\Modules\Width\Transformer;

use App\Modules\Width\Models\Width;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class WidthTransformer extends UpdatableByUserTransformer
{
    public function transform(Width $width)
    {
        return [
            'id' => (int)$width->getId(),
            'text' => $width->getName(),
            'name' => $width->getName(),
            'isStandard' => (bool) $width->getIsStandard(),
            'isStandardStatus' => (bool) $width->getIsStandard() ? 'Yes' : 'No',
            'updatedAt' => $width->updated_at,
            'editUri' => route('width_edit', $width->getId()),
            'showUri' => route('width_show', $width->getId()),
        ];
    }
}
