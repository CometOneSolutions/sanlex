<?php

namespace App\Modules\Width\Requests;

use Dingo\Api\Http\FormRequest;

class WidthRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'                  => 'required|unique:widths,name,' . $this->route('widthId') ?? null,
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => 'Name is required',
            'name.unique'           => 'Name already exists'
        ];
    }

    public function getName()
    {
        return $this->input('name');
    }

    public function getIsStandard()
    {
        return $this->input('isStandard') ? $this->input('isStandard'): false;
    }
}
