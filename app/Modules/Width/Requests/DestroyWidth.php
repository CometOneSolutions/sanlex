<?php

namespace App\Modules\Width\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyWidth extends WidthRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Width');
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}