<?php

namespace App\Modules\Width\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateWidth extends WidthRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Width');
    }

}