<?php

namespace App\Modules\Width\Requests;

use Dingo\Api\Http\FormRequest;

class StoreWidth extends WidthRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Width');
    }

}
