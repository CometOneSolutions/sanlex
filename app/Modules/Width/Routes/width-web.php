<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'widths'], function () {
        Route::get('/', 'WidthController@index')->name('width_index');
        Route::get('/create', 'WidthController@create')->name('width_create');
        Route::get('{widthId}/edit', 'WidthController@edit')->name('width_edit');
        Route::get('{widthId}/print', 'WidthController@print')->name('width_print');
        Route::get('{widthId}', 'WidthController@show')->name('width_show');
    });

});
