<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'widths'], function () use ($api) {
        $api->get('/', 'App\Modules\Width\Controllers\WidthApiController@index');
        $api->get('{widthId}', 'App\Modules\Width\Controllers\WidthApiController@show');
        $api->post('/', 'App\Modules\Width\Controllers\WidthApiController@store');
        $api->patch('{widthId}', 'App\Modules\Width\Controllers\WidthApiController@update');
        $api->delete('{widthId}', 'App\Modules\Width\Controllers\WidthApiController@destroy');
    });
});
