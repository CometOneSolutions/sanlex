<?php

Breadcrumbs::register('width.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Widths', route('width_index'));
});
Breadcrumbs::register('width.create', function ($breadcrumbs) {
    $breadcrumbs->parent('width.index');
    $breadcrumbs->push('Create', route('width_create'));
});
Breadcrumbs::register('width.show', function ($breadcrumbs, $width) {
    $breadcrumbs->parent('width.index');
    $breadcrumbs->push($width->getName(), route('width_show', $width->getId()));
});
Breadcrumbs::register('width.edit', function ($breadcrumbs, $width) {
    $breadcrumbs->parent('width.show', $width);
    $breadcrumbs->push('Edit', route('width_edit', $width->getId()));
});
