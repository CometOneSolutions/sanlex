<?php

namespace App\Modules\Insulation\Repositories;

use App\Modules\Insulation\Models\Insulation;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentInsulationRepository extends EloquentRepository implements InsulationRepository
{
    public function __construct(Insulation $insulation)
    {
        parent::__construct($insulation);
    }

    public function save(Insulation $insulation)
    {
        return $insulation->save();
    }

    public function delete(Insulation $insulation)
    {
        $product = $insulation->getProduct();
        $product->delete();
        return $insulation->delete();
    }
    public function filterByName($productName)
    {
        return $this->model->whereHas('product', function ($product) use ($productName) {
            return $product->where('name', 'like', '%' . $productName . '%');
        });
    }
    public function filterBySupplierName($supplierName)
    {
        return $this->model->whereHas('product', function ($product) use ($supplierName) {
            return $product->whereHas('supplier', function ($supplier) use ($supplierName) {
                return $supplier->where('name', 'like', '%' . $supplierName . '%');
            });
        });
    }

    public function filterByInsulationTypeName($insulationTypeName)
    {
        return $this->model->insulationTypeName($insulationTypeName);
    }


    public function inStock()
    {
        $this->model = $this->model->inStock();
        return $this;
    }
}
