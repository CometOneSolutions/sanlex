<?php

namespace App\Modules\Insulation\Repositories;

use App\Modules\Insulation\Models\Insulation;
use CometOneSolutions\Common\Repositories\Repository;

interface InsulationRepository extends Repository
{
    public function save(Insulation $insulation);
    public function delete(Insulation $insulation);
}
