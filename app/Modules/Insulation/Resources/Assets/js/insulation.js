import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select3 from "@c1_common_js/components/Select3";


new Vue({
    el: '#insulation',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select3
    },
    data: {
        form: new Form({
            name: null,
            supplierId: null,
            shortCode: null,
            backingSideText: null,
            backingSideId: null,
            lengthText: null,
            lengthId: null,
            widthText: null,
            widthId: null,
            thicknessText: null,
            thicknessId: null,
            insulationDensityText: null,
            insulationDensityId: null,
            insulationTypeText: null,
            insulationTypeId: null,

        }),
        dataInitialized: true,
        supplierUrl: '/api/suppliers?sort=name',
        defaultSelectedSupplier: {},
        backingSideUrl: '/api/backing-sides?sort=name',
        defaultSelectedBackingSide: {},
        lengthUrl: '/api/lengths?sort=name',
        widthUrl: '/api/widths?sort=name',
        defaultSelectedLength: {},
        thicknessUrl: '/api/thicknesses?sort=name',
        defaultSelectedThickness: {},
        insulationDensityUrl: '/api/insulation-densities?sort=name',
        defaultSelectedInsulationDensity: {},
        insulationTypeUrl: '/api/insulation-types?sort=name',
        defaultSelectedInsulationType: {},

    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },
    computed: {
        // selectedSupplier() {
        //     if(this.form.supplierId == null)
        //     {
        //         return undefined;
        //     }
        //     return this.supplierSelections.find(supplier => supplier.id == this.form.supplierId);
        // },
        name() {
            if (this.form.supplierId === undefined || this.form.insulationTypeId === null || this.form.lengthId === null || this.form.widthId === null || this.form.insulationDensityId === null || this.form.backingSideId === null || this.form.thicknessId === null) {
                return undefined;
            }
            return this.form.shortCode + '~' + this.form.insulationTypeText.toUpperCase() + '~' + this.form.insulationDensityText.toUpperCase()
                + '~' + this.form.thicknessText.toUpperCase() + '~' + this.form.lengthText.toUpperCase() + '~' + this.form.widthText.toUpperCase() + '~' + this.form.backingSideText.toUpperCase();
        },
        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        setShortCode(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("shortCode")) {
                this.form.shortCode = selectedObjects[0].shortCode;
            }
        },
        setBackingSide(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.backingSideText = selectedObjects[0].name;
            }

        },
        setLength(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.lengthText = selectedObjects[0].name;
            }

        },
        setWidth(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.widthText = selectedObjects[0].name;
            }

        },
        setThickness(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.thicknessText = selectedObjects[0].name;
            }

        },
        setInsulationType(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.insulationTypeText = selectedObjects[0].name;
            }

        },
        setInsulationDensity(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.insulationDensityText = selectedObjects[0].name;
            }

        },
        destroy() {
            this.form.deleteWithConfirmation('/api/insulations/' + this.form.id).then(response => {
                this.form.successModal('Insulation was removed.').then(() =>
                    window.location = '/master-file/insulations/'
                );
            });
        },

        store() {
            this.form.name = this.name;
            this.form.postWithModal('/api/insulations', null, 'Insulation was saved.');
        },

        update() {
            this.form.name = this.name;
            this.form.patch('/api/insulations/' + this.form.id).then(response => {
                this.form.successModal('Insulation was updated.').then(() =>
                    window.location = '/master-file/insulations/' + this.form.id
                );
            })
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {


        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/insulations/' + id + '?include=product.supplier,backingSide,thickness,length,width,insulationDensity,insulationType')
                .then(response => {
                    this.loadData(response.data);

                    this.form.shortCode = response.data.product.data.supplier.data.shortCode;
                    this.form.lengthText = response.data.length.data.name;
                    this.form.widthText = response.data.width.data.name;
                    this.form.backingSideText = response.data.backingSide.data.name;
                    this.form.thicknessText = response.data.thickness.data.name;
                    this.form.insulationTypeText = response.data.insulationType.data.name;
                    this.form.insulationDensityText = response.data.insulationDensity.data.name;

                    this.defaultSelectedSupplier = {
                        text: response.data.product.data.supplier.data.name,
                        id: response.data.product.data.supplier.data.id,
                    };
                    this.defaultSelectedBackingSide = {
                        text: response.data.backingSide.data.name,
                        id: response.data.backingSide.data.id,
                    };
                    this.defaultSelectedThickness = {
                        text: response.data.thickness.data.name,
                        id: response.data.thickness.data.id,
                    };
                    this.defaultSelectedLength = {
                        text: response.data.length.data.name,
                        id: response.data.length.data.id,
                    };
                    this.defaultSelectedWidth = {
                        text: response.data.width.data.name,
                        id: response.data.width.data.id,
                    };
                    this.defaultSelectedInsulationType = {
                        text: response.data.insulationType.data.name,
                        id: response.data.insulationType.data.id,
                    };
                    this.defaultSelectedInsulationDensity = {
                        text: response.data.insulationDensity.data.name,
                        id: response.data.insulationDensity.data.id,
                    };
                    this.dataInitialized = true;
                });
        }
    },
    mounted() {
        console.log("Init insulation script...");
    }
});