import {Form} from '@c1_common_js/components/Form';

export const insulationService = new Vue({

    data: function() {
        return {
            form: new Form({}),
            uri: {
              accessories: '/api/accessories?limit=' + Number.MAX_SAFE_INTEGER
            },
        }
    },   

    methods: {

        getColors(){
            return this.form.get(this.uri.accessories);
        },
    }
});