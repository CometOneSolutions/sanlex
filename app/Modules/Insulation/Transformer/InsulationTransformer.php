<?php

namespace App\Modules\Insulation\Transformer;

use App\Modules\BackingSide\Transformer\BackingSideTransformer;
use App\Modules\Insulation\Models\Insulation;
use App\Modules\InsulationDensity\Transformer\InsulationDensityTransformer;
use App\Modules\InsulationType\Transformer\InsulationTypeTransformer;
use App\Modules\Length\Transformer\LengthTransformer;
use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\Thickness\Transformer\ThicknessTransformer;
use App\Modules\Width\Transformer\WidthTransformer;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class InsulationTransformer extends UpdatableByUserTransformer
{
	protected $availableIncludes = [
		'product', 'backingSide', 'thickness', 'width', 'length', 'insulationDensity', 'insulationType'
	];
	public function transform(Insulation $insulation)
	{
		return [
			'id' => (int) $insulation->getId(),
			'name' => $insulation->getProduct() ? $insulation->getProduct()->getName() : null,
			'supplierId' => $insulation->getProduct() ? $insulation->getProduct()->getSupplier()->getId() : null,
			'backingSideId' => $insulation->getWidthId(),
			'backingSideText' => null,
			'thicknessId' => $insulation->getThicknessId(),
			'thicknessText' => null,
			'lengthId'    => $insulation->getLengthId(),
			'lengthText' => null,
			'widthId'    => $insulation->getWidthId(),
			'widthText' => null,
			'insulationDensityId' => $insulation->getInsulationDensityId(),
			'insulationDensityText' => null,
			'insulationTypeId' => $insulation->getInsulationTypeId(),
			'insulationTypeText' => null,

			'updatedAt' => $insulation->updated_at,
			'editUri' => route('insulation_edit', $insulation->getId()),
			'showUri' => route('insulation_show', $insulation->getId()),
		];
	}

	public function includeProduct(Insulation $insulation)
	{
		$product = $insulation->getProduct();
		return $this->item($product, new ProductTransformer);
	}
	public function includeBackingSide(Insulation $insulation)
	{
		$backingSide = $insulation->getBackingSide();
		return $this->item($backingSide, new BackingSideTransformer);
	}
	public function includeThickness(Insulation $insulation)
	{
		$thickness = $insulation->getThickness();
		return $this->item($thickness, new ThicknessTransformer);
	}
	public function includeLength(Insulation $insulation)
	{
		$length = $insulation->getLength();
		return $this->item($length, new LengthTransformer);
	}
	public function includeWidth(Insulation $insulation)
	{
		$width = $insulation->getWidth();
		return $this->item($width, new WidthTransformer);
	}
	public function includeInsulationType(Insulation $insulation)
	{
		$insulationType = $insulation->getInsulationType();
		return $this->item($insulationType, new InsulationTypeTransformer);
	}
	public function includeInsulationDensity(Insulation $insulation)
	{
		$insulationDensity = $insulation->getInsulationDensity();
		return $this->item($insulationDensity, new InsulationDensityTransformer);
	}
}
