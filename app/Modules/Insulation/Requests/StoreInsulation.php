<?php

namespace App\Modules\Insulation\Requests;

use Dingo\Api\Http\FormRequest;

class StoreInsulation extends InsulationRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Insulation');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }

}
