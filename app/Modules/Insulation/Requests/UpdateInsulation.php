<?php

namespace App\Modules\Insulation\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateInsulation extends InsulationRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Insulation');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }

}