<?php

namespace App\Modules\Insulation\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyInsulation extends InsulationRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Insulation');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}