<?php

namespace App\Modules\Insulation\Requests;

use App\Modules\BackingSide\Services\BackingSideRecordService;
use App\Modules\Insulation\Services\InsulationRecordService;
use App\Modules\InsulationDensity\Services\InsulationDensityRecordService;
use App\Modules\InsulationType\Services\InsulationTypeRecordService;
use App\Modules\Length\Services\LengthRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\Thickness\Services\ThicknessRecordService;
use App\Modules\Width\Services\WidthRecordService;
use Dingo\Api\Http\FormRequest;

class InsulationRequest extends FormRequest
{
	private $service;
	private $supplierRecordService;
	private $widthRecordService;
	private $lengthRecordService;

	private $thicknessRecordService;
	private $insulationTypeRecordService;
	private $insulationDensityRecordService;

	public function __construct(
		SupplierRecordService $supplierRecordService,
		InsulationRecordService $insulationRecordService,
		BackingSideRecordService $backingSideRecordService,
		LengthRecordService $lengthRecordService,
		WidthRecordService $widthRecordService,
		ThicknessRecordService $thicknessRecordService,
		InsulationTypeRecordService $insulationTypeRecordService,
		InsulationDensityRecordService $insulationDensityRecordService
	) {
		$this->supplierRecordService = $supplierRecordService;
		$this->service = $insulationRecordService;
		$this->backingSideRecordService = $backingSideRecordService;
		$this->lengthRecordService = $lengthRecordService;
		$this->widthRecordService = $widthRecordService;
		$this->thicknessRecordService = $thicknessRecordService;
		$this->insulationTypeRecordService = $insulationTypeRecordService;
		$this->insulationDensityRecordService = $insulationDensityRecordService;
	}

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$insulation = $this->route('insulationId') ? $this->service->getById($this->route('insulationId')) : null;

		if ($insulation) {
			return [
				'name'          => 'required|unique:products,name,' . $insulation->getProduct()->getId(),

				'backingSideId'       => 'required',
				'lengthId'      => 'required',
				'widthId'      => 'required',
				'thicknessId'   => 'required',
				'insulationDensityId' => 'required',
				'insulationTypeId' => 'required'
			];
		} else {
			return [
				'name'                  => 'required|unique:products,name',

				'backingSideId'               => 'required',
				'lengthId'              => 'required',
				'widthId'      => 'required',
				'thicknessId'           => 'required',
				'insulationDensityId'        => 'required',
				'insulationTypeId'       => 'required'
			];
		}
	}
	public function messages()
	{
		return [
			'name.required'                  => 'Name is required.',
			'name.unique'                    => 'Name already exists.',
			'widthId.required'              => 'Width is required.',
			'backingSideId.required'               => 'Backing side is required.',
			'lengthId.required'              => 'Length is required.',
			'thicknessId.required'           => 'Thickness is required.',
			'insulationDensityId.required'        => 'Insulation density is required.',
			'insulationTypeId.required'       => 'Insulation type is required.'
		];
	}



	public function getName($index = 'name')
	{
		return $this->input($index);
	}

	public function getSupplier($index = 'supplierId')
	{
		return $this->supplierRecordService->getById($this->input($index));
	}

	public function getInsulation($index = 'id')
	{
		return $this->service->getById($this->input($index));
	}

	public function getBackingSide($index = 'backingSideId')
	{
		return $this->backingSideRecordService->getById($this->input($index));
	}

	public function getBacking($index = 'backingSideId')
	{
		return $this->widthRecordService->getById($this->input($index));
	}
	public function getLength($index = 'lengthId')
	{
		return $this->lengthRecordService->getById($this->input($index));
	}
	public function getWidth($index = 'widthId')
	{
		return $this->widthRecordService->getById($this->input($index));
	}
	public function getThickness($index = 'thicknessId')
	{

		return $this->thicknessRecordService->getById($this->input($index));
	}
	public function getInsulationType($index = 'insulationTypeId')
	{
		return $this->insulationTypeRecordService->getById($this->input($index));
	}
	public function getInsulationDensity($index = 'insulationDensityId')
	{
		return $this->insulationDensityRecordService->getById($this->input($index));
	}
}
