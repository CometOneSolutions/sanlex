<?php

namespace App\Modules\Insulation\Models;

use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\BackingSide\Models\BackingSideModel;
use App\Modules\InsulationDensity\Models\InsulationDensity;
use App\Modules\InsulationDensity\Models\InsulationDensityModel;
use App\Modules\InsulationType\Models\InsulationType;
use App\Modules\InsulationType\Models\InsulationTypeModel;
use App\Modules\Length\Models\Length;
use App\Modules\Length\Models\LengthModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Product\Traits\Productable;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Thickness\Models\ThicknessModel;
use App\Modules\Width\Models\Width;
use App\Modules\Width\Models\WidthModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;

class InsulationModel extends Model implements Insulation, UpdatableByUser, Productable
{
    use HasUpdatedByUser;

    protected $table = 'insulations';

    public function product()
    {
        return $this->morphOne(ProductModel::class, 'productable');
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }
    public function backingSide()
    {
        return $this->belongsTo(BackingSideModel::class, 'backing_side_id');
    }
    public function insulationDensity()
    {
        return $this->belongsTo(InsulationDensityModel::class, 'insulation_density_id');
    }
    public function width()
    {
        return $this->belongsTo(WidthModel::class, 'width_id');
    }

    public function length()
    {
        return $this->belongsTo(LengthModel::class, 'length_id');
    }

    public function thickness()
    {
        return $this->belongsTo(ThicknessModel::class, 'thickness_id');
    }

    public function insulationType()
    {
        return $this->belongsTo(InsulationTypeModel::class, 'insulation_type_id');
    }

    public function getThicknessId()
    {
        return (int) $this->thickness_id;
    }
    public function getThickness()
    {
        return $this->thickness;
    }

    public function setThickness(Thickness $thickness)
    {
        $this->thickness()->associate($thickness);
        $this->thickness_id = $thickness->getId();
        return $this;
    }
    public function getWidthId()
    {
        return (int) $this->width_id;
    }
    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth(Width $width)
    {
        $this->width()->associate($width);
        $this->width_id = $width->getId();
        return $this;
    }
    public function getLengthId()
    {
        return (int) $this->length_id;
    }
    public function getLength()
    {
        return $this->length;
    }

    public function setLength(Length $length)
    {
        $this->length()->associate($length);
        $this->length_id = $length->getId();
        return $this;
    }

    public function getInsulationType()
    {
        return $this->insulationType;
    }

    public function getInsulationTypeId()
    {
        return (int) $this->insulation_type_id;
    }

    public function setInsulationType(InsulationType $insulationType)
    {
        $this->insulationType()->associate($insulationType);
        return $this;
    }

    public function getInsulationDensity()
    {
        return $this->insulationDensity;
    }

    public function setInsulationDensity(InsulationDensity $insulationDensity)
    {
        $this->insulationDensity()->associate($insulationDensity);
        return $this;
    }

    public function getInsulationDensityId()
    {
        return $this->insulation_type_id;
    }
    public function getBackingSide()
    {
        return $this->backingSide;
    }
    public function setBackingSide(BackingSide $backingSide)
    {
        $this->backingSide()->associate($backingSide);
        return $this;
    }
    public function getBackingSideId()
    {
        return (int) $this->backing_side_id;
    }


    public function isCoil()
    {
        return false;
    }
    public function scopeInStock($query)
    {
        return $query->whereHas('product', function ($product) {
            return $product->whereHas('inventory', function ($inventory) {
                return $inventory->where('stock', '>', 0);
            });
        });
    }

    public function getAssembledProductName()
    {
        $nameAttributes = [
            $this->getProduct()->getSupplier()->getShortCode(),
            $this->getInsulationType()->getName(),
            $this->getInsulationDensity()->getName(),
            $this->getThickness()->getName(),
            $this->getLength()->getName(),
            $this->getWidth()->getName(),
            $this->getBackingSide()->getName()
        ];

        return  strtoupper(implode("~", $nameAttributes));
    }

    public function scopeInsulationType($query, InsulationType $insulationType)
    {
        return $query->whereInsulationTypeId($insulationType->getId());
    }

    public function scopeThickness($query, Thickness $thickness)
    {
        return $query->whereThicknessId($thickness->getId());
    }

    public function scopeBackingSide($query, BackingSide $backingSide)
    {
        return $query->whereBackingSideId($backingSide->getId());
    }

    public function scopeInsulationDensity($query, InsulationDensity $insulationDensity)
    {
        return $query->whereInsulationDensityId($insulationDensity->getId());
    }

    public function scopeLength($query, Length $length)
    {
        return $query->whereLengthId($length->getId());
    }

    public function scopeWidth($query, Width $width)
    {
        return $query->whereWidthId($width->getId());
    }

    public function scopeInsulationTypeName($query, $insulationTypeName)
    {
        return $query->whereHas('insulationType', function ($insulationType) use ($insulationTypeName) {
            return $insulationType->whereName($insulationTypeName);
        });
    }
}
