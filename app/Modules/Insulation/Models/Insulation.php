<?php

namespace App\Modules\Insulation\Models;



interface Insulation
{
    public function getId();

    public function getDescription();

    public function setDescription($value);
}
