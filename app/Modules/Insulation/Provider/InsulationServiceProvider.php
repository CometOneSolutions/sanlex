<?php

namespace App\Modules\Insulation\Provider;

use App\Modules\Insulation\Models\Insulation;
use App\Modules\Insulation\Models\InsulationModel;
use App\Modules\Insulation\Repositories\EloquentInsulationRepository;
use App\Modules\Insulation\Repositories\InsulationRepository;
use App\Modules\Insulation\Services\InsulationRecordService;
use App\Modules\Insulation\Services\InsulationRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class InsulationServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Insulation/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(InsulationRecordService::class, InsulationRecordServiceImpl::class);
        $this->app->bind(InsulationRepository::class, EloquentInsulationRepository::class);
        $this->app->bind(Insulation::class, InsulationModel::class);
    }
}
