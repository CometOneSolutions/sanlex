<?php

Breadcrumbs::register('insulation.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Insulations', route('insulation_index'));
});
Breadcrumbs::register('insulation.create', function ($breadcrumbs) {
    $breadcrumbs->parent('insulation.index');
    $breadcrumbs->push('Create', route('insulation_create'));
});
Breadcrumbs::register('insulation.show', function ($breadcrumbs, $insulation) {
    $breadcrumbs->parent('insulation.index');
    $breadcrumbs->push($insulation->getProduct()->getName(), route('insulation_show', $insulation->getId()));
});
Breadcrumbs::register('insulation.edit', function ($breadcrumbs, $insulation) {
    $breadcrumbs->parent('insulation.show', $insulation);
    $breadcrumbs->push('Edit', route('insulation_edit', $insulation->getId()));
});
