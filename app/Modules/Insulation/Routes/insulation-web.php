<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'insulations'], function () {
        Route::get('/', 'InsulationController@index')->name('insulation_index');
        Route::get('/create', 'InsulationController@create')->name('insulation_create');
        Route::get('{insulationId}/edit', 'InsulationController@edit')->name('insulation_edit');
        Route::get('{insulationId}/print', 'InsulationController@print')->name('insulation_print');
        Route::get('{insulationId}', 'InsulationController@show')->name('insulation_show');
    });
});
