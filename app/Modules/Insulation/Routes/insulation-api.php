<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'insulations'], function () use ($api) {
        $api->get('/', 'App\Modules\Insulation\Controllers\InsulationApiController@index');
        $api->get('{insulationId}', 'App\Modules\Insulation\Controllers\InsulationApiController@show');
        $api->post('/', 'App\Modules\Insulation\Controllers\InsulationApiController@store');
        $api->patch('{insulationId}', 'App\Modules\Insulation\Controllers\InsulationApiController@update');
        $api->delete('{insulationId}', 'App\Modules\Insulation\Controllers\InsulationApiController@destroy');
    });
});
