<?php

namespace App\Modules\Insulation\Controllers;

use JavaScript;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\Insulation\Services\InsulationRecordService;

class InsulationController extends Controller
{
	private $service;

	public function __construct(InsulationRecordService $insulationRecordService)
	{
		$this->service = $insulationRecordService;
		$this->middleware('permission:Read [MAS] Insulation')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Insulation')->only('create');
		$this->middleware('permission:Update [MAS] Insulation')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'id',
			'sortAscending' => true,
			'baseUrl' => '/api/insulations?include=product,backingSide,thickness,length,width,insulationType,insulationDensity'
		]);
		return view('insulations.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('insulations.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$insulation = $this->service->getById($id);
		return view('insulations.show', compact('insulation'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$insulation = $this->service->getById($id);
		return view('insulations.edit', compact('insulation'));
	}
}
