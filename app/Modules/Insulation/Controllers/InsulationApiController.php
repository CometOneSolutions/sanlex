<?php

namespace App\Modules\Insulation\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Insulation\Services\InsulationRecordService;
use App\Modules\Insulation\Transformer\InsulationTransformer;
use App\Modules\Insulation\Requests\StoreInsulation;
use App\Modules\Insulation\Requests\UpdateInsulation;
use App\Modules\Insulation\Requests\DestroyInsulation;
use App\Modules\Product\Services\ProductRecordService;


class InsulationApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;
	protected $productRecordService;

	public function __construct(
		InsulationRecordService $insulationRecordService,
		InsulationTransformer $transformer,
		ProductRecordService $productRecordService
	) {
		parent::__construct($insulationRecordService, $transformer);
		$this->service = $insulationRecordService;
		$this->transformer = $transformer;
		$this->productRecordService = $productRecordService;
	}

	public function store(StoreInsulation $request)
	{

		$insulation = \DB::transaction(function () use ($request) {
			$name = $request->getName();

			return $this->service->create(
				$name,
				$request->getSupplier(),
				$request->getBackingSide(),
				$request->getThickness(),
				$request->getLength(),
				$request->getWidth(),
				$request->getInsulationType(),
				$request->getInsulationDensity(),
				$request->user()
			);
		});
		return $this->response->item($insulation, $this->transformer)->setStatusCode(201);
	}

	public function update($insulationId, UpdateInsulation $request)
	{
		$insulation = \DB::transaction(function () use ($request) {
			$name = $request->getName();

			return $this->service->update(
				$request->getInsulation(),
				$name,
				$request->getSupplier(),
				$request->getBackingSide(),
				$request->getThickness(),
				$request->getLength(),
				$request->getWidth(),
				$request->getInsulationType(),
				$request->getInsulationDensity(),
				$request->user()
			);
		});
		return $this->response->item($insulation, $this->transformer)->setStatusCode(200);
	}

	public function destroy($insulationId, DestroyInsulation $request)
	{
		$insulation = $this->service->getById($insulationId);
		$this->service->delete($insulation);
		return $this->response->item($insulation, $this->transformer)->setStatusCode(200);
	}
}
