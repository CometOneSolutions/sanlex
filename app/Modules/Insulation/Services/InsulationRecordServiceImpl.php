<?php

namespace App\Modules\Insulation\Services;

use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\Insulation\Models\Insulation;
use App\Modules\Insulation\Repositories\InsulationRepository;
use App\Modules\InsulationDensity\Models\InsulationDensity;
use App\Modules\InsulationType\Models\InsulationType;
use App\Modules\Length\Models\Length;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class InsulationRecordServiceImpl extends RecordServiceImpl implements InsulationRecordService
{
	private $insulation;
	private $insulations;
	private $productRecordService;

	protected $availableFilters = [
		["id" => "name", "text" => "Name"],

	];

	public function __construct(InsulationRepository $insulations, Insulation $insulation, ProductRecordService $productRecordService)
	{
		$this->insulations = $insulations;
		$this->insulation = $insulation;
		$this->productRecordService = $productRecordService;
		parent::__construct($insulations);
	}

	public function create(
		$name,
		Supplier $supplier,
		BackingSide $backingSide,
		Thickness $thickness,
		Length $length,
		Width $width,
		InsulationType $insulationType,
		InsulationDensity $insulationDensity,
		User $user = null
	) {
		$insulation = new $this->insulation;
		$insulation->setBackingSide($backingSide);
		$insulation->setThickness($thickness);
		$insulation->setLength($length);
		$insulation->setWidth($width);
		$insulation->setInsulationType($insulationType);
		$insulation->setInsulationDensity($insulationDensity);

		if ($user) {
			$insulation->setUpdatedByUser($user);
		}
		$this->insulations->save($insulation);
		$this->productRecordService->create($name, $supplier, $insulation, $user);
		return $insulation;
	}

	public function update(
		Insulation $insulation,
		$name,
		Supplier $supplier,
		BackingSide $backingSide,
		Thickness $thickness,
		Length $length,
		Width $width,
		InsulationType $insulationType,
		InsulationDensity $insulationDensity,
		User $user = null
	) {
		$tempInsulation = clone $insulation;
		$product = $tempInsulation->getProduct();
		$tempInsulation->setBackingSide($backingSide);
		$tempInsulation->setThickness($thickness);
		$tempInsulation->setLength($length);
		$tempInsulation->setWidth($width);
		$tempInsulation->setInsulationType($insulationType);
		$tempInsulation->setInsulationDensity($insulationDensity);

		if ($user) {
			$tempInsulation->setUpdatedByUser($user);
		}
		$this->insulations->save($tempInsulation);
		$this->productRecordService->update($product, $name, $supplier, $tempInsulation, $user);
		return $tempInsulation;
	}

	public function delete(Insulation $insulation)
	{
		$this->insulations->delete($insulation);
		return $insulation;
	}

	public function inStock()
	{
		$this->insulations = $this->insulations->inStock();
		return $this;
	}
}
