<?php

namespace App\Modules\Insulation\Services;

use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\Insulation\Models\Insulation;
use App\Modules\InsulationDensity\Models\InsulationDensity;
use App\Modules\InsulationType\Models\InsulationType;
use App\Modules\Length\Models\Length;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface InsulationRecordService extends RecordService
{
	public function create(
		$name,
		Supplier $supplier,
		BackingSide $backingSide,
		Thickness $thickness,
		Length $length,
		Width $width,
		InsulationType $insulationType,
		InsulationDensity $insulationDensity,
		User $user = null
	);

	public function update(
		Insulation $insulation,
		$name,
		Supplier $supplier,

		BackingSide $backingSide,
		Thickness $thickness,
		Length $length,
		Width $width,
		InsulationType $insulationType,
		InsulationDensity $insulationDensity,
		User $user = null
	);

	public function delete(Insulation $insulation);
}
