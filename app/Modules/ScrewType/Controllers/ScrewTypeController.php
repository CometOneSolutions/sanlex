<?php

namespace App\Modules\ScrewType\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\ScrewType\Services\ScrewTypeRecordService;


class ScrewTypeController extends Controller
{
    private $service;

    public function __construct(ScrewTypeRecordService $screwTypeRecordService)
    {
        $this->service = $screwTypeRecordService;
        $this->middleware('permission:Read [MAS] Screw Type')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Screw Type')->only('create');
        $this->middleware('permission:Update [MAS] Screw Type')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/screw-types'
        ]);
        return view('screw-types.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('screw-types.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $screwType = $this->service->getById($id);
        return view('screw-types.show', compact('screwType'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $screwType = $this->service->getById($id);
        return view('screw-types.edit', compact('screwType'));
    }
}
