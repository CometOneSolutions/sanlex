<?php

namespace App\Modules\ScrewType\Controllers;

use CometOneSolutions\Common\Controllers\ResourceApiController;
use Illuminate\Http\Request;

use App\Modules\ScrewType\Services\ScrewTypeRecordService;
use App\Modules\ScrewType\Transformer\ScrewTypeTransformer;
use App\Modules\ScrewType\Requests\StoreScrewType;
use App\Modules\ScrewType\Requests\UpdateScrewType;
use App\Modules\ScrewType\Requests\DestroyScrewType;

class ScrewTypeApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        ScrewTypeRecordService $screwTypeRecordService,
        ScrewTypeTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($screwTypeRecordService, $transformer);
        $this->service = $screwTypeRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreScrewType $request)
    {
        $screwType = \DB::transaction(function () use ($request) {
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->create(
                $name,

                $user
            );
        });
        return $this->response->item($screwType, $this->transformer)->setStatusCode(201);
    }

    public function update($screwTypeId, UpdateScrewType $request)
    {
        $screwType = \DB::transaction(function () use ($screwTypeId, $request) {
            $screwType   = $this->service->getById($screwTypeId);
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->update(
                $screwType,
                $name,

                $user
            );
        });
        return $this->response->item($screwType, $this->transformer)->setStatusCode(200);
    }

    public function destroy($screwTypeId, DestroyScrewType $request)
    {
        $screwType = $this->service->getById($screwTypeId);
        $this->service->delete($screwType);
        return $this->response->item($screwType, $this->transformer)->setStatusCode(200);
    }
}
