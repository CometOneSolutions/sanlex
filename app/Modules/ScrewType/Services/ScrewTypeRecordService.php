<?php

namespace App\Modules\ScrewType\Services;

use App\Modules\ScrewType\Models\ScrewType;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface ScrewTypeRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        ScrewType $screwType,
        $name,
        User $user = null
    );

    public function delete(ScrewType $screwType);
}
