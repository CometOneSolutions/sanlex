<?php

namespace App\Modules\ScrewType\Services;

use App\Modules\ScrewType\Models\ScrewType;
use App\Modules\ScrewType\Repositories\ScrewTypeRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class ScrewTypeRecordServiceImpl extends RecordServiceImpl implements ScrewTypeRecordService
{

    private $screwType;
    private $screwTypes;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(ScrewTypeRepository $screwTypes, ScrewType $screwType)
    {
        $this->screwTypes = $screwTypes;
        $this->screwType = $screwType;
        parent::__construct($screwTypes);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $screwType = new $this->screwType;
        $screwType->setName($name);

        if ($user) {
            $screwType->setUpdatedByUser($user);
        }
        $this->screwTypes->save($screwType);
        return $screwType;
    }

    public function update(
        ScrewType $screwType,
        $name,
        User $user = null
    ) {
        $tempScrewType = clone $screwType;
        $tempScrewType->setName($name);
        if ($user) {
            $tempScrewType->setUpdatedByUser($user);
        }
        $this->screwTypes->save($tempScrewType);
        return $tempScrewType;
    }

    public function delete(ScrewType $screwType)
    {
        $this->screwTypes->delete($screwType);
        return $screwType;
    }
}
