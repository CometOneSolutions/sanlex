<?php

namespace App\Modules\ScrewType\Repositories;

use App\Modules\ScrewType\Models\ScrewType;
use CometOneSolutions\Common\Repositories\Repository;

interface ScrewTypeRepository extends Repository
{
    public function save(ScrewType $screwType);
    public function delete(ScrewType $screwType);
}
