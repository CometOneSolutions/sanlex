<?php

namespace App\Modules\ScrewType\Repositories;

use App\Modules\ScrewType\Models\ScrewType;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentScrewTypeRepository extends EloquentRepository implements ScrewTypeRepository
{
    public function __construct(ScrewType $screwType)
    {
        parent::__construct($screwType);
    }

    public function save(ScrewType $screwType)
    {
        return $screwType->save();
    }

    public function delete(ScrewType $screwType)
    {
        return $screwType->delete();
    }
}
