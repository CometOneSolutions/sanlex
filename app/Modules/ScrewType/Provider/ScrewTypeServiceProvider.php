<?php

namespace App\Modules\ScrewType\Provider;

use App\Modules\ScrewType\Models\ScrewType;
use App\Modules\ScrewType\Models\ScrewTypeModel;
use App\Modules\ScrewType\Repositories\EloquentScrewTypeRepository;
use App\Modules\ScrewType\Repositories\ScrewTypeRepository;
use App\Modules\ScrewType\Services\ScrewTypeRecordService;
use App\Modules\ScrewType\Services\ScrewTypeRecordServiceImpl;
use Illuminate\Support\ServiceProvider;


class ScrewTypeServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/ScrewType/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(ScrewTypeRecordService::class, ScrewTypeRecordServiceImpl::class);
        $this->app->bind(ScrewTypeRepository::class, EloquentScrewTypeRepository::class);
        $this->app->bind(ScrewType::class, ScrewTypeModel::class);
    }
}
