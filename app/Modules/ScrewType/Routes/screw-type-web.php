<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'screw-types'], function () {
        Route::get('/', 'ScrewTypeController@index')->name('screw-type_index');
        Route::get('/create', 'ScrewTypeController@create')->name('screw-type_create');
        Route::get('{screwTypeId}/edit', 'ScrewTypeController@edit')->name('screw-type_edit');
        Route::get('{screwTypeId}/print', 'ScrewTypeController@print')->name('screw-type_print');
        Route::get('{screwTypeId}', 'ScrewTypeController@show')->name('screw-type_show');
    });
});
