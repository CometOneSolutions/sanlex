<?php

Breadcrumbs::register('screw-type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Screw Types', route('screw-type_index'));
});
Breadcrumbs::register('screw-type.create', function ($breadcrumbs) {
    $breadcrumbs->parent('screw-type.index');
    $breadcrumbs->push('Create', route('screw-type_create'));
});
Breadcrumbs::register('screw-type.show', function ($breadcrumbs, $screwType) {
    $breadcrumbs->parent('screw-type.index');
    $breadcrumbs->push($screwType->getName(), route('screw-type_show', $screwType->getId()));
});
Breadcrumbs::register('screw-type.edit', function ($breadcrumbs, $screwType) {
    $breadcrumbs->parent('screw-type.show', $screwType);
    $breadcrumbs->push('Edit', route('screw-type_edit', $screwType->getId()));
});
