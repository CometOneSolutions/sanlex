<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'screw-types'], function () use ($api) {
        $api->get('/', 'App\Modules\ScrewType\Controllers\ScrewTypeApiController@index');
        $api->get('{screwTypeId}', 'App\Modules\ScrewType\Controllers\ScrewTypeApiController@show');
        $api->post('/', 'App\Modules\ScrewType\Controllers\ScrewTypeApiController@store');
        $api->patch('{screwTypeId}', 'App\Modules\ScrewType\Controllers\ScrewTypeApiController@update');
        $api->delete('{screwTypeId}', 'App\Modules\ScrewType\Controllers\ScrewTypeApiController@destroy');
    });
});
