<?php

namespace App\Modules\ScrewType\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyScrewType extends ScrewTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Screw Type');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
