<?php

namespace App\Modules\ScrewType\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateScrewType extends ScrewTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Screw Type');
    }
}
