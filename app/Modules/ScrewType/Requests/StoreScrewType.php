<?php

namespace App\Modules\ScrewType\Requests;

use Dingo\Api\Http\FormRequest;

class StoreScrewType extends ScrewTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Screw Type');
    }
}
