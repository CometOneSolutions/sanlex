<?php

namespace App\Modules\ScrewType\Models;


interface ScrewType
{
    public function getId();

    public function getName();

    public function setName($value);
}
