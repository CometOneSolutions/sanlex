<?php

namespace App\Modules\ScrewType\Models;

use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;


class ScrewTypeModel extends Model implements ScrewType, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'screw_types';

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
