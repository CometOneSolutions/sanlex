<?php

namespace App\Modules\ScrewType\Transformer;

use App\Modules\ScrewType\Models\ScrewType;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class ScrewTypeTransformer extends UpdatableByUserTransformer
{
    public function transform(ScrewType $screwType)
    {
        return [
            'id' => (int) $screwType->getId(),
            'text' => $screwType->getName(),
            'name' => $screwType->getName(),
            'updatedAt' => $screwType->updated_at,
            'editUri' => route('screw-type_edit', $screwType->getId()),
            'showUri' => route('screw-type_show', $screwType->getId()),
        ];
    }
}
