<?php

namespace App\Modules\FastenerType\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyFastenerType extends FastenerTypeRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Fastener Type');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
