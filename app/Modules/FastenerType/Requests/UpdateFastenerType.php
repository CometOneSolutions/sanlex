<?php

namespace App\Modules\FastenerType\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateFastenerType extends FastenerTypeRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] Fastener Type');
	}
}
