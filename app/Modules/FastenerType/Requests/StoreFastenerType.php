<?php

namespace App\Modules\FastenerType\Requests;

use Dingo\Api\Http\FormRequest;

class StoreFastenerType extends FastenerTypeRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [MAS] Fastener Type');
	}
}
