<?php

namespace App\Modules\FastenerType\Services;

use App\Modules\FastenerType\Models\FastenerType;
use App\Modules\FastenerType\Repositories\FastenerTypeRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class FastenerTypeRecordServiceImpl extends RecordServiceImpl implements FastenerTypeRecordService
{

	use CanUpdateProductName;
	private $fastenerType;
	private $fastenerTypes;


	protected $availableFilters = [
		["id" => "name", "text" => "Name"]
	];

	public function __construct(FastenerTypeRepository $fastenerTypes, FastenerType $fastenerType)
	{
		$this->fastenerTypes = $fastenerTypes;
		$this->fastenerType = $fastenerType;
		parent::__construct($fastenerTypes);
	}

	public function create(
		$name,
		User $user = null
	) {
		$fastenerType = new $this->fastenerType;
		$fastenerType->setName($name);

		if ($user) {
			$fastenerType->setUpdatedByUser($user);
		}
		$this->fastenerTypes->save($fastenerType);
		return $fastenerType;
	}

	public function update(
		FastenerType $fastenerType,
		$name,
		User $user = null
	) {
		$tempFastenerType = clone $fastenerType;
		$tempFastenerType->setName($name);
		if ($user) {
			$tempFastenerType->setUpdatedByUser($user);
		}

		$this->fastenerTypes->save($tempFastenerType);

		$this->updateProductableProductNames([
			$tempFastenerType->getFasteners(),
		]);

		return $tempFastenerType;
	}

	public function delete(FastenerType $fastenerType)
	{
		$this->fastenerTypes->delete($fastenerType);
		return $fastenerType;
	}
}
