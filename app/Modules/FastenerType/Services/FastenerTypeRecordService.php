<?php

namespace App\Modules\FastenerType\Services;

use App\Modules\FastenerType\Models\FastenerType;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface FastenerTypeRecordService extends RecordService
{
	public function create(
		$name,
		User $user = null
	);

	public function update(
		FastenerType $fastenerType,
		$name,
		User $user = null
	);

	public function delete(FastenerType $fastenerType);
}
