<?php

namespace App\Modules\FastenerType\Models;


use App\Modules\Fastener\Models\FastenerModel;
use App\Modules\FastenerDimension\Models\FastenerDimensionModel;
use App\Modules\Finish\Models\FinishModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;

class FastenerTypeModel extends Model implements FastenerType, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'fastener_types';

    public function fastenerDimensions()
    {
        return $this->belongsToMany(FastenerDimensionModel::class, 'fasteners', 'fastener_type_id', 'fastener_dimension_id')
            ->withTimestamps();
    }

    public function finishes()
    {
        return $this->belongsToMany(FinishModel::class, 'fasteners', 'fastener_type_id', 'finish_id')
            ->withTimestamps();
    }

    public function getFastenerDimensions()
    {
        return $this->fastenerDimensions;
    }

    public function getFinishes()
    {
        return $this->finishes;
    }

    public function fasteners()
    {
        return $this->hasMany(FastenerModel::class, 'fastener_type_id');
    }

    public function getFasteners()
    {
        return $this->fasteners;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
