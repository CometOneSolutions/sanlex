<?php

namespace App\Modules\FastenerType\Models;


interface FastenerType
{
	public function getId();

	public function getName();

	public function setName($value);
}
