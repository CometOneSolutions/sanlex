<?php

namespace App\Modules\FastenerType\Transformer;

use App\Modules\FastenerType\Models\FastenerType;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class FastenerTypeTransformer extends UpdatableByUserTransformer
{
	public function transform(FastenerType $fastenerType)
	{
		return [
			'id' => (int) $fastenerType->getId(),
			'text' => $fastenerType->getName(),
			'name' => $fastenerType->getName(),
			'updatedAt' => $fastenerType->updated_at,
			'editUri' => route('fastener-type_edit', $fastenerType->getId()),
			'showUri' => route('fastener-type_show', $fastenerType->getId()),
		];
	}
}
