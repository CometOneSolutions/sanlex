<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'fastener-types'], function () use ($api) {
		$api->get('/', 'App\Modules\FastenerType\Controllers\FastenerTypeApiController@index');
		$api->get('{fastenerTypeId}', 'App\Modules\FastenerType\Controllers\FastenerTypeApiController@show');
		$api->post('/', 'App\Modules\FastenerType\Controllers\FastenerTypeApiController@store');
		$api->patch('{fastenerTypeId}', 'App\Modules\FastenerType\Controllers\FastenerTypeApiController@update');
		$api->delete('{fastenerTypeId}', 'App\Modules\FastenerType\Controllers\FastenerTypeApiController@destroy');
	});
});
