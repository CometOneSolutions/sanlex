<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'fastener-types'], function () {
		Route::get('/', 'FastenerTypeController@index')->name('fastener-type_index');
		Route::get('/create', 'FastenerTypeController@create')->name('fastener-type_create');
		Route::get('{fastenerTypeId}/edit', 'FastenerTypeController@edit')->name('fastener-type_edit');
		Route::get('{fastenerTypeId}/print', 'FastenerTypeController@print')->name('fastener-type_print');
		Route::get('{fastenerTypeId}', 'FastenerTypeController@show')->name('fastener-type_show');
	});
});
