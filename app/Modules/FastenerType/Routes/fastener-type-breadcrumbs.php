<?php

Breadcrumbs::register('fastener-type.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Fastener Types', route('fastener-type_index'));
});
Breadcrumbs::register('fastener-type.create', function ($breadcrumbs) {
	$breadcrumbs->parent('fastener-type.index');
	$breadcrumbs->push('Create', route('fastener-type_create'));
});
Breadcrumbs::register('fastener-type.show', function ($breadcrumbs, $fastenerType) {
	$breadcrumbs->parent('fastener-type.index');
	$breadcrumbs->push($fastenerType->getName(), route('fastener-type_show', $fastenerType->getId()));
});
Breadcrumbs::register('fastener-type.edit', function ($breadcrumbs, $fastenerType) {
	$breadcrumbs->parent('fastener-type.show', $fastenerType);
	$breadcrumbs->push('Edit', route('fastener-type_edit', $fastenerType->getId()));
});
