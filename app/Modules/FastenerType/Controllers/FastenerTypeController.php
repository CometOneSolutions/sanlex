<?php

namespace App\Modules\FastenerType\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\FastenerType\Services\FastenerTypeRecordService;


class FastenerTypeController extends Controller
{
	private $service;

	public function __construct(FastenerTypeRecordService $fastenerTypeRecordService)
	{
		$this->service = $fastenerTypeRecordService;
		$this->middleware('permission:Read [MAS] Fastener Type')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Fastener Type')->only('create');
		$this->middleware('permission:Update [MAS] Fastener Type')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'name',
			'sortAscending' => true,
			'baseUrl' => '/api/fastener-types'
		]);
		return view('fastener-types.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('fastener-types.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$fastenerType = $this->service->getById($id);
		return view('fastener-types.show', compact('fastenerType'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$fastenerType = $this->service->getById($id);
		return view('fastener-types.edit', compact('fastenerType'));
	}
}
