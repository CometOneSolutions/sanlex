<?php

namespace App\Modules\FastenerType\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\FastenerType\Services\FastenerTypeRecordService;
use App\Modules\FastenerType\Transformer\FastenerTypeTransformer;
use App\Modules\FastenerType\Requests\StoreFastenerType;
use App\Modules\FastenerType\Requests\UpdateFastenerType;
use App\Modules\FastenerType\Requests\DestroyFastenerType;

class FastenerTypeApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;

	public function __construct(
		FastenerTypeRecordService $fastenerTypeRecordService,
		FastenerTypeTransformer $transformer
	) {
		$this->middleware('auth:api');
		parent::__construct($fastenerTypeRecordService, $transformer);
		$this->service = $fastenerTypeRecordService;
		$this->transformer = $transformer;
	}

	public function store(StoreFastenerType $request)
	{
		$fastenerType = \DB::transaction(function () use ($request) {
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->create(
				$name,

				$user
			);
		});
		return $this->response->item($fastenerType, $this->transformer)->setStatusCode(201);
	}

	public function update($fastenerTypeId, UpdateFastenerType $request)
	{
		$fastenerType = \DB::transaction(function () use ($fastenerTypeId, $request) {
			$fastenerType   = $this->service->getById($fastenerTypeId);
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->update(
				$fastenerType,
				$name,

				$user
			);
		});
		return $this->response->item($fastenerType, $this->transformer)->setStatusCode(200);
	}

	public function destroy($fastenerTypeId, DestroyFastenerType $request)
	{
		$fastenerType = $this->service->getById($fastenerTypeId);
		$this->service->delete($fastenerType);
		return $this->response->item($fastenerType, $this->transformer)->setStatusCode(200);
	}
}
