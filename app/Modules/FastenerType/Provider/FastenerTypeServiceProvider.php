<?php

namespace App\Modules\FastenerType\Provider;

use App\Modules\FastenerType\Models\FastenerType;
use App\Modules\FastenerType\Models\FastenerTypeModel;
use App\Modules\FastenerType\Repositories\EloquentFastenerTypeRepository;
use App\Modules\FastenerType\Repositories\FastenerTypeRepository;
use App\Modules\FastenerType\Services\FastenerTypeRecordService;
use App\Modules\FastenerType\Services\FastenerTypeRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class FastenerTypeServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/FastenerType/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(FastenerTypeRecordService::class, FastenerTypeRecordServiceImpl::class);
		$this->app->bind(FastenerTypeRepository::class, EloquentFastenerTypeRepository::class);
		$this->app->bind(FastenerType::class, FastenerTypeModel::class);
	}
}
