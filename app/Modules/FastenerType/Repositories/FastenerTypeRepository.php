<?php

namespace App\Modules\FastenerType\Repositories;

use App\Modules\FastenerType\Models\FastenerType;
use CometOneSolutions\Common\Repositories\Repository;

interface FastenerTypeRepository extends Repository
{
	public function save(FastenerType $fastenerType);
	public function delete(FastenerType $fastenerType);
}
