<?php

namespace App\Modules\FastenerType\Repositories;

use App\Modules\FastenerType\Models\FastenerType;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentFastenerTypeRepository extends EloquentRepository implements FastenerTypeRepository
{
	public function __construct(FastenerType $fastenerType)
	{
		parent::__construct($fastenerType);
	}

	public function save(FastenerType $fastenerType)
	{
		return $fastenerType->save();
	}

	public function delete(FastenerType $fastenerType)
	{
		return $fastenerType->delete();
	}
}
