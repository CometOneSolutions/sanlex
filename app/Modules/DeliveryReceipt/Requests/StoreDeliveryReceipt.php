<?php

namespace App\Modules\DeliveryReceipt\Requests;

class StoreDeliveryReceipt extends DeliveryReceiptRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [OUT] Delivery Receipt');
    }
}
