<?php

namespace App\Modules\DeliveryReceipt\Requests;

use App\Modules\Customer\Services\CustomerRecordService;
use App\Modules\DeliveryReceiptDetails\Models\DeliveryReceiptDetail;
use App\Modules\DeliveryReceiptDetails\Services\DeliveryReceiptDetailRecordService;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordService;
use Dingo\Api\Http\FormRequest;
use \DateTime;

class DeliveryReceiptRequest extends FormRequest
{
	protected $deliveryReceiptDetail;
	protected $customerRecordService;
	protected $deliveryReceiptDetailRecordService;
	protected $inventoryDetailRecordService;

	public function __construct(
		DeliveryReceiptDetail $deliveryReceiptDetail,
		InventoryDetailRecordService $inventoryDetailRecordService,
		DeliveryReceiptDetailRecordService $deliveryReceiptDetailRecordService,
		CustomerRecordService $customerRecordService
	) {
		$this->deliveryReceiptDetail = $deliveryReceiptDetail;
		$this->customerRecordService = $customerRecordService;
		$this->deliveryReceiptDetailRecordService = $deliveryReceiptDetailRecordService;
		$this->inventoryDetailRecordService = $inventoryDetailRecordService;
	}

	public function getDeliveryReceiptDetails($field = 'deliveryReceiptDetails.data')
	{
		return array_map(function ($deliveryReceiptDetailAttributes) {
			if (isset($deliveryReceiptDetailAttributes['id']) && $deliveryReceiptDetailAttributes['id'] > 0) {
				$deliveryReceiptDetail = $this->deliveryReceiptDetailRecordService->getById($deliveryReceiptDetailAttributes['id']);
			} else {
				$deliveryReceiptDetail = new $this->deliveryReceiptDetail;
			}
			$inventoryDetail = $this->inventoryDetailRecordService->getById($deliveryReceiptDetailAttributes['inventoryDetailId']);
			$deliveryReceiptDetail->setInventoryDetail($inventoryDetail);
			$deliveryReceiptDetail->setOrderedQuantity($deliveryReceiptDetailAttributes['orderedQuantity']);

			return $deliveryReceiptDetail;
		}, $this->input($field));
	}

	public function getCustomer($field = 'customerId')
	{
		return $this->customerRecordService->getById($this->input($field));
	}

	public function getDate()
	{
		return new DateTime($this->input('date'));
	}

	public function getDrNumber()
	{
		return $this->input('drNumber');
	}

	public function getRemarks()
	{
		return $this->input('remarks');
	}

	public function getStatus()
	{
		return $this->input('status');
	}

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'drNumber' => 'required|unique:delivery_receipts,dr_number,' . $this->route('deliveryReceiptId') ?? null,
			'date' => 'required',
			'customerId' => 'required',
			'deliveryReceiptDetails.data.*.inventoryDetailId' => 'required',
			'deliveryReceiptDetails.data.*.orderedQuantity' => 'required|min:1'
		];
	}

	public function messages()
	{
		return [
			'drNumber.required'         => 'Delivery receipt number is required',
			'date.required'             => 'Date is required',
			'customerId.required'       => 'Customer is required',
			'deliveryReceiptDetails.data.*.inventoryDetailId' => 'Inventory is required',
			'deliveryReceiptDetails.data.*.orderedQuantity' => 'Quantity is required'

		];
	}
}
