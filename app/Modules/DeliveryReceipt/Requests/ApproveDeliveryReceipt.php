<?php

namespace App\Modules\DeliveryReceipt\Requests;

class ApproveDeliveryReceipt extends DeliveryReceiptRequest
{
    public function authorize()
    {
        return $this->user()->can('Approve [OUT] Delivery Receipt');
    }

    public function rules()
    {
        return [

        ];
    }
}
