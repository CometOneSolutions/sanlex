<?php

namespace App\Modules\DeliveryReceipt\Requests;

class UpdateDeliveryReceipt extends DeliveryReceiptRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [OUT] Delivery Receipt');
    }

    public function rules()
    {
        return [

        ];
    }
}
