<?php

namespace App\Modules\DeliveryReceipt\Transformers;

use App\Modules\Customer\Transformer\CustomerTransformer;
use App\Modules\DeliveryReceipt\Models\DeliveryReceipt;
use App\Modules\DeliveryReceiptDetails\Transformers\DeliveryReceiptDetailTransformer;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class DeliveryReceiptTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'customer', 'deliveryReceiptDetails', 'pivot'
    ];

    public function transform(DeliveryReceipt $deliveryReceipt)
    {
        return [
            'id' => (int) $deliveryReceipt->getId(),
            'customerId' => $deliveryReceipt->getCustomerId(),
            'drNumber' => $deliveryReceipt->getDrNumber(),
            'date' => $deliveryReceipt->getDate()->toDateString(),
            'approvedDate' => $deliveryReceipt->getApprovedDate() ? $deliveryReceipt->getApprovedDate()->toDateString() : '',
            'remarks' => $deliveryReceipt->getRemarks(),
            'status' => $deliveryReceipt->getStatus(),
            'isDraft' => $deliveryReceipt->isDraft(),
            'badgeClass' => ($deliveryReceipt->isDraft()) ? 'badge badge-warning' : 'badge badge-success',
            'showUri' => route('delivery-receipt_show', $deliveryReceipt->getId()),
            'editUri' => route('delivery-receipt_edit', $deliveryReceipt->getId()),
            'updatedAt' => $deliveryReceipt->updated_at,
        ];
    }

    public function includeCustomer(DeliveryReceipt $deliveryReceipt)
    {
        return $this->item($deliveryReceipt->getCustomer(), new CustomerTransformer);
    }

    public function includeDeliveryReceiptDetails(DeliveryReceipt $deliveryReceipt)
    {
        return $this->collection($deliveryReceipt->getDeliveryReceiptDetails(), new DeliveryReceiptDetailTransformer);
    }

    public function includePivot(DeliveryReceipt $deliveryReceipt)
    {
        return $this->item($deliveryReceipt->pivot, new DeliveryReceiptPaymentTransformer);
    }
}
