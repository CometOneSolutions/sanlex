<?php

namespace App\Modules\DeliveryReceipt\Models;

class DeliveryReceiptCode
{
    const LETTER_PREFIX = 'IN';
    const YEAR_FORMAT = 'y';
    const MONTH_FORMAT = 'm';
    const PAD_LENGTH = 4;
    const PAD_STRING = '0';
    const YEAR_SECTION_LENGTH = 2;
    const MONTH_SECTION_LENGTH = 2;

    private $year;
    private $month;
    private $number;

    public function __construct($codeString = null)
    {
        $this->init($codeString);
    }

    public function init($codeString)
    {
        $this->setYearFromCodeString($codeString);
        $this->setMonthFromCodeString($codeString);
        $this->setNumberFromCodeString($codeString);
    }

    private function setYearFromCodeString($codeString)
    {
        $this->year = $this->getYearFromCodeString($codeString);
    }

    private function getYearFromCodeString($codeString)
    {
        return (int) substr($codeString, strlen(static::LETTER_PREFIX), static::YEAR_SECTION_LENGTH);
    }

    private function setMonthFromCodeString($codeString)
    {
        $this->month = $this->getMonthFromCodeString($codeString);
    }

    private function getMonthFromCodeString($codeString)
    {
        return (int) substr($codeString, strlen(static::LETTER_PREFIX) + static::YEAR_SECTION_LENGTH, static::MONTH_SECTION_LENGTH);
    }

    private function setNumberFromCodeString($codeString)
    {
        $this->number = $this->getNumberFromCodeString($codeString);
    }

    private function getNumberFromCodeString($codeString)
    {
        return (int) substr($codeString, strlen(static::LETTER_PREFIX) + static::YEAR_SECTION_LENGTH + static::MONTH_SECTION_LENGTH, static::PAD_LENGTH);
    }

    private function setNumber($number)
    {
        $this->number = $number;
    }

    private function getNumber()
    {
        return $this->number;
    }

    private function getNumericSection()
    {
        return str_pad($this->number, self::PAD_LENGTH, self::PAD_STRING, STR_PAD_LEFT);
    }

    private function getYearSection()
    {
        return str_pad($this->year, self::YEAR_SECTION_LENGTH, self::PAD_STRING, STR_PAD_LEFT);
    }

    private function getMonthSection()
    {
        return str_pad($this->month, self::MONTH_SECTION_LENGTH, self::PAD_STRING, STR_PAD_LEFT);
    }

    public static function generateNewCurrentCode()
    {
        $newCurrentCode = new static;
        $newCurrentCode->setYear((int) now()->format(static::YEAR_FORMAT));
        $newCurrentCode->setMonth((int) now()->format(static::MONTH_FORMAT));
        $newCurrentCode->setNumber(1);
        return $newCurrentCode;
    }

    private function setYear($year)
    {
        $this->year = $year;
    }

    private function setMonth($month)
    {
        $this->month = $month;
    }

    public static function generateNewCurrentCodeString()
    {
        $newCurrentCode = static::generateNewCurrentCode();
        return $newCurrentCode->getCodeString();
    }

    public static function generateNextCodeString($codeString)
    {
        $nextCode = static::generateNextCode($codeString);
        return $nextCode->getCodeString();
    }

    public static function generateNextCode($codeString)
    {
        $code = new static;
        $code->init($codeString);
        return $code->getNext();
    }

    public function getNext()
    {
        $nextCode = clone $this;
        $nextNumber = $nextCode->getNumber() + 1;
        $nextCode->setNumber($nextNumber);
        return $nextCode;
    }

    public function getCodeString()
    {
        return static::LETTER_PREFIX . $this->getYearSection() . $this->getMonthSection() . $this->getNumericSection();
    }

    public static function generateCurrentPrefix()
    {
        return static::LETTER_PREFIX . static::generateCurrentYearSection() . static::generateCurrentMonthSection();
    }

    private static function generateCurrentYearSection()
    {
        return now()->format(static::YEAR_FORMAT);
    }

    private static function generateCurrentMonthSection()
    {
        return now()->format(static::MONTH_FORMAT);
    }
}
