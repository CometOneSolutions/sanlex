<?php

namespace App\Modules\DeliveryReceipt\Models;

use App\Modules\Customer\Models\Customer;
use App\Modules\Customer\Models\CustomerModel;
use App\Modules\DeliveryReceiptDetails\Models\DeliveryReceiptDetailModel;
use \DateTime;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Common\Models\C1Model;
use CometOneSolutions\Accounting\Traits\HasEntries;
use CometOneSolutions\Accounting\Accountable;

class DeliveryReceiptModel extends C1Model implements DeliveryReceipt, Accountable
{
    use HasUpdatedByUser;
    use HasEntries;

    protected $table = 'delivery_receipts';

    protected $dates = ['date', 'approved_date'];

    protected $deliveryReceiptDetailsToSet = null;

    const DECIMAL_PLACES = 2;

    public function customer()
    {
        return $this->belongsTo(CustomerModel::class, 'customer_id');
    }

    public function deliveryReceiptDetails()
    {
        return $this->hasMany(DeliveryReceiptDetailModel::class, 'delivery_receipt_id');
    }

    public function getAccountingUri()
    {
        return route('delivery-receipt_show', $this->getId());
    }

    public function getAccountingRefNo()
    {
        return $this->getDrNumber();
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }

    public function isDraft()
    {
        return $this->getStatus() == DeliveryReceipt::DRAFT;
    }

    public function setCustomer(Customer $customer)
    {
        $this->customer()->associate($customer);
        $this->customer_id = $customer->getId();
        return $this;
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function getCustomerId()
    {
        return $this->customer_id;
    }

    public function setDate(DateTime $date)
    {
        $this->date = $date;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getDrNumber()
    {
        return $this->dr_number;
    }

    public function setDrNumber($value)
    {
        $this->dr_number = $value;
        return $this;
    }

    public function setDeliveryReceiptDetails(array $deliveryReceiptDetails)
    {
        $this->deliveryReceiptDetailsToSet = $deliveryReceiptDetails;
        // $this->setTotalAmountFromDeliveryReceiptDetails($deliveryReceiptDetails);
        return $this;
    }



    public function getDeliveryReceiptDetails()
    {
        if ($this->deliveryReceiptDetailsToSet !== null) {
            return collect($this->deliveryReceiptDetailsToSet);
        }
        return $this->deliveryReceiptDetails;
    }

    public function getRemarks()
    {
        return $this->remarks;
    }

    public function setRemarks($value)
    {
        $this->remarks = $value;
        return $this;
    }

    public function setApprovedDate($value)
    {
        $this->approved_date = $value;
        return $this;
    }

    public function getApprovedDate()
    {
        return $this->approved_date;
    }
}
