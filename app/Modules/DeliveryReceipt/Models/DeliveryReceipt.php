<?php

namespace App\Modules\DeliveryReceipt\Models;

use App\Modules\Customer\Models\Customer;
use \DateTime;
use CometOneSolutions\Auth\Models\UpdatableByUser;

interface DeliveryReceipt extends UpdatableByUser
{
    const DRAFT = 'Draft';
    const APPROVED = 'Approved';


    public function getId();

    public function setDrNumber($value);

    public function getDrNumber();

    public function getDate();

    public function setCustomer(Customer $customer);

    public function getCustomer();

    public function getCustomerId();

    public function setDate(DateTime $date);

    public function setDeliveryReceiptDetails(array $deliveryReceiptDetails);

    public function getDeliveryReceiptDetails();

    public function setApprovedDate($value);

    public function getApprovedDate();
}
