<?php

namespace App\Modules\DeliveryReceipt\Services;

use App\Modules\Customer\Models\Customer;
use App\Modules\DeliveryReceipt\Models\DeliveryReceipt;
use App\Modules\DeliveryReceipt\Repositories\DeliveryReceipts;
use App\Modules\InventoryDetail\Services\InventoryDetailRecordService;
use Carbon\Carbon;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use \DateTime;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Utils\Filter;
use CometOneSolutions\Common\Exceptions\GeneralApiException;

class DeliveryReceiptRecordServiceImpl extends RecordServiceImpl implements DeliveryReceiptRecordService
{
    private $deliveryReceipts;
    private $deliveryReceipt;
    protected $inventoryDetailRecordService;

    public $availableFilters = [
        ['id' => 'dr_number', 'text' => 'DR No.'],
        ['id' => 'date', 'text' => 'Date'],
        ['id' => 'customer', 'text' => 'Customer'],
        ['id' => 'remarks', 'text' => 'Remarks'],

    ];

    public function __construct(DeliveryReceipts $deliveryReceipts, DeliveryReceipt $deliveryReceipt, InventoryDetailRecordService $inventoryDetailRecordService)
    {
        parent::__construct($deliveryReceipts);
        $this->deliveryReceipts = $deliveryReceipts;
        $this->deliveryReceipt = $deliveryReceipt;
        $this->inventoryDetailRecordService = $inventoryDetailRecordService;
    }

    public function create(
        DateTime $date,
        Customer $customer,
        array $deliveryReceiptDetails,
        $drNumber,
        $remarks = null,
        $status,
        User $user = null
    ) {
        $deliveryReceipt = new $this->deliveryReceipt;
        $deliveryReceipt->setDate($date);
        $deliveryReceipt->setDeliveryReceiptDetails($deliveryReceiptDetails);
        $deliveryReceipt->setCustomer($customer);
        $deliveryReceipt->setDrNumber($drNumber);
        $deliveryReceipt->setRemarks($remarks);
        $deliveryReceipt->setStatus($status);
        if ($user) {
            $deliveryReceipt->setUpdatedByUser($user);
        }

        $this->deliveryReceipts->save($deliveryReceipt);
        return $deliveryReceipt;
    }
    public function approveAll()
    {
        $filter = new Filter('status', DeliveryReceipt::DRAFT);
        $draftDeliveryReceipts = $this->getAll([$filter]);
        $approved = 0;
        $messages = [];
        $deliveryReceiptsCreated = [];
        foreach ($draftDeliveryReceipts as $deliveryReceipt) {
            if ($this->verifyIfWithStock($deliveryReceipt, $messages)) {
                $deliveryReceiptsCreated[] = $this->approve($deliveryReceipt);
                $approved++;
            }
        }

        $response = sprintf("Successfully approved <strong>%s</strong> DR/s. <br><br>", $approved);
        if (count($messages) > 0) {
            $response = $response . "The following has insufficient stock: <br>";
            for ($i = 0; $i < count($messages); $i++) {
                $response = (count($messages) != $i + 1) ? $response . $messages[$i] . ", <br>" : $response . $messages[$i];
            }
        }

        return [
            'response' => $response,
            'deliveryReceiptsCreated' => $deliveryReceiptsCreated
        ];
    }
    public function approve(
        DeliveryReceipt $deliveryReceipt
    ) {
        $messages = [];
        $tempDeliveryReceipt = clone $deliveryReceipt;
        $tempDeliveryReceipt->setStatus(DeliveryReceipt::APPROVED);
        $tempDeliveryReceipt->setApprovedDate(Carbon::now());

        if ($this->verifyIfWithStock($deliveryReceipt, $messages)) {
            $this->deliveryReceipts->save($tempDeliveryReceipt);
        } else {
            throw new GeneralApiException("A product has insufficient stock");
        }

        $this->inventoryDetailRecordService->updateInventoryStockFromDeliveryReceiptDetails($tempDeliveryReceipt->getDeliveryReceiptDetails());
        return $this->getById($deliveryReceipt->getId());
    }

    public function update(
        DeliveryReceipt $deliveryReceipt,
        DateTime $date,
        array $deliveryReceiptDetails,
        $drNumber,
        $remarks = null,
        User $user = null
    ) {
        $tempDeliveryReceipt = clone $deliveryReceipt;
        $tempDeliveryReceipt->setDate($date);
        $tempDeliveryReceipt->setDeliveryReceiptDetails($deliveryReceiptDetails);
        $tempDeliveryReceipt->setDrNumber($drNumber);
        $tempDeliveryReceipt->setRemarks($remarks);
        if ($user) {
            $tempDeliveryReceipt->setUpdatedByUser($user);
        }
        $this->deliveryReceipts->save($tempDeliveryReceipt);

        return $tempDeliveryReceipt;
    }

    public function delete(DeliveryReceipt $deliveryReceipt)
    {
        // TODO delete checks
        $this->deliveryReceipts->delete($deliveryReceipt);
        $this->inventoryDetailRecordService->updateInventoryStockFromDeliveryReceiptDetails($deliveryReceipt->getDeliveryReceiptDetails());
        return $deliveryReceipt;
    }

    public function verifyIfWithStock(DeliveryReceipt $deliveryReceipt, &$messages)
    {
        $isValid = true;
        foreach ($deliveryReceipt->getDeliveryReceiptDetails() as $deliveryReceiptDetail) {
            if ($deliveryReceiptDetail->getInventoryDetail()->getQuantity() < $deliveryReceiptDetail->getOrderedQuantity()) {
                $isValid = false;
                if (!in_array($deliveryReceipt->getDrNumber(), $messages)) {
                    $messages[] = $deliveryReceipt->getDrNumber();
                }
            }
        }
        return $isValid;
    }
}
