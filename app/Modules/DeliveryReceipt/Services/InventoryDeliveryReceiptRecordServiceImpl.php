<?php

namespace App\Modules\DeliveryReceipt\Services;

use App\Modules\Customer\Models\Customer;
use App\Modules\DeliveryReceipt\Models\DeliveryReceipt;
use App\Modules\DeliveryReceiptDetails\Services\DeliveryReceiptDetailRecordService;
use App\Modules\InventoryDetail\Models\InventoryDetail;
use CometOneSolutions\Inventory\Model\ItemMovementType;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use \DateTime;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\DecoratorRecordServiceImpl;

use \CometOneSolutions\Inventory\Model\Inventoriable;

// In: DeliveryReceipts
// Out: Warehouses

class InventoryDeliveryReceiptRecordServiceImpl extends DecoratorRecordServiceImpl implements DeliveryReceiptRecordService
{
    protected $deliveryReceiptRecordService;
    protected $itemMovementRecordService;
    protected $deliveryReceiptDetailRecordService;
    protected $locationRecordService;

    public function __construct(
        DeliveryReceiptRecordService $deliveryReceiptRecordService,
        DeliveryReceiptDetailRecordService $deliveryReceiptDetailRecordService,
        ItemMovementRecordService $itemMovementRecordService,
        LocationRecordService $locationRecordService
    ) {
        parent::__construct($deliveryReceiptRecordService);
        $this->deliveryReceiptRecordService = $deliveryReceiptRecordService;
        $this->itemMovementRecordService = $itemMovementRecordService;
        $this->deliveryReceiptDetailRecordService = $deliveryReceiptDetailRecordService;
        $this->locationRecordService = $locationRecordService;
    }

    protected function createItemMovements(Inventoriable $inventoriable, DateTime $date, $quantity, InventoryDetail $inventoryDetail)
    {
        $this->itemMovementRecordService->create(
            $inventoriable,
            $date,
            ItemMovementType::IN,
            $quantity,
            $this->locationRecordService->getByName('Deliveries'),
            $inventoryDetail
        );

        $this->itemMovementRecordService->create(
            $inventoriable,
            $date,
            ItemMovementType::OUT,
            $quantity,
            $this->locationRecordService->getByName('Warehouses'),
            $inventoryDetail
        );
        return $this;
    }

    public function create(DateTime $date, Customer $customer, array $deliveryReceiptDetails, $drNumber, $remarks = null, $status, User $user = null)
    {
        return $this->deliveryReceiptRecordService->create($date, $customer, $deliveryReceiptDetails, $drNumber, $remarks, $status, $user);
    }

    public function approve(DeliveryReceipt $deliveryReceipt)
    {
        $created = $this->deliveryReceiptRecordService->approve($deliveryReceipt);
        foreach ($created->getDeliveryReceiptDetails() as $deliveryReceiptDetail) {
            $date = $created->getDate();
            $quantity = $deliveryReceiptDetail->getOrderedQuantity();
            $inventoryDetail = $deliveryReceiptDetail->getInventoryDetail();
            $this->createItemMovements($deliveryReceiptDetail, $date, $quantity, $inventoryDetail);
        }
        return $created;
    }

    public function update(DeliveryReceipt $deliveryReceipt, DateTime $date, array $deliveryReceiptDetails, $drNumber, $remarks = null, User $user = null)
    {
        return $this->deliveryReceiptRecordService->update($deliveryReceipt, $date, $deliveryReceiptDetails, $drNumber, $remarks, $user);
        // Delete itemMovements of receivingDetails that are in original but not in updated
        // $originalDeliveryReceiptDetailIds = $deliveryReceipt->getDeliveryReceiptDetails()->map(function ($deliveryReceiptDetail) {
        //     return $deliveryReceiptDetail->getId();
        // })->all();

        // $updatedDeliveryReceiptDetailIds = array_where(array_map(function ($deliveryReceiptDetail) {
        //     return $deliveryReceiptDetail->getId();
        // }, $deliveryReceiptDetails), function ($id) {
        //     return $id != null;
        // });

        // $deletedDeliveryReceiptDetailIds = array_diff($originalDeliveryReceiptDetailIds, $updatedDeliveryReceiptDetailIds);

        // $deletedDeliveryReceiptDetails = $this->deliveryReceiptDetailRecordService->getManyByIds($deletedDeliveryReceiptDetailIds);

        // foreach ($deletedDeliveryReceiptDetails as $deletedDeliveryReceiptDetail) {
        //     $this->itemMovementRecordService->deleteFromInventoriable($deletedDeliveryReceiptDetail);
        // }

        // $updated = $this->deliveryReceiptRecordService->update($deliveryReceipt, $date, $deliveryReceiptDetails, $drNumber, $remarks, $user);

        // $date = $updated->getDate();

        // foreach ($updated->getDeliveryReceiptDetails() as $deliveryReceiptDetail) {
        //     $quantity = $deliveryReceiptDetail->getOrderedQuantity();
        //     $inventoryDetail = $deliveryReceiptDetail->getInventoryDetail();
        //     if (!$deliveryReceiptDetail->hasItemMovements()) {
        //         $this->createItemMovements($deliveryReceiptDetail, $date, $quantity, $inventoryDetail);
        //     } else {
        //         $this->itemMovementRecordService->updateFromInventoriable($deliveryReceiptDetail, $date, $quantity, $inventoryDetail);
        //     }
        // }

        // return $updated;
    }

    public function delete(DeliveryReceipt $deliveryReceipt)
    {
        foreach ($deliveryReceipt->getDeliveryReceiptDetails() as $deliveryReceiptDetail) {
            $this->itemMovementRecordService->deleteFromInventoriable($deliveryReceiptDetail);
        }
        $deleted = $this->deliveryReceiptRecordService->delete($deliveryReceipt);
        return $deleted;
    }

    public function approveAll()
    {
        $data =  $this->deliveryReceiptRecordService->approveAll();
        foreach ($data['deliveryReceiptsCreated'] as $created) {
            foreach ($created->getDeliveryReceiptDetails() as $deliveryReceiptDetail) {
                $date = $created->getDate();
                $quantity = $deliveryReceiptDetail->getOrderedQuantity();
                $inventoryDetail = $deliveryReceiptDetail->getInventoryDetail();
                $this->createItemMovements($deliveryReceiptDetail, $date, $quantity, $inventoryDetail);
            }
        }

        return $data;
    }
}
