<?php

namespace App\Modules\DeliveryReceipt\Services;

use App\Modules\Customer\Models\Customer;
use App\Modules\DeliveryReceipt\Models\DeliveryReceipt;

use CometOneSolutions\Accounting\Services\Account\AccountRecordService;
use CometOneSolutions\Accounting\Services\Entry\AccountableEntryRecordService;
use CometOneSolutions\Accounting\Services\Entry\EntryRecordService;
use CometOneSolutions\Accounting\Services\Transaction\TransactionRecordService;
use CometOneSolutions\Common\Services\RecordService;
use \DateTime;
use CometOneSolutions\Auth\Models\Users\User;

use CometOneSolutions\Accounting\Accountable;

class AccountingDeliveryReceiptRecordServiceImpl extends AccountableEntryRecordService implements DeliveryReceiptRecordService
{
    protected $accountRecordService;

    public function __construct(
        RecordService $decoratedRecordService,
        EntryRecordService $entryRecordService,
        AccountRecordService $accountRecordService,
        TransactionRecordService $transactionRecordService
    ) {
        parent::__construct($decoratedRecordService, $entryRecordService, $transactionRecordService);
        $this->accountRecordService = $accountRecordService;
    }

    protected function getDebitAccount(Accountable $accountable)
    {
        return $this->accountRecordService->getByName('DeliveryReceipts', 'title');
    }

    protected function getCreditAccount(Accountable $accountable)
    {
        return $this->accountRecordService->getByName('Accounts Receivable', 'title');
    }

    public function create(DateTime $date, Customer $customer, array $deliveryReceiptDetails, $drNumber, $remarks = null, User $user = null)
    {
        $created = $this->decoratedRecordService->create($date, $customer, $deliveryReceiptDetails, $drNumber, $remarks, $user);
        $this->createAccountableEntry($created, $date, $created->getTotalAmount(), $user);
        return $created;
    }

    public function update(DeliveryReceipt $deliveryReceipt, DateTime $date, array $deliveryReceiptDetails, $drNumber, $remarks = null, User $user = null)
    {
        $updated = $this->decoratedRecordService->update($deliveryReceipt, $deliveryReceiptDetails, $drNumber, $remarks, $user);
        $this->updateAccountableEntries($updated, $date, $updated->getTotalAmount(), $user);
        return $updated;
    }

    public function delete(DeliveryReceipt $deliveryReceipt)
    {
        $this->deleteAccountableEntries($deliveryReceipt);
        return $this->decoratedRecordService->delete($deliveryReceipt);
    }

    public function updateBalanceAmountsFromInboundPayment(InboundPayment $inboundPayment)
    {
        return $this->decoratedRecordService->updateBalanceAmountsFromInboundPayment($inboundPayment);
    }
}
