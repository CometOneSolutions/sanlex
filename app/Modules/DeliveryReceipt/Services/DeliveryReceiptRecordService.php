<?php

namespace App\Modules\DeliveryReceipt\Services;

use App\Modules\Customer\Models\Customer;
use App\Modules\DeliveryReceipt\Models\DeliveryReceipt;
use CometOneSolutions\Common\Services\RecordService;
use \DateTime;
use CometOneSolutions\Auth\Models\Users\User;

interface DeliveryReceiptRecordService extends RecordService
{
    public function create(
        DateTime $date,
        Customer $customer,
        array $deliveryReceiptDetails,
        $drNumber,
        $remarks = null,
        $status,
        User $user = null
    );

    public function update(
        DeliveryReceipt $deliveryReceipt,
        DateTime $date,
        array $deliveryReceiptDetails,
        $drNumber,
        $remarks = null,
        User $user = null
    );

    public function delete(DeliveryReceipt $deliveryReceipt);
}
