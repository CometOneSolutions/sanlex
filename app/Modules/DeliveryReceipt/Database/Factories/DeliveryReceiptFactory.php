<?php

use Faker\Generator as Faker;

$factory->define(Sanlex\DeliveryReceiptModel::class, function (Faker $faker) {
    return [
        'date' => $faker->date,
        'remarks' => $faker->sentence,
        'customer_id' => function () {
            return factory(Sanlex\CustomerModel::class)->create()->id;
        },
    ];
});
