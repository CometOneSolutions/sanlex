<?php

use Faker\Generator as Faker;

$factory->define(Sanlex\DeliveryReceiptDetailModel::class, function (Faker $faker) {
    return [
        'delivery_receipt_id' => function () {
            return factory(Sanlex\DeliveryReceiptModel::class)->create()->id;
        },
        'unit_price' => 200,
        'quantity' => 200,
    ];
});
