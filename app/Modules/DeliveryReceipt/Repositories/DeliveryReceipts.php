<?php

namespace App\Modules\DeliveryReceipt\Repositories;

use App\Modules\DeliveryReceipt\Models\DeliveryReceipt;
use CometOneSolutions\Common\Repositories\Repository;

interface DeliveryReceipts extends Repository
{
    public function save(DeliveryReceipt $deliveryReceipt);

    public function delete(DeliveryReceipt $deliveryReceipt);
}
