<?php

namespace App\Modules\DeliveryReceipt\Repositories;

use App\Modules\DeliveryReceipt\Models\DeliveryReceipt;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentDeliveryReceipts extends EloquentRepository implements DeliveryReceipts
{
    protected $deliveryReceipt;

    public function __construct(DeliveryReceipt $deliveryReceipt)
    {
        parent::__construct($deliveryReceipt);
        $this->deliveryReceipt = $deliveryReceipt;
    }

    protected function filterByCustomer($value)
    {
        return $this->model->whereHas('customer', function ($customer) use ($value) {
            return $customer->where('name', 'LIKE', $value);
        });
    }

    protected function orderByCustomerName($direction)
    {
        return $this->model->join('customers', 'customers.id', 'delivery_receipts.customer_id')
            ->select('customers.name as customer_name', 'delivery_receipts.*')
            ->orderBy('customer_name', $direction);
    }

    protected function filterByNotReferencedByInboundPaymentId($inboundPaymentId)
    {
        return $this->model->whereDoesntHave('deliveryReceiptPayments', function ($deliveryReceiptPayment) use ($inboundPaymentId) {
            return $deliveryReceiptPayment->whereInboundPaymentId($inboundPaymentId);
        });
    }

    public function save(DeliveryReceipt $deliveryReceipt)
    {
        $result = $deliveryReceipt->save();
        $deliveryReceipt->deliveryReceiptDetails()->sync($deliveryReceipt->getDeliveryReceiptDetails());
        return $result;
    }

    public function delete(DeliveryReceipt $deliveryReceipt)
    {
        return $deliveryReceipt->delete();
    }
}
