<?php

Route::group(['prefix' => 'out'], function () {
    Route::get('/', 'OutMenuDashboardController@dashboard')->name('out-menu-dashboard_index');

    Route::group(['prefix' => 'delivery-receipts'], function () {
        Route::get('/', 'DeliveryReceiptController@index')->name('delivery-receipt_index');
        Route::get('/export', 'DeliveryReceiptController@export')->name('delivery-receipt_export');
        Route::get('/create', 'DeliveryReceiptController@create')->name('delivery-receipt_create');
        Route::get('{deliveryReceiptId}/edit', 'DeliveryReceiptController@edit')->name('delivery-receipt_edit');
        Route::get('{deliveryReceiptId}/print', 'DeliveryReceiptController@print')->name('delivery-receipt_print');
        Route::get('{deliveryReceiptId}', 'DeliveryReceiptController@show')->name('delivery-receipt_show');
    });
});
