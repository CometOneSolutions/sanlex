<?php

Breadcrumbs::register('delivery-receipt.index', function ($breadcrumbs) {
    $breadcrumbs->parent('out-menu-dashboard.index');
    $breadcrumbs->push('Delivery Receipt', route('delivery-receipt_index'));
});

Breadcrumbs::register('delivery-receipt.create', function ($breadcrumbs) {
    $breadcrumbs->parent('delivery-receipt.index');
    $breadcrumbs->push('Create', route('delivery-receipt_create'));
});

Breadcrumbs::register('delivery-receipt.show', function ($breadcrumbs, $deliveryReceipt) {
    $breadcrumbs->parent('delivery-receipt.index');
    $breadcrumbs->push($deliveryReceipt->getDrNumber(), route('delivery-receipt_show', $deliveryReceipt->getId()));
});

Breadcrumbs::register('delivery-receipt.edit', function ($breadcrumbs, $deliveryReceipt) {
    $breadcrumbs->parent('delivery-receipt.show', $deliveryReceipt);
    $breadcrumbs->push('Edit', route('delivery-receipt_edit', $deliveryReceipt->getId()));
});
