<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'delivery-receipts'], function () use ($api) {
        $api->get('/', 'App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptApiController@index');
        $api->get('{deliveryReceiptId}', 'App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptApiController@show');
        $api->post('/', 'App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptApiController@store');
        $api->post('approve/all', 'App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptApiController@approveAll');
        $api->patch('approve/{deliveryReceiptId}', 'App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptApiController@approve');
        $api->patch('{deliveryReceiptId}', 'App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptApiController@update');
        $api->delete('{deliveryReceiptId}', 'App\Modules\DeliveryReceipt\Controllers\DeliveryReceiptApiController@destroy');
    });
    // $api->group(['prefix' => 'deliveryReceipt-details'], function () use ($api) {
    //     $api->post('/', 'App\Http\Controllers\DeliveryReceiptApiController@store');
    //     $api->delete('{deliveryReceiptsId}', 'App\Http\Controllers\DeliveryReceiptApiController@destroy');
    //     $api->get('/', 'App\Http\Controllers\DeliveryReceiptApiController@index');
    //     $api->get('{deliveryReceiptId}', 'App\Http\Controllers\DeliveryReceiptApiController@show');
    // });
});
