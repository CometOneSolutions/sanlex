<?php

namespace App\Modules\DeliveryReceipt\Providers;

use App\Modules\DeliveryReceipt\Models\DeliveryReceipt;
use App\Modules\DeliveryReceipt\Models\DeliveryReceiptModel;
use App\Modules\DeliveryReceipt\Repositories\DeliveryReceipts;
use App\Modules\DeliveryReceipt\Repositories\EloquentDeliveryReceipts;
use App\Modules\DeliveryReceipt\Services\DeliveryReceiptRecordService;
use App\Modules\DeliveryReceipt\Services\DeliveryReceiptRecordServiceImpl;
use App\Modules\DeliveryReceipt\Services\InventoryDeliveryReceiptRecordServiceImpl;
use App\Modules\DeliveryReceiptDetails\Services\DeliveryReceiptDetailRecordService;
use CometOneSolutions\Inventory\Services\ItemMovement\ItemMovementRecordService;
use CometOneSolutions\Inventory\Services\Location\LocationRecordService;
use Illuminate\Support\ServiceProvider;


class DeliveryReceiptServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DeliveryReceipts::class, EloquentDeliveryReceipts::class);
        $this->app->bind(DeliveryReceipt::class, DeliveryReceiptModel::class);
        $this->app->bind(DeliveryReceiptRecordService::class, DeliveryReceiptRecordServiceImpl::class);
        $this->app->singleton(DeliveryReceiptRecordService::class, function ($app) {
            $deliveryReceiptRecordServiceImpl = $app->make(DeliveryReceiptRecordServiceImpl::class);
            $inventoryRecordServiceImpl = new InventoryDeliveryReceiptRecordServiceImpl(
                $deliveryReceiptRecordServiceImpl,
                $app->make(DeliveryReceiptDetailRecordService::class),
                $app->make(ItemMovementRecordService::class),
                $app->make(LocationRecordService::class)
            );
            return $inventoryRecordServiceImpl;
        });
    }
}
