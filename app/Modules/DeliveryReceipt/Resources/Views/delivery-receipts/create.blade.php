@extends('app')
@section('breadcrumbs', Breadcrumbs::render('delivery-receipt.create'))
@section('content')
<section id="delivery-receipt" v-cloak>
    <div class="row">
        <div class="col-12">
            <div class="card" id="customer">
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Create Delivery Receipt
                    </div>
                    <v-form @validate="store">
                        <div class="card-body">
                            @include('delivery-receipts._form')
                        </div>

                        <!-- <div class="card-footer">
                        <div class="text-right">
                            SUBTOTAL: P @{{ form.subtotal }}
                        </div>
                        
                        <div class="text-right">
                            TOTAL: <strong>P @{{ form.total }}</strong>
                        </div>
                    </div> -->

                        <div class="card-footer">
                            <div class="text-right">
                                <span v-if="isValid">
                                    <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                                </span>
                                <span class="text-danger" v-else>
                                    Please verify the form details.
                                </span>
                            </div>
                        </div>
                    </v-form>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection