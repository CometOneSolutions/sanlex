@extends('app')
@section('breadcrumbs', Breadcrumbs::render('delivery-receipt.edit', $deliveryReceipt))
@section('content')
<section id="delivery-receipt">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        <b>{{$deliveryReceipt->getDrNumber()}}</b>
                        @if(auth()->user()->can('Delete [OUT] Delivery Receipt'))
                            <div class="float-right">
                                <delete-button :is-busy="form.isBusy" :is-deleting="form.isDeleting" @destroy="destroy">
                                </delete-button>
                            </div>
                        @endif
                    </div>
                    <v-form @validate="update">
                        <div class="card-body">
                            @include('delivery-receipts._form')
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <span v-if="isValid">
                                    <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                                </span>
                                <span class="text-danger" v-else>
                                    Please verify the form details.
                                </span>
                            </div>
                        </div>
                    </v-form>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection