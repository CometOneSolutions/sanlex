@extends('app')
@section('breadcrumbs', Breadcrumbs::render('delivery-receipt.report'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Reports
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending"
                v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('dr_number')">
                                    DR No. <i class="fa" :class="getSortIcon('dr_number')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('date')">
                                    Date <small>(YYYY/MM/DD)</small> <i class="fa" :class="getSortIcon('date')"></i>
                                </a>
                            </th>
                            <th>Customer</th>
                            <th class="text-right">Total</th>
                            <th class="text-right">Receivable</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{ item.drNumber}}</a></td>
                            <td>@{{ item.date}}</td>
                            <td>@{{ item.customer.data.name}}</td>
                            <td class="text-right">@{{ item.totalAmount | numeric}}</td>
                            <td class="text-right">@{{ item.balanceAmount | numeric }}</td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>
<!-- End Watchlist-->
@endsection
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush