<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Delivery Receipt</title>
    <link href="{{ asset('css/delivery-receipt-page-view.css') }}" rel="stylesheet">
    <link href="{{ asset('css/delivery-receipt-print-view.css') }}" media="print" rel="stylesheet">



</head>

<body>

    <br><br>
    <section id="customerHeaderInfo">
        <center>
            <h2>LOGO GOES HERE</h2>
            <span class="uppercaseLetters" style="font-size: 30px; font-family: 'Oswald', sans-serif;">COMPANY
                NAME</span><br>
            <span>{{$customer->getMainAddress()->getStreet(), $customer->getMainAddress()->getCity(), $customer->getMainAddress()->getZip()}}</span><br>
            <span>{{$customer->getTelephone()}} / {{$customer->getFax()}}</span>
        </center>
    </section>
    <br><br><br>
    <section id="subCustomerHeaderInfo">
        <center>
            <h4 style="margin-bottom: 60px">DELIVERY RECEIPT</h4>
        </center>
        <p class="float-left">DR #: <strong>{{$deliveryReceipt->getDrNumber()}}</strong></p>
        <p class="float-right">Date: <strong>{{$deliveryReceipt->getDate()->toFormattedDateString()}}</strong></p>
        <div class="clearfix"></div>
        <span><strong>{{$customer->getName()}}</strong></span><br>
        <span><strong>{{$customer->getDeliveryAddress()->getStreet()}},
                {{$customer->getDeliveryAddress()->getCity()}}</strong> </span>
    </section><br>
    <section id="deliveryReceiptDetailTable">
        <center>
            <table border=1 cellpadding=3 style="border-collapse: collapse; width: 100%">
                <thead>
                    <tr>
                        <th style="text-align: center">#</th>
                        <th style="text-align: left">Item</th>
                        <th style="text-align: center">Qty</th>
                        <th style="text-align: left">Unit Price</th>
                        <th style="text-align: right">Line Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($deliveryReceiptDetails as $key => $deliveryReceiptDetail)
                    <tr style="text-align:center">
                        <td style="align-center">{{++$key}}</td>
                        <td style="text-align: left">{{$deliveryReceiptDetail->getProduct()->getName()}}</td>
                        <td style="text-align: center">{{$deliveryReceiptDetail->getQuantity()}}</td>
                        <td style="text-align: left">{{number_format($deliveryReceiptDetail->getUnitPrice(),2)}}</td>
                        <td style="text-align: right">{{number_format($deliveryReceiptDetail->getLineTotal(),2)}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="5"><span style="float:right"><strong>TOTAL:
                                    {{ number_format($deliveryReceipt->getTotalAmount(),2) }}
                                </strong></span></td>
                    </tr>

                </tbody>

            </table>
        </center>
    </section>



</body>

</html>