@extends('app')
@section('breadcrumbs', Breadcrumbs::render('delivery-receipt.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Delivery Receipt
            @if(auth()->user()->can('Create [OUT] Delivery Receipt'))
            <div class="float-right">
                @if(auth()->user()->hasRole('ADMIN'))
                <button v-if="items.length > 0" type="button" class="btn btn-warning btn-sm" @click="approveAllDr" :disabled="form.isBusy"><i class="fas fa-check-double"></i> </button>
                @endif
                <a href="{{route('delivery-receipt_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
            </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending" v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('dr_number')">
                                    DR No. <i class="fa" :class="getSortIcon('dr_number')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('date')">
                                    Date <i class="fa" :class="getSortIcon('date')"></i>
                                </a>
                            </th>
                            <th class="text-center">
                                <a v-on:click="setSorter('status')">
                                    Status <i class="fa" :class="getSortIcon('status')"></i>
                                </a>
                            </th>
                            <th>
                                <a v-on:click="setSorter('customer-name')">
                                    Customer <i class="fa" :class="getSortIcon('customer-name')"></i>
                                </a>
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{ item.drNumber}}</a></td>
                            <td>@{{ item.date}}</td>
                            <td class="text-center"><span :class="item.badgeClass">@{{item.status}}</span></td>
                            <td>@{{ item.customer.data.name}}</td>

                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>
<!-- End Watchlist-->
@endsection
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush
