<div class="row">
    <div class="col-4">
        <div class="form-group">
            <label for="drNumber"><strong>DR No.</strong></label>
            <p>@{{form.drNumber}}</p>

        </div>
    </div>

    <div class="col-4">
        <div class="form-group">
            <label for="date"><strong>Date</strong></label>
            <p>@{{form.date}}</p>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="exampleInputEmail1"><strong>Customer</strong></label>
            <p>@{{form.customer.data.name}}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="exampleInputEmail1"><strong>Remarks</strong></label>
            <p>@{{form.remarks}}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in form.deliveryReceiptDetails.data">
                    <td>@{{item.inventoryDetail.data.name}}</td>
                    <td>@{{item.orderedQuantity | numeric(3) }}</td>

                </tr>
            </tbody>
        </table>
    </div>
    
</div>


@push('scripts')
<script src="{{ mix('js/delivery-receipt.js') }}"></script>
@endpush