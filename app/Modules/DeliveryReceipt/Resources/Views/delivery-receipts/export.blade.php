<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Date</th>
            <th>Delivery Receipt Number</th>
            <th>Customer</th>
            <th>Balance Amount</th>
            <th>Total Amount</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $deliveryReceipt)
        <tr>
            <td>{{ $deliveryReceipt->getId() }}</td>
            <td>{{ $deliveryReceipt->getDate() }}</td>
            <td>{{ $deliveryReceipt->getDrNumber() }}</td>
            <td>{{ $deliveryReceipt->getCustomer()->getName() }}</td>
            <td>{{ $deliveryReceipt->getBalanceAmount() }}</td>
            <td>{{ $deliveryReceipt->getTotalAmount() }}</td>
        </tr>
    @endforeach
    </tbody>
</table>