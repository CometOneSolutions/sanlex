@extends('app')
@section('breadcrumbs', Breadcrumbs::render('delivery-receipt.show', $deliveryReceipt))
@section('content')
<section id="delivery-receipt">
    <div class="card" v-cloak>
        <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
            <div class="col-12 text-center h4">
                <i class="fas fa-cog fa-spin"></i> Initializing
            </div>
        </div>
        <div v-else>
            <div class="card-header">
                {{$deliveryReceipt->getDrNumber()}}
                <div class="float-right" v-if="form.isDraft">
                    @if(auth()->user()->can('Approve [OUT] Delivery Receipt') && auth()->user()->hasRole('ADMIN'))
                    <button type="button" @click="approve" class="btn btn-warning btn-sm" :disabled="form.isBusy"><i class="far fa-thumbs-up"></i>
                        Approve</button>
                    @endif
                    @if(auth()->user()->can('Update [OUT] Delivery Receipt'))
                    <a :href="form.editUri" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                    @endif
                </div>
            </div>

            <div class="card-body">
                @include('delivery-receipts._list')
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <timestamp :name="form.updatedByUser.data.name" :time="form.updatedAt"></timestamp>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Watchlist-->
@endsection
