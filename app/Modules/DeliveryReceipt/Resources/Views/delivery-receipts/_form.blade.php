<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="drNumber">DR No. <span class="text text-danger">*</span></label>
            <input type="text" class="form-control" id="drNumber" name="drNumber" aria-describedby="drNumberHelp" placeholder="DR No." v-model="form.drNumber">
        </div>
    </div>
    <!-- <div class="col-4">
        <div class="form-group">
            <label for="inputEmail4">Status</label>
            <select class="form-control select2-single" name="status" id="status" v-model="form.status" disabled>
                <option value="" disabled>Please Select</option>
                <option value="Draft">Draft</option>
                <option value="Approved">Approved</option>
            </select>
        </div>
    </div> -->
    <div class="col-6">
        <div class="form-group">
            <label for="date">Date <span class="text text-danger">*</span></label>
            <datepicker :typeable="true" :input-class="{'is-invalid': form.errors.has('date'),'form-control': true}" v-model="form.date" :required="true"></datepicker>
        </div>
    </div>


</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Customer <span class="text text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter customer" v-model="form.customerId" :selected="defaultSelectedCustomer" :url="url" search="name">
            </select3>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Remarks</label>
            <input type="text" class="form-control" id="remarks" name="remarks" aria-describedby="remarksHelp" placeholder="" v-model="form.remarks">
        </div>
    </div>
</div>

<div class="form-check">
    <input class="form-check-input" style="transform: scale(1.5)" type="checkbox" id="coilToggle" v-model="isCoil" @change="loadCoils">
    <label class="form-check-label" for="coilToggle">
        Show Coils
    </label>
</div>
<br>
<table class="table">
    <thead>
        <tr class="row">
            <th class="col-7">Product</th>
            <th class="col-2 text-right">Balance</th>
            <th class="col-2 text-right">Quantity</th>
            <th class="col-1"></th>
        </tr>
    </thead>
    <tbody>
        <tr is="deliveryReceipt-detail" class="row" v-for="(detail, index) in form.deliveryReceiptDetails.data" :key="detail.id" :detail="detail" :inventory-selections="inventorySelections" v-on:remove="form.deliveryReceiptDetails.data.splice(index, 1)">
        </tr>

        <tr class="row">
            <td colspan="4" class="col-12">
                <button type="button" class="btn btn-outline-success btn-sm" title="add New?" @click="addDeliveryReceiptDetail" :disabled="!inventorySelectionsInitialized && !initialInventorySelectionsInitialized">Add
                    New</button>
            </td>
        </tr>

    </tbody>
    <!-- <tfoot>
        <tr class="d-flex">
            <td colspan="3" class="col-9 text-right">TOTAL</td>
            <td class="col-2 text-right">@{{ totalAmount | numeric }}</td>
            <td class="col-1"></td>
        <tr>
    </tfoot> -->
</table>

@push('scripts')
<script src="{{ mix('js/delivery-receipt.js') }}"></script>
@endpush