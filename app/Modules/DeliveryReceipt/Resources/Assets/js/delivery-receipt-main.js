import { Form } from "@c1_common_js/components/Form";

export const deliveryReceiptService = new Vue({
    data: function() {
        return {
            form: new Form({}),
            uri: {
                deliveryReceipt: "/api/delivery-receipts",
            }
        };
    },

    methods: {
        getCustomerDeliveryReceiptWithBalanceExcludingInboundPayment(customerId, inboundPaymentId) {
            return this.form.get(
                `${this.uri.deliveryReceipt}?customer_id=${customerId}&balance_amount>0&not_referenced_by_inbound_payment_id=${inboundPaymentId}`
            );
        },
    }

});