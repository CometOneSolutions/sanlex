import { Form } from "@c1_common_js/components/Form";
import VForm from "@c1_common_js/components/VForm";
import SaveButton from "@c1_common_js/components/SaveButton";
import DeleteButton from "@c1_common_js/components/DeleteButton";
import Timestamp from "@c1_common_js/components/Timestamp";
import DeliveryReceiptDetail from "@c1_module_js/DeliveryReceiptDetails/Resources/Assets/components/DeliveryReceiptDetail";
import Datepicker from 'vuejs-datepicker';
import Select3 from "@c1_common_js/components/Select3";
import { inventoryDetailService } from "@c1_module_js/InventoryDetail/Resources/Assets/js/inventory-detail-main";


new Vue({
    el: "#delivery-receipt",

    components: {
        SaveButton,
        DeleteButton,
        Timestamp,
        DeliveryReceiptDetail,
        Select3,
        Datepicker,
        VForm,
        inventoryDetailService
    },

    data: {

        url: '/api/customers',
        form: new Form({
            drNumber: null,
            date: moment(),
            customerId: "",
            status: 'Draft',
            remarks: null,
            deliveryReceiptDetails: {
                data: []
            }
        }),
        counter: 0,
        isCoil: false,
        dataInitialized: true,
        defaultSelectedCustomer: {},
        inventorySelections: {},
        inventorySelectionsInitialized: false,
        initialInventorySelectionsInitialized: false,
    },

    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },

    computed: {
        initializationComplete() {
            return (
                this.dataInitialized && this.initialInventorySelectionsInitialized
            );
        },
        isValid() {
            let formIsValid = true;
            if (this.form.deliveryReceiptDetails.data.length == 0) {
                formIsValid = false;
            }
            this.form.deliveryReceiptDetails.data.forEach(detail => {
                if (detail.isNotValid || detail.inventoryDetailId == null || detail.orderedQuantity <=
                    0) {
                    formIsValid = false;
                }
            });
            return formIsValid;
        }

    },
    methods: {
        loadCoils() {
            this.form.deliveryReceiptDetails.data = [];
            if (this.isCoil) {
                this.inventorySelectionsInitialized = false;
                this.inventorySelections = {};
                inventoryDetailService.getInventoryDetailsWithCoils().then(response => {
                    this.inventorySelections = response.data;
                    this.inventorySelectionsInitialized = true;
                });
            } else {
                this.inventorySelectionsInitialized = false;
                this.inventorySelections = {};
                inventoryDetailService.getInventoryDetailsWhereProductsAreAccessory().then(response => {
                    this.inventorySelections = response.data;
                    this.inventorySelectionsInitialized = true;
                });
            }
        },
        addDeliveryReceiptDetail() {
            this.form.deliveryReceiptDetails.data.push({
                id: --this.counter,
                inventoryDetailId: null,
                balance: 0,
                isNotValid: false,
                unit: '',
                orderedQuantity: 0,
            });
        },

        destroy() {
            this.form.confirm().then((result) => {
                if (result.value) {
                    this.form.delete('/api/delivery-receipts/' + this.form.id).then(response => {
                        this.$swal({
                            title: 'Success',
                            text: 'Delivery receipt was successfully deleted.',
                            type: 'success'
                        }).then(() => window.location = '/out/delivery-receipts/');
                    });
                }
            });
        },

        approve() {
            this.form.affirm().then((result) => {
                if (result.value) {
                    this.form.patch('/api/delivery-receipts/approve/' + id).then(
                        response => {
                            this.$swal({
                                title: 'Success',
                                text: 'Delivery receipt is approved.',
                                type: 'success'
                            }).then(() => window.location = '/out/delivery-receipts/' +
                                id);
                        })
                }
            });
        },

        store() {
            this.form.post("/api/delivery-receipts").then(response => {

                this.$swal({
                    title: "Success",
                    text: "Delivery receipt was successfully saved.",
                    type: "success"
                });
                this.form.reset();
            });
        },

        update() {
            this.form.patch("/api/delivery-receipts/" + this.form.id).then(response => {
                this.$swal({
                    title: "Success",
                    text: "Delivery receipt was successfully updated.",
                    type: "success"
                }).then(() => (window.location = "/out/delivery-receipts/" + id));
            });
        },

        loadData(data) {
            this.form = new Form(data);
        }
    },

    created() {

        inventoryDetailService.getInventoryDetailsWhereProductsAreAccessory().then(response => {
            this.inventorySelections = response.data;
            this.initialInventorySelectionsInitialized = true;
        });
        if (id != null) {
            this.dataInitialized = false;
            this.form
                .get(
                    "/api/delivery-receipts/" + id + "?include=customer,deliveryReceiptDetails.inventoryDetail"
                ).then(response => {
                    this.loadData(response.data);
                    this.defaultSelectedCustomer = {
                        text: response.data.customer.data.name,
                        id: response.data.customerId,
                    };
                    this.dataInitialized = true;
                });
        }
    },
});