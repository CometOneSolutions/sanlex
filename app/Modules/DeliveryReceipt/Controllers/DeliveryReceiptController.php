<?php

namespace App\Modules\DeliveryReceipt\Controllers;

use JavaScript;
use App\Modules\DeliveryReceipt\Services\DeliveryReceiptRecordService;
use CometOneSolutions\Common\Controllers\ExportableController;

class DeliveryReceiptController extends ExportableController
{
    private $deliveryReceiptRecordService;
    protected $exportView = 'delivery-receipt.export';

    public function __construct(DeliveryReceiptRecordService $deliveryReceiptRecordService)
    {
        $this->middleware('auth');
        parent::__construct($deliveryReceiptRecordService);
        $this->deliveryReceiptRecordService = $deliveryReceiptRecordService;
        $this->middleware('permission:Read [OUT] Delivery Receipt')->only(['show', 'index', 'indexPending']);
        $this->middleware('permission:Create [OUT] Delivery Receipt')->only('create');
        $this->middleware('permission:Update [OUT] Delivery Receipt')->only('edit');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        JavaScript::put([
            'filterable' => $this->deliveryReceiptRecordService->getAvailableFilters(),
            'sorter' => 'status,-created_at',
            'sortAscending' => false,
            'baseUrl' => '/api/delivery-receipts?include=customer',
            'exportBaseUrl' => '/delivery-receipts'
        ]);
        return view('delivery-receipts.index');
    }

    public function indexPending()
    {
        JavaScript::put([
            'filterable' => $this->deliveryReceiptRecordService->getAvailableFilters(),
            'sorter' => 'status,-created_at',
            'sortAscending' => true,
            'baseUrl' => '/api/delivery-receipts?status=Draft&include=customer',
            'exportBaseUrl' => '/delivery-receipts'
        ]);
        return view('delivery-receipts.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('delivery-receipts.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put(
            ['id' => $id]
        );
        $deliveryReceipt = $this->deliveryReceiptRecordService->getById($id);
        return view('delivery-receipts.show', compact('deliveryReceipt'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        JavaScript::put(
            ['id' => $id]
        );
        $deliveryReceipt = $this->deliveryReceiptRecordService->getById($id);
        return view('delivery-receipts.edit', compact('deliveryReceipt'));
    }

    public function print($id)
    {
        $deliveryReceipt = $this->deliveryReceiptRecordService->getById($id);
        $deliveryReceiptDetails = $deliveryReceipt->getDeliveryReceiptDetails();
        $customer = $deliveryReceipt->getCustomer();

        return view('delivery-receipts.print', compact('deliveryReceipt', 'customer', 'deliveryReceiptDetails'));
    }
}
