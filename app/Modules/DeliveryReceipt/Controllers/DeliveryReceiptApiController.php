<?php

namespace App\Modules\DeliveryReceipt\Controllers;

use App\Modules\DeliveryReceipt\Services\DeliveryReceiptRecordService;
use App\Modules\DeliveryReceipt\Transformers\DeliveryReceiptTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\DeliveryReceipt\Requests\StoreDeliveryReceipt;
use App\Modules\DeliveryReceipt\Requests\UpdateDeliveryReceipt;
use App\Modules\DeliveryReceipt\Requests\ApproveDeliveryReceipt;

class DeliveryReceiptApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        DeliveryReceiptRecordService $service,
        DeliveryReceiptTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($service, $transformer);
        $this->service = $service;
        $this->transformer = $transformer;
    }

    public function store(StoreDeliveryReceipt $request)
    {
        $deliveryReceipt = \DB::transaction(function () use ($request) {
            return $this->service->create(
                $request->getDate(),
                $request->getCustomer(),
                $request->getDeliveryReceiptDetails(),
                $request->getDrNumber(),
                $request->getRemarks(),
                $request->getStatus(),
                $request->user()
            );
        });

        return $this->response->item($deliveryReceipt, $this->transformer)->setStatusCode(201);
    }

    public function update($deliveryReceiptId, UpdateDeliveryReceipt $request)
    {
        $deliveryReceipt = \DB::transaction(function () use ($request, $deliveryReceiptId) {
            $deliveryReceipt = $this->service->getById($deliveryReceiptId);
            return $this->service->update(
                $deliveryReceipt,
                $request->getDate(),
                $request->getDeliveryReceiptDetails(),
                $request->getDrNumber(),
                $request->getRemarks(),
                $request->user()
            );
        });

        return $this->response->item($deliveryReceipt, $this->transformer)->setStatusCode(200);
    }

    public function approve($deliveryReceiptId, ApproveDeliveryReceipt $request)
    {
        $deliveryReceipt = \DB::transaction(function () use ($deliveryReceiptId) {
            $deliveryReceipt = $this->service->getById($deliveryReceiptId);
            return $this->service->approve(
                $deliveryReceipt
            );
        });

        return $this->response->item($deliveryReceipt, $this->transformer)->setStatusCode(200);
    }
    public function approveAll()
    {
        $data = $this->service->approveAll();
        return $this->response->array(['message' => $data['response']])->setStatusCode(200);;
    }
    public function destroy($deliveryReceiptId)
    {
        $deliveryReceipt = \DB::transaction(function () use ($deliveryReceiptId) {
            $deliveryReceipt = $this->service->getById($deliveryReceiptId);
            $this->service->delete($deliveryReceipt);
            return $deliveryReceipt;
        });
        return $this->response->item($deliveryReceipt, $this->transformer)->setStatusCode(200);
    }
}
