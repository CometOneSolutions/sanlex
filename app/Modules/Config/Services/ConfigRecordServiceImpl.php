<?php

namespace App\Modules\Config\Services;

use CometOneSolutions\Common\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Filter;
use CometOneSolutions\Accounting\CurrencyRecordService;
use CometOneSolutions\Common\GeneralApiExceptionWhereMessageIsArray;

class ConfigRecordServiceImpl extends RecordServiceImpl implements ConfigRecordService
{
    private $configs;
    private $config;
    private $currencyRecordService;

    public function __construct(
        ConfigRepository $configRepository,
        Config $config,
        CurrencyRecordService $currencyRecordService
    ) {
        parent::__construct($configRepository);
        $this->config = $config;
        $this->configs = $configRepository;
        $this->currencyRecordService = $currencyRecordService;
    }

    public function getConfig()
    {
        if ($this->configs->getAll()->count() > 0) {
            return $this->configs->getFirst();
        } else {
            $newConfig = new $this->config;
            $newConfig->setUsdPhpRate(0.00);
            $this->configs->save($newConfig);
            return $newConfig;
        }
    }

    public function update(
        Config $config,
        $usdPhpRate
    ) {
        $tempConfig = clone $config;
        if ($usdPhpRate > 99.99) {
            throw new GeneralApiExceptionWhereMessageIsArray(['Rate should be between 0 - 99.99'], 'Oops', 400, true);
        } else {
            $tempConfig->setUsdPhpRate($usdPhpRate);
            $this->configs->save($tempConfig);
            return $this->getById($config->getId());
        }
    }

    public function computeRate(Config $config, CurrencyExchange $currencyExchange)
    {
        $usdFilter = new Filter('code', 'USD');
        $currency = $this->currencyRecordService->getFirst([$usdFilter]);
        $currency->load(['books.accounts.internalAccounts']);
        $newRate = $config->computeRate($currencyExchange, $this->getDollarsInHand($currency));

        return $newRate;
    }
    private function getDollarsInHand($currency)
    {
        $dollarsInHand = 0;
        foreach ($currency->getBooks() as $book) {
            foreach ($book->getAccounts() as $account) {
                foreach ($account->getInternalAccounts() as $internalAccount) {
                    $dollarsInHand += $internalAccount->getBalance();
                }
            }
        }
        return $dollarsInHand;
    }
}
