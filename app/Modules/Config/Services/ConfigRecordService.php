<?php

namespace App\Modules\Config\Services;

use CometOneSolutions\Common\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface ConfigRecordService extends RecordService
{
    public function getConfig();
    public function update(
        Config $config,
        $usdPhpRate
    );
    public function computeRate(Config $config, CurrencyExchange $currencyExchange);
}
