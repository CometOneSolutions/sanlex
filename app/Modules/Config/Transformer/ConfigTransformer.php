<?php

namespace App\Modules\Config\Transformer;

use League\Fractal;

use CometOneSolutions\Auth\UserTransformer;

class ConfigTransformer extends Fractal\TransformerAbstract
{
    // protected $availableIncludes = [''];

    public function transform(Config $config)
    {
        return [
            'id'                => (int)$config->getId(),
            'usdPhpRate'        => $config->getUsdPhpRate()
        ];

    }

    // public function includeInboundPayments(Customer $customer)
    // {
    //     $inboundPayments = $customer->getInboundPayments();
    //     return $this->collection($inboundPayments, new InboundPaymentTransformer);
    // }
    
}
