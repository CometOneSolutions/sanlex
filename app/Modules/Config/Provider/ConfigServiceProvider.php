<?php

namespace App\Modules\Config\Provider;

use Illuminate\Support\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ConfigRecordService::class, ConfigRecordServiceImpl::class);
        $this->app->bind(ConfigRepository::class, EloquentConfigRepository::class);
        $this->app->bind(Config::class, ConfigModel::class);
    }
}
