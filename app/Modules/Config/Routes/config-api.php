<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'configs'], function () use ($api) {
        $api->get('/', 'App\Http\Controllers\ConfigApiController@show');
        $api->patch('/', 'App\Http\Controllers\ConfigApiController@update');
    });
});

