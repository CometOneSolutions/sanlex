import {Form} from '@c1_common_js/components/Form';
import {serviceBus} from '@c1_common_js/main';
import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm';

new Vue({
    el: '#config',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, serviceBus
    },
    data: {
    
        form: new Form({
            usdPhpRate: '',
        }),
        isSaving: false,
        isBusy: false,
        isDeleting: false,
        isBusyDeleting: false,
        dataInitialized: true,

    },
    watch: {
        initializationComplete(val) { 
            this.form.isInitializing = false;
        } 
    },

    computed: {
        
        initializationComplete() { 
            if (this.dataInitialized) 
            {
                this.form.isInitializing = false;
                return true;
            }
            return false;
        },
    },
    methods: {

        update() {
            this.isSaving = true;
            this.isBusy = true;
            this.form.patch('/api/configs')
            .then(response => {
                this.isSaving = false;
                this.isBusy = false;
                this.$swal({
                    title: 'Success',
                    text: 'Default values updated.',
                    type: 'success',
                    confirmButtonColor: '#20a8d8',
                }).then(() => 
                window.location = '/master-file/configs'
            );
                
            })
            .catch(error => {
                this.isSaving = false;
                this.isBusy = false;
            });
        },

        loadData(data) {
            this.form = new Form(data);
            let id = data.id;
        },
    },
    created() {
        this.form.get('/api/configs')
        .then(response => {
            this.loadData(response.data);
        });
    },
    mounted() { 
        console.log("Init config script");
    }
});