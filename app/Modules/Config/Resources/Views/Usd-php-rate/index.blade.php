@extends('app')
@section('breadcrumbs', Breadcrumbs::render('default-config.edit'))
@section('content')
<section id="config" v-cloak>
    <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
            <div class="col-12 text-center h4">
                <i class="fas fa-cog fa-spin"></i> Initializing
            </div>
    </div>
        <div v-else>        
        <br/>
        <v-form @validate="update">
            <div class="card table-responsive">
                <div class="card-header">
                    Default Values

                </div>
                    <div class="card-body">
                        <div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
                            <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
                                    @{{ error[0] }}
                            </small>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="inputCustomer" class="col-form-label">USD-PHP Rate: <span class="text text-danger">*</span></label>
                                <money class="form-control" id="usdPhpRate" name="usdPhpRate" v-model.number="form.usdPhpRate" @focus="$event.target.select()" :class="{'is-invalid': form.errors.has('usdPhpRate') }" required data-msg-required="USD-PHP rate field is required"></money>
                            </div>
                        </div>                   
                    </div>
                <div class="card-footer">
                    <div class="float-right">
                        <button class="btn btn-primary btn-sm" type="submit" :disabled="isBusy">
                            <div v-if="isSaving">
                                <i class="fas fa-spinner fa-pulse"></i> Saving
                            </div>
                            <div v-if="!isSaving">
                                Save
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </v-form>
        </div>
    </section>

@endsection
@push('scripts')
<script src="{{ mix('js/config.js') }}"></script>
@endpush