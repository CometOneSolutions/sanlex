<?php

namespace App\Modules\Config\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use Illuminate\Http\Request;
use JavaScript;
use App\Modules\Config\Services\ConfigRecordService;

class ConfigController extends Controller
{
    private $service;

    public function __construct(ConfigRecordService $configRecordService)
    {
        $this->service = $configRecordService;
        $this->middleware('permission:Update Config')->only('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("usd-php-rate.index");
    }


}
