<?php

namespace App\Modules\Config\Controllers;

use Dingo\Api\Routing\Helpers;
use App\Modules\Config\Services\ConfigRecordService;
use App\Modules\Config\Transformer\ConfigTransformer;
use Illuminate\Http\Request;

class ConfigApiController
{
    use Helpers;

    protected $service;
    protected $configTransformer;

    public function __construct(
        ConfigRecordService $configRecordService,
        ConfigTransformer $configTransformer
    ) {
        $this->service = $configRecordService;
        $this->configTransformer = $configTransformer;
    }

    public function show()
    {
        $config = $this->service->getConfig();
        return $this->response->item($config, $this->configTransformer);
    }

    public function update(Request $request)
    {
        $config = $this->service->getConfig();
        $usdPhpRate = $request->input('usdPhpRate');

        $config = $this->service->update(
            $config,
            $usdPhpRate
        );
        return $this->response->item($config, $this->configTransformer);
    }
}
