<?php

namespace App\Modules\Config\Repositories;

use CometOneSolutions\Common\Repository;

interface ConfigRepository extends Repository
{
    public function save(Config $config);

    public function delete(Config $config);
}
