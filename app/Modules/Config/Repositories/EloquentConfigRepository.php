<?php

namespace App\Modules\Config\Repositories;

use CometOneSolutions\Common\EloquentRepository;

class EloquentConfigRepository extends EloquentRepository implements ConfigRepository
{
    public function __construct(Config $config)
    {
        parent::__construct($config);
    }

    public function save(Config $config)
    {
        return $config->save();
    }

    public function delete(Config $config)
    {
        return $config->delete();
    }
}
