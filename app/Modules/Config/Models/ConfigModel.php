<?php

namespace App\Modules\Config\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigModel extends Model implements Config
{
    protected $table = 'usd_php_rate_config';

    public function getId()
    {
        return $this->id;
    }

    public function setUsdPhpRate($rate)
    {
        $this->usd_php_rate = $rate;
        return $this;
    }

    public function getUsdPhpRate()
    {
        return $this->usd_php_rate;
    }
    public function computeRate(CurrencyExchange $currencyExchange, $dollarsOnHand)
    {
        $oldRate = (float) $this->getUsdPhpRate();
        
        $newRate = round((((abs($dollarsOnHand - $currencyExchange->getDestinationAmount())) * $oldRate)
                        /
                        ($dollarsOnHand))
                            +
                                (($currencyExchange->getDestinationAmount() * $currencyExchange->getExchangeRate())
                                    /
                                    ($dollarsOnHand)), 2);
        return $newRate;
    }
}
