<?php

namespace App\Modules\Config\Models;

interface Config
{
    public function getId();
    public function getUsdPhpRate();
    public function setUsdPhpRate($rate);
//    public function computeRate(CurrencyExchange $currencyExchange, $dollarsOnHand);
}
