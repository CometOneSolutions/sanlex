<?php

namespace App\Modules\Brand\Provider;

use App\Modules\Brand\Models\Brand;
use Illuminate\Support\ServiceProvider;
use App\Modules\Brand\Models\BrandModel;
use Illuminate\Database\Eloquent\Factory;
use App\Modules\Brand\Services\BrandRecordService;
use App\Modules\Brand\Repositories\BrandRepository;
use App\Modules\Brand\Services\BrandRecordServiceImpl;
use App\Modules\Brand\Repositories\EloquentBrandRepository;

class BrandServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Brand/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(BrandRecordService::class, BrandRecordServiceImpl::class);
        $this->app->bind(BrandRepository::class, EloquentBrandRepository::class);
        $this->app->bind(Brand::class, BrandModel::class);
    }
}
