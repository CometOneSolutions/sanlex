<?php

namespace App\Modules\Brand\Repositories;

use App\Modules\Brand\Models\Brand;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentBrandRepository extends EloquentRepository implements BrandRepository
{
    public function __construct(Brand $brand)
    {
        parent::__construct($brand);
    }

    public function save(Brand $brand)
    {
        return $brand->save();
    }

    public function delete(Brand $brand)
    {
        return $brand->delete();
    }
}
