<?php

namespace App\Modules\Brand\Repositories;

use App\Modules\Brand\Models\Brand;
use CometOneSolutions\Common\Repositories\Repository;

interface BrandRepository extends Repository
{
    public function save(Brand $brand);

    public function delete(Brand $brand);
}
