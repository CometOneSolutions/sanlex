<?php

namespace App\Modules\Brand\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Brand\Services\BrandRecordService;

class BrandController extends Controller
{
    private $service;

    public function __construct(BrandRecordService $brandRecordService)
    {
        $this->service = $brandRecordService;
        $this->middleware('permission:Read [MAS] Brand')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Brand')->only('create');
        $this->middleware('permission:Update [MAS] Brand')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/brands'
        ]);
        return view('brands.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('brands.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $brand = $this->service->getById($id);
        return view('brands.show', compact('brand'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $brand = $this->service->getById($id);
        return view('brands.edit', compact('brand'));
    }
}
