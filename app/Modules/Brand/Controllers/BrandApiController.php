<?php

namespace App\Modules\Brand\Controllers;


use App\Modules\Brand\Services\BrandRecordService;
use App\Modules\Brand\Transformer\BrandTransformer;
use App\Modules\Brand\Requests\DestroyBrand;
use App\Modules\Brand\Requests\StoreBrand;
use App\Modules\Brand\Requests\UpdateBrand;
use CometOneSolutions\Common\Controllers\ResourceApiController;

class BrandApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        BrandRecordService $brandRecordService,
        BrandTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($brandRecordService, $transformer);
        $this->service = $brandRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreBrand $request)
    {
        $brand = \DB::transaction(function () use ($request) {
            $name = $request->getName();

            $user = $request->user();

            return $this->service->create(
                $name,
                $user
            );
        });
        return $this->response->item($brand, $this->transformer)->setStatusCode(201);
    }

    public function update($brandId, UpdateBrand $request)
    {
        $brand = \DB::transaction(function () use ($brandId, $request) {
            $brand = $this->service->getById($brandId);
            $name = $request->getName();

            $user = $request->user();

            return $this->service->update(
                $brand,
                $name,
                $user
            );
        });
        return $this->response->item($brand, $this->transformer)->setStatusCode(200);
    }

    public function destroy($brandId, DestroyBrand $request)
    {
        $brand = $this->service->getById($brandId);
        $this->service->delete($brand);
        return $this->response->item($brand, $this->transformer)->setStatusCode(200);
    }
}
