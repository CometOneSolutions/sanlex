<?php

namespace App\Modules\Brand\Services;

use App\Modules\Brand\Models\Brand;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordService;

interface BrandRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        Brand $brand,
        $name,
        User $user = null
    );

    public function delete(Brand $brand);
}
