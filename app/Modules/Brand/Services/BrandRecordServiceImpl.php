<?php

namespace App\Modules\Brand\Services;

use App\Modules\Brand\Models\Brand;
use App\Modules\Brand\Repositories\BrandRepository;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use App\Modules\Product\Traits\CanUpdateProductName;

class BrandRecordServiceImpl extends RecordServiceImpl implements BrandRecordService
{
    use CanUpdateProductName;

    private $brand;
    private $brands;

    protected $availableFilters = [
        ['id' => 'name', 'text' => 'Name']
    ];

    public function __construct(BrandRepository $brands, Brand $brand)
    {
        $this->brands = $brands;
        $this->brand = $brand;
        parent::__construct($brands);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $brand = new $this->brand;
        $brand->setName($name);

        if ($user) {
            $brand->setUpdatedByUser($user);
        }
        $this->brands->save($brand);
        return $brand;
    }

    public function update(
        Brand $brand,
        $name,
        User $user = null
    ) {
        $tempBrand = clone $brand;
        $tempBrand->setName($name);
        if ($user) {
            $tempBrand->setUpdatedByUser($user);
        }

        $this->brands->save($tempBrand);

        $this->updateProductableProductNames([
            $tempBrand->getPvcFittings(),
            $tempBrand->getPaintAdhesives()
        ]);

        return $tempBrand;
    }

    public function delete(Brand $brand)
    {
        $this->brands->delete($brand);
        return $brand;
    }
}
