<?php

namespace App\Modules\Brand\Requests;

class StoreBrand extends BrandRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Brand');
    }
}
