<?php

namespace App\Modules\Brand\Requests;

class DestroyBrand extends BrandRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Brand');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
