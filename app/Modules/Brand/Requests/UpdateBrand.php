<?php

namespace App\Modules\Brand\Requests;

class UpdateBrand extends BrandRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Brand');
    }
}
