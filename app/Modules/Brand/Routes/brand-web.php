<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'brands'], function () {
        Route::get('/', 'BrandController@index')->name('brand_index');
        Route::get('/create', 'BrandController@create')->name('brand_create');
        Route::get('{brandId}/edit', 'BrandController@edit')->name('brand_edit');
        // Route::get('{brandId}/print', 'BrandController@print')->name('brand_print');
        Route::get('{brandId}', 'BrandController@show')->name('brand_show');
    });
});
