<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'brands'], function () use ($api) {
        $api->get('/', 'App\Modules\Brand\Controllers\BrandApiController@index');
        $api->get('{brandId}', 'App\Modules\Brand\Controllers\BrandApiController@show');
        $api->post('/', 'App\Modules\Brand\Controllers\BrandApiController@store');
        $api->patch('{brandId}', 'App\Modules\Brand\Controllers\BrandApiController@update');
        $api->delete('{brandId}', 'App\Modules\Brand\Controllers\BrandApiController@destroy');
    });
});
