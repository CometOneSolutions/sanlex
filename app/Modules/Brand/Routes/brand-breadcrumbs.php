<?php

Breadcrumbs::register('brand.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Brands', route('brand_index'));
});
Breadcrumbs::register('brand.create', function ($breadcrumbs) {
    $breadcrumbs->parent('brand.index');
    $breadcrumbs->push('Create', route('brand_create'));
});
Breadcrumbs::register('brand.show', function ($breadcrumbs, $brand) {
    $breadcrumbs->parent('brand.index');
    $breadcrumbs->push($brand->getName(), route('brand_show', $brand->getId()));
});
Breadcrumbs::register('brand.edit', function ($breadcrumbs, $brand) {
    $breadcrumbs->parent('brand.show', $brand);
    $breadcrumbs->push('Edit', route('brand_edit', $brand->getId()));
});
