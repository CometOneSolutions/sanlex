<?php

namespace App\Modules\Brand\Models;

interface Brand
{
    public function getId();

    public function getName();

    public function setName($value);
}
