<?php

namespace App\Modules\Brand\Models;

use App\Modules\Color\Models\ColorModel;
use App\Modules\PaintAdhesive\Models\PaintAdhesiveModel;
use App\Modules\PvcFitting\Models\PvcFittingModel;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model implements Brand, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'brands';

    public function paintAdhesiveColors()
    {
        return $this->belongsToMany(ColorModel::class, 'paint_adhesives', 'brand_id', 'color_id');
    }

    public function getPaintAdhesiveColors()
    {
        return $this->paintAdhesiveColors;
    }

    public function pvcFittings()
    {
        return $this->hasMany(PvcFittingModel::class, 'brand_id');
    }

    public function paintAdhesives()
    {
        return $this->hasMany(PaintAdhesiveModel::class, 'brand_id');
    }

    public function getPvcFittings()
    {
        return $this->pvcFittings;
    }

    public function getPaintAdhesives()
    {
        return $this->paintAdhesives;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
