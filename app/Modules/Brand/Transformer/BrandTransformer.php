<?php

namespace App\Modules\Brand\Transformer;

use App\Modules\Brand\Models\Brand;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class BrandTransformer extends UpdatableByUserTransformer
{
    public function transform(Brand $brand)
    {
        return [
            'id' => (int) $brand->getId(),
            'text' => $brand->getName(),
            'name' => $brand->getName(),
            'updatedAt' => $brand->updated_at,
            'editUri' => route('brand_edit', $brand->getId()),
            'showUri' => route('brand_show', $brand->getId()),
        ];
    }
}
