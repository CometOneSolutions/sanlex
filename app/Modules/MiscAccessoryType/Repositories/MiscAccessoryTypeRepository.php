<?php

namespace App\Modules\MiscAccessoryType\Repositories;

use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use CometOneSolutions\Common\Repositories\Repository;

interface MiscAccessoryTypeRepository extends Repository
{
    public function save(MiscAccessoryType $miscAccessoryType);
    public function delete(MiscAccessoryType $miscAccessoryType);
}
