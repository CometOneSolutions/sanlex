<?php

namespace App\Modules\MiscAccessoryType\Repositories;

use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentMiscAccessoryTypeRepository extends EloquentRepository implements MiscAccessoryTypeRepository
{
    public function __construct(MiscAccessoryType $miscAccessoryType)
    {
        parent::__construct($miscAccessoryType);
    }

    public function save(MiscAccessoryType $miscAccessoryType)
    {
        return $miscAccessoryType->save();
    }

    public function delete(MiscAccessoryType $miscAccessoryType)
    {
        return $miscAccessoryType->delete();
    }
}
