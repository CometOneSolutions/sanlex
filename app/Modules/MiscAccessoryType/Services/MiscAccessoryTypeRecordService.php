<?php

namespace App\Modules\MiscAccessoryType\Services;

use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface MiscAccessoryTypeRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        MiscAccessoryType $miscAccessoryType,
        $name,
        User $user = null
    );

    public function delete(MiscAccessoryType $miscAccessoryType);
}
