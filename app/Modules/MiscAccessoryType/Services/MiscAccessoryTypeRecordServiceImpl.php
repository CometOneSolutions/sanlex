<?php

namespace App\Modules\MiscAccessoryType\Services;

use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use App\Modules\MiscAccessoryType\Repositories\MiscAccessoryTypeRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class MiscAccessoryTypeRecordServiceImpl extends RecordServiceImpl implements MiscAccessoryTypeRecordService
{

    use CanUpdateProductName;

    private $miscAccessoryType;
    private $miscAccessoryTypes;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(MiscAccessoryTypeRepository $miscAccessoryTypes, MiscAccessoryType $miscAccessoryType)
    {
        $this->screwTypes = $miscAccessoryTypes;
        $this->screwType = $miscAccessoryType;
        parent::__construct($miscAccessoryTypes);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $miscAccessoryType = new $this->screwType;
        $miscAccessoryType->setName($name);

        if ($user) {
            $miscAccessoryType->setUpdatedByUser($user);
        }
        $this->screwTypes->save($miscAccessoryType);
        return $miscAccessoryType;
    }

    public function update(
        MiscAccessoryType $miscAccessoryType,
        $name,
        User $user = null
    ) {
        $tempMiscAccessoryType = clone $miscAccessoryType;
        $tempMiscAccessoryType->setName($name);
        if ($user) {
            $tempMiscAccessoryType->setUpdatedByUser($user);
        }
        $this->screwTypes->save($tempMiscAccessoryType);

        $this->updateProductableProductNames([
            $tempMiscAccessoryType->getMiscAccessories()
        ]);

        return $tempMiscAccessoryType;
    }

    public function delete(MiscAccessoryType $miscAccessoryType)
    {
        $this->screwTypes->delete($miscAccessoryType);
        return $miscAccessoryType;
    }
}
