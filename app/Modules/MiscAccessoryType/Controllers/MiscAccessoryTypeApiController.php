<?php

namespace App\Modules\MiscAccessoryType\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\MiscAccessoryType\Services\MiscAccessoryTypeRecordService;
use App\Modules\MiscAccessoryType\Transformer\MiscAccessoryTypeTransformer;
use App\Modules\MiscAccessoryType\Requests\StoreMiscAccessoryType;
use App\Modules\MiscAccessoryType\Requests\UpdateMiscAccessoryType;
use App\Modules\MiscAccessoryType\Requests\DestroyMiscAccessoryType;

class MiscAccessoryTypeApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        MiscAccessoryTypeRecordService $miscAccessoryTypeRecordService,
        MiscAccessoryTypeTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($miscAccessoryTypeRecordService, $transformer);
        $this->service = $miscAccessoryTypeRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreMiscAccessoryType $request)
    {
        $miscAccessoryType = \DB::transaction(function () use ($request) {
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->create(
                $name,

                $user
            );
        });
        return $this->response->item($miscAccessoryType, $this->transformer)->setStatusCode(201);
    }

    public function update($miscAccessoryTypeId, UpdateMiscAccessoryType $request)
    {
        $miscAccessoryType = \DB::transaction(function () use ($miscAccessoryTypeId, $request) {
            $miscAccessoryType   = $this->service->getById($miscAccessoryTypeId);
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->update(
                $miscAccessoryType,
                $name,

                $user
            );
        });
        return $this->response->item($miscAccessoryType, $this->transformer)->setStatusCode(200);
    }

    public function destroy($miscAccessoryTypeId, DestroyMiscAccessoryType $request)
    {
        $miscAccessoryType = $this->service->getById($miscAccessoryTypeId);
        $this->service->delete($miscAccessoryType);
        return $this->response->item($miscAccessoryType, $this->transformer)->setStatusCode(200);
    }
}
