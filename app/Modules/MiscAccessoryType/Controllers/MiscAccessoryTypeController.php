<?php

namespace App\Modules\MiscAccessoryType\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\MiscAccessoryType\Services\MiscAccessoryTypeRecordService;


class MiscAccessoryTypeController extends Controller
{
    private $service;

    public function __construct(MiscAccessoryTypeRecordService $miscAccessoryTypeRecordService)
    {
        $this->service = $miscAccessoryTypeRecordService;
        $this->middleware('permission:Read [MAS] Misc Accessory Type')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Misc Accessory Type')->only('create');
        $this->middleware('permission:Update [MAS] Misc Accessory Type')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/misc-accessory-types'
        ]);
        return view('misc-accessory-types.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('misc-accessory-types.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $miscAccessoryType = $this->service->getById($id);
        return view('misc-accessory-types.show', compact('miscAccessoryType'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $miscAccessoryType = $this->service->getById($id);
        return view('misc-accessory-types.edit', compact('miscAccessoryType'));
    }
}
