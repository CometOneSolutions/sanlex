<?php

namespace App\Modules\MiscAccessoryType\Transformer;

use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class MiscAccessoryTypeTransformer extends UpdatableByUserTransformer
{
    public function transform(MiscAccessoryType $miscAccessoryType)
    {
        return [
            'id' => (int) $miscAccessoryType->getId(),
            'text' => $miscAccessoryType->getName(),
            'name' => $miscAccessoryType->getName(),
            'updatedAt' => $miscAccessoryType->updated_at,
            'editUri' => route('misc-accessory-type_edit', $miscAccessoryType->getId()),
            'showUri' => route('misc-accessory-type_show', $miscAccessoryType->getId()),
        ];
    }
}
