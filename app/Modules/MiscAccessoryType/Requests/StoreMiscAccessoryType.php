<?php

namespace App\Modules\MiscAccessoryType\Requests;

use Dingo\Api\Http\FormRequest;

class StoreMiscAccessoryType extends MiscAccessoryTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Misc Accessory Type');
    }
}
