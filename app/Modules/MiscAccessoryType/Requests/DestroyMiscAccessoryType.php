<?php

namespace App\Modules\MiscAccessoryType\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyMiscAccessoryType extends MiscAccessoryTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Misc Accessory Type');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
