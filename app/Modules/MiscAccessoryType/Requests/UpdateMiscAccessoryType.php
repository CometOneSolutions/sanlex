<?php

namespace App\Modules\MiscAccessoryType\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateMiscAccessoryType extends MiscAccessoryTypeRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Misc Accessory Type');
    }
}
