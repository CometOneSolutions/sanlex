<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'misc-accessory-types'], function () use ($api) {
        $api->get('/', 'App\Modules\MiscAccessoryType\Controllers\MiscAccessoryTypeApiController@index');
        $api->get('{miscAccessoryTypeId}', 'App\Modules\MiscAccessoryType\Controllers\MiscAccessoryTypeApiController@show');
        $api->post('/', 'App\Modules\MiscAccessoryType\Controllers\MiscAccessoryTypeApiController@store');
        $api->patch('{miscAccessoryTypeId}', 'App\Modules\MiscAccessoryType\Controllers\MiscAccessoryTypeApiController@update');
        $api->delete('{miscAccessoryTypeId}', 'App\Modules\MiscAccessoryType\Controllers\MiscAccessoryTypeApiController@destroy');
    });
});
