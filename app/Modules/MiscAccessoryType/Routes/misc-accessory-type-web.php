<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'misc-accessory-types'], function () {
        Route::get('/', 'MiscAccessoryTypeController@index')->name('misc-accessory-type_index');
        Route::get('/create', 'MiscAccessoryTypeController@create')->name('misc-accessory-type_create');
        Route::get('{miscAccessoryTypeId}/edit', 'MiscAccessoryTypeController@edit')->name('misc-accessory-type_edit');
        Route::get('{miscAccessoryTypeId}/print', 'MiscAccessoryTypeController@print')->name('misc-accessory-type_print');
        Route::get('{miscAccessoryTypeId}', 'MiscAccessoryTypeController@show')->name('misc-accessory-type_show');
    });
});
