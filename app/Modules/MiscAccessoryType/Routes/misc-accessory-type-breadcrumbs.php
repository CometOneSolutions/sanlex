<?php

Breadcrumbs::register('misc-accessory-type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Misc Accessory Types', route('misc-accessory-type_index'));
});
Breadcrumbs::register('misc-accessory-type.create', function ($breadcrumbs) {
    $breadcrumbs->parent('misc-accessory-type.index');
    $breadcrumbs->push('Create', route('misc-accessory-type_create'));
});
Breadcrumbs::register('misc-accessory-type.show', function ($breadcrumbs, $miscAccessoryType) {
    $breadcrumbs->parent('misc-accessory-type.index');
    $breadcrumbs->push($miscAccessoryType->getName(), route('misc-accessory-type_show', $miscAccessoryType->getId()));
});
Breadcrumbs::register('misc-accessory-type.edit', function ($breadcrumbs, $miscAccessoryType) {
    $breadcrumbs->parent('misc-accessory-type.show', $miscAccessoryType);
    $breadcrumbs->push('Edit', route('misc-accessory-type_edit', $miscAccessoryType->getId()));
});
