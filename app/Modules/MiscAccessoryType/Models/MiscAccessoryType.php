<?php

namespace App\Modules\MiscAccessoryType\Models;



interface MiscAccessoryType
{
    public function getId();

    public function getName();

    public function setName($value);
}
