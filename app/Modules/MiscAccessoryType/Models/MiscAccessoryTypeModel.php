<?php

namespace App\Modules\MiscAccessoryType\Models;

use App\Modules\Length\Models\LengthModel;
use App\Modules\MiscAccessory\Models\MiscAccessoryModel;
use App\Modules\Width\Models\WidthModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Models\Users\User;

class MiscAccessoryTypeModel extends Model implements MiscAccessoryType, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'misc_accessory_types';

    public function miscAccessoryLengths()
    {
        return $this->belongsToMany(LengthModel::class, 'misc_accessories', 'misc_accessory_type_id', 'length_id');
    }

    public function miscAccessoryWidths()
    {
        return $this->belongsToMany(WidthModel::class, 'misc_accessories', 'misc_accessory_type_id', 'width_id');
    }

    public function getMiscAccessoryLengths()
    {
        return $this->miscAccessoryLengths;
    }

    public function getMiscAccessoryWidths()
    {
        return $this->miscAccessoryWidths;
    }

    public function miscAccessories()
    {
        return $this->hasMany(MiscAccessoryModel::class, 'misc_accessory_type_id');
    }

    public function getMiscAccessories()
    {
        return $this->miscAccessories;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
