<?php

namespace App\Modules\MiscAccessoryType\Provider;

use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use App\Modules\MiscAccessoryType\Models\MiscAccessoryTypeModel;
use App\Modules\MiscAccessoryType\Repositories\EloquentMiscAccessoryTypeRepository;
use App\Modules\MiscAccessoryType\Repositories\MiscAccessoryTypeRepository;
use App\Modules\MiscAccessoryType\Services\MiscAccessoryTypeRecordService;
use App\Modules\MiscAccessoryType\Services\MiscAccessoryTypeRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class MiscAccessoryTypeServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/MiscAccessoryType/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(MiscAccessoryTypeRecordService::class, MiscAccessoryTypeRecordServiceImpl::class);
        $this->app->bind(MiscAccessoryTypeRepository::class, EloquentMiscAccessoryTypeRepository::class);
        $this->app->bind(MiscAccessoryType::class, MiscAccessoryTypeModel::class);
    }
}
