<?php

namespace App\Modules\Accessory\Services;

use App\Modules\Accessory\Models\Accessory;
use App\Modules\Accessory\Repositories\AccessoryRepository;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Auth\CanSetUpdatedByUser;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class AccessoryRecordServiceImpl extends RecordServiceImpl implements AccessoryRecordService
{
    private $accessory;
    private $accessories;
    private $productRecordService;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"],
        ['id' => 'supplier_name', 'text' => 'Supplier']
    ];

    public function __construct(AccessoryRepository $accessories, Accessory $accessory, ProductRecordService $productRecordService)
    {
        $this->accessories = $accessories;
        $this->accessory = $accessory;
        $this->productRecordService = $productRecordService;
        parent::__construct($accessories);
    }

    public function create(
        $name,
        Supplier $supplier,
        $description,
        User $user = null
    ) {
        $accessory = new $this->accessory;
        $accessory->setDescription($description);
        if ($user) {
            $accessory->setUpdatedByUser($user);
        }
        $this->accessories->save($accessory);
        $this->productRecordService->create($name, $supplier, $accessory, $user);
        return $accessory;
    }

    public function update(
        Accessory $accessory,
        $name,
        Supplier $supplier,
        $description,
        User $user = null
    ) {
        $tempAccessory = clone $accessory;
        $product = $tempAccessory->getProduct();
        $tempAccessory->setDescription($description);
        if ($user) {
            $tempAccessory->setUpdatedByUser($user);
        }
        $this->accessories->save($tempAccessory);
        $this->productRecordService->update($product, $name, $supplier, $accessory, $user);
        return $tempAccessory;
    }

    public function delete(Accessory $accessory)
    {
        $this->accessories->delete($accessory);
        return $accessory;
    }
    public function inStock()
    {
        $this->accessories = $this->accessories->inStock();
        return $this;
    }
}
