<?php

namespace App\Modules\Accessory\Services;

use App\Modules\Supplier\Models\Supplier;
use App\Modules\Accessory\Models\Accessory;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordService;

interface AccessoryRecordService extends RecordService
{
    public function create(
        $name,
        Supplier $supplier,
        $description,
        User $user = null
    );

    public function update(
        Accessory $accessory,
        $name,
        Supplier $supplier,
        $description,
        User $user = null
    );

    public function delete(Accessory $accessory);
}
