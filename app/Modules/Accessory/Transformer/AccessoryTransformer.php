<?php

namespace App\Modules\Accessory\Transformer;

use App\Modules\Accessory\Models\Accessory;
use App\Modules\Product\Transformer\ProductTransformer;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;
use League\Fractal;


class AccessoryTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'product'
    ];
    public function transform(Accessory $accessory)
    {
        return [
            'id' => (int)$accessory->getId(),
            'name' => $accessory->getProduct() ? $accessory->getProduct()->getName() : null,
            'supplierId' => $accessory->getProduct() ? $accessory->getProduct()->getSupplier()->getId() : null,
            'description' => $accessory->getDescription(),
            'updatedAt' => $accessory->updated_at,
            'editUri' => route('accessory_edit', $accessory->getId()),
            'showUri' => route('accessory_show', $accessory->getId()),
        ];
    }

    public function includeProduct(Accessory $accessory)
    {
        $product = $accessory->getProduct();
        return $this->item($product, new ProductTransformer);
    }
}
