<?php

namespace App\Modules\Accessory\Controllers;

use JavaScript;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\Accessory\Services\AccessoryRecordService;

class AccessoryController extends Controller
{
    private $service;

    public function __construct(AccessoryRecordService $accesorieRecordService)
    {
        $this->service = $accesorieRecordService;
        $this->middleware('permission:Read [MAS] Accessory')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Accessory')->only('create');
        $this->middleware('permission:Update [MAS] Accessory')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/accessories?include=product'
        ]);
        return view('accessories.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('accessories.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $accessory = $this->service->getById($id);
        return view('accessories.show', compact('accessory'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $accessory = $this->service->getById($id);
        return view('accessories.edit', compact('accessory'));
    }
}
