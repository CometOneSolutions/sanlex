<?php

namespace App\Modules\Accessory\Controllers;


use App\Modules\Accessory\Requests\DestroyAccessory;
use App\Modules\Accessory\Requests\StoreAccessory;
use App\Modules\Accessory\Requests\UpdateAccessory;
use App\Modules\Accessory\Services\AccessoryRecordService;
use App\Modules\Accessory\Transformer\AccessoryTransformer;
use App\Modules\Product\Services\ProductRecordService;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use DB;

class AccessoryApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;
    protected $productRecordService;

    public function __construct(
        AccessoryRecordService $accessoryRecordService,
        AccessoryTransformer $transformer,
        ProductRecordService $productRecordService
    )
    {
        $this->middleware('auth:api');
        parent::__construct($accessoryRecordService, $transformer);
        $this->service = $accessoryRecordService;
        $this->transformer = $transformer;
        $this->productRecordService = $productRecordService;
    }

    public function store(StoreAccessory $request)
    {
        $accessory = DB::transaction(function () use ($request) {
            $name = ($request->getName($request->getSupplier(), $request->getDescription()));

            return $this->service->create(
                $name,
                $request->getSupplier(),
                $request->getDescription(),
                $request->user()
            );
        });
        return $this->response->item($accessory, $this->transformer)->setStatusCode(201);
    }

    public function update($accessoryId, UpdateAccessory $request)
    {
        $accessory = DB::transaction(function () use ($request) {
            $name = ($request->getName($request->getSupplier(), $request->getDescription()));

            return $this->service->update(
                $request->getAccessory(),
                $name,
                $request->getSupplier(),
                $request->getDescription(),
                $request->user()
            );
        });
        return $this->response->item($accessory, $this->transformer)->setStatusCode(200);
    }

    public function destroy($accessoryId, DestroyAccessory $request)
    {
        $accessory = $this->service->getById($accessoryId);
        $this->service->delete($accessory);
        return $this->response->item($accessory, $this->transformer)->setStatusCode(200);
    }
}
