<?php

namespace App\Modules\Accessory\Models;


interface Accessory
{
    public function getId();

    public function getDescription();

    public function setDescription($value);
}
