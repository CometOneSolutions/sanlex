<?php

namespace App\Modules\Accessory\Models;

use App\Modules\Product\Models\ProductModel;
use App\Modules\Product\Traits\Productable;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;

class AccessoryModel extends Model implements Accessory, UpdatableByUser, Productable
{
    use HasUpdatedByUser;

    protected $table = 'accessories';

    public function product()
    {
        return $this->morphOne(ProductModel::class, 'productable');
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getShortDescription($keyword)
    {
        $index = (strpos($this->getDescription(), $keyword));
        return substr($this->getDescription(), 0, $index - 1);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

    public function isCoil()
    {
        return false;
    }
    public function scopeInStock($query)
    {
        return $query->whereHas('product', function ($product) {
            return $product->whereHas('inventory', function ($inventory) {
                return $inventory->where('stock', '>', 0);
            });
        });
    }

    public function getAssembledProductName()
    {
        $nameAttributes = [
            $this->getProduct()->getSupplier()->getShortCode(),
            $this->getDescription(),
        ];

        return  strtoupper(implode("~", $nameAttributes));
    }
}
