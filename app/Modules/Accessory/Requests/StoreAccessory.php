<?php

namespace App\Modules\Accessory\Requests;

use Dingo\Api\Http\FormRequest;

class StoreAccessory extends AccessoryRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Accessory');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }

}
