<?php

namespace App\Modules\Accessory\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateAccessory extends AccessoryRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Accessory');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }

}