<?php

namespace App\Modules\Accessory\Requests;

use App\Modules\Accessory\Services\AccessoryRecordService;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Supplier\Services\SupplierRecordService;
use Dingo\Api\Http\FormRequest;

class AccessoryRequest extends FormRequest
{
    private $service;
    private $supplierRecordService;

    public function __construct(
        SupplierRecordService $supplierRecordService,
        AccessoryRecordService $accessoryRecordService
        ) {
        $this->supplierRecordService = $supplierRecordService;
        $this->service = $accessoryRecordService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $accessory = $this->route('accessoryId') ? $this->service->getById($this->route('accessoryId')) : null;

        if ($accessory) {
            return [
                'name'          => 'required|unique:products,name,'.$accessory->getProduct()->getId(),
                'description'   => 'required'
            ];
        } else {
            return [
                'name'                  => 'required|unique:products,name',
                'description'           => 'required'
            ];
        }
    }
    public function messages()
    {
        return [
            'name.required'      => 'Name is required.',
            'name.unique'        => 'Name already exists.',
            'description.required'      => 'Description is required.',
        ];
    }

    public function getDescription()
    {
        return $this->input('description') ? $this->input('description') : null;
    }

    public function getName(Supplier $supplier, $description)
    {
        $format = '%s~%s';
        return sprintf($format, $supplier->getShortCode(), $description);
    }

    public function getSupplier($index = 'supplierId')
    {
        return $this->supplierRecordService->getById($this->input($index));
    }

    public function getAccessory($index = 'id')
    {
        return $this->service->getById($this->input($index));
    }
}
