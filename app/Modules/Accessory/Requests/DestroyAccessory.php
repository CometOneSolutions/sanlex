<?php

namespace App\Modules\Accessory\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyAccessory extends AccessoryRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Accessory');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}