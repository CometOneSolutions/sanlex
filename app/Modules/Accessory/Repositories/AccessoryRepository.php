<?php

namespace App\Modules\Accessory\Repositories;

use App\Modules\Accessory\Models\Accessory;
use CometOneSolutions\Common\Repositories\Repository;



interface AccessoryRepository extends Repository
{
    public function save(Accessory $accessory);
    public function delete(Accessory $accessory);
}
