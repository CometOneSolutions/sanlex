<?php

namespace App\Modules\Accessory\Repositories;

use App\Modules\Accessory\Models\Accessory;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentAccessoryRepository extends EloquentRepository implements AccessoryRepository
{
    public function __construct(Accessory $accessory)
    {
        parent::__construct($accessory);
    }

    public function save(Accessory $accessory)
    {
        return $accessory->save();
    }

    public function delete(Accessory $accessory)
    {
        $product = $accessory->getProduct();
        $product->delete();
        return $accessory->delete();
    }

    public function filterBySupplierName($supplierName)
    {
        return $this->model->whereHas('product', function ($product) use ($supplierName) {
            return $product->whereHas('supplier', function ($supplier) use ($supplierName) {
                return $supplier->where('name', 'like', '%' . $supplierName . '%');
            });
        });
    }
    public function inStock()
    {
        $this->model = $this->model->inStock();
        return $this;
    }
}
