<?php

namespace App\Modules\Accessory\Database\Seeds;

use App\Modules\Accessory\Models\AccessoryModel;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

class AccessoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $colors = factory(AccessoryModel::class, 5)->create();
    }
}
