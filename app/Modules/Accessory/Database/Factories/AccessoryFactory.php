<?php

use Faker\Generator as Faker;
use App\Modules\Accessory\Models\AccessoryModel;
use CometOneSolutions\Auth\UserModel;

$factory->define(AccessoryModel::class, function (Faker $faker) {
    return [
        'description' => $faker->unique()->company,
        'updated_by_user_id' => $faker->randomElement(UserModel::pluck('id')->toArray()),
    ];
});
