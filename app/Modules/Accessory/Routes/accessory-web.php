<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'accessories'], function () {
        Route::get('/', 'AccessoryController@index')->name('accessory_index');
        Route::get('/create', 'AccessoryController@create')->name('accessory_create');
        Route::get('{accessoryId}/edit', 'AccessoryController@edit')->name('accessory_edit');
        Route::get('{accessoryId}/print', 'AccessoryController@print')->name('accessory_print');
        Route::get('{accessoryId}', 'AccessoryController@show')->name('accessory_show');
    });

});
