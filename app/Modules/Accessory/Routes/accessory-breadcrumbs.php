<?php

Breadcrumbs::register('accessory.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Accessories', route('accessory_index'));
});
Breadcrumbs::register('accessory.create', function ($breadcrumbs) {
    $breadcrumbs->parent('accessory.index');
    $breadcrumbs->push('Create', route('accessory_create'));
});
Breadcrumbs::register('accessory.show', function ($breadcrumbs, $accessory) {
    $breadcrumbs->parent('accessory.index');
    $breadcrumbs->push($accessory->getProduct()->getName(), route('accessory_show', $accessory->getId()));
});
Breadcrumbs::register('accessory.edit', function ($breadcrumbs, $accessory) {
    $breadcrumbs->parent('accessory.show', $accessory);
    $breadcrumbs->push('Edit', route('accessory_edit', $accessory->getId()));
});
