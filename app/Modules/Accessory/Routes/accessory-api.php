<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'accessories'], function () use ($api) {
        $api->get('/', '\App\Modules\Accessory\Controllers\AccessoryApiController@index');
        $api->get('{accessoryId}', '\App\Modules\Accessory\Controllers\AccessoryApiController@show');
        $api->post('/', '\App\Modules\Accessory\Controllers\AccessoryApiController@store');
        $api->patch('{accessoryId}', '\App\Modules\Accessory\Controllers\AccessoryApiController@update');
        $api->delete('{accessoryId}', '\App\Modules\Accessory\Controllers\AccessoryApiController@destroy');
    });
});
