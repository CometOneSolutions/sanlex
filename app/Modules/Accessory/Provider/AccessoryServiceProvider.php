<?php

namespace App\Modules\Accessory\Provider;

use App\Modules\Accessory\Models\Accessory;
use App\Modules\Accessory\Models\AccessoryModel;
use App\Modules\Accessory\Repositories\AccessoryRepository;
use App\Modules\Accessory\Repositories\EloquentAccessoryRepository;
use App\Modules\Accessory\Services\AccessoryRecordService;
use App\Modules\Accessory\Services\AccessoryRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class AccessoryServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Accessory/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(AccessoryRecordService::class, AccessoryRecordServiceImpl::class);
        $this->app->bind(AccessoryRepository::class, EloquentAccessoryRepository::class);
        $this->app->bind(Accessory::class, AccessoryModel::class);
    }
}
