import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select2 from '@c1_common_js/components/Select2';
import { supplierService } from "@c1_module_js/Supplier/Resources/Assets/js/supplier-main";

new Vue({
    el: '#accessory',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select2, supplierService
    },
    data: {
        form: new Form({
            name: null,
            supplierId: null,
            description: null
        }),
        dataInitialized: true,
        supplierSelections: [],
        supplierSelectionsInitialized: false
    },
    watch: {
        initializationComplete(val) {      
            this.form.isInitializing = !val;
        }
    },
    computed: {
        selectedSupplier() {
            if(this.form.supplierId == null)
            {
                return undefined;
            }
            return this.supplierSelections.find(supplier => supplier.id == this.form.supplierId);
        },
        name() {
            if(this.selectedSupplier == undefined || this.form.description == null)
            {
                return undefined;
            }
            return this.selectedSupplier.shortCode + '~' + this.form.description;
        },
        initializationComplete() {
            return this.dataInitialized && this.supplierSelectionsInitialized;
        }
    },
    methods: {
        destroy() {
            this.form.deleteWithConfirmation('/api/accessories/' + this.form.id).then(response => {
                this.form.successModal('Accessory was removed.').then(() => 
                    window.location = '/master-file/accessories/'
                );                
            }); 
        },
       
        store() {
            this.form.name = this.name;
            this.form.postWithModal('/api/accessories', null, 'Accessory was saved.');
        },

        update() {
            this.form.name = this.name;
            this.form.patch('/api/accessories/' + this.form.id).then(response => {
                this.form.successModal('Accessory was updated.').then(() => 
                    window.location = '/master-file/accessories/' + this.form.id
                );
            })
        },
       
        loadData(data) {
            this.form = new Form(data);
        },
    },

    created(){
        supplierService.getSuppliers().then(response => {
            this.supplierSelections = response.data;
            this.supplierSelectionsInitialized = true;
        });
        
        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/accessories/' + id + '?include=product.supplier')
            .then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
            });
        }
    },
    mounted() {
        console.log("Init accessory script...");
    }
});