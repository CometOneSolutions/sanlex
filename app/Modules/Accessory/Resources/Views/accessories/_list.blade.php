
<div class="row">
<div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Name</label>
            <p>@{{form.product.data.name }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Supplier</label>
            <p>@{{form.product.data.supplier.data.name }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Description</label>
            <p>@{{form.description || "--" }}</p>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{ mix('js/accessory.js') }}"></script>
@endpush