<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
            @{{ error[0] }}
    </small>
</div>
<div class="row">
    
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Supplier <span class="text-danger">*</span></label>
            <select2 class="form-control" :allow-custom="false" v-model="form.supplierId" id="supplierId" name="supplierId"
            placeholder="Please select"
            :class="{'is-invalid': form.errors.has('supplierId') }" 
            required data-msg-required="Enter supplier">
                    <option disabled value=''>Please select Supplier</option>
                    <option v-for="item in supplierSelections" :value="item.id">@{{ item.name }}</option>
            </select2>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="description">Description <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="description" name="description" v-model="form.description" aria-describedby="descriptionHelp" 
            :class="{'is-invalid': form.errors.has('description')}" 
            required data-msg-required="Please input description" >
        </div>
    </div>
</div>
@push('scripts')
<script src="{{ mix('js/accessory.js') }}"></script>
@endpush