<?php

namespace App\Modules\PvcDiameter\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyPvcDiameter extends PvcDiameterRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Pvc Diameter');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
