<?php

namespace App\Modules\PvcDiameter\Requests;

use Dingo\Api\Http\FormRequest;

class PvcDiameterRequest extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'name'                  => 'required|unique:pvc_diameters,name,' . $this->route('pvcDiameterId') ?? null,
		];
	}
	public function messages()
	{
		return [
			'name.required'         => 'Name is required',
			'name.unique'           => 'Name already exists'
		];
	}

	public function getName()
	{
		return $this->input('name');
	}
}
