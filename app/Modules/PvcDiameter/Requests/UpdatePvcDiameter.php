<?php

namespace App\Modules\PvcDiameter\Requests;

use Dingo\Api\Http\FormRequest;

class UpdatePvcDiameter extends PvcDiameterRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] Pvc Diameter');
	}
}
