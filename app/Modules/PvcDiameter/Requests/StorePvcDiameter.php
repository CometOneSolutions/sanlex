<?php

namespace App\Modules\PvcDiameter\Requests;

use Dingo\Api\Http\FormRequest;

class StorePvcDiameter extends PvcDiameterRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [MAS] Pvc Diameter');
	}
}
