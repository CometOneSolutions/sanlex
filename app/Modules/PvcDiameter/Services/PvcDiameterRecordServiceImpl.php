<?php

namespace App\Modules\PvcDiameter\Services;

use App\Modules\Product\Traits\CanUpdateProductName;
use App\Modules\PvcDiameter\Models\PvcDiameter;
use App\Modules\PvcDiameter\Repositories\PvcDiameterRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class PvcDiameterRecordServiceImpl extends RecordServiceImpl implements PvcDiameterRecordService
{
	use CanUpdateProductName;

	private $pvcDiameter;
	private $pvcDiameters;

	protected $availableFilters = [
		["id" => "name", "text" => "Name"]
	];

	public function __construct(PvcDiameterRepository $pvcDiameters, PvcDiameter $pvcDiameter)
	{
		$this->pvcDiameters = $pvcDiameters;
		$this->pvcDiameter = $pvcDiameter;
		parent::__construct($pvcDiameters);
	}

	public function create(
		$name,
		User $user = null
	) {
		$pvcDiameter = new $this->pvcDiameter;
		$pvcDiameter->setName($name);

		if ($user) {
			$pvcDiameter->setUpdatedByUser($user);
		}
		$this->pvcDiameters->save($pvcDiameter);
		return $pvcDiameter;
	}

	public function update(
		PvcDiameter $pvcDiameter,
		$name,
		User $user = null
	) {
		$tempPvcDiameter = clone $pvcDiameter;
		$tempPvcDiameter->setName($name);
		if ($user) {
			$tempPvcDiameter->setUpdatedByUser($user);
		}
		$this->pvcDiameters->save($tempPvcDiameter);

		$this->updateProductableProductNames([
			$tempPvcDiameter->getPvcFittings(),
		]);

		return $tempPvcDiameter;
	}

	public function delete(PvcDiameter $pvcDiameter)
	{
		$this->pvcDiameters->delete($pvcDiameter);
		return $pvcDiameter;
	}
}
