<?php

namespace App\Modules\PvcDiameter\Services;

use App\Modules\PvcDiameter\Models\PvcDiameter;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface PvcDiameterRecordService extends RecordService
{
	public function create(
		$name,
		User $user = null
	);

	public function update(
		PvcDiameter $pvcDiameter,
		$name,
		User $user = null
	);

	public function delete(PvcDiameter $pvcDiameter);
}
