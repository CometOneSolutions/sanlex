<?php

namespace App\Modules\PvcDiameter\Repositories;

use App\Modules\PvcDiameter\Models\PvcDiameter;
use CometOneSolutions\Common\Repositories\Repository;

interface PvcDiameterRepository extends Repository
{
	public function save(PvcDiameter $pvcDiameter);
	public function delete(PvcDiameter $pvcDiameter);
}
