<?php

namespace App\Modules\PvcDiameter\Repositories;

use App\Modules\PvcDiameter\Models\PvcDiameter;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentPvcDiameterRepository extends EloquentRepository implements PvcDiameterRepository
{
	public function __construct(PvcDiameter $pvcDiameter)
	{
		parent::__construct($pvcDiameter);
	}

	public function save(PvcDiameter $pvcDiameter)
	{
		return $pvcDiameter->save();
	}

	public function delete(PvcDiameter $pvcDiameter)
	{
		return $pvcDiameter->delete();
	}
}
