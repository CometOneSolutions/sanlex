<?php

namespace App\Modules\PvcDiameter\Provider;

use App\Modules\PvcDiameter\Models\PvcDiameter;
use App\Modules\PvcDiameter\Models\PvcDiameterModel;
use App\Modules\PvcDiameter\Repositories\EloquentPvcDiameterRepository;
use App\Modules\PvcDiameter\Repositories\PvcDiameterRepository;
use App\Modules\PvcDiameter\Services\PvcDiameterRecordService;
use App\Modules\PvcDiameter\Services\PvcDiameterRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class PvcDiameterServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/PvcDiameter/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(PvcDiameterRecordService::class, PvcDiameterRecordServiceImpl::class);
		$this->app->bind(PvcDiameterRepository::class, EloquentPvcDiameterRepository::class);
		$this->app->bind(PvcDiameter::class, PvcDiameterModel::class);
	}
}
