<?php

namespace App\Modules\PvcDiameter\Transformer;

use App\Modules\PvcDiameter\Models\PvcDiameter;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class PvcDiameterTransformer extends UpdatableByUserTransformer
{
	public function transform(PvcDiameter $pvcDiameter)
	{
		return [
			'id' => (int) $pvcDiameter->getId(),
			'text' => $pvcDiameter->getName(),
			'name' => $pvcDiameter->getName(),
			'updatedAt' => $pvcDiameter->updated_at,
			'editUri' => route('pvc-diameter_edit', $pvcDiameter->getId()),
			'showUri' => route('pvc-diameter_show', $pvcDiameter->getId()),
		];
	}
}
