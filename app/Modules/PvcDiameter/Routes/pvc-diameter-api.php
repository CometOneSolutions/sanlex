<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'pvc-diameters'], function () use ($api) {
		$api->get('/', 'App\Modules\PvcDiameter\Controllers\PvcDiameterApiController@index');
		$api->get('{pvcDiameterId}', 'App\Modules\PvcDiameter\Controllers\PvcDiameterApiController@show');
		$api->post('/', 'App\Modules\PvcDiameter\Controllers\PvcDiameterApiController@store');
		$api->patch('{pvcDiameterId}', 'App\Modules\PvcDiameter\Controllers\PvcDiameterApiController@update');
		$api->delete('{pvcDiameterId}', 'App\Modules\PvcDiameter\Controllers\PvcDiameterApiController@destroy');
	});
});
