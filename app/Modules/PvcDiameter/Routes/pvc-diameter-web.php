<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'pvc-diameters'], function () {
		Route::get('/', 'PvcDiameterController@index')->name('pvc-diameter_index');
		Route::get('/create', 'PvcDiameterController@create')->name('pvc-diameter_create');
		Route::get('{pvcDiameterId}/edit', 'PvcDiameterController@edit')->name('pvc-diameter_edit');
		Route::get('{pvcDiameterId}/print', 'PvcDiameterController@print')->name('pvc-diameter_print');
		Route::get('{pvcDiameterId}', 'PvcDiameterController@show')->name('pvc-diameter_show');
	});
});
