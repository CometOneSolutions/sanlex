<?php

Breadcrumbs::register('pvc-diameter.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Pvc Diameters', route('pvc-diameter_index'));
});
Breadcrumbs::register('pvc-diameter.create', function ($breadcrumbs) {
	$breadcrumbs->parent('pvc-diameter.index');
	$breadcrumbs->push('Create', route('pvc-diameter_create'));
});
Breadcrumbs::register('pvc-diameter.show', function ($breadcrumbs, $pvcDiameter) {
	$breadcrumbs->parent('pvc-diameter.index');
	$breadcrumbs->push($pvcDiameter->getName(), route('pvc-diameter_show', $pvcDiameter->getId()));
});
Breadcrumbs::register('pvc-diameter.edit', function ($breadcrumbs, $pvcDiameter) {
	$breadcrumbs->parent('pvc-diameter.show', $pvcDiameter);
	$breadcrumbs->push('Edit', route('pvc-diameter_edit', $pvcDiameter->getId()));
});
