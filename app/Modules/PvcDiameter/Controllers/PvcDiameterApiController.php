<?php

namespace App\Modules\PvcDiameter\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\PvcDiameter\Services\PvcDiameterRecordService;
use App\Modules\PvcDiameter\Transformer\PvcDiameterTransformer;
use App\Modules\PvcDiameter\Requests\StorePvcDiameter;
use App\Modules\PvcDiameter\Requests\UpdatePvcDiameter;
use App\Modules\PvcDiameter\Requests\DestroyPvcDiameter;

class PvcDiameterApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;

	public function __construct(
		PvcDiameterRecordService $pvcDiameterRecordService,
		PvcDiameterTransformer $transformer
	) {
		$this->middleware('auth:api');
		parent::__construct($pvcDiameterRecordService, $transformer);
		$this->service = $pvcDiameterRecordService;
		$this->transformer = $transformer;
	}

	public function store(StorePvcDiameter $request)
	{
		$pvcDiameter = \DB::transaction(function () use ($request) {
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->create(
				$name,

				$user
			);
		});
		return $this->response->item($pvcDiameter, $this->transformer)->setStatusCode(201);
	}

	public function update($pvcDiameterId, UpdatePvcDiameter $request)
	{
		$pvcDiameter = \DB::transaction(function () use ($pvcDiameterId, $request) {
			$pvcDiameter   = $this->service->getById($pvcDiameterId);
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->update(
				$pvcDiameter,
				$name,

				$user
			);
		});
		return $this->response->item($pvcDiameter, $this->transformer)->setStatusCode(200);
	}

	public function destroy($pvcDiameterId, DestroyPvcDiameter $request)
	{
		$pvcDiameter = $this->service->getById($pvcDiameterId);
		$this->service->delete($pvcDiameter);
		return $this->response->item($pvcDiameter, $this->transformer)->setStatusCode(200);
	}
}
