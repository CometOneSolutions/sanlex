<?php

namespace App\Modules\PvcDiameter\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\PvcDiameter\Services\PvcDiameterRecordService;


class PvcDiameterController extends Controller
{
	private $service;

	public function __construct(PvcDiameterRecordService $pvcDiameterRecordService)
	{
		$this->service = $pvcDiameterRecordService;
		$this->middleware('permission:Read [MAS] Pvc Diameter')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Pvc Diameter')->only('create');
		$this->middleware('permission:Update [MAS] Pvc Diameter')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'name',
			'sortAscending' => true,
			'baseUrl' => '/api/pvc-diameters'
		]);
		return view('pvc-diameters.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('pvc-diameters.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$pvcDiameter = $this->service->getById($id);
		return view('pvc-diameters.show', compact('pvcDiameter'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$pvcDiameter = $this->service->getById($id);
		return view('pvc-diameters.edit', compact('pvcDiameter'));
	}
}
