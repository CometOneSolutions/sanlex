<?php

namespace App\Modules\PvcDiameter\Models;

use App\Modules\PvcFitting\Models\PvcFittingModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;


class PvcDiameterModel extends Model implements PvcDiameter, UpdatableByUser
{
	use HasUpdatedByUser;

	protected $table = 'pvc_diameters';

	public function pvcFittings()
	{
		return $this->hasMany(PvcFittingModel::class, 'pvc_diameter_id');
	}

	public function getPvcFittings()
	{
		return $this->pvcFittings;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($value)
	{
		$this->name = $value;
		return $this;
	}
}
