<?php

namespace App\Modules\PvcDiameter\Models;


interface PvcDiameter
{
	public function getId();

	public function getName();

	public function setName($value);
}
