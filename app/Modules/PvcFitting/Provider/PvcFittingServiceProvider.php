<?php

namespace App\Modules\PvcFitting\Provider;

use App\Modules\PvcFitting\Models\PvcFitting;
use App\Modules\PvcFitting\Models\PvcFittingModel;
use App\Modules\PvcFitting\Repositories\EloquentPvcFittingRepository;
use App\Modules\PvcFitting\Repositories\PvcFittingRepository;
use App\Modules\PvcFitting\Services\PvcFittingRecordService;
use App\Modules\PvcFitting\Services\PvcFittingRecordServiceImpl;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;

class PvcFittingServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/PvcFitting/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath.'factories'));
        $this->app->bind(PvcFittingRecordService::class, PvcFittingRecordServiceImpl::class);
        $this->app->bind(PvcFittingRepository::class, EloquentPvcFittingRepository::class);
        $this->app->bind(PvcFitting::class, PvcFittingModel::class);
    }
}
