<?php

namespace App\Modules\PvcFitting\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\PvcFitting\Services\PvcFittingRecordService;

class PvcFittingController extends Controller
{
    private $service;

    public function __construct(PvcFittingRecordService $pvcFittingRecordService)
    {
        $this->service = $pvcFittingRecordService;
        $this->middleware('permission:Read [MAS] Pvc Fitting')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Pvc Fitting')->only('create');
        $this->middleware('permission:Update [MAS] Pvc Fitting')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/pvc-fittings?include=product,pvcType,pvcDiameter,brand'
        ]);
        return view('pvc-fittings.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('pvc-fittings.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $pvcFitting = $this->service->getById($id);
        return view('pvc-fittings.show', compact('pvcFitting'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $pvcFitting = $this->service->getById($id);
        return view('pvc-fittings.edit', compact('pvcFitting'));
    }
}
