<?php

namespace App\Modules\PvcFitting\Controllers;

use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\PvcFitting\Requests\DestroyPvcFitting;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\PvcFitting\Services\PvcFittingRecordService;
use App\Modules\PvcFitting\Transformer\PvcFittingTransformer;
use App\Modules\PvcFitting\Requests\StorePvcFitting;
use App\Modules\PvcFitting\Requests\UpdatePvcFitting;

class PvcFittingApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;
    protected $productRecordService;

    public function __construct(
        PvcFittingRecordService $pvcFittingRecordService,
        PvcFittingTransformer $transformer,
        ProductRecordService $productRecordService
    ) {
        $this->middleware('auth:api');
        parent::__construct($pvcFittingRecordService, $transformer);
        $this->service = $pvcFittingRecordService;
        $this->transformer = $transformer;
        $this->productRecordService = $productRecordService;
    }

    public function store(StorePvcFitting $request)
    {
        $pvcFitting = \DB::transaction(function () use ($request) {
            $name = $request->getName();

            return $this->service->create(
                $name,
                $request->getSupplier(),
                $request->getPvcType(),
                $request->getPvcDiameter(),
                $request->getBrand(),
                $request->user()
            );
        });
        return $this->response->item($pvcFitting, $this->transformer)->setStatusCode(201);
    }

    public function update($pvcFittingId, UpdatePvcFitting $request)
    {
        $pvcFitting = \DB::transaction(function () use ($request) {
            $name = $request->getName();

            return $this->service->update(
                $request->getPvcFitting(),
                $name,
                $request->getSupplier(),
                $request->getPvcType(),
                $request->getPvcDiameter(),
                $request->getBrand(),
                $request->user()
            );
        });
        return $this->response->item($pvcFitting, $this->transformer)->setStatusCode(200);
    }

    public function destroy($pvcFittingId, DestroyPvcFitting $request)
    {
        $pvcFitting = $this->service->getById($pvcFittingId);
        $this->service->delete($pvcFitting);
        return $this->response->item($pvcFitting, $this->transformer)->setStatusCode(200);
    }
}
