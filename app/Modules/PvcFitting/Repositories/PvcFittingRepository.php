<?php

namespace App\Modules\PvcFitting\Repositories;

use App\Modules\PvcFitting\Models\PvcFitting;
use CometOneSolutions\Common\Repositories\Repository;

interface PvcFittingRepository extends Repository
{
    public function save(PvcFitting $pvcFitting);

    public function delete(PvcFitting $pvcFitting);
}
