<?php

namespace App\Modules\PvcFitting\Repositories;

use App\Modules\PvcFitting\Models\PvcFitting;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentPvcFittingRepository extends EloquentRepository implements PvcFittingRepository
{
    public function __construct(PvcFitting $pvcFitting)
    {
        parent::__construct($pvcFitting);
    }

    public function save(PvcFitting $pvcFitting)
    {
        return $pvcFitting->save();
    }

    public function delete(PvcFitting $pvcFitting)
    {
        $product = $pvcFitting->getProduct();
        $product->delete();
        return $pvcFitting->delete();
    }

    public function filterByName($productName)
    {
        return $this->model->whereHas('product', function ($product) use ($productName) {
            return $product->where('name', 'like', '%'.$productName.'%');
        });
    }

    public function filterBySupplierName($supplierName)
    {
        return $this->model->whereHas('product', function ($product) use ($supplierName) {
            return $product->whereHas('supplier', function ($supplier) use ($supplierName) {
                return $supplier->where('name', 'like', '%'.$supplierName.'%');
            });
        });
    }

    public function inStock()
    {
        $this->model = $this->model->inStock();
        return $this;
    }
}
