<?php

namespace App\Modules\PvcFitting\Requests;

use App\Modules\Brand\Services\BrandRecordService;
use App\Modules\PvcDiameter\Services\PvcDiameterRecordService;
use App\Modules\PvcFitting\Services\PvcFittingRecordService;
use App\Modules\PvcType\Services\PvcTypeRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use Dingo\Api\Http\FormRequest;

class PvcFittingRequest extends FormRequest
{
    private $service;
    private $supplierRecordService;
    private $pvcTypeRecordService;
    private $pvcDiameterRecordService;
    private $brandRecordService;

    public function __construct(
        SupplierRecordService $supplierRecordService,
        PvcFittingRecordService $pvcRecordService,
        PvcTypeRecordService $pvcTypeRecordService,
        PvcDiameterRecordService $pvcDiameterRecordService,
        BrandRecordService $brandRecordService
    ) {
        $this->supplierRecordService = $supplierRecordService;
        $this->service = $pvcRecordService;
        $this->brandRecordService = $brandRecordService;

        $this->pvcTypeRecordService = $pvcTypeRecordService;
        $this->pvcDiameterRecordService = $pvcDiameterRecordService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $pvc = $this->route('pvcFittingId') ? $this->service->getById($this->route('pvcFittingId')) : null;

        if ($pvc) {
            return [
                'name' => 'required|unique:products,name,'.$pvc->getProduct()->getId(),

                'supplierId' => 'required',
                'pvcTypeId' => 'required',
                'pvcDiameterId' => 'required',
                'brandId' => 'required',
            ];
        } else {
            return [
                'name' => 'required|unique:products,name',
                'supplierId' => 'required',
                'pvcTypeId' => 'required',
                'pvcDiameterId' => 'required',
                'brandId' => 'required',
            ];
        }
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required.',
            'name.unique' => 'Name already exists.',
            'supplierId.required' => 'Supplier is required.',
            'pvcTypeId.required' => 'Pvc type is required.',
            'pvcDiameterId.required' => 'Pvc dimension is required.',
            'brandId.required' => 'Brand is required.'
        ];
    }

    public function getName($index = 'name')
    {
        return $this->input($index);
    }

    public function getSupplier($index = 'supplierId')
    {
        return $this->supplierRecordService->getById($this->input($index));
    }

    public function getBrand($index = 'brandId')
    {
        return $this->brandRecordService->getById($this->input($index));
    }

    public function getPvcFitting($index = 'id')
    {
        return $this->service->getById($this->input($index));
    }

    public function getPvcType($index = 'pvcTypeId')
    {
        return $this->pvcTypeRecordService->getById($this->input($index));
    }

    public function getPvcDiameter($index = 'pvcDiameterId')
    {
        return $this->pvcDiameterRecordService->getById($this->input($index));
    }
}
