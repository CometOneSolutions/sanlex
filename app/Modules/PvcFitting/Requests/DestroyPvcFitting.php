<?php

namespace App\Modules\PvcFitting\Requests;

class DestroyPvcFitting extends PvcFittingRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Pvc Fitting');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
