<?php

namespace App\Modules\PvcFitting\Requests;

class StorePvcFitting extends PvcFittingRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Pvc Fitting');
    }
}
