<?php

namespace App\Modules\PvcFitting\Requests;

class UpdatePvcFitting extends PvcFittingRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Pvc Fitting');
    }
}
