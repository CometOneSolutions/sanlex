import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select3 from "@c1_common_js/components/Select3";


new Vue({
    el: '#pvc-fitting',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select3
    },
    data: {
        form: new Form({
            name: null,
            supplierId: null,
            shortCode: null,
            pvcTypeId: null,
            pvcTypeText: null,
            pvcDiameterId: null,
            pvcDiameterText: null,
            brandId: null,
            brandText: null,
        }),
        dataInitialized: true,
        supplierUrl: '/api/suppliers?sort=name',
        defaultSelectedSupplier: {},
        pvcTypeUrl: '/api/pvc-types?sort=name',
        defaultSelectedPvcType: {},
        pvcDiameterUrl: '/api/pvc-diameters?sort=name',
        defaultSelectedPvcDiameter: {},
        brandUrl: '/api/brands?sort=name',
        defaultSelectedBrand: {},

    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },
    computed: {
        // selectedSupplier() {
        //     if(this.form.supplierId == null)
        //     {
        //         return undefined;
        //     }
        //     return this.supplierSelections.find(supplier => supplier.id == this.form.supplierId);
        // },
        name() {
            if (this.form.supplierId === undefined || this.form.pvcTypeId === null || this.form.pvcDiameterId === null || this.form.brandId === null) {
                return undefined;
            }
            return this.form.shortCode + '~' + this.form.pvcTypeText.toUpperCase() + '~' + this.form.pvcDiameterText.toUpperCase() + '~' + this.form.brandText.toUpperCase();

        },
        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        setShortCode(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("shortCode")) {
                this.form.shortCode = selectedObjects[0].shortCode;
            }
        },
        setPvcType(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.pvcTypeText = selectedObjects[0].name;
            }

        },
        setPvcDiameter(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.pvcDiameterText = selectedObjects[0].name;
            }

        },
        setBrand(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.brandText = selectedObjects[0].name;
            }

        },

        destroy() {
            this.form.deleteWithConfirmation('/api/pvc-fittings/' + this.form.id).then(response => {
                this.form.successModal('Pvc fitting was removed.').then(() =>
                    window.location = '/master-file/pvc-fittings/'
                );
            });
        },

        store() {
            this.form.name = this.name;
            this.form.postWithModal('/api/pvc-fittings', null, 'Pvc fitting was saved.');
        },

        update() {
            this.form.name = this.name;
            this.form.patch('/api/pvc-fittings/' + this.form.id).then(response => {
                this.form.successModal('Pvc fitting was updated.').then(() =>
                    window.location = '/master-file/pvc-fittings/' + this.form.id
                );
            })
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {


        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/pvc-fittings/' + id + '?include=product.supplier,pvcType,pvcDiameter,brand')
                .then(response => {
                    this.loadData(response.data);

                    this.form.shortCode = response.data.product.data.supplier.data.shortCode;
                    this.form.pvcTypeText = response.data.pvcType.data.name;
                    this.form.pvcDiameterText = response.data.pvcDiameter.data.name;
                    this.form.brandText = response.data.brand.data.name;


                    this.defaultSelectedSupplier = {
                        text: response.data.product.data.supplier.data.name,
                        id: response.data.product.data.supplier.data.id,
                    };
                    this.defaultSelectedPvcType = {
                        text: response.data.pvcType.data.name,
                        id: response.data.pvcType.data.id,
                    };
                    this.defaultSelectedPvcDiameter = {
                        text: response.data.pvcDiameter.data.name,
                        id: response.data.pvcDiameter.data.id,
                    };
                    this.defaultSelectedBrand = {
                        text: response.data.brand.data.name,
                        id: response.data.brand.data.id,
                    };
                    this.dataInitialized = true;
                });
        }
    },
    mounted() {
        console.log("Init pvc fitting script...");
    }
});