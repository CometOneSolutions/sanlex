@extends('app')
@section('breadcrumbs', Breadcrumbs::render('pvc-fitting.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Pvc Fittings
            @if(auth()->user()->can('Create [MAS] Pvc Fitting'))
            <div class="float-right">
                <a href="{{route('pvc-fitting_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
            </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sort-ascending="sortAscending" :sorter="sorter" v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('id')">
                                    Product <i class="fa" :class="getSortIcon('id')"></i>
                                </a>
                            </th>
                            <th>Pvc Type</th>
                            <th>Pvc Diameter</th>
                            <th>Brand</th>
							
                           
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{ item.product.data.name }}</a></td>
							<td>@{{item.pvcType.data.name}}</td>
                            <td>@{{item.pvcDiameter.data.name}}</td>
                            <td>@{{item.brand.data.name}}</td>
                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush