<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Supplier <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter supplier" v-model="form.supplierId" :selected="defaultSelectedSupplier" :url="supplierUrl" search="name" placeholder="Please select" @change="setShortCode" id="supplier" name="supplier">
            </select3>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Pvc Type <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter pvc type" v-model="form.pvcTypeId" :selected="defaultSelectedPvcType" :url="pvcTypeUrl" search="name" placeholder="Please select" @change="setPvcType" id="pvcType" name="pvcType">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Pvc Diameter <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter pvc dimension" v-model="form.pvcDiameterId" :selected="defaultSelectedPvcDiameter" :url="pvcDiameterUrl" search="name" placeholder="Please select" @change="setPvcDiameter" id="pvcDiameter" name="pvcDiameter">
            </select3>
        </div>
	</div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Brand <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter brand" v-model="form.brandId" :selected="defaultSelectedBrand" :url="brandUrl" search="name" placeholder="Please select" @change="setBrand" id="brand" name="brand">
            </select3>
        </div>
    </div>
    
</div>
@push('scripts')
<script src="{{ mix('js/pvc-fitting.js') }}"></script>
@endpush