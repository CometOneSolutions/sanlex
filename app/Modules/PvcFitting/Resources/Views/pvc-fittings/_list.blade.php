<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Name</label>
            <p>@{{form.product.data.name }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Supplier</label>
            <p>@{{form.product.data.supplier.data.name }}</p>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Pvc Type</label>
            <p>@{{form.pvcType.data.name || "--" }}</p>
        </div>
    </div>
    
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Pvc Diameter</label>
            <p>@{{form.pvcDiameter.data.name || "--" }}</p>
        </div>
	</div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Brand</label>
            <p>@{{form.brand.data.name || "--" }}</p>
        </div>
    </div>
  
</div>
@push('scripts')
<script src="{{ mix('js/pvc-fitting.js') }}"></script>
@endpush