<?php

Breadcrumbs::register('pvc-fitting.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('PVC Fittings', route('pvc-fitting_index'));
});
Breadcrumbs::register('pvc-fitting.create', function ($breadcrumbs) {
    $breadcrumbs->parent('pvc-fitting.index');
    $breadcrumbs->push('Create', route('pvc-fitting_create'));
});
Breadcrumbs::register('pvc-fitting.show', function ($breadcrumbs, $pvcFitting) {
    $breadcrumbs->parent('pvc-fitting.index');
    $breadcrumbs->push($pvcFitting->getProduct()->getName(), route('pvc-fitting_show', $pvcFitting->getId()));
});
Breadcrumbs::register('pvc-fitting.edit', function ($breadcrumbs, $pvcFitting) {
    $breadcrumbs->parent('pvc-fitting.show', $pvcFitting);
    $breadcrumbs->push('Edit', route('pvc-fitting_edit', $pvcFitting->getId()));
});
