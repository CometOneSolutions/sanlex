<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'pvc-fittings'], function () use ($api) {
        $api->get('/', 'App\Modules\PvcFitting\Controllers\PvcFittingApiController@index');
        $api->get('{pvcFittingId}', 'App\Modules\PvcFitting\Controllers\PvcFittingApiController@show');
        $api->post('/', 'App\Modules\PvcFitting\Controllers\PvcFittingApiController@store');
        $api->patch('{pvcFittingId}', 'App\Modules\PvcFitting\Controllers\PvcFittingApiController@update');
        $api->delete('{pvcFittingId}', 'App\Modules\PvcFitting\Controllers\PvcFittingApiController@destroy');
    });
});
