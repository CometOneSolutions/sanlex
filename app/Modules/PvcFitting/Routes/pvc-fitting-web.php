<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'pvc-fittings'], function () {
        Route::get('/', 'PvcFittingController@index')->name('pvc-fitting_index');
        Route::get('/create', 'PvcFittingController@create')->name('pvc-fitting_create');
        Route::get('{pvcFittingId}/edit', 'PvcFittingController@edit')->name('pvc-fitting_edit');
        Route::get('{pvcFittingId}/print', 'PvcFittingController@print')->name('pvc-fitting_print');
        Route::get('{pvcFittingId}', 'PvcFittingController@show')->name('pvc-fitting_show');
    });
});
