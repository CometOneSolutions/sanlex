<?php

namespace App\Modules\PvcFitting\Services;

use App\Modules\Brand\Models\Brand;
use App\Modules\PvcDiameter\Models\PvcDiameter;
use App\Modules\PvcFitting\Models\PvcFitting;
use App\Modules\PvcType\Models\PvcType;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordService;

interface PvcFittingRecordService extends RecordService
{
    public function create(
        $name,
        Supplier $supplier,
        PvcType $pvcType,
        PvcDiameter $pvcDiameter,
        Brand $brand,
        User $user = null
    );

    public function update(
        PvcFitting $pvcFitting,
        $name,
        Supplier $supplier,
        PvcType $pvcType,
        PvcDiameter $pvcDiameter,
        Brand $brand,
        User $user = null
    );

    public function delete(PvcFitting $pvcFitting);
}
