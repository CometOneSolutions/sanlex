<?php

namespace App\Modules\PvcFitting\Services;

use App\Modules\Brand\Models\Brand;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\PvcDiameter\Models\PvcDiameter;
use App\Modules\PvcFitting\Models\PvcFitting;
use App\Modules\PvcFitting\Repositories\PvcFittingRepository;
use App\Modules\PvcType\Models\PvcType;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class PvcFittingRecordServiceImpl extends RecordServiceImpl implements PvcFittingRecordService
{
    private $pvcFitting;
    private $pvcFittings;
    private $productRecordService;

    protected $availableFilters = [
        ['id' => 'name', 'text' => 'Name'],
    ];

    public function __construct(PvcFittingRepository $pvcFittings, PvcFitting $pvcFitting, ProductRecordService $productRecordService)
    {
        $this->pvcFittings = $pvcFittings;
        $this->pvcFitting = $pvcFitting;
        $this->productRecordService = $productRecordService;
        parent::__construct($pvcFittings);
    }

    public function create(
        $name,
        Supplier $supplier,
        PvcType $pvcType,
        PvcDiameter $pvcDiameter,
        Brand $brand,
        User $user = null
    ) {
        $pvcFitting = new $this->pvcFitting;
        $pvcFitting->setPvcDiameter($pvcDiameter);
        $pvcFitting->setPvcType($pvcType);
        $pvcFitting->setBrand($brand);

        if ($user) {
            $pvcFitting->setUpdatedByUser($user);
        }
        $this->pvcFittings->save($pvcFitting);
        $this->productRecordService->create($name, $supplier, $pvcFitting, $user);
        return $pvcFitting;
    }

    public function update(
        PvcFitting $pvcFitting,
        $name,
        Supplier $supplier,
        PvcType $pvcType,
        PvcDiameter $pvcDiameter,
        Brand $brand,
        User $user = null
    ) {
        $tempPvcFitting = clone $pvcFitting;
        $product = $tempPvcFitting->getProduct();
        $tempPvcFitting->setPvcDiameter($pvcDiameter);
        $tempPvcFitting->setPvcType($pvcType);
        $tempPvcFitting->setBrand($brand);

        if ($user) {
            $tempPvcFitting->setUpdatedByUser($user);
        }
        $this->pvcFittings->save($tempPvcFitting);
        $this->productRecordService->update($product, $name, $supplier, $tempPvcFitting, $user);
        return $tempPvcFitting;
    }

    public function delete(PvcFitting $pvcFitting)
    {
        $this->pvcFittings->delete($pvcFitting);
        return $pvcFitting;
    }

    public function inStock()
    {
        $this->pvcFittings = $this->pvcFittings->inStock();
        return $this;
    }
}
