<?php

namespace App\Modules\PvcFitting\Models;

interface PvcFitting
{
    public function getId();

    public function getDescription();

    public function setDescription($value);
}
