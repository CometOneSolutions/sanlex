<?php

namespace App\Modules\PvcFitting\Models;

use App\Modules\Brand\Models\Brand;
use App\Modules\Brand\Models\BrandModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Product\Traits\Productable;
use App\Modules\PvcDiameter\Models\PvcDiameter;
use App\Modules\PvcDiameter\Models\PvcDiameterModel;
use App\Modules\PvcType\Models\PvcType;
use App\Modules\PvcType\Models\PvcTypeModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;

class PvcFittingModel extends Model implements PvcFitting, UpdatableByUser, Productable
{
    use HasUpdatedByUser;

    protected $table = 'pvc_fittings';

    public function product()
    {
        return $this->morphOne(ProductModel::class, 'productable');
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function brand()
    {
        return $this->belongsTo(BrandModel::class, 'brand_id');
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand(Brand $brand)
    {
        $this->brand()->associate($brand);
        return $this;
    }

    public function getBrandId()
    {
        return (int) $this->brand_id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

    public function pvcType()
    {
        return $this->belongsTo(PvcTypeModel::class, 'pvc_type_id');
    }

    public function getPvcTypeId()
    {
        return (int) $this->pvc_type_id;
    }

    public function getPvcType()
    {
        return $this->pvcType;
    }

    public function setPvcType(PvcType $pvcType)
    {
        $this->pvcType()->associate($pvcType);
        return $this;
    }

    public function pvcDiameter()
    {
        return $this->belongsTo(PvcDiameterModel::class, 'pvc_diameter_id');
    }

    public function getPvcDiameterId()
    {
        return (int) $this->pvc_diameter_id;
    }

    public function getPvcDiameter()
    {
        return $this->pvcDiameter;
    }

    public function setPvcDiameter(PvcDiameter $pvcDiameter)
    {
        $this->pvcDiameter()->associate($pvcDiameter);
        return $this;
    }

    public function isCoil()
    {
        return false;
    }

    public function scopeInStock($query)
    {
        return $query->whereHas('product', function ($product) {
            return $product->whereHas('inventory', function ($inventory) {
                return $inventory->where('stock', '>', 0);
            });
        });
    }

    public function getAssembledProductName()
    {
        $nameAttributes = [
            $this->getProduct()->getSupplier()->getShortCode(),
            $this->getPvcType()->getName(),
            $this->getPvcDiameter()->getName(),
            $this->getBrand()->getName()
        ];

        return  strtoupper(implode("~", $nameAttributes));
    }


    public function scopePvcType($query, PvcType $pvcType)
    {
        return $query->wherePvcTypeId($pvcType->getId());
    }

    public function scopeBrand($query, Brand $brand)
    {
        return $query->whereBrandId($brand->getId());
    }

    public function scopePvcDiameter($query, PvcDiameter $pvcDiameter)
    {
        return $query->wherePvcDiameterId($pvcDiameter->getId());
    }
}
