<?php

namespace App\Modules\PvcFitting\Transformer;

use App\Modules\Brand\Transformer\BrandTransformer;
use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\PvcDiameter\Transformer\PvcDiameterTransformer;
use App\Modules\PvcFitting\Models\PvcFitting;
use App\Modules\PvcType\Transformer\PvcTypeTransformer;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class PvcFittingTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'product', 'pvcDiameter', 'pvcType', 'brand'
    ];

    public function transform(PvcFitting $pvcFitting)
    {
        return [
            'id' => (int) $pvcFitting->getId(),
            'name' => $pvcFitting->getProduct() ? $pvcFitting->getProduct()->getName() : null,
            'supplierId' => $pvcFitting->getProduct() ? $pvcFitting->getProduct()->getSupplier()->getId() : null,
            'pvcDiameterId' => $pvcFitting->getPvcDiameterId(),
            'pvcDiameterText' => null,
            'brandId' => $pvcFitting->getBrandId(),
            'brandText' => null,
            'pvcTypeId' => $pvcFitting->getPvcTypeId(),
            'pvcTypeText' => null,
            'updatedAt' => $pvcFitting->updated_at,
            'editUri' => route('pvc-fitting_edit', $pvcFitting->getId()),
            'showUri' => route('pvc-fitting_show', $pvcFitting->getId()),
        ];
    }

    public function includeProduct(PvcFitting $pvcFitting)
    {
        $product = $pvcFitting->getProduct();
        return $this->item($product, new ProductTransformer);
    }

    public function includePvcType(PvcFitting $pvcFitting)
    {
        $pvcType = $pvcFitting->getPvcType();
        return $this->item($pvcType, new PvcTypeTransformer);
    }

    public function includePvcDiameter(PvcFitting $pvcFitting)
    {
        $pvcDiameter = $pvcFitting->getPvcDiameter();
        return $this->item($pvcDiameter, new PvcDiameterTransformer);
    }

    public function includeBrand(PvcFitting $pvcFitting)
    {
        $brand = $pvcFitting->getBrand();
        return $this->item($brand, new BrandTransformer);
    }
}
