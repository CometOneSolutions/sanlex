import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import draggable from 'vuedraggable'
import Axios from 'axios';
import { ListGenerator } from './ListGenerator';
const FASTENER = 'FASTENER';
const PAINT_ADHESIVE = 'PAINT ADHESIVE';
const PVC_FITTING = 'PVC FITTING';
const STRUCTURAL_STEEL = 'STRUCTURAL STEEL';
const STAINLESS_STEEL = 'STAINLESS STEEL';
const MISC_ACCESSORY = 'MISC ACCESSORY';
const SKYLIGHT = 'SKYLIGHT';
const BUBBLE_CORE = 'BUBBLE CORE';
const FIBERGLASS = 'FIBERGLASS';
const PE_FOAM = 'PE FOAM';


new Vue({
    el: '#summary-order',
    components: {
        SaveButton,
        DeleteButton,
        Timestamp,
        VForm,
        draggable

    },
    data: {
        form: new Form({
            name: null,
            report: null,
            list: [],
            unsortedList: []
        }),

        listGenerator: new ListGenerator,
        dataInitialized: true,
    },

    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        },
    },

    computed: {
        initializationComplete() {
            return this.dataInitialized;
        },

        dragOptions() {
            return {
                animation: 0,
                group: "description",
                disabled: false,
                ghostClass: "ghost"
            };
        }
    },

    methods: {
        generateUnorderedList(val) {
            switch (val) {
                case BUBBLE_CORE:
                    axios.get(`/api/insulations?insulationTypeName=${BUBBLE_CORE}&include=thickness,backingSide&limit=${Number.MAX_SAFE_INTEGER}`).then(response => {
                        this.listGenerator.generateListFromBubbleCore(response);
                        this.form.unsortedList = this.listGenerator.getUnsortedList();
                        this.dataInitialized = true;
                    });
                    break;
                case FASTENER:
                    axios.get(`/api/fasteners?include=fastenerType&limit=${Number.MAX_SAFE_INTEGER}`).then(response => {
                        this.listGenerator.generateListFromFastener(response);
                        this.form.unsortedList = this.listGenerator.getUnsortedList();
                        this.dataInitialized = true;
                    });
                    break;
                case FIBERGLASS:
                    axios.get(`/api/insulations?insulationTypeName=${FIBERGLASS}&include=insulationDensity&limit=${Number.MAX_SAFE_INTEGER}`).then(response => {
                        this.listGenerator.generateListFromFiberGlass(response);
                        this.form.unsortedList = this.listGenerator.getUnsortedList();
                        this.dataInitialized = true;
                    });
                    break;
                case MISC_ACCESSORY:
                    axios.get(`/api/misc-accessories?include=miscAccessoryType&limit=${Number.MAX_SAFE_INTEGER}`).then(response => {
                        this.listGenerator.generateListFromMiscAccessory(response);
                        this.form.unsortedList = this.listGenerator.getUnsortedList();
                        this.dataInitialized = true;
                    });
                    break;
                case PAINT_ADHESIVE:
                    axios.get(`/api/paint-adhesives?include=paintAdhesiveType&limit=${Number.MAX_SAFE_INTEGER}`).then(response => {
                        this.listGenerator.generateListFromPaintAdhesive(response);
                        this.form.unsortedList = this.listGenerator.getUnsortedList();
                        this.dataInitialized = true;
                    });
                    break;
                case PE_FOAM:
                    axios.get(`/api/insulations?insulationTypeName=${PE_FOAM}&include=insulationDensity&limit=${Number.MAX_SAFE_INTEGER}`).then(response => {
                        this.listGenerator.generateListFromPEFoam(response);
                        this.form.unsortedList = this.listGenerator.getUnsortedList();
                        this.dataInitialized = true;
                    });
                    break;
                case PVC_FITTING:
                    axios.get(`/api/pvc-fittings?include=pvcType&limit=${Number.MAX_SAFE_INTEGER}`).then(response => {
                        this.listGenerator.generateListFromPvcFitting(response);
                        this.form.unsortedList = this.listGenerator.getUnsortedList();
                        this.dataInitialized = true;
                    });
                    break;
                case SKYLIGHT:
                    axios.get(`/api/skylights?include=panelProfile,skylightClass,width&limit=${Number.MAX_SAFE_INTEGER}`).then(response => {
                        this.listGenerator.generateListFromSkylight(response);
                        this.form.unsortedList = this.listGenerator.getUnsortedList();
                        this.dataInitialized = true;
                    });
                    break;
                case STAINLESS_STEEL:
                    axios.get(`/api/stainless-steels?include=stainlessSteelType,finish,length&limit=${Number.MAX_SAFE_INTEGER}`).then(response => {
                        this.listGenerator.generateListFromStainlessSteel(response);
                        this.form.unsortedList = this.listGenerator.getUnsortedList();
                        this.dataInitialized = true;
                    });
                    break;
                case STRUCTURAL_STEEL:
                    axios.get(`/api/structural-steels?include=structuralSteelType,finish&limit=${Number.MAX_SAFE_INTEGER}`).then(response => {
                        this.listGenerator.generateListFromStructuralSteel(response);
                        this.form.unsortedList = this.listGenerator.getUnsortedList();
                        this.dataInitialized = true;
                    });
                    break;

            }
        },

        sortAlphabetically() {
            this.form.list = this.listGenerator.sortByName(this.form.list);
        },

        destroy() {
            this.form.deleteWithConfirmation('/api/summary-orders/' + this.form.id).then(response => {
                this.form.successModal(this.form.report.toUpperCase() + ' was removed.').then(() =>
                    window.location = '/master-file/summary-orders/'
                );
            });
        },

        store() {
            this.form.postWithModal('/api/summary-orders', null, this.form.report.toUpperCase() + ' was saved.');
        },

        update() {
            this.form.patch('/api/summary-orders/' + this.form.id).then(response => {
                this.form.successModal(this.form.report.toUpperCase() + ' was updated.').then(() =>
                    window.location = '/master-file/summary-orders/' + this.form.id
                );
            })
        },

        loadData(data) {
            this.form = new Form(data);

            if (this.form.hasSummaryOrderDetails) {
                this.listGenerator.generateListFromSummaryOrderDetails(this.form.summaryOrderDetails.data);
                this.form.list = this.listGenerator.getList();
            }

            this.generateUnorderedList(this.form.report);
        },
    },

    created() {
        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/summary-orders/' + id + '?include=summaryOrderDetails')
                .then(response => {
                    this.loadData(response.data);
                });
        }
    },
    mounted() {
        console.log("Init summary order script...");
    }
});