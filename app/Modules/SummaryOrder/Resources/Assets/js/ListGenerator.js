
export class ListGenerator {
    constructor() {
        this.unsortedList = [];
        this.list = [];
    }
    generateListFromBubbleCore(response) {
        this.unsortedList = response.data.data.map(function (item) {
            let name = `${item.thickness.data.name}${item.backingSide.data.name}`
            return { "name": name };
        });
    }

    generateListFromFastener(response) {
        this.unsortedList = response.data.data.map(function (item) {
            let name = item.fastenerType.data.name;
            return { "name": name };
        });
    }

    generateListFromFiberGlass(response) {
        this.unsortedList = response.data.data.map(function (item) {
            let name = item.insulationDensity.data.name;
            return { "name": name };
        });
    }

    generateListFromMiscAccessory(response) {
        this.unsortedList = response.data.data.map(function (item) {
            let name = item.miscAccessoryType.data.name;
            return { "name": name };
        });
    }

    generateListFromPaintAdhesive(response) {
        this.unsortedList = response.data.data.map(function (item) {
            let name = item.paintAdhesiveType.data.name;
            return { "name": name };
        });
    }
    generateListFromPEFoam(response) {
        this.unsortedList = response.data.data.map(function (item) {
            let name = item.insulationDensity.data.name;
            return { "name": name };
        });
    }

    generateListFromPvcFitting(response) {
        this.unsortedList = response.data.data.map(function (item) {
            let name = item.pvcType.data.name;
            return { "name": name };
        });

    }

    generateListFromSkylight(response) {
        this.unsortedList = response.data.data.map(function (item) {
            let name = `${item.panelProfile.data.name} ${item.skylightClass.data.name} ${item.width.data.name}`
            return { "name": name };
        });
    }

    generateListFromStainlessSteel(response) {
        this.unsortedList = response.data.data.map(function (item) {
            let name = `${item.stainlessSteelType.data.name} / ${item.finish.data.name} ${item.length.data.name}`
            return { "name": name };
        });
    }

    generateListFromStructuralSteel(response) {
        this.unsortedList = response.data.data.map(function (item) {
            let name = `${item.structuralSteelType.data.name} ${item.finish.data.name}`
            return { "name": name };
        });
    }

    generateListFromSummaryOrderDetails(summaryOrderDetails) {
        this.list = summaryOrderDetails;
    }


    //Stuff

    getUnsortedList() {
        this.unsortedList = this.setOrder(this.unsortedList)
        this.unsortedList = this.filterUnique(this.unsortedList)
        this.unsortedList = this.removeAlreadyOrderedItem(this.list, this.unsortedList);

        return this.unsortedList;
    }

    getList() {
        this.list = this.sortByOrder(this.list)
        return this.list;
    }

    setOrder(list) {
        list.forEach((element, index) => {
            element["order"] = index;
        });

        return list;
    }

    filterUnique(list) {
        list = list.filter((array, index, self) =>
            index === self.findIndex((t) => (
                t.name === array.name
            ))
        )
        return list;
    }

    removeAlreadyOrderedItem(list1, list2) {
        for (var list1Counter = 0, list1Length = list1.length; list1Counter < list1Length; list1Counter++) {
            for (var list2Counter = 0, list2Length = list2.length; list2Counter < list2Length; list2Counter++) {
                if (list1[list1Counter].name === list2[list2Counter].name) {
                    list2.splice(list2Counter, 1);
                    list2Length = list2.length;
                }
            }
        }

        return list2;
    }

    sortByOrder(list) {
        list = list.sort(function (a, b) {
            if (a.order < b.order) { return -1; }
            if (a.order > b.order) { return 1; }
            return 0;
        });

        return list;
    }

    sortByName(list) {
        list = list.sort(function (a, b) {
            if (a.name < b.name) { return -1; }
            if (a.name > b.name) { return 1; }
            return 0;
        });

        return list;
    }
}