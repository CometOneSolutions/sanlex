<div class="form-row">
    <div class="form-group col-md-4">
        <label for="inputEmail4" class="col-form-label">Report Name</label>
        <p>@{{form.report}}</p>
    </div>

</div>
<div class="row mt-2">
    <div class="col-md-6">
        <div class="form-group">
            <span class="badge badge-primary mb-2">Arranged List</span> 
            <div class="text-center text-muted" v-if="form.list.length <= 0">
                <small> NO ITEM FOUND </small>
            </div>
            <ul class="list-group" >
                <li class="list-group-item" v-for="element in form.list" :key="element.order">@{{ element.name }}</li>
            </ul>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <span>
                <span class="badge badge-warning mb-2">Unsorted List</span> 
            </span>    
            <div class="text-center text-muted" v-if="form.unsortedList.length <= 0">
                    <small> NO ITEM FOUND </small>
                </div>
            <ul class="list-group" >
                <li class="list-group-item" v-for="element in form.unsortedList" :key="element.order">@{{ element.name }}</li>
            </ul>
        </div>
    </div>
</div>
