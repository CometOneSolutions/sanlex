@extends('app')
@section('breadcrumbs', Breadcrumbs::render('summary-order.show', $summaryOrder))
@section('content')
<section id="summary-order">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Summary Order Record
                        @if(auth()->user()->can('Update [PUS] Summary Order'))
                            <div class="float-right">
                                <a :href="form.editUri" class="btn btn-sm btn-primary" role="button"><i class="fas fa-pencil-alt"></i></a>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <form>
                            @include("summary-orders._list")
                        </form>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <timestamp :name="form.updatedByUser.data.name || 'System'" :time="form.updatedAt"></timestamp>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection

@push('scripts')
<script src="{{ mix('js/summary-order.js') }}"></script>
@endpush