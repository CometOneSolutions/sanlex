<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-form-label">Report Name <span class="text text-danger">*</span></label>
            <select v-model="form.report" class="form-control" :allow-custom="false" id="type" name="type" required :class="{'is-invalid': form.errors.has('report') }" required data-msg-required="Enter report" disabled>

                <option disabled value='' selected>Please select</option>
                @foreach(App\Modules\SummaryOrder\Models\SummaryOrder::REPORTS as $report)
                <option value="{{ $report }}">{{ $report }}</option>
                @endforeach
            </select>
        </div>
    </div>

</div>
{{-- <div class="row mt-2">
    <div class="col-md-6">
        <div class="form-group">
            <button type="button" class="btn btn-primary btn-sm" @click="sortAlphabetically">Sort Alphabetically</button>
        </div>
    </div>
</div> --}}
<div class="row mt-2">
    <div class="col-md-6">
        <div class="form-group">
            <span class="badge badge-primary mb-2">Arranged List</span>
            <div class="text-center text-muted" v-if="form.list.length <= 0">
                <small> DRAG ITEMS HERE </small>
            </div>
            <draggable class="list-group draggable-list-group" tag="ul" v-model="form.list" v-bind="dragOptions">
                <transition-group type="transition" name="flip-list">
                    <li class="list-group-item draggable-list-group-item" v-for="element in form.list" :key="element.order">
                        @{{ element.name }}
                    </li>
                </transition-group>
            </draggable>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <span>
                <span class="badge badge-warning mb-2">Unsorted List</span> <small class="text-muted">(Newly added product will appear here first.)</small>
            </span>
            <div class="text-center text-muted" v-if="form.unsortedList.length <= 0">
                <small> DRAG ITEMS HERE </small>
            </div>
            <draggable class="list-group draggable-list-group" tag="ul" v-model="form.unsortedList" v-bind="dragOptions">
                <transition-group type="transition" name="flip-list">
                    <li class="list-group-item draggable-list-group-item" v-for="element in form.unsortedList" :key="element.order">
                        @{{ element.name }}
                    </li>
                </transition-group>
            </draggable>
        </div>
    </div>
</div>


@push('scripts')
<script src="{{ mix('js/summary-order.js') }}"></script>
@endpush
