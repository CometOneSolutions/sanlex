@extends('app')
@section('breadcrumbs', Breadcrumbs::render('summary-order.edit', $summaryOrder))
@section('content')
<section id="summary-order">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header clearfix">
                        Edit Summary Order Record
                    </div>

                    <v-form @validate="update">
                        <div class="card-body">

                            @include('summary-orders._form')
                        </div>

                        <div class="card-footer">
                            <div class="text-right">
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                            </div>
                        </div>
                    </v-form>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection