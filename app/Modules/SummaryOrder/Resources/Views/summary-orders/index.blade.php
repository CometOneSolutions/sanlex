@extends('app')
@section('breadcrumbs', Breadcrumbs::render('summary-order.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Summary Order
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sorter="sorter" :sort-ascending="sortAscending"
            v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            <a v-on:click="setSorter('report')">
                                Report <i class="fas fa-sort" :class="getSortIcon('report')"></i>
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="item in items" v-if="!isLoading">
                        <td><a :href="item.showUri">@{{ item.report }}</a></td>
              
                    </tr>
                </tbody>
            </table>
        </index>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush