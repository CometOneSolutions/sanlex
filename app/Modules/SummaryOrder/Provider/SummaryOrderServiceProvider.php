<?php

namespace App\Modules\SummaryOrder\Provider;

use App\Modules\SummaryOrder\Models\SummaryOrder;
use App\Modules\SummaryOrder\Models\SummaryOrderModel;
use App\Modules\SummaryOrder\Repositories\EloquentSummaryOrderRepository;
use App\Modules\SummaryOrder\Repositories\SummaryOrderRepository;
use App\Modules\SummaryOrder\Services\SummaryOrderRecordService;
use App\Modules\SummaryOrder\Services\SummaryOrderRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class SummaryOrderServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/SummaryOrder/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(SummaryOrderRecordService::class, SummaryOrderRecordServiceImpl::class);
		$this->app->bind(SummaryOrderRepository::class, EloquentSummaryOrderRepository::class);
		$this->app->bind(SummaryOrder::class, SummaryOrderModel::class);
	}
}
