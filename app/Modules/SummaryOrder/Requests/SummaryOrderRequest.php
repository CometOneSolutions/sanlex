<?php

namespace App\Modules\SummaryOrder\Requests;

use App\Modules\SummaryOrderDetail\Models\SummaryOrderDetailModel;
use App\Modules\SummaryOrderDetail\Services\SummaryOrderDetailRecordService;
use Dingo\Api\Http\FormRequest;

class SummaryOrderRequest extends FormRequest
{
	protected $productRecordService;

	public function __construct(
		SummaryOrderDetailRecordService $summaryOrderDetailRecordService
	) {
		$this->summaryOrderDetailRecordService = $summaryOrderDetailRecordService;
	}

	public function authorize()
	{
		return true;
	}

	public function getReport($index = 'report')
	{
		return $this->input($index);
	}


	public function getSummaryOrderDetails($index = 'list')
	{
		return array_map(function ($itemArray, $index) {
			if (isset($itemArray['id']) && $itemArray['id'] > 0) {
				$summaryOrderDetail = $this->summaryOrderDetailRecordService->getById($itemArray['id']);
			} else {
				$summaryOrderDetail = new SummaryOrderDetailModel();
			}

			$summaryOrderDetail->setName($itemArray['name']);
			$summaryOrderDetail->setOrder($index);

			return $summaryOrderDetail;
		}, $this->input($index), array_keys($this->input($index)));
	}

	public function rules()
	{
		return [
			'report' => 'required|unique:summary_orders,report,' . $this->route('summaryOrderId') ?? null,
		];
	}

	public function messages()
	{
		return [
			'report.required'           => 'Report Name is required.',
			'report.unique'             => 'Report Name already exists.',
		];
	}
}
