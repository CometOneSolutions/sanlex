<?php

namespace App\Modules\SummaryOrder\Requests;

use Dingo\Api\Http\FormRequest;

class DestroySummaryOrder extends SummaryOrderRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Summary Order');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
