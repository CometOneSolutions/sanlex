<?php

namespace App\Modules\SummaryOrder\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateSummaryOrder extends SummaryOrderRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] Summary Order');
	}
}
