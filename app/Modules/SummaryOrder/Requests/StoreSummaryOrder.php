<?php

namespace App\Modules\SummaryOrder\Requests;

use Dingo\Api\Http\FormRequest;

class StoreSummaryOrder extends SummaryOrderRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [MAS] Summary Order');
	}
}
