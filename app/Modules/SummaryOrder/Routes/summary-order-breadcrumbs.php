<?php

Breadcrumbs::register('summary-order.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Summary Orders', route('summary-order_index'));
});
Breadcrumbs::register('summary-order.create', function ($breadcrumbs) {
	$breadcrumbs->parent('summary-order.index');
	$breadcrumbs->push('Create', route('summary-order_create'));
});
Breadcrumbs::register('summary-order.show', function ($breadcrumbs, $summaryOrder) {
	$breadcrumbs->parent('summary-order.index');
	$breadcrumbs->push($summaryOrder->getReport(), route('summary-order_show', $summaryOrder->getId()));
});
Breadcrumbs::register('summary-order.edit', function ($breadcrumbs, $summaryOrder) {
	$breadcrumbs->parent('summary-order.show', $summaryOrder);
	$breadcrumbs->push('Edit', route('summary-order_edit', $summaryOrder->getId()));
});
