<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'summary-orders'], function () {
        Route::get('/', 'SummaryOrderController@index')->name('summary-order_index');
        Route::get('/create', 'SummaryOrderController@create')->name('summary-order_create');
        Route::get('{summaryOrderId}/edit', 'SummaryOrderController@edit')->name('summary-order_edit');
        Route::get('{summaryOrderId}/print', 'SummaryOrderController@print')->name('summary-order_print');
        Route::get('{summaryOrderId}', 'SummaryOrderController@show')->name('summary-order_show');
    });
});
