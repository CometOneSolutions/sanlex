<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'summary-orders'], function () use ($api) {
		$api->get('/', 'App\Modules\SummaryOrder\Controllers\SummaryOrderApiController@index');
		$api->get('{summaryOrderId}', 'App\Modules\SummaryOrder\Controllers\SummaryOrderApiController@show');
		$api->post('/', 'App\Modules\SummaryOrder\Controllers\SummaryOrderApiController@store');
		$api->patch('{summaryOrderId}', 'App\Modules\SummaryOrder\Controllers\SummaryOrderApiController@update');
		$api->delete('{summaryOrderId}', 'App\Modules\SummaryOrder\Controllers\SummaryOrderApiController@destroy');
	});
});
