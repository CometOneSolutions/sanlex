<?php

namespace App\Modules\SummaryOrder\Transformer;

use App\Modules\SummaryOrder\Models\SummaryOrder;
use App\Modules\SummaryOrderDetail\Transformer\SummaryOrderDetailTransformer;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class SummaryOrderTransformer extends UpdatableByUserTransformer
{
	protected $availableIncludes = [
		'summaryOrderDetails'
	];

	public function __construct()
	{
		$this->defaultIncludes = array_merge($this->defaultIncludes, []);
	}

	public function transform(SummaryOrder $summaryOrder)
	{
		return [
			'id'						=> (int) $summaryOrder->getId(),
			'report'					=> $summaryOrder->getReport(),
			'hasSummaryOrderDetails'	=> $summaryOrder->hasSummaryOrderDetails(),
			'showUri'					=> route('summary-order_show', $summaryOrder->id),
			'editUri'					=> route('summary-order_edit', $summaryOrder->id),
			'list'						=> [],
			'unsortedList'				=> [],
			'updatedAt'					=> $summaryOrder->updated_at,
		];
	}

	public function includeSummaryOrderDetails(SummaryOrder $summaryOrder)
	{
		$summaryOrderDetails = $summaryOrder->getSummaryOrderDetails();
		return $this->collection($summaryOrderDetails, new SummaryOrderDetailTransformer);
	}
}
