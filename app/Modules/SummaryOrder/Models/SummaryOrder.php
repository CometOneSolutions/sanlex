<?php

namespace App\Modules\SummaryOrder\Models;



interface SummaryOrder
{
	const REPORTS = [
		'FASTENER',
		'PAINT ADHESIVE',
		'PVC FITTING',
		'STRUCTURAL STEEL',
		'STAINLESS STEEL',
		'MISC ACCESSORY',
		'SKYLIGHT',
		'BUBBLE CORE',
		'FIBERGLASS',
		'PE FOAM',
	];

	public function getId();
}
