<?php

namespace App\Modules\SummaryOrder\Models;

use App\Modules\SummaryOrderDetail\Models\SummaryOrderDetailModel;
use CometOneSolutions\Common\Models\C1Model;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use DateTime;

class SummaryOrderModel extends C1Model implements SummaryOrder, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'summary_orders';

    protected $summaryOrderDetailsToSet = null;

    public function summaryOrderDetails()
    {
        return $this->hasMany(SummaryOrderDetailModel::class, 'summary_order_id');
    }

    public function setSummaryOrderDetails(array $summaryOrderDetails)
    {
        $this->summaryOrderDetailsToSet = $summaryOrderDetails;
        return $this;
    }

    public function getSummaryOrderDetails()
    {
        if ($this->summaryOrderDetailsToSet !== null) {
            return collect($this->summaryOrderDetailsToSet);
        }
        return $this->summaryOrderDetails;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setReport($value)
    {
        $this->report = $value;
        return $this;
    }

    public function getReport()
    {
        return $this->report;
    }

    public function hasSummaryOrderDetails()
    {
        return $this->summaryOrderDetails()->exists();
    }
}
