<?php

namespace App\Modules\SummaryOrder\Repositories;

use App\Modules\SummaryOrder\Models\SummaryOrder;
use CometOneSolutions\Common\Repositories\Repository;

interface SummaryOrderRepository extends Repository
{
	public function save(SummaryOrder $summaryOrder);
	public function delete(SummaryOrder $summaryOrder);
}
