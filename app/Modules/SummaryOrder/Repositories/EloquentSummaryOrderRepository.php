<?php

namespace App\Modules\SummaryOrder\Repositories;

use App\Modules\SummaryOrder\Models\SummaryOrder;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentSummaryOrderRepository extends EloquentRepository implements SummaryOrderRepository
{
	public function __construct(SummaryOrder $summaryOrder)
	{
		parent::__construct($summaryOrder);
	}

	public function save(SummaryOrder $summaryOrder)
	{
		$result =  $summaryOrder->save();
		$summaryOrder->summaryOrderDetails()->sync($summaryOrder->getSummaryOrderDetails());

		return $result;
	}

	public function delete(SummaryOrder $summaryOrder)
	{
		return $summaryOrder->delete();
	}
}
