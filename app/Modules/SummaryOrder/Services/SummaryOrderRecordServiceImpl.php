<?php

namespace App\Modules\SummaryOrder\Services;

use App\Modules\SummaryOrder\Models\SummaryOrder;
use App\Modules\SummaryOrder\Repositories\SummaryOrderRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;
use DateTime;

class SummaryOrderRecordServiceImpl extends RecordServiceImpl implements SummaryOrderRecordService
{
	private $summaryOrder;
	private $summaryOrders;

	protected $availableFilters = [
		['id' => 'report', 'text' => 'Report Name'],
	];
	public function __construct(
		SummaryOrderRepository $summaryOrders,
		SummaryOrder $summaryOrder
	) {
		$this->summaryOrders = $summaryOrders;
		$this->summaryOrder = $summaryOrder;
		parent::__construct($summaryOrders);
	}

	public function create(
		$report,
		array $summaryOrderDetails,
		User $user = null
	) {
		$summaryOrder = $this->summaryOrder;
		$summaryOrder->setReport($report);
		$summaryOrder->setSummaryOrderDetails($summaryOrderDetails);

		if ($user) {
			$summaryOrder->setUpdatedByUser($user);
		}

		$this->summaryOrders->save($summaryOrder);
		return $summaryOrder;
	}

	public function update(
		SummaryOrder $summaryOrder,
		$report,
		array $summaryOrderDetails,
		User $user = null
	) {
		$tmpSummaryOrder = clone $summaryOrder;
		$tmpSummaryOrder->setReport($report);
		$tmpSummaryOrder->setSummaryOrderDetails($summaryOrderDetails);

		if ($user) {
			$tmpSummaryOrder->setUpdatedByUser($user);
		}

		$this->summaryOrders->save($tmpSummaryOrder);

		return $tmpSummaryOrder;
	}

	public function delete(SummaryOrder $summaryOrder)
	{
		$this->summaryOrders->delete($summaryOrder);
		return $summaryOrder;
	}
}
