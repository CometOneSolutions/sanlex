<?php

namespace App\Modules\SummaryOrder\Services;

use App\Modules\SummaryOrder\Models\SummaryOrder;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;
use DateTime;

interface SummaryOrderRecordService extends RecordService
{
	public function create(
		$report,
		array $summaryOrderDetails,
		User $user = null
	);

	public function update(
		SummaryOrder $summaryOrder,
		$report,
		array $summaryOrderDetails,
		User $user = null
	);

	public function delete(SummaryOrder $summaryOrder);
}
