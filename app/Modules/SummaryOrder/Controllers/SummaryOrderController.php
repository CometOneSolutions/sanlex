<?php

namespace App\Modules\SummaryOrder\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\SummaryOrder\Services\SummaryOrderRecordService;


class SummaryOrderController extends Controller
{
	private $service;

	public function __construct(SummaryOrderRecordService $summaryOrderRecordService)
	{
		$this->service = $summaryOrderRecordService;
		$this->middleware('permission:Read [MAS] Summary Order')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Summary Order')->only('create');
		$this->middleware('permission:Update [MAS] Summary Order')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'report,-created_at',
			'sortAscending' => true,
			'baseUrl' => '/api/summary-orders'
		]);
		return view('summary-orders.index');
	}

	// public function create()
	// {
	// 	JavaScript::put(['id' => null]);
	// 	return view('summary-orders.create');
	// }

	public function show($id)
	{
		$summaryOrder = $this->service->getById($id);

		JavaScript::put([
			'id' => $id
		]);

		return view('summary-orders.show', compact('summaryOrder'));
	}

	public function edit($id)
	{
		$summaryOrder = $this->service->getById($id);

		JavaScript::put([
			'id' => $id
		]);

		return view('summary-orders.edit', compact('summaryOrder'));
	}
}
