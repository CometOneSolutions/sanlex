<?php

namespace App\Modules\SummaryOrder\Controllers;

use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\SummaryOrder\Services\SummaryOrderRecordService;
use App\Modules\SummaryOrder\Transformer\SummaryOrderTransformer;
use App\Modules\SummaryOrder\Requests\UpdateSummaryOrder;

class SummaryOrderApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        SummaryOrderRecordService $summaryOrderRecordService,
        SummaryOrderTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($summaryOrderRecordService, $transformer);
        $this->service = $summaryOrderRecordService;
        $this->transformer = $transformer;
    }

    // public function store(StoreSummaryOrder $request)
    // {
    // 	$summaryOrder = \DB::transaction(function () use ($request) {
    // 		return $this->service->create(
    // 			$request->getReport(),
    // 			$request->getSummaryOrderDetails(),
    // 			$request->user()
    // 		);
    // 	});
    // 	return $this->response->item($summaryOrder, $this->transformer)->setStatusCode(201);
    // }

    public function update($summaryOrderId, UpdateSummaryOrder $request)
    {
        $summaryOrder = \DB::transaction(function () use ($summaryOrderId, $request) {
            $summaryOrder   = $this->service->getById($summaryOrderId);

            return $this->service->update(
                $summaryOrder,
                $request->getReport(),
                $request->getSummaryOrderDetails(),
                $request->user()
            );
        });
        return $this->response->item($summaryOrder, $this->transformer)->setStatusCode(200);
    }

    // public function destroy($summaryOrderId, DestroySummaryOrder $request)
    // {
    // 	$summaryOrder = $this->service->getById($summaryOrderId);
    // 	$this->service->delete($summaryOrder);
    // 	return $this->response->item($summaryOrder, $this->transformer)->setStatusCode(200);
    // }
}
