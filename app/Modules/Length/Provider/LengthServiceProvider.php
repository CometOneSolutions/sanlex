<?php

namespace App\Modules\Length\Provider;

use App\Modules\Length\Models\Length;
use App\Modules\Length\Models\LengthModel;
use App\Modules\Length\Repositories\EloquentLengthRepository;
use App\Modules\Length\Repositories\LengthRepository;
use App\Modules\Length\Services\LengthRecordService;
use App\Modules\Length\Services\LengthRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class LengthServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Length/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(LengthRecordService::class, LengthRecordServiceImpl::class);
        $this->app->bind(LengthRepository::class, EloquentLengthRepository::class);
        $this->app->bind(Length::class, LengthModel::class);
    }
}
