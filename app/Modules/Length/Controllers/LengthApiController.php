<?php

namespace App\Modules\Length\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Length\Services\LengthRecordService;
use App\Modules\Length\Transformer\LengthTransformer;
use App\Modules\Length\Requests\StoreLength;
use App\Modules\Length\Requests\UpdateLength;
use App\Modules\Length\Requests\DestroyLength;

class LengthApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        LengthRecordService $lengthRecordService,
        LengthTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($lengthRecordService, $transformer);
        $this->service = $lengthRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreLength $request)
    {
        $length = \DB::transaction(function () use ($request) {
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->create(
                $name,

                $user
            );
        });
        return $this->response->item($length, $this->transformer)->setStatusCode(201);
    }

    public function update($lengthId, UpdateLength $request)
    {
        $length = \DB::transaction(function () use ($lengthId, $request) {
            $length   = $this->service->getById($lengthId);
            $name       = $request->getName();

            $user       = $request->user();

            return $this->service->update(
                $length,
                $name,

                $user
            );
        });
        return $this->response->item($length, $this->transformer)->setStatusCode(200);
    }

    public function destroy($lengthId, DestroyLength $request)
    {
        $length = $this->service->getById($lengthId);
        $this->service->delete($length);
        return $this->response->item($length, $this->transformer)->setStatusCode(200);
    }
}
