<?php

namespace App\Modules\Length\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Length\Services\LengthRecordService;


class LengthController extends Controller
{
    private $service;

    public function __construct(LengthRecordService $lengthRecordService)
    {
        $this->service = $lengthRecordService;
        $this->middleware('permission:Read [MAS] Length')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Length')->only('create');
        $this->middleware('permission:Update [MAS] Length')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/lengths'
        ]);
        return view('lengths.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('lengths.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $length = $this->service->getById($id);
        return view('lengths.show', compact('length'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $length = $this->service->getById($id);
        return view('lengths.edit', compact('length'));
    }
}
