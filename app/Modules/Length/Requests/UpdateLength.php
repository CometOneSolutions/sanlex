<?php

namespace App\Modules\Length\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateLength extends LengthRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Length');
    }
}
