<?php

namespace App\Modules\Length\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyLength extends LengthRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Length');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
