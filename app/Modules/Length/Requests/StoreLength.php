<?php

namespace App\Modules\Length\Requests;

use Dingo\Api\Http\FormRequest;

class StoreLength extends LengthRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Length');
    }
}
