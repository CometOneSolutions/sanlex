<?php

namespace App\Modules\Length\Transformer;

use App\Modules\Length\Models\Length;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class LengthTransformer extends UpdatableByUserTransformer
{
    public function transform(Length $length)
    {
        return [
            'id' => (int) $length->getId(),
            'text' => $length->getName(),
            'name' => $length->getName(),
            'updatedAt' => $length->updated_at,
            'editUri' => route('length_edit', $length->getId()),
            'showUri' => route('length_show', $length->getId()),
        ];
    }
}
