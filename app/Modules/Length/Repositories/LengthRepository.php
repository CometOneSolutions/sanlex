<?php

namespace App\Modules\Length\Repositories;

use App\Modules\Length\Models\Length;
use CometOneSolutions\Common\Repositories\Repository;

interface LengthRepository extends Repository
{
    public function save(Length $length);
    public function delete(Length $length);
}
