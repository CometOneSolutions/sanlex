<?php

namespace App\Modules\Length\Repositories;

use App\Modules\Length\Models\Length;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentLengthRepository extends EloquentRepository implements LengthRepository
{
    public function __construct(Length $length)
    {
        parent::__construct($length);
    }

    public function save(Length $length)
    {
        return $length->save();
    }

    public function delete(Length $length)
    {
        return $length->delete();
    }
}
