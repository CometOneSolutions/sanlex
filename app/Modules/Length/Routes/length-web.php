<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'lengths'], function () {
        Route::get('/', 'LengthController@index')->name('length_index');
        Route::get('/create', 'LengthController@create')->name('length_create');
        Route::get('{lengthId}/edit', 'LengthController@edit')->name('length_edit');
        Route::get('{lengthId}/print', 'LengthController@print')->name('length_print');
        Route::get('{lengthId}', 'LengthController@show')->name('length_show');
    });
});
