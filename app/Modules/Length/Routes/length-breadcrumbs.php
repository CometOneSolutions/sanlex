<?php

Breadcrumbs::register('length.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Lengths', route('length_index'));
});
Breadcrumbs::register('length.create', function ($breadcrumbs) {
    $breadcrumbs->parent('length.index');
    $breadcrumbs->push('Create', route('length_create'));
});
Breadcrumbs::register('length.show', function ($breadcrumbs, $length) {
    $breadcrumbs->parent('length.index');
    $breadcrumbs->push($length->getName(), route('length_show', $length->getId()));
});
Breadcrumbs::register('length.edit', function ($breadcrumbs, $length) {
    $breadcrumbs->parent('length.show', $length);
    $breadcrumbs->push('Edit', route('length_edit', $length->getId()));
});
