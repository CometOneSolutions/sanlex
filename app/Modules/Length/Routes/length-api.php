<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'lengths'], function () use ($api) {
        $api->get('/', 'App\Modules\Length\Controllers\LengthApiController@index');
        $api->get('{lengthId}', 'App\Modules\Length\Controllers\LengthApiController@show');
        $api->post('/', 'App\Modules\Length\Controllers\LengthApiController@store');
        $api->patch('{lengthId}', 'App\Modules\Length\Controllers\LengthApiController@update');
        $api->delete('{lengthId}', 'App\Modules\Length\Controllers\LengthApiController@destroy');
    });
});
