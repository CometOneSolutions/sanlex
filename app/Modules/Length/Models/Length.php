<?php

namespace App\Modules\Length\Models;


interface Length
{
    public function getId();

    public function getName();

    public function setName($value);
}
