<?php

namespace App\Modules\Length\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Skylight\Models\SkylightModel;
use App\Modules\Insulation\Models\InsulationModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use App\Modules\MiscAccessory\Models\MiscAccessoryModel;
use App\Modules\StainlessSteel\Models\StainlessSteelModel;
use App\Modules\StructuralSteel\Models\StructuralSteelModel;


class LengthModel extends Model implements Length, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'lengths';


    public function structuralSteels()
    {
        return $this->hasMany(StructuralSteelModel::class, 'length_id');
    }

    public function stainlessSteels()
    {
        return $this->hasMany(StainlessSteelModel::class, 'length_id');
    }

    public function skylights()
    {
        return $this->hasMany(SkylightModel::class, 'length_id');
    }

    public function miscAccessories()
    {
        return $this->hasMany(MiscAccessoryModel::class, 'length_id');
    }

    public function insulations()
    {
        return $this->hasMany(InsulationModel::class, 'length_id');
    }

    public function getInsulations()
    {
        return $this->insulations;
    }

    public function getMiscAccessories()
    {
        return $this->miscAccessories;
    }

    public function getSkylights()
    {
        return $this->skylights;
    }

    public function getStainlessSteels()
    {
        return $this->stainlessSteels;
    }

    public function getStructuralSteels()
    {
        return $this->structuralSteels;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
