<?php

namespace App\Modules\Length\Services;

use App\Modules\Length\Models\Length;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface LengthRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        Length $length,
        $name,
        User $user = null
    );

    public function delete(Length $length);
}
