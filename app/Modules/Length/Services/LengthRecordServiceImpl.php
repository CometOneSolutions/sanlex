<?php

namespace App\Modules\Length\Services;

use App\Modules\Length\Models\Length;
use App\Modules\Length\Repositories\LengthRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class LengthRecordServiceImpl extends RecordServiceImpl implements LengthRecordService
{
    use CanUpdateProductName;

    private $length;
    private $lengths;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(LengthRepository $lengths, Length $length)
    {
        $this->lengths = $lengths;
        $this->length = $length;
        parent::__construct($lengths);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $length = new $this->length;
        $length->setName($name);

        if ($user) {
            $length->setUpdatedByUser($user);
        }
        $this->lengths->save($length);
        return $length;
    }

    public function update(
        Length $length,
        $name,
        User $user = null
    ) {
        $tempLength = clone $length;
        $tempLength->setName($name);
        if ($user) {
            $tempLength->setUpdatedByUser($user);
        }
        $this->lengths->save($tempLength);

        $this->updateProductableProductNames([
            $tempLength->structuralSteels(),
            $tempLength->stainlessSteels(),
            $tempLength->skylights(),
            $tempLength->miscAccessories(),
            $tempLength->insulations(),
        ]);

        return $tempLength;
    }

    public function delete(Length $length)
    {
        $this->lengths->delete($length);
        return $length;
    }
}
