<?php

namespace App\Modules\Product\Models;



use App\Modules\Supplier\Models\Supplier;
use App\Modules\Uom\Models\Uom;

interface Product
{

    public function getId();

    public function setSupplier(Supplier $supplier);

    public function getSupplier();

    public function setUom(Uom $uom);

    public function getUom();

    public function setName($value);

    public function getName();
}
