<?php

namespace App\Modules\Product\Models;

use App\Modules\Uom\Models\Uom;
use App\Modules\Size\Models\Size;
use App\Modules\Brand\Models\Brand;
use App\Modules\Color\Models\Color;
use App\Modules\Width\Models\Width;
use App\Modules\Uom\Models\UomModel;
use App\Modules\Finish\Models\Finish;
use App\Modules\Length\Models\Length;
use App\Modules\Coil\Models\CoilModel;
use App\Modules\Coating\Models\Coating;
use App\Modules\PvcType\Models\PvcType;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Product\Traits\Productable;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\DimensionX\Models\DimensionX;
use App\Modules\DimensionY\Models\DimensionY;
use App\Modules\Fastener\Models\FastenerModel;
use App\Modules\Skylight\Models\SkylightModel;
use App\Modules\Supplier\Models\SupplierModel;
use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\PvcDiameter\Models\PvcDiameter;
use App\Modules\Accessory\Models\AccessoryModel;
use App\Modules\Inventory\Models\InventoryModel;
use App\Modules\Receiving\Models\ReceivingModel;
use App\Modules\FastenerType\Models\FastenerType;
use App\Modules\PanelProfile\Models\PanelProfile;
use App\Modules\Insulation\Models\InsulationModel;
use App\Modules\PvcFitting\Models\PvcFittingModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use App\Modules\SkylightClass\Models\SkylightClass;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use App\Modules\InsulationType\Models\InsulationType;
use App\Modules\JobOrders\Models\JobOrderDetailModel;
use App\Modules\MiscAccessory\Models\MiscAccessoryModel;
use App\Modules\PaintAdhesive\Models\PaintAdhesiveModel;
use App\Modules\StainlessSteel\Models\StainlessSteelModel;
use App\Modules\FastenerDimension\Models\FastenerDimension;
use App\Modules\InsulationDensity\Models\InsulationDensity;
use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use App\Modules\StructuralSteel\Models\StructuralSteelModel;
use App\Modules\StainlessSteelType\Models\StainlessSteelType;
use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use App\Modules\PurchaseOrderDetail\Models\PurchaseOrderDetailModel;
use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetailModel;
use App\Modules\PurchaseOrderAccessoryDetail\Models\PurchaseOrderAccessoryDetailModel;

class ProductModel extends Model implements Product, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'products';

    public function productable()
    {
        return $this->morphTo();
    }

    public function supplier()
    {
        return $this->belongsTo(SupplierModel::class, 'supplier_id');
    }

    public function coil()
    {
        return $this->belongsTo(CoilModel::class, 'productable_id')->whereProductableType(CoilModel::class);
    }

    public function fastener()
    {
        return $this->belongsTo(FastenerModel::class, 'productable_id')->whereProductableType(FastenerModel::class);
    }

    public function paintAdhesive()
    {
        return $this->belongsTo(PaintAdhesiveModel::class, 'productable_id')->whereProductableType(PaintAdhesiveModel::class);
    }

    public function accessory()
    {
        return $this->belongsTo(AccessoryModel::class, 'productable_id')->whereProductableType(AccessoryModel::class);
    }

    public function skylight()
    {
        return $this->belongsTo(SkylightModel::class, 'productable_id')->whereProductableType(SkylightModel::class);
    }

    public function insulation()
    {
        return $this->belongsTo(InsulationModel::class, 'productable_id')->whereProductableType(InsulationModel::class);
    }

    public function pvcFitting()
    {
        return $this->belongsTo(PvcFittingModel::class, 'productable_id')->whereProductableType(PvcFittingModel::class);
    }

    public function structuralSteel()
    {
        return $this->belongsTo(StructuralSteelModel::class, 'productable_id')->whereProductableType(StructuralSteelModel::class);
    }

    public function stainlessSteel()
    {
        return $this->belongsTo(StainlessSteelModel::class, 'productable_id')->whereProductableType(StainlessSteelModel::class);
    }

    public function miscAccessory()
    {
        return $this->belongsTo(MiscAccessoryModel::class, 'productable_id')->whereProductableType(MiscAccessoryModel::class);
    }

    public function purchaseOrderDetails()
    {
        return $this->hasMany(PurchaseOrderDetailModel::class, 'product_id');
    }

    public function purchaseOrderAccessoryDetails()
    {
        return $this->hasMany(PurchaseOrderAccessoryDetailModel::class, 'product_id');
    }

    public function receivings()
    {
        return $this->hasMany(ReceivingModel::class, 'product_id');
    }

    public function receivingAccessoryDetails()
    {
        return $this->hasMany(ReceivingAccessoryDetailModel::class, 'product_id');
    }

    public function uom()
    {
        return $this->belongsTo(UomModel::class, 'uom_id');
    }

    public function inventories()
    {
        return $this->hasMany(InventoryModel::class, 'product_id');
    }

    public function inventory()
    {
        return $this->hasOne(InventoryModel::class, 'product_id');
    }

    public function getInventories()
    {
        return $this->inventories;
    }

    public function getInventory()
    {
        return $this->inventory;
    }

    public function setSupplier(Supplier $supplier)
    {
        $this->supplier()->associate($supplier);
        return $this;
    }

    public function setProductable(Productable $productable)
    {
        $this->productable()->associate($productable);
        return $this;
    }

    public function getProductable()
    {
        return $this->productable;
    }

    public function getReceivings()
    {
        return $this->receivings;
    }

    public function getSupplier()
    {
        return $this->supplier;
    }

    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    public function getUom()
    {
        return $this->uom;
    }

    public function setUom(Uom $uom)
    {
        $this->uom()->associate($uom);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getClassName()
    {
        if ($this->productable_type == 'App\Modules\Accessory\Models\AccessoryModel') {
            return 'Accessory';
        } elseif ($this->productable_type == "BackingModel") {
            return 'Backing';
        } elseif ($this->productable_type == 'App\Modules\Insulation\Models\InsulationModel') {
            return 'Insulation';
        } elseif ($this->productable_type == 'App\Modules\Screw\Models\ScrewModel') {
            return 'Screw';
        } elseif ($this->productable_type == 'App\Modules\Skylight\Models\SkylightModel') {
            return 'Skylight';
        } elseif ($this->productable_type == 'App\Modules\MiscAccessory\Models\MiscAccessoryModel') {
            return 'Misc Accessory';
        } elseif ($this->productable_type == 'App\Modules\StainlessSteel\Models\StainlessSteelModel') {
            return 'Stainless Steel';
        } elseif ($this->productable_type == 'App\Modules\StructuralSteel\Models\StructuralSteelModel') {
            return 'Structural Steel';
        } elseif ($this->productable_type == 'App\Modules\PaintAdhesive\Models\PaintAdhesiveModel') {
            return 'Paint Adhesive';
        } elseif ($this->productable_type == 'App\Modules\Fastener\Models\FastenerModel') {
            return 'Fastener';
        } elseif ($this->productable_type == 'App\Modules\PvcFitting\Models\PvcFittingModel') {
            return 'Pvc Fitting';
        }
    }

    public function getPendingQuantity()
    {
        if ($this->productable_type  == "App\Modules\Coil\Models\CoilModel") {
            return $this->purchaseOrderDetails()->inTransit()->sum('ordered_quantity');
        }
        
        $orderedQuantity = $this->purchaseOrderAccessoryDetails()->open()->sum('ordered_quantity');
        $receivedQuantity = $this->receivingAccessoryDetails()->open()->received()->sum('quantity');

        return $orderedQuantity - $receivedQuantity;
    }

    public function scopeHasGoodStock($query)
    {
        return $query->whereHas('inventory', function ($inventory) {
            return $inventory->hasGoodStock();
        });
    }

    public function scopeHasGoodStockOrHasPending($query)
    {
        return $query->hasGoodStock()->orWhere(function ($query2) {
            return $query2->hasPending();
        });
    }

    public function getStockQuantity()
    {
        return $this->inventory()->sum('stock');
    }

    public function getGoodStockQuantity()
    {
        return $this->getInventory() ? $this->getInventory()->inventoryDetails()->condition('Good')->get()->sum(function ($inventoryDetail) {
            return $inventoryDetail->getQuantity();
        }) : 0;
    }

    public function getBadStockQuantity()
    {
        return $this->getInventory() ? $this->getInventory()->inventoryDetails()->condition('Bad')->get()->sum(function ($inventoryDetail) {
            return $inventoryDetail->getQuantity();
        }) : 0;
    }

    public function getFormattedPendingQuantity()
    {
        $qty = $this->getPendingQuantity();

        if ($qty == 0) {
            return '-';
        }

        return number_format($qty);
    }

    public function getDraftJobOrderQuantity()
    {
        $inventory = $this->getInventory();
        if ($inventory == null) {
            return 0;
        }
        return JobOrderDetailModel::whereIn(
            'inventory_detail_id',
            $inventory->inventoryDetails()->pluck('id')
        )->whereHas('jobOrder', function ($jo) {
            return $jo->whereStatus('Draft');
        })->sum('weight');
    }

    public function getFormattedStockQuantity()
    {
        // $qty = $this->getGoodStockQuantity() - $this->getDraftJobOrderQuantity();
        $qty = $this->getGoodStockQuantity();

        if ($qty == 0) {
            return '-';
        }

        return number_format($qty, 3);
    }

    public function getFormattedBadStockQuantity()
    {
        $qty = $this->getBadStockQuantity();

        if ($qty == 0) {
            return '--';
        }

        return number_format($qty, 3);
    }

    public function getNumberFormattedPendingQuantity()
    {
        $qty = $this->getPendingQuantity();

        if ($qty == 0) {
            return '-';
        }

        return number_format($qty, 0);
    }

    public function getNumberFormattedStockQuantity()
    {
        $qty = $this->getStockQuantity();

        if ($qty == 0) {
            return '-';
        }

        return number_format($qty, 0);
    }

    public function getInsulationNumberFormattedPendingQuantity()
    {
        $qty = $this->getPendingQuantity();

        if ($qty == 0) {
            return '-';
        }

        return ($this->is_decimal($qty)) ? number_format($qty, 2) : number_format($qty, 0);
    }

    public function getInsulationNumberFormattedStockQuantity()
    {
        $qty = $this->getStockQuantity();

        if ($qty == 0) {
            return '-';
        }

        return ($this->is_decimal($qty)) ? number_format($qty, 2) : number_format($qty, 0);
    }

    private function is_decimal($val)
    {
        return is_numeric($val) && floor($val) != $val;
    }

    public static function getRowCount($widthDetails)
    {
        // dd($widthDetails);
        $count = 0;
        foreach ($widthDetails as $details) {
            $count += $details->count();
        }

        return $count;
    }

    public static function getDeepRowCount($widthDetails)
    {
        $count = 0;
        foreach ($widthDetails as $details) {
            foreach ($details as $subdetail) {
                $count += $subdetail->count();
            }
        }

        return $count;
    }

    public function scopeInStock($query)
    {
        return $query->whereHas('inventory', function ($inventory) {
            return $inventory->where('stock', '>', 0);
        });
    }

    public function scopeHasPending($query)
    {
        return $query->whereHas('purchaseOrderDetails', function ($purchaseOrderDetails) {
            return $purchaseOrderDetails->inTransit()->where('ordered_quantity', '>', 0);
        })->orWhereHas('purchaseOrderAccessoryDetails', function ($purchaseOrderAccessoryDetails) {
            return $purchaseOrderAccessoryDetails->open()->where('ordered_quantity', '>', 0);
        });
    }

    public function scopeInStockOrHasPending($query)
    {
        return $query->inStock()->orWhere(function ($query2) {
            return $query2->hasPending();
        });
    }

    public function scopeCoilColor($query, Color $color)
    {
        return $query->whereHas('coil', function ($coil) use ($color) {
            return $coil->color($color);
        });
    }

    public function scopeCoilWidthIsStandard($query)
    {
        return $query->whereHas('coil', function ($coil) {
            return $coil->whereHas('width', function ($width) {
                return $width->whereIsStandard(true);
            });
        });
    }

    public function scopeCoilThickness($query, Thickness $thickness)
    {
        return $query->whereHas('coil', function ($coil) use ($thickness) {
            return $coil->thickness($thickness);
        });
    }

    public function scopeCoilWidth($query, Width $width)
    {
        return $query->whereHas('coil', function ($coil) use ($width) {
            return $coil->width($width);
        });
    }

    public function scopeCoilCoating($query, Coating $coating)
    {
        return $query->whereHas('coil', function ($coil) use ($coating) {
            return $coil->coating($coating);
        });
    }

    public function scopeSupplier($query, Supplier $supplier)
    {
        return $query->where('supplier_id', $supplier->getId());
    }

    public function scopeNonStandards($query)
    {
        return $query->whereHas('coil', function ($coil) {
            return $coil->whereHas('width', function ($width) {
                return $width->whereIsStandard(false);
            });
        });
    }

    public function scopeGINonStandards($query)
    {
        return $query->whereHas('coil', function ($coil) {
            return $coil->whereHas('width', function ($width) {
                return $width->whereIsStandard(false);
            })->whereHas('color', function ($color) {
                return $color->whereName('GI');
            });
        });
    }

    public function scopeStainlessSteelNonStandards($query)
    {
        return $query->whereHas('coil', function ($coil) {
            return $coil->whereHas('width', function ($width) {
                return $width->whereIsStandard(false);
            })->whereHas('color', function ($color) {
                return $color->whereName('STAINLESS STEEL');
            });
        });
    }

    public function scopeCoilByColor($query, $name)
    {
        return $query->whereHas('coil', function ($coil) use ($name){
            $coil->whereHas('color', function ($query) use ($name){
                return $query->whereName($name);
            });
        });
    }

    public function scopeColoredNonStandards($query)
    {
        return $query->whereHas('coil', function ($coil) {
            return $coil->whereHas('width', function ($width) {
                return $width->whereIsStandard(false);
            })->whereHas('color', function ($color) {
                return $color->where('name', '!=', 'GI')
                    ->where('name', '!=', 'STAINLESS STEEL');
            });
        });
    }

    public function scopeFastenerSize($query, Size $size)
    {
        return $query->whereHas('fastener', function ($fastener) use ($size) {
            return $fastener->size($size);
        });
    }

    public function scopeFastenerFinish($query, Finish $finish)
    {
        return $query->whereHas('fastener', function ($fastener) use ($finish) {
            return $fastener->finish($finish);
        });
    }

    public function scopeFastenerDimension($query, FastenerDimension $fastenerDimension)
    {
        return $query->whereHas('fastener', function ($fastener) use ($fastenerDimension) {
            return $fastener->fastenerDimension($fastenerDimension);
        });
    }

    public function scopeFastenerType($query, FastenerType $fastenerType)
    {
        return $query->whereHas('fastener', function ($fastener) use ($fastenerType) {
            return $fastener->fastenerType($fastenerType);
        });
    }

    public function scopePaintAdhesiveSize($query, Size $size)
    {
        return $query->whereHas('paintAdhesive', function ($paintAdhesive) use ($size) {
            return $paintAdhesive->size($size);
        });
    }

    public function scopePaintAdhesiveColor($query, Color $color)
    {
        return $query->whereHas('paintAdhesive', function ($paintAdhesive) use ($color) {
            return $paintAdhesive->color($color);
        });
    }
    public function scopePaintAdhesiveBrand($query,  Brand $brand)
    {
        return $query->whereHas('paintAdhesive', function ($paintAdhesive) use ($brand) {
            return $paintAdhesive->brand($brand);
        });
    }

    public function scopePaintAdhesiveType($query, PaintAdhesiveType $paintAdhesiveType)
    {
        return $query->whereHas('paintAdhesive', function ($paintAdhesive) use ($paintAdhesiveType) {
            return $paintAdhesive->paintAdhesiveType($paintAdhesiveType);
        });
    }

    public function scopePvcType($query, PvcType $pvcType)
    {
        return $query->whereHas('pvcFitting', function ($pvcFitting) use ($pvcType) {
            return $pvcFitting->pvcType($pvcType);
        });
    }

    public function scopePvcFittingBrand($query, Brand $brand)
    {
        return $query->whereHas('pvcFitting', function ($pvcFitting) use ($brand) {
            return $pvcFitting->brand($brand);
        });
    }

    public function scopePvcDiameter($query, PvcDiameter $pvcDiameter)
    {
        return $query->whereHas('pvcFitting', function ($pvcFitting) use ($pvcDiameter) {
            return $pvcFitting->pvcDiameter($pvcDiameter);
        });
    }

    public function scopeStructuralSteelType($query, StructuralSteelType $structuralSteelType)
    {
        return $query->whereHas('structuralSteel', function ($structuralSteel) use ($structuralSteelType) {
            return $structuralSteel->structuralSteelType($structuralSteelType);
        });
    }

    public function scopeStructuralSteelFinish($query, Finish $finish)
    {
        return $query->whereHas('structuralSteel', function ($structuralSteel) use ($finish) {
            return $structuralSteel->finish($finish);
        });
    }

    public function scopeStructuralSteelDimensionX($query, DimensionX $dimensionX)
    {
        return $query->whereHas('structuralSteel', function ($structuralSteel) use ($dimensionX) {
            return $structuralSteel->dimensionX($dimensionX);
        });
    }
    public function scopeStructuralSteelDimensionY($query, DimensionY $dimensionY)
    {
        return $query->whereHas('structuralSteel', function ($structuralSteel) use ($dimensionY) {
            return $structuralSteel->dimensionY($dimensionY);
        });
    }

    public function scopeStructuralSteelLength($query, Length $length)
    {
        return $query->whereHas('structuralSteel', function ($structuralSteel) use ($length) {
            return $structuralSteel->length($length);
        });
    }

    public function scopeStructuralSteelThickness($query, Thickness $thickness)
    {
        return $query->whereHas('structuralSteel', function ($structuralSteel) use ($thickness) {
            return $structuralSteel->thickness($thickness);
        });
    }

    public function scopeStainlessSteelType($query, StainlessSteelType $stainlessSteelType)
    {
        return $query->whereHas('stainlessSteel', function ($stainlessSteel) use ($stainlessSteelType) {
            return $stainlessSteel->stainlessSteelType($stainlessSteelType);
        });
    }

    public function scopeStainlessSteelFinish($query, Finish $finish)
    {
        return $query->whereHas('stainlessSteel', function ($stainlessSteel) use ($finish) {
            return $stainlessSteel->finish($finish);
        });
    }

    public function scopeStainlessSteelLength($query, Length $length)
    {
        return $query->whereHas('stainlessSteel', function ($stainlessSteel) use ($length) {
            return $stainlessSteel->length($length);
        });
    }

    public function scopeStainlessSteelThickness($query, Thickness $thickness)
    {
        return $query->whereHas('stainlessSteel', function ($stainlessSteel) use ($thickness) {
            return $stainlessSteel->thickness($thickness);
        });
    }

    public function scopeStainlessSteelWidth($query, Width $width)
    {
        return $query->whereHas('stainlessSteel', function ($stainlessSteel) use ($width) {
            return $stainlessSteel->width($width);
        });
    }

    public function scopeMiscAccessoryType($query, MiscAccessoryType $miscAccessoryType)
    {
        return $query->whereHas('miscAccessory', function ($miscAccessory) use ($miscAccessoryType) {
            return $miscAccessory->miscAccessoryType($miscAccessoryType);
        });
    }

    public function scopeMiscAccessoryLength($query, Length $length)
    {
        return $query->whereHas('miscAccessory', function ($miscAccessory) use ($length) {
            return $miscAccessory->length($length);
        });
    }
    public function scopeMiscAccessoryWidth($query, Width $width)
    {
        return $query->whereHas('miscAccessory', function ($miscAccessory) use ($width) {
            return $miscAccessory->width($width);
        });
    }

    public function scopeMiscAccessoryBackingSide($query, BackingSide $backingSide)
    {
        return $query->whereHas('miscAccessory', function ($miscAccessory) use ($backingSide) {
            return $miscAccessory->backingSide($backingSide);
        });
    }

    public function scopeSkylightPanelProfile($query, PanelProfile $panelProfile)
    {
        return $query->whereHas('skylight', function ($skylight) use ($panelProfile) {
            return $skylight->panelProfile($panelProfile);
        });
    }
    public function scopeSkylightClass($query, SkylightClass $skylightClass)
    {
        return $query->whereHas('skylight', function ($skylight) use ($skylightClass) {
            return $skylight->skylightClass($skylightClass);
        });
    }
    public function scopeSkylightWidth($query, Width $width)
    {
        return $query->whereHas('skylight', function ($skylight) use ($width) {
            return $skylight->width($width);
        });
    }
    public function scopeSkylightLength($query, Length $length)
    {
        return $query->whereHas('skylight', function ($skylight) use ($length) {
            return $skylight->length($length);
        });
    }
    public function scopeSkylightThickness($query, Thickness $thickness)
    {
        return $query->whereHas('skylight', function ($skylight) use ($thickness) {
            return $skylight->thickness($thickness);
        });
    }

    public function scopeInsulationType($query, InsulationType $insulationType)
    {
        return $query->whereHas('insulation', function ($insulation) use ($insulationType) {
            return $insulation->insulationType($insulationType);
        });
    }

    public function scopeInsulationThickness($query, Thickness $thickness)
    {
        return $query->whereHas('insulation', function ($insulation) use ($thickness) {
            return $insulation->thickness($thickness);
        });
    }

    public function scopeInsulationBackingSide($query, BackingSide $backingSide)
    {
        return $query->whereHas('insulation', function ($insulation) use ($backingSide) {
            return $insulation->backingSide($backingSide);
        });
    }

    public function scopeInsulationWidth($query, Width $width)
    {
        return $query->whereHas('insulation', function ($insulation) use ($width) {
            return $insulation->width($width);
        });
    }

    public function scopeInsulationLength($query, Length $length)
    {
        return $query->whereHas('insulation', function ($insulation) use ($length) {
            return $insulation->length($length);
        });
    }

    public function scopeInsulationDensity($query, InsulationDensity $insulationDensity)
    {
        return $query->whereHas('insulation', function ($insulation) use ($insulationDensity) {
            return $insulation->insulationDensity($insulationDensity);
        });
    }

    public function scopePurchaseOrderAccessoryId($query, $purchaseOrderAccessoryId)
    {
        return $query->whereHas('purchaseOrderAccessoryDetails', function ($purchaseOrderAccessoryDetails) use ($purchaseOrderAccessoryId) {
            return $purchaseOrderAccessoryDetails->wherePurchaseOrderAccessoryId($purchaseOrderAccessoryId);
        });
    }

    public function scopeHasDamagedStock($query)
    {
        return $query->whereHas('inventory', function ($inventory) {
            return $inventory->whereHas('inventoryDetails', function ($inventoryDetails) {
                return $inventoryDetails->whereCondition('Bad');
            });
        });
    }
}
