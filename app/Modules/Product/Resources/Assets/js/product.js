import { Form } from '@c1_common_js/components/Form';
import { serviceBus } from '@c1_common_js/main';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm';
import Select2 from '@c1_common_js/components/Select2';

new Vue({
    el: '#product',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select2, serviceBus
    },
    data: {
        form: new Form({
            name: '',
            sku: '',
            gsm: 0,
            variant: '',
            remarks: '',
        }),
        isSaving: false,
        isBusy: false,
        isDeleting: false,
        isBusyDeleting: false,
        dataInitialized: true,
        paperTypesInitialized: false,
    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }

    },

    computed: {

        initializationComplete() {
            return this.dataInitialized && this.paperTypesInitialized;
        }

    },
    methods: {

        destroy() {
            this.form.confirm().then((result) => {
                if (result.value) {
                    this.isDeleting = true;
                    this.isBusyDeleting = true;
                    this.form.delete('/api/products/' + this.form.id)
                        .then(response => {
                            this.isDeleting = false;
                            this.isBusyDeleting = false;
                            this.$swal({
                                title: 'Success',
                                text: 'Product was removed.',
                                type: 'success'
                            }).then(() => window.location = '/master-file/products/');
                        }).catch(error => {
                            this.isDeleting = false;
                            this.isBusyDeleting = false;
                        });

                }

            });
        },

        store() {
            this.isSaving = true;
            this.isBusy = true;
            this.form.post('/api/products')
                .then(response => {
                    this.isSaving = false;
                    this.isBusy = false;
                    this.$swal({
                        title: 'Success',
                        text: this.form.name.toUpperCase() + ' was saved.',
                        type: 'success'
                    });
                    this.form.reset();
                }).catch(error => {
                    this.isSaving = false;
                    this.isBusy = false;
                });

        },

        update() {
            this.isSaving = true;
            this.isBusy = true;
            this.form.patch('/api/products/' + this.form.id)
                .then(response => {
                    this.isSaving = false;
                    this.isBusy = false;
                    this.$swal({
                        title: 'Success',
                        text: this.form.name.toUpperCase() + ' was updated.',
                        type: 'success',
                        confirmButtonColor: '#20a8d8',
                    }).then(() =>
                        window.location = '/master-file/products/' + this.form.id
                    );

                }).catch(error => {
                    this.isSaving = false;
                    this.isBusy = false;
                });

        },

        loadData(data) {
            this.form = new Form(data);
        },
    },
    created() {

        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/products/' + id)
                .then(response => {
                    this.loadData(response.data);
                    this.dataInitialized = true;
                });
        }


    },
    mounted() {

        console.log("Init product script...");

    }
});