import { Form } from "@c1_common_js/components/Form";

export const productService = new Vue({
    data: function () {
        return {
            form: new Form({}),
            uri: {
                products: '/api/products?limit=' + Number.MAX_SAFE_INTEGER,
            }
        };
    },
    methods: {
        getProducts() {
            return this.form.get(this.uri.products);
        },

        getCoilOnlyProducts(supplierId) {
            return this.form.get(this.uri.products + '&coils-only=1&supplier_id=' + supplierId + '&sort=name');
        },
        getAccessoryOnlyProducts(supplierId) {
            return this.form.get(this.uri.products + '&accessories-only=1&supplier_id=' + supplierId + '&sort=name');
        },
        getStainlessSteelOnlyProducts(supplierId) {
            return this.form.get(this.uri.products + '&stainless-steels-only=1&supplier_id=' + supplierId + '&sort=name');
        },
        getSkylightOnlyProducts(supplierId) {
            return this.form.get(this.uri.products + '&skylights-only=1&supplier_id=' + supplierId + '&sort=name');
        },
        getInsulationOnlyProducts(supplierId) {
            return this.form.get(this.uri.products + '&insulations-only=1&supplier_id=' + supplierId + '&sort=name');
        },
        getScrewOnlyProducts(supplierId) {
            return this.form.get(this.uri.products + '&screws-only=1&supplier_id=' + supplierId + '&sort=name');
        },
        getMiscAccessoryOnlyProducts(supplierId) {
            return this.form.get(this.uri.products + '&misc-accessories-only=1&supplier_id=' + supplierId + '&sort=name');
        },
        getStructuralSteelOnlyProducts(supplierId) {
            return this.form.get(this.uri.products + '&structural-steels-only=1&supplier_id=' + supplierId + '&sort=name');
        },
        getPaintAdhesiveOnlyProducts(supplierId) {
            return this.form.get(this.uri.products + '&paint-adhesives-only=1&supplier_id=' + supplierId + '&sort=name');
        },
        getFastenerOnlyProducts(supplierId) {
            return this.form.get(this.uri.products + '&fasteners-only=1&supplier_id=' + supplierId + '&sort=name');
        },
        getPvcFittingOnlyProducts(supplierId) {
            return this.form.get(this.uri.products + '&pvc-fittings-only=1&supplier_id=' + supplierId + '&sort=name');
        },
        getProductsByPurchaseOrderAccessory(purchaseOrderAccessoryId) {
            return this.form.get(this.uri.products + '&purchase-order-accessory-id=' + purchaseOrderAccessoryId)
        },
        getCoils() {
            return this.form.get(this.uri.products + '&productable_type=App\\Modules\\Coil\\Models\\CoilModel');
        }
    }
});
