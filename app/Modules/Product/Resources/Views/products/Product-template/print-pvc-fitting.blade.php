<table>
    <tr>
        <th>SUPPLIER CODE</th>
        <th>TYPE</th>
        <th>DIAMETER</th>
        <th>BRAND</th>
        <th>QUANTITY (IN PCS)</th>
    </tr>
    @foreach($data->getPurchaseOrderAccessoryDetails() as $detail)
    @php
        $pvcFitting = $detail->getProduct()->productable;
    @endphp

    <tr>
        <td> {{ $pvcFitting->getProduct()->getSupplier()->getShortCode() }} </td>
        <td> {{ $pvcFitting->getPvcType()->getName() }}</td>
        <td> {{ $pvcFitting->getPvcDiameter()->getName() }} </td>
        <td> {{ $pvcFitting->getBrand()->getName() }}</td>
        <td> {{ $detail->getOrderedQuantity() }} </td>
    </tr>
    @endforeach

</table>

