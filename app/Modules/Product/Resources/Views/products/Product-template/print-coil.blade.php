<table>
    <tr>
        <th>SUPPLIER CODE</th>
        <th>COIL NUMBER</th>
        <th>COATING</th>
        <th>WIDTH</th>
        <th>THICKNESS</th>
        <th>NW (MT)</th>
        <th>COLOR</th>
        <th>LENGTH (LIN MTR)</th>
    </tr>
    @if($data->hasReceivings())
        @foreach($data->getReceivings() as $receiving)
        @php
            $coil = $receiving->getProduct()->productable;
        @endphp
        <tr>
            <td> {{ $coil->getProduct()->getSupplier()->getShortCode() }}</td>
            <td> {{ $receiving->getCoilNumber() }} </td>
            <td> {{ $coil->getCoating()->getName() }}</td>
            <td> {{ $coil->getWidth()->getName() }}</td>
            <td> {{ $coil->getThickness()->getName() }}</td>
            <td> {{ $receiving->getNetWeight() }} </td>
            <td> {{ $coil->getColor()->getName() }}</td>
            <td> {{ $receiving->getLinearMeter() }} </td>
        </tr>
        @endforeach
    @else
        @foreach($data->getPurchaseOrderDetails() as $purchaseOrderDetail)
            @php
                $coil = $purchaseOrderDetail->getProduct()->productable;
            @endphp
                <tr>
                    <td>{{ $coil->getProduct()->getSupplier()->getShortCode() }}</td>
                    <td> </td>
                    <td> {{ $coil->getCoating()->getName() }}</td>
                    <td> {{ $coil->getWidth()->getName() }}</td>
                    <td> {{ $coil->getThickness()->getName() }}</td>
                    <td> </td>
                    <td> {{ $coil->getColor()->getName() }}</td>
                    <td> </td>
                </tr>
        @endforeach
    @endif
</table>