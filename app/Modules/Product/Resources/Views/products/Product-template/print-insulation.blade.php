<table>
    <tr>
        <th>SUPPLIER CODE</th>
        <th>TYPE</th>
        <th>DENSITY</th>
        <th>THICKNESS</th>
        <th>LENGTH</th>
        <th>WIDTH</th>
        <th>BACKING</th>
        <th>BALANCE QUANTITY (IN PCS)</th>
        <th>QUANTITY (IN PCS)</th>
    </tr>
    @foreach($data->getPurchaseOrderAccessoryDetails() as $detail)
    @php
        $insulation = $detail->getProduct()->productable;
        $purchaseOrderAccessory = $detail->getPurchaseOrderAccessory();
        $balanceQuantity = $purchaseOrderAccessory->getPurchaseOrderAccessoryDetailBalanceQuantity($detail->getProductId()); 
    @endphp

    <tr>
        <td>{{ $insulation->getProduct()->getSupplier()->getShortCode() }} </td>
        <td>{{ $insulation->getInsulationType()->getName() }} </td>
        <td>{{ $insulation->getInsulationDensity()->getName() }} </td>
        <td>{{ $insulation->getThickness()->getName() }} </td>
        <td>{{ $insulation->getLength()->getName() }} </td>
        <td>{{ $insulation->getWidth()->getName() }} </td>
        <td>{{ $insulation->getBackingSide()->getName() }} </td>
        <td>{{ $balanceQuantity }} </td>
    </tr>
    @endforeach

</table>

