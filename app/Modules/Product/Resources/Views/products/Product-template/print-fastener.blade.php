<table>
    <tr>
        <th>SUPPLIER CODE</th>
        <th>FASTENER TYPE</th>
        <th>FINISH</th>
        <th>FASTENER DIMENSION</th>
        <th>SIZE PER UNIT</th>
        <th>BALANCE QUANTITY (IN PCS)</th>
        <th>QUANTITY (IN PCS)</th>
    </tr>

    @foreach($data->getPurchaseOrderAccessoryDetails() as $detail)
    
    @php
        $fastener = $detail->getProduct()->productable;
        $purchaseOrderAccessory = $detail->getPurchaseOrderAccessory();
        $balanceQuantity = $purchaseOrderAccessory->getPurchaseOrderAccessoryDetailBalanceQuantity($detail->getProductId()); 
    @endphp

    <tr>
        <td> {{ $fastener->getProduct()->getSupplier()->getShortCode() }} </td>
        <td> {{ $fastener->getFastenerType()->getName() }} </td>
        <td> {{ $fastener->getFinish()->getName() }} </td>
        <td> {{ $fastener->getFastenerDimension()->getName() }} </td>
        <td> {{ $fastener->getSize()->getName() }} </td> 
        <td> {{ $balanceQuantity }} </td>
        <td></td>
    </tr>
    @endforeach
</table>

