<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
            @{{ error[0] }}
    </small>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="name" class="col-form-label">Name <span class="text text-danger">*</span></label>
        <input type="text" v-model="form.name" class="form-control" id="name" placeholder=""
        :class="{'is-invalid': form.errors.has('name')}" 
        required 
        data-msg-required="Enter product name">
    </div>
    <div class="form-group col-md-6">
        <label for="sku" class="col-form-label">SKU <small>(System Generated)</small></label>
        <input type="text" v-model="form.sku" class="form-control" id="sku" disabled>
    </div>
   
    <div class="form-group col-md-6">
        <label for="paper-type" class="col-form-label">Paper Type <span class="text text-danger">*</span></label>
        <select2 class="form-control" :allow-custom="false" v-model="form.paperTypeId" id="paperTypeId" 
        :class="{'is-invalid': form.errors.has('paperTypeId')}" 
        required data-msg-required="Please select a Paper Type" placeholder="Select Paper Type">
                <option disabled value=''>Please select Paper Type</option>
                <option v-for="item in paperTypes" :value="item.id">@{{ item.name }}</option>
        </select2>
    </div>
    <div class="form-group col-md-6">
        <label for="gsm" class="col-form-label">GSM </label>
        <input type="number" v-model="form.gsm" class="form-control" id="gsm" placeholder="" @click="$event.target.select()">
    </div>
    <div class="form-group col-md-6">
        <label for="variant" class="col-form-label">Variant <span class="text text-danger">*</span></label>
        <input type="text" v-model="form.variant" class="form-control" id="variant" name="variant" placeholder="" 
        :class="{'is-invalid': form.errors.has('variant')}" 
        required 
        data-msg-required="Enter variant">
    </div>
    <div class="form-group col-md-6">
        <label for="remarks" class="col-form-label">Remarks</label>
        <input type="text" v-model="form.remarks" class="form-control" name="remarks" id="remarks" placeholder="">
    </div>
</div>

@push('scripts')
<script src="/js/product.js"></script>
@endpush