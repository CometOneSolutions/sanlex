@extends('app')
@section('breadcrumbs', Breadcrumbs::render('product.master-list.create'))
@section('content')

<section id="product">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Create New Product
                    </div>
                    <!-- <form @submit.prevent="store" v-on:keydown="form.errors.clear($event.target.name)" autocomplete="off"> -->
                    <v-form
                        @validate="store">

                        <div class="card-body">
                            @include('products._form')
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <button class="btn btn-success btn-sm" type="submit" :disabled="isBusy">
                                    <div v-if="isSaving">
                                        <i class="fas fa-spinner fa-pulse"></i> Saving
                                    </div>
                                    <div v-if="!isSaving">
                                        Create
                                    </div>
                                </button>
                            </div>
                        </div>
                    </v-form>
                </div>  
            </div>
        </div>
    </div>
</section>
<br/>

@endsection


