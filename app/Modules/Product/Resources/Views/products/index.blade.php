@extends('app')
@section('breadcrumbs', Breadcrumbs::render('product.master-list'))
@section('content')

<div id="index" v-cloak>
        <div class="card">
            <div class="card-header">
                Product
                <div class="float-right">
                    <a href="{{route('product_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
                </div>
            </div>
            <div class="card-body">
                <index 
                :filterable="filterable"
                :base-url="baseUrl"
                :sorter="sorter"
                :sort-ascending="sortAscending"
            
                v-on:update-loading="(val) => isLoading = val"
                v-on:update-items="(val) => items = val">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('name')">
                                    Name <i class="fa" :class="getSortIcon('name')"></i>
                                </a>
                            </th>
                            <th>
                                    SKU
                            </th>
                            <th>
                                    Paper Type
                            </th>
                            <th>
                                    GSM
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{ item.name }}</a></td>
                            <td>@{{ item.sku }}</td>
                            <td>@{{ item.paperType.name }}</td>
                            <td>@{{ item.gsm }}</td>
                        </tr>
                        </tbody>
                    </table> 
                </index>
            </div>
        </div>
    </div>

   


   

    <!-- End Watchlist-->
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush


