<div class="form-row">
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Name</label>
        <p>@{{form.name}}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">SKU</label>
        <p>@{{form.sku}}</p>
    </div>
    {{--<div class="form-group col-md-6">--}}
        {{--<label for="inputEmail4" class="col-form-label">Unit of Measure</label>--}}
        {{--<p>Roll</p>--}}
    {{--</div>--}}
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Paper Type</label>
        <p>@{{form.paperType.name}}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">GSM</label>
        <p>@{{form.gsm}}</p>
    </div>
    {{--<div class="form-group col-md-6">--}}
        {{--<label for="inputEmail4" class="col-form-label">R/W <small>(in)</small></label>--}}
        {{--<p>26</p>--}}
    {{--</div>--}}
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Variant</label>
        <p>@{{form.variant}}</p>
    </div>
    <div class="form-group col-md-6">
        <label for="inputEmail4" class="col-form-label">Remarks</label>
        <p>@{{form.remarks}}</p>
    </div>
   
</div>

@push('scripts')
<script src="/js/product.js"></script>
@endpush