@extends('app')
@section('breadcrumbs', Breadcrumbs::render('product.master-list.show', $product))
@section('content')
<section id="product">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fas fa-cog fa-spin"></i> Initializing
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        Product Record
                        <div class="float-right">
                            <a :href="form.editUri"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> </button></a>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('products._list')
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <timestamp :name="form.updatedByUser.data.name || 'System'" :time="form.updatedAt"></timestamp>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</section>
<br/>
@endsection


