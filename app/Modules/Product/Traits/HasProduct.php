<?php

namespace App\Modules\Product\Traits;

use App\Modules\Product\Models\ProductModel;

trait HasProduct
{
    public function product()
    {
        return $this->morphOne(ProductModel::class, 'productable');
    }

    public function getProduct()
    {
        return $this->product;
    }
}
