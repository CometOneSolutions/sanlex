<?php

namespace App\Modules\Product\Traits;

interface Productable
{
    public function product();

    public function getProduct();

    public function getAssembledProductName();
}
