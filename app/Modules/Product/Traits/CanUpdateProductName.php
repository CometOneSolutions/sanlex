<?php

namespace App\Modules\Product\Traits;

use App\Modules\Product\Services\ProductRecordService;

trait CanUpdateProductName
{
    public function updateProductableProductNames(array $productablesArray)
    {
        collect($productablesArray)->each(function ($productables) {
            $productables->each(function ($productable) {
                $this->updateProductName(
                    $productable
                );
            });
        });
    }

    public function updateProductName(
        Productable $productable
    ) {
        $productRecordService = resolve(ProductRecordService::class);

        return $productRecordService->updateName(
            $productable->getProduct(),
            $productable->getAssembledProductName()
        );
    }
}
