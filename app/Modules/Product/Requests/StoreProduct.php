<?php

namespace App\Modules\Product\Requests;

use Dingo\Api\Http\FormRequest;

class StoreProduct extends ProductRequest
{
    public function authorize()
    {
        return $this->user()->can('Create Products');
    }
}
