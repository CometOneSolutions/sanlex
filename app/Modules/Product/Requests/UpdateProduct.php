<?php

namespace App\Modules\Product\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateProduct extends ProductRequest
{
    public function authorize()
    {
        return $this->user()->can('Update Products');
    }
}
