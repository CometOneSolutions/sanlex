<?php

namespace App\Modules\Product\Requests;

use Dingo\Api\Http\FormRequest;
use \DateTime;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\PaperType\Services\PaperTypeRecordService;

class ProductRequest extends FormRequest
{
    protected $service;

    public function __construct(
        ProductRecordService $productRecordService,
        PaperTypeRecordService $paperTypeRecordService
    ) {
        $this->service = $productRecordService;
        $this->paperTypeRecordService = $paperTypeRecordService;
    }

    public function getProduct()
    {
        return $this->service->getById($this->input('id'));
    }

    public function getProductName()
    {
        return $this->input('name');
    }

    public function getSku()
    {
        // return $this->input('sku');
        return '';
    }

    public function getPaperType()
    {
        return $this->paperTypeRecordService->getById($this->input('paperTypeId'));
    }

    public function getGsm()
    {
        return $this->input('gsm');
    }

    public function getVariant()
    {
        return $this->input('variant');
    }

    public function getRemarks()
    {
        return ($this->input('remarks') != null) ? $this->input('remarks') : null;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|unique:products,name,' . $this->route('productId') ?? null,
            'paperTypeId'   => 'required',
            'variant'       => 'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required'        => 'Product name is a required field',
            'name.unique'          => 'Product name already exist',
            'paperTypeId.required'          => 'Paper type is required',
            'variant.required'      => 'Please enter variant'
        ];
    }


}
