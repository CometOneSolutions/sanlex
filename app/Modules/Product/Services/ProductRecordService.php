<?php

namespace App\Modules\Product\Services;

use App\Modules\Product\Models\Product;
use App\Modules\Product\Traits\Productable;
use App\Modules\Supplier\Models\Supplier;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface ProductRecordService extends RecordService
{
    public function create($name, Supplier $supplier, Productable $productable, User $user = null);
    public function update(Product $product, $name, Supplier $supplier, Productable $productable, User $user = null);
    public function delete(Product $product);
}
