<?php

namespace App\Modules\Product\Services;

use App\Modules\Product\Models\Product;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Product\Traits\Productable;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Uom\Services\UomRecordService;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Utils\Filter;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class ProductRecordServiceImpl extends RecordServiceImpl implements ProductRecordService
{
    private $products;
    private $product;
    private $uomRecordService;

    protected $availableFilters = [
        ['id' => 'name', 'text' => 'Name'],
        ['id' => 'sku', 'text' => 'SKU'],
        ['id' => 'paper_type_number', 'text' => 'Paper Type'],
        ['id' => 'gsm', 'text' => 'GSM'],
    ];

    public function __construct(ProductRepository $products, Product $product, UomRecordService $uomRecordService)
    {
        parent::__construct($products);
        $this->products = $products;
        $this->product = $product;
        $this->uomRecordService = $uomRecordService;
    }

    public function getUomMT()
    {
        $findMTFilter = new Filter('name', 'MT');
        return $this->uomRecordService->getFirst([$findMTFilter]);
    }

    public function getUomPiece()
    {
        $findPieceFilter = new Filter('name', 'PIECE');
        return $this->uomRecordService->getFirst([$findPieceFilter]);
    }

    public function create(
        $name,
        Supplier $supplier,
        Productable $productable,
        User $user = null
    ) {
        $product = new $this->product;
        $product->setProductable($productable);
        $product->setName($name);
        if ($productable->isCoil()) {
            $product->setUom($this->getUomMT());
        } else {
            $product->setUom($this->getUomPiece());
        }
        $product->setSupplier($supplier);
        if ($user) {
            $product->setUpdatedByUser($user);
        }
        $this->products->save($product);
        return $product;
    }

    public function update(
        Product $product,
        $name,
        Supplier $supplier,
        Productable $productable,
        User $user = null
    ) {
        $tempProduct = clone $product;
        $tempProduct->setProductable($productable);
        $tempProduct->setName($name);
        if ($productable->isCoil()) {
            $tempProduct->setUom($this->getUomMT());
        } else {
            $tempProduct->setUom($this->getUomPiece());
        }

        $tempProduct->setSupplier($supplier);
        if ($user) {
            $tempProduct->setUpdatedByUser($user);
        }
        $this->products->save($tempProduct);
        return $tempProduct;
    }

    public function delete(Product $product)
    {
        $this->products->delete($product);
        return $product;
    }

    public function checkIfProductExists($name)
    {
        return ($this->products->where('name', $name)->getFirst() != null);
    }

    public function addBrandToProductHierarchy()
    {
        $products = $this->product->whereProductableType('App\PaintAdhesiveModel')->get();
        $format = '%s~%s~%s~%s~%s';

        foreach ($products as $product) {
            $product->name = sprintf($format, $product->getSupplier()->getShortCode(), $product->productable->getPaintAdhesiveType()->getName(), $product->productable->getColor()->getName(), $product->productable->getSize()->getName(), strtoupper($product->productable->getBrand()->getName()));
            $product->save();
        }

        $products = $this->product->whereProductableType('App\PvcFittingModel')->get();
        $format = '%s~%s~%s~%s';

        foreach ($products as $product) {
            $product->name = sprintf($format, $product->getSupplier()->getShortCode(), $product->productable->getPvcType()->getName(), $product->productable->getPvcDiameter()->getName(), strtoupper($product->productable->getBrand()->getName()));
            $product->save();
        }
    }

    public function updateName(
        Product $product,
        $name
    ) {
        $tempProduct = clone $product;
        $tempProduct->setName($name);
        $this->products->save($tempProduct);


        return $tempProduct;
    }
}
