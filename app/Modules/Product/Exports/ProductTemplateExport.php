<?php

namespace App\Modules\Product\Exports;

use CometOneSolutions\Common\Utils\Export;
use Maatwebsite\Excel\Concerns\WithTitle;


class ProductTemplateExport extends Export implements WithTitle
{

    public function title(): String
    {
        return 'Template';
    }
}
