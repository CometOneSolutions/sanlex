<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'products'], function () use ($api) {
        $api->get('/', 'App\Modules\Product\Controllers\ProductApiController@index');
        // $api->get('/list', 'App\Modules\Product\Controllers\ProductListApiController@index');
        $api->get('{productId}', 'App\Modules\Product\Controllers\ProductApiController@show');
        $api->post('/', 'App\Modules\Product\Controllers\ProductApiController@store');
        $api->patch('{productId}', 'App\Modules\Product\Controllers\ProductApiController@update');
        $api->delete('{productId}', 'App\Modules\Product\Controllers\ProductApiController@destroy');
    });
});

