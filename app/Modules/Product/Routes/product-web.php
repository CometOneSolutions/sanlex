<?php

Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'ProductController@index')->name('product_index');
        Route::get('create', 'ProductController@create')->name('product_create');
        Route::get('{productId}/edit', 'ProductController@edit')->name('product_edit');
        Route::get('{productId}', 'ProductController@show')->name('product_show');
        Route::get('{productId}/supplier-products/create', '\App\Http\Controllers\SupplierProductController@create')->name('supplier-product_create');
        Route::get('{productId}/supplier-products/{supplierproduct}', '\App\Http\Controllers\SupplierProductController@show')->name('supplier-product_show');
        Route::get('{productId}/supplier-products/{supplierproduct}/edit', '\App\Http\Controllers\SupplierProductController@edit')->name('supplier-product_edit');
    });

});
