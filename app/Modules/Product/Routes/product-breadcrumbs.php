<?php

Breadcrumbs::register('product.master-list', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Products', route('product_index'));
});

Breadcrumbs::register('product.master-list.create', function ($breadcrumbs) {
    $breadcrumbs->parent('product.master-list');
    $breadcrumbs->push('Create', route('product_create'));
});

Breadcrumbs::register('product.master-list.show', function ($breadcrumbs, $product) {
    $breadcrumbs->parent('product.master-list');
    $breadcrumbs->push($product->getName(), route('product_show', $product->getId()));
});

Breadcrumbs::register('product.master-list.edit', function ($breadcrumbs, $product) {
    $breadcrumbs->parent('product.master-list.show', $product);
    $breadcrumbs->push('Edit', route('product_edit', $product->getId()));
});
