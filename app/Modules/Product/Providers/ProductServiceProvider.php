<?php

namespace App\Modules\Product\Providers;

use App\Modules\Product\Models\Product;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Product\Repositories\EloquentProductRepository;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Product\Services\ProductRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ProductServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Product/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(ProductRecordService::class, ProductRecordServiceImpl::class);
        $this->app->bind(ProductRepository::class, EloquentProductRepository::class);
        $this->app->bind(Product::class, ProductModel::class);
    }
}
