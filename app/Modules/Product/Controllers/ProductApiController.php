<?php

namespace App\Modules\Product\Controllers;

use App\Modules\Product\Services\ProductRecordService;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use Illuminate\Http\Request;
use App\Modules\Product\Transformer\ProductTransformer;


use App\Modules\Product\Requests\DestroyProduct;

// use Dingo\Api\Http\Middleware\Request;

class ProductApiController extends ResourceApiController
{
    protected $service;
    protected $paperTypeService;
    protected $transformer;

    public function __construct(
        ProductRecordService $productRecordService,
        ProductTransformer $transformer
    ) {
        $this->service = $productRecordService;
        $this->transformer = $transformer;
        parent::__construct($productRecordService, $transformer);
        $this->middleware('auth:api');
    }

    // public function store(StoreProduct $request)
    // {
    //     $product = \DB::transaction(function () use ($request) {
    //         return $this->service->create(
    //             $request->getProductName(),
    //             $request->getPaperType(),
    //             $request->getGsm(),
    //             $request->getVariant(),
    //             $request->getRemarks(),
    //             $request->user()
    //         );
    //     });
    //     return $this->response->item($product, $this->transformer)->setStatusCode(201);
    // }

    // public function update(UpdateProduct $request)
    // {
    //     $product = \DB::transaction(function () use ($request) {
    //         return $this->service->update(
    //             $request->getProduct(),
    //             $request->getProductName(),
    //             $request->getPaperType(),
    //             $request->getGsm(),
    //             $request->getVariant(),
    //             $request->getRemarks(),
    //             $request->user()
    //         );
    //     });
    //     return $this->response->item($product, $this->transformer)->setStatusCode(200);
    // }

    public function destroy($id, DestroyProduct $request)
    {
        $product = \DB::transaction(function () use ($id) {
            $product = $this->service->getById($id);
            $this->service->delete($product);
            return $product;
        });
        return $this->response->item($product, $this->transformer)->setStatusCode(200);
    }
}
