<?php

namespace App\Modules\Product\Transformer;

use App\Modules\Product\Models\Product;
use App\Modules\Supplier\Transformer\SupplierTransformer;
use App\Modules\Uom\Transformer\UomTransformer;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;
use CometOneSolutions\Auth\Tranformers\UserTransformer;

class ProductTransformer extends UpdatableByUserTransformer
{
    // protected $availableIncludes = [
    //     'customerOrderDetails'
    // ];


    public function __construct()
    {
        $this->defaultIncludes = array_merge($this->defaultIncludes, [
            'supplier', 'uom'
        ]);
    }
    public function transform(Product $product)
    {
        return [
            'id'            => (int) $product->getId(),
            'name'          => $product->getName(),
            'text'          => $product->getName(),
            'type'          => $product->getClassName(),
            // 'productableType' => $product->getProductable(),
            // 'editUri'       => route('product_edit', $product->id),
            // 'showUri'       => route('product_show', $product->id),
            'updatedAt'     => $product->updated_at,
        ];
    }

    public function includeSupplier(Product $product)
    {
        $supplier = $product->getSupplier();
        return $this->item($supplier, new SupplierTransformer);
    }

    public function includeUom(Product $product)
    {
        $uom = $product->getUom();
        return $this->item($uom, new UomTransformer);
    }

    public function productable(Product $product)
    {
        return $this->item($product, new ProductableTransformer);
    }
}
