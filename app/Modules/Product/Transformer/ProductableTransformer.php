<?php

namespace App\Modules\Product\Transformer;

use App\Modules\Product\Models\Product;

class ProductableTransformer extends ProductTransformer
{
    public function transform(Product $product)
    {
        return ProductTransformerFactory::make($product)->transform($product->productable);
    }
}
