<?php

namespace App\Modules\Product\Transformer;

use App\Modules\Accessory\Transformer\AccessoryTransformer;
use App\Modules\Coil\Transformer\CoilTransformer;
use App\Modules\Product\Models\Product;
use \Exception as Exception;

class ProductTransformerFactory
{
    public static function make(Product $product)
    {
        switch (class_basename($product->productable)) {
            case 'CoilModel':
                return new CoilTransformer;
                break;
            case 'AccessoryModel':
            return new AccessoryTransformer;
            break;
            default:
                throw new Exception('Invalid Product Type.');
                break;
        }
    }
}
