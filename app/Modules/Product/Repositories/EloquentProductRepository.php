<?php

namespace App\Modules\Product\Repositories;

use App\Modules\Accessory\Models\AccessoryModel;
use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\Brand\Models\Brand;
use App\Modules\Coating\Models\Coating;
use App\Modules\Coil\Models\CoilModel;
use App\Modules\Color\Models\Color;
use App\Modules\DimensionX\Models\DimensionX;
use App\Modules\DimensionY\Models\DimensionY;
use App\Modules\Fastener\Models\FastenerModel;
use App\Modules\FastenerDimension\Models\FastenerDimension;
use App\Modules\FastenerType\Models\FastenerType;
use App\Modules\Finish\Models\Finish;
use App\Modules\Insulation\Models\InsulationModel;
use App\Modules\InsulationDensity\Models\InsulationDensity;
use App\Modules\InsulationType\Models\InsulationType;
use App\Modules\Length\Models\Length;
use App\Modules\MiscAccessory\Models\MiscAccessoryModel;
use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use App\Modules\PaintAdhesive\Models\PaintAdhesiveModel;
use App\Modules\PaintAdhesiveType\Models\PaintAdhesiveType;
use App\Modules\PanelProfile\Models\PanelProfile;
use App\Modules\Product\Models\Product;
use App\Modules\PvcDiameter\Models\PvcDiameter;
use App\Modules\PvcFitting\Models\PvcFittingModel;
use App\Modules\PvcType\Models\PvcType;
use App\Modules\Size\Models\Size;
use App\Modules\Skylight\Models\SkylightModel;
use App\Modules\SkylightClass\Models\SkylightClass;
use App\Modules\StainlessSteel\Models\StainlessSteelModel;
use App\Modules\StainlessSteelType\Models\StainlessSteelType;
use App\Modules\StructuralSteel\Models\StructuralSteelModel;
use App\Modules\StructuralSteelType\Models\StructuralSteelType;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentProductRepository extends EloquentRepository implements ProductRepository
{
    public function __construct(Product $product)
    {
        parent::__construct($product);
    }

    public function save(Product $product)
    {
        return $product->save();
    }

    public function delete(Product $product)
    {
        return $product->delete();
    }

    public function supplier(Supplier $supplier)
    {
        $this->model = $this->model->where('supplier_id', $supplier->getId());
        return $this;
    }

    public function hasDamagedStock()
    {
        $this->model = $this->model->hasDamagedStock();
        return $this;
    }

    public function hasGoodStockOrHasPending()
    {
        $this->model = $this->model->hasGoodStockOrHasPending();
        return $this;
    }

    public function inStockOrHasPending()
    {
        $this->model = $this->model->inStockOrHasPending();
        return $this;
    }

    public function inStock()
    {
        $this->model = $this->model->inStock();
        return $this;
    }

    public function hasPending()
    {
        $this->model = $this->model->hasPending();
        return $this;
    }

    public function GINonStandards()
    {
        $this->model = $this->model->GINonStandards();
        return $this;
    }

    public function stainlessSteelNonStandards()
    {
        $this->model = $this->model->stainlessSteelNonStandards();
        return $this;
    }

    public function coilByColor($color)
    {
        $this->model = $this->model->coilByColor($color);
        return $this;
    }

    public function coloredNonStandards()
    {
        $this->model = $this->model->coloredNonStandards();
        return $this;
    }

    public function coilWidth(Width $width)
    {
        $this->model = $this->model->coilWidth($width);
        return $this;
    }

    public function coilThickness(Thickness $thickness)
    {
        $this->model = $this->model->coilThickness($thickness);
        return $this;
    }

    public function coilCoating(Coating $coating)
    {
        $this->model = $this->model->coilCoating($coating);
        return $this;
    }

    public function coilColor(Color $color)
    {
        $this->model = $this->model->coilColor($color);
        return $this;
    }

    public function fastenerSize(Size $size)
    {
        $this->model = $this->model->fastenerSize($size);
        return $this;
    }

    public function fastenerFinish(Finish $finish)
    {
        $this->model = $this->model->fastenerFinish($finish);
        return $this;
    }

    public function fastenerDimension(FastenerDimension $dimension)
    {
        $this->model = $this->model->fastenerDimension($dimension);
        return $this;
    }

    public function fastenerType(FastenerType $type)
    {
        $this->model = $this->model->fastenerType($type);
        return $this;
    }

    public function paintAdhesiveSize(Size $size)
    {
        $this->model = $this->model->paintAdhesiveSize($size);
        return $this;
    }

    public function paintAdhesiveColor(Color $color)
    {
        $this->model = $this->model->paintAdhesiveColor($color);
        return $this;
    }

    public function paintAdhesiveBrand(Brand $brand)
    {
        $this->model = $this->model->paintAdhesiveBrand($brand);
        return $this;
    }

    public function paintAdhesiveType(PaintAdhesiveType $paintAdhesiveType)
    {
        $this->model = $this->model->paintAdhesiveType($paintAdhesiveType);
        return $this;
    }

    public function pvcType(PvcType $pvcType)
    {
        $this->model = $this->model->pvcType($pvcType);
        return $this;
    }

    public function pvcFittingBrand(Brand $brand)
    {
        $this->model = $this->model->pvcFittingBrand($brand);
        return $this;
    }

    public function pvcDiameter(PvcDiameter $pvcDiameter)
    {
        $this->model = $this->model->pvcDiameter($pvcDiameter);
        return $this;
    }

    public function structuralSteelType(StructuralSteelType $structuralSteelType)
    {
        $this->model = $this->model->structuralSteelType($structuralSteelType);
        return $this;
    }

    public function structuralSteelFinish(Finish $finish)
    {
        $this->model = $this->model->structuralSteelFinish($finish);
        return $this;
    }

    public function structuralSteelDimensionX(DimensionX $dimensionX)
    {
        $this->model = $this->model->structuralSteelDimensionX($dimensionX);
        return $this;
    }

    public function structuralSteelDimensionY(DimensionY $dimensionY)
    {
        $this->model = $this->model->structuralSteelDimensionY($dimensionY);
        return $this;
    }

    public function structuralSteelLength(Length $length)
    {
        $this->model = $this->model->structuralSteelLength($length);
        return $this;
    }

    public function structuralSteelThickness(Thickness $thickness)
    {
        $this->model = $this->model->structuralSteelThickness($thickness);
        return $this;
    }

    public function stainlessSteelType(StainlessSteelType $stainlessSteelType)
    {
        $this->model = $this->model->stainlessSteelType($stainlessSteelType);
        return $this;
    }

    public function stainlessSteelFinish(Finish $finish)
    {
        $this->model = $this->model->stainlessSteelFinish($finish);
        return $this;
    }

    public function stainlessSteelLength(Length $length)
    {
        $this->model = $this->model->stainlessSteelLength($length);
        return $this;
    }

    public function stainlessSteelThickness(Thickness $thickness)
    {
        $this->model = $this->model->stainlessSteelThickness($thickness);
        return $this;
    }

    public function stainlessSteelWidth(Width $width)
    {
        $this->model = $this->model->stainlessSteelWidth($width);
        return $this;
    }

    public function miscAccessoryType(MiscAccessoryType $miscAccessoryType)
    {
        $this->model = $this->model->miscAccessoryType($miscAccessoryType);
        return $this;
    }

    public function miscAccessoryLength(Length $length)
    {
        $this->model = $this->model->miscAccessoryLength($length);
        return $this;
    }
    public function miscAccessoryWidth(Width $width)
    {
        $this->model = $this->model->miscAccessoryWidth($width);
        return $this;
    }
    public function miscAccessoryBackingSide(BackingSide $backingSide)
    {
        $this->model = $this->model->miscAccessoryBackingSide($backingSide);
        return $this;
    }

    public function skylightPanelProfile(PanelProfile $panelProfile)
    {
        $this->model = $this->model->skylightPanelProfile($panelProfile);
        return $this;
    }
    public function skylightClass(SkylightClass $skylightClass)
    {
        $this->model = $this->model->skylightClass($skylightClass);
        return $this;
    }
    public function skylightWidth(Width $width)
    {
        $this->model = $this->model->skylightWidth($width);
        return $this;
    }
    public function skylightLength(Length $length)
    {
        $this->model = $this->model->skylightLength($length);
        return $this;
    }
    public function skylightThickness(Thickness $thickness)
    {
        $this->model = $this->model->skylightThickness($thickness);
        return $this;
    }

    public function coilWidthIsStandard()
    {
        $this->model = $this->model->coilWidthIsStandard();
        return $this;
    }

    public function insulationType(InsulationType $insulationType)
    {
        $this->model = $this->model->insulationType($insulationType);
        return $this;
    }

    public function insulationThickness(Thickness $thickness)
    {
        $this->model = $this->model->insulationThickness($thickness);
        return $this;
    }

    public function insulationBackingSide(BackingSide $backingSide)
    {
        $this->model = $this->model->insulationBackingSide($backingSide);
        return $this;
    }

    public function insulationWidth(Width $width)
    {
        $this->model = $this->model->insulationWidth($width);
        return $this;
    }

    public function insulationLength(Length $length)
    {
        $this->model = $this->model->insulationLength($length);
        return $this;
    }

    public function insulationDensity(InsulationDensity $insulationDensity)
    {
        $this->model = $this->model->insulationDensity($insulationDensity);
        return $this;
    }


    public function coil()
    {
        $this->model = $this->model->whereProductableType(CoilModel::class);
        return $this;
    }

    public function accessory()
    {
        $this->model = $this->model->whereProductableType(AccessoryModel::class);
        return $this;
    }

    public function miscAccessory()
    {
        $this->model = $this->model->whereProductableType(MiscAccessoryModel::class);
        return $this;
    }

    public function stainlessSteel()
    {
        $this->model = $this->model->whereProductableType(StainlessSteelModel::class);
        return $this;
    }

    public function structuralSteel()
    {
        $this->model = $this->model->whereProductableType(StructuralSteelModel::class);
        return $this;
    }

    public function paintAdhesive()
    {
        $this->model = $this->model->whereProductableType(PaintAdhesiveModel::class);
        return $this;
    }

    public function fastener()
    {
        $this->model = $this->model->whereProductableType(FastenerModel::class);
        return $this;
    }

    public function insulation()
    {
        $this->model = $this->model->whereProductableType(InsulationModel::class)->with(['productable.width', 'productable.length', 'productable.backingSide', 'productable.insulationType', 'productable.insulationDensity', 'productable.thickness']);
        return $this;
    }

    public function skylight()
    {
        $this->model = $this->model->whereProductableType(SkylightModel::class)->with(['productable.width', 'productable.length', 'productable.thickness', 'productable.panelProfile', 'productable.skylightClass']);
        return $this;
    }

    public function pvcFitting()
    {
        $this->model = $this->model->whereProductableType(PvcFittingModel::class);
        return $this;
    }

    protected function filterByPaperTypeNumber($value)
    {
        return $this->model->whereHas('paperType', function ($paperType) use ($value) {
            return $paperType->where('name', 'LIKE', $value);
        });
    }

    public function filterByCoilsOnly()
    {
        return $this->model->where('productable_type', CoilModel::class);
    }

    public function filterByAccessoriesOnly()
    {
        return $this->model->where('productable_type', AccessoryModel::class);
    }

    public function filterByStainlessSteelsOnly()
    {
        return $this->model->where('productable_type', StainlessSteelModel::class);
    }

    public function filterByStructuralSteelsOnly()
    {
        return $this->model->where('productable_type', StructuralSteelModel::class);
    }

    public function filterByPaintAdhesivesOnly()
    {
        return $this->model->where('productable_type', PaintAdhesiveModel::class);
    }

    public function filterByPvcFittingsOnly()
    {
        return $this->model->where('productable_type', PvcFittingModel::class);
    }

    public function filterByFastenersOnly()
    {
        return $this->model->where('productable_type', FastenerModel::class);
    }

    public function filterBySkylightsOnly()
    {
        return $this->model->where('productable_type', SkylightModel::class);
    }

    public function filterByInsulationsOnly()
    {
        return $this->model->where('productable_type', InsulationModel::class);
    }

    public function filterByScrewsOnly()
    {
        return $this->model->where('productable_type', ScrewModel::class);
    }

    public function filterByMiscAccessoriesOnly()
    {
        return $this->model->where('productable_type', MiscAccessoryModel::class);
    }

    public function getAllProductsWithProductType($productType)
    {
        return $this->model->where('productable_type', $productType);
    }

    public function filterByPurchaseOrderAccessoryId($purchaseOrderAccessoryId)
    {
        return $this->model->purchaseOrderAccessoryId($purchaseOrderAccessoryId);
    }
}
