<?php

namespace App\Modules\Product\Repositories;

use App\Modules\Product\Models\Product;
use CometOneSolutions\Common\Repositories\Repository;

interface ProductRepository extends Repository
{
    public function save(Product $product);
    public function delete(Product $product);
}


