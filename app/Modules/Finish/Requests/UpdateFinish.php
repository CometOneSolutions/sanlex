<?php

namespace App\Modules\Finish\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateFinish extends FinishRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Finish');
    }

}