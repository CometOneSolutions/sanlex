<?php

namespace App\Modules\Finish\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyFinish extends FinishRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Finish');
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}