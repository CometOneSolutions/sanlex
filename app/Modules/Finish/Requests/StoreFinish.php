<?php

namespace App\Modules\Finish\Requests;

use Dingo\Api\Http\FormRequest;

class StoreFinish extends FinishRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Finish');
    }

}
