<?php

namespace App\Modules\Finish\Models;

use App\Modules\Fastener\Models\FastenerModel;
use App\Modules\Size\Models\SizeModel;
use App\Modules\StainlessSteel\Models\StainlessSteelModel;
use App\Modules\StructuralSteel\Models\StructuralSteelModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;


class FinishModel extends Model implements Finish, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'finishes';

    public function structuralSteels()
    {
        return $this->hasMany(StructuralSteelModel::class, 'finish_id');
    }

    public function fasteners()
    {
        return $this->hasMany(FastenerModel::class, 'finish_id');
    }

    public function stainlessSteels()
    {
        return $this->hasMany(StainlessSteelModel::class, 'finish_id');
    }

    public function fastenerSizes()
    {
        return $this->belongsToMany(SizeModel::class, 'fasteners', 'finish_id', 'size_id')
            ->withTimestamps();
    }

    public function getFastenerSizes()
    {
        return $this->fastenerSizes;
    }

    public function getStructuralSteels()
    {
        return $this->structuralSteels;
    }

    public function getFasteners()
    {
        return $this->fasteners;
    }

    public function getStainlessSteels()
    {
        return $this->stainlessSteels;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
