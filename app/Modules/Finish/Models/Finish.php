<?php

namespace App\Modules\Finish\Models;



interface Finish
{
    public function getId();

    public function getName();

    public function setName($value);


}
