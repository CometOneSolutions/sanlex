import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'

new Vue({
    el: '#finish',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm
    },
    data: {
        form: new Form({
            name: null
        }),
        dataInitialized: true,
        
    },
    watch: {
        initializationComplete(val) {      
            this.form.isInitializing = !val;
        }
    },
    computed: {

        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        destroy() {
            this.form.deleteWithConfirmation('/api/finishes/' + this.form.id).then(response => {
                this.form.successModal(this.form.name.toUpperCase() + ' was removed.').then(() => 
                    window.location = '/master-file/finishes/'
                );                
            }); 
        },
       
        store() {
            this.form.postWithModal('/api/finishes', null, this.form.name.toUpperCase() + ' was saved.');
        },

        update() {
            this.form.patch('/api/finishes/' + this.form.id).then(response => {
                this.form.successModal(this.form.name.toUpperCase() + ' was updated.').then(() => 
                    window.location = '/master-file/finishes/' + this.form.id
                );
            })
        },
       
        loadData(data) {
            this.form = new Form(data);
        },
    },

    created(){
        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/finishes/' + id)
            .then(response => {
                this.loadData(response.data);
                this.dataInitialized = true;
            });
        }
    },
    mounted() {
        console.log("Init finish script...");
    }
});