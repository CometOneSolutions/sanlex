import {Form} from '@c1_common_js/components/Form';

export const finishService = new Vue({

    data: function() {
        return {
            form: new Form({}),
            uri: {
              finishes: '/api/finishes?limit=' + Number.MAX_SAFE_INTEGER
            },
        }
    },   

    methods: {

        getFinishes(){
            return this.form.get(this.uri.finishes);
        },
    }
});