<?php

namespace App\Modules\Finish\Repositories;

use App\Modules\Finish\Models\Finish;
use CometOneSolutions\Common\Repositories\Repository;

interface FinishRepository extends Repository
{
    public function save(Finish $finish);
    public function delete(Finish $finish);
}
