<?php

namespace App\Modules\Finish\Repositories;

use App\Modules\Finish\Models\Finish;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentFinishRepository extends EloquentRepository implements FinishRepository
{
    public function __construct(Finish $finish)
    {
        parent::__construct($finish);
    }

    public function save(Finish $finish)
    {
        return $finish->save();
    }

    public function delete(Finish $finish)
    {
        return $finish->delete();
    }

}
