<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'finishes'], function () use ($api) {
        $api->get('/', 'App\Modules\Finish\Controllers\FinishApiController@index');
        $api->get('{finishId}', 'App\Modules\Finish\Controllers\FinishApiController@show');
        $api->post('/', 'App\Modules\Finish\Controllers\FinishApiController@store');
        $api->patch('{finishId}', 'App\Modules\Finish\Controllers\FinishApiController@update');
        $api->delete('{finishId}', 'App\Modules\Finish\Controllers\FinishApiController@destroy');
    });
});
