<?php

Breadcrumbs::register('finish.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Finishes', route('finish_index'));
});
Breadcrumbs::register('finish.create', function ($breadcrumbs) {
    $breadcrumbs->parent('finish.index');
    $breadcrumbs->push('Create', route('finish_create'));
});
Breadcrumbs::register('finish.show', function ($breadcrumbs, $finish) {
    $breadcrumbs->parent('finish.index');
    $breadcrumbs->push($finish->getName(), route('finish_show', $finish->getId()));
});
Breadcrumbs::register('finish.edit', function ($breadcrumbs, $finish) {
    $breadcrumbs->parent('finish.show', $finish);
    $breadcrumbs->push('Edit', route('finish_edit', $finish->getId()));
});
