<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'finishes'], function () {
        Route::get('/', 'FinishController@index')->name('finish_index');
        Route::get('/create', 'FinishController@create')->name('finish_create');
        Route::get('{finishId}/edit', 'FinishController@edit')->name('finish_edit');
        Route::get('{finishId}/print', 'FinishController@print')->name('finish_print');
        Route::get('{finishId}', 'FinishController@show')->name('finish_show');
    });
});
