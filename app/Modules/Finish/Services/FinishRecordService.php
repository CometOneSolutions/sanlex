<?php

namespace App\Modules\Finish\Services;

use App\Modules\Finish\Models\Finish;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface FinishRecordService extends RecordService
{
    public function create(
        $name,
        User $user = null
    );

    public function update(
        Finish $finish,
        $name,
        User $user = null
    );

    public function delete(Finish $finish);
}
