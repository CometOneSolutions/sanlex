<?php

namespace App\Modules\Finish\Services;

use App\Modules\Finish\Models\Finish;
use App\Modules\Finish\Repositories\FinishRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class FinishRecordServiceImpl extends RecordServiceImpl implements FinishRecordService
{
    use CanUpdateProductName;

    private $finish;
    private $finishes;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"]
    ];

    public function __construct(FinishRepository $finishes, Finish $finish)
    {
        $this->finishes = $finishes;
        $this->finish = $finish;
        parent::__construct($finishes);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $finish = new $this->finish;
        $finish->setName($name);
        if ($user) {
            $finish->setUpdatedByUser($user);
        }
        $this->finishes->save($finish);
        return $finish;
    }

    public function update(
        Finish $finish,
        $name,
        User $user = null
    ) {
        $tempFinish = clone $finish;
        $tempFinish->setName($name);
        if ($user) {
            $tempFinish->setUpdatedByUser($user);
        }
        $this->finishes->save($tempFinish);

        $this->updateProductableProductNames([
            $tempFinish->getStructuralSteels(),
            $tempFinish->getFasteners(),
            $tempFinish->getStainlessSteels()
        ]);

        return $tempFinish;
    }

    public function delete(Finish $finish)
    {
        $this->finishes->delete($finish);
        return $finish;
    }
}
