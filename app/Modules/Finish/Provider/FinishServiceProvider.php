<?php

namespace App\Modules\Finish\Provider;

use App\Modules\Finish\Models\Finish;
use App\Modules\Finish\Models\FinishModel;
use App\Modules\Finish\Repositories\EloquentFinishRepository;
use App\Modules\Finish\Repositories\FinishRepository;
use App\Modules\Finish\Services\FinishRecordService;
use App\Modules\Finish\Services\FinishRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class FinishServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Finish/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(FinishRecordService::class, FinishRecordServiceImpl::class);
        $this->app->bind(FinishRepository::class, EloquentFinishRepository::class);
        $this->app->bind(Finish::class, FinishModel::class);
    }
}
