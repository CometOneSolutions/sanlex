<?php

namespace App\Modules\Finish\Transformer;

use App\Modules\Finish\Models\Finish;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;
use League\Fractal;


class FinishTransformer extends UpdatableByUserTransformer
{
    public function transform(Finish $finish)
    {
        return [
            'id' => (int)$finish->getId(),
            'text' => $finish->getName(),
            'name' => $finish->getName(),
            'updatedAt' => $finish->updated_at,
            'editUri' => route('finish_edit', $finish->getId()),
            'showUri' => route('finish_show', $finish->getId()),
        ];
    }
}
