<?php

namespace App\Modules\Finish\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Finish\Services\FinishRecordService;

class FinishController extends Controller
{
    private $service;

    public function __construct(FinishRecordService $finishRecordService)
    {
        $this->service = $finishRecordService;
        $this->middleware('permission:Read [MAS] Finish')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Finish')->only('create');
        $this->middleware('permission:Update [MAS] Finish')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/finishes'
        ]);
        return view('finishes.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null, ]);
        return view('finishes.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $finish = $this->service->getById($id);
        return view('finishes.show', compact('finish'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $finish = $this->service->getById($id);
        return view('finishes.edit', compact('finish'));
    }
}
