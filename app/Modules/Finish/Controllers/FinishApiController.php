<?php

namespace App\Modules\Finish\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Finish\Services\FinishRecordService;
use App\Modules\Finish\Transformer\FinishTransformer;
use App\Modules\Finish\Requests\StoreFinish;
use App\Modules\Finish\Requests\UpdateFinish;
use App\Modules\Finish\Requests\DestroyFinish;

class FinishApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        FinishRecordService $finishRecordService,
        FinishTransformer $transformer
    ) {
        $this->middleware('auth:api');
        parent::__construct($finishRecordService, $transformer);
        $this->service = $finishRecordService;
        $this->transformer = $transformer;
    }

    public function store(StoreFinish $request)
    {
        $finish = \DB::transaction(function () use ($request) {
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->create(
                $name,
                $user
            );
        });
        return $this->response->item($finish, $this->transformer)->setStatusCode(201);
    }

    public function update($finishId, UpdateFinish $request)
    {
        $finish = \DB::transaction(function () use ($finishId, $request) {
            $finish   = $this->service->getById($finishId);
            $name       = $request->getName();
            $user       = $request->user();

            return $this->service->update(
                $finish,
                $name,
                $user
            );
        });
        return $this->response->item($finish, $this->transformer)->setStatusCode(200);
    }

    public function destroy($finishId, DestroyFinish $request)
    {
        $finish = $this->service->getById($finishId);
        $this->service->delete($finish);
        return $this->response->item($finish, $this->transformer)->setStatusCode(200);
    }

}
