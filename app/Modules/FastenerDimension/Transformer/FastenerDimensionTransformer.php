<?php

namespace App\Modules\FastenerDimension\Transformer;

use App\Modules\FastenerDimension\Models\FastenerDimension;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class FastenerDimensionTransformer extends UpdatableByUserTransformer
{
	public function transform(FastenerDimension $fastenerDimension)
	{
		return [
			'id' => (int) $fastenerDimension->getId(),
			'text' => $fastenerDimension->getName(),
			'name' => $fastenerDimension->getName(),
			'updatedAt' => $fastenerDimension->updated_at,
			'editUri' => route('fastener-dimension_edit', $fastenerDimension->getId()),
			'showUri' => route('fastener-dimension_show', $fastenerDimension->getId()),
		];
	}
}
