<?php

namespace App\Modules\FastenerDimension\Provider;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use App\Modules\FastenerDimension\Models\FastenerDimension;
use App\Modules\FastenerDimension\Models\FastenerDimensionModel;
use App\Modules\FastenerDimension\Services\FastenerDimensionRecordService;
use App\Modules\FastenerDimension\Repositories\FastenerDimensionRepository;
use App\Modules\FastenerDimension\Services\FastenerDimensionRecordServiceImpl;
use App\Modules\FastenerDimension\Repositories\EloquentFastenerDimensionRepository;

class FastenerDimensionServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/FastenerDimension/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(FastenerDimensionRecordService::class, FastenerDimensionRecordServiceImpl::class);
		$this->app->bind(FastenerDimensionRepository::class, EloquentFastenerDimensionRepository::class);
		$this->app->bind(FastenerDimension::class, FastenerDimensionModel::class);
	}
}
