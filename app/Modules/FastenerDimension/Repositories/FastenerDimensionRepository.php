<?php

namespace App\Modules\FastenerDimension\Repositories;

use App\Modules\FastenerDimension\Models\FastenerDimension;
use CometOneSolutions\Common\Repositories\Repository;

interface FastenerDimensionRepository extends Repository
{
	public function save(FastenerDimension $fastenerDimension);
	public function delete(FastenerDimension $fastenerDimension);
}
