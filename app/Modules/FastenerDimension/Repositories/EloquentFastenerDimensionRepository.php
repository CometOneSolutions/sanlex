<?php

namespace App\Modules\FastenerDimension\Repositories;

use App\Modules\FastenerDimension\Models\FastenerDimension;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentFastenerDimensionRepository extends EloquentRepository implements FastenerDimensionRepository
{
	public function __construct(FastenerDimension $fastenerDimension)
	{
		parent::__construct($fastenerDimension);
	}

	public function save(FastenerDimension $fastenerDimension)
	{
		return $fastenerDimension->save();
	}

	public function delete(FastenerDimension $fastenerDimension)
	{
		return $fastenerDimension->delete();
	}
}
