<?php

namespace App\Modules\FastenerDimension\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\FastenerDimension\Services\FastenerDimensionRecordService;
use App\Modules\FastenerDimension\Transformer\FastenerDimensionTransformer;
use App\Modules\FastenerDimension\Requests\StoreFastenerDimension;
use App\Modules\FastenerDimension\Requests\UpdateFastenerDimension;
use App\Modules\FastenerDimension\Requests\DestroyFastenerDimension;

class FastenerDimensionApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;

	public function __construct(
		FastenerDimensionRecordService $fastenerDimensionRecordService,
		FastenerDimensionTransformer $transformer
	) {
		$this->middleware('auth:api');
		parent::__construct($fastenerDimensionRecordService, $transformer);
		$this->service = $fastenerDimensionRecordService;
		$this->transformer = $transformer;
	}

	public function store(StoreFastenerDimension $request)
	{
		$fastenerDimension = \DB::transaction(function () use ($request) {
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->create(
				$name,

				$user
			);
		});
		return $this->response->item($fastenerDimension, $this->transformer)->setStatusCode(201);
	}

	public function update($fastenerDimensionId, UpdateFastenerDimension $request)
	{
		$fastenerDimension = \DB::transaction(function () use ($fastenerDimensionId, $request) {
			$fastenerDimension   = $this->service->getById($fastenerDimensionId);
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->update(
				$fastenerDimension,
				$name,

				$user
			);
		});
		return $this->response->item($fastenerDimension, $this->transformer)->setStatusCode(200);
	}

	public function destroy($fastenerDimensionId, DestroyFastenerDimension $request)
	{
		$fastenerDimension = $this->service->getById($fastenerDimensionId);
		$this->service->delete($fastenerDimension);
		return $this->response->item($fastenerDimension, $this->transformer)->setStatusCode(200);
	}
}
