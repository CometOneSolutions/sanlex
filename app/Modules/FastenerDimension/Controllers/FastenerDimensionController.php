<?php

namespace App\Modules\FastenerDimension\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\FastenerDimension\Services\FastenerDimensionRecordService;


class FastenerDimensionController extends Controller
{
	private $service;

	public function __construct(FastenerDimensionRecordService $fastenerDimensionRecordService)
	{
		$this->service = $fastenerDimensionRecordService;
		$this->middleware('permission:Read [MAS] Fastener Dimension')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Fastener Dimension')->only('create');
		$this->middleware('permission:Update [MAS] Fastener Dimension')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'name',
			'sortAscending' => true,
			'baseUrl' => '/api/fastener-dimensions'
		]);
		return view('fastener-dimensions.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('fastener-dimensions.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$fastenerDimension = $this->service->getById($id);
		return view('fastener-dimensions.show', compact('fastenerDimension'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$fastenerDimension = $this->service->getById($id);
		return view('fastener-dimensions.edit', compact('fastenerDimension'));
	}
}
