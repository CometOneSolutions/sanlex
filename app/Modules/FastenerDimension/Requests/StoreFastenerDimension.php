<?php

namespace App\Modules\FastenerDimension\Requests;

use Dingo\Api\Http\FormRequest;

class StoreFastenerDimension extends FastenerDimensionRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [MAS] Fastener Dimension');
	}
}
