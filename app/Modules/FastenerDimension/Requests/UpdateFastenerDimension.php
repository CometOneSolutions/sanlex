<?php

namespace App\Modules\FastenerDimension\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateFastenerDimension extends FastenerDimensionRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] Fastener Dimension');
	}
}
