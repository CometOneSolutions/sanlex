<?php

namespace App\Modules\FastenerDimension\Requests;

use Dingo\Api\Http\FormRequest;

class FastenerDimensionRequest extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'name'                  => 'required|unique:fastener_dimensions,name,' . $this->route('fastenerDimensionId') ?? null,
		];
	}
	public function messages()
	{
		return [
			'name.required'         => 'Name is required',
			'name.unique'           => 'Name already exists'
		];
	}

	public function getName()
	{
		return $this->input('name');
	}
}
