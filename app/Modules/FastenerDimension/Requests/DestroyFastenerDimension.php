<?php

namespace App\Modules\FastenerDimension\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyFastenerDimension extends FastenerDimensionRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Fastener Dimension');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
