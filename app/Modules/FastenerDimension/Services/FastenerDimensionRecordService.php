<?php

namespace App\Modules\FastenerDimension\Services;

use App\Modules\FastenerDimension\Models\FastenerDimension;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface FastenerDimensionRecordService extends RecordService
{
	public function create(
		$name,
		User $user = null
	);

	public function update(
		FastenerDimension $fastenerDimension,
		$name,
		User $user = null
	);

	public function delete(FastenerDimension $fastenerDimension);
}
