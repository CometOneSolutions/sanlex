<?php

namespace App\Modules\FastenerDimension\Services;

use App\Modules\FastenerDimension\Models\FastenerDimension;
use App\Modules\FastenerDimension\Repositories\FastenerDimensionRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Auth\Models\Users\User;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class FastenerDimensionRecordServiceImpl extends RecordServiceImpl implements FastenerDimensionRecordService
{
    use CanUpdateProductName;

    private $fastenerDimension;
    private $fastenerDimensions;

    protected $availableFilters = [
        ['id' => 'name', 'text' => 'Name']
    ];

    public function __construct(FastenerDimensionRepository $fastenerDimensions, FastenerDimension $fastenerDimension)
    {
        $this->fastenerDimensions = $fastenerDimensions;
        $this->fastenerDimension = $fastenerDimension;
        parent::__construct($fastenerDimensions);
    }

    public function create(
        $name,
        User $user = null
    ) {
        $fastenerDimension = new $this->fastenerDimension;
        $fastenerDimension->setName($name);

        if ($user) {
            $fastenerDimension->setUpdatedByUser($user);
        }
        $this->fastenerDimensions->save($fastenerDimension);
        return $fastenerDimension;
    }

    public function update(
        FastenerDimension $fastenerDimension,
        $name,
        User $user = null
    ) {
        $tempFastenerDimension = clone $fastenerDimension;
        $tempFastenerDimension->setName($name);
        if ($user) {
            $tempFastenerDimension->setUpdatedByUser($user);
        }
        $this->fastenerDimensions->save($tempFastenerDimension);

        $this->updateProductableProductNames([
            $tempFastenerDimension->getFasteners(),
        ]);

        return $tempFastenerDimension;
    }

    public function delete(FastenerDimension $fastenerDimension)
    {
        $this->fastenerDimensions->delete($fastenerDimension);
        return $fastenerDimension;
    }
}
