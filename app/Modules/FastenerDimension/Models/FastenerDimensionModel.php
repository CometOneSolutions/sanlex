<?php

namespace App\Modules\FastenerDimension\Models;


use Illuminate\Database\Eloquent\Model;
use App\Modules\Fastener\Models\FastenerModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;

class FastenerDimensionModel extends Model implements FastenerDimension, UpdatableByUser
{
    use HasUpdatedByUser;

    protected $table = 'fastener_dimensions';

    public function fasteners()
    {
        return $this->hasMany(FastenerModel::class, 'fastener_dimension_id');
    }

    public function getFasteners()
    {
        return $this->fasteners;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
