<?php

namespace App\Modules\FastenerDimension\Models;



interface FastenerDimension
{
	public function getId();

	public function getName();

	public function setName($value);
}
