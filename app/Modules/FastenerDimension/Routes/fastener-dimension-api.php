<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'fastener-dimensions'], function () use ($api) {
		$api->get('/', 'App\Modules\FastenerDimension\Controllers\FastenerDimensionApiController@index');
		$api->get('{fastenerDimensionId}', 'App\Modules\FastenerDimension\Controllers\FastenerDimensionApiController@show');
		$api->post('/', 'App\Modules\FastenerDimension\Controllers\FastenerDimensionApiController@store');
		$api->patch('{fastenerDimensionId}', 'App\Modules\FastenerDimension\Controllers\FastenerDimensionApiController@update');
		$api->delete('{fastenerDimensionId}', 'App\Modules\FastenerDimension\Controllers\FastenerDimensionApiController@destroy');
	});
});
