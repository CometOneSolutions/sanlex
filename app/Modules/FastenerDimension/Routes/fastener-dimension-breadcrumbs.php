<?php

Breadcrumbs::register('fastener-dimension.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Fastener Dimensions', route('fastener-dimension_index'));
});
Breadcrumbs::register('fastener-dimension.create', function ($breadcrumbs) {
	$breadcrumbs->parent('fastener-dimension.index');
	$breadcrumbs->push('Create', route('fastener-dimension_create'));
});
Breadcrumbs::register('fastener-dimension.show', function ($breadcrumbs, $fastenerDimension) {
	$breadcrumbs->parent('fastener-dimension.index');
	$breadcrumbs->push($fastenerDimension->getName(), route('fastener-dimension_show', $fastenerDimension->getId()));
});
Breadcrumbs::register('fastener-dimension.edit', function ($breadcrumbs, $fastenerDimension) {
	$breadcrumbs->parent('fastener-dimension.show', $fastenerDimension);
	$breadcrumbs->push('Edit', route('fastener-dimension_edit', $fastenerDimension->getId()));
});
