<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'fastener-dimensions'], function () {
		Route::get('/', 'FastenerDimensionController@index')->name('fastener-dimension_index');
		Route::get('/create', 'FastenerDimensionController@create')->name('fastener-dimension_create');
		Route::get('{fastenerDimensionId}/edit', 'FastenerDimensionController@edit')->name('fastener-dimension_edit');
		Route::get('{fastenerDimensionId}/print', 'FastenerDimensionController@print')->name('fastener-dimension_print');
		Route::get('{fastenerDimensionId}', 'FastenerDimensionController@show')->name('fastener-dimension_show');
	});
});
