<?php

namespace App\Modules\StainlessSteel\Requests;

use Dingo\Api\Http\FormRequest;

class StoreStainlessSteel extends StainlessSteelRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Stainless Steel');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
