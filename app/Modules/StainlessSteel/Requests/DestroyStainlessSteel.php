<?php

namespace App\Modules\StainlessSteel\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyStainlessSteel extends StainlessSteelRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Stainless Steel');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
