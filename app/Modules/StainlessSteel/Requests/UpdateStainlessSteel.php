<?php

namespace App\Modules\StainlessSteel\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateStainlessSteel extends StainlessSteelRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Stainless Steel');
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
