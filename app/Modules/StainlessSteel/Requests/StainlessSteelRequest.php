<?php

namespace App\Modules\StainlessSteel\Requests;

use App\Modules\Finish\Services\FinishRecordService;
use App\Modules\Length\Services\LengthRecordService;
use App\Modules\StainlessSteel\Services\StainlessSteelRecordService;
use App\Modules\StainlessSteelType\Services\StainlessSteelTypeRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\Thickness\Services\ThicknessRecordService;
use App\Modules\Width\Services\WidthRecordService;
use Dingo\Api\Http\FormRequest;

class StainlessSteelRequest extends FormRequest
{
	private $service;
	private $supplierRecordService;
	private $widthRecordService;
	private $lengthRecordService;
	private $thicknessRecordService;
	private $stainlessSteelTypeRecordService;
	private $finishRecordService;

	public function __construct(
		SupplierRecordService $supplierRecordService,
		StainlessSteelRecordService $stainlessSteelRecordService,
		WidthRecordService $widthRecordService,
		LengthRecordService $lengthRecordService,
		ThicknessRecordService $thicknessRecordService,
		StainlessSteelTypeRecordService $stainlessSteelTypeRecordService,
		FinishRecordService $finishRecordService
	) {
		$this->supplierRecordService = $supplierRecordService;
		$this->service = $stainlessSteelRecordService;
		$this->widthRecordService = $widthRecordService;
		$this->lengthRecordService = $lengthRecordService;
		$this->thicknessRecordService = $thicknessRecordService;
		$this->stainlessSteelTypeRecordService = $stainlessSteelTypeRecordService;
		$this->finishRecordService = $finishRecordService;
	}

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$stainlessSteel = $this->route('stainlessSteelId') ? $this->service->getById($this->route('stainlessSteelId')) : null;

		if ($stainlessSteel) {
			return [
				'name'          => 'required|unique:products,name,' . $stainlessSteel->getProduct()->getId(),

				'widthId'       => 'required',
				'lengthId'      => 'required',
				'thicknessId'   => 'required',
				'stainlessSteelTypeId' => 'required'
			];
		} else {
			return [
				'name'                  => 'required|unique:products,name',

				'widthId'               => 'required',
				'lengthId'              => 'required',
				'thicknessId'           => 'required',
				'stainlessSteelTypeId'       => 'required'
			];
		}
	}
	public function messages()
	{
		return [
			'name.required'                  => 'Name is required.',
			'name.unique'                    => 'Name already exists.',

			'widthId.required'               => 'Width is required.',
			'lengthId.required'              => 'Length is required.',
			'thicknessId.required'           => 'Thickness is required.',
			'stainlessSteelTypeId.required'       => 'StainlessSteel class is required.'
		];
	}



	public function getName($index = 'name')
	{
		return $this->input($index);
	}

	public function getSupplier($index = 'supplierId')
	{

		return $this->supplierRecordService->getById($this->input($index));
	}

	public function getStainlessSteel($index = 'id')
	{
		return $this->service->getById($this->input($index));
	}

	public function getWidth($index = 'widthId')
	{
		return $this->widthRecordService->getById($this->input($index));
	}
	public function getLength($index = 'lengthId')
	{


		return $this->lengthRecordService->getById($this->input($index));
	}
	public function getThickness($index = 'thicknessId')
	{
		return $this->thicknessRecordService->getById($this->input($index));
	}
	public function getStainlessSteelType($index = 'stainlessSteelTypeId')
	{

		return $this->stainlessSteelTypeRecordService->getById($this->input($index));
	}
	public function getFinish($index = 'finishId')
	{
		return $this->finishRecordService->getById($this->input($index));
	}
}
