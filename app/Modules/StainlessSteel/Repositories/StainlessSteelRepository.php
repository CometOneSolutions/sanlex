<?php

namespace App\Modules\StainlessSteel\Repositories;

use App\Modules\StainlessSteel\Models\StainlessSteel;
use CometOneSolutions\Common\Repositories\Repository;

interface StainlessSteelRepository extends Repository
{
    public function save(StainlessSteel $stainlessSteel);
    public function delete(StainlessSteel $stainlessSteel);
}
