<?php

namespace App\Modules\StainlessSteel\Repositories;

use App\Modules\StainlessSteel\Models\StainlessSteel;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentStainlessSteelRepository extends EloquentRepository implements StainlessSteelRepository
{
    public function __construct(StainlessSteel $stainlessSteel)
    {
        parent::__construct($stainlessSteel);
    }

    public function save(StainlessSteel $stainlessSteel)
    {
        return $stainlessSteel->save();
    }

    public function delete(StainlessSteel $stainlessSteel)
    {
        $product = $stainlessSteel->getProduct();
        $product->delete();
        return $stainlessSteel->delete();
    }
    public function filterByName($productName)
    {
        return $this->model->whereHas('product', function ($product) use ($productName) {
            return $product->where('name', 'like', '%' . $productName . '%');
        });
    }
    public function filterBySupplierName($supplierName)
    {
        return $this->model->whereHas('product', function ($product) use ($supplierName) {
            return $product->whereHas('supplier', function ($supplier) use ($supplierName) {
                return $supplier->where('name', 'like', '%' . $supplierName . '%');
            });
        });
    }
    public function inStock()
    {
        $this->model = $this->model->inStock();
        return $this;
    }
}
