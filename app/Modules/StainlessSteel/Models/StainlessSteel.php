<?php

namespace App\Modules\StainlessSteel\Models;



interface StainlessSteel
{
    public function getId();

    public function getDescription();

    public function setDescription($value);
}
