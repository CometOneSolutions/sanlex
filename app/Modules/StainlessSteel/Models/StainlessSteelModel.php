<?php

namespace App\Modules\StainlessSteel\Models;

use App\Modules\Finish\Models\Finish;
use App\Modules\Finish\Models\FinishModel;
use App\Modules\Length\Models\Length;
use App\Modules\Length\Models\LengthModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Product\Traits\Productable;
use App\Modules\StainlessSteelType\Models\StainlessSteelType;
use App\Modules\StainlessSteelType\Models\StainlessSteelTypeModel;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Thickness\Models\ThicknessModel;
use App\Modules\Width\Models\Width;
use App\Modules\Width\Models\WidthModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;

class StainlessSteelModel extends Model implements StainlessSteel, UpdatableByUser, Productable
{
    use HasUpdatedByUser;

    protected $table = 'stainless_steels';

    public function product()
    {
        return $this->morphOne(ProductModel::class, 'productable');
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

    public function stainlessSteelType()
    {
        return $this->belongsTo(StainlessSteelTypeModel::class, 'stainless_steel_type_id');
    }
    public function finish()
    {
        return $this->belongsTo(FinishModel::class, 'finish_id');
    }
    public function width()
    {
        return $this->belongsTo(WidthModel::class, 'width_id');
    }

    public function length()
    {
        return $this->belongsTo(LengthModel::class, 'length_id');
    }

    public function thickness()
    {
        return $this->belongsTo(ThicknessModel::class, 'thickness_id');
    }

    public function getThicknessId()
    {
        return (int) $this->thickness_id;
    }
    public function getThickness()
    {
        return $this->thickness;
    }

    public function setThickness(Thickness $thickness)
    {
        $this->thickness()->associate($thickness);
        $this->thickness_id = $thickness->getId();
        return $this;
    }

    public function getFinishId()
    {
        return (int) $this->finish_id;
    }
    public function getFinish()
    {
        return $this->finish;
    }

    public function setFinish(Finish $finish)
    {
        $this->finish()->associate($finish);
        $this->finish_id = $finish->getId();
        return $this;
    }
    public function getWidthId()
    {
        return (int) $this->width_id;
    }
    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth(Width $width)
    {
        $this->width()->associate($width);
        $this->width_id = $width->getId();
        return $this;
    }
    public function getLengthId()
    {
        return (int) $this->length_id;
    }
    public function getLength()
    {
        return $this->length;
    }

    public function setLength(Length $length)
    {
        $this->length()->associate($length);
        $this->length_id = $length->getId();
        return $this;
    }

    public function getStainlessSteelType()
    {
        return $this->stainlessSteelType;
    }

    public function setStainlessSteelType(StainlessSteelType $stainlessSteelType)
    {
        $this->stainlessSteelType()->associate($stainlessSteelType);
        return $this;
    }

    public function getStainlessSteelTypeId()
    {
        return $this->stainlessSteel_class_id;
    }

    public function isCoil()
    {
        return false;
    }
    public function scopeInStock($query)
    {
        return $query->whereHas('product', function ($product) {
            return $product->whereHas('inventory', function ($inventory) {
                return $inventory->where('stock', '>', 0);
            });
        });
    }

    public function getAssembledProductName()
    {
        $nameAttributes = [
            $this->getProduct()->getSupplier()->getShortCode(),
            $this->getStainlessSteelType()->getName(),
            $this->getThickness()->getName(),
            $this->getWidth()->getName(),
            $this->getLength()->getName(),
            $this->getFinish()->getName()
        ];

        return  strtoupper(implode("~", $nameAttributes));
    }

    public function scopeStainlessSteelType($query, StainlessSteelType $stainlessSteelType)
    {
        return $query->whereStainlessSteelTypeId($stainlessSteelType->getId());
    }

    public function scopeFinish($query, Finish $finish)
    {
        return $query->whereFinishId($finish->getId());
    }

    public function scopeLength($query, Length $length)
    {
        return $query->whereLengthId($length->getId());
    }

    public function scopeThickness($query, Thickness $thickness)
    {
        return $query->whereThicknessId($thickness->getId());
    }

    public function scopeWidth($query, Width $width)
    {
        return $query->whereWidthId($width->getId());
    }
}
