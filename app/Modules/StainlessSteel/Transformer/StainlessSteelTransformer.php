<?php

namespace App\Modules\StainlessSteel\Transformer;

use App\Modules\Finish\Transformer\FinishTransformer;
use App\Modules\Length\Transformer\LengthTransformer;
use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\StainlessSteel\Models\StainlessSteel;
use App\Modules\StainlessSteelType\Transformer\StainlessSteelTypeTransformer;
use App\Modules\Thickness\Transformer\ThicknessTransformer;
use App\Modules\Width\Transformer\WidthTransformer;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;
use League\Fractal;


class StainlessSteelTransformer extends UpdatableByUserTransformer
{
	protected $availableIncludes = [
		'product', 'width', 'thickness', 'length', 'stainlessSteelType', 'finish'
	];
	public function transform(StainlessSteel $stainlessSteel)
	{
		return [
			'id' => (int) $stainlessSteel->getId(),
			'name' => $stainlessSteel->getProduct() ? $stainlessSteel->getProduct()->getName() : null,
			'supplierId' => $stainlessSteel->getProduct() ? $stainlessSteel->getProduct()->getSupplier()->getId() : null,
			'widthId' => $stainlessSteel->getWidthId(),
			'widthText' => null,
			'finishId' => $stainlessSteel->getFinishId(),
			'finishText' => null,
			'thicknessId' => $stainlessSteel->getThicknessId(),
			'thicknessText' => null,
			'lengthId'    => $stainlessSteel->getLengthId(),
			'lengthText' => null,
			'stainlessSteelTypeId' => $stainlessSteel->getStainlessSteelTypeId(),
			'stainlessSteelTypeText' => null,

			'updatedAt' => $stainlessSteel->updated_at,
			'editUri' => route('stainless-steel_edit', $stainlessSteel->getId()),
			'showUri' => route('stainless-steel_show', $stainlessSteel->getId()),
		];
	}

	public function includeProduct(StainlessSteel $stainlessSteel)
	{
		$product = $stainlessSteel->getProduct();
		return $this->item($product, new ProductTransformer);
	}
	public function includeWidth(StainlessSteel $stainlessSteel)
	{
		$width = $stainlessSteel->getWidth();
		return $this->item($width, new WidthTransformer);
	}
	public function includeThickness(StainlessSteel $stainlessSteel)
	{
		$thickness = $stainlessSteel->getThickness();
		return $this->item($thickness, new ThicknessTransformer);
	}
	public function includeLength(StainlessSteel $stainlessSteel)
	{
		$length = $stainlessSteel->getLength();
		return $this->item($length, new LengthTransformer);
	}

	public function includeStainlessSteelType(StainlessSteel $stainlessSteel)
	{
		$stainlessSteelType = $stainlessSteel->getStainlessSteelType();
		return $this->item($stainlessSteelType, new StainlessSteelTypeTransformer);
	}
	public function includeFinish(StainlessSteel $stainlessSteel)
	{
		$finish = $stainlessSteel->getFinish();
		return $this->item($finish, new FinishTransformer);
	}
}
