import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select3 from "@c1_common_js/components/Select3";


new Vue({
    el: '#stainless-steel',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select3
    },
    data: {
        form: new Form({
            name: null,
            supplierId: null,
            shortCode: null,
            widthId: null,
            widthText: null,
            lengthId: null,
            lengthText: null,
            thicknessId: null,
            thicknessText: null,
            stainlessSteelTypeId: null,
            stainlessSteelTypeText: null,
            finishId: null,
            finishText: null
        }),
        dataInitialized: true,
        supplierUrl: '/api/suppliers?sort=name',
        defaultSelectedSupplier: {},
        widthUrl: '/api/widths?sort=name',
        defaultSelectedWidth: {},
        lengthUrl: '/api/lengths?sort=name',
        defaultSelectedLength: {},
        thicknessUrl: '/api/thicknesses?sort=name',
        defaultSelectedThickness: {},
        finishUrl: '/api/finishes?sort=name',
        defaultSelectedFinish: {},

        stainlessSteelTypeUrl: '/api/stainless-steel-types?sort=name',
        defaultSelectedStainlessSteelType: {},

    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },
    computed: {
        // selectedSupplier() {
        //     if(this.form.supplierId == null)
        //     {
        //         return undefined;
        //     }
        //     return this.supplierSelections.find(supplier => supplier.id == this.form.supplierId);
        // },
        name() {
            if (this.form.supplierId === undefined || this.form.widthId === null || this.form.lengthId === null || this.form.stainlessSteelTypeId === null || this.form.thicknessId === null || this.form.finishId === null) {
                return undefined;
            }
            return this.form.shortCode + '~' + this.form.stainlessSteelTypeText.toUpperCase()
                + '~' + this.form.thicknessText.toUpperCase() + '~' + this.form.widthText.toUpperCase() + '~' + this.form.lengthText.toUpperCase() + '~' + this.form.finishText.toUpperCase();
        },
        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        setShortCode(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("shortCode")) {
                this.form.shortCode = selectedObjects[0].shortCode;
            }
        },
        setLength(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.lengthText = selectedObjects[0].name;
            }

        },
        setThickness(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.thicknessText = selectedObjects[0].name;
            }

        },
        setFinish(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.finishText = selectedObjects[0].name;
            }

        },
        setWidth(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.widthText = selectedObjects[0].name;
            }

        },
        setStainlessSteelType(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.stainlessSteelTypeText = selectedObjects[0].name;
            }

        },
        destroy() {
            this.form.deleteWithConfirmation('/api/stainless-steels/' + this.form.id).then(response => {
                this.form.successModal('Stainless steel was removed.').then(() =>
                    window.location = '/master-file/stainless-steels/'
                );
            });
        },

        store() {
            this.form.name = this.name;
            this.form.postWithModal('/api/stainless-steels', null, 'Stainless steel was saved.');
        },

        update() {
            this.form.name = this.name;
            this.form.patch('/api/stainless-steels/' + this.form.id).then(response => {
                this.form.successModal('Stainless steel was updated.').then(() =>
                    window.location = '/master-file/stainless-steels/' + this.form.id
                );
            })
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {


        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/stainless-steels/' + id + '?include=product.supplier,width,thickness,length,stainlessSteelType, finish')
                .then(response => {
                    this.loadData(response.data);

                    this.form.shortCode = response.data.product.data.supplier.data.shortCode;
                    this.form.lengthText = response.data.length.data.name;
                    this.form.widthText = response.data.width.data.name;
                    this.form.thicknessText = response.data.thickness.data.name;
                    this.form.stainlessSteelTypeText = response.data.stainlessSteelType.data.name;
                    this.form.finishText = response.data.finish.data.name;

                    this.defaultSelectedSupplier = {
                        text: response.data.product.data.supplier.data.name,
                        id: response.data.product.data.supplier.data.id,
                    };
                    this.defaultSelectedWidth = {
                        text: response.data.width.data.name,
                        id: response.data.width.data.id,
                    };
                    this.defaultSelectedThickness = {
                        text: response.data.thickness.data.name,
                        id: response.data.thickness.data.id,
                    };
                    this.defaultSelectedLength = {
                        text: response.data.length.data.name,
                        id: response.data.length.data.id,
                    };
                    this.defaultSelectedFinish = {
                        text: response.data.finish.data.name,
                        id: response.data.finish.data.id,
                    };

                    this.defaultSelectedStainlessSteelType = {
                        text: response.data.stainlessSteelType.data.name,
                        id: response.data.stainlessSteelType.data.id,
                    };
                    this.dataInitialized = true;
                });
        }
    },
    mounted() {
        console.log("Init stainless steel script...");
    }
});