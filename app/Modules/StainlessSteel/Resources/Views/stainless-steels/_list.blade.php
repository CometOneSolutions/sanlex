<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Name</label>
            <p>@{{form.product.data.name }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Supplier</label>
            <p>@{{form.product.data.supplier.data.name }}</p>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Width</label>
            <p>@{{form.width.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Thickness</label>
            <p>@{{form.thickness.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Length</label>
            <p>@{{form.length.data.name || "--" }}</p>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Stainless Steel Type</label>
            <p>@{{form.stainlessSteelType.data.name || "--" }}</p>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{ mix('js/stainless-steel.js') }}"></script>
@endpush