<?php

namespace App\Modules\StainlessSteel\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\StainlessSteel\Services\StainlessSteelRecordService;
use App\Modules\StainlessSteel\Transformer\StainlessSteelTransformer;
use App\Modules\StainlessSteel\Requests\StoreStainlessSteel;
use App\Modules\StainlessSteel\Requests\UpdateStainlessSteel;
use App\Modules\StainlessSteel\Requests\DestroyStainlessSteel;
use App\Modules\Product\Services\ProductRecordService;


class StainlessSteelApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;
	protected $productRecordService;

	public function __construct(
		StainlessSteelRecordService $stainlessSteelRecordService,
		StainlessSteelTransformer $transformer,
		ProductRecordService $productRecordService
	) {
		$this->middleware('auth:api');
		parent::__construct($stainlessSteelRecordService, $transformer);
		$this->service = $stainlessSteelRecordService;
		$this->transformer = $transformer;
		$this->productRecordService = $productRecordService;
	}

	public function store(StoreStainlessSteel $request)
	{
		$stainlessSteel = \DB::transaction(function () use ($request) {
			$name = $request->getName();

			return $this->service->create(
				$name,
				$request->getSupplier(),

				$request->getWidth(),
				$request->getThickness(),
				$request->getLength(),
				$request->getStainlessSteelType(),
				$request->getFinish(),
				$request->user()
			);
		});
		return $this->response->item($stainlessSteel, $this->transformer)->setStatusCode(201);
	}

	public function update($stainlessSteelId, UpdateStainlessSteel $request)
	{
		$stainlessSteel = \DB::transaction(function () use ($request) {
			$name = $request->getName();

			return $this->service->update(
				$request->getStainlessSteel(),
				$name,
				$request->getSupplier(),

				$request->getWidth(),
				$request->getThickness(),
				$request->getLength(),
				$request->getStainlessSteelType(),
				$request->getFinish(),
				$request->user()
			);
		});
		return $this->response->item($stainlessSteel, $this->transformer)->setStatusCode(200);
	}

	public function destroy($stainlessSteelId, DestroyStainlessSteel $request)
	{
		$stainlessSteel = $this->service->getById($stainlessSteelId);
		$this->service->delete($stainlessSteel);
		return $this->response->item($stainlessSteel, $this->transformer)->setStatusCode(200);
	}
}
