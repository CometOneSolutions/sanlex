<?php

namespace App\Modules\StainlessSteel\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\StainlessSteel\Services\StainlessSteelRecordService;


class StainlessSteelController extends Controller
{
    private $service;

    public function __construct(StainlessSteelRecordService $stainlessSteelRecordService)
    {
        $this->service = $stainlessSteelRecordService;
        $this->middleware('permission:Read [MAS] Stainless Steel')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Stainless Steel')->only('create');
        $this->middleware('permission:Update [MAS] Stainless Steel')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/stainless-steels?include=product,width,thickness,length,stainlessSteelType,finish'
        ]);
        return view('stainless-steels.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('stainless-steels.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $stainlessSteel = $this->service->getById($id);
        return view('stainless-steels.show', compact('stainlessSteel'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $stainlessSteel = $this->service->getById($id);
        return view('stainless-steels.edit', compact('stainlessSteel'));
    }
}
