<?php

namespace App\Modules\StainlessSteel\Provider;

use App\Modules\StainlessSteel\Models\StainlessSteel;
use App\Modules\StainlessSteel\Models\StainlessSteelModel;
use App\Modules\StainlessSteel\Repositories\EloquentStainlessSteelRepository;
use App\Modules\StainlessSteel\Repositories\StainlessSteelRepository;
use App\Modules\StainlessSteel\Services\StainlessSteelRecordService;
use App\Modules\StainlessSteel\Services\StainlessSteelRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class StainlessSteelServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/StainlessSteel/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(StainlessSteelRecordService::class, StainlessSteelRecordServiceImpl::class);
        $this->app->bind(StainlessSteelRepository::class, EloquentStainlessSteelRepository::class);
        $this->app->bind(StainlessSteel::class, StainlessSteelModel::class);
    }
}
