<?php

namespace App\Modules\StainlessSteel\Services;

use App\Modules\Width\Models\Width;
use App\Modules\Finish\Models\Finish;
use App\Modules\Length\Models\Length;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use CometOneSolutions\Auth\Models\Users\User;
use App\Modules\StainlessSteel\Models\StainlessSteel;
use App\Modules\Product\Services\ProductRecordService;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use App\Modules\StainlessSteelType\Models\StainlessSteelType;
use App\Modules\StainlessSteel\Repositories\StainlessSteelRepository;


class StainlessSteelRecordServiceImpl extends RecordServiceImpl implements StainlessSteelRecordService
{
    private $stainlessSteel;
    private $stainlessSteels;
    private $productRecordService;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"],

    ];

    public function __construct(StainlessSteelRepository $stainlessSteels, StainlessSteel $stainlessSteel, ProductRecordService $productRecordService)
    {
        $this->stainlessSteels = $stainlessSteels;
        $this->stainlessSteel = $stainlessSteel;
        $this->productRecordService = $productRecordService;
        parent::__construct($stainlessSteels);
    }

    public function create(
        $name,
        Supplier $supplier,

        Width $width,
        Thickness $thickness,
        Length $length,
        StainlessSteelType $stainlessSteelType,
        Finish $finish,
        User $user = null
    ) {
        $stainlessSteel = new $this->stainlessSteel;
        $stainlessSteel->setWidth($width);
        $stainlessSteel->setThickness($thickness);
        $stainlessSteel->setLength($length);

        $stainlessSteel->setStainlessSteelType($stainlessSteelType);
        $stainlessSteel->setFinish($finish);
        if ($user) {
            $stainlessSteel->setUpdatedByUser($user);
        }
        $this->stainlessSteels->save($stainlessSteel);
        $this->productRecordService->create($name, $supplier, $stainlessSteel, $user);
        return $stainlessSteel;
    }

    public function update(
        StainlessSteel $stainlessSteel,
        $name,
        Supplier $supplier,

        Width $width,
        Thickness $thickness,
        Length $length,

        StainlessSteelType $stainlessSteelType,
        Finish $finish,
        User $user = null
    ) {
        $tempStainlessSteel = clone $stainlessSteel;
        $product = $tempStainlessSteel->getProduct();
        $tempStainlessSteel->setWidth($width);
        $tempStainlessSteel->setThickness($thickness);
        $tempStainlessSteel->setLength($length);

        $tempStainlessSteel->setStainlessSteelType($stainlessSteelType);
        $tempStainlessSteel->setFinish($finish);
        if ($user) {
            $tempStainlessSteel->setUpdatedByUser($user);
        }
        $this->stainlessSteels->save($tempStainlessSteel);
        $this->productRecordService->update($product, $name, $supplier, $tempStainlessSteel, $user);
        return $tempStainlessSteel;
    }

    public function delete(StainlessSteel $stainlessSteel)
    {
        $this->stainlessSteels->delete($stainlessSteel);
        return $stainlessSteel;
    }
    public function inStock()
    {
        $this->stainlessSteels = $this->stainlessSteels->inStock();
        return $this;
    }
}
