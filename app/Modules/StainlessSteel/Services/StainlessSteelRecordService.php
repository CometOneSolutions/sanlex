<?php

namespace App\Modules\StainlessSteel\Services;

use App\Modules\Finish\Models\Finish;
use App\Modules\Length\Models\Length;
use App\Modules\StainlessSteel\Models\StainlessSteel;
use App\Modules\StainlessSteelType\Models\StainlessSteelType;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface StainlessSteelRecordService extends RecordService
{
    public function create(
        $name,
        Supplier $supplier,
        Width $width,
        Thickness $thickness,
        Length $length,
        StainlessSteelType $stainlessSteelType,
        Finish $finish,
        User $user = null
    );

    public function update(
        StainlessSteel $stainlessSteel,
        $name,
        Supplier $supplier,
        Width $width,
        Thickness $thickness,
        Length $length,
        StainlessSteelType $stainlessSteelType,
        Finish $finish,
        User $user = null
    );

    public function delete(StainlessSteel $stainlessSteel);
}
