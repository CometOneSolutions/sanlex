<?php

Breadcrumbs::register('stainless-steel.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Stainless Steels', route('stainless-steel_index'));
});
Breadcrumbs::register('stainless-steel.create', function ($breadcrumbs) {
    $breadcrumbs->parent('stainless-steel.index');
    $breadcrumbs->push('Create', route('stainless-steel_create'));
});
Breadcrumbs::register('stainless-steel.show', function ($breadcrumbs, $stainlessSteel) {
    $breadcrumbs->parent('stainless-steel.index');
    $breadcrumbs->push($stainlessSteel->getProduct()->getName(), route('stainless-steel_show', $stainlessSteel->getId()));
});
Breadcrumbs::register('stainless-steel.edit', function ($breadcrumbs, $stainlessSteel) {
    $breadcrumbs->parent('stainless-steel.show', $stainlessSteel);
    $breadcrumbs->push('Edit', route('stainless-steel_edit', $stainlessSteel->getId()));
});
