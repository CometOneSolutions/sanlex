<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'stainless-steels'], function () use ($api) {
        $api->get('/', 'App\Modules\StainlessSteel\Controllers\StainlessSteelApiController@index');
        $api->get('{stainlessSteelId}', 'App\Modules\StainlessSteel\Controllers\StainlessSteelApiController@show');
        $api->post('/', 'App\Modules\StainlessSteel\Controllers\StainlessSteelApiController@store');
        $api->patch('{stainlessSteelId}', 'App\Modules\StainlessSteel\Controllers\StainlessSteelApiController@update');
        $api->delete('{stainlessSteelId}', 'App\Modules\StainlessSteel\Controllers\StainlessSteelApiController@destroy');
    });
});
