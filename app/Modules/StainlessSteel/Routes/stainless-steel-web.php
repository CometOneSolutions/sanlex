<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'stainless-steels'], function () {
        Route::get('/', 'StainlessSteelController@index')->name('stainless-steel_index');
        Route::get('/create', 'StainlessSteelController@create')->name('stainless-steel_create');
        Route::get('{stainlessSteelId}/edit', 'StainlessSteelController@edit')->name('stainless-steel_edit');
        Route::get('{stainlessSteelId}/print', 'StainlessSteelController@print')->name('stainless-steel_print');
        Route::get('{stainlessSteelId}', 'StainlessSteelController@show')->name('stainless-steel_show');
    });
});
