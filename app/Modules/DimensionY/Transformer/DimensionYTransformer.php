<?php

namespace App\Modules\DimensionY\Transformer;

use App\Modules\DimensionY\Models\DimensionY;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class DimensionYTransformer extends UpdatableByUserTransformer
{
	public function transform(DimensionY $dimensionY)
	{
		return [
			'id' => (int) $dimensionY->getId(),
			'text' => $dimensionY->getName(),
			'name' => $dimensionY->getName(),
			'updatedAt' => $dimensionY->updated_at,
			'editUri' => route('dimension-y_edit', $dimensionY->getId()),
			'showUri' => route('dimension-y_show', $dimensionY->getId()),
		];
	}
}
