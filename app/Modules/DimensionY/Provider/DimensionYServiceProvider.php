<?php

namespace App\Modules\DimensionY\Provider;

use App\Modules\DimensionY\Models\DimensionY;
use App\Modules\DimensionY\Models\DimensionYModel;
use App\Modules\DimensionY\Repositories\DimensionYRepository;
use App\Modules\DimensionY\Repositories\EloquentDimensionYRepository;
use App\Modules\DimensionY\Services\DimensionYRecordService;
use App\Modules\DimensionY\Services\DimensionYRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class DimensionYServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/DimensionY/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(DimensionYRecordService::class, DimensionYRecordServiceImpl::class);
		$this->app->bind(DimensionYRepository::class, EloquentDimensionYRepository::class);
		$this->app->bind(DimensionY::class, DimensionYModel::class);
	}
}
