<?php

namespace App\Modules\DimensionY\Models;



interface DimensionY
{
	public function getId();

	public function getName();

	public function setName($value);
}
