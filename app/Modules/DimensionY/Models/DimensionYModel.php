<?php

namespace App\Modules\DimensionY\Models;

use App\Modules\StructuralSteel\Models\StructuralSteelModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;

use CometOneSolutions\Auth\Models\Users\User;

class DimensionYModel extends Model implements DimensionY, UpdatableByUser
{
	use HasUpdatedByUser;

	protected $table = 'dimension_y';

	public function structuralSteels()
	{
		return $this->hasMany(StructuralSteelModel::class, 'dimension_y_id');
	}

	public function getStructuralSteels()
	{
		return $this->structuralSteels;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($value)
	{
		$this->name = $value;
		return $this;
	}
}
