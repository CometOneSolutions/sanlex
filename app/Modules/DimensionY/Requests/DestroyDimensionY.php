<?php

namespace App\Modules\DimensionY\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyDimensionY extends DimensionYRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Dimension Y');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
