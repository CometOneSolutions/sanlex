<?php

namespace App\Modules\DimensionY\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateDimensionY extends DimensionYRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] Dimension Y');
	}
}
