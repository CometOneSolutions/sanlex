<?php

namespace App\Modules\DimensionY\Requests;

use Dingo\Api\Http\FormRequest;

class StoreDimensionY extends DimensionYRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [MAS] Dimension Y');
	}
}
