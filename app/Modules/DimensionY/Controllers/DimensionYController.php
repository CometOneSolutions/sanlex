<?php

namespace App\Modules\DimensionY\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\DimensionY\Services\DimensionYRecordService;


class DimensionYController extends Controller
{
	private $service;

	public function __construct(DimensionYRecordService $dimensionYRecordService)
	{
		$this->service = $dimensionYRecordService;
		$this->middleware('permission:Read [MAS] Dimension Y')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Dimension Y')->only('create');
		$this->middleware('permission:Update [MAS] Dimension Y')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'name',
			'sortAscending' => true,
			'baseUrl' => '/api/dimension-y'
		]);
		return view('dimension-y.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('dimension-y.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$dimensionY = $this->service->getById($id);
		return view('dimension-y.show', compact('dimensionY'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$dimensionY = $this->service->getById($id);
		return view('dimension-y.edit', compact('dimensionY'));
	}
}
