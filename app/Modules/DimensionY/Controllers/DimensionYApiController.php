<?php

namespace App\Modules\DimensionY\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\DimensionY\Services\DimensionYRecordService;
use App\Modules\DimensionY\Transformer\DimensionYTransformer;
use App\Modules\DimensionY\Requests\StoreDimensionY;
use App\Modules\DimensionY\Requests\UpdateDimensionY;
use App\Modules\DimensionY\Requests\DestroyDimensionY;

class DimensionYApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;

	public function __construct(
		DimensionYRecordService $dimensionYRecordService,
		DimensionYTransformer $transformer
	) {
		$this->middleware('auth:api');
		parent::__construct($dimensionYRecordService, $transformer);
		$this->service = $dimensionYRecordService;
		$this->transformer = $transformer;
	}

	public function store(StoreDimensionY $request)
	{
		$dimensionY = \DB::transaction(function () use ($request) {
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->create(
				$name,

				$user
			);
		});
		return $this->response->item($dimensionY, $this->transformer)->setStatusCode(201);
	}

	public function update($dimensionYId, UpdateDimensionY $request)
	{
		$dimensionY = \DB::transaction(function () use ($dimensionYId, $request) {
			$dimensionY   = $this->service->getById($dimensionYId);
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->update(
				$dimensionY,
				$name,

				$user
			);
		});
		return $this->response->item($dimensionY, $this->transformer)->setStatusCode(200);
	}

	public function destroy($dimensionYId, DestroyDimensionY $request)
	{
		$dimensionY = $this->service->getById($dimensionYId);
		$this->service->delete($dimensionY);
		return $this->response->item($dimensionY, $this->transformer)->setStatusCode(200);
	}
}
