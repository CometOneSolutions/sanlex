<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'dimension-y'], function () {
		Route::get('/', 'DimensionYController@index')->name('dimension-y_index');
		Route::get('/create', 'DimensionYController@create')->name('dimension-y_create');
		Route::get('{dimensionYId}/edit', 'DimensionYController@edit')->name('dimension-y_edit');
		Route::get('{dimensionYId}/print', 'DimensionYController@print')->name('dimension-y_print');
		Route::get('{dimensionYId}', 'DimensionYController@show')->name('dimension-y_show');
	});
});
