<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'dimension-y'], function () use ($api) {
		$api->get('/', 'App\Modules\DimensionY\Controllers\DimensionYApiController@index');
		$api->get('{dimensionYId}', 'App\Modules\DimensionY\Controllers\DimensionYApiController@show');
		$api->post('/', 'App\Modules\DimensionY\Controllers\DimensionYApiController@store');
		$api->patch('{dimensionYId}', 'App\Modules\DimensionY\Controllers\DimensionYApiController@update');
		$api->delete('{dimensionYId}', 'App\Modules\DimensionY\Controllers\DimensionYApiController@destroy');
	});
});
