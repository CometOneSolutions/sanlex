<?php

Breadcrumbs::register('dimension-y.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Dimension Y', route('dimension-y_index'));
});
Breadcrumbs::register('dimension-y.create', function ($breadcrumbs) {
	$breadcrumbs->parent('dimension-y.index');
	$breadcrumbs->push('Create', route('dimension-y_create'));
});
Breadcrumbs::register('dimension-y.show', function ($breadcrumbs, $dimensionY) {
	$breadcrumbs->parent('dimension-y.index');
	$breadcrumbs->push($dimensionY->getName(), route('dimension-y_show', $dimensionY->getId()));
});
Breadcrumbs::register('dimension-y.edit', function ($breadcrumbs, $dimensionY) {
	$breadcrumbs->parent('dimension-y.show', $dimensionY);
	$breadcrumbs->push('Edit', route('dimension-y_edit', $dimensionY->getId()));
});
