<?php

namespace App\Modules\DimensionY\Services;

use App\Modules\DimensionY\Models\DimensionY;
use App\Modules\DimensionY\Repositories\DimensionYRepository;
use App\Modules\Product\Traits\CanUpdateProductName;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class DimensionYRecordServiceImpl extends RecordServiceImpl implements DimensionYRecordService
{
	use CanUpdateProductName;


	private $dimensionY;
	private $dimensionYs;

	protected $availableFilters = [
		["id" => "name", "text" => "Name"]
	];

	public function __construct(DimensionYRepository $dimensionYs, DimensionY $dimensionY)
	{

		$this->dimensionYs = $dimensionYs;
		$this->dimensionY = $dimensionY;
		parent::__construct($dimensionYs);
	}

	public function create(
		$name,
		User $user = null
	) {
		$dimensionY = new $this->dimensionY;
		$dimensionY->setName($name);

		if ($user) {
			$dimensionY->setUpdatedByUser($user);
		}
		$this->dimensionYs->save($dimensionY);
		return $dimensionY;
	}

	public function update(
		DimensionY $dimensionY,
		$name,
		User $user = null
	) {
		$tempDimensionY = clone $dimensionY;
		$tempDimensionY->setName($name);

		if ($user) {
			$tempDimensionY->setUpdatedByUser($user);
		}
		$this->dimensionYs->save($tempDimensionY);

		$this->updateProductableProductNames([
			$tempDimensionY->getStructuralSteels()
		]);

		return $tempDimensionY;
	}

	public function delete(DimensionY $dimensionY)
	{
		$this->dimensionYs->delete($dimensionY);
		return $dimensionY;
	}
}
