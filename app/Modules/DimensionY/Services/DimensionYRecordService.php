<?php

namespace App\Modules\DimensionY\Services;

use App\Modules\DimensionY\Models\DimensionY;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface DimensionYRecordService extends RecordService
{
	public function create(
		$name,
		User $user = null
	);

	public function update(
		DimensionY $dimensionY,
		$name,
		User $user = null
	);

	public function delete(DimensionY $dimensionY);
}
