<?php

namespace App\Modules\DimensionY\Repositories;

use App\Modules\DimensionY\Models\DimensionY;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentDimensionYRepository extends EloquentRepository implements DimensionYRepository
{
	public function __construct(DimensionY $dimensionY)
	{
		parent::__construct($dimensionY);
	}

	public function save(DimensionY $dimensionY)
	{
		return $dimensionY->save();
	}

	public function delete(DimensionY $dimensionY)
	{
		return $dimensionY->delete();
	}
}
