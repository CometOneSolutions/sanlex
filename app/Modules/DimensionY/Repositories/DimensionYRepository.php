<?php

namespace App\Modules\DimensionY\Repositories;

use App\Modules\DimensionY\Models\DimensionY;
use CometOneSolutions\Common\Repositories\Repository;

interface DimensionYRepository extends Repository
{
	public function save(DimensionY $dimensionY);
	public function delete(DimensionY $dimensionY);
}
