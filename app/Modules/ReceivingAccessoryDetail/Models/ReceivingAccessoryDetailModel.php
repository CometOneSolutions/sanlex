<?php

namespace App\Modules\ReceivingAccessoryDetail\Models;

use Illuminate\Support\Collection;
use App\Modules\Product\Models\Product;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Receiving\Models\Receiving;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Receiving\Models\ReceivingModel;
use CometOneSolutions\Inventory\Model\Inventoriable;
use CometOneSolutions\Inventory\Traits\HasItemMovements;
use App\Modules\InventoryDetail\Models\InventoryDetailModel;
use App\Modules\ReceivingAccessory\Models\ReceivingAccessoryModel;

class ReceivingAccessoryDetailModel extends Model implements ReceivingAccessoryDetail, Inventoriable
{
    use HasItemMovements;

    protected $table = 'receiving_accessory_details';
    protected $inventoryDetailsToSet = null;

    public function receivingAccessory()
    {
        return $this->belongsTo(ReceivingAccessoryModel::class, 'receiving_accessory_id');
    }

    public function product()
    {
        return $this->belongsTo(ProductModel::class, 'product_id');
    }

    public function inventoryDetails()
    {
        return $this->hasMany(InventoryDetailModel::class, 'receiving_accessory_detail_id');
    }

    public function oldReceiving()
    {
        return $this->belongsTo(ReceivingModel::class, 'old_receiving_id');
    }

    public function setOldReceiving(Receiving $oldReceiving)
    {
        $this->oldReceiving()->associate($oldReceiving);
        $this->old_receiving_id = $oldReceiving->getId();
        return $this;
    }

    public function getOldReceiving()
    {
        return $this->oldReceiving;
    }

    public function getOldReceivingId()
    {
        return $this->old_receiving_id;
    }

    public function setInventoryDetails(Collection $inventoryDetails)
    {
        $this->inventoryDetailsToSet = $inventoryDetails;
        return $this;
    }

    public function getInventoryDetails()
    {
        if ($this->inventoryDetailsToSet !== null) {
            return collect($this->inventoryDetailsToSet);
        }

        return $this->inventoryDetails;
    }


    public function getReceivingAccessory()
    {
        return $this->receivingAccessory;
    }

    public function setProduct(Product $product)
    {
        $this->product()->associate($product);
        $this->product_id = $product->getId();
        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getProductId()
    {
        return $this->product_id;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($value)
    {
        $this->quantity = $value;
        return $this;
    }

    public function scopeProduct($query, $productId)
    {
        return $query->whereProductId($productId);
    }

    public function getInventoryRefNo()
    {
        return $this->getReceivingAccessory()->getReferenceNo();
    }

    public function getInventoryUri()
    {
        return route('receiving-accessory_show', $this->getReceivingAccessory()->getId());
    }

    public function scopeInTransit($query)
    {
        return $query->whereHas('receivingAccessory', function ($receivingAccessory) {
            return $receivingAccessory->inTransit();
        });
    }

    public function scopeProductType($query, $productType)
    {
        return $query->whereHas('receivingAccessory', function ($receivingAccessory) use ($productType) {
            return $receivingAccessory->whereHas('purchaseOrderAccessory', function ($purchaseOrderAccessory) use ($productType) {
                return $purchaseOrderAccessory->whereProductType($productType);
            });
        });
    }

    public function scopeReceived($query)
    {
        return $query->whereHas('receivingAccessory', function ($receivingAccessory) {
            return $receivingAccessory->received();
        });
    }
    public function scopeOpen($query)
    {
        return $query->whereHas('receivingAccessory', function ($receivingAccessory) {
            return $receivingAccessory->whereHas('purchaseOrderAccessory', function ($purchaseOrderAccessory) {
                return $purchaseOrderAccessory->open();
            });
        });
    }
}
