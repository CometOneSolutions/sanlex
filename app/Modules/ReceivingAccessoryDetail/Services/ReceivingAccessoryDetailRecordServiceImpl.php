<?php

namespace App\Modules\ReceivingAccessoryDetail\Services;

use App\Modules\ReceivingAccessoryDetail\Repositories\ReceivingAccessoryDetails;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class ReceivingAccessoryDetailRecordServiceImpl extends RecordServiceImpl implements ReceivingAccessoryDetailRecordService
{
    private $receivingAccessoryDetails;

    public function __construct(ReceivingAccessoryDetails $receivingAccessoryDetails)
    {
        parent::__construct($receivingAccessoryDetails);
        $this->receivingAccessoryDetails = $receivingAccessoryDetails;
    }
}
