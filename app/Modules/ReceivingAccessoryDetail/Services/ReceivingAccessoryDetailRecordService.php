<?php

namespace App\Modules\ReceivingAccessoryDetail\Services;

use CometOneSolutions\Common\Services\RecordService;

interface ReceivingAccessoryDetailRecordService extends RecordService
{ }
