<?php

namespace App\Modules\ReceivingAccessoryDetail\Transformer;

use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\ReceivingAccessory\Transformer\ReceivingAccessoryTransformer;
use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetail;
use League\Fractal;

class ReceivingAccessoryDetailTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'product', 'receivingAccessory'
    ];

    public function transform(ReceivingAccessoryDetail $receivingAccessoryDetail)
    {

        return [
            'id'                              => (int) $receivingAccessoryDetail->id,
            'productId'                       => (int) $receivingAccessoryDetail->getProductId(),
            'quantity'                        => (float) $receivingAccessoryDetail->getQuantity(),
            'productName'                     => $receivingAccessoryDetail->getProduct()->getName(),
        ];
    }

    public function includeProduct(ReceivingAccessoryDetail $receivingAccessoryDetail)
    {
        $product = $receivingAccessoryDetail->getProduct();
        return $this->item($product, new ProductTransformer);
    }

    public function includeReceivingAccessory(ReceivingAccessoryDetail $receivingAccessoryDetail)
    {
        $receivingAccessory = $receivingAccessoryDetail->getReceivingAccessory();
        return $this->item($receivingAccessory, new ReceivingAccessoryTransformer);
    }
}
