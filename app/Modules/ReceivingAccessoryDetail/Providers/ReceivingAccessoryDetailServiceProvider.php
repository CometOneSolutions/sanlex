<?php

namespace App\Modules\ReceivingAccessoryDetail\Providers;

use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetail;
use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetailModel;
use App\Modules\ReceivingAccessoryDetail\Repositories\EloquentReceivingAccessoryDetails;
use App\Modules\ReceivingAccessoryDetail\Repositories\ReceivingAccessoryDetails;
use App\Modules\ReceivingAccessoryDetail\Services\ReceivingAccessoryDetailRecordService;
use App\Modules\ReceivingAccessoryDetail\Services\ReceivingAccessoryDetailRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ReceivingAccessoryDetailServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/ReceivingAccessoryDetail/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(ReceivingAccessoryDetailRecordService::class, ReceivingAccessoryDetailRecordServiceImpl::class);
        $this->app->bind(ReceivingAccessoryDetails::class, EloquentReceivingAccessoryDetails::class);
        $this->app->bind(ReceivingAccessoryDetail::class, ReceivingAccessoryDetailModel::class);
    }
}
