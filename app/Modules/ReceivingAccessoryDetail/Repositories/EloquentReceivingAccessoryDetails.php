<?php

namespace App\Modules\ReceivingAccessoryDetail\Repositories;

use App\Modules\ReceivingAccessoryDetail\Models\ReceivingAccessoryDetail;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentReceivingAccessoryDetails extends EloquentRepository implements ReceivingAccessoryDetails
{
    protected $model;

    public function __construct(ReceivingAccessoryDetail $receivingAccessoryDetail)
    {
        parent::__construct($receivingAccessoryDetail);
    }

    public function save(ReceivingAccessoryDetail $receivingAccessoryDetail)
    {
        return $receivingAccessoryDetail->save();
    }

    public function delete(ReceivingAccessoryDetail $receivingAccessoryDetail)
    {
        return $receivingAccessoryDetail->delete();
    }

    public function productType($productType)
    {
        $this->model =  $this->model->productType($productType);

        return $this;
    }
}
