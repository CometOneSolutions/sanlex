<?php

Breadcrumbs::register('size.index', function ($breadcrumbs) {
	$breadcrumbs->parent('master-file.index');
	$breadcrumbs->push('Sizes', route('size_index'));
});
Breadcrumbs::register('size.create', function ($breadcrumbs) {
	$breadcrumbs->parent('size.index');
	$breadcrumbs->push('Create', route('size_create'));
});
Breadcrumbs::register('size.show', function ($breadcrumbs, $size) {
	$breadcrumbs->parent('size.index');
	$breadcrumbs->push($size->getName(), route('size_show', $size->getId()));
});
Breadcrumbs::register('size.edit', function ($breadcrumbs, $size) {
	$breadcrumbs->parent('size.show', $size);
	$breadcrumbs->push('Edit', route('size_edit', $size->getId()));
});
