<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['prefix' => 'sizes'], function () use ($api) {
		$api->get('/', 'App\Modules\Size\Controllers\SizeApiController@index');
		$api->get('{sizeId}', 'App\Modules\Size\Controllers\SizeApiController@show');
		$api->post('/', 'App\Modules\Size\Controllers\SizeApiController@store');
		$api->patch('{sizeId}', 'App\Modules\Size\Controllers\SizeApiController@update');
		$api->delete('{sizeId}', 'App\Modules\Size\Controllers\SizeApiController@destroy');
	});
});
