<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'sizes'], function () {
		Route::get('/', 'SizeController@index')->name('size_index');
		Route::get('/create', 'SizeController@create')->name('size_create');
		Route::get('{sizeId}/edit', 'SizeController@edit')->name('size_edit');
		Route::get('{sizeId}/print', 'SizeController@print')->name('size_print');
		Route::get('{sizeId}', 'SizeController@show')->name('size_show');
	});
});
