@extends('app')
@section('breadcrumbs', Breadcrumbs::render('size.edit', $size))
@section('content')
<section id="size">
    <div class="row">
        <div class="col-12">
            <div class="card" v-cloak>
                <div class="row align-items-center" v-if="!initializationComplete" style="height: 100px">
                    <div class="col-12 text-center h4">
                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Initializing...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header clearfix">
                        Edit Size
                        @if(auth()->user()->can('Delete [MAS] Size'))
                        <div class="float-right">
                            <delete-button :is-busy="form.isBusy" :is-deleting="form.isDeleting" @destroy="destroy"></delete-button>
                        </div>
                        @endif
                    </div>

                    <v-form @validate="update">
                        <div class="card-body">

                            @include('sizes._form')
                        </div>

                        <div class="card-footer">
                            <div class="text-right">
                                <save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>
                            </div>
                        </div>
                    </v-form>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@endsection