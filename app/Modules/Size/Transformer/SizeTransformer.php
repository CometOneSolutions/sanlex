<?php

namespace App\Modules\Size\Transformer;

use App\Modules\Size\Models\Size;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class SizeTransformer extends UpdatableByUserTransformer
{
	public function transform(Size $size)
	{
		return [
			'id' => (int) $size->getId(),
			'text' => $size->getName(),
			'name' => $size->getName(),
			'updatedAt' => $size->updated_at,
			'editUri' => route('size_edit', $size->getId()),
			'showUri' => route('size_show', $size->getId()),
		];
	}
}
