<?php

namespace App\Modules\Size\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateSize extends SizeRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] Size');
	}
}
