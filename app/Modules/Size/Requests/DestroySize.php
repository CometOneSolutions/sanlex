<?php

namespace App\Modules\Size\Requests;

use Dingo\Api\Http\FormRequest;

class DestroySize extends SizeRequest
{
	public function authorize()
	{
		return $this->user()->can('Delete [MAS] Size');
	}

	public function rules()
	{
		return [];
	}

	public function messages()
	{
		return [];
	}
}
