<?php

namespace App\Modules\Size\Requests;

use Dingo\Api\Http\FormRequest;

class StoreSize extends SizeRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [MAS] Size');
	}
}
