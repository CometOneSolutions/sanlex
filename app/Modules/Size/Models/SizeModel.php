<?php

namespace App\Modules\Size\Models;

use App\Modules\Fastener\Models\FastenerModel;
use App\Modules\PaintAdhesive\Models\PaintAdhesiveModel;
use Illuminate\Database\Eloquent\Model;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Models\Users\User;

class SizeModel extends Model implements Size, UpdatableByUser
{
	use HasUpdatedByUser;

	protected $table = 'sizes';

	public function paintAdhesives()
	{
		return $this->hasMany(PaintAdhesiveModel::class, 'size_id');
	}

	public function fasteners()
	{
		return $this->hasMany(FastenerModel::class, 'size_id');
	}

	public function getFasteners()
	{
		return $this->fasteners;
	}

	public function getPaintAdhesives()
	{
		return $this->paintAdhesives;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($value)
	{
		$this->name = $value;
		return $this;
	}
}
