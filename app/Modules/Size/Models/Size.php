<?php

namespace App\Modules\Size\Models;



interface Size
{
	public function getId();

	public function getName();

	public function setName($value);
}
