<?php

namespace App\Modules\Size\Repositories;

use App\Modules\Size\Models\Size;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentSizeRepository extends EloquentRepository implements SizeRepository
{
	public function __construct(Size $size)
	{
		parent::__construct($size);
	}

	public function save(Size $size)
	{
		return $size->save();
	}

	public function delete(Size $size)
	{
		return $size->delete();
	}
}
