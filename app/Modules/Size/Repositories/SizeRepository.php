<?php

namespace App\Modules\Size\Repositories;

use App\Modules\Size\Models\Size;
use CometOneSolutions\Common\Repositories\Repository;

interface SizeRepository extends Repository
{
	public function save(Size $size);
	public function delete(Size $size);
}
