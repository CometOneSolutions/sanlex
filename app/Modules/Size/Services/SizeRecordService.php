<?php

namespace App\Modules\Size\Services;

use App\Modules\Size\Models\Size;
use CometOneSolutions\Common\Services\RecordService;
use CometOneSolutions\Auth\Models\Users\User;

interface SizeRecordService extends RecordService
{
	public function create(
		$name,
		User $user = null
	);

	public function update(
		Size $size,
		$name,
		User $user = null
	);

	public function delete(Size $size);
}
