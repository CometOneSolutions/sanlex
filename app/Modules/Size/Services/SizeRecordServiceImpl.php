<?php

namespace App\Modules\Size\Services;

use App\Modules\Product\Traits\CanUpdateProductName;
use App\Modules\Size\Models\Size;
use App\Modules\Size\Repositories\SizeRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;

use CometOneSolutions\Auth\Models\Users\User;

class SizeRecordServiceImpl extends RecordServiceImpl implements SizeRecordService
{
	use CanUpdateProductName;


	private $size;
	private $sizes;

	protected $availableFilters = [
		["id" => "name", "text" => "Name"]
	];

	public function __construct(SizeRepository $sizes, Size $size)
	{
		$this->sizes = $sizes;
		$this->size = $size;
		parent::__construct($sizes);
	}

	public function create(
		$name,
		User $user = null
	) {
		$size = new $this->size;
		$size->setName($name);

		if ($user) {
			$size->setUpdatedByUser($user);
		}
		$this->sizes->save($size);
		return $size;
	}

	public function update(
		Size $size,
		$name,
		User $user = null
	) {
		$tempSize = clone $size;
		$tempSize->setName($name);
		if ($user) {
			$tempSize->setUpdatedByUser($user);
		}
		$this->sizes->save($tempSize);

		$this->updateProductableProductNames([
			$tempSize->getPaintAdhesives(),
			$tempSize->getFasteners()
		]);

		return $tempSize;
	}

	public function delete(Size $size)
	{
		$this->sizes->delete($size);
		return $size;
	}
}
