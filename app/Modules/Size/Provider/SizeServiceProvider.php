<?php

namespace App\Modules\Size\Provider;

use App\Modules\Size\Models\Size;
use App\Modules\Size\Models\SizeModel;
use App\Modules\Size\Repositories\EloquentSizeRepository;
use App\Modules\Size\Repositories\SizeRepository;
use App\Modules\Size\Services\SizeRecordService;
use App\Modules\Size\Services\SizeRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class SizeServiceProvider extends ServiceProvider
{
	protected $dbPath = 'Modules/Size/Database/';
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
		$this->app->bind(SizeRecordService::class, SizeRecordServiceImpl::class);
		$this->app->bind(SizeRepository::class, EloquentSizeRepository::class);
		$this->app->bind(Size::class, SizeModel::class);
	}
}
