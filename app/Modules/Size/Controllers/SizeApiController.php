<?php

namespace App\Modules\Size\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Size\Services\SizeRecordService;
use App\Modules\Size\Transformer\SizeTransformer;
use App\Modules\Size\Requests\StoreSize;
use App\Modules\Size\Requests\UpdateSize;
use App\Modules\Size\Requests\DestroySize;

class SizeApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;

	public function __construct(
		SizeRecordService $sizeRecordService,
		SizeTransformer $transformer
	) {
		$this->middleware('auth:api');
		parent::__construct($sizeRecordService, $transformer);
		$this->service = $sizeRecordService;
		$this->transformer = $transformer;
	}

	public function store(StoreSize $request)
	{
		$size = \DB::transaction(function () use ($request) {
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->create(
				$name,

				$user
			);
		});
		return $this->response->item($size, $this->transformer)->setStatusCode(201);
	}

	public function update($sizeId, UpdateSize $request)
	{
		$size = \DB::transaction(function () use ($sizeId, $request) {
			$size   = $this->service->getById($sizeId);
			$name       = $request->getName();

			$user       = $request->user();

			return $this->service->update(
				$size,
				$name,

				$user
			);
		});
		return $this->response->item($size, $this->transformer)->setStatusCode(200);
	}

	public function destroy($sizeId, DestroySize $request)
	{
		$size = $this->service->getById($sizeId);
		$this->service->delete($size);
		return $this->response->item($size, $this->transformer)->setStatusCode(200);
	}
}
