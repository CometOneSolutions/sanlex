<?php

namespace App\Modules\Size\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Size\Services\SizeRecordService;


class SizeController extends Controller
{
	private $service;

	public function __construct(SizeRecordService $sizeRecordService)
	{
		$this->service = $sizeRecordService;
		$this->middleware('permission:Read [MAS] Size Type')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Size Type')->only('create');
		$this->middleware('permission:Update [MAS] Size Type')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'name',
			'sortAscending' => true,
			'baseUrl' => '/api/sizes'
		]);
		return view('sizes.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('sizes.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$size = $this->service->getById($id);
		return view('sizes.show', compact('size'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$size = $this->service->getById($id);
		return view('sizes.edit', compact('size'));
	}
}
