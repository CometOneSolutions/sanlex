<?php

namespace App\Modules\Uom\Providers;

use App\Modules\Uom\Models\Uom;
use App\Modules\Uom\Models\UomModel;
use App\Modules\Uom\Repositories\EloquentUomRepository;
use App\Modules\Uom\Repositories\UomRepository;
use App\Modules\Uom\Services\UomRecordService;
use App\Modules\Uom\Services\UomRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class UomServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Uom/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(UomRecordService::class, UomRecordServiceImpl::class);
        $this->app->bind(UomRepository::class, EloquentUomRepository::class);
        $this->app->bind(Uom::class, UomModel::class);
    }
}
