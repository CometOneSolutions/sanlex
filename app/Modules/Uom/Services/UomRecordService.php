<?php

namespace App\Modules\Uom\Services;

use App\Modules\Uom\Models\Uom;
use CometOneSolutions\Common\Services\RecordService;

interface UomRecordService extends RecordService
{
    public function create(string $name);
    public function update(Uom $uom, string $name);
    public function delete(Uom $uom);
}
