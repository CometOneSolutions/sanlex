<?php

namespace App\Modules\Uom\Services;

use App\Modules\Uom\Models\Uom;
use App\Modules\Uom\Repositories\UomRepository;
use CometOneSolutions\Common\Services\RecordServiceImpl;

class UomRecordServiceImpl extends RecordServiceImpl implements UomRecordService
{
    private $uoms;
    private $uom;

    public function __construct(UomRepository $uoms, Uom $uom)
    {
        $this->uoms = $uoms;
        $this->uom = new $uom;
        parent::__construct($uoms);
    }

    public function create(
        string $name
    ) {
        $uom = new $this->uom;
        $this->setFields(
            $uom,
            $name
        );
        $this->uoms->save($uom);
        return $uom;
    }

    private function setFields(
        Uom $uom,
        string $name
    ) {
        $uom->setName($name);
    }

    public function update(
        Uom $uom,
        string $name
    ) {
        $this->setFields(
            $uom,
            $name
        );
        $this->uoms->save($uom);
        return $uom;
    }

    public function delete(Uom $uom)
    {
        // TODO delete checks
        $this->uoms->delete($uom);
        return $uom;
    }
}
