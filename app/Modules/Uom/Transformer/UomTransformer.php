<?php

namespace App\Modules\Uom\Transformer;

use App\Modules\Uom\Models\Uom;
use League\Fractal;

class UomTransformer extends Fractal\TransformerAbstract
{
    public function transform(Uom $uom)
    {
        return [
            'id' => (int)$uom->getId(),
            'name' => $uom->getName(),
            // 'cost' => $uom->getCost(),
            // 'length' => $uom->getLength(),
            // 'width' => $uom->getWidth(),
            // 'editUri' => route('paper-type_edit', $uom->id),
            // 'showUri' => route('paper-type_show', $uom->id),
        ];
    }
}
