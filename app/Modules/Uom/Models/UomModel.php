<?php

namespace App\Modules\Uom\Models;

use Illuminate\Database\Eloquent\Model;

class UomModel extends Model implements Uom
{
    protected $table = 'uoms';
    
    

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public static function new($name)
    {
        $newUom = new static;
        $this->setName($name);
        return $newUom;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

}
