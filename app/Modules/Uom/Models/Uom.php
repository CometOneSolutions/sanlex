<?php

namespace App\Modules\Uom\Models;

interface Uom
{
    const MT = 'MT';
    const PIECE = 'PIECE';
    
    public function getId();

    public function setName($value);

    public function getName();
}
