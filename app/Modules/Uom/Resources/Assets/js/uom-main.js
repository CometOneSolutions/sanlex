import { Form } from "@c1_common_js/components/Form";

export const uomService = new Vue({
    data: function() {
        return {
            form: new Form({}),
            uri: {
                uoms: '/api/uoms?limit=' + Number.MAX_SAFE_INTEGER,
            }
        };
    },
    methods: {
        getUoms() {
            return this.form.get(this.uri.uoms);
        },   
    }
});
