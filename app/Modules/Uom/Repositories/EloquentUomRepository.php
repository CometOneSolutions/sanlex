<?php

namespace App\Modules\Uom\Repositories;

use App\Modules\Uom\Models\Uom;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentUomRepository extends EloquentRepository implements UomRepository
{
    public function __construct(Uom $uom)
    {
        parent::__construct($uom);
    }

    public function save(Uom $uom)
    {
        return $uom->save();
    }

    public function delete(Uom $uom)
    {
        return $uom->delete();
    }
}
