<?php

namespace App\Modules\Uom\Repositories;

use App\Modules\Uom\Models\Uom;
use CometOneSolutions\Common\Repositories\Repository;

interface UomRepository extends Repository
{
    public function save(Uom $uom);
    public function delete(Uom $uom);
}
