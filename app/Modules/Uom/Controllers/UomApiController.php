<?php

namespace App\Modules\Uom\Controllers;

use App\Modules\Uom\Services\UomRecordService;
use Illuminate\Http\Request;
use App\Modules\Uom\Transformer\UomTransformer;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\Uom\Requests\StoreUom;

// use Dingo\Api\Http\Middleware\Request;

class UomApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        UomRecordService $uomRecordService,
        UomTransformer $transformer
    ) {
        $this->service = $uomRecordService;
        $this->transformer = $transformer;
        parent::__construct($uomRecordService, $transformer);
    }

    public function store(StoreUom $request)
    {
        // $name = $request->get('name');
        // $uom = $this->service->create($name);
        // return $this->response->item($uom, $this->transformer)->setStatusCode(201);
    }

    public function update($uomId, Request $request)
    {
        // $uom = $this->service->getById($uomId);
        // $name = $request->get('name');
        // $uom = $this->service->update($uom, $name);
        // return $this->response->item($uom, $this->transformer)->setStatusCode(200);
    }

    public function destroy($id)
    {
        // $uom = $this->service->getById($id);
        // $this->service->delete($uom);
        // return $this->response->item($uom, $this->transformer)->setStatusCode(200);
    }
}
