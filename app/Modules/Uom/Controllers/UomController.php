<?php

namespace App\Modules\Uom\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use Illuminate\Http\Request;
use JavaScript;
// use App\Modules\Uom\Services\UomRecordService;

class UomController extends Controller
{
    // private $uomRecordService;

    // public function __construct(UomRecordService $uomRecordService)
    // {
    //     $this->uomRecordService = $uomRecordService;
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        JavaScript::put([
            'filterable' => $this->getDisplayedFilterable(),
            'sorter' => 'name',
            'sortAscending' => true,
            'baseUrl' => '/api/uoms'
        ]);
        // return view("master-lists.currencies.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // JavaScript::put(['id' => null,]);
        // return view("master-lists.uoms.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // JavaScript::put(['id' => $id]);
        // $uom = $this->uomRecordService->getById($id);
        // $data = [
        //     "id" => $id,
        //     "name" => $uom->getName(),
        // ];
        // return view("master-lists.uoms.show", compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // JavaScript::put(['id' => $id]);
        // $uom = $this->uomRecordService->getById($id);
        // $data = [
        //     "id" => $id,
        //     "name" => $uom->getName(),
        // ];
        // return view("master-lists.uoms.edit", compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getDisplayedFilterable()
    {
        return [
            ["id" => "name", "text" => "Name"]
        ];
    }
}
