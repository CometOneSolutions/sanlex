<?php

namespace App\Modules\Uom\Requests;

use Dingo\Api\Http\FormRequest;

class StoreUom extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }
    public function messages()
    {
        return [
            // 'name.numeric' => 'Name must be numeric.',
            // 'creditLimit.alpha' => 'Credit must be in alpha.'

        ];
    }
}
