<?php

namespace App\Modules\Skylight\Services;

use App\Modules\Length\Models\Length;
use App\Modules\PanelProfile\Models\PanelProfile;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Skylight\Models\Skylight;
use App\Modules\Skylight\Repositories\SkylightRepository;
use App\Modules\SkylightClass\Models\SkylightClass;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class SkylightRecordServiceImpl extends RecordServiceImpl implements SkylightRecordService
{
    private $skylight;
    private $skylights;
    private $productRecordService;

    protected $availableFilters = [
        ["id" => "name", "text" => "Name"],

    ];

    public function __construct(SkylightRepository $skylights, Skylight $skylight, ProductRecordService $productRecordService)
    {
        $this->skylights = $skylights;
        $this->skylight = $skylight;
        $this->productRecordService = $productRecordService;
        parent::__construct($skylights);
    }

    public function create(
        $name,
        Supplier $supplier,
        Width $width,
        Thickness $thickness,
        Length $length,
        PanelProfile $panelProfile,
        SkylightClass $skylightClass,
        User $user = null
    ) {
        $skylight = new $this->skylight;
        $skylight->setWidth($width);
        $skylight->setThickness($thickness);
        $skylight->setLength($length);
        $skylight->setPanelProfile($panelProfile);
        $skylight->setSkylightClass($skylightClass);

        if ($user) {
            $skylight->setUpdatedByUser($user);
        }
        $this->skylights->save($skylight);
        $this->productRecordService->create($name, $supplier, $skylight, $user);
        return $skylight;
    }

    public function update(
        Skylight $skylight,
        $name,
        Supplier $supplier,

        Width $width,
        Thickness $thickness,
        Length $length,
        PanelProfile $panelProfile,
        SkylightClass $skylightClass,
        User $user = null
    ) {
        $tempSkylight = clone $skylight;
        $product = $tempSkylight->getProduct();
        $tempSkylight->setWidth($width);
        $tempSkylight->setThickness($thickness);
        $tempSkylight->setLength($length);
        $tempSkylight->setPanelProfile($panelProfile);
        $tempSkylight->setSkylightClass($skylightClass);

        if ($user) {
            $tempSkylight->setUpdatedByUser($user);
        }
        $this->skylights->save($tempSkylight);
        $this->productRecordService->update($product, $name, $supplier, $tempSkylight, $user);
        return $tempSkylight;
    }

    public function delete(Skylight $skylight)
    {
        $this->skylights->delete($skylight);
        return $skylight;
    }
    public function inStock()
    {
        $this->skylights = $this->skylights->inStock();
        return $this;
    }
}
