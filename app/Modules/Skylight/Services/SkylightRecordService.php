<?php

namespace App\Modules\Skylight\Services;

use App\Modules\Length\Models\Length;
use App\Modules\PanelProfile\Models\PanelProfile;
use App\Modules\Skylight\Models\Skylight;
use App\Modules\SkylightClass\Models\SkylightClass;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface SkylightRecordService extends RecordService
{
    public function create(
        $name,
        Supplier $supplier,
        Width $width,
        Thickness $thickness,
        Length $length,
        PanelProfile $panelProfile,
        SkylightClass $skylightClass,
        User $user = null
    );

    public function update(
        Skylight $skylight,
        $name,
        Supplier $supplier,
        Width $width,
        Thickness $thickness,
        Length $length,
        PanelProfile $panelProfile,
        SkylightClass $skylightClass,
        User $user = null
    );

    public function delete(Skylight $skylight);
}
