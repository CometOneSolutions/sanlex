<?php

namespace App\Modules\Skylight\Provider;

use App\Modules\Skylight\Models\Skylight;
use App\Modules\Skylight\Models\SkylightModel;
use App\Modules\Skylight\Repositories\EloquentSkylightRepository;
use App\Modules\Skylight\Repositories\SkylightRepository;
use App\Modules\Skylight\Services\SkylightRecordService;
use App\Modules\Skylight\Services\SkylightRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class SkylightServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/Skylight/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(SkylightRecordService::class, SkylightRecordServiceImpl::class);
        $this->app->bind(SkylightRepository::class, EloquentSkylightRepository::class);
        $this->app->bind(Skylight::class, SkylightModel::class);
    }
}
