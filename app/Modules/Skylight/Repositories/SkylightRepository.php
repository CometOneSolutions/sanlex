<?php

namespace App\Modules\Skylight\Repositories;

use App\Modules\Skylight\Models\Skylight;
use CometOneSolutions\Common\Repositories\Repository;

interface SkylightRepository extends Repository
{
    public function save(Skylight $skylight);
    public function delete(Skylight $skylight);
}
