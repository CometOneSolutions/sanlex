<?php

namespace App\Modules\Skylight\Repositories;

use App\Modules\Skylight\Models\Skylight;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentSkylightRepository extends EloquentRepository implements SkylightRepository
{
    public function __construct(Skylight $skylight)
    {
        parent::__construct($skylight);
    }

    public function save(Skylight $skylight)
    {
        return $skylight->save();
    }

    public function delete(Skylight $skylight)
    {
        $product = $skylight->getProduct();
        $product->delete();
        return $skylight->delete();
    }
    public function filterByName($productName)
    {
        return $this->model->whereHas('product', function ($product) use ($productName) {
            return $product->where('name', 'like', '%' . $productName . '%');
        });
    }
    public function filterBySupplierName($supplierName)
    {
        return $this->model->whereHas('product', function ($product) use ($supplierName) {
            return $product->whereHas('supplier', function ($supplier) use ($supplierName) {
                return $supplier->where('name', 'like', '%' . $supplierName . '%');
            });
        });
    }
    public function inStock()
    {
        $this->model = $this->model->inStock();
        return $this;
    }
}
