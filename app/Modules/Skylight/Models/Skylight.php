<?php

namespace App\Modules\Skylight\Models;



interface Skylight
{
    public function getId();

    public function getDescription();

    public function setDescription($value);
}
