<?php

namespace App\Modules\Skylight\Models;

use App\Modules\Length\Models\Length;
use App\Modules\Length\Models\LengthModel;
use App\Modules\PanelProfile\Models\PanelProfile;
use App\Modules\PanelProfile\Models\PanelProfileModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Product\Traits\Productable;
use App\Modules\SkylightClass\Models\SkylightClass;
use App\Modules\SkylightClass\Models\SkylightClassModel;
use App\Modules\Thickness\Models\Thickness;
use App\Modules\Thickness\Models\ThicknessModel;
use App\Modules\Width\Models\Width;
use App\Modules\Width\Models\WidthModel;
use CometOneSolutions\Auth\Models\UpdatableByUser;
use CometOneSolutions\Auth\Traits\HasUpdatedByUser;
use Illuminate\Database\Eloquent\Model;


class SkylightModel extends Model implements Skylight, UpdatableByUser, Productable
{
    use HasUpdatedByUser;

    protected $table = 'skylights';

    public function product()
    {
        return $this->morphOne(ProductModel::class, 'productable');
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

    public function skylightClass()
    {
        return $this->belongsTo(SkylightClassModel::class, 'skylight_class_id');
    }


    public function width()
    {
        return $this->belongsTo(WidthModel::class, 'width_id');
    }

    public function length()
    {
        return $this->belongsTo(LengthModel::class, 'length_id');
    }

    public function thickness()
    {
        return $this->belongsTo(ThicknessModel::class, 'thickness_id');
    }

    public function panelProfile()
    {
        return $this->belongsTo(PanelProfileModel::class, 'panel_profile_id');
    }

    public function getThicknessId()
    {
        return (int) $this->thickness_id;
    }
    public function getThickness()
    {
        return $this->thickness;
    }

    public function setThickness(Thickness $thickness)
    {
        $this->thickness()->associate($thickness);
        $this->thickness_id = $thickness->getId();
        return $this;
    }
    public function getWidthId()
    {
        return (int) $this->width_id;
    }
    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth(Width $width)
    {
        $this->width()->associate($width);
        $this->width_id = $width->getId();
        return $this;
    }
    public function getLengthId()
    {
        return (int) $this->length_id;
    }
    public function getLength()
    {
        return $this->length;
    }

    public function setLength(Length $length)
    {
        $this->length()->associate($length);
        $this->length_id = $length->getId();
        return $this;
    }

    public function getPanelProfile()
    {
        return $this->panelProfile;
    }

    public function getPanelProfileId()
    {
        return (int) $this->panel_profile_id;
    }

    public function setPanelProfile(PanelProfile $panelProfile)
    {
        $this->panelProfile()->associate($panelProfile);
        return $this;
    }

    public function getSkylightClass()
    {
        return $this->skylightClass;
    }

    public function setSkylightClass(SkylightClass $skylightClass)
    {
        $this->skylightClass()->associate($skylightClass);
        return $this;
    }

    public function getSkylightClassId()
    {
        return $this->skylight_class_id;
    }
    public function getName()
    {
        return "$this->getPanelProfile()->getName() $this->getSkylightClass()->getName()";
    }


    public function isCoil()
    {
        return false;
    }

    public function scopeInStock($query)
    {
        return $query->whereHas('product', function ($product) {
            return $product->whereHas('inventory', function ($inventory) {
                return $inventory->where('stock', '>', 0);
            });
        });
    }

    public function getAssembledProductName()
    {
        $nameAttributes = [
            $this->getProduct()->getSupplier()->getShortCode(),
            'SKYLIGHT',
            $this->getPanelProfile()->getName(),
            $this->getThickness()->getName(),
            $this->getWidth()->getName(),
            $this->getLength()->getName(),
            $this->getSkylightClass()->getName()
        ];

        return  strtoupper(implode("~", $nameAttributes));
    }

    public function scopePanelProfile($query, PanelProfile $panelProfile)
    {
        return $query->wherePanelProfileId($panelProfile->getId());
    }
    public function scopeSkylightClass($query, SkylightClass $skylightClass)
    {
        return $query->whereSkylightClassId($skylightClass->getId());
    }

    public function scopeWidth($query, Width $width)
    {
        return $query->whereWidthId($width->getId());
    }

    public function scopeLength($query, Length $length)
    {
        return $query->whereLengthId($length->getId());
    }

    public function scopeThickness($query, Thickness $thickness)
    {
        return $query->whereThicknessId($thickness->getId());
    }
}
