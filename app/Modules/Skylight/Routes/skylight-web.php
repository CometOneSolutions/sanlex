<?php
Route::group(['prefix' => 'master-file'], function () {


    Route::group(['prefix' => 'skylights'], function () {
        Route::get('/', 'SkylightController@index')->name('skylight_index');
        Route::get('/create', 'SkylightController@create')->name('skylight_create');
        Route::get('{skylightId}/edit', 'SkylightController@edit')->name('skylight_edit');
        Route::get('{skylightId}/print', 'SkylightController@print')->name('skylight_print');
        Route::get('{skylightId}', 'SkylightController@show')->name('skylight_show');
    });
});
