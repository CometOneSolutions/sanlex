<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'skylights'], function () use ($api) {
        $api->get('/', 'App\Modules\Skylight\Controllers\SkylightApiController@index');
        $api->get('{skylightId}', 'App\Modules\Skylight\Controllers\SkylightApiController@show');
        $api->post('/', 'App\Modules\Skylight\Controllers\SkylightApiController@store');
        $api->patch('{skylightId}', 'App\Modules\Skylight\Controllers\SkylightApiController@update');
        $api->delete('{skylightId}', 'App\Modules\Skylight\Controllers\SkylightApiController@destroy');
    });
});
