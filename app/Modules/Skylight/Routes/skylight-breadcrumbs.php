<?php

Breadcrumbs::register('skylight.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Skylights', route('skylight_index'));
});
Breadcrumbs::register('skylight.create', function ($breadcrumbs) {
    $breadcrumbs->parent('skylight.index');
    $breadcrumbs->push('Create', route('skylight_create'));
});
Breadcrumbs::register('skylight.show', function ($breadcrumbs, $skylight) {
    $breadcrumbs->parent('skylight.index');
    $breadcrumbs->push($skylight->getProduct()->getName(), route('skylight_show', $skylight->getId()));
});
Breadcrumbs::register('skylight.edit', function ($breadcrumbs, $skylight) {
    $breadcrumbs->parent('skylight.show', $skylight);
    $breadcrumbs->push('Edit', route('skylight_edit', $skylight->getId()));
});
