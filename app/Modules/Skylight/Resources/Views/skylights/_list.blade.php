<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Name</label>
            <p>@{{form.product.data.name }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Supplier</label>
            <p>@{{form.product.data.supplier.data.name }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Description</label>
            <p>@{{form.description || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Width</label>
            <p>@{{form.width.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Thickness</label>
            <p>@{{form.thickness.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Length</label>
            <p>@{{form.length.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Panel Profile</label>
            <p>@{{form.panelProfile.data.name || "--" }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="name" class="small">Skylight Class</label>
            <p>@{{form.skylightClass.data.name || "--" }}</p>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{ mix('js/skylight.js') }}"></script>
@endpush