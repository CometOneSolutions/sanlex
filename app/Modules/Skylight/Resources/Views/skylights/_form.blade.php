<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Supplier <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter supplier" v-model="form.supplierId" :selected="defaultSelectedSupplier" :url="supplierUrl" search="name" placeholder="Please select" @change="setShortCode" id="supplier" name="supplier">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Width <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter width" v-model="form.widthId" :selected="defaultSelectedWidth" :url="widthUrl" search="name" placeholder="Please select" @change="setWidth" id="width" name="width">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Length <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter length" v-model="form.lengthId" :selected="defaultSelectedLength" :url="lengthUrl" search="name" placeholder="Please select" @change="setLength" id="length" name="length">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Thickness <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter thickness" v-model="form.thicknessId" :selected="defaultSelectedThickness" :url="thicknessUrl" search="name" placeholder="Please select" @change="setThickness" id="thickness" name="thickness">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Panel Profile <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter panel profile" v-model="form.panelProfileId" :selected="defaultSelectedPanelProfile" :url="panelProfileUrl" search="name" placeholder="Please select" @change="setPanelProfile" id="panelProfile" name="panelProfile">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Skylight Class <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter skylight class" v-model="form.skylightClassId" :selected="defaultSelectedSkylightClass" :url="skylightClassUrl" search="name" placeholder="Please select" id="skylightClass" @change="setSkylightClass" name="skylightClass">
            </select3>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{ mix('js/skylight.js') }}"></script>
@endpush