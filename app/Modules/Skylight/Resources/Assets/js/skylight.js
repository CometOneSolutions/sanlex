import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select3 from "@c1_common_js/components/Select3";


new Vue({
    el: '#skylight',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select3
    },
    data: {
        form: new Form({
            name: null,
            supplierId: null,
            shortCode: null,
            widthId: null,
            widthText: null,
            lengthId: null,
            lengthText: null,
            thicknessId: null,
            thicknessText: null,
            panelProfileId: null,
            panelProfileText: null,
            skylightClassId: null,
            skylightClassText: null,
        }),
        dataInitialized: true,
        supplierUrl: '/api/suppliers?sort=name',
        defaultSelectedSupplier: {},
        widthUrl: '/api/widths?sort=name',
        defaultSelectedWidth: {},
        lengthUrl: '/api/lengths?sort=name',
        defaultSelectedLength: {},
        thicknessUrl: '/api/thicknesses?sort=name',
        defaultSelectedThickness: {},
        panelProfileUrl: '/api/panel-profiles?sort=name',
        defaultSelectedPanelProfile: {},
        skylightClassUrl: '/api/skylight-classes?sort=name',
        defaultSelectedSkylightClass: {},

    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },
    computed: {
        // selectedSupplier() {
        //     if(this.form.supplierId == null)
        //     {
        //         return undefined;
        //     }
        //     return this.supplierSelections.find(supplier => supplier.id == this.form.supplierId);
        // },
        name() {
            if (this.form.supplierId === undefined ||
                this.form.widthId === null ||
                this.form.lengthId === null ||
                this.form.thicknessId === null ||
                this.form.panelProfileId === null ||
                this.form.skylightClassId === null
            ) {
                return undefined;
            }
            return this.form.shortCode + '~' + 'SKYLIGHT' + '~' + this.form.panelProfileText.toUpperCase() + '~'
                + this.form.thicknessText.toUpperCase() + '~' + this.form.widthText.toUpperCase() + '~' + this.form.lengthText.toUpperCase() + '~' + this.form.skylightClassText.toUpperCase();
        },
        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        setShortCode(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("shortCode")) {
                this.form.shortCode = selectedObjects[0].shortCode;
            }
        },
        setLength(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.lengthText = selectedObjects[0].name;
            }

        },
        setThickness(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.thicknessText = selectedObjects[0].name;
            }

        },
        setWidth(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.widthText = selectedObjects[0].name;
            }

        },
        setPanelProfile(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.panelProfileText = selectedObjects[0].name;
            }
        },
        setSkylightClass(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.skylightClassText = selectedObjects[0].name;
            }
        },
        destroy() {
            this.form.deleteWithConfirmation('/api/skylights/' + this.form.id).then(response => {
                this.form.successModal('Skylight was removed.').then(() =>
                    window.location = '/master-file/skylights/'
                );
            });
        },

        store() {
            this.form.name = this.name;
            this.form.postWithModal('/api/skylights', null, 'Skylight was saved.');
        },

        update() {
            this.form.name = this.name;
            this.form.patch('/api/skylights/' + this.form.id).then(response => {
                this.form.successModal('Skylight was updated.').then(() =>
                    window.location = '/master-file/skylights/' + this.form.id
                );
            })
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {


        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/skylights/' + id + '?include=product.supplier,width,thickness,length,panelProfile,skylightClass')
                .then(response => {
                    this.loadData(response.data);

                    this.form.shortCode = response.data.product.data.supplier.data.shortCode;
                    this.form.lengthText = response.data.length.data.name;
                    this.form.widthText = response.data.width.data.name;
                    this.form.thicknessText = response.data.thickness.data.name;
                    this.form.panelProfileText = response.data.panelProfile.data.name;
                    this.form.skylightClassText = response.data.skylightClass.data.name;

                    this.defaultSelectedSupplier = {
                        text: response.data.product.data.supplier.data.name,
                        id: response.data.product.data.supplier.data.id,
                    };
                    this.defaultSelectedWidth = {
                        text: response.data.width.data.name,
                        id: response.data.width.data.id,
                    };
                    this.defaultSelectedThickness = {
                        text: response.data.thickness.data.name,
                        id: response.data.thickness.data.id,
                    };
                    this.defaultSelectedLength = {
                        text: response.data.length.data.name,
                        id: response.data.length.data.id,
                    };
                    this.defaultSelectedPanelProfile = {
                        text: response.data.panelProfile.data.name,
                        id: response.data.panelProfile.data.id,
                    };
                    this.defaultSelectedSkylightClass = {
                        text: response.data.skylightClass.data.name,
                        id: response.data.skylightClass.data.id,
                    };
                    this.dataInitialized = true;
                });
        }
    },
    mounted() {
        console.log("Init skylight script...");
    }
});