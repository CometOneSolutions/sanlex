<?php

namespace App\Modules\Skylight\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateSkylight extends SkylightRequest
{
    public function authorize()
    {
        return $this->user()->can('Update [MAS] Skylight');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }

}