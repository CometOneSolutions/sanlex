<?php

namespace App\Modules\Skylight\Requests;

use Dingo\Api\Http\FormRequest;

class StoreSkylight extends SkylightRequest
{
    public function authorize()
    {
        return $this->user()->can('Create [MAS] Skylight');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }

}
