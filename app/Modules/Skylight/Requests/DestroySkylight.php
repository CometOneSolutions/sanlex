<?php

namespace App\Modules\Skylight\Requests;

use Dingo\Api\Http\FormRequest;

class DestroySkylight extends SkylightRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] Skylight');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}