<?php

namespace App\Modules\Skylight\Requests;

use App\Modules\Length\Services\LengthRecordService;
use App\Modules\PanelProfile\Services\PanelProfileRecordService;
use App\Modules\Skylight\Services\SkylightRecordService;
use App\Modules\SkylightClass\Services\SkylightClassRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\Thickness\Services\ThicknessRecordService;
use App\Modules\Width\Services\WidthRecordService;
use Dingo\Api\Http\FormRequest;

class SkylightRequest extends FormRequest
{
    private $service;
    private $supplierRecordService;
    private $widthRecordService;
    private $lengthRecordService;
    private $thicknessRecordService;
    private $panelProfileRecordService;
    private $skylightClassRecordService;

    public function __construct(
        SupplierRecordService $supplierRecordService,
        SkylightRecordService $skylightRecordService,
        WidthRecordService $widthRecordService,
        LengthRecordService $lengthRecordService,
        ThicknessRecordService $thicknessRecordService,
        PanelProfileRecordService $panelProfileRecordService,
        SkylightClassRecordService $skylightClassRecordService
    ) {
        $this->supplierRecordService = $supplierRecordService;
        $this->service = $skylightRecordService;
        $this->widthRecordService = $widthRecordService;
        $this->lengthRecordService = $lengthRecordService;
        $this->thicknessRecordService = $thicknessRecordService;
        $this->panelProfileRecordService = $panelProfileRecordService;
        $this->skylightClassRecordService = $skylightClassRecordService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $skylight = $this->route('skylightId') ? $this->service->getById($this->route('skylightId')) : null;

        if ($skylight) {
            return [
                'name'          => 'required|unique:products,name,' . $skylight->getProduct()->getId(),
                'widthId'       => 'required',
                'lengthId'      => 'required',
                'thicknessId'   => 'required',
                'panelProfileId' => 'required',
                'skylightClassId' => 'required'
            ];
        } else {
            return [
                'name'                  => 'required|unique:products,name',
                'widthId'               => 'required',
                'lengthId'              => 'required',
                'thicknessId'           => 'required',
                'panelProfileId'        => 'required',
                'skylightClassId'       => 'required'
            ];
        }
    }
    public function messages()
    {
        return [
            'name.required'                  => 'Name is required.',
            'name.unique'                    => 'Name already exists.',
            'widthId.required'               => 'Width is required.',
            'lengthId.required'              => 'Length is required.',
            'thicknessId.required'           => 'Thickness is required.',
            'panelProfileId.required'        => 'Panel profile is required.',
            'skylightClassId.required'       => 'Skylight class is required.'
        ];
    }


    public function getName($index = 'name')
    {
        return $this->input($index);
    }

    public function getSupplier($index = 'supplierId')
    {
        return $this->supplierRecordService->getById($this->input($index));
    }

    public function getSkylight($index = 'id')
    {
        return $this->service->getById($this->input($index));
    }

    public function getWidth($index = 'widthId')
    {
        return $this->widthRecordService->getById($this->input($index));
    }
    public function getLength($index = 'lengthId')
    {
        return $this->lengthRecordService->getById($this->input($index));
    }
    public function getThickness($index = 'thicknessId')
    {

        return $this->thicknessRecordService->getById($this->input($index));
    }
    public function getPanelProfile($index = 'panelProfileId')
    {
        return $this->panelProfileRecordService->getById($this->input($index));
    }
    public function getSkylightClass($index = 'skylightClassId')
    {
        return $this->skylightClassRecordService->getById($this->input($index));
    }
}
