<?php

namespace App\Modules\Skylight\Controllers;

use CometOneSolutions\Common\Controllers\Controller;
use JavaScript;
use App\Modules\Skylight\Services\SkylightRecordService;


class SkylightController extends Controller
{
    private $service;

    public function __construct(SkylightRecordService $skylightRecordService)
    {
        $this->service = $skylightRecordService;
        $this->middleware('permission:Read [MAS] Skylight')->only(['show', 'index']);
        $this->middleware('permission:Create [MAS] Skylight')->only('create');
        $this->middleware('permission:Update [MAS] Skylight')->only('edit');
    }

    public function index()
    {
        JavaScript::put([
            'filterable' => $this->service->getAvailableFilters(),
            'sorter' => 'id',
            'sortAscending' => true,
            'baseUrl' => '/api/skylights?include=product,width,thickness,length,panelProfile,skylightClass'
        ]);
        return view('skylights.index');
    }

    public function create()
    {
        JavaScript::put(['id' => null,]);
        return view('skylights.create');
    }

    public function show($id)
    {
        JavaScript::put(['id' => $id]);
        $skylight = $this->service->getById($id);
        return view('skylights.show', compact('skylight'));
    }

    public function edit($id)
    {
        JavaScript::put(['id' => $id]);
        $skylight = $this->service->getById($id);
        return view('skylights.edit', compact('skylight'));
    }
}
