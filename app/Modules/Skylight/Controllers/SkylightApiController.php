<?php

namespace App\Modules\Skylight\Controllers;

use CometOneSolutions\Common\Controllers\ResourceApiController;
use Illuminate\Http\Request;

use App\Modules\Skylight\Services\SkylightRecordService;
use App\Modules\Skylight\Transformer\SkylightTransformer;
use App\Modules\Skylight\Requests\StoreSkylight;
use App\Modules\Skylight\Requests\UpdateSkylight;
use App\Modules\Skylight\Requests\DestroySkylight;
use App\Modules\Product\Services\ProductRecordService;


class SkylightApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;
    protected $productRecordService;

    public function __construct(
        SkylightRecordService $skylightRecordService,
        SkylightTransformer $transformer,
        ProductRecordService $productRecordService
    ) {
        $this->middleware('auth:api');
        parent::__construct($skylightRecordService, $transformer);
        $this->service = $skylightRecordService;
        $this->transformer = $transformer;
        $this->productRecordService = $productRecordService;
    }

    public function store(StoreSkylight $request)
    {

        $skylight = \DB::transaction(function () use ($request) {
            $name = $request->getName();

            return $this->service->create(
                $name,
                $request->getSupplier(),
                $request->getWidth(),
                $request->getThickness(),
                $request->getLength(),
                $request->getPanelProfile(),
                $request->getSkylightClass(),
                $request->user()
            );
        });
        return $this->response->item($skylight, $this->transformer)->setStatusCode(201);
    }

    public function update($skylightId, UpdateSkylight $request)
    {
        $skylight = \DB::transaction(function () use ($request) {
            $name = $request->getName();

            return $this->service->update(
                $request->getSkylight(),
                $name,
                $request->getSupplier(),
                $request->getWidth(),
                $request->getThickness(),
                $request->getLength(),
                $request->getPanelProfile(),
                $request->getSkylightClass(),
                $request->user()
            );
        });
        return $this->response->item($skylight, $this->transformer)->setStatusCode(200);
    }

    public function destroy($skylightId, DestroySkylight $request)
    {
        $skylight = $this->service->getById($skylightId);
        $this->service->delete($skylight);
        return $this->response->item($skylight, $this->transformer)->setStatusCode(200);
    }
}
