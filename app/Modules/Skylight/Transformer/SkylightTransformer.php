<?php

namespace App\Modules\Skylight\Transformer;

use App\Modules\Length\Transformer\LengthTransformer;
use App\Modules\PanelProfile\Transformer\PanelProfileTransformer;
use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\Skylight\Models\Skylight;
use App\Modules\SkylightClass\Transformer\SkylightClassTransformer;
use App\Modules\Thickness\Transformer\ThicknessTransformer;
use App\Modules\Width\Transformer\WidthTransformer;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class SkylightTransformer extends UpdatableByUserTransformer
{
    protected $availableIncludes = [
        'product', 'width', 'thickness', 'length', 'panelProfile', 'skylightClass'
    ];
    public function transform(Skylight $skylight)
    {
        return [
            'id' => (int) $skylight->getId(),
            'name' => $skylight->getProduct() ? $skylight->getProduct()->getName() : null,
            'supplierId' => $skylight->getProduct() ? $skylight->getProduct()->getSupplier()->getId() : null,
            'widthId' => $skylight->getWidthId(),
            'wdithText' => null,
            'thicknessId' => $skylight->getThicknessId(),
            'thicknessText' => null,
            'lengthId'    => $skylight->getLengthId(),
            'lengthText' => null,
            'panelProfileId' => $skylight->getPanelProfileId(),
            'panelProfileText' => null,
            'skylightClassId' => $skylight->getSkylightClassId(),
            'skylightClassText' => null,
            'description' => $skylight->getDescription(),
            'updatedAt' => $skylight->updated_at,
            'editUri' => route('skylight_edit', $skylight->getId()),
            'showUri' => route('skylight_show', $skylight->getId()),
        ];
    }

    public function includeProduct(Skylight $skylight)
    {
        $product = $skylight->getProduct();
        return $this->item($product, new ProductTransformer);
    }
    public function includeWidth(Skylight $skylight)
    {
        $width = $skylight->getWidth();
        return $this->item($width, new WidthTransformer);
    }
    public function includeThickness(Skylight $skylight)
    {
        $thickness = $skylight->getThickness();
        return $this->item($thickness, new ThicknessTransformer);
    }
    public function includeLength(Skylight $skylight)
    {
        $length = $skylight->getLength();
        return $this->item($length, new LengthTransformer);
    }
    public function includePanelProfile(Skylight $skylight)
    {
        $panelProfile = $skylight->getPanelProfile();
        return $this->item($panelProfile, new PanelProfileTransformer);
    }
    public function includeSkylightClass(Skylight $skylight)
    {
        $skylightClass = $skylight->getSkylightClass();
        return $this->item($skylightClass, new SkylightClassTransformer);
    }
}
