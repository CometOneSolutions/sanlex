<?php

namespace App\Modules\MiscAccessory\Controllers;

use JavaScript;
use CometOneSolutions\Common\Controllers\Controller;
use App\Modules\MiscAccessory\Services\MiscAccessoryRecordService;

class MiscAccessoryController extends Controller
{
	private $service;

	public function __construct(MiscAccessoryRecordService $miscAccessoryRecordService)
	{
		$this->service = $miscAccessoryRecordService;
		$this->middleware('permission:Read [MAS] Misc Accessory')->only(['show', 'index']);
		$this->middleware('permission:Create [MAS] Misc Accessory')->only('create');
		$this->middleware('permission:Update [MAS] Misc Accessory')->only('edit');
	}

	public function index()
	{
		JavaScript::put([
			'filterable' => $this->service->getAvailableFilters(),
			'sorter' => 'id',
			'sortAscending' => true,
			'baseUrl' => '/api/misc-accessories?include=product,length,width,backingSide,miscAccessoryType'
		]);
		return view('misc-accessories.index');
	}

	public function create()
	{
		JavaScript::put(['id' => null,]);
		return view('misc-accessories.create');
	}

	public function show($id)
	{
		JavaScript::put(['id' => $id]);
		$miscAccessory = $this->service->getById($id);
		return view('misc-accessories.show', compact('miscAccessory'));
	}

	public function edit($id)
	{
		JavaScript::put(['id' => $id]);
		$miscAccessory = $this->service->getById($id);
		return view('misc-accessories.edit', compact('miscAccessory'));
	}
}
