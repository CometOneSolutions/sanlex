<?php

namespace App\Modules\MiscAccessory\Controllers;

use Illuminate\Http\Request;
use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\MiscAccessory\Services\MiscAccessoryRecordService;
use App\Modules\MiscAccessory\Transformer\MiscAccessoryTransformer;
use App\Modules\MiscAccessory\Requests\StoreMiscAccessory;
use App\Modules\MiscAccessory\Requests\UpdateMiscAccessory;
use App\Modules\MiscAccessory\Requests\DestroyMiscAccessory;
use App\Modules\Product\Services\ProductRecordService;


class MiscAccessoryApiController extends ResourceApiController
{
	protected $service;
	protected $transformer;
	protected $productRecordService;

	public function __construct(
		MiscAccessoryRecordService $miscAccessoryRecordService,
		MiscAccessoryTransformer $transformer,
		ProductRecordService $productRecordService
	) {
		$this->middleware('auth:api');
		parent::__construct($miscAccessoryRecordService, $transformer);
		$this->service = $miscAccessoryRecordService;
		$this->transformer = $transformer;
		$this->productRecordService = $productRecordService;
	}

	public function store(StoreMiscAccessory $request)
	{

		$miscAccessory = \DB::transaction(function () use ($request) {
			$name = $request->getName();

			return $this->service->create(
				$name,
				$request->getSupplier(),
				$request->getWidth(),
				$request->getLength(),
				$request->getBackingSide(),
				$request->getMiscAccessoryType(),
				$request->user()
			);
		});
		return $this->response->item($miscAccessory, $this->transformer)->setStatusCode(201);
	}

	public function update($miscAccessoryId, UpdateMiscAccessory $request)
	{
		$miscAccessory = \DB::transaction(function () use ($request) {
			$name = $request->getName();

			return $this->service->update(
				$request->getMiscAccessory(),
				$name,
				$request->getSupplier(),
				$request->getWidth(),
				$request->getLength(),
				$request->getBackingSide(),
				$request->getMiscAccessoryType(),
				$request->user()
			);
		});
		return $this->response->item($miscAccessory, $this->transformer)->setStatusCode(200);
	}

	public function destroy($miscAccessoryId, DestroyMiscAccessory $request)
	{
		$miscAccessory = $this->service->getById($miscAccessoryId);
		$this->service->delete($miscAccessory);
		return $this->response->item($miscAccessory, $this->transformer)->setStatusCode(200);
	}
}
