<?php

namespace App\Modules\MiscAccessory\Provider;

use App\Modules\MiscAccessory\Models\MiscAccessory;
use App\Modules\MiscAccessory\Models\MiscAccessoryModel;
use App\Modules\MiscAccessory\Repositories\EloquentMiscAccessoryRepository;
use App\Modules\MiscAccessory\Repositories\MiscAccessoryRepository;
use App\Modules\MiscAccessory\Services\MiscAccessoryRecordService;
use App\Modules\MiscAccessory\Services\MiscAccessoryRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class MiscAccessoryServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/MiscAccessory/Database/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(MiscAccessoryRecordService::class, MiscAccessoryRecordServiceImpl::class);
        $this->app->bind(MiscAccessoryRepository::class, EloquentMiscAccessoryRepository::class);
        $this->app->bind(MiscAccessory::class, MiscAccessoryModel::class);
    }
}
