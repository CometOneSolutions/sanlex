<?php

namespace App\Modules\MiscAccessory\Transformer;

use App\Modules\BackingSide\Transformer\BackingSideTransformer;
use App\Modules\Length\Transformer\LengthTransformer;
use App\Modules\MiscAccessory\Models\MiscAccessory;
use App\Modules\MiscAccessoryType\Transformer\MiscAccessoryTypeTransformer;
use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\Width\Transformer\WidthTransformer;
use League\Fractal;
use CometOneSolutions\Auth\Tranformers\UpdatableByUserTransformer;

class MiscAccessoryTransformer extends UpdatableByUserTransformer
{
	protected $availableIncludes = [
		'product', 'length', 'miscAccessoryType', 'width', 'backingSide'
	];
	public function transform(MiscAccessory $miscAccessory)
	{
		return [
			'id' => (int) $miscAccessory->getId(),
			'name' => $miscAccessory->getProduct() ? $miscAccessory->getProduct()->getName() : null,
			'supplierId' => $miscAccessory->getProduct() ? $miscAccessory->getProduct()->getSupplier()->getId() : null,
			'lengthId'    => $miscAccessory->getLengthId(),
			'lengthText'    => null,
			'widthId'    => $miscAccessory->getWidthId(),
			'widthText'    => null,
			'backingSideId'    => $miscAccessory->getBackingSideId(),
			'backingSideText'    => null,
			'miscAccessoryTypeId' => $miscAccessory->getMiscAccessoryTypeId(),
			'miscAccessoryTypeText' => null,
			'updatedAt' => $miscAccessory->updated_at,
			'editUri' => route('misc-accessory_edit', $miscAccessory->getId()),
			'showUri' => route('misc-accessory_show', $miscAccessory->getId()),
		];
	}

	public function includeProduct(MiscAccessory $miscAccessory)
	{
		$product = $miscAccessory->getProduct();
		return $this->item($product, new ProductTransformer);
	}

	public function includeLength(MiscAccessory $miscAccessory)
	{
		$length = $miscAccessory->getLength();
		return $this->item($length, new LengthTransformer);
	}
	public function includeWidth(MiscAccessory $miscAccessory)
	{
		$width = $miscAccessory->getWidth();
		return $this->item($width, new WidthTransformer);
	}
	public function includeBackingSide(MiscAccessory $miscAccessory)
	{
		$backingSide = $miscAccessory->getBackingSide();
		return $this->item($backingSide, new BackingSideTransformer);
	}
	public function includeMiscAccessoryType(MiscAccessory $miscAccessory)
	{
		$miscAccessoryType = $miscAccessory->getMiscAccessoryType();
		return $this->item($miscAccessoryType, new MiscAccessoryTypeTransformer);
	}
}
