<?php

Breadcrumbs::register('misc-accessory.index', function ($breadcrumbs) {
    $breadcrumbs->parent('master-file.index');
    $breadcrumbs->push('Misc Accessories', route('misc-accessory_index'));
});
Breadcrumbs::register('misc-accessory.create', function ($breadcrumbs) {
    $breadcrumbs->parent('misc-accessory.index');
    $breadcrumbs->push('Create', route('misc-accessory_create'));
});
Breadcrumbs::register('misc-accessory.show', function ($breadcrumbs, $miscAccessory) {
    $breadcrumbs->parent('misc-accessory.index');
    $breadcrumbs->push($miscAccessory->getProduct()->getName(), route('misc-accessory_show', $miscAccessory->getId()));
});
Breadcrumbs::register('misc-accessory.edit', function ($breadcrumbs, $miscAccessory) {
    $breadcrumbs->parent('misc-accessory.show', $miscAccessory);
    $breadcrumbs->push('Edit', route('misc-accessory_edit', $miscAccessory->getId()));
});
