<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'misc-accessories'], function () use ($api) {
        $api->get('/', 'App\Modules\MiscAccessory\Controllers\MiscAccessoryApiController@index');
        $api->get('{miscAccessoryId}', 'App\Modules\MiscAccessory\Controllers\MiscAccessoryApiController@show');
        $api->post('/', 'App\Modules\MiscAccessory\Controllers\MiscAccessoryApiController@store');
        $api->patch('{miscAccessoryId}', 'App\Modules\MiscAccessory\Controllers\MiscAccessoryApiController@update');
        $api->delete('{miscAccessoryId}', 'App\Modules\MiscAccessory\Controllers\MiscAccessoryApiController@destroy');
    });
});
