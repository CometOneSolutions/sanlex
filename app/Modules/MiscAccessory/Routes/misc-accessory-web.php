<?php
Route::group(['prefix' => 'master-file'], function () {


	Route::group(['prefix' => 'misc-accessories'], function () {
		Route::get('/', 'MiscAccessoryController@index')->name('misc-accessory_index');
		Route::get('/create', 'MiscAccessoryController@create')->name('misc-accessory_create');
		Route::get('{miscAccessoryId}/edit', 'MiscAccessoryController@edit')->name('misc-accessory_edit');
		Route::get('{miscAccessoryId}/print', 'MiscAccessoryController@print')->name('misc-accessory_print');
		Route::get('{miscAccessoryId}', 'MiscAccessoryController@show')->name('misc-accessory_show');
	});
});
