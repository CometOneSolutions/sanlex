<?php

namespace App\Modules\MiscAccessory\Requests;

use Dingo\Api\Http\FormRequest;

class StoreMiscAccessory extends MiscAccessoryRequest
{
	public function authorize()
	{
		return $this->user()->can('Create [MAS] MiscAccessory');
	}
}
