<?php

namespace App\Modules\MiscAccessory\Requests;

use App\Modules\BackingSide\Services\BackingSideRecordService;
use App\Modules\Length\Services\LengthRecordService;
use App\Modules\MiscAccessory\Services\MiscAccessoryRecordService;
use App\Modules\MiscAccessoryType\Services\MiscAccessoryTypeRecordService;
use App\Modules\Supplier\Services\SupplierRecordService;
use App\Modules\Width\Services\WidthRecordService;
use Dingo\Api\Http\FormRequest;

class MiscAccessoryRequest extends FormRequest
{
	private $service;
	private $supplierRecordService;
	private $lengthRecordService;
	private $miscAccessoryTypeRecordService;
	private $backingSideRecordService;


	public function __construct(
		SupplierRecordService $supplierRecordService,
		MiscAccessoryRecordService $miscAccessoryRecordService,
		LengthRecordService $lengthRecordService,
		WidthRecordService $widthRecordService,
		MiscAccessoryTypeRecordService $miscAccessoryTypeRecordService,
		BackingSideRecordService $backingSideRecordService

	) {
		$this->supplierRecordService = $supplierRecordService;
		$this->service = $miscAccessoryRecordService;
		$this->lengthRecordService = $lengthRecordService;
		$this->widthRecordService = $widthRecordService;
		$this->miscAccessoryTypeRecordService = $miscAccessoryTypeRecordService;
		$this->backingSideRecordService = $backingSideRecordService;
	}

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$miscAccessoryId = null;
		if ($this->route('miscAccessoryId')) {
			$miscAccessory = $this->getMiscAccessory('id');
			$miscAccessoryId = $miscAccessory->product->getId();
		}
		return [
			'name'          => 'required|unique:products,name,' . $miscAccessoryId,
			'widthId'       => 'required',
			'lengthId'      => 'required',
			'backingSideId'      => 'required',
			'miscAccessoryTypeId' => 'required'
		];
	}
	public function messages()
	{
		return [
			'name.required'                  => 'Name is required.',
			'name.unique'                    => 'Name already exists.',
			'widthId.required'               => 'Width is required.',
			'lengthId.required'              => 'Length is required.',
			'backingSideId.required'              => 'Backing side is required.',
			'miscAccessoryTypeId.required'       => 'Misc Accessory type is required.'
		];
	}



	public function getName($index = 'name')
	{
		return $this->input($index);
	}

	public function getSupplier($index = 'supplierId')
	{
		return $this->supplierRecordService->getById($this->input($index));
	}

	public function getMiscAccessory($index = 'id')
	{
		return $this->service->getById($this->input($index));
	}

	public function getLength($index = 'lengthId')
	{
		return $this->lengthRecordService->getById($this->input($index));
	}

	public function getWidth($index = 'widthId')
	{
		return $this->widthRecordService->getById($this->input($index));
	}

	public function getBackingSide($index = 'backingSideId')
	{
		return $this->backingSideRecordService->getById($this->input($index));
	}

	public function getMiscAccessoryType($index = 'miscAccessoryTypeId')
	{
		return $this->miscAccessoryTypeRecordService->getById($this->input($index));
	}
}
