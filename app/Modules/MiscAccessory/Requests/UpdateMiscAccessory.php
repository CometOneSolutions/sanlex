<?php

namespace App\Modules\MiscAccessory\Requests;

use Dingo\Api\Http\FormRequest;

class UpdateMiscAccessory extends MiscAccessoryRequest
{
	public function authorize()
	{
		return $this->user()->can('Update [MAS] MiscAccessory');
	}
}
