<?php

namespace App\Modules\MiscAccessory\Requests;

use Dingo\Api\Http\FormRequest;

class DestroyMiscAccessory extends MiscAccessoryRequest
{
    public function authorize()
    {
        return $this->user()->can('Delete [MAS] MiscAccessory');
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}