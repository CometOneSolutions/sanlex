import { Form } from '@c1_common_js/components/Form';

import SaveButton from '@c1_common_js/components/SaveButton'
import DeleteButton from '@c1_common_js/components/DeleteButton'
import Timestamp from '@c1_common_js/components/Timestamp'
import VForm from '@c1_common_js/components/VForm'
import Select3 from "@c1_common_js/components/Select3";


new Vue({
    el: '#miscAccessory',
    components: {
        SaveButton, DeleteButton, Timestamp, VForm, Select3
    },
    data: {
        form: new Form({
            name: null,
            supplierId: null,
            shortCode: null,
            lengthId: null,
            lengthText: null,
            widthId: null,
            widthText: null,
            backingSideId: null,
            backingSideText: null,
            miscAccessoryTypeId: null,
            miscAccessoryTypeText: null
        }),
        dataInitialized: true,
        supplierUrl: '/api/suppliers?sort=name',
        defaultSelectedSupplier: {},
        lengthUrl: '/api/lengths?sort=name',
        widthUrl: '/api/widths?sort=name',
        backingSideUrl: '/api/backing-sides?sort=name',
        defaultSelectedBackingSide: {},
        defaultSelectedWidth: {},
        defaultSelectedLength: {},
        miscAccessoryTypeUrl: '/api/misc-accessory-types?sort=name',
        defaultSelectedMiscAccessoryType: {},

    },
    watch: {
        initializationComplete(val) {
            this.form.isInitializing = !val;
        }
    },
    computed: {
        // selectedSupplier() {
        //     if(this.form.supplierId == null)
        //     {
        //         return undefined;
        //     }
        //     return this.supplierSelections.find(supplier => supplier.id == this.form.supplierId);
        // },
        name() {
            if (this.form.supplierId === undefined || this.form.lengthId === null || this.form.miscAccessoryTypeId === null || this.form.widthId === null || this.form.backingSideId === null) {
                return undefined;
            }
            return this.form.shortCode + '~' + this.form.widthText.toUpperCase() + '~' + this.form.lengthText.toUpperCase() + '~' + this.form.backingSideText.toUpperCase() + '~' + this.form.miscAccessoryTypeText.toUpperCase();
        },
        initializationComplete() {
            return this.dataInitialized;
        }
    },
    methods: {
        setShortCode(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("shortCode")) {
                this.form.shortCode = selectedObjects[0].shortCode;
            }
        },
        setLength(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.lengthText = selectedObjects[0].name;
            }

        },
        setWidth(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.widthText = selectedObjects[0].name;
            }

        },
        setBackingSide(selectedValue, selectedObjects) {
            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.backingSideText = selectedObjects[0].name;
            }
        },
        setMiscAccessoryType(selectedValue, selectedObjects) {

            if (selectedObjects[0].hasOwnProperty("name")) {
                this.form.miscAccessoryTypeText = selectedObjects[0].name;
            }

        },
        destroy() {
            this.form.deleteWithConfirmation('/api/misc-accessories/' + this.form.id).then(response => {
                this.form.successModal('Misc accessory was removed.').then(() =>
                    window.location = '/master-file/misc-accessories/'
                );
            });
        },

        store() {
            this.form.name = this.name;
            this.form.postWithModal('/api/misc-accessories', null, 'Misc accessory was saved.');
        },

        update() {
            this.form.name = this.name;
            this.form.patch('/api/misc-accessories/' + this.form.id).then(response => {
                this.form.successModal('Misc accessory was updated.').then(() =>
                    window.location = '/master-file/misc-accessories/' + this.form.id
                );
            })
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    created() {


        if (id != null) {
            this.dataInitialized = false;
            this.form.get('/api/misc-accessories/' + id + '?include=product.supplier,length,width,backingSide,miscAccessoryType')
                .then(response => {
                    this.loadData(response.data);
                    this.form.shortCode = response.data.product.data.supplier.data.shortCode;
                    this.defaultSelectedSupplier = {
                        text: response.data.product.data.supplier.data.name,
                        id: response.data.product.data.supplier.data.id,
                    };
                    this.defaultSelectedLength = {
                        text: response.data.length.data.name,
                        id: response.data.length.data.id,
                    };
                    this.form.lengthText = response.data.length.data.name;
                    this.defaultSelectedMiscAccessoryType = {
                        text: response.data.miscAccessoryType.data.name,
                        id: response.data.miscAccessoryType.data.id,
                    };
                    this.form.widthText = response.data.width.data.name;
                    this.defaultSelectedWidth = {
                        text: response.data.width.data.name,
                        id: response.data.width.data.id,
                    };
                    this.form.backingSideText = response.data.backingSide.data.name;
                    this.defaultSelectedBackingSide = {
                        text: response.data.backingSide.data.name,
                        id: response.data.backingSide.data.id,
                    };
                    this.form.miscAccessoryTypeText = response.data.miscAccessoryType.data.name;

                    this.dataInitialized = true;
                });
        }
    },
    mounted() {
        console.log("Init misc accessory script...");
    }
});