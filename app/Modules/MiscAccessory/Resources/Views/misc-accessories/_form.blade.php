<div v-if="Object.keys(form.errors.errors).length" class="alert alert-danger" role="alert">
    <small class="form-text form-control-feedback" v-for="error in form.errors.errors">
        @{{ error[0] }}
    </small>
</div>
<div class="row">

    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Supplier <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter supplier" v-model="form.supplierId" :selected="defaultSelectedSupplier" :url="supplierUrl" search="name" placeholder="Please select" @change="setShortCode" id="supplier" name="supplier">
            </select3>
        </div>
    </div>


    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Length <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter length" v-model="form.lengthId" :selected="defaultSelectedLength" :url="lengthUrl" search="name" placeholder="Please select" @change="setLength" id="length" name="length">
            </select3>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Width <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter width" v-model="form.widthId" :selected="defaultSelectedWidth" :url="widthUrl" search="name" placeholder="Please select" @change="setWidth" id="width" name="width">
            </select3>
        </div>
    </div>
				<div class="col-12">
					<div class="form-group">
									<label for="supplier">Backing Side <span class="text-danger">*</span></label>
									<select3 class="custom-select" required data-msg-required="Enter backing side" v-model="form.backingSideId" :selected="defaultSelectedBackingSide" :url="backingSideUrl" search="name" placeholder="Please select" @change="setBackingSide" id="backingSide" name="backingSide">
									</select3>
					</div>
				</div>
    <div class="col-12">
        <div class="form-group">
            <label for="supplier">Misc Accessory Type <span class="text-danger">*</span></label>
            <select3 class="custom-select" required data-msg-required="Enter misc accessory type" v-model="form.miscAccessoryTypeId" :selected="defaultSelectedMiscAccessoryType" :url="miscAccessoryTypeUrl" search="name" placeholder="Please select" @change="setMiscAccessoryType" id="miscAccessoryType" name="miscAccessoryType">
            </select3>
        </div>
    </div>

</div>
@push('scripts')
<script src="{{ mix('js/misc-accessory.js') }}"></script>
@endpush