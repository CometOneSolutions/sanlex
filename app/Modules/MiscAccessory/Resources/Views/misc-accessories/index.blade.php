@extends('app')
@section('breadcrumbs', Breadcrumbs::render('misc-accessory.index'))
@section('content')
<div id="index" v-cloak>
    <div class="card">
        <div class="card-header">
            Misc Accessories
            @if(auth()->user()->can('Create [MAS] Misc Accessory'))
            <div class="float-right">
                <a href="{{route('misc-accessory_create')}}"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> </button></a>
            </div>
            @endif
        </div>
        <div class="card-body">
            <index :filterable="filterable" :base-url="baseUrl" :sort-ascending="sortAscending" :sorter="sorter" v-on:update-loading="(val) => isLoading = val" v-on:update-items="(val) => items = val">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                <a v-on:click="setSorter('id')">
                                    Product <i class="fa" :class="getSortIcon('id')"></i>
                                </a>
                            </th>

																												<th>Length</th>
																												<th>Width</th>
																												<th>Backing Side</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in items" v-if="!isLoading">
                            <td><a :href="item.showUri">@{{ item.product.data.name }}</a></td>

																												<td>@{{item.length.data.name}}</td>
																												<td>@{{item.width.data.name}}</td>
																												<td>@{{item.backingSide.data.name}}</td>
																												<td>@{{item.miscAccessoryType.data.name}}</td>


                        </tr>
                    </tbody>
                </table>
            </index>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{ mix('js/index.js') }}"></script>
@endpush