<?php

namespace App\Modules\MiscAccessory\Repositories;

use App\Modules\MiscAccessory\Models\MiscAccessory;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentMiscAccessoryRepository extends EloquentRepository implements MiscAccessoryRepository
{
    public function __construct(MiscAccessory $miscAccessory)
    {
        parent::__construct($miscAccessory);
    }

    public function save(MiscAccessory $miscAccessory)
    {

        return $miscAccessory->save();
    }

    public function delete(MiscAccessory $miscAccessory)
    {
        $product = $miscAccessory->getProduct();
        $product->delete();
        return $miscAccessory->delete();
    }
    public function filterByName($productName)
    {
        return $this->model->whereHas('product', function ($product) use ($productName) {
            return $product->where('name', 'like', '%' . $productName . '%');
        });
    }
    public function filterBySupplierName($supplierName)
    {
        return $this->model->whereHas('product', function ($product) use ($supplierName) {
            return $product->whereHas('supplier', function ($supplier) use ($supplierName) {
                return $supplier->where('name', 'like', '%' . $supplierName . '%');
            });
        });
    }
    public function inStock()
    {
        $this->model = $this->model->inStock();
        return $this;
    }
}
