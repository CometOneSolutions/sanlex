<?php

namespace App\Modules\MiscAccessory\Repositories;

use App\Modules\MiscAccessory\Models\MiscAccessory;
use CometOneSolutions\Common\Repositories\Repository;

interface MiscAccessoryRepository extends Repository
{
    public function save(MiscAccessory $miscAccessory);
    public function delete(MiscAccessory $miscAccessory);
}
