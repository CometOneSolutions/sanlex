<?php

use Faker\Generator as Faker;
use App\Modules\MiscAccessory\Models\MiscAccessoryModel;
use CometOneSolutions\Auth\UserModel;

$factory->define(MiscAccessoryModel::class, function (Faker $faker) {
    return [
        'description' => $faker->unique()->company,
        'updated_by_user_id' => $faker->randomElement(UserModel::pluck('id')->toArray()),
    ];
});
