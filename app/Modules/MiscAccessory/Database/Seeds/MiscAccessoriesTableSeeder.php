<?php

namespace App\Modules\MiscAccessory\Database\Seeds;

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

class MiscAccessoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $colors = factory(AccessoriesModel::class, 5)->create();
    }
}
