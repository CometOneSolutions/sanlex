<?php

namespace App\Modules\MiscAccessory\Models;


interface MiscAccessory
{
    public function getId();

    public function getDescription();

    public function setDescription($value);
}
