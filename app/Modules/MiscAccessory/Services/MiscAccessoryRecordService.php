<?php

namespace App\Modules\MiscAccessory\Services;

use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\Length\Models\Length;
use App\Modules\MiscAccessory\Models\MiscAccessory;
use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Services\RecordService;

use CometOneSolutions\Auth\Models\Users\User;

interface MiscAccessoryRecordService extends RecordService
{
	public function create(
		$name,
		Supplier $supplier,
		Width $width,
		Length $length,
		BackingSide $backingSide,
		MiscAccessoryType $miscAccessoryType,
		User $user = null
	);

	public function update(
		MiscAccessory $miscAccessory,
		$name,
		Supplier $supplier,
		Width $width,
		Length $length,
		BackingSide $backingSide,
		MiscAccessoryType $miscAccessoryType,
		User $user = null
	);

	public function delete(MiscAccessory $miscAccessory);
}
