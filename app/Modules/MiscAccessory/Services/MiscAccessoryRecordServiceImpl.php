<?php

namespace App\Modules\MiscAccessory\Services;

use App\Modules\BackingSide\Models\BackingSide;
use App\Modules\Length\Models\Length;
use App\Modules\MiscAccessory\Models\MiscAccessory;
use App\Modules\MiscAccessory\Repositories\MiscAccessoryRepository;
use App\Modules\MiscAccessoryType\Models\MiscAccessoryType;
use App\Modules\Product\Services\ProductRecordService;
use App\Modules\Supplier\Models\Supplier;
use App\Modules\Width\Models\Width;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Auth\Models\Users\User;

class MiscAccessoryRecordServiceImpl extends RecordServiceImpl implements MiscAccessoryRecordService
{
	private $miscAccessory;
	private $miscAccessorys;
	private $productRecordService;

	protected $availableFilters = [
		["id" => "name", "text" => "Name"],

	];

	public function __construct(MiscAccessoryRepository $miscAccessorys, MiscAccessory $miscAccessory, ProductRecordService $productRecordService)
	{
		$this->miscAccessorys = $miscAccessorys;
		$this->miscAccessory = $miscAccessory;
		$this->productRecordService = $productRecordService;
		parent::__construct($miscAccessorys);
	}

	public function create(
		$name,
		Supplier $supplier,
		Width $width,
		Length $length,
		BackingSide $backingSide,
		MiscAccessoryType $miscAccessoryType,
		User $user = null
	) {
		$miscAccessory = new $this->miscAccessory;
		$miscAccessory->setLength($length);
		$miscAccessory->setWidth($width);
		$miscAccessory->setBackingSide($backingSide);
		// dd($miscAccessoryType);
		$miscAccessory->setMiscAccessoryType($miscAccessoryType);

		if ($user) {
			$miscAccessory->setUpdatedByUser($user);
		}

		$this->miscAccessorys->save($miscAccessory);

		$this->productRecordService->create($name, $supplier, $miscAccessory, $user);
		return $miscAccessory;
	}

	public function update(
		MiscAccessory $miscAccessory,
		$name,
		Supplier $supplier,
		Width $width,
		Length $length,
		BackingSide $backingSide,
		MiscAccessoryType $miscAccessoryType,
		User $user = null
	) {
		$tempMiscAccessory = clone $miscAccessory;
		$product = $tempMiscAccessory->getProduct();
		$tempMiscAccessory->setLength($length);
		$tempMiscAccessory->setBackingSide($backingSide);
		$tempMiscAccessory->setMiscAccessoryType($miscAccessoryType);
		$tempMiscAccessory->setWidth($width);
		if ($user) {
			$tempMiscAccessory->setUpdatedByUser($user);
		}
		$this->miscAccessorys->save($tempMiscAccessory);
		$this->productRecordService->update($product, $name, $supplier, $tempMiscAccessory, $user);
		return $tempMiscAccessory;
	}

	public function delete(MiscAccessory $miscAccessory)
	{
		$this->miscAccessorys->delete($miscAccessory);
		return $miscAccessory;
	}
	public function inStock()
	{
		$this->miscAccessorys = $this->miscAccessorys->inStock();
		return $this;
	}
}
