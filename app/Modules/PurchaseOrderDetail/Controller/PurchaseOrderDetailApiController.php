<?php

namespace App\Modules\PurchaseOrderDetail\Controller;

use CometOneSolutions\Common\Controllers\ResourceApiController;
use App\Modules\PurchaseOrderDetail\Services\PurchaseOrderDetailRecordService;
use App\Modules\PurchaseOrderDetail\Transformer\PurchaseOrderDetailTransformer;

class PurchaseOrderDetailApiController extends ResourceApiController
{
    protected $service;
    protected $transformer;

    public function __construct(
        PurchaseOrderDetailRecordService $purchaseOrderRecordService,
        PurchaseOrderDetailTransformer $transformer
    ) {
        parent::__construct($purchaseOrderRecordService, $transformer);
        $this->service = $purchaseOrderRecordService;
        $this->transformer = $transformer;
    }
}
