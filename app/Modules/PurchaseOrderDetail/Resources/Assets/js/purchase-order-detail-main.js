import { Form } from "@c1_common_js/components/Form";

export const purchaseOrderDetailService = new Vue({
    data: function() {
        return {
            form: new Form({}),
            uri: {
                purchaseOrderDetails: '/api/purchase-order-details?limit=' + Number.MAX_SAFE_INTEGER,
            }
        };
    },
    methods: {
        getPurchaseOrderDetails() {
            return this.form.get(this.uri.purchaseOrderDetails);
        },
        getPurchaseOrderDetailWithInventory() {
            let url = this.uri.purchaseOrderDetails + '&inventory_quantity>0';
            return this.form.get(url);
        },

        getProFormaDetailsByPurchaseId(id){
            let url = this.uri.purchaseOrderDetails + '&purchase_order_id=' + id;
            return this.form.get(url);
        },
        
    }
});
