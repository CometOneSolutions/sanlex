<?php

namespace App\Modules\PurchaseOrderDetail\Services;

use CometOneSolutions\Common\Services\RecordService;

interface PurchaseOrderDetailRecordService extends RecordService
{
}
