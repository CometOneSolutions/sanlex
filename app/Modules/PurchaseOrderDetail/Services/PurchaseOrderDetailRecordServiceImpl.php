<?php

namespace App\Modules\PurchaseOrderDetail\Services;

use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\PurchaseOrderDetail\Models\PurchaseOrderDetail;
use App\Modules\PurchaseOrderDetail\Repositories\PurchaseOrderDetails;
use CometOneSolutions\Common\Services\RecordServiceImpl;
use CometOneSolutions\Common\Utils\Filter;

class PurchaseOrderDetailRecordServiceImpl extends RecordServiceImpl implements PurchaseOrderDetailRecordService
{
    private $purchaseOrderDetails;

    public function __construct(PurchaseOrderDetails $purchaseOrderDetails)
    {
        parent::__construct($purchaseOrderDetails);
        $this->purchaseOrderDetails = $purchaseOrderDetails;
    }
    public function getFirstWithPurchaseOrderAndProductId(PurchaseOrder $purchaseOrder, $productId)
    {
        $purchaseOrderFilter = new Filter('purchase_order_id', $purchaseOrder->getId());
        $productIdFilter = new Filter('product_id', $productId);
        return $this->getFirst([$purchaseOrderFilter, $productIdFilter]);
    }
    public function updateInventoryQuantity(PurchaseOrderDetail $purchaseOrderDetail, $quantity)
    {
        $tmpPurchaseOrderDetail = clone $purchaseOrderDetail;
        $tmpPurchaseOrderDetail->setInventoryQuantity($quantity);
        $this->purchaseOrderDetails->save($tmpPurchaseOrderDetail);
        return $tmpPurchaseOrderDetail;
    }

    public function updateReceivedQuantity(PurchaseOrderDetail $purchaseOrderDetail, $quantity)
    {
        $tmpPurchaseOrderDetail = clone $purchaseOrderDetail;
        $tmpPurchaseOrderDetail->setReceivedQuantity($quantity);
        $tmpPurchaseOrderDetail->setPackingUnlistedQuantity(abs($tmpPurchaseOrderDetail->getOrderedQuantity() - $tmpPurchaseOrderDetail->getReceivedQuantity()));
        $this->purchaseOrderDetails->save($tmpPurchaseOrderDetail);
        return $tmpPurchaseOrderDetail;
    }
}
