<?php

namespace App\Modules\PurchaseOrderDetail\Repositories;

use App\Modules\PurchaseOrderDetail\Models\PurchaseOrderDetail;
use CometOneSolutions\Common\Repositories\EloquentRepository;

class EloquentPurchaseOrderDetails extends EloquentRepository implements PurchaseOrderDetails
{
    protected static $filterable = [
        'in_stock', 'referenced_by_sales_order'
    ];

    protected $model;

    public function __construct(PurchaseOrderDetail $purchaseOrderDetail)
    {
        parent::__construct($purchaseOrderDetail);
    }

    function renew()
    {
        if (!$this->model instanceof PurchaseOrderDetail) {
            $this->model = new PurchaseOrderDetail();
        }
        return $this;
    }

    public function getTransit()
    {
        return PurchaseOrderDetail::TRANSIT;
    }

    public function getStock()
    {
        return PurchaseOrderDetail::STOCK;
    }

    public function getOut()
    {
        return PurchaseOrderDetail::OUT;
    }

    public function filterByInStock($query, $value = true)
    {
        $query->where('quantity_balance', '>', 0)->where('status', '=', 'IN_STOCK');
    }

    public function filterByReferencedBySalesOrder($query, $value)
    {
        $query->orWhereHas('salesOrderDetails', function ($salesOrderDetail) use ($value) {
            return $salesOrderDetail->whereSalesOrderId($value);
        });
    }

    public function save(PurchaseOrderDetail $purchaseOrderDetail)
    {
        return $purchaseOrderDetail->save();
    }

    public function delete(PurchaseOrderDetail $purchaseOrderDetail)
    {
        return $purchaseOrderDetail->delete();
    }

    // public function getById($id)
    // {
    //     $query->where('quantity_balance', '>', 0)->where('status', '=', 'IN_STOCK');
    // }
}
