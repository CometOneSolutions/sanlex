<?php

namespace App\Modules\PurchaseOrderDetail\Repositories;

use CometOneSolutions\Common\Repositories\Repository;

interface PurchaseOrderDetails extends Repository
{
    public function getTransit();
    public function getStock();
    public function getOut();
    public function renew();
}
