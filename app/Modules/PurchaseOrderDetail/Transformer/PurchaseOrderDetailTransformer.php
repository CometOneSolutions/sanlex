<?php

namespace App\Modules\PurchaseOrderDetail\Transformer;

use App\Modules\Product\Transformer\ProductTransformer;
use App\Modules\PurchaseOrder\Models\PurchaseOrder;
use App\Modules\PurchaseOrder\Transformer\PurchaseOrderTransformer;
use App\Modules\PurchaseOrderDetail\Models\PurchaseOrderDetail;
use League\Fractal;

class PurchaseOrderDetailTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'purchaseOrder', 'product'
    ];

    public function transform(PurchaseOrderDetail $purchaseOrderDetail)
    {

        return [
            'id' => (int) $purchaseOrderDetail->id,
            'productId' => (int) $purchaseOrderDetail->product_id,
            'productName' => $purchaseOrderDetail->getProduct()->getName(),
            'quantity' => (float) $purchaseOrderDetail->getOrderedQuantity(),
            'receivedQuantity' => ($purchaseOrderDetail->getPurchaseOrder()->getProductType() == PurchaseOrder::COIL) ? $purchaseOrderDetail->getPurchaseOrder()->getReceivingNetWeight($purchaseOrderDetail->product_id) : $purchaseOrderDetail->getPurchaseOrder()->getReceivingQuantity($purchaseOrderDetail->product_id),
            'uomId' => (int) $purchaseOrderDetail->getUomId(),
            'uomName' => $purchaseOrderDetail->getUom()->getName(),
            'currencyName' => $purchaseOrderDetail->getCurrency() ? $purchaseOrderDetail->getCurrency()->getCode() : null,
            'currencyId' => $purchaseOrderDetail->getCurrency() ? $purchaseOrderDetail->getCurrency()->getId() : null,
            'length' => (float) ($purchaseOrderDetail->length),
            'width' => (float) ($purchaseOrderDetail->width),
            'lineTotal' => (float) $purchaseOrderDetail->getLineTotal(),
        ];
    }

    public function includePurchaseOrder(PurchaseOrderDetail $purchaseOrderDetail)
    {
        $purchaseOrder = $purchaseOrderDetail->getPurchaseOrder();
        return $this->item($purchaseOrder, new PurchaseOrderTransformer);
    }

    public function includeProduct(PurchaseOrderDetail $purchaseOrderDetail)
    {
        $product = $purchaseOrderDetail->getProduct();
        return $this->item($product, new ProductTransformer);
    }
}
