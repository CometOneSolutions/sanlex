<?php

namespace App\Modules\PurchaseOrderDetail\Models;

use App\Modules\Inventory\Models\InventoryModel;
use App\Modules\Product\Models\Product;
use App\Modules\Product\Models\ProductModel;
use App\Modules\PurchaseOrder\Models\PurchaseOrderModel;
use App\Modules\Uom\Models\Uom;
use App\Modules\Uom\Models\UomModel;
use Illuminate\Database\Eloquent\Model;


class PurchaseOrderDetailModel extends Model implements PurchaseOrderDetail
{

    protected $table = 'purchase_order_details';


    public function product()
    {
        return $this->belongsTo(ProductModel::class, 'product_id');
    }

    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrderModel::class, 'purchase_order_id');
    }

    public function uom()
    {
        return $this->belongsTo(UomModel::class, 'uom_id');
    }

    public function inventory()
    {
        return $this->hasOne(InventoryModel::class, 'purchase_order_detail_id');
    }

    public function getInventory()
    {
        return $this->inventory;
    }

    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    public function getInventoryQuantity()
    {
        return $this->inventory_quantity;
    }

    public function setInventoryQuantity($value)
    {
        $this->inventory_quantity = $value;
        return $this;
    }
    public function getUomId()
    {
        return $this->uom_id;
    }
    public function getUom()
    {
        return $this->uom;
    }

    public function setUom(Uom $uom)
    {
        $this->uom()->associate($uom);
        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct(Product $product)
    {
        $this->product()->associate($product);
        return $this;
    }

    public function setPackingUnlistedQuantity($value)
    {
        $this->packing_unlisted_quantity = $value;
        return $this;
    }

    public function getPackingUnlistedQuantity()
    {
        return $this->packing_unlisted_quantity;
    }

    public function setPONumber($value)
    {
        $this->po_no = $value;
        return $this;
    }

    public function getPONumber()
    {
        return $this->po_no;
    }

    public function setLength($value)
    {
        $this->length = $value;
        return $this;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function setWidth($value)
    {
        $this->width = $value;
        return $this;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setOrderedQuantity($value)
    {
        $this->ordered_quantity = $value;
        return $this;
    }

    public function getOrderedQuantity()
    {
        return $this->ordered_quantity;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setProformaQuantity($value)
    {
        $this->setPackingUnlistedQuantity($value);

        $this->proforma_quantity = $value;
        $this->setLineTotal();
        return $this;
    }

    public function getProformaQuantity()
    {
        return $this->proforma_quantity;
    }

    public function setReceivedQuantity($value)
    {
        $this->received_quantity = $value;
        return $this;
    }

    public function getReceivedQuantity()
    {
        return $this->received_quantity;
    }

    public function setUnitCost($value)
    {
        $this->unit_cost = round($value, 2);
        $this->setLineTotal();
        return $this;
    }

    public function getUnitCost()
    {
        return $this->unit_cost;
    }

    protected function setLineTotal()
    {
        $this->line_total = ($this->getProformaQuantity() ?? 0) * ($this->getUnitCost() ?? 0);
        return $this;
    }

    public function getLineTotal()
    {
        return $this->line_total;
    }



    public function getCurrency()
    {
        return $this->currency;
    }

    public function getContainerDetails()
    {
        return $this->containerDetails;
    }

    public function getContainerDetailsQuantity()
    {
        return $this->containerDetails()->sum('quantity');
    }

    public function scopeInTransit($query)
    {
        return $query->whereHas('purchaseOrder', function ($purchaseOrder) {
            return $purchaseOrder->inTransit();
        });
    }
    // public function scopeReceivings($query, $purchaseOrderId, $productId)
    // {
    //     return $query->whereHas('purchaseOrder', function ($purchaseOrder) use($purchaseOrderId){
    //         return $purchaseOrder->where("purchase_order_id", $purchaseOrderId);
    //     });
    // }
}
