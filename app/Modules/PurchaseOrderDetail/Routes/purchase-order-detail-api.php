<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'purchase-order-details'], function () use ($api) {
        $api->get('/', 'App\Modules\PurchaseOrderDetail\Controllers\PurchaseOrderDetailApiController@index');
    });

});

