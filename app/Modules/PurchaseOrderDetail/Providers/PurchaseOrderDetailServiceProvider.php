<?php

namespace App\Modules\PurchaseOrderDetail\Providers;

use App\Modules\PurchaseOrderDetail\Models\PurchaseOrderDetail;
use App\Modules\PurchaseOrderDetail\Models\PurchaseOrderDetailModel;
use App\Modules\PurchaseOrderDetail\Repositories\EloquentPurchaseOrderDetails;
use App\Modules\PurchaseOrderDetail\Repositories\PurchaseOrderDetails;
use App\Modules\PurchaseOrderDetail\Services\PurchaseOrderDetailRecordService;
use App\Modules\PurchaseOrderDetail\Services\PurchaseOrderDetailRecordServiceImpl;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class PurchaseOrderDetailServiceProvider extends ServiceProvider
{
    protected $dbPath = 'Modules/PurchaseOrderDetail/Database/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Factory::class)->load(app_path($this->dbPath . 'factories'));
        $this->app->bind(PurchaseOrderDetailRecordService::class, PurchaseOrderDetailRecordServiceImpl::class);
        $this->app->bind(PurchaseOrderDetails::class, EloquentPurchaseOrderDetails::class);
        $this->app->bind(PurchaseOrderDetail::class, PurchaseOrderDetailModel::class);
    }
}
