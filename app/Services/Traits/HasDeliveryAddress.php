<?php

namespace App\Services\Traits;

use App\Models\Address;

trait HasDeliveryAddress
{
    public function getDeliveryAddress()
    {
        return new Address($this->delivery_street_address, $this->delivery_city_address, $this->delivery_zip_address);
    }

    public function setDeliveryAddress(Address $address)
    {
        $this->delivery_street_address = $address->getStreet();
        $this->delivery_city_address = $address->getCity();
        $this->delivery_zip_address = $address->getZip();
        return $this;
    }
}
