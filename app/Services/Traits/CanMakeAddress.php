<?php

namespace App\Services\Traits;

use App\Models\Address;

trait CanMakeAddress
{
    public function makeAddress($street, $city, $zip)
    {
        $address = new Address($street, $city, $zip);
        return $address;
    }
}
