<?php

namespace App\Services\Traits;

use App\Models\Address;

trait HasMainAddress
{
    public function getMainAddress()
    {
        return new Address($this->main_street_address, $this->main_city_address, $this->main_zip_address);
    }

    public function setMainAddress(Address $address)
    {
        $this->main_street_address = $address->getStreet();
        $this->main_city_address = $address->getCity();
        $this->main_zip_address = $address->getZip();
        return $this;
    }
}
