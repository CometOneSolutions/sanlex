<?php

namespace App\Services\Traits;

use App\Modules\Contacts\Models\Base\Contact;

trait CanMakeContact
{
    public function makeContact($person, $position, $telephone, $fax, $mobile, $email)
    {
        $contact = resolve(Contact::class);
        $contact->setContactPerson($person);
        $contact->setPosition($position);
        $contact->setTelephone($telephone);
        $contact->setFax($fax);
        $contact->setMobile($mobile);
        $contact->setEmail($email);
        return $contact;
    }
}
