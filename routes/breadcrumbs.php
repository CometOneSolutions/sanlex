<?php
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

Breadcrumbs::register('master-file.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Master File', route('master-file_index'));
});

//PURCHASING
Breadcrumbs::register('purchasing-menu-dashboard.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Purchasing', route('purchasing-menu-dashboard_index'));
});

//REPORT
Breadcrumbs::register('report-menu-dashboard.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Reports', route('report-menu-dashboard_index'));
});

//OUT
Breadcrumbs::register('out-menu-dashboard.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Out', route('out-menu-dashboard_index'));
});

//COLLECTION
Breadcrumbs::register('collection.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Collections', route('collection_index'));
});

//SALES
Breadcrumbs::register('sales-menu-dashboard.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Sales', route('sales-menu-dashboard_index'));
});

//PRODUCTION
Breadcrumbs::register('production-menu-dashboard.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Production', route('production-menu-dashboard_index'));
});




// Breadcrumbs::register('banking-menu-dashboard.index', function ($breadcrumbs) {
//     $breadcrumbs->parent('home');
//     $breadcrumbs->push('Banking', route('banking-menu-dashboard_index'));
// });


//SUPPLIER PRODUCT
Breadcrumbs::register('supplier-product_create', function ($breadcrumbs) {
    $breadcrumbs->parent('product_show', 'C2S');
    $breadcrumbs->push('Create', route('supplier-product_create', 1));
});

//USER
Breadcrumbs::register('user.master-list', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Master Lists - Users', route('user_index'));
});
Breadcrumbs::register('user.master-list.create', function ($breadcrumbs) {
    $breadcrumbs->parent('user.master-list');
    $breadcrumbs->push('Create', route('user_create'));
});
Breadcrumbs::register('user.master-list.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('user.master-list');
    $breadcrumbs->push($data["userName"], route('user_show', $data["userId"]));
});
Breadcrumbs::register('user.master-list.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('user.master-list.show', $data);
    $breadcrumbs->push('Edit', route('user_edit', $data["userId"]));
});


//ADJUSTMENT
Breadcrumbs::register('adjustment_index', function ($breadcrumbs) {
    $breadcrumbs->parent('inventory_show', 'C2S#80-000111~26~SA-18');
    $breadcrumbs->push('Adjustments', route('inventory_show', '1'));
});

Breadcrumbs::register('adjustment_create', function ($breadcrumbs) {
    $breadcrumbs->parent('adjustment_index');
    $breadcrumbs->push('Create', route('adjustment_create', 1));
});

//CONVERSION
Breadcrumbs::register('conversion_index', function ($breadcrumbs) {
    $breadcrumbs->parent('inventory_show', 'C2S#80-000113S~8.5X11');
    $breadcrumbs->push('Conversions', route('inventory_show', '1'));
});

Breadcrumbs::register('conversion_create', function ($breadcrumbs) {
    $breadcrumbs->parent('conversion_index');
    $breadcrumbs->push('Create', route('conversion_create', 1));
});
