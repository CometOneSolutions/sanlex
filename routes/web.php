<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::post('/login', 'Auth\LoginController@login')->name('post_login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/404', function () {
    return view('errors.404');
});
Route::get('/500', function () {
    return view('errors.500');
});

//CUSTOMER
//The alias or route name should match the method name delimited by "{singular form e.g. 'supplier' for suppliers}_".
Route::group(['middleware' => 'auth'], function () {

    // Route::get('/', function () {
    //     return view('dashboard');
    // })->name('home');

    Route::group(['prefix' => '/'], function () {
        Route::get('/', 'DefaultDashboardController@dashboard')->name('home');
    });
    Route::group(['prefix' => 'master-file'], function () {
        Route::get('/', 'MasterFileController@index')->name('master-file_index');
    });
    Route::group(['prefix' => 'purchasing'], function () {
        Route::get('/', 'PurchasingMenuDashboardController@dashboard')->name('purchasing-menu-dashboard_index');
    });

    Route::group(['prefix' => 'files'], function () {
        Route::get('{path}/download', 'MediaController@downloadFile')->where('path', '(.*)')->name('media_download');
        Route::get('{path}', 'MediaController@getFile')->where('path', '(.*)')->name('media_show');
        Route::delete('{path}', 'MediaController@delete')->name('media_delete');
    });
    
});
