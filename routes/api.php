<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {


    // Uoms
    $api->group(['prefix' => 'uoms'], function () use ($api) {
        $api->get('/', 'App\Http\Controllers\UomApiController@index');
    });




    // Currency Exchange
    $api->group(['prefix' => 'currency-exchanges'], function () use ($api) {
        $api->get('/', 'App\Http\Controllers\CurrencyExchangeApiController@index');
        // $api->get('/list', 'App\Http\Controllers\CurrencyExchangeListApiController@index');
        $api->get('{currencyExchangeId}', 'App\Http\Controllers\CurrencyExchangeApiController@show');
        $api->post('/', 'App\Http\Controllers\CurrencyExchangeApiController@store');
        $api->patch('{currencyExchangeId}', 'App\Http\Controllers\CurrencyExchangeApiController@update');
        $api->delete('{currencyExchangeId}', 'App\Http\Controllers\CurrencyExchangeApiController@destroy');
    });

    // Capital Injection
    $api->group(['prefix' => 'capital-injections'], function () use ($api) {
        $api->get('/', 'App\Http\Controllers\CapitalInjectionApiController@index');
        // $api->get('/list', 'App\Http\Controllers\CapitalInjectionListApiController@index');
        $api->get('{capitalInjectionId}', 'App\Http\Controllers\CapitalInjectionApiController@show');
        $api->post('/', 'App\Http\Controllers\CapitalInjectionApiController@store');
        $api->patch('{capitalInjectionId}', 'App\Http\Controllers\CapitalInjectionApiController@update');
        $api->delete('{capitalInjectionId}', 'App\Http\Controllers\CapitalInjectionApiController@destroy');
    });

    // Withdrawals
    $api->group(['prefix' => 'withdrawals'], function () use ($api) {
        $api->get('/', 'App\Http\Controllers\WithdrawalApiController@index');
        // $api->get('/list', 'App\Http\Controllers\WithdrawalListApiController@index');
        $api->get('{withdrawalId}', 'App\Http\Controllers\WithdrawalApiController@show');
        $api->post('/', 'App\Http\Controllers\WithdrawalApiController@store');
        $api->patch('{withdrawalId}', 'App\Http\Controllers\WithdrawalApiController@update');
        $api->delete('{withdrawalId}', 'App\Http\Controllers\WithdrawalApiController@destroy');
    });

    // Transfers
    $api->group(['prefix' => 'transfers'], function () use ($api) {
        $api->get('/', 'App\Http\Controllers\TransferApiController@index');
        // $api->get('/list', 'App\Http\Controllers\TransferListApiController@index');
        $api->get('{transferId}', 'App\Http\Controllers\TransferApiController@show');
        $api->post('/', 'App\Http\Controllers\TransferApiController@store');
        $api->patch('{transferId}', 'App\Http\Controllers\TransferApiController@update');
        $api->delete('{transferId}', 'App\Http\Controllers\TransferApiController@destroy');
    });


});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
