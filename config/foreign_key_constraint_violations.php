<?php


function message(string $parentModule, string $childModule)
{
    return sprintf("Cannot delete %s that is referenced by %s", strtoupper($parentModule), strtoupper($childModule));
}

return [
    //Products
    'inventories_product_id_foreign' => message('product', 'inventory'),
    'purchase_order_details_product_id_foreign' => message('product', 'purchase order'),
    'purchase_order_accessory_details_product_id_foreign' => message('product', 'purchase order accessory'),
    'receivings_product_id_foreign' => message('product', 'receiving'),
    'job_orders_product_id_foreign' => message('product', 'job order'),

    //Customers
    'job_orders_customer_id_foreign' => message('customer', 'job order'),
    'delivery_receipts_customer_id_foreign' => message('customer', 'delivery receipt'),

    //Suppliers
    'products_supplier_id_foreign' => message('supplier', 'product'),
    'purchase_orders_supplier_id_foreign' => message('supplier', 'purchase order'),
    'purchase_order_accessories_supplier_id_foreign' => message('supplier', 'purchase order accessory'),

    // Thicknesses
    'coils_thickness_id_foreign' => message('thickness', 'coil'),
    'skylights_thickness_id_foreign' => message('thickness', 'skylight'),
    'insulations_thickness_id_foreign' => message('thickness', 'insulation'),
    'stainless_steels_thickness_id_foreign' => message('thickness', 'stainless steel'),
    'structural_steels_thickness_id_foreign' => message('thickness', 'structural steel'),

    // Widths
    'coils_width_id_foreign' => message('width', 'coil'),
    'skylights_width_id_foreign' => message('width', 'skylight'),
    'insulations_width_id_foreign' => message('width', 'insulation'),
    'stainless_steels_width_id_foreign' => message('width', 'stainless steel'),
    'misc_accessories_width_id_foreign' => message('width', 'misc accessory'),

    // Lengths
    'skylights_length_id_foreign' => message('length', 'skylight'),
    'insulations_length_id_foreign' => message('length', 'insulation'),
    'screws_length_id_foreign' => message('length', 'screw'),
    'stainless_steels_length_id_foreign' => message('length', 'stainless steel'),
    'misc_accessories_length_id_foreign' => message('length', 'misc accessory'),
    'structural_steels_length_id_foreign' => message('length', 'structural steel'),

    // Dimension X
    'structural_steels_dimension_x_id_foreign' => message('dimension x', 'structural steel'),

    // Sizes
    'paint_adhesives_size_id_foreign' => message('size', 'paint adhesive'),
    'fasteners_size_id_foreign' => message('size', 'fastener'),

    // Panel Profiles
    'job_orders_panel_profile_id_foreign' => message('panel profile', 'job order'),
    'skylights_panel_profile_id_foreign' => message('panel profile', 'skylight'),

    // Coatings
    'coils_coating_id_foreign' => message('coating', 'coil'),

    //Colors
    'coils_color_id_foreign' => message('color', 'coil'),
    'paint_adhesives_color_id_foreign' => message('color', 'paint adhesive'),

    //Insulation Types
    'insulations_insulation_type_id_foreign' => message('insulation type', 'insulation'),

    //Insulation Densities
    'insulations_insulation_density_id_foreign' => message('insulation density', 'insulation'),

    //Skylight Classes
    'skylights_skylight_class_id_foreign' => message('skylight class', 'skylight'),

    //Backing Sides
    'insulations_backing_side_id_foreign' => message('backing side', 'insulation'),
    'misc_accessories_backing_side_id_foreign' => message('backing side', 'misc accessory'),

    //Stainless Steel Types
    'stainless_steels_stainless_steel_type_id_foreign' => message('stainless steel type', 'stainless steel'),

    //Finishes
    'stainless_steels_finish_id_foreign' => message('finish', 'stainless steel'),
    'structural_steels_finish_id_foreign' => message('finish', 'structural steel'),
    'fasteners_finish_id_foreign' => message('finish', 'fastener'),

    //Misc Accessory Types
    'misc_accessories_misc_accessory_type_id_foreign' => message('misc accessory type', 'misc accessory'),

    //Structural Steel Types
    'structural_steels_structural_steel_type_id_foreign' => message('structural steel type', 'structural steel'),

    //Paint Adhesive Types
    'paint_adhesives_paint_adhesive_type_id_foreign' => message('paint adhesive type', 'paint adhesive'),

    //PVC Types
    'pvc_fittings_pvc_type_id_foreign' => message('pvc type', 'pvc fitting'),

    //PVC Diameters
    'pvc_fittings_pvc_diameter_id_foreign' => message('pvc diameter', 'pvc fitting'),

    //Fastener Types
    'fasteners_fastener_type_id_foreign' => message('fastener type', 'fastener'),

    //Fastener Dimensions
    'fasteners_fastener_dimension_id_foreign' => message('fastener dimension', 'fastener'),

    //Brands
    'pvc_fittings_brand_id_foreign' => message('brand', 'pvc fitting'),
    'paint_adhesives_brand_id_foreign' => message('brand', 'paint adhesive'),

];
