<?php

return [
    'model' => \App\Modules\InventoryDetail\Models\InventoryDetailModel::class,
    'table' => 'inventory_details',
    'id' => 'id',
];
