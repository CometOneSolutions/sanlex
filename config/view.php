<?php

return [
    /*
    |--------------------------------------------------------------------------
    | View Storage Paths
    |--------------------------------------------------------------------------
    |
    | Most templating systems load templates from disk. Here you may specify
    | an array of paths that should be checked for your views. Of course
    | the usual Laravel view path has already been registered for you.
    |
    */

    'paths' => [
        resource_path('views'),
        base_path('CometOneSolutions/Accounting/Resources/Views'),
        base_path('CometOneSolutions/Auth/Resources/Views'),
        base_path('CometOneSolutions/Common/Resources/Views'),
        base_path('resources/views'),
        //Main
        base_path('app/Modules/Customer/Resources/Views'),
        base_path('app/Modules/PaperType/Resources/Views'),
        base_path('app/Modules/Product/Resources/Views'),
        base_path('app/Modules/Supplier/Resources/Views'),
        base_path('app/Modules/Coating/Resources/Views'),
        base_path('app/Modules/Color/Resources/Views'),
        base_path('app/Modules/Thickness/Resources/Views'),
        base_path('app/Modules/Config/Resources/Views'),
        base_path('app/Modules/Width/Resources/Views'),
        base_path('app/Modules/Accessory/Resources/Views'),
        base_path('app/Modules/Coil/Resources/Views'),
        base_path('app/Modules/PanelProfile/Resources/Views'),
        base_path('app/Modules/Adjustment/Resources/Views'),
        base_path('app/Modules/Reports/Resources/Views'),
        base_path('app/Modules/Reports/Coil/Resources/Views'),
        base_path('app/Modules/Reports/Accessory/Resources/Views'),
        base_path('app/Modules/Length/Resources/Views'),
        base_path('app/Modules/InsulationType/Resources/Views'),
        base_path('app/Modules/ScrewType/Resources/Views'),
        base_path('app/Modules/MiscAccessory/Resources/Views'),
        base_path('app/Modules/MiscAccessoryType/Resources/Views'),
        base_path('app/Modules/BackingSide/Resources/Views'),
        base_path('app/Modules/Skylight/Resources/Views'),
        base_path('app/Modules/Insulation/Resources/Views'),
        base_path('app/Modules/Screw/Resources/Views'),
        base_path('app/Modules/StainlessSteelType/Resources/Views'),
        base_path('app/Modules/StainlessSteel/Resources/Views'),
        base_path('app/Modules/InsulationDensity/Resources/Views'),
        base_path('app/Modules/SkylightClass/Resources/Views'),
        base_path('app/Modules/Finish/Resources/Views'),
        base_path('app/Modules/StructuralSteelType/Resources/Views'),
        base_path('app/Modules/DimensionX/Resources/Views'),
        base_path('app/Modules/DimensionY/Resources/Views'),
        base_path('app/Modules/PaintAdhesiveType/Resources/Views'),
        base_path('app/Modules/PvcType/Resources/Views'),
        base_path('app/Modules/PvcDiameter/Resources/Views'),
        base_path('app/Modules/FastenerType/Resources/Views'),
        base_path('app/Modules/FastenerDimension/Resources/Views'),
        base_path('app/Modules/StructuralSteel/Resources/Views'),
        base_path('app/Modules/PaintAdhesive/Resources/Views'),
        base_path('app/Modules/PvcFitting/Resources/Views'),
        base_path('app/Modules/Fastener/Resources/Views'),
        base_path('app/Modules/Brand/Resources/Views'),
        // base_path('app/Modules/Contacts/Resources/Views'),
        //Purchasing
        base_path('app/Modules/PurchaseOrder/Resources/Views'),
        base_path('app/Modules/PurchaseOrderAccessory/Resources/Views'),
        base_path('app/Modules/ReceivingAccessory/Resources/Views'),
        // base_path('app/Modules/PackingList/Resources/Views'),
        // base_path('app/Modules/CommercialInvoice/Resources/Views'),
        // base_path('app/Modules/OutboundPayment/Resources/Views'),
        // base_path('app/Modules/Container/Resources/Views'),
        //Sales
        // base_path('app/Modules/CustomerOrder/Resources/Views'),
        // base_path('app/Modules/CommitOrder/Resources/Views'),
        // base_path('app/Modules/Return/Resources/Views'),
        // base_path('app/Modules/InboundPayment/Resources/Views'),
        base_path('app/Modules/DeliveryReceipt/Resources/Views'),
        base_path('app/Modules/Size/Resources/Views'),
        //Production
        // base_path('app/Modules/JobSheet/Resources/Views'),
        base_path('app/Modules/JobOrders/Resources/Views'),
        //Inventories
        base_path('app/Modules/Inventory/Resources/Views'),
        base_path('app/Modules/InventoryDetail/Resources/Views'),
        // base_path('app/Modules/CurrencyExchange/Resources/Views'),
        //Report
        base_path('app/Modules/Reports/Thickness/Resources/Views'),
        base_path('app/Modules/SummaryOrder/Resources/Views'),
        base_path('app/Modules/Contract/Resources/Views'),
        base_path('app/Modules/InboundPayment/Resources/Views'),
        // base_path('app/Modules/Reports/StatementOfAccount/Resources/Views'),
        // base_path('app/Modules/Reports/Aging/Resources/Views'),

        //New Report Views
        base_path('app/Modules/Reports/Accessory/FastenerReport/Resources/Views'),
        base_path('app/Modules/Reports/Accessory/PaintAdhesiveReport/Resources/Views'),
        base_path('app/Modules/Reports/Accessory/PvcFittingReport/Resources/Views'),
        base_path('app/Modules/Reports/Accessory/StructuralSteelReport/Resources/Views'),
        base_path('app/Modules/Reports/Accessory/StainlessSteelReport/Resources/Views'),
        base_path('app/Modules/Reports/Accessory/MiscAccessoryReport/Resources/Views'),
        base_path('app/Modules/Reports/Accessory/SkylightReport/Resources/Views'),
        base_path('app/Modules/Reports/Accessory/BubbleCoreReport/Resources/Views'),
        base_path('app/Modules/Reports/Accessory/FiberglassReport/Resources/Views'),
        base_path('app/Modules/Reports/Accessory/PEFoamReport/Resources/Views'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Compiled View Path
    |--------------------------------------------------------------------------
    |
    | This option determines where all the compiled Blade templates will be
    | stored for your application. Typically, this is within the storage
    | directory. However, as usual, you are free to change this value.
    |
    */

    'compiled' => realpath(storage_path('framework/views')),
];
