<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Sanlex Inventory System'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', true),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    'log_level' => env('APP_LOG_LEVEL', 'debug'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [
        /*
         * Laravel Framework Service Providers...
         */

        // C1 Common
        CometOneSolutions\Common\Providers\CometOneServiceProvider::class,

        // C1 auth
        CometOneSolutions\Auth\Providers\CometOneAuthServiceProvider::class,
        CometOneSolutions\Auth\Providers\CometOneAuthRouteServiceProvider::class,
        CometOneSolutions\Auth\Providers\RoleServiceProvider::class,
        CometOneSolutions\Auth\Providers\PermissionServiceProvider::class,

        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package Service Providers...
         */
        // CometOneSolutions\Base\Providers\BaseServiceProvider::class,
        // CometOneSolutions\Customer\Providers\CustomerServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

        // App\SanlexServiceProvider::class,
        // App\CapitalInjectionServiceProvider::class,
        // App\InboundPaymentServiceProvider::class,

        //Purchasing

        App\Modules\PurchaseOrder\Providers\PurchaseOrderServiceProvider::class,
        App\Modules\PurchaseOrder\Providers\PurchaseOrderRouteServiceProvider::class,
        App\Modules\PurchaseOrderDetail\Providers\PurchaseOrderDetailServiceProvider::class,
        App\Modules\PurchaseOrderDetail\Providers\PurchaseOrderDetailRouteServiceProvider::class,
//        App\Modules\PackingList\Providers\PackingListServiceProvider::class,
        // App\PackingListRouteServiceProvider::class,
        // App\ContainerServiceProvider::class,
        // App\ContainerRouteServiceProvider::class,
        // App\ContainerDetailServiceProvider::class,
        // App\ContainerDetailRouteServiceProvider::class,
        // App\CommercialInvoiceServiceProvider::class,
        // App\CommercialInvoiceRouteServiceProvider::class,
        // App\OutboundPaymentServiceProvider::class,
        // App\OutboundPaymentRouteServiceProvider::class,
        App\Modules\Receiving\Provider\ReceivingServiceProvider::class,
        App\Modules\Receiving\Provider\ReceivingRouteServiceProvider::class,

        //Purchase Accessory
        App\Modules\PurchaseOrderAccessory\Provider\PurchaseOrderAccessoryServiceProvider::class,
        App\Modules\PurchaseOrderAccessory\Provider\PurchaseOrderAccessoryRouteServiceProvider::class,
        App\Modules\PurchaseOrderAccessoryDetail\Providers\PurchaseOrderAccessoryDetailServiceProvider::class,
        App\Modules\PurchaseOrderAccessoryDetail\Providers\PurchaseOrderAccessoryDetailRouteServiceProvider::class,


        App\Modules\ReceivingAccessory\Provider\ReceivingAccessoryServiceProvider::class,
        App\Modules\ReceivingAccessory\Provider\ReceivingAccessoryRouteServiceProvider::class,
        App\Modules\ReceivingAccessoryDetail\Providers\ReceivingAccessoryDetailServiceProvider::class,

        //Production
        // App\JobSheetServiceProvider::class,
        // App\JobSheetRouteServiceProvider::class,
//        App\JobOrderServiceProvider::class,
//        App\JobOrderRouteServiceProvider::class,
        App\Modules\JobOrders\Providers\JobOrderServiceProvider::class,
        App\Modules\JobOrders\Providers\JobOrderRouteServiceProvider::class,
        //Inventory

        App\Modules\Inventory\Providers\InventoryServiceProvider::class,
        App\Modules\Inventory\Providers\InventoryRouteServiceProvider::class,
        App\Modules\InventoryDetail\Providers\InventoryDetailServiceProvider::class,
        App\Modules\InventoryDetail\Providers\InventoryDetailRouteServiceProvider::class,


        // C1 Inventory
        CometOneSolutions\Inventory\Providers\ItemMovementServiceProvider::class,
        CometOneSolutions\Inventory\Providers\ItemMovementRouteServiceProvider::class,
        CometOneSolutions\Inventory\Providers\LocationServiceProvider::class,
        CometOneSolutions\Inventory\Providers\LocationRouteServiceProvider::class,

        // C1 Accounting
        // CometOneSolutions\Accounting\AccountingRouteServiceProvider::class,
        // CometOneSolutions\Accounting\AccountingServiceProvider::class,

        //Main
        \App\Modules\Customer\Providers\CustomerServiceProvider::class,
        \App\Modules\Customer\Providers\CustomerRouteServiceProvider::class,

        \App\Modules\Product\Providers\ProductServiceProvider::class,
        \App\Modules\Product\Providers\ProductRouteServiceProvider::class,
        \App\Modules\Supplier\Provider\SupplierServiceProvider::class,
        \App\Modules\Supplier\Provider\SupplierRouteServiceProvider::class,

        \App\Modules\Coating\Provider\CoatingServiceProvider::class,
        \App\Modules\Coating\Provider\CoatingRouteServiceProvider::class,
        \App\Modules\Color\Provider\ColorServiceProvider::class,
        \App\Modules\Color\Provider\ColorRouteServiceProvider::class,
        \App\Modules\Width\Provider\WidthServiceProvider::class,
        \App\Modules\Width\Provider\WidthRouteServiceProvider::class,
        \App\Modules\Length\Provider\LengthServiceProvider::class,
        \App\Modules\Length\Provider\LengthRouteServiceProvider::class,
        \App\Modules\InsulationType\Provider\InsulationTypeServiceProvider::class,
        \App\Modules\InsulationType\Provider\InsulationTypeRouteServiceProvider::class,
        \App\Modules\ScrewType\Provider\ScrewTypeServiceProvider::class,
        \App\Modules\ScrewType\Provider\ScrewTypeRouteServiceProvider::class,
        \App\Modules\BackingSide\Provider\BackingSideServiceProvider::class,
        \App\Modules\BackingSide\Provider\BackingSideRouteServiceProvider::class,
        \App\Modules\InsulationDensity\Provider\InsulationDensityServiceProvider::class,
        \App\Modules\InsulationDensity\Provider\InsulationDensityRouteServiceProvider::class,
        \App\Modules\SkylightClass\Provider\SkylightClassServiceProvider::class,
        \App\Modules\SkylightClass\Provider\SkylightClassRouteServiceProvider::class,
        \App\Modules\Thickness\Provider\ThicknessServiceProvider::class,
        \App\Modules\Thickness\Provider\ThicknessRouteServiceProvider::class,
        \App\Modules\PanelProfile\Provider\PanelProfileServiceProvider::class,
        \App\Modules\PanelProfile\Provider\PanelProfileRouteServiceProvider::class,
        \App\Modules\Skylight\Provider\SkylightServiceProvider::class,
        \App\Modules\Skylight\Provider\SkylightRouteServiceProvider::class,
        \App\Modules\Insulation\Provider\InsulationServiceProvider::class,
        \App\Modules\Insulation\Provider\InsulationRouteServiceProvider::class,
        \App\Modules\Screw\Provider\ScrewServiceProvider::class,
        \App\Modules\Screw\Provider\ScrewRouteServiceProvider::class,
        \App\Modules\StainlessSteelType\Provider\StainlessSteelTypeServiceProvider::class,
        \App\Modules\StainlessSteelType\Provider\StainlessSteelTypeRouteServiceProvider::class,
        \App\Modules\PaintAdhesive\Provider\PaintAdhesiveServiceProvider::class,
        \App\Modules\PaintAdhesive\Provider\PaintAdhesiveRouteServiceProvider::class,
        \App\Modules\Fastener\Provider\FastenerServiceProvider::class,
        \App\Modules\Fastener\Provider\FastenerRouteServiceProvider::class,

        \App\Modules\StainlessSteel\Provider\StainlessSteelServiceProvider::class,
        \App\Modules\StainlessSteel\Provider\StainlessSteelRouteServiceProvider::class,
        \App\Modules\Finish\Provider\FinishServiceProvider::class,
        \App\Modules\Finish\Provider\FinishRouteServiceProvider::class,
        \App\Modules\MiscAccessory\Provider\MiscAccessoryServiceProvider::class,
        \App\Modules\MiscAccessoryType\Provider\MiscAccessoryTypeServiceProvider::class,
        \App\Modules\MiscAccessory\Provider\MiscAccessoryRouteServiceProvider::class,
        \App\Modules\MiscAccessoryType\Provider\MiscAccessoryTypeRouteServiceProvider::class,


        \App\Modules\Uom\Providers\UomServiceProvider::class,
        \App\Modules\Accessory\Provider\AccessoryServiceProvider::class,
        \App\Modules\Accessory\Provider\AccessoryRouteServiceProvider::class,
        \App\Modules\Coil\Provider\CoilServiceProvider::class,
        \App\Modules\Coil\Provider\CoilRouteServiceProvider::class,
        \App\Modules\Adjustment\Provider\AdjustmentServiceProvider::class,
        \App\Modules\Adjustment\Provider\AdjustmentRouteServiceProvider::class,

        //Sales
        \App\Modules\DeliveryReceipt\Providers\DeliveryReceiptServiceProvider::class,
        \App\Modules\DeliveryReceipt\Providers\DeliveryReceiptRouteServiceProvider::class,
        \App\Modules\DeliveryReceiptDetails\Providers\DeliveryReceiptDetailServiceProvider::class,

        \App\Modules\SummaryOrder\Provider\SummaryOrderServiceProvider::class,
        \App\Modules\SummaryOrder\Provider\SummaryOrderRouteServiceProvider::class,
        \App\Modules\SummaryOrderDetail\Providers\SummaryOrderDetailServiceProvider::class,

        \App\Modules\Reports\Coil\Providers\CoilReportRouteServiceProvider::class,
        \App\Modules\Reports\Accessory\Providers\AccessoryReportRouteServiceProvider::class,
        \App\Modules\StructuralSteel\Provider\StructuralSteelServiceProvider::class,
        \App\Modules\StructuralSteel\Provider\StructuralSteelRouteServiceProvider::class,
        \App\Modules\DimensionX\Provider\DimensionXServiceProvider::class,
        \App\Modules\DimensionX\Provider\DimensionXRouteServiceProvider::class,
        \App\Modules\DimensionY\Provider\DimensionYServiceProvider::class,
        \App\Modules\DimensionY\Provider\DimensionYRouteServiceProvider::class,
        \App\Modules\PaintAdhesiveType\Provider\PaintAdhesiveTypeServiceProvider::class,
        \App\Modules\PaintAdhesiveType\Provider\PaintAdhesiveTypeRouteServiceProvider::class,
        \App\Modules\Size\Provider\SizeServiceProvider::class,
        \App\Modules\Size\Provider\SizeRouteServiceProvider::class,
        \App\Modules\PvcType\Provider\PvcTypeServiceProvider::class,
        \App\Modules\PvcType\Provider\PvcTypeRouteServiceProvider::class,
        \App\Modules\PvcFitting\Provider\PvcFittingServiceProvider::class,
        \App\Modules\PvcFitting\Provider\PvcFittingRouteServiceProvider::class,
        \App\Modules\PvcDiameter\Provider\PvcDiameterServiceProvider::class,
        \App\Modules\PvcDiameter\Provider\PvcDiameterRouteServiceProvider::class,
        \App\Modules\FastenerType\Provider\FastenerTypeServiceProvider::class,
        \App\Modules\FastenerType\Provider\FastenerTypeRouteServiceProvider::class,
        \App\Modules\FastenerDimension\Provider\FastenerDimensionServiceProvider::class,
        \App\Modules\FastenerDimension\Provider\FastenerDimensionRouteServiceProvider::class,
        \App\Modules\Brand\Provider\BrandServiceProvider::class,
        \App\Modules\Brand\Provider\BrandRouteServiceProvider::class,
        \App\Modules\StructuralSteelType\Provider\StructuralSteelTypeServiceProvider::class,
        \App\Modules\StructuralSteelType\Provider\StructuralSteelTypeRouteServiceProvider::class,

        \App\Modules\Contract\Providers\ContractRouteServiceProvider::class,
        \App\Modules\Contract\Providers\ContractServiceProvider::class,

        \App\Modules\InboundPayment\Providers\InboundPaymentRouteServiceProvider::class,
        \App\Modules\InboundPayment\Providers\InboundPaymentServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [
        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
    ],
];
