<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SupplierCreateTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        // $this->faker = Faker::create();
    }

    /**
     * @group supplier-create
     */
    public function test()
    {
        $supplierToo = resolve(\App\SupplierRecordService::class)->create('Howell', new \App\Address('1', '2', '3'));
        $payload = [
            'name' => 'Jhovee',
            'mainAddress' => [
                'street' => '123 Sesame Street',
                'city' => 'Manila',
                'zip' => '123',
            ],
            'partners' => [
                [
                    'supplierId' => $supplierToo->getId(),
                    'partnerTypeName' => '',
                    'partnerTypeId' => 1,
                ], [
                    'supplierId' => '',
                    'partnerTypeName' => '',
                    'partnerTypeId' => 2,
                ],
            ],
        ];

        $response = $this->json('POST', '/api/suppliers', $payload);
        $responseJson = json_decode($response->getContent());
        $supplierId = $responseJson->data->id;
        $response->assertStatus(200);
        $this->assertDatabaseHas('supplier_partnerships', [
            'partner_type_id' => 1,
            'parent_id' => $supplierId,
            'partner_id' => $supplierToo->getId(),
        ]);
        $this->assertDatabaseHas('supplier_partnerships', [
            'partner_type_id' => 2,
            'parent_id' => $supplierId,
            'partner_id' => $supplierId,
        ]);
        // $expected = [
        //     'quantity' => $payload['quantity'],
        //     'unitPrice' => 2.04,
        //     'reorderPrice' => 2.04,
        // ];
        // $response->assertExactJson([
        //     'data' => $expected
        // ]);
    }
}
