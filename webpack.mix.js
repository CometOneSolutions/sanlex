let mix = require('laravel-mix');


let c1_common_js = __dirname + "/CometOneSolutions/Common/Resources/Assets/js";
let c1_auth_js = __dirname + "/CometOneSolutions/Auth/Resources/Assets/js";
let c1_banking_js = __dirname + "/CometOneSolutions/Banking";
let c1_inventory_js = __dirname + "/CometOneSolutions/Inventory";
let c1_module_js = __dirname + "/app/Modules";
let c1_accounting_js = __dirname + "/CometOneSolutions/Accounting/Resources/Assets/js";

mix.webpackConfig({
    resolve: {
        extensions: [".js", ".vue", ".json"],
        alias: {
            // 'vue$': 'vue/dist/vue.esm.js',
            // '@': __dirname + '/resources/assets/js',
            "@c1_common_js": c1_common_js,
            "@c1_auth_js": c1_auth_js,
            "@c1_module_js": c1_module_js,
            "@c1_banking_js": c1_banking_js,
            "@c1_inventory_js": c1_inventory_js,
            "@c1_accounting_js": c1_accounting_js,
        }
    }
});
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps4
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


// .copy('resources/coreui/Static_Full_Project/vendors/js/popper.min.js', 'public/js/popper.js')
// .copy('resources/coreui/Static_Full_Project/vendors/js/select2.min.js', 'public/js/select2.js')
// .copy('resources/coreui/Static_Full_Project/vendors/js/jquery.maskedinput.min.js', 'public/js/masked.js');


mix
    // .js('resources/coreui/Static_Full_Project/vendors/js/bootstrap.min.js', 'public/js/bootstrap.js')
    .js(`${c1_common_js}/app.js`, "public/js")
    .js(`${c1_common_js}/index.js`, "public/js")
    .js(`${c1_common_js}/dashboard.js`, "public/js")
    .js(`${c1_auth_js}/user.js`, "public/js")
    .js(`${c1_auth_js}/role.js`, "public/js")
    // .js('resources/assets/js/adjustment.js', 'public/js')
    // .js('app/Modules/CurrencyExchange/Resources/Assets/js/currency-exchange.js', 'public/js')
    // .js("app/Modules/Container/Resources/Assets/js/container.js", "public/js")
    // .js("app/Modules/Container/Resources/Assets/js/container-detail.js", "public/js")
    .js("app/Modules/Inventory/Resources/Assets/js/inventory.js", "public/js")
    .js("app/Modules/InventoryDetail/Resources/Assets/js/transaction-index.js", "public/js")
    .js("app/Modules/PurchaseOrder/Resources/Assets/js/purchase.js", "public/js")
    .js("app/Modules/PurchaseOrderAccessory/Resources/Assets/js/purchase-order-accessory.js", "public/js")
    .js("app/Modules/ReceivingAccessory/Resources/Assets/js/receiving-accessory.js", "public/js")
    // .js("app/Modules/PurchaseOrder/Resources/Assets/js/attachment-purchase-order.js", "public/js")
    // .js("app/Modules/PackingList/Resources/Assets/js/packing-list.js", "public/js")
    // .js("app/Modules/CommercialInvoice/Resources/Assets/js/commercial-invoice.js", "public/js")
    // .js("app/Modules/OutboundPayment/Resources/Assets/js/outbound-payment.js", "public/js")
    // .js("app/Modules/CustomerOrder/Resources/Assets/js/customer-order.js", "public/js")
    // .js("app/Modules/CustomerOrderDetails/Resources/Assets/js/customer-order-detail.js", "public/js")
    .js("app/Modules/DeliveryReceipt/Resources/Assets/js/delivery-receipt.js", "public/js")
    // .js("app/Modules/Return/Resources/Assets/js/sales-return.js", "public/js")
    // .js("app/Modules/JobSheet/Resources/Assets/js/job-sheet.js", "public/js")
    .js("app/Modules/JobOrders/Resources/Assets/js/job-order.js", "public/js")
    .js("app/Modules/Customer/Resources/Assets/js/customer.js", "public/js")
    // .js("app/Modules/PaperType/Resources/Assets/js/paper-type.js", "public/js")
    .js("app/Modules/Product/Resources/Assets/js/product.js", "public/js")
    .js("app/Modules/Skylight/Resources/Assets/js/skylight.js", "public/js")
    .js("app/Modules/Insulation/Resources/Assets/js/insulation.js", "public/js")
    .js("app/Modules/Screw/Resources/Assets/js/screw.js", "public/js")
    // .js("app/Modules/Config/Resources/Assets/js/config.js", "public/js")
    .js("app/Modules/Supplier/Resources/Assets/js/supplier.js", "public/js")
    .js("app/Modules/Coating/Resources/Assets/js/coating.js", "public/js")
    .js("app/Modules/Color/Resources/Assets/js/color.js", "public/js")
    .js("app/Modules/Thickness/Resources/Assets/js/thickness.js", "public/js")
    .js("app/Modules/Accessory/Resources/Assets/js/accessory.js", "public/js")
    .js("app/Modules/Width/Resources/Assets/js/width.js", "public/js")
    .js("app/Modules/Length/Resources/Assets/js/length.js", "public/js")
    .js("app/Modules/StainlessSteelType/Resources/Assets/js/stainless-steel-type.js", "public/js")
    .js("app/Modules/StructuralSteelType/Resources/Assets/js/structural-steel-type.js", "public/js")
    .js("app/Modules/StructuralSteel/Resources/Assets/js/structural-steel.js", "public/js")
    .js("app/Modules/DimensionX/Resources/Assets/js/dimension-x.js", "public/js")
    .js("app/Modules/DimensionY/Resources/Assets/js/dimension-y.js", "public/js")
    .js("app/Modules/StainlessSteel/Resources/Assets/js/stainless-steel.js", "public/js")
    .js("app/Modules/SkylightClass/Resources/Assets/js/skylight-class.js", "public/js")
    .js("app/Modules/InsulationType/Resources/Assets/js/insulation-type.js", "public/js")
    .js("app/Modules/InsulationDensity/Resources/Assets/js/insulation-density.js", "public/js")
    .js("app/Modules/ScrewType/Resources/Assets/js/screw-type.js", "public/js")
    .js("app/Modules/MiscAccessory/Resources/Assets/js/misc-accessory.js", "public/js")
    .js("app/Modules/MiscAccessoryType/Resources/Assets/js/misc-accessory-type.js", "public/js")
    .js("app/Modules/PaintAdhesiveType/Resources/Assets/js/paint-adhesive-type.js", "public/js")
    .js("app/Modules/PaintAdhesive/Resources/Assets/js/paint-adhesive.js", "public/js")
    .js("app/Modules/PvcType/Resources/Assets/js/pvc-type.js", "public/js")
    .js("app/Modules/PvcFitting/Resources/Assets/js/pvc-fitting.js", "public/js")
    .js("app/Modules/PvcDiameter/Resources/Assets/js/pvc-diameter.js", "public/js")
    .js("app/Modules/FastenerType/Resources/Assets/js/fastener-type.js", "public/js")
    .js("app/Modules/FastenerDimension/Resources/Assets/js/fastener-dimension.js", "public/js")
    .js("app/Modules/Brand/Resources/Assets/js/brand.js", "public/js")
    .js("app/Modules/SummaryOrder/Resources/Assets/js/summary-order.js", "public/js")

    .js("app/Modules/BackingSide/Resources/Assets/js/backing-side.js", "public/js")
    .js("app/Modules/Size/Resources/Assets/js/size.js", "public/js")
    .js("app/Modules/Coil/Resources/Assets/js/coil.js", "public/js")
    .js("app/Modules/PanelProfile/Resources/Assets/js/panel-profile.js", "public/js")
    .js("app/Modules/Adjustment/Resources/Assets/js/adjustment.js", "public/js")
    .js("app/Modules/Finish/Resources/Assets/js/finish.js", "public/js")
    .js("app/Modules/Fastener/Resources/Assets/js/fastener.js", "public/js")
    .js("app/Modules/Contract/Resources/Assets/js/contract.js", "public/js")
    .js("app/Modules/InboundPayment/Resources/Assets/js/inbound-payment.js", "public/js")

    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/accessory-report.scss', 'public/css')
    .sass('resources/assets/sass/report-responsive.scss', 'public/css')
    .js('resources/assets/js/report.js', 'public/js')
    .version();
